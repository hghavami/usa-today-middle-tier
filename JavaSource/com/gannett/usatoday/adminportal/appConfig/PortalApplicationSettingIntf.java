package com.gannett.usatoday.adminportal.appConfig;

import org.joda.time.DateTime;

public interface PortalApplicationSettingIntf {

	public static final String UT_DEFAULT_KEYCODE = "usat.ut.default.keycode";
	public static final String SW_DEFAULT_KEYCODE = "usat.sw.default.keycode";

	public static final String UT_EXPIRED_KEYCODE = "usat.ut.default.expired.offer";
	public static final String SW_EXPIRED_KEYCODE = "usat.sw.default.expired.offer";

	public static final String FORCE_EZPAY_GROUP = "forceEZPayOffer";
	public static final String UT_DEFAULT_RENEWAL_RATECODE = "usat.ut.default.renewal.ratecode";
	public static final String SW_DEFAULT_RENEWAL_RATECODE = "usat.sw.default.renewal.ratecode";

	public static final String UT_DEFAULT_FORCE_EZPAY_OFFER_RATECODE = "usat.ut.default.forceEZPayOffer.rateCode";
	public static final String SW_DEFAULT_FORCE_EZPAY_OFFER_RATECODE = "usat.sw.default.forceEZPayOffer.rateCode";

	public static final String UT_FORCE_EZPAY_OFFER_ENABLED = "usat.ut.default.forceEZPayOffer.enabled";
	public static final String SW_FORCE_EZPAY_OFFER_ENABLED = "usat.sw.default.forceEZPayOffer.enabled";

	public static final String PORTAL_ADMIN_PUBLISH_ALERT_RECIPIENTS = "usat.admin.campaign.publishingAlerts";

	public static final String PRODUCTION_DOMAIN = "usat.production.domain";
	public static final String PORTAL_ADMIN_DESCRIPTION = "usat.subportaladmin.sys.description";

	// ICON API Context.
	public static final String ICON_API_GROUP = "icon_api";
	public static final String ICON_API_BASE_URL = "base_url";
	public static final String ICON_API_SECURE_BASE_URL = "secure_base_url";
	public static final String ICON_API_SECURE_SELF_SERV_URL = "secure_self_serv_url";
	public static final String ICON_API_SECURE_SELF_SERV_URL_PATH = "secure_self_serv_url_path";	
	public static final String ICON_API_SECURE_SELF_SERV_URL_MAKE_PAYMENT_PATH = "secure_self_serv_url_make_payment_path";
	public static final String ICON_API_SECURE_SELF_SERV_URL_CHANGE_CARD_PATH = "secure_self_serv_url_change_credit_card_path";
	public static final String ICON_API_SECURE_SELF_SERV_URL_CHANGE_CARD_EZPAY_PATH = "secure_self_serv_url_signup_ezpay_path";
	public static final String ICON_API_ADDRESS_URL = "address_url";
	public static final String ICON_API_SECURE_ADDRESS_URL = "secure_address_url";
	public static final String ICON_API_DEBUG_MODE = "debug_mode";
	public static final String ICON_API_GCI_UNIT = "gci_unit";
	public static final String ICON_API_UID = "api_user_id";
	public static final String ICON_API_PWD = "api_password";
	
	// Firefly API Context
	public static final String FIREFLY_API_GROUP = "firefly_api";
	public static final String FIREFLY_API_MARKET_ID = "market_id";
	public static final String FIREFLY_API_DEBUG_MODE = "debug_mode_firefly";
	public static final String FIREFLY_API_BASE_URL = "base_firefly_url";
	public static final String FIREFLY_API_BASE_USER_SERVICE_URL = "base_user_service_url";
	public static final String FIREFLY_API_UID = "api_user_id";
	public static final String FIREFLY_API_PWD = "api_password";
	public static final String FIREFLY_API_EXTERNAL_SECRET = "api_external_secret";
	public static final String FIREFLY_API_INTERNAL_SECRET = "api_internal_secret";

	// UserService API Context
	public static final String USERSERVICE_API_GROUP = "userservice_api";
	public static final String USERSERVICE_API_MARKET_ID = "market_id";
	public static final String USERSERVICE_API_DEBUG_MODE = "debug_mode_userservice";
	public static final String USERSERVICE_API_BASE_URL = "base_userservice_url";
	public static final String USERSERVICE_API_BASE_USER_SERVICE_URL = "base_userservice_url";
	public static final String USERSERVICE_API_UID = "api_user_id";
	public static final String USERSERVICE_API_PWD = "api_password";
	public static final String USERSERVICE_API_EXTERNAL_SECRET = "api_external_secret";
	public static final String USERSERVICE_API_INTERNAL_SECRET = "api_internal_secret";
	public static final String USERSERVICE_API_MODIFIED_BY = "api_modified_by";
	public static final String USERSERVICE_API_CREDENTIAL_TYPE = "credential_type";
	public static final String USERSERVICE_API_ON_SUCCESS_REDIRECT_URL = "api_on_success_redirect_url";

	
	public int getID();

	public DateTime getLastUpdateTime();

	public String getGroup();

	public String getKey();

	public String getValue();

	public boolean getValueAsBoolean();

	public String getLabel();

	public String getDescription();

}
