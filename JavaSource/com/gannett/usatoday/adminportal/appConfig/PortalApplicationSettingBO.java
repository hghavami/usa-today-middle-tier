package com.gannett.usatoday.adminportal.appConfig;

import org.joda.time.DateTime;

public class PortalApplicationSettingBO implements PortalApplicationSettingIntf {
	private int id = -1;
	private String group = null;
	private String key = null;
	private String value = null;
	private String description = null;
	private DateTime lastUpdateTime = null;
	private String label = null;

	public PortalApplicationSettingBO() {
		super();
	}

	public PortalApplicationSettingBO(PortalApplicationSettingIntf source) {
		super();
		this.id = source.getID();
		this.group = source.getGroup();
		this.key = source.getKey();
		this.value = source.getValue();
		this.description = source.getDescription();
		this.lastUpdateTime = source.getLastUpdateTime();
		this.label = source.getLabel();
	}

	public String getDescription() {
		return this.description;
	}

	public String getGroup() {
		return this.group;
	}

	public int getID() {
		return this.id;
	}

	public String getKey() {
		return this.key;
	}

	public String getValue() {
		return this.value;
	}

	public void setDescription(String newDescription) {
		if (newDescription != null) {
			newDescription = newDescription.trim();
		}
		this.description = newDescription;
	}

	public void setGroup(String newGroup) {
		if (newGroup != null) {
			newGroup = newGroup.trim();
		}
		this.group = newGroup;
	}

	public void setKey(String newKey) {
		if (newKey != null) {
			newKey = newKey.trim();
		}
		this.key = newKey;
	}

	public void setValue(String newValue) {
		if (newValue != null) {
			newValue = newValue.trim();
		}
		this.value = newValue;
	}

	public DateTime getLastUpdateTime() {
		return lastUpdateTime;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public boolean getValueAsBoolean() {

		return Boolean.parseBoolean(this.getValue());

	}
}
