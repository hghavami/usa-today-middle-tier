package com.usatoday.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.subscriptionTransactions.ExtranetSubscriberTransactionIntf;
import com.usatoday.util.constants.UsaTodayConstants;

public class SubscriberTransactionDAO extends USATodayDAO {

	public static final int NEW = 0;
	public static final int IN_PROCESSING = 1;
	public static final int PROCESSED = 2;
	public static final int REPROCESSED = 3;

	public static final String TRANS_COLUMNS = "*";
	public static final String TRANS_COLUMNS_2 = "id, Active, ABCCode, AddrTyp, AgencyName, AppFlag, BankActNum, BankRtNum, BatchNum, "
			+ "PubCode, TranRecTyp, LastName, FirstName, FirmName, AddAddr1, AddAddr2, UnitNum, HalfUnitNum, StreetDir, StreetName, StreetTyp, "
			+ "StreetPstDir, SubUnitCode, SubUnitNum, City, ShortCity, State, Zip5, Zip4, HomePhone, BusPhone, TranType, TranCode, BillLastName, BillFirstName, "
			+ "BillFirmName, BillAddr1, BillAddr2, BillUnitNum, BillHalfUnitNum, BillStreetDir, BillStreetName, BillStreetType, BillStreetPostDir, "
			+ "BillSubUnitCode, BillSubUnitNum, BillCity, BillShortCity, BillState, BillZip5, BillZip4, BillHomePhone, BillBusPhone, SubsDur, CCType, CCNum, CCExpireDt, "
			+ "BillDelPtBarCode, BillCarrRteSort, BillAddrType, CarrRteSort, CCCIDCode, CheckNum, CensusTract, CensusBlkGrp, CurrAddrNum, CurrMktcode,"
			+ "EffDate1, PromoCode, Comment, TtlMktCov, SubsAmount, RejComm, AutoRegComm, Code1ErrCode, PrintFlag, Filler, SrcOrdCode, ContestCode, "
			+ "DelPref, OneTimeBill, RateCode, PerpCCFlag, BillChargePd, NewAddlAddr1, NewAddlAddr2, CCAuthCode, CCAuthDate, CCAuthRejDesc, EmailType, "
			+ "EmailAddress, BadSubAddress, BadTrnGftAddress, CCBatchProc, Status, NumPaper, PayAmount, Premium, ClubNum, ClubType, Trandate, CurrAcctNum, "
			+ "NewLastName, NewFirstName, NewFirmName, NewUnitNum, NewHalfUnitNum, NewStreetDir, NewStreetName, NewStreetType, NewStreetPostDir, "
			+ "NewSubUnitNum, NewSubUnitCode, NewCity, NewState, NewZip5, NewPhoneNum, NewBusPhoneNum, DltGiftPayer, CreditDay1, CreditDay3, "
			+ "ContactCust, ExstDelMethod, MicrTranCode, insert_timestamp, update_timestamp";

	// insert subscription transaction
	public static java.lang.String INSERT_SUBSCRIPTION_TRX = "INSERT INTO XTRNTDWLD "
			+ "(PubCode, TranRecTyp, LastName, FirstName, FirmName, AddAddr1, AddAddr2, UnitNum, HalfUnitNum, StreetDir, StreetName, StreetTyp, StreetPstDir, SubUnitCode, SubUnitNum, City, State, Zip5, HomePhone, BusPhone, TranType, TranCode, BillLastName, BillFirstName, BillFirmName, BillAddr1, BillAddr2, BillUnitNum, BillHalfUnitNum, BillStreetDir, BillStreetName, BillStreetType, BillStreetPostDir, BillSubUnitCode, BillSubUnitNum, BillCity, BillState, BillZip5, BillHomePhone, BillBusPhone, SubsDur, CCType, CCNum, CCExpireDt, EffDate1, PromoCode, Comment, TtlMktCov, SubsAmount, RejComm, AutoRegComm, Code1ErrCode, PrintFlag, Filler, SrcOrdCode, ContestCode, DelPref, OneTimeBill, RateCode, PerpCCFlag, BillChargePd, NewAddlAddr1, NewAddlAddr2, CCAuthCode, CCAuthDate, EmailType, EmailAddress, BadSubAddress, BadTrnGftAddress, CCBatchProc, Status, NumPaper, PayAmount, Premium, ClubNum, Trandate, CurrAcctNum, NewLastName, NewFirstName, NewFirmName, NewUnitNum, NewHalfUnitNum, NewStreetDir, NewStreetName, NewStreetType, NewStreetPostDir, NewSubUnitNum, NewSubUnitCode, NewCity, NewState, NewZip5, NewPhoneNum, NewBusPhoneNum, DltGiftPayer, CreditDay1, CreditDay3, ContactCust, ExstDelMethod, insert_timestamp, update_timestamp, MicrTranCode)"
			+ "VALUES "
			+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	// updates for batch processing
	public static final String UPDATE_TRANSACTION_STATE = "update XTRNTDWLD set update_timestamp = ?, transactionstate = ? where transactionstate = ?";
	public static final String UPDATE_TRANSACTION_STATE_FOR_BATCH = "update XTRNTDWLD set update_timestamp = ?, transactionstate = ? where batch_id = ?";
	public static final String UPDATE_BATCH_ID = "update XTRNTDWLD set batch_id = ? where transactionstate = "
			+ SubscriberTransactionDAO.IN_PROCESSING;

	// query for batch processing
	public static final String SELECT_TRANS_IN_STATE = "select " + SubscriberTransactionDAO.TRANS_COLUMNS
			+ " from XTRNTDWLD where transactionstate = ?";
	public static final String SELECT_BY_BATCHID = "select " + SubscriberTransactionDAO.TRANS_COLUMNS
			+ " from XTRNTDWLD where batch_id = ?";

	// misc
	public static final String SELECT_LAST_BATCHID = "select distinct batch_id from XTRNTDWLD where batch_id like ? order by batch_id asc";

	// query counts
	public static final String SELECT_COUNT_OF_TRANS_IN_STATE = "select count(*) from XTRNTDWLD where transactionstate = ?";

	// Deletion for removing archive
	public static final String DELETE_OLD_RECORDS = "delete from XTRNTDWLD where transactionstate >= "
			+ SubscriberTransactionDAO.PROCESSED + " and update_timestamp <= ?";

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private Collection<ExtranetSubscriberTransactionIntf> objectFactory(ResultSet rs) throws UsatException {
		ArrayList<ExtranetSubscriberTransactionIntf> trans = new ArrayList<ExtranetSubscriberTransactionIntf>();

		try {

			while (rs.next()) {

				SubscriberTransactionTO tran = new SubscriberTransactionTO();

				long primaryKey = -1;

				primaryKey = rs.getLong("id");
				tran.setID(primaryKey);

				String temp;

				temp = rs.getString("PubCode");
				if (temp != null) {
					tran.setPubCode(temp);
				}

				temp = rs.getString("AddrTyp");
				if (temp != null) {
					tran.setAddrType(temp);
				}

				temp = rs.getString("AgencyName");
				if (temp != null) {
					tran.setAgencyName(temp);
				}

				temp = rs.getString("AppFlag");
				if (temp != null) {
					tran.setAppFlag(temp);
				}

				temp = rs.getString("BankActNum");
				if (temp != null) {
					tran.setBankAccountNum(temp);
				}

				temp = rs.getString("BankRtNum");
				if (temp != null) {
					tran.setBankRouteNum(temp);
				}

				temp = rs.getString("BillDelPtBarCode");
				if (temp != null) {
					tran.setBillingDelPtBarCode(temp);
				}

				temp = rs.getString("BillCarrRteSort");
				if (temp != null) {
					tran.setBillingCarrRteSort(temp);
				}

				temp = rs.getString("CarrRteSort");
				if (temp != null) {
					tran.setCarrRouteSort(temp);
				}

				temp = rs.getString("CCCIDCode");
				if (temp != null) {
					tran.setcCCIDCode(temp);
				}

				temp = rs.getString("CheckNum");
				if (temp != null) {
					tran.setCheckNum(temp);
				}

				temp = rs.getString("CensusTract");
				if (temp != null) {
					tran.setCensusTract(temp);
				}

				temp = rs.getString("CensusBlkGrp");
				if (temp != null) {
					tran.setCensusBlkGrp(temp);
				}

				temp = rs.getString("EntryMethod");
				if (temp != null) {
					tran.setEntryMethod(temp);
				}

				temp = rs.getString("FIPSCounty");
				if (temp != null) {
					tran.setfIPSCounty(temp);
				}

				temp = rs.getString("FIPSState");
				if (temp != null) {
					tran.setfIPSState(temp);
				}

				temp = rs.getString("FreqDel");
				if (temp != null) {
					tran.setFrequencyOfDelivery(temp);
				}

				temp = rs.getString("FulFillItem");
				if (temp != null) {
					tran.setFulfillItem(temp);
				}

				temp = rs.getString("TranRecTyp");
				if (temp != null) {
					tran.setTransactionRecType(temp);
				}

				temp = rs.getString("CurrAddrNum");
				if (temp != null) {
					tran.setCurrAddrNum(temp);
				}

				temp = rs.getString("CurrMktcode");
				if (temp != null) {
					tran.setCurrMarketCode(temp);
				}

				temp = rs.getString("CurrPrtSiteCode");
				if (temp != null) {
					tran.setCurrPrtSiteCode(temp);
				}

				temp = rs.getString("CreditAdjAmount");
				if (temp != null) {
					tran.setCreditAdjAmount(temp);
				}

				temp = rs.getString("DebtAdjAmount");
				if (temp != null) {
					tran.setDebtAdjAmount(temp);
				}

				temp = rs.getString("DelPtBarCode");
				if (temp != null) {
					tran.setDelPtBarCode(temp);
				}

				temp = rs.getString("District");
				if (temp != null) {
					tran.setDistrict(temp);
				}

				temp = rs.getString("DiscAgncyFlg");
				if (temp != null) {
					tran.setDiscAgncyFlg(temp);
				}

				temp = rs.getString("DeptRef");
				if (temp != null) {
					tran.setDeptRef(temp);
				}

				temp = rs.getString("GatedCommFlg");
				if (temp != null) {
					tran.setGatedCommFlg(temp);
				}

				temp = rs.getString("GateSecCode");
				if (temp != null) {
					tran.setGateSecCode(temp);
				}

				temp = rs.getString("GiftCard");
				if (temp != null) {
					tran.setGiftCard(temp);
				}

				temp = rs.getString("GiftPayerTMC");
				if (temp != null) {
					tran.setGiftPayerTMC(temp);
				}

				temp = rs.getString("IssuePdUnpd");
				if (temp != null) {
					tran.setIssuePdUnpd(temp);
				}

				temp = rs.getString("InfoRTY2Based");
				if (temp != null) {
					tran.setInfoRTY2Based(temp);
				}

				temp = rs.getString("LastName");
				if (temp != null) {
					tran.setLastName(temp);
				}

				temp = rs.getString("FirstName");
				if (temp != null) {
					tran.setFirstName(temp);
				}

				temp = rs.getString("FirmName");
				if (temp != null) {
					tran.setFirmName(temp);
				}

				temp = rs.getString("AddAddr1");
				if (temp != null) {
					tran.setAddAddr1(temp);
				}

				temp = rs.getString("AddAddr2");
				if (temp != null) {
					tran.setAddAddr2(temp);
				}
				temp = rs.getString("UnitNum");
				if (temp != null) {
					tran.setUnitNum(temp);
				}
				temp = rs.getString("HalfUnitNum");
				if (temp != null) {
					tran.setHalfUnitNum(temp);
				}
				temp = rs.getString("StreetDir");
				if (temp != null) {
					tran.setStreetDir(temp);
				}
				temp = rs.getString("StreetName");
				if (temp != null) {
					tran.setStreetName(temp);
				}
				temp = rs.getString("StreetTyp");
				if (temp != null) {
					tran.setStreetType(temp);
				}
				temp = rs.getString("StreetPstDir");
				if (temp != null) {
					tran.setStreetPostDir(temp);
				}

				temp = rs.getString("SubUnitCode");
				if (temp != null) {
					tran.setSubUnitCode(temp);
				}

				temp = rs.getString("SubUnitNum");
				if (temp != null) {
					tran.setSubUnitNum(temp);
				}

				temp = rs.getString("City");
				if (temp != null) {
					tran.setCity(temp);
				}

				temp = rs.getString("ShortCity");
				if (temp != null) {
					tran.setShortCity(temp);
				}

				temp = rs.getString("State");
				if (temp != null) {
					tran.setState(temp);
				}

				temp = rs.getString("Zip5");
				if (temp != null) {
					tran.setZip5(temp);
				}

				temp = rs.getString("Zip4");
				if (temp != null) {
					tran.setZip4(temp);
				}

				temp = rs.getString("HomePhone");
				if (temp != null) {
					tran.setHomePhone(temp);
				}

				temp = rs.getString("BusPhone");
				if (temp != null) {
					tran.setBusinessPhone(temp);
				}

				temp = rs.getString("Latitude");
				if (temp != null) {
					tran.setLatitude(temp);
				}

				temp = rs.getString("Longtitude");
				if (temp != null) {
					tran.setLongitude(temp);
				}

				temp = rs.getString("MarketCode");
				if (temp != null) {
					tran.setMarketCode(temp);
				}

				temp = rs.getString("MktSeg");
				if (temp != null) {
					tran.setMarketSeg(temp);
				}

				temp = rs.getString("OperID");
				if (temp != null) {
					tran.setOperatorID(temp);
				}

				temp = rs.getString("OvrCode1");
				if (temp != null) {
					tran.setOverrideCode1(temp);
				}

				temp = rs.getString("PIATranCode");
				if (temp != null) {
					tran.setPiaTranCode(temp);
				}

				temp = rs.getString("PndLookSts");
				if (temp != null) {
					tran.setPndLookSts(temp);
				}

				temp = rs.getString("TranType");
				if (temp != null) {
					tran.setTransactionType(temp);
				}

				temp = rs.getString("TranCode");
				if (temp != null) {
					tran.setTransactionCode(temp);
				}

				temp = rs.getString("BillAddrType");
				if (temp != null) {
					tran.setBillingAddrType(temp);
				}

				temp = rs.getString("BillLastName");
				if (temp != null) {
					tran.setBillingLastName(temp);
				}

				temp = rs.getString("BillFirstName");
				if (temp != null) {
					tran.setBillingFirstName(temp);
				}

				temp = rs.getString("BillFirmName");
				if (temp != null) {
					tran.setBillingFirmName(temp);
				}

				temp = rs.getString("BillAddr1");
				if (temp != null) {
					tran.setBillingAddress1(temp);
				}

				temp = rs.getString("BillAddr2");
				if (temp != null) {
					tran.setBillingAddress2(temp);
				}

				temp = rs.getString("BillUnitNum");
				if (temp != null) {
					tran.setBillingUnitNum(temp);
				}

				temp = rs.getString("BillHalfUnitNum");
				if (temp != null) {
					tran.setBillingHalfUnitNum(temp);
				}

				temp = rs.getString("BillStreetDir");
				if (temp != null) {
					tran.setBillingStreetDir(temp);
				}

				temp = rs.getString("BillStreetName");
				if (temp != null) {
					tran.setBillingStreetName(temp);
				}

				temp = rs.getString("BillStreetType");
				if (temp != null) {
					tran.setBillingStreetType(temp);
				}

				temp = rs.getString("BillStreetPostDir");
				if (temp != null) {
					tran.setBillingStreetPostDir(temp);
				}

				temp = rs.getString("BillSubUnitCode");
				if (temp != null) {
					tran.setBillingSubUnitCode(temp);
				}

				temp = rs.getString("BillSubUnitNum");
				if (temp != null) {
					tran.setBillingSubUnitNum(temp);
				}

				temp = rs.getString("BillCity");
				if (temp != null) {
					tran.setBillingCity(temp);
				}

				temp = rs.getString("BillShortCity");
				if (temp != null) {
					tran.setBillingShortCity(temp);
				}

				temp = rs.getString("BillState");
				if (temp != null) {
					tran.setBillingState(temp);
				}
				temp = rs.getString("BillZip5");
				if (temp != null) {
					tran.setBillingZip5(temp);
				}

				temp = rs.getString("BillZip4");
				if (temp != null) {
					tran.setBillingZip4(temp);
				}

				temp = rs.getString("BillHomePhone");
				if (temp != null) {
					tran.setBillingHomePhone(temp);
				}
				temp = rs.getString("BillBusPhone");
				if (temp != null) {
					tran.setBillingBusinessPhone(temp);
				}
				temp = rs.getString("SubsDur");
				if (temp != null) {
					tran.setSubscriptionDuration(temp);
				}
				temp = rs.getString("CCType");
				if (temp != null) {
					tran.setCreditCardType(temp);
				}

				temp = rs.getString("CCNum");
				if (temp != null) {
					tran.setCreditCardNumber(temp);
				}
				temp = rs.getString("CCExpireDt");
				if (temp != null) {
					tran.setCreditCardExpirationDate(temp);
				}

				temp = rs.getString("CCAuthRejDesc");
				if (temp != null) {
					tran.setCreditCardAuthRejDesc(temp);
				}

				temp = rs.getString("EffDate1");
				if (temp != null) {
					tran.setEffectiveDate(temp);
				}

				temp = rs.getString("EffDate2");
				if (temp != null) {
					tran.setEffectiveDate2(temp);
				}
				temp = rs.getString("PromoCode");
				if (temp != null) {
					tran.setPromoCode(temp);
				}
				temp = rs.getString("Comment");
				if (temp != null) {
					tran.setComment(temp);
				}
				temp = rs.getString("TtlMktCov");
				if (temp != null) {
					tran.setTtlMktCov(temp);
				}
				temp = rs.getString("SubsAmount");
				if (temp != null) {
					tran.setSubscriptionAmount(temp);
				}
				temp = rs.getString("RejComm");
				if (temp != null) {
					tran.setRejComment(temp);
				}
				temp = rs.getString("AutoRegComm");
				if (temp != null) {
					tran.setAutoRegComm(temp);
				}
				temp = rs.getString("Code1ErrCode");
				if (temp != null) {
					tran.setCode1ErrorCode(temp);
				}
				temp = rs.getString("PrintFlag");
				if (temp != null) {
					tran.setPrintFlag(temp);
				}
				temp = rs.getString("Filler");
				if (temp != null) {
					tran.setFiller(temp);
				}
				temp = rs.getString("SrcOrdCode");
				if (temp != null) {
					tran.setSrcOrdCode(temp);
				}
				temp = rs.getString("ContestCode");
				if (temp != null) {
					tran.setContestCode(temp);
				}
				temp = rs.getString("DelPref");
				if (temp != null) {
					tran.setDeliveryPreference(temp);
				}
				temp = rs.getString("OneTimeBill");
				if (temp != null) {
					tran.setOneTimeBill(temp);
				}
				temp = rs.getString("RateCode");
				if (temp != null) {
					tran.setRateCode(temp);
				}
				temp = rs.getString("PerpCCFlag");
				if (temp != null) {
					tran.setPerpCCFlag(temp);
				}
				temp = rs.getString("BillChargePd");
				if (temp != null) {
					tran.setBillChargePd(temp);
				}
				temp = rs.getString("NewAddlAddr1");
				if (temp != null) {
					tran.setNewAddlAddr1(temp);
				}

				temp = rs.getString("NewAddlAddr2");
				if (temp != null) {
					tran.setNewAddlAddr2(temp);
				}

				temp = rs.getString("CCAuthCode");
				if (temp != null) {
					tran.setCreditCardAuthCode(temp);
				}

				temp = rs.getString("CCAuthDate");
				if (temp != null) {
					tran.setCreditCardAuthDate(temp);
				}

				temp = rs.getString("EmailType");
				if (temp != null) {
					tran.setEmailType(temp);
				}

				temp = rs.getString("EmailAddress");
				if (temp != null) {
					tran.setEmailAddress(temp);
				}

				temp = rs.getString("BadSubAddress");
				if (temp != null) {
					tran.setBadSubAddress(temp);
				}

				temp = rs.getString("BadTrnGftAddress");
				if (temp != null) {
					tran.setBadTrnGftAddress(temp);
				}

				temp = rs.getString("BatchNum");
				if (temp != null) {
					tran.setBatchNum(temp);
				}

				temp = rs.getString("CCBatchProc");
				if (temp != null) {
					tran.setCreditCardBatchProcess(temp);
				}
				temp = rs.getString("Status");
				if (temp != null) {
					tran.setStatus(temp);
				}

				temp = rs.getString("NumPaper");
				if (temp != null) {
					tran.setNumPaper(temp);
				}

				temp = rs.getString("PayAmount");
				if (temp != null) {
					tran.setPayAmount(temp);
				}

				temp = rs.getString("Premium");
				if (temp != null) {
					tran.setPremium(temp);
				}

				temp = rs.getString("PremType");
				if (temp != null) {
					tran.setPremiumType(temp);
				}

				temp = rs.getString("PremAttr");
				if (temp != null) {
					tran.setPremiumAttr(temp);
				}

				temp = rs.getString("PrizmCode");
				if (temp != null) {
					tran.setPrizmCode(temp);
				}

				temp = rs.getString("PrizmGrp");
				if (temp != null) {
					tran.setPrizmGroup(temp);
				}

				temp = rs.getString("PurOrdNum");
				if (temp != null) {
					tran.setPurchaseOrderNum(temp);
				}

				temp = rs.getString("RebillCode");
				if (temp != null) {
					tran.setRebillCode(temp);
				}

				temp = rs.getString("RadPostdDt");
				if (temp != null) {
					tran.setRadPostdDt(temp);
				}

				temp = rs.getString("RadPostdTm");
				if (temp != null) {
					tran.setRadPostdTm(temp);
				}

				temp = rs.getString("Route");
				if (temp != null) {
					tran.setRoute(temp);
				}

				temp = rs.getString("ClubNum");
				if (temp != null) {
					tran.setClubNum(temp);
				}

				temp = rs.getString("ClubType");
				if (temp != null) {
					tran.setClubType(temp);
				}

				temp = rs.getString("SalesRep");
				if (temp != null) {
					tran.setSalesRep(temp);
				}

				temp = rs.getString("SpecialRunCode");
				if (temp != null) {
					tran.setSpecialRunCode(temp);
				}

				temp = rs.getString("SoldBy");
				if (temp != null) {
					tran.setSoldBy(temp);
				}

				temp = rs.getString("SubsType");
				if (temp != null) {
					tran.setSubsType(temp);
				}

				temp = rs.getString("TaxExempt");
				if (temp != null) {
					tran.setTaxExempt(temp);
				}

				temp = rs.getString("Trandate");
				if (temp != null) {
					tran.setTransactionDate(temp);
				}

				temp = rs.getString("TranSeqNum");
				if (temp != null) {
					tran.setTranSeqNum(temp);
				}

				int tempInt = rs.getInt("TransactionState");
				tran.setTransactionState(tempInt);

				temp = rs.getString("TranferType");
				if (temp != null) {
					tran.setTransferType(temp);
				}

				temp = rs.getString("TRFSecRecType");
				if (temp != null) {
					tran.settRFSecRecType(temp);
				}

				temp = rs.getString("CurrAcctNum");
				if (temp != null) {
					tran.setAccountNum(temp);
				}

				temp = rs.getString("NewAddrType");
				if (temp != null) {
					tran.setNewAddrType(temp);
				}

				temp = rs.getString("NewLastName");
				if (temp != null) {
					tran.setNewLastName(temp);
				}
				temp = rs.getString("NewFirstName");
				if (temp != null) {
					tran.setNewFirstName(temp);
				}
				temp = rs.getString("NewFirmName");
				if (temp != null) {
					tran.setNewFirmName(temp);
				}
				temp = rs.getString("NewUnitNum");
				if (temp != null) {
					tran.setNewUnitNum(temp);
				}
				temp = rs.getString("NewHalfUnitNum");
				if (temp != null) {
					tran.setNewHalfUnitNum(temp);
				}
				temp = rs.getString("NewStreetDir");
				if (temp != null) {
					tran.setNewStreetDir(temp);
				}
				temp = rs.getString("NewStreetName");
				if (temp != null) {
					tran.setNewStreetName(temp);
				}
				temp = rs.getString("NewStreetType");
				if (temp != null) {
					tran.setNewStreetType(temp);
				}
				temp = rs.getString("NewStreetPostDir");
				if (temp != null) {
					tran.setNewStreetPostDir(temp);
				}
				temp = rs.getString("NewSubUnitNum");
				if (temp != null) {
					tran.setNewSubUnitNum(temp);
				}
				temp = rs.getString("NewSubUnitCode");
				if (temp != null) {
					tran.setNewSubUnitCode(temp);
				}
				temp = rs.getString("NewCity");
				if (temp != null) {
					tran.setNewCity(temp);
				}

				temp = rs.getString("NewShortCity");
				if (temp != null) {
					tran.setNewShortCity(temp);
				}

				temp = rs.getString("NewState");
				if (temp != null) {
					tran.setNewState(temp);
				}
				temp = rs.getString("NewZip5");
				if (temp != null) {
					tran.setNewZip(temp);
				}
				temp = rs.getString("NewZip4");
				if (temp != null) {
					tran.setNewZip4(temp);
				}
				temp = rs.getString("NewPhoneNum");
				if (temp != null) {
					tran.setNewPhoneNum(temp);
				}
				temp = rs.getString("NewBusPhoneNum");
				if (temp != null) {
					tran.setNewBusPhoneNum(temp);
				}

				temp = rs.getString("NewCarrRteSort");
				if (temp != null) {
					tran.setNewCarrRteSort(temp);
				}

				temp = rs.getString("NewDelPtBarCode");
				if (temp != null) {
					tran.setNewDelPtBarCode(temp);
				}

				temp = rs.getString("NewGatedCommFlag");
				if (temp != null) {
					tran.setNewGatedCommFlag(temp);
				}

				temp = rs.getString("NewGatedCommSecCode");
				if (temp != null) {
					tran.setNewGatedCommSecCode(temp);
				}

				temp = rs.getString("NewAddrNum");
				if (temp != null) {
					tran.setNewAddrNum(temp);
				}

				temp = rs.getString("NewCensusTrack");
				if (temp != null) {
					tran.setNewCensusTrack(temp);
				}

				temp = rs.getString("NewDelMethod");
				if (temp != null) {
					tran.setNewDelMethod(temp);
				}

				temp = rs.getString("NewCensusBlkGrp");
				if (temp != null) {
					tran.setNewCensusBlkGrp(temp);
				}

				temp = rs.getString("NewFIPSCounty");
				if (temp != null) {
					tran.setNewFIPSCounty(temp);
				}

				temp = rs.getString("NewFIPSState");
				if (temp != null) {
					tran.setNewFIPSState(temp);
				}

				temp = rs.getString("NewLatitude");
				if (temp != null) {
					tran.setNewLatitude(temp);
				}

				temp = rs.getString("NewLongitude");
				if (temp != null) {
					tran.setNewLongitude(temp);
				}

				temp = rs.getString("NewMktcode");
				if (temp != null) {
					tran.setNewMktcode(temp);
				}

				temp = rs.getString("NewMktSeg");
				if (temp != null) {
					tran.setNewMktSeg(temp);
				}

				temp = rs.getString("NewPrintsiteCode");
				if (temp != null) {
					tran.setNewPrintsiteCode(temp);
				}

				temp = rs.getString("NewPrizmGrp");
				if (temp != null) {
					tran.setNewPrizmGrp(temp);
				}

				temp = rs.getString("NewPrizmCode");
				if (temp != null) {
					tran.setNewPrizmCode(temp);
				}

				temp = rs.getString("NewSiteCode");
				if (temp != null) {
					tran.setNewSiteCode(temp);
				}

				temp = rs.getString("NewAcctNum");
				if (temp != null) {
					tran.setNewAcctNum(temp);
				}

				temp = rs.getString("DltGiftPayer");
				if (temp != null) {
					tran.setDeleteGiftPayer(temp);
				}
				temp = rs.getString("CreditDay1");
				if (temp != null) {
					tran.setCreditDays1(temp);
				}

				temp = rs.getString("CreditDay2");
				if (temp != null) {
					tran.setCreditDays2(temp);
				}

				temp = rs.getString("CreditDay3");
				if (temp != null) {
					tran.setCreditDays3(temp);
				}

				temp = rs.getString("CreditDay4");
				if (temp != null) {
					tran.setCreditDays4(temp);
				}

				temp = rs.getString("CreditDay5");
				if (temp != null) {
					tran.setCreditDays5(temp);
				}

				temp = rs.getString("CreditDay6");
				if (temp != null) {
					tran.setCreditDays6(temp);
				}
				temp = rs.getString("CreditDay7");
				if (temp != null) {
					tran.setCreditDays7(temp);
				}
				temp = rs.getString("ContactCust");
				if (temp != null) {
					tran.setContactCustomer(temp);
				}
				temp = rs.getString("ExstDelMethod");
				if (temp != null) {
					tran.setExistingDeliveryMethod(temp);
				}

				temp = rs.getString("MicrTranCode");
				if (temp != null) {
					tran.setMicrTranCode(temp);
				}

				temp = rs.getString("VIPLabel");
				if (temp != null) {
					tran.setVIPLabel(temp);
				}

				temp = rs.getString("VicsSrcOrdCode");
				if (temp != null) {
					tran.setVicsSrcOrdCode(temp);
				}

				temp = rs.getString("VicsSubsType");
				if (temp != null) {
					tran.setVicsSubsType(temp);
				}

				temp = rs.getString("VicsFreqDel");
				if (temp != null) {
					tran.setVicsFreqDel(temp);
				}

				temp = rs.getString("VicsNumPaper");
				if (temp != null) {
					tran.setVicsNumPaper(temp);
				}
				temp = rs.getString("VicsRateCode");
				if (temp != null) {
					tran.setVicsRateCode(temp);
				}
				temp = rs.getString("VicsSubsLngth");
				if (temp != null) {
					tran.setVicsSubsLngth(temp);
				}
				temp = rs.getString("VicsSoldBy");
				if (temp != null) {
					tran.setVicsSoldBy(temp);
				}
				temp = rs.getString("VicsPromoCode");
				if (temp != null) {
					tran.setVicsPromoCode(temp);
				}
				temp = rs.getString("VicsContestCode");
				if (temp != null) {
					tran.setVicsContestCode(temp);
				}
				temp = rs.getString("VicsRebilCode");
				if (temp != null) {
					tran.setVicsRebilCode(temp);
				}
				temp = rs.getString("VicsSpecialRunCode");
				if (temp != null) {
					tran.setVicsSpecialRunCode(temp);
				}
				temp = rs.getString("WebID");
				if (temp != null) {
					tran.setWebID(temp);
				}
				temp = rs.getString("Zone");
				if (temp != null) {
					tran.setZone(temp);
				}

				Timestamp ts = rs.getTimestamp("insert_timestamp");
				if (ts != null) {
					tran.setInsertTimestamp(new DateTime(ts.getTime()));
				}
				ts = rs.getTimestamp("update_timestamp");
				if (temp != null) {
					tran.setUpdateTimestamp(new DateTime(ts.getTime()));
				}

				temp = rs.getString("batch_id");
				if (temp != null) {
					tran.setBatchID(temp);
				}

				trans.add(tran);
			}

		} catch (SQLException sqle) {
			throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
		}
		return trans;
	}

	public void insert(ExtranetSubscriberTransactionIntf transaction) throws UsatException {
		java.sql.Connection conn = null;
		try {

			try {
				conn = ConnectionManager.getInstance().getPCIConnection();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to obtain PCI compliant connection...attempting normal connection.");
				conn = ConnectionManager.getInstance().getConnection();
				System.out.println("OBTAINED a NON PCI Connection to esub: ");
			}

			java.sql.PreparedStatement statement = conn.prepareStatement(SubscriberTransactionDAO.INSERT_SUBSCRIPTION_TRX);

			// 1 PubCode, TranRecTyp, LastName, FirstName, FirmName, AddAddr1,
			// 7 AddAddr2, UnitNum, HalfUnitNum, StreetDir, StreetName, StreetTyp,
			// 13 StreetPstDir, SubUnitCode, SubUnitNum, City, State, Zip5,
			// 19 HomePhone, BusPhone, TranType, TranCode, BillLastName, BillFirstName,
			statement.setString(1, transaction.getPubCode());
			statement.setString(2, transaction.getTransactionRecType()); // account number only come from 400 updates
			statement.setString(3, transaction.getLastName());
			statement.setString(4, transaction.getFirstName());
			statement.setString(5, transaction.getFirmName());
			statement.setString(6, transaction.getAddAddr1()); // don't use agency name
			statement.setString(7, transaction.getAddAddr2());
			statement.setString(8, transaction.getUnitNum());
			statement.setString(9, transaction.getHalfUnitNum());
			statement.setString(10, transaction.getStreetDir());
			statement.setString(11, transaction.getStreetName());
			statement.setString(12, transaction.getStreetType());
			statement.setString(13, transaction.getStreetPostDir());
			statement.setString(14, transaction.getSubUnitCode());
			statement.setString(15, transaction.getSubUnitNum());
			statement.setString(16, transaction.getCity());
			statement.setString(17, transaction.getState());
			statement.setString(18, transaction.getZip5());
			statement.setString(19, transaction.getHomePhone());
			statement.setString(20, transaction.getBusinessPhone());
			statement.setString(21, transaction.getTransactionType());
			statement.setString(22, transaction.getTransactionCode());
			statement.setString(23, transaction.getBillingLastName());
			statement.setString(24, transaction.getBillingFirstName());
			// 25 BillFirmName, BillAddr1, BillAddr2, BillUnitNum, BillHalfUnitNum,
			// 30 BillStreetDir, BillStreetName, BillStreetType, BillStreetPostDir,
			// 34 BillSubUnitCode, BillSubUnitNum, BillCity, BillState, BillZip5,
			// 39 BillHomePhone, BillBusPhone, SubsDur, CCType, CCNum, CCExpireDt,
			// 45 EffDate1, PromoCode, Comment, TtlMktCov, SubsAmount, RejComm, AutoRegComm,
			statement.setString(25, transaction.getBillingFirmName());
			statement.setString(26, transaction.getBillingAddress1());
			statement.setString(27, transaction.getBillingAddress2());
			statement.setString(28, transaction.getBillingUnitNum());
			statement.setString(29, transaction.getBillingHalfUnitNum());
			statement.setString(30, transaction.getBillingStreetDir());
			statement.setString(31, transaction.getBillingStreetName());
			statement.setString(32, transaction.getBillingStreetType());
			statement.setString(33, transaction.getBillingStreetPostDir());
			statement.setString(34, transaction.getBillingSubUnitCode());
			statement.setString(35, transaction.getBillingSubUnitNum());
			statement.setString(36, transaction.getBillingCity());
			statement.setString(37, transaction.getBillingState());
			statement.setString(38, transaction.getBillingZip5());
			statement.setString(39, transaction.getBillingHomePhone());
			statement.setString(40, transaction.getBillingBusinessPhone());
			statement.setString(41, transaction.getSubscriptionDuration());
			statement.setString(42, transaction.getCreditCardType());
			statement.setString(43, transaction.getCreditCardNumber());
			statement.setString(44, transaction.getCreditCardExpirationDate());
			statement.setString(45, transaction.getEffectiveDate());
			statement.setString(46, transaction.getPromoCode());
			statement.setString(47, transaction.getComment());
			statement.setString(48, transaction.getTtlMktCov());
			statement.setString(49, transaction.getSubscriptionAmount());
			statement.setString(50, transaction.getRejComment());
			statement.setString(51, transaction.getAutoRegComm());
			// 52 Code1ErrCode, PrintFlag, Filler, SrcOrdCode, ContestCode, DelPref,
			// 58 OneTimeBill, RateCode, PerpCCFlag, BillChargePd, NewAddlAddr1, NewAddlAddr2,
			// 64 CCAuthCode, CCAuthDate, EmailType, EmailAddress, BadSubAddress, BadTrnGftAddress,
			statement.setString(52, transaction.getCode1ErrorCode());
			statement.setString(53, transaction.getPrintFlag());
			statement.setString(54, transaction.getFiller());
			statement.setString(55, transaction.getSrcOrdCode());
			statement.setString(56, transaction.getContestCode());
			statement.setString(57, transaction.getDeliveryPreference());
			statement.setString(58, transaction.getOneTimeBill());
			statement.setString(59, transaction.getRateCode());
			statement.setString(60, transaction.getPerpCCFlag());
			statement.setString(61, transaction.getBillChargePd());
			statement.setString(62, transaction.getNewAddlAddr1());
			statement.setString(63, transaction.getNewAddlAddr2());
			statement.setString(64, transaction.getCreditCardAuthCode());
			statement.setString(65, transaction.getCreditCardAuthDate());
			statement.setString(66, transaction.getEmailType());
			statement.setString(67, transaction.getEmailAddress());
			statement.setString(68, transaction.getBadSubAddress());
			statement.setString(69, transaction.getBadTrnGftAddress());
			// 70 CCBatchProc, Status, NumPaper, PayAmount, Premium, ClubNum,
			// 76 Trandate
			statement.setString(70, transaction.getCreditCardBatchProcess());
			statement.setString(71, transaction.getStatus());
			statement.setString(72, transaction.getNumPaper());
			statement.setString(73, transaction.getPayAmount());
			statement.setString(74, transaction.getPremium());
			statement.setString(75, transaction.getClubNum());
			statement.setString(76, transaction.getTransactionDate());
			statement.setString(77, transaction.getAccountNum());
			statement.setString(78, transaction.getNewLastName());
			statement.setString(79, transaction.getNewFirstName());
			statement.setString(80, transaction.getNewFirmName());
			statement.setString(81, transaction.getNewUnitNum());
			statement.setString(82, transaction.getNewHalfUnitNum());
			statement.setString(83, transaction.getNewStreetDir());
			statement.setString(84, transaction.getNewStreetName());
			statement.setString(85, transaction.getNewStreetType());
			statement.setString(86, transaction.getNewStreetPostDir());
			statement.setString(87, transaction.getNewSubUnitNum());
			statement.setString(88, transaction.getNewSubUnitCode());
			statement.setString(89, transaction.getNewCity());
			statement.setString(90, transaction.getNewState());
			statement.setString(91, transaction.getNewZip());
			statement.setString(92, transaction.getNewPhoneNum());
			statement.setString(93, transaction.getNewBusPhoneNum());
			statement.setString(94, transaction.getDeleteGiftPayer());
			statement.setString(95, transaction.getCreditDays1());
			statement.setString(96, transaction.getCreditDays3());
			statement.setString(97, transaction.getContactCustomer());
			statement.setString(98, transaction.getExistingDeliveryMethod());

			// batch processing columns
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			// insert time stamp
			statement.setTimestamp(99, ts);
			// update time stamp
			statement.setTimestamp(100, ts);
			if (transaction.getMicrTranCode() != null) {
				statement.setString(101, transaction.getMicrTranCode());
			} else {
				statement.setNull(101, Types.CHAR);
			}

			statement.executeUpdate();

		} catch (Exception e) {
			System.out.println("SubscriberTransactionDAO : insert() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}
	}

	private int changeState(int oldState, int newState) throws UsatException {
		int numRowsAffected = 0;
		Connection conn = null;
		try {

			try {
				conn = ConnectionManager.getInstance().getPCIConnection();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to obtain PCI compliant connection...attempting normal connection.");
				conn = ConnectionManager.getInstance().getConnection();
				System.out.println("OBTAINED a NON PCI Connection to esub: ");
			}

			java.sql.PreparedStatement statement = conn.prepareStatement(SubscriberTransactionDAO.UPDATE_TRANSACTION_STATE);

			// update XTRNTDWLD set update_timestamp = ?, transactionstate = ? where transactionstate = ?
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			statement.setTimestamp(1, ts);
			statement.setInt(2, newState);
			statement.setInt(3, oldState);

			numRowsAffected = statement.executeUpdate();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return numRowsAffected;
	}

	public int changeStateToInBatchProcessing() throws UsatException {
		return this.changeState(SubscriberTransactionDAO.NEW, SubscriberTransactionDAO.IN_PROCESSING);
	}

	public int changeStateToProcessed() throws UsatException {
		return this.changeState(SubscriberTransactionDAO.IN_PROCESSING, SubscriberTransactionDAO.PROCESSED);
	}

	public int assignBatchIDToInProcessing(String batchID) throws UsatException {
		int tranCount = 0;
		Connection conn = null;
		try {
			if (batchID == null || batchID.length() == 0) {
				throw new UsatException("Invalid Batch ID: " + batchID);
			}

			try {
				conn = ConnectionManager.getInstance().getPCIConnection();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to obtain PCI compliant connection...attempting normal connection.");
				conn = ConnectionManager.getInstance().getConnection();
				System.out.println("OBTAINED a NON PCI Connection to esub: ");
			}

			PreparedStatement statement = conn.prepareStatement(SubscriberTransactionDAO.UPDATE_BATCH_ID);

			// update XTRNTDWLD set batch_id = ? where transactionstate = " + SubscriberTransactionDAO.IN_PROCESSING
			statement.setString(1, batchID);
			tranCount = statement.executeUpdate();

		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return tranCount;
	}

	public int changeStateToReProcessedForBatch(String batchID) throws UsatException {
		int tranCount = 0;
		Connection conn = null;
		try {
			if (batchID == null || batchID.length() == 0) {
				throw new UsatException("Invalid Batch ID: " + batchID);
			}

			try {
				conn = ConnectionManager.getInstance().getPCIConnection();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to obtain PCI compliant connection...attempting normal connection.");
				conn = ConnectionManager.getInstance().getConnection();
				System.out.println("OBTAINED a NON PCI Connection to esub: ");
			}

			PreparedStatement statement = conn.prepareStatement(SubscriberTransactionDAO.UPDATE_TRANSACTION_STATE_FOR_BATCH);

			// update XTRNTDWLD set update_timestamp = ?, transactionstate = ? where batch_id = ?

			Timestamp ts = new Timestamp(System.currentTimeMillis());
			statement.setTimestamp(1, ts);
			statement.setInt(2, SubscriberTransactionDAO.REPROCESSED);

			statement.setString(3, batchID);
			tranCount = statement.executeUpdate();

		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return tranCount;
	}

	public int getCountOfTransactionsInState(int state) throws UsatException {
		int tranCount = 0;
		Connection conn = null;
		try {
			try {
				conn = ConnectionManager.getInstance().getPCIConnection();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to obtain PCI compliant connection...attempting normal connection.");
				conn = ConnectionManager.getInstance().getConnection();
				System.out.println("OBTAINED a NON PCI Connection to esub: ");
			}

			PreparedStatement ps = conn.prepareStatement(SubscriberTransactionDAO.SELECT_COUNT_OF_TRANS_IN_STATE);
			ps.setInt(1, state);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				tranCount = rs.getInt(1);
			}
		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return tranCount;
	}

	public int deleteProcessedTransactionsOlderThanSpecifiedDays(int numDays) throws UsatException {
		int rowsAffected = 0;
		Connection conn = null;
		try {

			try {
				conn = ConnectionManager.getInstance().getPCIConnection();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to obtain PCI compliant connection...attempting normal connection.");
				conn = ConnectionManager.getInstance().getConnection();
				System.out.println("OBTAINED a NON PCI Connection to esub: ");
			}

			java.sql.PreparedStatement statement = conn.prepareStatement(SubscriberTransactionDAO.DELETE_OLD_RECORDS);

			// delete from XTRNTDWNLD where transactionstate >= " + SubscriberTransactionDAO.PROCESSED + " and update_timestamp <= ?
			DateTime purgeDate = new DateTime();

			purgeDate = purgeDate.minusMillis(purgeDate.getMillisOfDay());

			purgeDate = purgeDate.minusDays(numDays);

			Timestamp ts = new Timestamp(purgeDate.getMillis());

			statement.setTimestamp(1, ts);

			rowsAffected = statement.executeUpdate();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return rowsAffected;
	}

	private Collection<ExtranetSubscriberTransactionIntf> fetchTransactionsInState(int state) throws UsatException {
		Collection<ExtranetSubscriberTransactionIntf> records = null;
		Connection conn = null;
		try {

			try {
				conn = ConnectionManager.getInstance().getPCIConnection();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to obtain PCI compliant connection...attempting normal connection.");
				conn = ConnectionManager.getInstance().getConnection();
				System.out.println("OBTAINED a NON PCI Connection to esub: ");
			}

			java.sql.PreparedStatement statement = conn.prepareStatement(SubscriberTransactionDAO.SELECT_TRANS_IN_STATE);

			statement.setInt(1, state);

			ResultSet rs = statement.executeQuery();

			records = this.objectFactory(rs);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return records;
	}

	public Collection<ExtranetSubscriberTransactionIntf> fetchTransactionsInProcessing() throws UsatException {
		return this.fetchTransactionsInState(SubscriberTransactionDAO.IN_PROCESSING);
	}

	public Collection<ExtranetSubscriberTransactionIntf> fetchTransactionsInBatch(String batchID) throws UsatException {
		Collection<ExtranetSubscriberTransactionIntf> records = null;
		Connection conn = null;
		try {
			if (batchID == null || batchID.length() == 0) {
				throw new UsatException("Invalid Batch ID: " + batchID);
			}

			try {
				conn = ConnectionManager.getInstance().getPCIConnection();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to obtain PCI compliant connection...attempting normal connection.");
				conn = ConnectionManager.getInstance().getConnection();
				System.out.println("OBTAINED a NON PCI Connection to esub: ");
			}

			java.sql.PreparedStatement statement = conn.prepareStatement(SubscriberTransactionDAO.SELECT_BY_BATCHID);

			statement.setString(1, batchID);

			ResultSet rs = statement.executeQuery();

			records = this.objectFactory(rs);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return records;
	}

	public String getLastBatchIDFromToday() throws UsatException {

		String batchID = null;
		Connection conn = null;
		try {

			try {
				conn = ConnectionManager.getInstance().getPCIConnection();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to obtain PCI compliant connection...attempting normal connection.");
				conn = ConnectionManager.getInstance().getConnection();
				System.out.println("OBTAINED a NON PCI Connection to esub: ");
			}

			java.sql.PreparedStatement statement = conn.prepareStatement(SubscriberTransactionDAO.SELECT_LAST_BATCHID);

			// select distinct batch_id from XTRNTDWNLD where batch_id like ? order by batch_id desc
			DateTime dt = new DateTime();
			String likeParm = dt.toString("yyyyMMdd") + "_%";

			statement.setString(1, likeParm);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				batchID = rs.getString("batch_id");
				if (batchID != null) {
					batchID = batchID.trim();
				}
			}

			if (UsaTodayConstants.debug) {
				if (batchID == null) {
					System.out.println("No Previous Batches for from clause: " + likeParm);
				} else {
					System.out.println("Last Batch ID from today: " + batchID);
				}
			}

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return batchID;
	}
}
