/*
 * Created on May 5, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

/**
 * @author aeast
 * @date May 5, 2006
 * @class PromotionDAO
 * 
 *        This class interfaces with the XTRNTPROMO table.
 * 
 */
public class PromotionDAO extends USATodayDAO {

	private static final String SELECT_ALL_PROMOS_FOR_PUB_KEYCODE = "select PubCode, KeyCode, ImageType, ImageName, FulfillText, FulfillURL, AltName from XTRNTPROMO where PubCode = ? AND KeyCode = ?";

	// private static final String SELECT_COUNT_OFALL_PROMOS_FOR_PUB_KEYCODE_IMAGETYPE =
	// "select count(*) from XTRNTPROMO where PubCode = ? AND (KeyCode = ? or KeyCode = 'ALL') AND ImageType = ?";

	/**
	 * 
	 * @param rs
	 * @param requestingUser
	 * @return
	 * @throws UsatException
	 */
	protected Collection<PromotionIntf> objectFactory(ResultSet rs) throws UsatException {
		ArrayList<PromotionIntf> al = new ArrayList<PromotionIntf>();

		try {
			while (rs.next()) {

				PromotionTO xp = new PromotionTO();

				String promoType = rs.getString("ImageType");

				xp.setType(promoType);

				String temp = rs.getString("AltName");
				if (temp != null) {
					xp.setAltName(temp);
				}

				temp = rs.getString("FulfillText");
				if (temp != null) {
					xp.setFulfillText(temp);
				}

				temp = rs.getString("FulfillURL");
				if (temp != null) {
					xp.setFulfillUrl(temp);
				}

				temp = rs.getString("KeyCode");
				if (temp != null) {
					xp.setKeyCode(temp);
				}

				temp = rs.getString("PubCode");
				if (temp != null) {
					xp.setPubCode(temp);
				}

				temp = rs.getString("ImageName");
				if (temp != null) {
					xp.setName(temp);
				}

				al.add(xp);
			}
		} catch (SQLException sqle) {
			throw new UsatException("PromotionDAO:objectFactory() :" + sqle.getMessage() + "     " + sqle.toString());
		}
		return al;
	}

	/**
	 * 
	 * @param pubcode
	 * @param keycode
	 * @return
	 * @throws UsatException
	 */
	public Collection<PromotionIntf> fetchPromosByPubcodeKeycode(String pubcode, String keycode) throws UsatException {

		Collection<PromotionIntf> promos = null;
		Connection conn = null;
		try {
			conn = ConnectionManager.getInstance().getConnection();
			PreparedStatement ps = conn.prepareStatement(PromotionDAO.SELECT_ALL_PROMOS_FOR_PUB_KEYCODE);

			ps.setString(1, pubcode);
			ps.setString(2, keycode);

			ResultSet rs = ps.executeQuery();

			promos = this.objectFactory(rs);

		} catch (UsatException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

		return promos;
	}

	/**
	 * 
	 * @param pubcode
	 * @param keycode
	 * @param type
	 * @return
	 * @throws UsatException
	 */
	public int fetchCountOfPromosByPubcodeKeycodeType(String pubcode, String keycode, String type) throws UsatException {
		int count = 0;

		Connection conn = null;
		try {
			conn = ConnectionManager.getInstance().getConnection();
			PreparedStatement ps = conn.prepareStatement(PromotionDAO.SELECT_ALL_PROMOS_FOR_PUB_KEYCODE);

			// where PubCode = ? AND KeyCode = ?
			ps.setString(1, pubcode);
			ps.setString(2, keycode);
			ps.setString(3, type);

			ResultSet rs = ps.executeQuery();

			rs.next();
			count = rs.getInt(1);
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

		return count;
	}

}
