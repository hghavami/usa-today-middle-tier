package com.usatoday.integration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import org.joda.time.DateMidnight;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.CustomerInterimInfoIntf;

public class CustomerInterimInfoDAO extends USATodayDAO {

	// Insert a record into the XTRNTINTCSTINF table
	public void insert(CustomerInterimInfoIntf customerInterimInfoIntf) throws UsatException {
		int rowsAffected = 0;

		if (customerInterimInfoIntf == null) {
			throw new UsatException("Null USAT Tech Guide Order object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();

			StringBuffer sql = new StringBuffer(
					"insert into XTRNTINTCSTINF (PubCode, Email, FirstName, LastName, FirmName, Address1, Address2, City, State, Zip, HomePhone, WorkPhone, BillFirstName, BillLastName, BillFirmName, BillAddress1, BillAddress2, BillCity, BillState, BillZip, BillPhone, RateCode, PromoCode, ContestCode, SrcOrdCode, SubsAmount, SubsDur, StartDate, DelvMethod, Insert_timestamp) values (");
			// Fill in the SQL statement

			sql.append(prepareSqlInsert(customerInterimInfoIntf));

			rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Customer Interim Information Object Inserted Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			ResultSet rs = statement.getGeneratedKeys();

			rs.next();
			int primaryKey = rs.getInt(1);

			customerInterimInfoIntf.setOrderID(primaryKey);

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException("XTRNTINTCSTINF: No Records Inserted and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

	// Update a record into the XTRNTINTCSTINF table
	public void update(CustomerInterimInfoIntf customerInterimInfoIntf) throws UsatException {
		int rowsAffected = 0;

		if (customerInterimInfoIntf == null) {
			throw new UsatException("Null USAT Tech Guide Order object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();

			StringBuffer sql = new StringBuffer("update XTRNTINTCSTINF set ");
			// Fill in the SQL statement

			sql.append(prepareSqlUpdate(customerInterimInfoIntf));
			sql.append(" where ID = '").append(customerInterimInfoIntf.getOrderID()).append("'");

			rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Customer Interim Information Object Updated Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException("XTRNTINTCSTINF: No Records Updated and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

	// Delete a record into the XTRNTINTCSTINF table
	public void delete(CustomerInterimInfoIntf customerInterimInfoIntf) throws UsatException {
		int rowsAffected = 0;

		if (customerInterimInfoIntf == null) {
			throw new UsatException("Null CustomerInterimInfo object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();

			StringBuffer sql = new StringBuffer("delete from XTRNTINTCSTINF where ID = " + customerInterimInfoIntf.getOrderID());
			// Fill in the SQL statement

			rowsAffected = statement.executeUpdate(sql.toString());

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Customer Interim Information Object Deleted Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException("XTRNTINTCSTINF: No Records Deleted and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			// e.printStackTrace();
			// System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			// e.printStackTrace();
			// System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

	// Prepare SQL for insert into XTRNTINTCSLINF table
	private StringBuffer prepareSqlInsert(CustomerInterimInfoIntf customerInterimInfoIntf) {

		StringBuffer sql = new StringBuffer();
		if (customerInterimInfoIntf.getPublication() != null) {
			sql.append("'").append(customerInterimInfoIntf.getPublication()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getEmail() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getEmail())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getFirstName() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getFirstName())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getLastName() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getLastName())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getFirmName() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getFirmName())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getAddress1() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getAddress1())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getAddress2() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getAddress2())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getCity() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getCity())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getState() != null) {
			sql.append("'").append(customerInterimInfoIntf.getState()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getZip() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getZip())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getHomePhone() != null) {
			sql.append("'").append(customerInterimInfoIntf.getHomePhone()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getWorkPhone() != null) {
			sql.append("'").append(customerInterimInfoIntf.getWorkPhone()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getBillFirstName() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillFirstName())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getBillLastName() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillLastName())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getBillFirmName() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillFirmName())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getBillAddress1() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillAddress1())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getBillAddress2() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillAddress2())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getBillCity() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillCity())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getBillState() != null) {
			sql.append("'").append(customerInterimInfoIntf.getBillState()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getBillZip() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillZip())).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getBillPhone() != null) {
			sql.append("'").append(customerInterimInfoIntf.getBillPhone()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getRateCode() != null) {
			sql.append("'").append(customerInterimInfoIntf.getRateCode()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getPromoCode() != null) {
			sql.append("'").append(customerInterimInfoIntf.getPromoCode()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getContestCode() != null) {
			sql.append("'").append(customerInterimInfoIntf.getContestCode()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getSrcOrdCode() != null) {
			sql.append("'").append(customerInterimInfoIntf.getSrcOrdCode()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getSubsAmount() != null) {
			sql.append("'").append(customerInterimInfoIntf.getSubsAmount()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getSubsDur() != null) {
			sql.append("'").append(customerInterimInfoIntf.getSubsDur()).append("',");
		} else {
			sql.append("null,");
		}
		if (customerInterimInfoIntf.getStartDate() != null) {
			sql.append("'").append(customerInterimInfoIntf.getStartDate()).append("',");
		} else {
			sql.append("null,");
		}

		if (customerInterimInfoIntf.getDelvMethod() != null) {
			sql.append("'").append(customerInterimInfoIntf.getDelvMethod()).append("',");
		} else {
			sql.append("null,");
		}

		Timestamp ts = new Timestamp(System.currentTimeMillis());
		if (ts != null) {
			sql.append("'").append(ts).append("')");
		}

		return sql;
	}

	// Prepare SQL for update into XTRNTINTCSLINF table
	private StringBuffer prepareSqlUpdate(CustomerInterimInfoIntf customerInterimInfoIntf) {

		StringBuffer sql = new StringBuffer();
		if (customerInterimInfoIntf.getPublication() != null) {
			sql.append("PubCode = '").append(customerInterimInfoIntf.getPublication()).append("',");
		} else {
			sql.append("PubCode = null,");
		}
		if (customerInterimInfoIntf.getEmail() != null) {
			sql.append(" Email = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getEmail())).append("',");
		} else {
			sql.append(" Email = null,");
		}
		if (customerInterimInfoIntf.getFirstName() != null) {
			sql.append(" FirstName = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getFirstName())).append("',");
		} else {
			sql.append(" FirstName = null,");
		}
		if (customerInterimInfoIntf.getLastName() != null) {
			sql.append(" LastName = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getLastName())).append("',");
		} else {
			sql.append(" LastName = null,");
		}
		if (customerInterimInfoIntf.getFirmName() != null) {
			sql.append(" FirmName = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getFirmName())).append("',");
		} else {
			sql.append(" FirmName = null,");
		}
		if (customerInterimInfoIntf.getAddress1() != null) {
			sql.append(" Address1 = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getAddress1())).append("',");
		} else {
			sql.append(" Address1 = null,");
		}
		if (customerInterimInfoIntf.getAddress2() != null) {
			sql.append(" Address2 = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getAddress2())).append("',");
		} else {
			sql.append(" Address2 = null,");
		}
		if (customerInterimInfoIntf.getCity() != null) {
			sql.append(" City = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getCity())).append("',");
		} else {
			sql.append(" City = null,");
		}
		if (customerInterimInfoIntf.getState() != null) {
			sql.append(" State = '").append(customerInterimInfoIntf.getState()).append("',");
		} else {
			sql.append(" State = null,");
		}
		if (customerInterimInfoIntf.getZip() != null) {
			sql.append(" Zip = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getZip())).append("',");
		} else {
			sql.append(" Zip = null,");
		}
		if (customerInterimInfoIntf.getHomePhone() != null) {
			sql.append(" HomePhone = '").append(customerInterimInfoIntf.getHomePhone()).append("',");
		} else {
			sql.append(" HomePhone = null,");
		}
		if (customerInterimInfoIntf.getWorkPhone() != null) {
			sql.append(" WorkPhone = '").append(customerInterimInfoIntf.getWorkPhone()).append("',");
		} else {
			sql.append(" workPhone = null,");
		}
		if (customerInterimInfoIntf.getBillFirstName() != null) {
			sql.append("  BillFirstName = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillFirstName()))
					.append("',");
		} else {
			sql.append("  BillFirstName = null,");
		}
		if (customerInterimInfoIntf.getBillLastName() != null) {
			sql.append("  BillLastName = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillLastName()))
					.append("',");
		} else {
			sql.append("  BillLastName = null,");
		}
		if (customerInterimInfoIntf.getBillFirmName() != null) {
			sql.append(" BillFirmName = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillFirmName()))
					.append("',");
		} else {
			sql.append(" BillFirmName = null,");
		}
		if (customerInterimInfoIntf.getBillAddress1() != null) {
			sql.append(" BillAddress1 = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillAddress1()))
					.append("',");
		} else {
			sql.append(" BillAddress1 = null,");
		}
		if (customerInterimInfoIntf.getBillAddress2() != null) {
			sql.append(" BillAddress2 = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillAddress2()))
					.append("',");
		} else {
			sql.append(" BillAddress2 = null,");
		}
		if (customerInterimInfoIntf.getBillCity() != null) {
			sql.append(" BillCity = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillCity())).append("',");
		} else {
			sql.append(" BillCity = null,");
		}
		if (customerInterimInfoIntf.getBillState() != null) {
			sql.append(" BillState = '").append(customerInterimInfoIntf.getBillState()).append("',");
		} else {
			sql.append(" BillState = null,");
		}
		if (customerInterimInfoIntf.getBillZip() != null) {
			sql.append(" BillZip = '").append(USATodayDAO.escapeApostrophes(customerInterimInfoIntf.getBillZip())).append("',");
		} else {
			sql.append(" BillZip = null,");
		}
		if (customerInterimInfoIntf.getBillPhone() != null) {
			sql.append(" BillPhone = '").append(customerInterimInfoIntf.getBillPhone()).append("',");
		} else {
			sql.append(" BillPhone = null,");
		}
		if (customerInterimInfoIntf.getRateCode() != null) {
			sql.append(" RateCode = '").append(customerInterimInfoIntf.getRateCode()).append("',");
		} else {
			sql.append(" RateCode = null,");
		}
		if (customerInterimInfoIntf.getPromoCode() != null) {
			sql.append(" PromoCode = '").append(customerInterimInfoIntf.getPromoCode()).append("',");
		} else {
			sql.append(" PromoCode = null,");
		}
		if (customerInterimInfoIntf.getContestCode() != null) {
			sql.append(" ContestCode = '").append(customerInterimInfoIntf.getContestCode()).append("',");
		} else {
			sql.append(" ContestCode = null,");
		}
		if (customerInterimInfoIntf.getSrcOrdCode() != null) {
			sql.append(" SrcOrdCode = '").append(customerInterimInfoIntf.getSrcOrdCode()).append("',");
		} else {
			sql.append(" SrcOrdCode = null,");
		}
		if (customerInterimInfoIntf.getSubsAmount() != null) {
			sql.append(" SubsAmount = '").append(customerInterimInfoIntf.getSubsAmount()).append("',");
		} else {
			sql.append(" SubsAmount = null,");
		}
		if (customerInterimInfoIntf.getSubsDur() != null) {
			sql.append(" SubsDur = '").append(customerInterimInfoIntf.getSubsDur()).append("',");
		} else {
			sql.append(" SubsDur = null,");
		}
		if (customerInterimInfoIntf.getStartDate() != null) {
			sql.append(" StartDate = '").append(customerInterimInfoIntf.getStartDate()).append("',");
		} else {
			sql.append(" StartDate = null,");
		}

		if (customerInterimInfoIntf.getStartDate() != null) {
			sql.append(" DelvMethod = '").append(customerInterimInfoIntf.getDelvMethod()).append("',");
		} else {
			sql.append(" DelvMethod = null,");
		}

		Timestamp ts = new Timestamp(System.currentTimeMillis());
		if (ts != null) {
			sql.append(" Insert_timestamp = '").append(ts).append("'");
		}

		return sql;
	}

	// Update a record into the XTRNTINTCSTINF table
	public void selectForFTP(CustomerInterimInfoIntf customerInterimInfoIntf) throws UsatException {
		int rowsAffected = 0;

		if (customerInterimInfoIntf == null) {
			throw new UsatException("Null USAT Tech Guide Order object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();
			// Set time stamp to compare with to today's with time of 12 am
			DateMidnight dm = new DateMidnight();

			Timestamp ts = new Timestamp(dm.getMillis());

			// Fill in the SQL statement
			StringBuffer sql = new StringBuffer("select * from XTRNTINTCSTINF where ");
			sql.append(" where ProcessState = 0 and Insert_timestamp < ").append(ts);

			rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Customer Interim Information Object Updated Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException("XTRNTINTCSTINF: No Records Updated and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

	/**
	 * 
	 * @param oldState
	 * @param newState
	 * @return number or rows affected
	 * @throws UsatException
	 */
	private int changeState(int oldState, int newState) throws UsatException {
		int numRowsAffected = 0;
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(CustomerInterimInfoDAO.UPDATE_INTERIM_BATCH_STATE);

			// "update XTRNTINTCSTINF set State=?, UpdateTimestamp=? where State = ?"
			statement.setInt(1, newState);
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			statement.setTimestamp(2, ts);
			statement.setInt(3, oldState);

			numRowsAffected = statement.executeUpdate();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return numRowsAffected;
	}

	/**
	 * 
	 * @return Number of records affected
	 * @throws UsatException
	 */
	public int changeStateToInBatchProcessing() throws UsatException {
		return this.changeState(CustomerInterimInfoIntf.READY_FOR_BATCH, CustomerInterimInfoIntf.BATCH_IN_PROGRESS);
	}

	/**
	 * 
	 * @return Number of records affected
	 * @throws UsatException
	 */
	public int changeStateToReadyForDelete() throws UsatException {
		return this.changeState(CustomerInterimInfoIntf.BATCH_IN_PROGRESS, CustomerInterimInfoIntf.READY_FOR_DELETION);
	}

	/**
	 * 
	 * @param numDays
	 * @return
	 * @throws UsatException
	 */
	public int deleteAttributesOlderThanSpecifiedDays(int numDays) throws UsatException {
		int rowsAffected = 0;
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn
					.prepareStatement(CustomerInterimInfoDAO.DELETE_INTERIM_ARRTIBUTES_IN_DELETE_STATE_OLDER_THAN_DATE);

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.add(Calendar.DAY_OF_YEAR, (-1 * numDays));

			Timestamp ts = new Timestamp(cal.getTimeInMillis());

			statement.setTimestamp(1, ts);

			rowsAffected = statement.executeUpdate();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return rowsAffected;
	}

	private Collection<CustomerInterimInfoIntf> fetchAttributesInState(int processState) throws UsatException {
		Collection<CustomerInterimInfoIntf> records = null;
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(CustomerInterimInfoDAO.FETCH_INTERIM_ATTRIBUTES_IN_STATE);

			statement.setInt(1, processState);

			ResultSet rs = statement.executeQuery();

			records = CustomerInterimInfoDAO.objectFactory(rs);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return records;
	}

	public Collection<CustomerInterimInfoIntf> fetchAttributesRecordsForBatchProcessing() throws UsatException {
		return this.fetchAttributesInState(CustomerInterimInfoIntf.BATCH_IN_PROGRESS);
	}

	// private static final String DELETE_INTERIM_ARRTIBUTE = "delete from XTRNTINTCSTINF where ID = ?";
	private static final String DELETE_INTERIM_ARRTIBUTES_IN_DELETE_STATE_OLDER_THAN_DATE = "delete from XTRNTINTCSTINF where ProcessState = 2 and insert_Timestamp <= ?";
	// private static final String FETCH_INTERIM_ATTRIBUTES_FOR_ORDER = "select * from XTRNTINTCSTINF where ID = ?";
	private static final String FETCH_INTERIM_ATTRIBUTES_IN_STATE = "select * from XTRNTINTCSTINF where ProcessState = ?";
	private static final String UPDATE_INTERIM_BATCH_STATE = "update XTRNTINTCSTINF set ProcessState=?, insert_Timestamp=? where ProcessState = ?";

	private synchronized static Collection<CustomerInterimInfoIntf> objectFactory(ResultSet rs) throws UsatException {
		Collection<CustomerInterimInfoIntf> records = new ArrayList<CustomerInterimInfoIntf>();

		try {
			// iterate over result set and build objects
			while (rs.next()) {

				long primaryKey = rs.getLong("ID");
				int processState = rs.getInt("ProcessState");

				String tempStr = null;

				CustomerInterimInfoTO attribute = new CustomerInterimInfoTO();

				attribute.setOrderID(primaryKey);
				attribute.setProcessState(processState);

				tempStr = rs.getString("Email");
				if (tempStr != null) {
					attribute.setEmail(tempStr.trim());
				}

				tempStr = rs.getString("PubCode");
				if (tempStr != null) {
					attribute.setPublication(tempStr.trim());
				}

				tempStr = rs.getString("FirstName");
				if (tempStr != null) {
					attribute.setFirstName(tempStr.trim());
				}

				tempStr = rs.getString("LastName");
				if (tempStr != null) {
					attribute.setLastName(tempStr.trim());
				}

				tempStr = rs.getString("FirmName");
				if (tempStr != null) {
					attribute.setFirmName(tempStr.trim());
				}

				tempStr = rs.getString("Address1");
				if (tempStr != null) {
					attribute.setAddress1(tempStr.trim());
				}

				tempStr = rs.getString("Address2");
				if (tempStr != null) {
					attribute.setAddress2(tempStr.trim());
				}

				tempStr = rs.getString("City");
				if (tempStr != null) {
					attribute.setCity(tempStr.trim());
				}

				tempStr = rs.getString("State");
				if (tempStr != null) {
					attribute.setState(tempStr.trim());
				}

				tempStr = rs.getString("Zip");
				if (tempStr != null) {
					attribute.setZip(tempStr.trim());
				}

				tempStr = rs.getString("HomePhone");
				if (tempStr != null) {
					attribute.setHomePhone(tempStr.trim());
				}

				tempStr = rs.getString("WorkPhone");
				if (tempStr != null) {
					attribute.setWorkPhone(tempStr.trim());
				}

				tempStr = rs.getString("BillFirstName");
				if (tempStr != null) {
					attribute.setBillFirstName(tempStr.trim());
				}

				tempStr = rs.getString("BillLastName");
				if (tempStr != null) {
					attribute.setBillLastName(tempStr.trim());
				}

				tempStr = rs.getString("BillFirmName");
				if (tempStr != null) {
					attribute.setBillFirmName(tempStr.trim());
				}

				tempStr = rs.getString("BillFirmName");
				if (tempStr != null) {
					attribute.setBillFirmName(tempStr.trim());
				}

				tempStr = rs.getString("BillAddress1");
				if (tempStr != null) {
					attribute.setBillAddress1(tempStr.trim());
				}

				tempStr = rs.getString("BillAddress2");
				if (tempStr != null) {
					attribute.setBillAddress2(tempStr.trim());
				}

				tempStr = rs.getString("BillCity");
				if (tempStr != null) {
					attribute.setBillCity(tempStr.trim());
				}

				tempStr = rs.getString("BillState");
				if (tempStr != null) {
					attribute.setBillState(tempStr.trim());
				}

				tempStr = rs.getString("BillZip");
				if (tempStr != null) {
					attribute.setBillZip(tempStr.trim());
				}

				tempStr = rs.getString("BillPhone");
				if (tempStr != null) {
					attribute.setBillPhone(tempStr.trim());
				}

				tempStr = rs.getString("RateCode");
				if (tempStr != null) {
					attribute.setRateCode(tempStr.trim());
				}

				tempStr = rs.getString("PromoCode");
				if (tempStr != null) {
					attribute.setPromoCode(tempStr.trim());
				}

				tempStr = rs.getString("ContestCode");
				if (tempStr != null) {
					attribute.setContestCode(tempStr.trim());
				}

				tempStr = rs.getString("SrcOrdCode");
				if (tempStr != null) {
					attribute.setSrcOrdCode(tempStr.trim());
				}

				tempStr = rs.getString("SubsAmount");
				if (tempStr != null) {
					attribute.setSubsAmount(tempStr.trim());
				}

				tempStr = rs.getString("SubsDur");
				if (tempStr != null) {
					attribute.setSubsDur(tempStr.trim());
				}

				tempStr = rs.getString("StartDate");
				if (tempStr != null) {
					attribute.setStartDate(tempStr.trim());
				}

				Timestamp ts = rs.getTimestamp("Insert_timestamp");
				if (ts != null) {
					attribute.setInsertedTimeStamp(ts);
				}

				records.add(attribute);
			}
		} catch (Exception e) {
			System.out.println("CustomerInterimInfoDAO::objectFactory() - Failed to create Customer Interim Info Record TO : "
					+ e.getMessage());
		}

		return records;
	}

	/**
	 * 
	 * @return Number of records affected
	 * @throws UsatException
	 */
	public int changeStateToBatchError() throws UsatException {
		return this.changeState(CustomerInterimInfoIntf.BATCH_IN_PROGRESS, CustomerInterimInfoIntf.BATCH_ERROR);
	}
}
