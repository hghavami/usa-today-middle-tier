/*
 * Created on Sep 23, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.integration;

import java.io.Serializable;
import java.sql.Timestamp;

import com.usatoday.business.interfaces.customer.ExpiredSubscriptionRenewedAccessIntf;

/**
 * @author aeast
 * 
 * 
 */
public class ExpiredSubscriptionRenewedAccessTO implements ExpiredSubscriptionRenewedAccessIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8316565004199062225L;
	private String publication = null;
	private String email = null;
	private String firstName = null;
	private String lastName = null;
	private String firmName = null;
	private String accountNum = null;
	private long orderID = -1;
	private int processState = 0;
	private Timestamp insertedTimeStamp = null;

	// flag to indicate if this object is saved

	/**
     *  
     */
	public ExpiredSubscriptionRenewedAccessTO() {
		super();
	}

	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return Returns the firmName.
	 */
	public String getFirmName() {
		return firmName;
	}

	/**
	 * @param firmName
	 *            The firmName to set.
	 */
	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the insertedTimeStamp.
	 */
	public Timestamp getInsertedTimeStamp() {
		return insertedTimeStamp;
	}

	/**
	 * @param insertedTimeStamp
	 *            The insertedTimeStamp to set.
	 */
	public void setInsertedTimeStamp(Timestamp insertedTimeStamp) {
		this.insertedTimeStamp = insertedTimeStamp;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the publication.
	 */
	public String getPublication() {
		return publication;
	}

	/**
	 * @param publication
	 *            The publication to set.
	 */
	public void setPublication(String publication) {
		this.publication = publication;
	}

	/**
	 * @return Returns the startDate.
	 */
	public String getAccountNum() {
		return accountNum;
	}

	/**
	 * @param startDate
	 *            The startDate to set.
	 */
	public void setAccountNum(String startDate) {
		this.accountNum = startDate;
	}

	/**
	 * @return Returns the orderID.
	 */
	public long getOrderID() {
		return orderID;
	}

	/**
	 * @param orderID
	 *            The orderID to set.
	 */
	public void setOrderID(long orderID) {
		this.orderID = orderID;
	}

	/**
	 * @return Returns the processState.
	 */
	public int getProcessState() {
		return processState;
	}

	/**
	 * @param processState
	 *            The processState to set.
	 */
	public void setProcessState(int processState) {
		this.processState = processState;
	}
}
