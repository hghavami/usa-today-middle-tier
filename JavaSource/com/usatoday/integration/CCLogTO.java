/*
 * Created on Apr 24, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;

import com.usatoday.business.interfaces.shopping.CCLogIntf;

/**
 * @author aeast
 * @date Apr 24, 2006
 * @class CCLogTO
 * 
 *        Represents an instance of a Credit Card Log.
 * 
 */
public class CCLogTO implements CCLogIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4912290931461561711L;
	private String email = "";
	private String CCNum = "";
	private String expMonth = "";
	private String expYear = "";
	private String authCode = "";
	private String firstName = "";
	private String lastName = "";
	private String responseCode = "";
	private String zip = "";
	private String amount = "";
	private String pubCode = "";
	private String responseMessage = "";
	private String ccType = "";
	private String vendorTranID = "";
	private String avsRespCode = "";
	private String avsRespMessage = "";
	private String orderID = "";
	private String clientIP = "";
	private String cvvResponseCode = "";
	private String cvvResponseMessage = "";
	private String reasonCode = "";
	private String externalKey = "";
	private String requestType = "";

	public CCLogTO() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getAmount()
	 */
	public String getAmount() {
		return amount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getCCNum()
	 */
	public String getCCNum() {
		return CCNum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getCcType()
	 */
	public String getCcType() {
		return ccType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getEmail()
	 */
	public String getEmail() {
		return email;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getExpMonth()
	 */
	public String getExpMonth() {
		return expMonth;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getExpYear()
	 */
	public String getExpYear() {
		return expYear;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getFirstName()
	 */
	public String getFirstName() {
		return firstName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getLastName()
	 */
	public String getLastName() {
		return lastName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getPubCode()
	 */
	public String getPubCode() {
		return pubCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getResponseCode()
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getResponseMessage()
	 */
	public String getResponseMessage() {
		return this.responseMessage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CCLogIntf#getZip()
	 */
	public String getZip() {
		return zip;
	}

	public String getAuthCode() {
		return authCode;
	}

	public String getAvsRespCode() {
		return avsRespCode;
	}

	public String getAvsRespMessage() {
		return avsRespMessage;
	}

	public String getVendorTranID() {
		return vendorTranID;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setAmount(String amount) {
		if (amount == null) {
			amount = "???";
		}

		String amt = amount.replaceAll(",", "");

		if (amt.trim().length() > 8) {
			amt = "too big";
		}

		this.amount = amt.trim();
	}

	public void setAuthCode(String authCode) {
		if (authCode == null) {
			authCode = "null";
		}
		authCode = authCode.trim();
		if (authCode.length() > 20) {
			authCode = authCode.substring(0, 19);
		}
		this.authCode = authCode;
	}

	public void setAvsRespCode(String avsRespCode) {
		if (avsRespCode == null) {
			avsRespCode = "null";
		}
		avsRespCode = avsRespCode.trim();
		if (avsRespCode.length() > 15) {
			avsRespCode = avsRespCode.substring(0, 14);
		}
		this.avsRespCode = avsRespCode;
	}

	public void setAvsRespMessage(String avsRespMessage) {
		if (avsRespMessage == null) {
			avsRespMessage = "null";
		}
		avsRespMessage = avsRespMessage.trim();
		if (avsRespMessage.length() > 128) {
			avsRespMessage = avsRespMessage.substring(0, 127);
		}
		this.avsRespMessage = avsRespMessage;
	}

	public void setCCNum(String num) {
		if (num == null) {
			num = "9999";
		}
		num = num.trim();
		if (num.length() > 4) {
			try {
				num = num.substring((num.length() - 4));
			} catch (IndexOutOfBoundsException e) {
				num = "9999";
			}
		}
		this.CCNum = num;
	}

	public void setCcType(String ccType) {
		if (ccType == null) {
			ccType = "?";
		}
		ccType = ccType.trim();
		if (ccType.length() > 1) {
			ccType = ccType.substring(0, 1);
		}
		this.ccType = ccType;
	}

	public void setEmail(String email) {
		if (email == null) {
			email = "null";
		}
		email = email.trim();
		if (email.length() > 60) {
			email = email.substring(0, 60);
		}
		this.email = email;
	}

	public void setExpMonth(String expMonth) {
		if (expMonth == null) {
			expMonth = "??";
		}
		expMonth = expMonth.trim();
		if (expMonth.length() > 2) {
			expMonth = expMonth.substring(0, 2);
		}
		this.expMonth = expMonth;
	}

	public void setExpYear(String expYear) {
		if (expYear == null) {
			expYear = "null";
		}
		expYear = expYear.trim();
		if (expYear.length() > 4) {
			expYear = expYear.substring(0, 4);
		}
		this.expYear = expYear;
	}

	public void setFirstName(String firstName) {
		if (firstName == null) {
			firstName = "null";
		}
		firstName = firstName.trim();
		if (firstName.length() > 25) {
			firstName = firstName.substring(0, 24);
		}
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		if (lastName == null) {
			lastName = "null";
		}
		lastName = lastName.trim();
		if (lastName.length() > 25) {
			lastName = lastName.substring(0, 24);
		}
		this.lastName = lastName;
	}

	public void setPubCode(String pubCode) {
		if (pubCode == null) {
			pubCode = "??";
		}
		pubCode = pubCode.trim();
		if (pubCode.length() > 2) {
			pubCode = pubCode.substring(0, 2);
		}
		this.pubCode = pubCode;
	}

	public void setResponseCode(String responseCode) {
		if (responseCode == null) {
			responseCode = "null";
		}
		responseCode = responseCode.trim();
		if (responseCode.length() > 15) {
			responseCode = responseCode.substring(0, 14);
		}
		this.responseCode = responseCode;
	}

	public void setResponseMessage(String responseMessage) {
		if (responseMessage == null) {
			responseMessage = "null";
		}
		responseMessage = responseMessage.trim();
		if (responseMessage.length() > 128) {
			responseMessage = responseMessage.substring(0, 127);
		}
		this.responseMessage = responseMessage;
	}

	public void setVendorTranID(String vendorTranID) {
		if (vendorTranID == null) {
			vendorTranID = "null";
		}
		vendorTranID = vendorTranID.trim();
		if (vendorTranID.length() > 32) {
			vendorTranID = vendorTranID.substring(0, 31);
		}
		this.vendorTranID = vendorTranID;
	}

	public void setZip(String zip) {
		if (zip == null) {
			zip = "null";
		}
		zip = zip.trim();
		if (zip.length() > 5) {
			zip = zip.substring(0, 5);
		}
		this.zip = zip;
	}

	public void setOrderID(String orderID) {
		if (orderID == null) {
			orderID = "null";
		}
		orderID = orderID.trim();
		if (orderID.length() > 20) {
			orderID = orderID.substring(0, 20);
		}
		this.orderID = orderID;
	}

	public String getClientIPAddress() {
		return this.clientIP;
	}

	public void setClientIPAddress(String ip) {
		if (ip != null) {
			this.clientIP = ip;
		}
	}

	public String getCVVResponseCode() {
		return this.cvvResponseCode;
	}

	public String getCVVResponseMessage() {
		return this.cvvResponseMessage;
	}

	public void setCvvResponseCode(String cvvResponseCode) {
		if (cvvResponseCode == null) {
			cvvResponseCode = "";
		}
		cvvResponseCode = cvvResponseCode.trim();
		if (cvvResponseCode.length() > 15) {
			cvvResponseCode = cvvResponseCode.substring(0, 14);
		}
		this.cvvResponseCode = cvvResponseCode;
	}

	public void setCvvResponseMessage(String cvvResponseMessage) {
		if (cvvResponseMessage == null) {
			this.cvvResponseMessage = "";
		}
		cvvResponseMessage = cvvResponseMessage.trim();
		if (cvvResponseMessage.length() > 128) {
			cvvResponseMessage = cvvResponseMessage.substring(0, 127);
		}

		this.cvvResponseMessage = cvvResponseMessage;
	}

	public String getExternalKey() {
		return this.externalKey;
	}

	public String getReasonCode() {
		return this.reasonCode;
	}

	public void setExternalKey(String externalKey) {
		if (externalKey == null) {
			this.externalKey = "";
		}
		externalKey = externalKey.trim();
		if (externalKey.length() > 50) {
			externalKey = externalKey.substring(0, 49);
		}
		this.externalKey = externalKey;
	}

	public void setReasonCode(String reasonCode) {
		if (reasonCode == null) {
			this.reasonCode = "";
		}
		reasonCode = reasonCode.trim();
		if (reasonCode.length() > 30) {
			reasonCode = reasonCode.substring(0, 29);
		}
		this.reasonCode = reasonCode;
	}

	public String toString() {
		StringBuffer sBuf = new StringBuffer("CCLOG::   ");
		try {
			sBuf.append(" AMOUNT:").append(this.getAmount());
			sBuf.append(" AUTH CODE:").append(this.getAuthCode());
			sBuf.append(" CCNum:").append(this.getCCNum());
			sBuf.append(" CCType:").append(this.getCcType());
			sBuf.append(" EMAIL:").append(this.getEmail());
			sBuf.append(" EXP MONTH:").append(this.getExpMonth());
			sBuf.append(" EXP YEAR:").append(this.getExpYear());
			sBuf.append(" PUB CODE:").append(this.getPubCode());
			sBuf.append(" FIRST NAME:").append(this.getFirstName());
			sBuf.append(" LAST NAME:").append(this.getLastName());
			sBuf.append(" ZIP:").append(this.getZip());
			sBuf.append(" RESPONSE CODE:").append(this.getResponseCode());
			sBuf.append(" RESPONSE MESSAGE:").append(this.getResponseMessage());
			sBuf.append(" AVS RESPONSE CODE:").append(this.getAvsRespCode());
			sBuf.append(" AVS RESPONSE MESSAGE:").append(this.getAvsRespMessage());
			sBuf.append(" CVV RESPONSE CODE:").append(this.getCVVResponseCode());
			sBuf.append(" CVV RESPONSE MESSAGE:").append(this.getCVVResponseMessage());
			sBuf.append(" VENDOR TRAN:").append(this.getVendorTranID());
			sBuf.append(" CLIENT IP:").append(this.getClientIPAddress());
			sBuf.append(" REASON CODE:").append(this.getReasonCode());
			sBuf.append(" EXTERNAL KEY:").append(this.getExternalKey());
		} catch (Exception e) {
			sBuf.append("Exception generating string.");
		}
		return sBuf.toString();
	}

	public String getRequestType() {
		return this.requestType;
	}

	public void setRequestType(String requestType) {
		if (requestType == null) {
			requestType = "";
		}
		requestType = requestType.trim();
		if (requestType.length() > 20) {
			requestType = requestType.substring(0, 19);
		}
		this.requestType = requestType;
	}
}
