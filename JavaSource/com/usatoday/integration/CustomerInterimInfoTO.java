/*
 * Created on Sep 23, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.integration;

import java.io.Serializable;
import java.sql.Timestamp;

import com.usatoday.business.interfaces.customer.CustomerInterimInfoIntf;

/**
 * @author aeast
 * 
 * 
 */
public class CustomerInterimInfoTO implements CustomerInterimInfoIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8316565004199062225L;
	private String publication = null;
	private String email = null;
	private String firstName = null;
	private String lastName = null;
	private String firmName = null;
	private String address1 = null;
	private String address2 = null;
	private String city = null;
	private String state = null;
	private String zip = null;
	private String homePhone = null;
	private String workPhone = null;
	private String billFirstName = null;
	private String billLastName = null;
	private String billFirmName = null;
	private String billAddress1 = null;
	private String billAddress2 = null;
	private String billCity = null;
	private String billState = null;
	private String billZip = null;
	private String billPhone = null;
	private String rateCode = null;
	private String promoCode = null;
	private String contestCode = null;
	private String srcOrdCode = null;
	private String subsAmount = null;
	private String subsDur = null;
	private String startDate = null;
	private String delvMethod = null;
	private long orderID = -1;
	private int processState = 0;
	private Timestamp insertedTimeStamp = null;

	// flag to indicate if this object is saved

	/**
     *  
     */
	public CustomerInterimInfoTO() {
		super();
	}

	/**
	 * @return Returns the delvMethod.
	 */
	public String getDelvMethod() {
		return delvMethod;
	}

	/**
	 * @param delvMethod
	 * 
	 */
	public void setDelvMethod(String delvMethod) {
		this.delvMethod = delvMethod;
	}

	/**
	 * @return Returns the address1.
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1
	 *            The address1 to set.
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return Returns the address2.
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2
	 *            The address2 to set.
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return Returns the billAddress1.
	 */
	public String getBillAddress1() {
		return billAddress1;
	}

	/**
	 * @param billAddress1
	 *            The billAddress1 to set.
	 */
	public void setBillAddress1(String billAddress1) {
		this.billAddress1 = billAddress1;
	}

	/**
	 * @return Returns the billAddress2.
	 */
	public String getBillAddress2() {
		return billAddress2;
	}

	/**
	 * @param billAddress2
	 *            The billAddress2 to set.
	 */
	public void setBillAddress2(String billAddress2) {
		this.billAddress2 = billAddress2;
	}

	/**
	 * @return Returns the billCity.
	 */
	public String getBillCity() {
		return billCity;
	}

	/**
	 * @param billCity
	 *            The billCity to set.
	 */
	public void setBillCity(String billCity) {
		this.billCity = billCity;
	}

	/**
	 * @return Returns the billFirmName.
	 */
	public String getBillFirmName() {
		return billFirmName;
	}

	/**
	 * @param billFirmName
	 *            The billFirmName to set.
	 */
	public void setBillFirmName(String billFirmName) {
		this.billFirmName = billFirmName;
	}

	/**
	 * @return Returns the billFirstName.
	 */
	public String getBillFirstName() {
		return billFirstName;
	}

	/**
	 * @param billFirstName
	 *            The billFirstName to set.
	 */
	public void setBillFirstName(String billFirstName) {
		this.billFirstName = billFirstName;
	}

	/**
	 * @return Returns the billLastName.
	 */
	public String getBillLastName() {
		return billLastName;
	}

	/**
	 * @param billLastName
	 *            The billLastName to set.
	 */
	public void setBillLastName(String billLastName) {
		this.billLastName = billLastName;
	}

	/**
	 * @return Returns the billPhone.
	 */
	public String getBillPhone() {
		return billPhone;
	}

	/**
	 * @param billPhone
	 *            The billPhone to set.
	 */
	public void setBillPhone(String billPhone) {
		this.billPhone = billPhone;
	}

	/**
	 * @return Returns the billZip.
	 */
	public String getBillZip() {
		return billZip;
	}

	/**
	 * @param billZip
	 *            The billZip to set.
	 */
	public void setBillZip(String billZip) {
		this.billZip = billZip;
	}

	/**
	 * @return Returns the city.
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            The city to set.
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return Returns the contestCode.
	 */
	public String getContestCode() {
		return contestCode;
	}

	/**
	 * @param contestCode
	 *            The contestCode to set.
	 */
	public void setContestCode(String contestCode) {
		this.contestCode = contestCode;
	}

	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return Returns the firmName.
	 */
	public String getFirmName() {
		return firmName;
	}

	/**
	 * @param firmName
	 *            The firmName to set.
	 */
	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the homePhone.
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * @param homePhone
	 *            The homePhone to set.
	 */
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	/**
	 * @return Returns the insertedTimeStamp.
	 */
	public Timestamp getInsertedTimeStamp() {
		return insertedTimeStamp;
	}

	/**
	 * @param insertedTimeStamp
	 *            The insertedTimeStamp to set.
	 */
	public void setInsertedTimeStamp(Timestamp insertedTimeStamp) {
		this.insertedTimeStamp = insertedTimeStamp;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the promoCode.
	 */
	public String getPromoCode() {
		return promoCode;
	}

	/**
	 * @param promoCode
	 *            The promoCode to set.
	 */
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	/**
	 * @return Returns the publication.
	 */
	public String getPublication() {
		return publication;
	}

	/**
	 * @param publication
	 *            The publication to set.
	 */
	public void setPublication(String publication) {
		this.publication = publication;
	}

	/**
	 * @return Returns the rateCode.
	 */
	public String getRateCode() {
		return rateCode;
	}

	/**
	 * @param rateCode
	 *            The rateCode to set.
	 */
	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	/**
	 * @return Returns the srcOrdCode.
	 */
	public String getSrcOrdCode() {
		return srcOrdCode;
	}

	/**
	 * @param srcOrdCode
	 *            The srcOrdCode to set.
	 */
	public void setSrcOrdCode(String srcOrdCode) {
		this.srcOrdCode = srcOrdCode;
	}

	/**
	 * @return Returns the startDate.
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            The startDate to set.
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return Returns the subsAmount.
	 */
	public String getSubsAmount() {
		return subsAmount;
	}

	/**
	 * @param subsAmount
	 *            The subsAmount to set.
	 */
	public void setSubsAmount(String subsAmount) {
		this.subsAmount = subsAmount;
	}

	/**
	 * @return Returns the subsDur.
	 */
	public String getSubsDur() {
		return subsDur;
	}

	/**
	 * @param subsDur
	 *            The subsDur to set.
	 */
	public void setSubsDur(String subsDur) {
		this.subsDur = subsDur;
	}

	/**
	 * @return Returns the zip.
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 *            The zip to set.
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return Returns the billState.
	 */
	public String getBillState() {
		return billState;
	}

	/**
	 * @param billState
	 *            The billState to set.
	 */
	public void setBillState(String billState) {
		this.billState = billState;
	}

	/**
	 * @return Returns the state.
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            The state to set.
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return Returns the orderID.
	 */
	public long getOrderID() {
		return orderID;
	}

	/**
	 * @param orderID
	 *            The orderID to set.
	 */
	public void setOrderID(long orderID) {
		this.orderID = orderID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CustomerInterimInfoIntf#getworkPhone()
	 */
	public String getWorkPhone() {
		// TODO Auto-generated method stub
		return workPhone;
	}

	/**
	 * @return Returns the processState.
	 */
	public int getProcessState() {
		return processState;
	}

	/**
	 * @param processState
	 *            The processState to set.
	 */
	public void setProcessState(int processState) {
		this.processState = processState;
	}

	/**
	 * @param workPhone
	 *            The workPhone to set.
	 */
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

}
