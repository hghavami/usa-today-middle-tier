/*
 * Created on Apr 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.CreditCardInfoIntf;

/**
 * @author aeast
 * @date Apr 2, 2007
 * @class UserTO
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class CreditCardInfoTO implements CreditCardInfoIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2365317440085169765L;
	private String creditCardNewGift = "";
	private String creditCardNum = "";
	private String creditCardExp = "";

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#save()
	 */
	public void save() throws UsatException {

	}

	public String getCreditCardNewGift() {
		return creditCardNewGift;
	}

	public String getCreditCardNum() {
		return creditCardNum;
	}

	public String getCreditCardExp() {
		return creditCardExp;
	}

	public void setCreditCardNewGift(String creditCardNewGift) {
		this.creditCardNewGift = creditCardNewGift;
	}

	public void setCreditCardNum(String creditCardNum) {
		this.creditCardNum = creditCardNum;
	}

	public void setCreditCardExp(String creditCardExp) {
		this.creditCardExp = creditCardExp;
	}
}
