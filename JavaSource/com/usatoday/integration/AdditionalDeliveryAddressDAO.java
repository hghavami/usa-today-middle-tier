package com.usatoday.integration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.AdditionalDeliveryAddressIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;

public class AdditionalDeliveryAddressDAO extends USATodayDAO {

	private static final String SELECT_ADDITIONAL_DELIVERY_ADDRESS_BY_ID = "{call USAT_SELECT_ADDITIONAL_DELIVERY_ADDRESS (?)}";
	private static final String DELETE_ADDITIONAL_DELIVERY_ADDRESS_BY_ID = "{call USAT_DELETE_ADDITIONAL_DELIVERY_ADDRESS (?)}";

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private static Collection<AdditionalDeliveryAddressIntf> objectFactory(ResultSet rs) throws UsatException {
		Collection<AdditionalDeliveryAddressIntf> records = new ArrayList<AdditionalDeliveryAddressIntf>();
		try {
			while (rs.next()) {

				int primaryKey = rs.getInt("id");

				String tempStr = null;

				AdditionalDeliveryAddressTO additionalDeliveryAddress = new AdditionalDeliveryAddressTO();

				additionalDeliveryAddress.setId(primaryKey);

				tempStr = rs.getString("Accountnum");
				if (tempStr != null) {
					additionalDeliveryAddress.setCurrAcctNum(tempStr.trim());
				}

				tempStr = rs.getString("LastName");
				if (tempStr != null) {
					additionalDeliveryAddress.setLastName(tempStr.trim());
				}

				tempStr = rs.getString("FirstName");
				if (tempStr != null) {
					additionalDeliveryAddress.setFirstName(tempStr.trim());
				}

				tempStr = rs.getString("FirmName");
				if (tempStr != null) {
					additionalDeliveryAddress.setFirmName(tempStr.trim());
				}

				tempStr = rs.getString("StreetAddress");
				if (tempStr != null) {
					additionalDeliveryAddress.setStreetAddress(tempStr.trim());
				}

				tempStr = rs.getString("AptDeptSuite");
				if (tempStr != null) {
					additionalDeliveryAddress.setAptDeptSuite(tempStr.trim());
				}

				tempStr = rs.getString("AddlAddr1");
				if (tempStr != null) {
					additionalDeliveryAddress.setAddlAddr1(tempStr.trim());
				}

				tempStr = rs.getString("City");
				if (tempStr != null) {
					additionalDeliveryAddress.setCity(tempStr);
				}

				tempStr = rs.getString("State");
				if (tempStr != null) {
					additionalDeliveryAddress.setState(tempStr);
				}

				tempStr = rs.getString("Zip");
				if (tempStr != null) {
					additionalDeliveryAddress.setZip(tempStr);
				}

				tempStr = rs.getString("HomePhone");
				if (tempStr != null) {
					additionalDeliveryAddress.setHomePhone(tempStr);
				}

				tempStr = rs.getString("BusPhone");
				if (tempStr != null) {
					additionalDeliveryAddress.setBusPhone(tempStr);
				}

				int tempInt = rs.getInt("EmailID");
				if (tempInt > 0) {
					additionalDeliveryAddress.setEmailID(tempInt);
				}

				Timestamp ts = rs.getTimestamp("Insert_TimeStamp");
				if (ts != null) {
					additionalDeliveryAddress.setInsertTimeStamp(ts);
				}

				records.add(additionalDeliveryAddress);
			}
		} catch (SQLException sqle) {
			throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
		}
		return records;
	}

	/**
	 * 
	 * @param emailID
	 * @return
	 * @throws UsatException
	 */
	public Collection<AdditionalDeliveryAddressIntf> select(String emailAddress) throws UsatException {

		java.sql.Connection conn = null;
		Collection<AdditionalDeliveryAddressIntf> records = null;

		try {
			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn
					.prepareStatement(AdditionalDeliveryAddressDAO.SELECT_ADDITIONAL_DELIVERY_ADDRESS_BY_ID);

			statement.setString(1, emailAddress);

			ResultSet rs = statement.executeQuery();

			records = AdditionalDeliveryAddressDAO.objectFactory(rs);
		} catch (Exception e) {
			System.out.println("additionalDeliveryAddressDAO:fetchadditionalDeliveryAddress " + e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}

		return records;
	}

	/**
	 * 
	 * @param emailID
	 * @return
	 * @throws UsatException
	 */
	public synchronized int delete(int ID) throws UsatException {
		java.sql.Connection conn = null;

		int aDADeleted = 0;

		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement cs = conn.prepareCall(AdditionalDeliveryAddressDAO.DELETE_ADDITIONAL_DELIVERY_ADDRESS_BY_ID);

			cs.setInt(1, ID);

			cs.executeUpdate();

			cs.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}
		return aDADeleted;
	}

	// Prepare SQL for insert into XTRNTINTCSLINF table
	private StringBuffer prepareSqlInsert(CustomerIntf customer) {

		StringBuffer sql = new StringBuffer();
		if (customer.getCurrentAccount().getAccountNumber() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getAccountNumber())).append("',");
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getLastName() != null) {
			// Only get the first 15 chars to prevent data truncation
			if (customer.getCurrentAccount().getDeliveryContact().getLastName().trim().length() <= 15) {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getLastName()))
						.append("',");
			} else {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getLastName()
								.trim().substring(0, 14))).append("',");
			}
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getFirstName() != null) {
			// Only get the first 10 chars to prevent data truncation
			if (customer.getCurrentAccount().getDeliveryContact().getFirstName().trim().length() <= 10) {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getFirstName()))
						.append("',");
			} else {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getFirstName()
								.trim().substring(0, 9))).append("',");
			}
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getFirmName() != null) {
			// Only get the first 28 chars to prevent data truncation
			if (customer.getCurrentAccount().getDeliveryContact().getFirmName().trim().length() <= 28) {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getFirmName()))
						.append("',");
			} else {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getFirmName()
								.trim().substring(0, 27))).append("',");
			}
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress1() != null) {
			// Only get the first 28 chars to prevent data truncation
			if (customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress1().trim().length() <= 28) {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getUIAddress()
								.getAddress1())).append("',");
			} else {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getUIAddress()
								.getAddress1().trim().substring(0, 27))).append("',");
			}
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAptSuite() != null) {
			// Only get the first 28 chars to prevent data truncation
			if (customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAptSuite().trim().length() <= 28) {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getUIAddress()
								.getAptSuite())).append("',");
			} else {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getUIAddress()
								.getAptSuite().trim().substring(0, 27))).append("',");
			}
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getUIAddress().getAddress2() != null) {
			sql.append("'")
					.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getUIAddress()
							.getAddress2())).append("',");
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getUIAddress().getCity() != null) {
			// Only get the first 28 chars to prevent data truncation
			if (customer.getCurrentAccount().getDeliveryContact().getUIAddress().getCity().trim().length() <= 28) {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getUIAddress()
								.getCity())).append("',");
			} else {
				sql.append("'")
						.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getUIAddress()
								.getCity().trim().substring(0, 27))).append("',");
			}
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getUIAddress().getState() != null) {
			sql.append("'")
					.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getUIAddress()
							.getState())).append("',");
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getUIAddress().getZip() != null) {
			sql.append("'")
					.append(USATodayDAO
							.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getUIAddress().getZip()))
					.append("',");
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getHomePhone() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getHomePhone()))
					.append("',");
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentAccount().getDeliveryContact().getBusinessPhone() != null) {
			sql.append("'")
					.append(USATodayDAO.escapeApostrophes(customer.getCurrentAccount().getDeliveryContact().getBusinessPhone()))
					.append("',");
		} else {
			sql.append("null,");
		}
		if (customer.getCurrentEmailRecord().getSerialNumber() != 0) {
			sql.append("'").append(customer.getCurrentEmailRecord().getSerialNumber()).append("',");
		} else {
			sql.append("null,");
		}
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		if (ts != null) {
			sql.append("'").append(ts).append("')");
		}

		return sql;
	}

	// Insert a record into the usat_additional_delivery_address table
	public void insert(CustomerIntf customer) throws UsatException {
		int rowsAffected = 0;

		if (customer == null) {
			throw new UsatException("Null Expired Subscription Renewed Access object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();

			StringBuffer sql = new StringBuffer(
					"insert into usat_additional_delivery_address (AccountNum, LastName, FirstName, FirmName, StreetAddress, AptDeptSuite, "
							+ "AddlAddr1, City, State, Zip, HomePhone, BusPhone, EmailID, Insert_timestamp) values (");
			// Fill in the SQL statement

			sql.append(prepareSqlInsert(customer));

			rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Additional Delivery Address Information Object Inserted Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			ResultSet rs = statement.getGeneratedKeys();

			rs.next();

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException(
						"usat_additional_delivery_address: No Records Inserted and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

}
