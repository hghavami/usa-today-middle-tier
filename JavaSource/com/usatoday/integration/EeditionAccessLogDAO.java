package com.usatoday.integration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import org.joda.time.DateMidnight;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.EeditionAccessLogIntf;

public class EeditionAccessLogDAO extends USATodayDAO {

	// Insert a record into the usat_eEdition_access_log table
	public void insert(EeditionAccessLogIntf EeditionAccessLogIntf) throws UsatException {
		int rowsAffected = 0;

		if (EeditionAccessLogIntf == null) {
			throw new UsatException("Null USAT Tech Guide Order object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();

			StringBuffer sql = new StringBuffer(
					"insert into usat_eEdition_access_log (PubCode, Email, FirstName, LastName, FirmName, Address1, Address2, City, State, Zip, HomePhone, WorkPhone, BillFirstName, BillLastName, BillFirmName, BillAddress1, BillAddress2, BillCity, BillState, BillZip, BillPhone, RateCode, PromoCode, ContestCode, SrcOrdCode, SubsAmount, SubsDur, StartDate, Insert_timestamp) values (");
			// Fill in the SQL statement

			sql.append(prepareSqlInsert(EeditionAccessLogIntf));

			rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Eedition Access Log Object Inserted Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			ResultSet rs = statement.getGeneratedKeys();

			rs.next();
			int primaryKey = rs.getInt(1);

			EeditionAccessLogIntf.setTransID(primaryKey);

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException(
						"usat_eEdition_access_log: No Records Inserted and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

	// Update a record into the usat_eEdition_access_log table
	public void update(EeditionAccessLogIntf EeditionAccessLogIntf) throws UsatException {
		int rowsAffected = 0;

		if (EeditionAccessLogIntf == null) {
			throw new UsatException("Null USAT Tech Guide Order object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();

			StringBuffer sql = new StringBuffer("update usat_eEdition_access_log set ");
			// Fill in the SQL statement

			sql.append(prepareSqlUpdate(EeditionAccessLogIntf));
			sql.append(" where ID = '").append(EeditionAccessLogIntf.getTransID()).append("'");

			rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Eedition Access Log Object Updated Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException("usat_eEdition_access_log: No Records Updated and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

	// Delete a record into the usat_eEdition_access_log table
	public void delete(EeditionAccessLogIntf EeditionAccessLogIntf) throws UsatException {
		int rowsAffected = 0;

		if (EeditionAccessLogIntf == null) {
			throw new UsatException("Null USAT Tech Guide Order object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();

			StringBuffer sql = new StringBuffer("delete from usat_eEdition_access_log where ID = "
					+ EeditionAccessLogIntf.getTransID());
			// Fill in the SQL statement

			rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Eedition Access Log Object Updated Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException("usat_eEdition_access_log: No Records Deleted and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

	// Prepare SQL for insert into XTRNTINTCSLINF table
	private StringBuffer prepareSqlInsert(EeditionAccessLogIntf EeditionAccessLogIntf) {

		StringBuffer sql = new StringBuffer();
		if (EeditionAccessLogIntf.getPublication() != null) {
			sql.append("'").append(EeditionAccessLogIntf.getPublication()).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getEmailAddress() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getEmailAddress())).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getAccountNum() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getAccountNum())).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getTrialAccess() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getTrialAccess())).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getDateTime() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getDateTime().toString())).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getClientIP() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getClientIP())).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getEmailAddress() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getEmailAddress())).append("',");
		} else {
			sql.append("null,");
		}

		Timestamp ts = new Timestamp(System.currentTimeMillis());
		if (ts != null) {
			sql.append("'").append(ts).append("')");
		}

		return sql;
	}

	// Prepare SQL for update into XTRNTINTCSLINF table
	private StringBuffer prepareSqlUpdate(EeditionAccessLogIntf EeditionAccessLogIntf) {

		StringBuffer sql = new StringBuffer();
		if (EeditionAccessLogIntf.getPublication() != null) {
			sql.append("PubCode = '").append(EeditionAccessLogIntf.getPublication()).append("',");
		} else {
			sql.append("PubCode = null,");
		}
		if (EeditionAccessLogIntf.getEmailAddress() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getEmailAddress())).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getAccountNum() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getAccountNum())).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getTrialAccess() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getTrialAccess())).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getDateTime() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getDateTime().toString())).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getClientIP() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getClientIP())).append("',");
		} else {
			sql.append("null,");
		}
		if (EeditionAccessLogIntf.getEmailAddress() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(EeditionAccessLogIntf.getEmailAddress())).append("',");
		} else {
			sql.append("null,");
		}

		Timestamp ts = new Timestamp(System.currentTimeMillis());
		if (ts != null) {
			sql.append(" Insert_timestamp = '").append(ts).append("'");
		}

		return sql;
	}

	// Update a record into the usat_eEdition_access_log table
	public void selectForFTP(EeditionAccessLogIntf EeditionAccessLogIntf) throws UsatException {
		int rowsAffected = 0;

		if (EeditionAccessLogIntf == null) {
			throw new UsatException("Null USAT Tech Guide Order object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();
			// Set time stamp to compare with to today's with time of 12 am
			DateMidnight dm = new DateMidnight();

			Timestamp ts = new Timestamp(dm.getMillis());

			// Fill in the SQL statement
			StringBuffer sql = new StringBuffer("select * from usat_eEdition_access_log where ");
			sql.append(" where ProcessState = 0 and Insert_timestamp < ").append(ts);

			rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Eedition Access Log Object Updated Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException("usat_eEdition_access_log: No Records Updated and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

	/**
	 * 
	 * @param numDays
	 * @return
	 * @throws UsatException
	 */
	public int deleteAttributesOlderThanSpecifiedDays(int numDays) throws UsatException {
		int rowsAffected = 0;
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn
					.prepareStatement(EeditionAccessLogDAO.DELETE_ACCESS_LOG_ARRTIBUTES_IN_DELETE_STATE_OLDER_THAN_DATE);

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.add(Calendar.DAY_OF_YEAR, (-1 * numDays));

			Timestamp ts = new Timestamp(cal.getTimeInMillis());

			statement.setTimestamp(1, ts);

			rowsAffected = statement.executeUpdate();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return rowsAffected;
	}

	private Collection<EeditionAccessLogIntf> fetchAttributesInState(Timestamp dateToStartProcess) throws UsatException {
		Collection<EeditionAccessLogIntf> records = null;
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(EeditionAccessLogDAO.FETCH_ACCESS_LOG_ATTRIBUTES_IN_STATE);

			statement.setTimestamp(1, dateToStartProcess);

			ResultSet rs = statement.executeQuery();

			records = EeditionAccessLogDAO.objectFactory(rs);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return records;
	}

	public Collection<EeditionAccessLogIntf> fetchAttributesRecordsForBatchProcessing(Timestamp dateToStartProcess)
			throws UsatException {
		return this.fetchAttributesInState(dateToStartProcess);
	}

	private static final String DELETE_ACCESS_LOG_ARRTIBUTES_IN_DELETE_STATE_OLDER_THAN_DATE = "delete from usat_eEdition_access_log where date_time <= ?";
	private static final String FETCH_ACCESS_LOG_ATTRIBUTES_IN_STATE = "select * from usat_eEdition_access_log where date_time >= ?";

	private synchronized static Collection<EeditionAccessLogIntf> objectFactory(ResultSet rs) throws UsatException {
		Collection<EeditionAccessLogIntf> records = new ArrayList<EeditionAccessLogIntf>();

		try {
			// iterate over result set and build objects
			while (rs.next()) {

				long primaryKey = rs.getLong("id");

				String tempStr = null;

				EeditionAccessLogTO attribute = new EeditionAccessLogTO();

				attribute.setTransID(primaryKey);

				tempStr = rs.getString("accountNumber");
				if (tempStr != null && tempStr.trim().length() > 2) {
					attribute.setAccountNum(tempStr.trim().substring(2)); // Drop the 34 from the account number
				}

				tempStr = rs.getString("pubCode");
				if (tempStr != null) {
					attribute.setPublication(tempStr.trim());
				}

				tempStr = rs.getString("trial_access");
				if (tempStr != null) {
					attribute.setTrialAccess(tempStr.trim());
				}

				Timestamp ts = rs.getTimestamp("date_time");
				if (ts != null) {
					attribute.setDateTime(ts);
				}

				tempStr = rs.getString("clientIP");
				if (tempStr != null) {
					attribute.setClientIP(tempStr.trim());
				}

				tempStr = rs.getString("emailAddress");
				if (tempStr != null) {
					attribute.setEmailAddress(tempStr.trim());
				}

				records.add(attribute);
			}
		} catch (Exception e) {
			System.out.println("EeditionAccessLogDAO::objectFactory() - Failed to create Eedition Access Log Record TO : "
					+ e.getMessage());
		}

		return records;
	}

}
