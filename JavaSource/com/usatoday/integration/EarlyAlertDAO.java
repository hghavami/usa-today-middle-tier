/*
 * Created on Mar 30, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.sql.SQLException;
import java.sql.Types;

import com.usatoday.UsatException;

/**
 * @author wong
 * @date Mar 30, 2007
 * @class UserDAO
 * 
 *        This class interfaces with the AS/400 to perform user management.
 * 
 */
public class EarlyAlertDAO extends USATodayDAO {

	// SQL

	private static final String GET_EARLY_ALERTNew = "{call NAT_UNIOBJ.EALRT_GET_CMPLT(?,?,?)}";

	/**
     * 
      */

	public EarlyAlertTO determineEarlyAlertNew(String pub, String accountNum) throws UsatException {
		java.sql.Connection conn = null;
		EarlyAlertTO earlyAlert = null;

		try {

			conn = ConnectionManager.getInstance().getIseriesConnection();

			java.sql.CallableStatement statement = conn.prepareCall(EarlyAlertDAO.GET_EARLY_ALERTNew);

			String upperCasePub = pub.toUpperCase();
			statement.setString(1, upperCasePub);

			statement.setString(2, accountNum);

			statement.setString(3, "");

			statement.registerOutParameter(3, Types.CHAR);

			statement.execute();

			earlyAlert = new EarlyAlertTO();

			String delay = statement.getString(3);

			earlyAlert.setEarlyAlertDelay(delay);

		} catch (SQLException sqle) {
			System.out.println("EarlyAlertDAO : getEarlyAlert() " + sqle.getMessage());

			throw new UsatException(sqle);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return earlyAlert;
	}

}
