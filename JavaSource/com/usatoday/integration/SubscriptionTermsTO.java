/*
 * Created on Apr 13, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;

import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;

/**
 * @author aeast
 * @date Apr 13, 2006
 * @class SubscriptionTermsTO
 * 
 *        This class is used to represent a single term and should only be retrieved using a SubscriptionOfferIntf.
 * 
 */
public class SubscriptionTermsTO implements SubscriptionTermsIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7627148488879304321L;
	private String description = "";
	private String DurationInWeeks = "";
	private double price = 0.0;
	private String piaRateCode = "";
	private String relatedRateCode = "";
	private String renewalRateCode = "";
	private String renewalPeriodLength = "";
	private String pubCode = "";

	private String duration = "";
	private String durationType = "M"; // Indicates Months or Weeks M/W
	private String fodCode = "MF"; // Indicated M-F and replaced the old DO
	private boolean requiresEZPAY = false;
	private String disclaimerText = "";

	public SubscriptionTermsTO(String description, String durationInWeeks, double price, String piaRateCode,
			String relatedRateCode, String renewalRateCode, String renewalPeriodLength, String pubCode, String duration,
			String durationType, String fodCode, boolean requiresEZPAY, String disclaimerText) {
		super();
		this.description = description;
		this.DurationInWeeks = durationInWeeks;
		this.price = price;
		this.piaRateCode = piaRateCode;
		this.relatedRateCode = relatedRateCode;
		this.renewalRateCode = renewalRateCode;
		this.renewalPeriodLength = renewalPeriodLength;
		this.pubCode = pubCode;

		this.duration = duration;
		this.durationType = durationType;
		this.fodCode = fodCode;
		this.requiresEZPAY = requiresEZPAY;
		this.disclaimerText = disclaimerText;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getDailyRate()
	 */
	public double getDailyRate() {
		// This method is only implemented in the business object
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getDescription()
	 */
	public String getDescription() {
		return description;
	}

	public String getDisclaimerText() {
		return disclaimerText;
	}

	public String getDuration() {
		return duration;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getDurationInWeeks()
	 */
	public String getDurationInWeeks() {
		return DurationInWeeks;
	}

	public String getDurationType() {
		return durationType;
	}

	public String getFODCode() {
		return fodCode;
	}

	public SubscriptionOfferIntf getParentOffer() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getPiaRateCode()
	 */
	public String getPiaRateCode() {
		return piaRateCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getPrice()
	 */
	public double getPrice() {
		return price;
	}

	@Override
	public String getPriceAsString() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getPubCode()
	 */
	public String getPubCode() {
		return this.pubCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getRelatedRateCode()
	 */
	public String getRelatedRateCode() {
		return relatedRateCode;
	}

	public String getRenewalPeriodLength() {
		return this.renewalPeriodLength;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getRenewalRateCode()
	 */
	public String getRenewalRateCode() {
		return renewalRateCode;
	}

	public String getTermAsString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getPubCode()).append("_");
		sb.append(this.getPiaRateCode()).append("_");
		sb.append(this.getDurationInWeeks()).append("_");
		if (this.requiresEZPAY()) {
			sb.append("true");
		} else {
			sb.append("false");
		}

		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#requiresEZPAY()
	 */
	public boolean requiresEZPAY() {
		// this is purely a business logic and contains no relevance here.
		return this.requiresEZPAY;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDisclaimerText(String disclaimerText) {
		this.disclaimerText = disclaimerText;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setDurationInWeeks(String durationInWeeks) {
		this.DurationInWeeks = durationInWeeks;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public void setFODCode(String fodCode) {
		this.fodCode = fodCode;
	}

	public void setPiaRateCode(String piaRateCode) {
		this.piaRateCode = piaRateCode;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setRelatedRateCode(String relatedRateCode) {
		this.relatedRateCode = relatedRateCode;
	}

	public void setRenewalPeriodLength(String renewalPeriodLength) {
		this.renewalPeriodLength = renewalPeriodLength;
	}

	public void setRenewalRateCode(String renewalRateCode) {
		this.renewalRateCode = renewalRateCode;
	}

}
