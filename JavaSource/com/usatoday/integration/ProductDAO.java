package com.usatoday.integration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.PersistentProductIntf;

public class ProductDAO extends USATodayDAO {

	private static final String PRODUCT_FIELDS = "id, product_code, default_keycode, expired_offer_keycode, default_renewal_keycode, product_name, branding_pubcode, tax_code, min_quantity, max_quantity, supplier_id, fulfillment_method, initial_inventory, current_inventory, insert_timestamp, update_timestamp, customer_service_phone, start_date, end_date, taxable, unit_price, handling_fee, short_description, long_description, max_days_in_future_to_fulfill, hold_delay_days, product_type";
	private static final String UPDATE = "update usat_product set product_code=?, product_name=?, tax_code=?, supplier_id=?, fulfillment_method=?, initial_inventory=?, current_inventory=?, update_timestamp=?, start_date=?, end_date=?, taxable=?, unit_price=?, handling_fee=?, short_description=?, long_description=?, default_keycode=?, expired_offer_keycode=?, default_renewal_keycode=?, max_days_in_future_to_fulfill=? where id = ?";
	private static final String DECREMENT_INVENTORY = "{call USAT_DECREMENT_INVENTORY (?,?)}";
	private static final String RETRIEVE_INVENTORY = "{call USAT_RETRIEVE_INVENTORY (?)}";
	private static final String SELECT_BY_ID = "select " + ProductDAO.PRODUCT_FIELDS + " from usat_product where id = ?";
	private static final String SEARCH = "select "
			+ ProductDAO.PRODUCT_FIELDS
			+ " from usat_product where (product_name like ? OR short_description like ? OR long_description like ?) AND Active = 'Y'";
	// private static final String DELETE_PRODUCT = "delete from usat_product where id = ? And Active = 'Y'";
	private static final String SELECT_BY_PRODUCT_CODE = "select " + ProductDAO.PRODUCT_FIELDS
			+ " from usat_product where product_code = ? AND Active = 'Y'";
	private static final String SELECT_BY_PRODUCT_CODE_INCLUDE_INACTIVE = "select " + ProductDAO.PRODUCT_FIELDS
			+ " from usat_product where product_code = ?";

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private static Collection<PersistentProductIntf> objectFactory(ResultSet rs) throws UsatException {
		Collection<PersistentProductIntf> records = new ArrayList<PersistentProductIntf>();
		try {
			while (rs.next()) {

				int primaryKey = rs.getInt("id");

				String tempStr = null;

				ProductTO product = new ProductTO();

				product.setId(primaryKey);

				tempStr = rs.getString("product_code");
				if (tempStr != null) {
					product.setProductCode(tempStr.trim());
				}

				tempStr = rs.getString("default_keycode");
				if (tempStr != null) {
					product.setDefaultKeycode(tempStr.trim());
				}

				tempStr = rs.getString("expired_offer_keycode");
				if (tempStr != null) {
					product.setExpiredOfferKeycode(tempStr.trim());
				}

				tempStr = rs.getString("default_renewal_keycode");
				if (tempStr != null) {
					product.setDefaultRenewalKeycode(tempStr.trim());
				}

				tempStr = rs.getString("tax_code");
				if (tempStr != null) {
					product.setTaxRateCode(tempStr.trim());
				}

				tempStr = rs.getString("product_name");
				if (tempStr != null) {
					product.setName(tempStr.trim());
				}

				tempStr = rs.getString("branding_pubcode");
				if (tempStr != null) {
					product.setBrandingPubCode(tempStr.trim());
				}

				tempStr = rs.getString("customer_service_phone");
				if (tempStr != null) {
					product.setCustomerServicePhone(tempStr.trim());
				}

				int tempInt = rs.getInt("initial_inventory");
				product.setInitialInventory(tempInt);

				tempInt = rs.getInt("hold_delay_days");
				product.setHoldDaysDelay(tempInt);

				tempInt = rs.getInt("max_days_in_future_to_fulfill");
				product.setMaxDaysInFutureBeforeFulfillment(tempInt);

				tempInt = rs.getInt("product_type");
				product.setProductType(tempInt);

				tempInt = rs.getInt("current_inventory");
				product.setInventory(tempInt);

				tempInt = rs.getInt("min_quantity");
				product.setMinQuantityPerOrder(tempInt);

				tempInt = rs.getInt("max_quantity");
				product.setMaxQuantityPerOrder(tempInt);

				Timestamp ts = rs.getTimestamp("insert_timestamp");
				if (ts != null) {
					product.setInsertTimestamp(new DateTime(ts.getTime()));
				}

				ts = rs.getTimestamp("update_timestamp");
				if (ts != null) {
					product.setUpdateTimestamp(new DateTime(ts.getTime()));
				}

				ts = rs.getTimestamp("start_date");
				if (ts != null) {
					product.setStartDate(new DateTime(ts.getTime()));
				}

				ts = rs.getTimestamp("end_date");
				if (ts != null) {
					product.setEndDate(new DateTime(ts.getTime()));
				}

				tempStr = rs.getString("taxable");
				if (tempStr != null) {
					if (tempStr.equalsIgnoreCase("Y")) {
						product.setTaxable(true);
					} else {
						product.setTaxable(false);
					}
				}

				double tempReal = rs.getDouble("unit_price");
				product.setUnitPrice(tempReal);

				// tempReal = rs.getDouble("handling_fee");
				// product.setHandlingFee(tempReal);

				tempStr = rs.getString("short_description");
				if (tempStr != null) {
					product.setDescription(tempStr.trim());
				}

				tempStr = rs.getString("long_description");
				if (tempStr != null) {
					product.setDetailedDescription(tempStr.trim());
				}

				try {
					tempInt = rs.getInt("fulfillment_method");
					if (tempInt >= 0) {
						product.setFulfillmentMethod(tempInt);
					}
				} catch (Exception e) {
					// ignore
				}

				tempInt = rs.getInt("supplier_id");
				if (tempInt > 0) {
					product.setSupplierID(tempInt);
				}

				records.add(product);
			}
		} catch (SQLException sqle) {
			throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
		}
		return records;
	}

	/**
	 * 
	 * @param product
	 * @return
	 * @throws UsatException
	 */
	public int update(PersistentProductIntf product) throws UsatException {
		int rowsAffected = 0;

		if (product == null || product.getID() <= 0) {
			throw new UsatException("Null or Unsaved USATProduct object passed in for update.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(ProductDAO.UPDATE);

			// update usat_product set product_code=?, product_name=?, tax_code=?, supplier_id=?,
			// fulfillment_method=?, initial_inventory=?, current_inventory=?,
			// update_timestamp=?, start_date=?, end_date=?,
			// taxable=?, unit_price=?, handling_fee=?, short_description=?,
			// long_description=?, default_keycode=?, expired_offer_keycode=?, max_days_in_future_to_fulfill=? where id = ?";

			if (product.getProductCode() != null) {
				statement.setString(1, product.getProductCode().trim());
			} else {
				statement.setNull(1, Types.CHAR);
			}

			if (product.getName() != null) {
				statement.setString(2, product.getName().trim());
			} else {
				statement.setNull(2, Types.CHAR);
			}

			if (product.getTaxRateCode() != null) {
				statement.setString(3, product.getTaxRateCode().trim());
			} else {
				statement.setNull(3, Types.CHAR);
			}

			// not using
			statement.setNull(4, Types.INTEGER);
			// not using
			statement.setNull(5, Types.INTEGER);

			statement.setInt(6, product.getInitialInventory());
			statement.setInt(7, product.getInventory());

			Timestamp updatets = new Timestamp(System.currentTimeMillis());
			// set update time same as insert time
			statement.setTimestamp(8, updatets);

			Timestamp ts = null;
			if (product.getStartDate() != null) {
				ts = new Timestamp(product.getStartDate().getMillis());
				statement.setTimestamp(9, ts);
			} else {
				statement.setNull(9, Types.TIMESTAMP);
			}

			if (product.getEndDate() != null) {
				ts = new Timestamp(product.getEndDate().getMillis());
				statement.setTimestamp(10, ts);
			} else {
				statement.setNull(10, Types.TIMESTAMP);
			}

			if (product.isTaxable()) {
				statement.setString(11, "Y");
			} else {
				statement.setString(11, "N");
			}

			statement.setDouble(12, product.getUnitPrice());
			// not used
			statement.setDouble(13, 0.0);

			if (product.getDescription() != null) {
				statement.setString(14, product.getDescription().trim());
			} else {
				statement.setNull(14, Types.VARCHAR);
			}

			if (product.getDetailedDescription() != null) {
				statement.setString(15, product.getDetailedDescription().trim());
			} else {
				statement.setNull(15, Types.VARCHAR);
			}

			if (product.getDefaultKeycode() != null) {
				statement.setString(16, product.getDefaultKeycode().trim());
			} else {
				statement.setNull(16, Types.CHAR);
			}

			if (product.getExpiredOfferKeycode() != null) {
				statement.setString(17, product.getExpiredOfferKeycode().trim());
			} else {
				statement.setNull(17, Types.CHAR);
			}

			if (product.getDefaultKeycode() != null) {
				statement.setString(18, product.getDefaultRenewalKeycode().trim());
			} else {
				statement.setNull(18, Types.CHAR);
			}

			if (product.getMaxDaysInFutureBeforeFulfillment() > 0) {
				statement.setInt(19, product.getMaxDaysInFutureBeforeFulfillment());
			} else {
				// set to default of 90 days
				statement.setInt(19, 90);
			}

			statement.setInt(20, product.getID());

			// execute the SQL
			rowsAffected = statement.executeUpdate();

			if (rowsAffected != 1) {
				throw new UsatException("No ProductDAO Updated: Rows Affected=" + rowsAffected + "  id=" + product.getID());
			}

			product.setUpdateTimestamp(new DateTime(updatets.getTime()));

			statement.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException("ProductDAO: " + e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException("ProductDAO: " + e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

		return rowsAffected;
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws UsatException
	 */
	public PersistentProductIntf getProduct(int id) throws UsatException {
		PersistentProductIntf product = null;
		java.sql.Connection conn = null;
		Collection<PersistentProductIntf> records = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(ProductDAO.SELECT_BY_ID);

			statement.setInt(1, id);

			ResultSet rs = statement.executeQuery();

			records = ProductDAO.objectFactory(rs);

			if (records.size() == 1) {
				product = (PersistentProductIntf) records.iterator().next();
			}
		} catch (Exception e) {
			System.out.println("ProductDAO:fetchProduct " + e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}

		return product;
	}

	/**
	 * 
	 * @param productCode
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentProductIntf> getProductsByProductCode(String productCode) throws UsatException {
		java.sql.Connection conn = null;
		Collection<PersistentProductIntf> records = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(ProductDAO.SELECT_BY_PRODUCT_CODE);

			statement.setString(1, productCode);

			ResultSet rs = statement.executeQuery();

			records = ProductDAO.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("ProductDAO:fetchProductbyProductCode " + e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}

		return records;
	}

	public Collection<PersistentProductIntf> getProductsByProductCodeIncludeInactive(String productCode) throws UsatException {
		java.sql.Connection conn = null;
		Collection<PersistentProductIntf> records = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(ProductDAO.SELECT_BY_PRODUCT_CODE_INCLUDE_INACTIVE);

			statement.setString(1, productCode);

			ResultSet rs = statement.executeQuery();

			records = ProductDAO.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("ProductDAO:fetchProductbyProductCode " + e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}

		return records;
	}

	/**
	 * 
	 * @param searchCriteria
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentProductIntf> searchForProducts(String searchCriteria) throws UsatException {
		java.sql.Connection conn = null;
		Collection<PersistentProductIntf> records = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(ProductDAO.SEARCH);

			String searchStr = "%" + searchCriteria + "%";
			statement.setString(1, searchStr);
			statement.setString(2, searchStr);
			statement.setString(3, searchStr);

			ResultSet rs = statement.executeQuery();

			records = ProductDAO.objectFactory(rs);
		} catch (Exception e) {
			System.out.println("ProductDAO:searchForProducts " + e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}
		return records;
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws UsatException
	 */
	public int getCurrentInventory(int id) throws UsatException {
		java.sql.Connection conn = null;

		int currentInventory = 0;

		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement cs = conn.prepareCall(ProductDAO.RETRIEVE_INVENTORY);

			cs.setInt(1, id);

			java.sql.ResultSet rs = cs.executeQuery();

			if (rs.next()) {
				currentInventory = rs.getInt(1);
			}

			rs.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}
		return currentInventory;
	}

	/**
	 * 
	 * @param quantity
	 * @param productID
	 * @return
	 * @throws UsatException
	 */
	public synchronized int decrementInventory(int quantity, int productID) throws UsatException {
		java.sql.Connection conn = null;

		int currentInventory = 0;

		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement cs = conn.prepareCall(ProductDAO.DECREMENT_INVENTORY);

			cs.setInt(1, quantity);
			cs.setInt(2, productID);

			java.sql.ResultSet rs = cs.executeQuery();

			if (rs.next()) {
				currentInventory = rs.getInt(1);
			}

			rs.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}
		return currentInventory;
	}

}
