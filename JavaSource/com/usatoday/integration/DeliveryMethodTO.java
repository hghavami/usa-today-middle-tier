/*
 * Created on Apr 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.DeliveryMethodIntf;

/**
 * 
 */
public class DeliveryMethodTO implements DeliveryMethodIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2365317440085169765L;
	private String deliveryMethod = null;
	private String deliveryDayDelay = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#save()
	 */
	public void save() throws UsatException {

	}

	/**
	 * @return Returns the deliveryMethod.
	 */
	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	/**
	 * @param deliveryMethod
	 *            The deliveryMethod to set.
	 */
	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public String getDeliveryDayDelay() {
		return deliveryDayDelay;
	}

	public void setDeliveryDayDelay(String deliveryDayDelay) {
		this.deliveryDayDelay = deliveryDayDelay;
	}
}
