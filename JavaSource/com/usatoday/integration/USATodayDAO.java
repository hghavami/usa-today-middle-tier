package com.usatoday.integration;

import java.sql.Connection;
import java.sql.SQLException;

import com.usatoday.UsatException;

public class USATodayDAO {

	protected void cleanupConnection(Connection c) throws UsatException {
		if (c != null) {
			try {
				c.close();
			} catch (SQLException e) {
				throw new UsatException(e);
			}
		}
	}

	/**
	 * 
	 * @param inputStr
	 * @return
	 */
	protected static String escapeApostrophes(String inputStr) {
		if (inputStr != null) {
			return inputStr.replaceAll("'", "''");
		}
		return null;
	}

	protected static String STORED_PROC_LIB = "";
	protected static String TABLE_LIB = "";

	/**
	 * @return Returns the sTORED_PROC_LIB.
	 */
	public static String getSTORED_PROC_LIB() {
		return STORED_PROC_LIB;
	}

	/**
	 * @return Returns the tABLE_LIB.
	 */
	public static String getTABLE_LIB() {
		return TABLE_LIB;
	}

	/**
	 * @param stored_proc_lib
	 *            The sTORED_PROC_LIB to set.
	 */
	public static void setSTORED_PROC_LIB(String stored_proc_lib) {
		STORED_PROC_LIB = stored_proc_lib;
	}

	/**
	 * @param table_lib
	 *            The tABLE_LIB to set.
	 */
	public static void setTABLE_LIB(String table_lib) {
		TABLE_LIB = table_lib;
	}

	protected ConnectionManager connectionManager = null;

	/**
	 * This method was added only because of an issue during load testing where threads do not have access to the JNDI context in
	 * WAS. Normally, the default connection manager will be used.
	 * 
	 * @param connectionManager
	 */
	public void setConnectionManager(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

}
