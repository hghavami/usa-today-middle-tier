/*
 * Created on Apr 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.EarlyAlertIntf;

/**
 * 
 */
public class EarlyAlertTO implements EarlyAlertIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2365317440085169765L;
	private String earlyAlert = null;
	private String earlyAlertDelay = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#save()
	 */
	public void save() throws UsatException {

	}

	/**
	 * @return Returns the earlyAlert.
	 */
	public String getEarlyAlert() {
		return earlyAlert;
	}

	/**
	 * @param earlyAlert
	 *            The earlyAlert to set.
	 */
	public void setEarlyAlert(String earlyAlert) {
		this.earlyAlert = earlyAlert;
	}

	public String getEarlyAlertDelay() {
		return earlyAlertDelay;
	}

	public void setEarlyAlertDelay(String earlyAlertDelay) {
		this.earlyAlertDelay = earlyAlertDelay;
	}
}
