package com.usatoday.integration;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.gannett.usat.iconapi.client.Subscription;
import com.gannett.usat.iconapi.domainbeans.subscriberAccount.SubscriberAccount;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.PersistentSubscriberAccountIntf;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;

public class SubscriberAccountDAO extends USATodayDAO {

	// SQL -
	private static final String SELECT_FIELDS = "XTRNTUPLD.pubcode, XTRNTUPLD.transDate, XTRNTUPLD.TransRecType, XTRNTUPLD.lastname, XTRNTUPLD.firstname, XTRNTUPLD.firmname, XTRNTUPLD.UnitNum, XTRNTUPLD.HalfUnitNum, XTRNTUPLD.StreetDir, XTRNTUPLD.StreetName, "
			+ "XTRNTUPLD.StreetType, XTRNTUPLD.StreetPostDir, XTRNTUPLD.SubUnitCode, XTRNTUPLD.SubUnitNum, XTRNTUPLD.City, XTRNTUPLD.State, XTRNTUPLD.Zip, XTRNTUPLD.HomePhone, XTRNTUPLD.BusPhone, XTRNTUPLD.addlAddr1, XTRNTUPLD.CurrAcctNum, "
			+ "XTRNTUPLD.DeliveryMethod,XTRNTUPLD.BillLastName, XTRNTUPLD.BillFirstName, XTRNTUPLD.BillFirmName, XTRNTUPLD.BillAddr1, XTRNTUPLD.BillAddr2, XTRNTUPLD.BillUnitNum, XTRNTUPLD.BillHalfUnitNum, XTRNTUPLD.BillStreetDir, XTRNTUPLD.BillStreetName, "
			+ "XTRNTUPLD.BillStreetType, XTRNTUPLD.BillStreetPostDir, XTRNTUPLD.BillSubUnitNum, XTRNTUPLD.BillSubUnitCode, XTRNTUPLD.BillCity, XTRNTUPLD.BillState, XTRNTUPLD.BillZip5, XTRNTUPLD.BillBusPhone, "
			+ "XTRNTUPLD.BillHomephone, XTRNTUPLD.CurrentAccountBalance, XTRNTUPLD.Active, XTRNTUPLD.PIAExpirationDate, XTRNTUPLD.LastPayRecvd, XTRNTUPLD.LastStopCode, XTRNTUPLD.VacRestart, XTRNTUPLD.PerpCCFlag, "
			+ "XTRNTUPLD.RenewalTypeAlpha, XTRNTUPLD.RateCode, XTRNTUPLD.OneTimeBill, XTRNTUPLD.ContestCode, XTRNTUPLD.SrcOrderCode, XTRNTUPLD.PromoCode, XTRNTUPLD.NumPapers, XTRNTUPLD.PIAPermStartDate, XTRNTUPLD.CreditCardNum, XTRNTUPLD.CreditCardType, XTRNTUPLD.CreditCardExp";

	private static final String SELECT_BY_ACCOUNT_NUMBER_PUB = "{call USAT_CUSTOMER_PULL_BY_ACCOUNTNUM_PUB(?,?)}";
	private static final String SELECT_ACCOUNTS_FOR_CUSTOMER = "{call USAT_CUSTOMER_PULL_BY_EMAILPWD(?, ?)}";
	private static final String SELECT_BY_DELIVERY_HOME_PHONE_AND_ZIP = "{call USAT_CUSTOMER_PULL_BY_HOME_PHONE_ZIP(?,?)}";
	private static final String SELECT_BY_BILLING_HOME_PHONE_AND_ZIP = "{call USAT_CUSTOMER_PULL_BY_BILLING_HOME_PHONE_ZIP(?,?)}";

	// private static final String SELECT_COUNT_BY_ACCOUNT_NUM =
	// "select count(*) from XTRNTUPLD where CurrAcctNum = ? AND Active = 'Y'";
	// private static final String SELECT_COUNT_BY_DELIVERY_HOME_PHONE_AND_ZIP =
	// "select count(*) from XTRNTUPLD where HomePhone = ? AND Zip = ? AND Active = 'Y'";
	// private static final String SELECT_COUNT_BY_BILLING_HOME_PHONE_AND_ZIP =
	// "select count(*) from XTRNTUPLD where BillHomephone = ? AND Zip = ? AND Active = 'Y'";

	/**
	 * 
	 * This method generates transfer objects for each record in the result set.
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private synchronized static Collection<PersistentSubscriberAccountIntf> objectFactory(ResultSet rs) throws UsatException {
		Collection<PersistentSubscriberAccountIntf> records = new ArrayList<PersistentSubscriberAccountIntf>();
		String accountNumber = null;
		try {

			while (rs.next()) {
				// iterate over result set and build objects
				boolean active = true;
				boolean onEZPay = false;
				boolean oneTimeBill = false;
				boolean showDeliveryAlert = false;
				boolean suspended = false;
				boolean donating = false;
				boolean expiredSubscriptionAccessRenewed = false;
				String pubCode, transDate, transRecType, lastName, firstName, addlAddr1 = null;
				String firmName, unitNum, halfUnitNum, streetDir, streetName, streetType = null;
				String streetPostDir, subUnitCode, subUnitNum, city, state, zip, homePhone, deliveryMethod = null;
				String businessPhone, billingLastName, billingFirstName, billingFirmName = null;
				String billingAddress1, billingAddress2, billingUnitNum, billingHalfUnitNum, billingStreetDir = null;
				String billingStreetName, billingStreetType, billingStreetPostDir, billingSubUnitNum = null;
				String billingSubUnitCode, billingCity, billingState, billingZip, billingBusinessPhone = null;
				String billingHomePhone = null;
				Date expirationDate = null;
				Date startDate = null;
				Date lastPaymentReceived = null;
				String keycode, lastStopCode, renewalTypeAlpha, donationCode, rateCode = null;
				String creditCardNum, creditCardType, creditCardExp = null;

				String tempStr = rs.getString("Active");
				tempStr = tempStr == null ? null : tempStr.trim();
				if ("Y".equalsIgnoreCase(tempStr)) {
					active = true;
				} else {
					active = false;
				}

				// P = on EZ Pay otherwise false
				tempStr = rs.getString("PerpCCFlag");
				tempStr = tempStr == null ? null : tempStr.trim();
				if ("P".equalsIgnoreCase(tempStr)) {
					onEZPay = true;
				} else {
					onEZPay = false;
				}

				// OneTimeBill Y= true (send future bills to delivery address
				// N= false (send future bills to billing address
				// blank= (assume N per Jeff W.)
				tempStr = rs.getString("OneTimeBill");
				tempStr = tempStr == null ? null : tempStr.trim();
				if ("Y".equalsIgnoreCase(tempStr)) {
					oneTimeBill = true;
				} else {
					oneTimeBill = false;
				}

				// Value of Y means account is on vacation currently
				tempStr = rs.getString("VacRestart");
				tempStr = tempStr == null ? null : tempStr.trim();
				if ("Y".equalsIgnoreCase(tempStr)) {
					suspended = true;
				} else {
					suspended = false;
				}

				int numPapers = 1;

				try {
					numPapers = Integer.parseInt(rs.getString("NumPapers"));
				} catch (Exception exp) {
					// assume one if problem with data
					numPapers = 1;
				}

				pubCode = rs.getString("pubcode");
				if (pubCode != null) {
					pubCode = pubCode.trim();
				}

				transDate = rs.getString("transDate");
				if (transDate != null) {
					transDate = transDate.trim();
				}

				transRecType = rs.getString("TransRecType");
				if (transRecType != null) {
					transRecType = transRecType.trim();
				}

				lastName = rs.getString("lastname");
				if (lastName != null) {
					lastName = lastName.trim();
				}

				firstName = rs.getString("firstname");
				if (firstName != null) {
					firstName = firstName.trim();
				}

				firmName = rs.getString("firmname");
				if (firmName != null) {
					firmName = firmName.trim();
				}

				unitNum = rs.getString("UnitNum");
				if (unitNum != null) {
					unitNum = unitNum.trim();
				}

				halfUnitNum = rs.getString("HalfUnitNum");
				if (halfUnitNum != null) {
					halfUnitNum = halfUnitNum.trim();
				}

				streetDir = rs.getString("StreetDir");
				if (streetDir != null) {
					streetDir = streetDir.trim();
				}

				streetPostDir = rs.getString("StreetPostDir");
				if (streetPostDir != null) {
					streetPostDir = streetPostDir.trim();
				}

				streetName = rs.getString("StreetName");
				if (streetName != null) {
					streetName = streetName.trim();
				}

				streetType = rs.getString("StreetType");
				if (streetType != null) {
					streetType = streetType.trim();
				}

				subUnitCode = rs.getString("SubUnitCode");
				if (subUnitCode != null) {
					subUnitCode = subUnitCode.trim();
				}

				subUnitNum = rs.getString("SubUnitNum");
				if (subUnitNum != null) {
					subUnitNum = subUnitNum.trim();
				}

				addlAddr1 = rs.getString("addlAddr1");
				if (addlAddr1 != null) {
					addlAddr1 = addlAddr1.trim();
				}

				city = rs.getString("City");
				if (city != null) {
					city = city.trim();
				}

				state = rs.getString("State");
				if (state != null) {
					state = state.trim();
				}

				zip = rs.getString("Zip");
				if (zip != null) {
					zip = zip.trim();
				}

				homePhone = rs.getString("HomePhone");
				if (homePhone != null) {
					homePhone = homePhone.trim();
					if ("0000000000".equalsIgnoreCase(homePhone)) {
						homePhone = "";
					}
				}

				businessPhone = rs.getString("BusPhone");
				if (businessPhone != null) {
					businessPhone = businessPhone.trim();
					if ("0000000000".equalsIgnoreCase(businessPhone)) {
						businessPhone = "";
					}
				}

				accountNumber = rs.getString("CurrAcctNum");
				if (accountNumber != null) {
					accountNumber = accountNumber.trim();
				}

				deliveryMethod = rs.getString("DeliveryMethod");
				if (deliveryMethod != null) {
					deliveryMethod = deliveryMethod.trim();
				}

				billingLastName = rs.getString("BillLastName");
				if (billingLastName != null) {
					billingLastName = billingLastName.trim();
				}

				billingFirstName = rs.getString("BillFirstName");
				if (billingFirstName != null) {
					billingFirstName = billingFirstName.trim();
				}

				billingFirmName = rs.getString("BillFirmName");
				if (billingFirmName != null) {
					billingFirmName = billingFirmName.trim();
				}

				billingAddress1 = rs.getString("BillAddr1");
				if (billingAddress1 != null) {
					billingAddress1 = billingAddress1.trim();
				}

				billingAddress2 = rs.getString("BillAddr2");
				if (billingAddress2 != null) {
					billingAddress2 = billingAddress2.trim();
				}

				billingUnitNum = rs.getString("BillUnitNum");
				if (billingUnitNum != null) {
					billingUnitNum = billingUnitNum.trim();
				}

				billingHalfUnitNum = rs.getString("BillHalfUnitNum");
				if (billingHalfUnitNum != null) {
					billingHalfUnitNum = billingHalfUnitNum.trim();
				}

				billingStreetName = rs.getString("BillStreetName");
				if (billingStreetName != null) {
					billingStreetName = billingStreetName.trim();
				}

				billingStreetDir = rs.getString("BillStreetDir");
				if (billingStreetDir != null) {
					billingStreetDir = billingStreetDir.trim();
				}

				billingStreetType = rs.getString("BillStreetType");
				if (billingStreetType != null) {
					billingStreetType = billingStreetType.trim();
				}

				billingStreetPostDir = rs.getString("BillStreetPostDir");
				if (billingStreetPostDir != null) {
					billingStreetPostDir = billingStreetPostDir.trim();
				}

				billingSubUnitNum = rs.getString("BillSubUnitNum");
				if (billingSubUnitNum != null) {
					billingSubUnitNum = billingSubUnitNum.trim();
				}

				billingSubUnitCode = rs.getString("BillSubUnitCode");
				if (billingSubUnitCode != null) {
					billingSubUnitCode = billingSubUnitCode.trim();
				}

				billingCity = rs.getString("BillCity");
				if (billingCity != null) {
					billingCity = billingCity.trim();
				}

				billingState = rs.getString("BillState");
				if (billingState != null) {
					billingState = billingState.trim();
				}

				billingZip = rs.getString("BillZip5");
				if (billingZip != null) {
					billingZip = billingZip.trim();
				}

				billingBusinessPhone = rs.getString("BillBusPhone");
				if (billingBusinessPhone != null) {
					billingBusinessPhone = billingBusinessPhone.trim();
					if ("0000000000".equalsIgnoreCase(billingBusinessPhone)) {
						billingBusinessPhone = "";
					}
				}

				billingHomePhone = rs.getString("BillHomePhone");
				if (billingHomePhone != null) {
					billingHomePhone = billingHomePhone.trim();
					if ("0000000000".equalsIgnoreCase(billingHomePhone)) {
						billingHomePhone = "";
					}
				}

				Double accountBalance = null;
				tempStr = rs.getString("CurrentAccountBalance");
				if (tempStr != null) {
					accountBalance = SubscriberAccountDAO.convert400AmountValueToDouble(tempStr.trim(), 5);
				}

				tempStr = rs.getString("PIAExpirationDate");
				if (tempStr != null) {
					expirationDate = SubscriberAccountDAO.convert400DateStringCYYMMDDToDate(tempStr.trim());
				}

				tempStr = rs.getString("PIAPermStartDate");
				if (tempStr != null) {
					startDate = SubscriberAccountDAO.convert400DateStringCYYMMDDToDate(tempStr.trim());
				}

				tempStr = rs.getString("LastPayRecvd");
				if (tempStr != null) {
					lastPaymentReceived = SubscriberAccountDAO.convert400DateStringCYYMMDDToDate(tempStr.trim());
				}

				keycode = rs.getString("SrcOrderCode") + rs.getString("PromoCode") + rs.getString("ContestCode");

				lastStopCode = rs.getString("LastStopCode");
				if (lastStopCode != null) {
					lastStopCode = lastStopCode.trim();
					if ("VE".equalsIgnoreCase(lastStopCode)) {
						donating = true;
					}
					donationCode = lastStopCode;
				} else {
					donationCode = "";
				}

				renewalTypeAlpha = rs.getString("RenewalTypeAlpha");
				if (renewalTypeAlpha != null) {
					renewalTypeAlpha = renewalTypeAlpha.trim();
				}

				rateCode = rs.getString("RateCode");
				if (rateCode != null) {
					rateCode = rateCode.trim();
				}

				creditCardNum = rs.getString("CreditCardNum");
				if (creditCardNum != null) {
					creditCardNum = creditCardNum.trim();
				}
				creditCardType = rs.getString("creditCardType");
				if (creditCardType != null) {
					creditCardType = creditCardType.trim();
				}

				creditCardExp = rs.getString("creditCardExp");
				if (creditCardExp != null) {
					creditCardExp = creditCardExp.trim();
				}

				// create the Transfer Object
				SubscriberAccountTO account = new SubscriberAccountTO(pubCode, transDate, transRecType, lastName, firstName,
						firmName, halfUnitNum, streetDir, streetName, streetType, streetPostDir, subUnitCode, subUnitNum, unitNum,
						addlAddr1, city, state, zip, homePhone, businessPhone, accountNumber, deliveryMethod, billingLastName,
						billingFirstName, billingFirmName, billingAddress1, billingAddress2, billingUnitNum, billingHalfUnitNum,
						billingStreetDir, billingStreetName, billingStreetType, billingStreetPostDir, billingSubUnitNum,
						billingSubUnitCode, billingCity, billingState, billingZip, accountBalance, expirationDate, startDate,
						active, lastPaymentReceived, lastStopCode, suspended, onEZPay, donating, donationCode, renewalTypeAlpha,
						rateCode, keycode, oneTimeBill, showDeliveryAlert, numPapers, billingHomePhone, billingBusinessPhone,
						creditCardNum, creditCardType, creditCardExp, expiredSubscriptionAccessRenewed, "DO");

				records.add(account);
			}
		} catch (Exception e) {
			System.out.println("SubscriberAccountDAO::objectFactory() - Failed to create Subscriber Account for accountNumber: "
					+ accountNumber + "  " + e.getMessage());
		}

		return records;
	}

	// private static final String SELECT_COUNT_BY_ACCOUNT_NUM =
	// "select count(*) from XTRNTUPLD where CurrAcctNum = ? AND Active = 'Y'";
	// private static final String SELECT_COUNT_BY_DELIVERY_HOME_PHONE_AND_ZIP =
	// "select count(*) from XTRNTUPLD where HomePhone = ? AND Zip = ? AND Active = 'Y'";
	// private static final String SELECT_COUNT_BY_BILLING_HOME_PHONE_AND_ZIP =
	// "select count(*) from XTRNTUPLD where BillHomephone = ? AND Zip = ? AND Active = 'Y'";

	/**
	 * 
	 * This method generates transfer objects for each record in the result set.
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private synchronized static Collection<PersistentSubscriberAccountIntf> objectFactoryGenesys(
			Collection<SubscriberAccount> genesysAccounts) throws UsatException {
		Collection<PersistentSubscriberAccountIntf> records = new ArrayList<PersistentSubscriberAccountIntf>();
		String accountNumber = null;
		try {

			for (SubscriberAccount accountBean : genesysAccounts) {
//				System.out.println("Account: " + accountBean.toString());
				// iterate over result set and build objects
				boolean active = true;
				boolean onEZPay = false;
				boolean oneTimeBill = false;
				boolean showDeliveryAlert = false;
				boolean suspended = false;
				boolean donating = false;
				boolean expiredSubscriptionAccessRenewed = false;
				String pubCode, transDate, transRecType, lastName, firstName, addlAddr1 = null;
				String firmName, unitNum, halfUnitNum, streetDir, streetName, streetType = null;
				String streetPostDir, subUnitCode, subUnitNum, city, state, zip, homePhone, deliveryMethod = null;
				String businessPhone, billingLastName, billingFirstName, billingFirmName = null;
				String billingAddress1, billingAddress2, billingUnitNum, billingHalfUnitNum, billingStreetDir = null;
				String billingStreetName, billingStreetType, billingStreetPostDir, billingSubUnitNum = null;
				String billingSubUnitCode, billingCity, billingState, billingZip, billingBusinessPhone = null;
				String billingHomePhone = null;
				Date expirationDate = null;
				Date startDate = null;
				Date lastPaymentReceived = null;
				String keycode, lastStopCode, renewalTypeAlpha, donationCode, rateCode = null;
				String creditCardNum, creditCardType, creditCardExp, fod = null;

				String tempStr = "";

				// P = on EZ Pay otherwise false
				tempStr = accountBean.getEz_pay();
				tempStr = tempStr == null ? null : tempStr.trim();
				if ("true".equalsIgnoreCase(tempStr)) {
					onEZPay = true;
				} else {
					onEZPay = false;
				}

				// OneTimeBill Y= true (send future bills to delivery address
				// N= false (send future bills to billing address
				// blank= (assume N per Jeff W.)
				tempStr = "N";
				tempStr = tempStr == null ? null : tempStr.trim();
				if ("Y".equalsIgnoreCase(tempStr)) {
					oneTimeBill = true;
				} else {
					oneTimeBill = false;
				}

				// Value of Y means account is on vacation currently
				tempStr = accountBean.getVacation_flag();
				tempStr = tempStr == null ? null : tempStr.trim();
				if ("Y".equalsIgnoreCase(tempStr)) {
					suspended = true;
				} else {
					suspended = false;
				}

				int numPapers = 1;

				numPapers = 1;
				try {
					if (accountBean.getNumber_of_copies() != null) {
						numPapers = (new Integer(accountBean.getNumber_of_copies()).intValue());
					}
				} catch (Exception exp) {
					// assume one if problem with data
					numPapers = 1;
				}

				pubCode = "";
				if (accountBean.getPub_code() == null) {
					pubCode = "UT";
				} else {
					pubCode = accountBean.getPub_code().trim();
				}

				java.util.Calendar cal = java.util.Calendar.getInstance();
				java.util.Date date = cal.getTime();
				transDate = new java.text.SimpleDateFormat("yyyyMMdd").format(date);
				if (transDate != null) {
					transDate = transDate.trim();
				}

				transRecType = "";
				if (accountBean.getSubscriber_status() != null) {
					transRecType = accountBean.getSubscriber_status().trim();
				}

				lastName = "";
				if (accountBean.getDelivery_last_name() != null) {
					lastName = accountBean.getDelivery_last_name().trim().toUpperCase();
				}

				firstName = "";
				if (accountBean.getDelivery_first_name() != null) {
					firstName = accountBean.getDelivery_first_name().trim().toUpperCase();
				}

				firmName = "";
				if (accountBean.getFirm_name() != null) {
					firmName = firmName.trim().toUpperCase();
				}

				unitNum = "";
				// if (accountBean.getDelivery_address_subunit() != null) {
				// unitNum = accountBean.getDelivery_address_subunit().trim().toUpperCase();
				// }

				halfUnitNum = "";
				if (halfUnitNum != null) {
					halfUnitNum = halfUnitNum.trim();
				}

				streetDir = "";
				if (streetDir != null) {
					streetDir = streetDir.trim();
				}

				streetPostDir = "";
				if (streetPostDir != null) {
					streetPostDir = streetPostDir.trim();
				}

				streetName = "";
				if (accountBean.getDelivery_address_line_1() != null) {
					streetName = accountBean.getDelivery_address_line_1().trim().toUpperCase();
				}

				streetType = "";
				if (streetType != null) {
					streetType = streetType.trim();
				}

				subUnitCode = "";
				if (subUnitCode != null) {
					subUnitCode = subUnitCode.trim();
				}

				subUnitNum = "";
				if (accountBean.getDelivery_address_subunit() != null) {
					subUnitNum = accountBean.getDelivery_address_subunit().trim().toUpperCase();
				}

				addlAddr1 = "";
				if (accountBean.getDelivery_address_line_2() != null) {
					addlAddr1 = accountBean.getDelivery_address_line_2().trim().toUpperCase();
				}

				city = "";
				if (accountBean.getDelivery_address_city() != null) {
					city = accountBean.getDelivery_address_city().trim().toUpperCase();
				}

				state = "";
				if (accountBean.getDelivery_address_state() != null) {
					state = accountBean.getDelivery_address_state().trim().toUpperCase();
				}

				zip = accountBean.getDelivery_address_zip_code();
				if (zip != null) {
					zip = accountBean.getDelivery_address_zip_code().trim();
				}

				homePhone = "";
				if (accountBean.getHome_phone() != null) {
					homePhone = accountBean.getHome_phone().trim();
					if ("0000000000".equalsIgnoreCase(homePhone)) {
						homePhone = "";
					}
				}

				businessPhone = "";
				if (accountBean.getWork_phone() != null) {
					businessPhone = accountBean.getWork_phone().trim();
					if ("0000000000".equalsIgnoreCase(businessPhone)) {
						businessPhone = "";
					}
				}

				accountNumber = "";
				if (accountBean.getAccount_number() != null) {
					accountNumber = accountBean.getAccount_number().trim();
				}

				deliveryMethod = "C";
				if (accountBean.getDelivery_method() != null) {
					deliveryMethod = accountBean.getDelivery_method().trim();
				}

				billingLastName = "";
				if (accountBean.getBilling_last_name() != null) {
					billingLastName = accountBean.getBilling_last_name().trim().toUpperCase();
				}

				billingFirstName = "";
				if (accountBean.getBilling_first_name() != null) {
					billingFirstName = accountBean.getBilling_first_name().trim().toUpperCase();
				}

				billingFirmName = "";
				if (accountBean.getBilling_firm_name() != null) {
					billingFirmName = accountBean.getBilling_firm_name().trim().toUpperCase();
				}

				billingAddress1 = "";
				if (accountBean.getBilling_address_line_2() != null) {
					billingAddress1 = accountBean.getBilling_address_line_2().trim().toUpperCase();
				}

				billingAddress2 = "";
				if (billingAddress2 != null) {
					billingAddress2 = billingAddress2.trim();
				}

				billingUnitNum = "";
				if (billingUnitNum != null) {
					billingUnitNum = billingUnitNum.trim();
				}

				billingHalfUnitNum = "";
				if (billingHalfUnitNum != null) {
					billingHalfUnitNum = billingHalfUnitNum.trim();
				}

				billingStreetName = "";
				if (accountBean.getBilling_address_line_1() != null) {
					billingStreetName = accountBean.getBilling_address_line_1().trim().toUpperCase();
				}

				billingStreetDir = "";
				if (billingStreetDir != null) {
					billingStreetDir = billingStreetDir.trim();
				}

				billingStreetType = "";
				if (billingStreetType != null) {
					billingStreetType = billingStreetType.trim();
				}

				billingStreetPostDir = "";
				if (billingStreetPostDir != null) {
					billingStreetPostDir = billingStreetPostDir.trim();
				}

				billingSubUnitNum = "";
				if (billingSubUnitNum != null) {
					billingSubUnitNum = billingSubUnitNum.trim();
				}

				billingSubUnitCode = "";
				if (billingSubUnitCode != null) {
					billingSubUnitCode = billingSubUnitCode.trim();
				}

				billingCity = "";
				if (accountBean.getBilling_address_city() != null) {
					billingCity = accountBean.getBilling_address_city().trim().toUpperCase();
				}

				billingState = "";
				if (accountBean.getBilling_address_state() != null) {
					billingState = accountBean.getBilling_address_state().toUpperCase();
				}

				billingZip = "";
				if (accountBean.getBilling_address_zip_code() != null) {
					billingZip = accountBean.getBilling_address_zip_code().trim();
				}

				billingBusinessPhone = "";
				// if (accountBean.getBilling_phone() != null) {
				// billingBusinessPhone = accountBean.getBilling_phone().trim();
				// if ("0000000000".equalsIgnoreCase(billingBusinessPhone)) {
				// billingBusinessPhone = "";
				// }
				// }

				billingHomePhone = "";
				if (accountBean.getBilling_phone() != null) {
					billingHomePhone = accountBean.getBilling_phone().trim();
					if ("0000000000".equalsIgnoreCase(billingHomePhone)) {
						billingHomePhone = "";
					}
				}

				Double accountBalance = 0.0;
				tempStr = accountBean.getPending_expiration_date();
				if (tempStr != null) {
					expirationDate = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(tempStr).toDate();
				}

				tempStr = accountBean.getStart_date();
				if (tempStr != null && !tempStr.equals("0")) {
					startDate = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(tempStr).toDate();
				}

				tempStr = accountBean.getLast_payment_date();
				if (tempStr != null && !tempStr.equals("0")) {
					lastPaymentReceived = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(tempStr).toDate();
				}
				keycode = accountBean.getSource_code() + accountBean.getPromo_code() + accountBean.getContest_code();

				lastStopCode = "";
				if (accountBean.getLast_stop_code() != null) {
					lastStopCode = accountBean.getLast_stop_code().trim();
					if ("VE".equalsIgnoreCase(lastStopCode)) {
						donating = true;
					}
					donationCode = lastStopCode;
				} else {
					donationCode = "";
				}

				renewalTypeAlpha = "";
				if (accountBean.getCurrent_pia_type() != null) {
					renewalTypeAlpha = accountBean.getCurrent_pia_type().trim();
				}

				rateCode = "";
				if (accountBean.getCurrent_rate_code() != null) {
					rateCode = accountBean.getCurrent_rate_code().trim();
				}

				creditCardNum = "";
				if (accountBean.getCredit_card_last_four() != null) {
					creditCardNum = accountBean.getCredit_card_last_four().trim();
				}

				creditCardType = "";
				if (accountBean.getCredit_card_type() != null) {
					creditCardType = accountBean.getCredit_card_type().trim();
				}

				creditCardExp = "";
				if (accountBean.getCredit_card_expiration_date() != null
						&& !accountBean.getCredit_card_expiration_date().equals("0")) {
					creditCardExp = accountBean.getCredit_card_expiration_date().trim();
				}

				fod = "";
				if (accountBean.getFod_code() != null) {
					fod = accountBean.getFod_code().trim();
				}
				// create the Transfer Object
				SubscriberAccountTO account = new SubscriberAccountTO(pubCode, transDate, transRecType, lastName, firstName,
						firmName, halfUnitNum, streetDir, streetName, streetType, streetPostDir, subUnitCode, subUnitNum, unitNum,
						addlAddr1, city, state, zip, homePhone, businessPhone, accountNumber, deliveryMethod, billingLastName,
						billingFirstName, billingFirmName, billingAddress1, billingAddress2, billingUnitNum, billingHalfUnitNum,
						billingStreetDir, billingStreetName, billingStreetType, billingStreetPostDir, billingSubUnitNum,
						billingSubUnitCode, billingCity, billingState, billingZip, accountBalance, expirationDate, startDate,
						active, lastPaymentReceived, lastStopCode, suspended, onEZPay, donating, donationCode, renewalTypeAlpha,
						rateCode, keycode, oneTimeBill, showDeliveryAlert, numPapers, billingHomePhone, billingBusinessPhone,
						creditCardNum, creditCardType, creditCardExp, expiredSubscriptionAccessRenewed, fod);

				records.add(account);
			}
		} catch (Exception e) {
			System.out.println("SubscriberAccountDAO::objectFactory() - Failed to create Subscriber Account for accountNumber: "
					+ accountNumber + "  " + e.getMessage());
		}

		return records;
	}

	// data converters
	/**
	 * Converts a database String representation of money into a Double
	 */
	private static Double convert400AmountValueToDouble(String value, int storedPrecision) {
		if (value == null) {
			return new Double("0.00");
		}

		if (storedPrecision >= value.length()) {
			return new Double(value);
		}

		// Sample stored in DB in 11.5 format. 00009925000 = 99.25

		Double amount = new Double("0.00");
		try {
			int dollarIndex = value.length() - storedPrecision;

			String dollars = value.substring(0, dollarIndex);
			// truncate any remaining cents
			String cents = value.substring(dollarIndex, dollarIndex + 2);

			amount = new Double(dollars + "." + cents);
		} catch (Exception e) {
			System.out.println("Invalid Dollar Value: " + value + " Precision: " + storedPrecision);
		}
		return amount;
	}

	/**
	 * The fields PIAExpirationDate, PIAPermStartDate, and LastPayRecvd are stored in this format
	 * 
	 * @param value
	 * @return
	 */
	private static Date convert400DateStringCYYMMDDToDate(String value) {

		if (value == null || value.trim().length() != 7 || value.equalsIgnoreCase("0000000")) {
			return null;
		}

		Date date = null;
		try {
			String dateStr = value.trim();

			// StringBuffer formattedDate = new StringBuffer("");

			String century = (dateStr.substring(0, 1));

			String year = null;
			String sub_year = (dateStr.substring(1, 3));

			String month = (dateStr.substring(3, 5));

			String day = (dateStr.substring(5, 7));

			if (Integer.parseInt(century) == 0) {
				year = "19" + sub_year;
			} else {
				year = "20" + sub_year;
			}

			DateTime dt = new DateTime(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day), 0, 0, 0, 0);

			date = dt.toDate();

		} catch (Exception e) {
			System.out.println("SubscriberAccountDAO::convert400DateStringCYYMMDDToDate() Failed to convert stored date to Date: "
					+ value);
		}
		return date;
	}

	/**
	 * 
	 * This method is used to query for a specific account
	 * 
	 * @param accountNumber
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentSubscriberAccountIntf> getGenesysAccountByAccountNumber(String accountNumber, String pubCode)
			throws UsatException {
		Subscription sApi = new Subscription();
		Collection<PersistentSubscriberAccountIntf> accounts = null;

		try {
			String responseJson = sApi.getAccountDataForAccountNumber(accountNumber, pubCode);

			// convert raw json to objects (could have just called the helper method tht returns objects instead of json
			Collection<SubscriberAccount> genesysAccounts = Subscription.jsonToSubscriberAccount(responseJson);
			accounts = SubscriberAccountDAO.objectFactoryGenesys(genesysAccounts);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return accounts;
	}

	/**
	 * 
	 * This method is used to query for a specific account
	 * 
	 * @param accountNumber
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentSubscriberAccountIntf> getAccountByAccountNumberAndPub(String accountNumber, String pubCode)
			throws UsatException {
		Collection<PersistentSubscriberAccountIntf> accounts = null;
		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement statement = conn.prepareCall(SubscriberAccountDAO.SELECT_BY_ACCOUNT_NUMBER_PUB);
			statement.setString(1, accountNumber);
			if (pubCode != null) {
				statement.setString(2, pubCode);
			} else {
				statement.setNull(2, Types.CHAR);
			}

			ResultSet rs = statement.executeQuery();

			accounts = SubscriberAccountDAO.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("SubscriberAccountDAO : getAccountByAccountNumberAndPub() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return accounts;
	}

	/**
	 * This query is less efficient than calling getAccountForCustomer(). Use it if possible.
	 * 
	 * @param accountNumbers
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentSubscriberAccountIntf> getAccountByAccountNumbers(Collection<String> accountNumbers)
			throws UsatException {
		if (accountNumbers == null || accountNumbers.size() == 0) {
			return null;
		}
		Collection<PersistentSubscriberAccountIntf> accounts = null;
		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			StringBuffer sql = new StringBuffer("select " + SubscriberAccountDAO.SELECT_FIELDS
					+ " from XTRNTUPLD where CurrAcctNum in (");
			Iterator<String> itr = accountNumbers.iterator();

			while (itr.hasNext()) {
				String acctNumber = itr.next();
				sql.append(" '").append(acctNumber).append("'");
				if (itr.hasNext()) {
					sql.append(",");
				}
			}
			sql.append(")");

			java.sql.PreparedStatement statement = conn.prepareStatement(sql.toString());

			ResultSet rs = statement.executeQuery();

			accounts = SubscriberAccountDAO.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("SubscriberAccountDAO : getAccountByAccountNumbers() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return accounts;
	}

	/**
	 * 
	 * @param accountNumbers
	 *            A comma delimited lis of account numbers
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentSubscriberAccountIntf> getGenesysAccountForCustomer(String emailAddress) throws UsatException {

		Subscription sApi = new Subscription();
		Collection<PersistentSubscriberAccountIntf> accounts = null;

		try {
			String responseJson = sApi.getAccountDataForEmail(emailAddress);

			// convert raw json to objects (could have just called the helper method tht returns objects instead of json
			Collection<SubscriberAccount> genesysAccounts = Subscription.jsonToSubscriberAccount(responseJson);
			accounts = SubscriberAccountDAO.objectFactoryGenesys(genesysAccounts);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return accounts;
	}

	/**
	 * 
	 * @param accountNumbers
	 *            A comma delimited lis of account numbers
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentSubscriberAccountIntf> getAccountForCustomer(String emailAddress, String password)
			throws UsatException {
		java.sql.Connection conn = null;
		Collection<PersistentSubscriberAccountIntf> accounts = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement statement = conn.prepareCall(SubscriberAccountDAO.SELECT_ACCOUNTS_FOR_CUSTOMER);

			statement.setString(1, emailAddress);
			statement.setString(2, password);

			ResultSet rs = statement.executeQuery();

			accounts = SubscriberAccountDAO.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("SubscriberAccountDAO : getAccountByAccountNumber() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return accounts;
	}

	/**
	 * 
	 * @param phoneNumber
	 * @param zip
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentSubscriberAccountIntf> getGenesysAccountByDeliveryPhoneNumberAndZip(String phoneNumber, String zip)
			throws UsatException {

		Subscription sApi = new Subscription();
		Collection<PersistentSubscriberAccountIntf> accounts = null;

		try {
			String responseJson = sApi.getAccountDataForHomePhoneZip(phoneNumber, zip);

			// convert raw json to objects (could have just called the helper method tht returns objects instead of json
			Collection<SubscriberAccount> genesysAccounts = Subscription.jsonToSubscriberAccount(responseJson);
			accounts = SubscriberAccountDAO.objectFactoryGenesys(genesysAccounts);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return accounts;
	}

	/**
	 * 
	 * @param phoneNumber
	 * @param zip
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentSubscriberAccountIntf> getAccountByDeliveryPhoneNumberAndZip(String phoneNumber, String zip)
			throws UsatException {
		java.sql.Connection conn = null;
		Collection<PersistentSubscriberAccountIntf> accounts = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement statement = conn.prepareCall(SubscriberAccountDAO.SELECT_BY_DELIVERY_HOME_PHONE_AND_ZIP);

			statement.setString(1, phoneNumber);
			statement.setString(2, zip);

			ResultSet rs = statement.executeQuery();

			accounts = SubscriberAccountDAO.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("SubscriberAccountDAO : getAccountByDeliveryPhoneNumberAndZip() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return accounts;
	}

	/**
	 * 
	 * @param phoneNumber
	 * @param zip
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentSubscriberAccountIntf> getGenesysAccountByBillingPhoneNumberAndZip(String phoneNumber, String zip)
			throws UsatException {
		Subscription sApi = new Subscription();
		Collection<PersistentSubscriberAccountIntf> accounts = null;

		try {
			String responseJson = sApi.getAccountDataForBillingPhoneZip(phoneNumber, zip);

			// convert raw json to objects (could have just called the helper method tht returns objects instead of json
			Collection<SubscriberAccount> genesysAccounts = Subscription.jsonToSubscriberAccount(responseJson);
			accounts = SubscriberAccountDAO.objectFactoryGenesys(genesysAccounts);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return accounts;

	}

	/**
	 * 
	 * @param phoneNumber
	 * @param zip
	 * @return
	 * @throws UsatException
	 */
	public Collection<PersistentSubscriberAccountIntf> getAccountByBillingPhoneNumberAndZip(String phoneNumber, String zip)
			throws UsatException {
		java.sql.Connection conn = null;
		Collection<PersistentSubscriberAccountIntf> accounts = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement statement = conn.prepareCall(SubscriberAccountDAO.SELECT_BY_BILLING_HOME_PHONE_AND_ZIP);

			statement.setString(1, phoneNumber);
			statement.setString(2, zip);

			ResultSet rs = statement.executeQuery();

			accounts = SubscriberAccountDAO.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("SubscriberAccountDAO : getAccountByBillingPhoneNumberAndZip() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return accounts;
	}

}
