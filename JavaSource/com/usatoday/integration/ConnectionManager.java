package com.usatoday.integration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400JDBCDriver;
import com.usatoday.util.constants.USATApplicationPropertyFileLoader;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * ConnectionManager
 * 
 * This class handles getting connections to the database through JNDI.
 * 
 */
public class ConnectionManager {

	/**
	 * Setting this to false will disable the connection pool - should only be used for testing the application
	 */
	protected static boolean USING_JNDI = false;
	protected static String UID = null;
	protected static String PWD = null;
	protected static String iseriesUID = null;
	protected static String iseriesPWD = null;
	protected static String jdbcDriverName = "net.sourceforge.jtds.jdbc.Driver";
	protected static String jdbcIseriesDriverName = "com.ibm.as400.access.AS400JDBCDriver";
	protected static String jdbcConnectionString = "";
	protected static String jdbcPCIConnectionString = "";
	protected static String jdbcIseriesConnectionString = "usat.default.iseries.jdbcdsnname: jdbc:as400:usat-mrunifd4";
	// JNDI parms for server 6.0
	// protected static String jndiDataSourceName = "java:comp/env/esub";
	// protected static String jndiIseriesDataSourceName = "java:comp/env/subdelivery";
	// JNDI parms for server 6.1 and higher
	protected static String jndiDataSourceName = "jdbc/esub";
	protected static String jndiEsubPCIDataSourceName = "jdbc/esubpci";
	protected static String jndiIseriesDataSourceName = "jdbc/subdelivery";
	protected static String jndiIseriesSecureDataSourceName = "jdbc/subdeliverysecure";

	private static ConnectionManager instance = null;

	DataSource subscriptionPortalDataSource = null;
	DataSource subscriptionPortalPCIDataSource = null;
	DataSource subscriptionPortalIseriesDataSource = null;
	DataSource subscriptionPortalIseriesSecureDataSource = null;

	public static synchronized ConnectionManager getInstance() {
		if (instance == null) {
			instance = new ConnectionManager();
		}
		return instance;
	}

	private ConnectionManager() {
		Exception originalEx = null;
		try {
			// read properties
			try {
				String tempStr = "";
				Properties config = new USATApplicationPropertyFileLoader().loadapplicationProperties();

				tempStr = config.getProperty("com.usatoday.db.connectionMode.useJNDI");
				if (tempStr != null && "false".equalsIgnoreCase(tempStr)) {
					ConnectionManager.USING_JNDI = false;
				} else {
					ConnectionManager.USING_JNDI = true;
				}

				tempStr = config.getProperty("usat.jdbc.driver.jdbcdriver");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.jdbcDriverName = tempStr.trim();
				}

				tempStr = config.getProperty("usat.jdbc.iseries.driver.jdbcdriver");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.jdbcIseriesDriverName = tempStr.trim();
				}

				tempStr = config.getProperty("usat.default.jdbc.schema.LIBRARY");
				if (tempStr != null && tempStr.trim().length() > 0) {
					USATodayDAO.setTABLE_LIB(tempStr.trim());
				}

				tempStr = config.getProperty("usat.default.jdbc.storedProc.LIBRARY");
				if (tempStr != null && tempStr.trim().length() > 0) {
					USATodayDAO.setSTORED_PROC_LIB(tempStr.trim());
				}

				tempStr = config.getProperty("usat.naming.datasource.name");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.jndiDataSourceName = tempStr.trim();
				}

				tempStr = config.getProperty("usat.naming.datasource.name.pci");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.jndiEsubPCIDataSourceName = tempStr.trim();
				}

				tempStr = config.getProperty("usat.naming.iseries.datasource.name");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.jndiIseriesDataSourceName = tempStr.trim();
				}

				tempStr = config.getProperty("usat.naming.iseries.secure.datasource.name");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.jndiIseriesSecureDataSourceName = tempStr.trim();
				}

				tempStr = config.getProperty("usat.default.jdbcdsnname");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.jdbcConnectionString = tempStr.trim();
				}

				tempStr = config.getProperty("usat.default.iseries.jdbcdsnname");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.jdbcIseriesConnectionString = tempStr.trim();
				}

				tempStr = config.getProperty("usat.default.jdbc.UID");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.UID = tempStr.trim();
				}

				tempStr = config.getProperty("usat.default.jdbc.PWD");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.PWD = tempStr.trim();
				}

				tempStr = config.getProperty("usat.default.jdbc.iseriesUID");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.iseriesUID = tempStr.trim();
				}

				tempStr = config.getProperty("usat.default.jdbc.iseriesPWD");
				if (tempStr != null && tempStr.trim().length() > 0) {
					ConnectionManager.iseriesPWD = tempStr.trim();
				}

			} catch (Exception e) {
				System.err.println("Error configuring Connection Manager- defaulting to JNDI");
				ConnectionManager.USING_JNDI = true;
			}

			if (ConnectionManager.USING_JNDI) {
				try {
					System.out.println("JNDI Initialization Starting. ");
					this.initJNDI();
					this.initIseriesJNDI();
					this.initIseriesSecureJNDI();
					System.out.println("JNDI Initialization complete. ");
				} catch (Exception e) {
					System.out.println("FAILED TO CONNECT VIA JNDI! " + e.getMessage());
					originalEx = e;
					System.out.println("Setting up JDBC Connection Mode");
					// switch modes
					ConnectionManager.USING_JNDI = false;
					this.initJDBC();
					this.initIseriesJDBC();
				}
				try {
					this.initPCI_JNDI();
				} catch (Exception e) {
					System.out.println("failed to initialize esubpci: " + e.getMessage());
					System.out.println("System will probably attempt to write to esub instance");
					e.printStackTrace();
				}
			} else {
				System.out.println("JDBC Initialization starting. ");
				this.initJDBC();
				this.initIseriesJDBC();
				System.out.println("JDBC Initialization complete. ");
			}
		} catch (Exception e) {
			e.printStackTrace();

			// throw the original issue if exists
			if (originalEx != null) {
				e = originalEx;
			}
			// this is fatal so thow an Error
			throw new Error(e);
		}
	}

	private void initJNDI() throws Exception {
		Context ctx = new InitialContext();
		subscriptionPortalDataSource = (DataSource) ctx.lookup(ConnectionManager.jndiDataSourceName);
	}

	private void initPCI_JNDI() throws Exception {
		Context ctx = new InitialContext();
		subscriptionPortalPCIDataSource = (DataSource) ctx.lookup(ConnectionManager.jndiEsubPCIDataSourceName);
	}

	private void initIseriesJNDI() throws Exception {
		Context ctx = new InitialContext();
		subscriptionPortalIseriesDataSource = (DataSource) ctx.lookup(ConnectionManager.jndiIseriesDataSourceName);
	}

	private void initIseriesSecureJNDI() throws Exception {
		Context ctx = new InitialContext();
		subscriptionPortalIseriesSecureDataSource = (DataSource) ctx.lookup(ConnectionManager.jndiIseriesSecureDataSourceName);
	}

	private void initJDBC() throws Exception {
		Class.forName(ConnectionManager.jdbcDriverName);
	}

	private void initIseriesJDBC() throws Exception {
		Class.forName(ConnectionManager.jdbcIseriesDriverName);
		AS400JDBCDriver driver = new AS400JDBCDriver();
		driver.hashCode();
		AS400 as400 = new AS400();
		as400.setGuiAvailable(false);
		DriverManager.registerDriver(new AS400JDBCDriver());
	}

	public Connection getConnection() {
		Connection connection = null;
		try {
			if (USING_JNDI) {
				try {
					connection = subscriptionPortalDataSource.getConnection();
				} catch (SQLException sqle) {
					try {
						// as a backup try the user id and password in config file
						if (ConnectionManager.UID != null && ConnectionManager.PWD != null) {
							connection = subscriptionPortalDataSource.getConnection(ConnectionManager.UID, ConnectionManager.PWD);
						} else {
							throw sqle;
						}
					} catch (SQLException f2) {
						// throw original exception
						throw sqle;
					}
				}
			} else {
				connection = DriverManager.getConnection(ConnectionManager.jdbcConnectionString);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Error(e);
		}
		return connection;
	}

	public Connection getPCIConnection() {
		Connection connection = null;
		try {
			if (USING_JNDI) {
				try {
					connection = subscriptionPortalPCIDataSource.getConnection();
				} catch (SQLException sqle) {
					try {
						// as a backup try the user id and password in config file
						if (ConnectionManager.UID != null && ConnectionManager.PWD != null) {
							connection = subscriptionPortalPCIDataSource
									.getConnection(ConnectionManager.UID, ConnectionManager.PWD);
						} else {
							throw sqle;
						}
					} catch (SQLException f2) {
						// throw original exception
						throw sqle;
					}
				}
			} else {
				connection = DriverManager.getConnection(ConnectionManager.jdbcConnectionString);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Error(e);
		}
		return connection;
	}

	public static void resetManager() {
		ConnectionManager.instance = null;
		ConnectionManager.instance = new ConnectionManager();
	}

	public Connection getIseriesConnection() {
		Connection connection = null;
		try {
			if (USING_JNDI) {
				try {
					connection = subscriptionPortalIseriesDataSource.getConnection();
					if (UsaTodayConstants.debug) {
						System.out.println("Got a JNDI Connection.");
					}
				} catch (SQLException sqle) {
					try {
						// as a backup try the user id and password in config file
						if (ConnectionManager.iseriesUID != null && ConnectionManager.iseriesPWD != null) {
							connection = subscriptionPortalIseriesDataSource.getConnection(ConnectionManager.iseriesUID,
									ConnectionManager.iseriesPWD);
							if (UsaTodayConstants.debug) {
								System.out.println("Got a JNDI Connection using UID: " + ConnectionManager.iseriesUID);
							}
						} else {
							throw sqle;
						}
					} catch (SQLException f2) {
						// throw original exception
						throw sqle;
					}
				}
			} else {
				if (ConnectionManager.iseriesUID != null && ConnectionManager.iseriesPWD != null) {
					connection = DriverManager.getConnection(ConnectionManager.jdbcIseriesConnectionString,
							ConnectionManager.iseriesUID, ConnectionManager.iseriesPWD);
					if (UsaTodayConstants.debug) {
						System.out.println("Got a JDBC Connection using UID: " + ConnectionManager.iseriesUID);
					}
				} else {
					connection = DriverManager.getConnection(ConnectionManager.jdbcIseriesConnectionString);
					if (UsaTodayConstants.debug) {
						System.out.println("Got a JDBC Connection using just the connection URL");
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Error(e);
		}
		return connection;
	}

	public Connection getIseriesSecureConnection() {
		Connection connection = null;
		try {
			if (USING_JNDI) {
				try {
					connection = subscriptionPortalIseriesSecureDataSource.getConnection();
					if (UsaTodayConstants.debug) {
						System.out.println("Got a JNDI Connection.");
					}
				} catch (SQLException sqle) {
					try {
						// as a backup try the user id and password in config file
						if (ConnectionManager.iseriesUID != null && ConnectionManager.iseriesPWD != null) {
							connection = subscriptionPortalIseriesSecureDataSource.getConnection(ConnectionManager.iseriesUID,
									ConnectionManager.iseriesPWD);
							if (UsaTodayConstants.debug) {
								System.out.println("Got a JNDI Connection using UID: " + ConnectionManager.iseriesUID);
							}
						} else {
							throw sqle;
						}
					} catch (SQLException f2) {
						// throw original exception
						throw sqle;
					}
				}
			} else {
				if (ConnectionManager.iseriesUID != null && ConnectionManager.iseriesPWD != null) {
					connection = DriverManager.getConnection(ConnectionManager.jdbcIseriesConnectionString,
							ConnectionManager.iseriesUID, ConnectionManager.iseriesPWD);
					if (UsaTodayConstants.debug) {
						System.out.println("Got a JDBC Connection using UID: " + ConnectionManager.iseriesUID);
					}
				} else {
					connection = DriverManager.getConnection(ConnectionManager.jdbcIseriesConnectionString);
					if (UsaTodayConstants.debug) {
						System.out.println("Got a JDBC Connection using just the connection URL");
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Error(e);
		}
		return connection;
	}
}
