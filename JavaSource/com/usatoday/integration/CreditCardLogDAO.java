package com.usatoday.integration;

import java.sql.Connection;
import java.sql.Timestamp;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.shopping.CCLogIntf;

public class CreditCardLogDAO extends USATodayDAO {
	public static final String INSERT_CCLOG = "insert into XTRNTCCLOG (email, CreditCardNumb, CreditCardExpMonth, CreditCardExpYear, CreditCardAuthCode, insert_timestamp, FirstName, LastName, response_code, zip, ChargeAmount, pubcode, response_message, cc_type, vendor_transaction_id, avs_response_code, avs_response_message, orderID, client_ip, cvv_response_code, cvv_response_message, external_key, reason_code, request_type) values "
			+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

	/**
	 * 
	 * @param log
	 * @throws UsatException
	 */
	public void insert(CCLogIntf log) throws UsatException {
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(CreditCardLogDAO.INSERT_CCLOG);

			statement.setString(1, log.getEmail());
			statement.setString(2, log.getCCNum());
			statement.setString(3, log.getExpMonth());
			statement.setString(4, log.getExpYear());
			statement.setString(5, log.getAuthCode());
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			statement.setTimestamp(6, ts);
			statement.setString(7, log.getFirstName());
			statement.setString(8, log.getLastName());
			statement.setString(9, log.getResponseCode());
			statement.setString(10, log.getZip());
			statement.setString(11, log.getAmount());
			statement.setString(12, log.getPubCode());
			statement.setString(13, log.getResponseMessage());
			statement.setString(14, log.getCcType());
			statement.setString(15, log.getVendorTranID());
			statement.setString(16, log.getAvsRespCode());
			statement.setString(17, log.getAvsRespMessage());
			statement.setString(18, log.getOrderID());
			statement.setString(19, log.getClientIPAddress());
			statement.setString(20, log.getCVVResponseCode());
			statement.setString(21, log.getCVVResponseMessage());
			statement.setString(22, log.getExternalKey());
			statement.setString(23, log.getReasonCode());
			statement.setString(24, log.getRequestType());

			// execute the SQL
			statement.execute();

			if (statement.getUpdateCount() == 0) {
				throw new UsatException("CreditCardLogDAO: No Records Inserted and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
	}

}
