/*
 * Created on Mar 30, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.sql.SQLException;
import java.sql.Types;

import com.usatoday.UsatException;
import com.usatoday.businessObjects.customer.CreditCardInfoBO;

/**
 * @author aeast
 * @date Mar 30, 2007
 * @class UserDAO
 * 
 *        This class interfaces with the AS/400 to perform user management.
 * 
 */
public class CreditCardInfoDAO extends USATodayDAO {

	// SQL

	private static final String GET_CREDIT_CARD_INFO = "{call " + USATodayDAO.getSTORED_PROC_LIB().trim() + "USAT_GET_GENESYS_CC_INFO(?, ?, ?, ?, ?)}";

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws UsatException
	 */
	public CreditCardInfoBO determineCreditCardInfo(String pub, String accountNum, String newGift, String creditCardNum,
			String creditCardExp) throws UsatException {
		java.sql.Connection conn = null;
		CreditCardInfoBO creditCardInfo = new CreditCardInfoBO();

		try {

			conn = ConnectionManager.getInstance().getIseriesSecureConnection();

			java.sql.CallableStatement statement = conn.prepareCall(CreditCardInfoDAO.GET_CREDIT_CARD_INFO);

			String upperCasePub = pub.toUpperCase();
			statement.setString(1, upperCasePub);

			if (accountNum.trim().length() > 7) {
				statement.setString(2, accountNum.substring(2));
			} else {
				statement.setString(2, accountNum);
			}

			statement.setString(3, newGift);

			statement.setString(4, creditCardNum);

			statement.setString(5, creditCardExp);

			statement.registerOutParameter(3, Types.CHAR);
			statement.registerOutParameter(4, Types.CHAR);
			statement.registerOutParameter(5, Types.CHAR);

			statement.execute();
			creditCardInfo.setCreditCardNewGift(statement.getString(3).trim());
			creditCardInfo.setCreditCardNum(statement.getString(4).trim());
			creditCardInfo.setCreditCardExp(statement.getString(5));

		} catch (SQLException sqle) {
			System.out.println("CreditCardInfo : getCreditCardInfo() " + sqle.getMessage());

			throw new UsatException(sqle);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return creditCardInfo;
	}
}
