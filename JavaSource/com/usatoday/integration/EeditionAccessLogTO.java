/*
 * Created on Sep 23, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.integration;

import java.io.Serializable;
import java.sql.Timestamp;

import com.usatoday.business.interfaces.customer.EeditionAccessLogIntf;

/**
 * @author aeast
 * 
 * 
 */
public class EeditionAccessLogTO implements EeditionAccessLogIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8316565004199062225L;

	/**
     *  
     */
	public EeditionAccessLogTO() {
		super();
	}

	private String publication = null;
	private String accountNum = null;
	private String trialAccess = null;
	private Timestamp dateTime = null;
	private String clientIP = null;
	private String emailAddress = null;
	private long transID = 0;

	public long getTransID() {
		return transID;
	}

	public void setTransID(long transID) {
		this.transID = transID;
	}

	public String getPublication() {
		return publication;
	}

	public void setPublication(String publication) {
		this.publication = publication;
	}

	public String getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public String getTrialAccess() {
		return trialAccess;
	}

	public void setTrialAccess(String trialAccess) {
		this.trialAccess = trialAccess;
	}

	public Timestamp getDateTime() {
		return dateTime;
	}

	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}