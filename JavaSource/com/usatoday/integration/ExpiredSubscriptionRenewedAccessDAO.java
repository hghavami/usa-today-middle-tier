package com.usatoday.integration;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.ExpiredSubscriptionRenewedAccessIntf;

public class ExpiredSubscriptionRenewedAccessDAO extends USATodayDAO {

	// Define stored procedures
	private static final String DELETE_EXPIRED_SUBSCRIPTION_RENEWED_ACCESS = "{call USAT_DELETE_EXPIRED_SUBSCRIPTION_RENEWED_ACCESS(?,?)}";
	private static final String SELECT_EXPIRED_SUBSCRIPTION_RENEWED_ACCESS = "{call USAT_SELECT_EXPIRED_SUBSCRIPTION_RENEWED_ACCESS(?,?)}";

	// Insert a record into the usat_expired_subscription_renewed_access table
	public void insert(ExpiredSubscriptionRenewedAccessIntf expiredSubscriptionRenewedAccessIntf) throws UsatException {
		int rowsAffected = 0;

		if (expiredSubscriptionRenewedAccessIntf == null) {
			throw new UsatException("Null Expired Subscription Renewed Access object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();

			StringBuffer sql = new StringBuffer(
					"insert into usat_expired_subscription_renewed_access (PubCode, AccountNum, FirstName, LastName, FirmName, Email, Insert_timestamp) values (");
			// Fill in the SQL statement

			sql.append(prepareSqlInsert(expiredSubscriptionRenewedAccessIntf));

			rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Expired Subscription Renewed Access Information Object Inserted Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			ResultSet rs = statement.getGeneratedKeys();

			rs.next();
			int primaryKey = rs.getInt(1);

			expiredSubscriptionRenewedAccessIntf.setOrderID(primaryKey);

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException(
						"usat_expired_subscription_renewed_access: No Records Inserted and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

	// Delete a record into the usat_expired_subscription_renewed_access table
	public void delete(ExpiredSubscriptionRenewedAccessIntf expiredSubscriptionRenewedAccessIntf) throws UsatException {

		if (expiredSubscriptionRenewedAccessIntf == null) {
			throw new UsatException("Null Expired Subscription Renewed Access object passed in for delete.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement statement = conn
					.prepareCall(ExpiredSubscriptionRenewedAccessDAO.DELETE_EXPIRED_SUBSCRIPTION_RENEWED_ACCESS);

			statement.setString(1, expiredSubscriptionRenewedAccessIntf.getPublication());
			statement.setString(2, expiredSubscriptionRenewedAccessIntf.getAccountNum());

			statement.executeUpdate();
			statement.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}
	}

	// Prepare SQL for insert into XTRNTINTCSLINF table
	private StringBuffer prepareSqlInsert(ExpiredSubscriptionRenewedAccessIntf expiredSubscriptionRenewedAccessIntf) {

		StringBuffer sql = new StringBuffer();
		if (expiredSubscriptionRenewedAccessIntf.getPublication() != null) {
			sql.append("'").append(expiredSubscriptionRenewedAccessIntf.getPublication()).append("',");
		} else {
			sql.append("null,");
		}
		if (expiredSubscriptionRenewedAccessIntf.getAccountNum() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(expiredSubscriptionRenewedAccessIntf.getAccountNum()))
					.append("',");
		} else {
			sql.append("null,");
		}
		if (expiredSubscriptionRenewedAccessIntf.getFirstName() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(expiredSubscriptionRenewedAccessIntf.getFirstName())).append("',");
		} else {
			sql.append("null,");
		}
		if (expiredSubscriptionRenewedAccessIntf.getLastName() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(expiredSubscriptionRenewedAccessIntf.getLastName())).append("',");
		} else {
			sql.append("null,");
		}
		if (expiredSubscriptionRenewedAccessIntf.getFirmName() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(expiredSubscriptionRenewedAccessIntf.getFirmName())).append("',");
		} else {
			sql.append("null,");
		}
		if (expiredSubscriptionRenewedAccessIntf.getEmail() != null) {
			sql.append("'").append(USATodayDAO.escapeApostrophes(expiredSubscriptionRenewedAccessIntf.getEmail())).append("',");
		} else {
			sql.append("null,");
		}

		Timestamp ts = new Timestamp(System.currentTimeMillis());
		if (ts != null) {
			sql.append("'").append(ts).append("')");
		}

		return sql;
	}

	// Delete a record into the usat_expired_subscription_renewed_access table
	public boolean select(ExpiredSubscriptionRenewedAccessIntf expiredSubscriptionRenewedAccessIntf) throws UsatException {
		boolean eSFound = false;

		if (expiredSubscriptionRenewedAccessIntf == null) {
			throw new UsatException("Null Expired Subscription Renewed Access object passed in for select.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			CallableStatement statement = conn
					.prepareCall(ExpiredSubscriptionRenewedAccessDAO.SELECT_EXPIRED_SUBSCRIPTION_RENEWED_ACCESS);
			// pull count first

			statement.setString(1, expiredSubscriptionRenewedAccessIntf.getPublication());
			statement.setString(2, expiredSubscriptionRenewedAccessIntf.getAccountNum());

			ResultSet countRS = statement.executeQuery();

			if (countRS.next()) {
				eSFound = true;
			}

			statement.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return eSFound;
	}
}
