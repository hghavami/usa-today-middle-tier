/*
 * Created on Mar 30, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.sql.SQLException;
import java.sql.Types;

import com.gannett.usat.iconapi.client.AddressDeliveryMethodCode1Validation;
import com.gannett.usat.iconapi.domainbeans.DeliveryMethod.DeliveryMethod;
import com.gannett.usat.iconapi.domainbeans.DeliveryMethod.DeliveryMethodCode1ValidationWrapper;
import com.usatoday.UsatException;

/**
 * @author aeast
 * @date Mar 30, 2007
 * @class UserDAO
 * 
 *        This class interfaces with the AS/400 to perform user management.
 * 
 */
public class DeliveryMethodDAO extends USATodayDAO {

	// SQL

	private static final String GET_DELIVERY_METHOD = "{call NAT_UNIOBJ.USAT_GET_DELIVERY_METHOD(?, ?, ?, ?, ?, ?, ?, ?)}";

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws UsatException
	 */
	public DeliveryMethodTO determineDeliveryMethod(String pub, String street1, String street2, String city, String state,
			String zip) throws UsatException {
		java.sql.Connection conn = null;
		DeliveryMethodTO deliveryMethod = null;

		try {

			conn = ConnectionManager.getInstance().getIseriesConnection();

			java.sql.CallableStatement statement = conn.prepareCall(DeliveryMethodDAO.GET_DELIVERY_METHOD);

			String upperCasePub = pub.toUpperCase();
			statement.setString(1, upperCasePub);

			String upperCaseStreet1 = street1.toUpperCase();
			statement.setString(2, upperCaseStreet1);

			if (street2 != null) {
				String upperCaseStreet2 = street2.toUpperCase();
				statement.setString(3, upperCaseStreet2);
			} else {
				statement.setString(3, "");
			}

			String upperCaseCity = city.toUpperCase();
			statement.setString(4, upperCaseCity);

			String upperCaseState = state.toUpperCase();
			statement.setString(5, upperCaseState);

			statement.setString(6, zip);

			statement.setString(7, "");

			statement.setString(8, "");

			statement.registerOutParameter(7, Types.CHAR);
			statement.registerOutParameter(8, Types.CHAR);

			statement.execute();

			deliveryMethod = new DeliveryMethodTO();

			deliveryMethod.setDeliveryMethod(statement.getString(7));

			String delay = statement.getString(8);
			if (delay != null) {
				delay = delay.trim();
			}
			deliveryMethod.setDeliveryDayDelay(delay);

		} catch (SQLException sqle) {
			System.out.println("DeliveryMethodDAO : getDeliveryMethod() " + sqle.getMessage());

			throw new UsatException(sqle);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return deliveryMethod;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws UsatException
	 */
	public DeliveryMethodTO determineGenesysDeliveryMethod(String pub, String street1, String street2, String city, String state,
			String zip) throws UsatException {

		AddressDeliveryMethodCode1Validation sApi = new AddressDeliveryMethodCode1Validation();
		DeliveryMethodTO delvMethInfo = null;

		try {
			DeliveryMethodCode1ValidationWrapper addressdmcvTable = sApi.getAddressDeliveryMethodCode1ValidateObject(pub, street1,
					street2, city, state, zip);

			// convert raw json to objects (could have just called the helper method tht returns objects instead of json
			delvMethInfo = DeliveryMethodDAO.objectFactoryGenesys(addressdmcvTable);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return delvMethInfo;

	}

	// private static final String SELECT_COUNT_BY_ACCOUNT_NUM =
	// "select count(*) from XTRNTUPLD where CurrAcctNum = ? AND Active = 'Y'";
	// private static final String SELECT_COUNT_BY_DELIVERY_HOME_PHONE_AND_ZIP =
	// "select count(*) from XTRNTUPLD where HomePhone = ? AND Zip = ? AND Active = 'Y'";
	// private static final String SELECT_COUNT_BY_BILLING_HOME_PHONE_AND_ZIP =
	// "select count(*) from XTRNTUPLD where BillHomephone = ? AND Zip = ? AND Active = 'Y'";

	/**
	 * 
	 * This method generates transfer objects for each record in the result set.
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private synchronized static DeliveryMethodTO objectFactoryGenesys(DeliveryMethodCode1ValidationWrapper addressdmcvTable)
			throws UsatException {

		DeliveryMethodTO delvMethInfo = new DeliveryMethodTO();

		try {

			DeliveryMethod dmcvBean = addressdmcvTable.getRoute();
			// for (DeliveryMethodCode1Validate dmcvBean : dmcvBean.getDeliveryMethodCode1ValidateTable()) {

//			System.out.println("Account: " + dmcvBean.toString());
			// iterate over result set and build objects

			String tempStr = "";

			// Delivery method
			tempStr = dmcvBean.getDelmeth_out();
			tempStr = tempStr == null ? null : tempStr.trim();
			if (tempStr.equals("A") || tempStr.equals("C")) {
				delvMethInfo.setDeliveryMethod("C");
			} else {
				delvMethInfo.setDeliveryMethod("M");
			}
			// For now, no delivery days
			delvMethInfo.setDeliveryDayDelay("0");
			// }
		} catch (Exception e) {
			System.out.println("DeliveryMethodDAO::objectFactory() - Failed to create Delivery Method for Address, because: "
					+ "  " + e.getMessage());
		}
		return delvMethInfo;
	}
}
