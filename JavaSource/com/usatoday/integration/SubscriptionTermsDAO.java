package com.usatoday.integration;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.client.AvailableOffers;
import com.gannett.usat.iconapi.client.Disclaimers;
import com.gannett.usat.iconapi.client.IconAPIContext;
import com.gannett.usat.iconapi.domainbeans.Errors;
import com.gannett.usat.iconapi.domainbeans.ErrorsSerializer;
import com.gannett.usat.iconapi.domainbeans.disclaimers.DisclaimersTextsWrapper;
import com.gannett.usat.iconapi.domainbeans.ratesTerms.RatesTerms;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.RenewalOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;

public class SubscriptionTermsDAO extends USATodayDAO {

	// private static final String XTRNTKEYRT_OPERID_FIELDS =
	// "PubCode,SrcOrderCode,PromoCode,ContestCode,OfferDesc,OperatorNum";
	// public static final String TABLE_NAME = "XTRNTKEYRT";
	//
	// // SQL
	// private static final String XTRNTKEYRT_FIELDS =
	// "FreqDel, RateCode, RateCodeType, RetailRate, SubsLength, SubsAmount, OfferDesc, OperatorNum, BegDateOffer, EndDateOffer, ACTIVE, BillMeAllowed, ForceEZPay, ClubNumberRequired";
	// private static final String XTRNTRELRT_FIELDS =
	// "Pubcode, FreqofDelivery, DelMethod, PiaRateCode, RelRateCode, RelPeriodLength, RelOfferAmount, RelOfferDesc, RenRateCode, RenPeriodLength, RenOfferAmount, RenOfferDesc";
	// private static final String SELECT_KEYCODE_DATA = "select " +
	// SubscriptionTermsDAO.XTRNTKEYRT_FIELDS
	// +
	// " from XTRNTKEYRT where PubCode = ? and SrcOrderCode = ? AND PromoCode = ? and ContestCode = ?";
	// private static final String SELECT_RELATED_RATES = "SELECT " +
	// SubscriptionTermsDAO.XTRNTRELRT_FIELDS
	// +
	// " FROM XTRNTRELRT where PubCode=? and PiaRateCode=? and ACTIVE = 'Y' ORDER BY RelPeriodLength ASC";
	// private static final String SELECT_RELATED_RENEWAL_RATE = "SELECT " +
	// SubscriptionTermsDAO.XTRNTRELRT_FIELDS
	// +
	// " FROM XTRNTRELRT where PubCode=? and PiaRateCode=? and RelRateCode=? and ACTIVE = 'Y' ORDER BY RelPeriodLength ASC";
	// // private static final String SELECT_RELATED_RENEWTO_RATES = "SELECT " +
	// SubscriptionTermsDAO.XTRNTRELRT_FIELDS +
	// //
	// " FROM XTRNTRELRT where PubCode=? and PiaRateCode=? and RelPeriodLength >=? and ACTIVE = 'Y' ORDER BY RelPeriodLength DESC";
	// private static final String SELECT_RELATED_RENEWTO_RATES = "SELECT " +
	// SubscriptionTermsDAO.XTRNTRELRT_FIELDS
	// +
	// " FROM XTRNTRELRT where PubCode=? and PiaRateCode=? and ACTIVE = 'Y' ORDER BY RelPeriodLength ASC";

	// NCS Query
	private static final String SUBSCRIPTION_OFFER_SEARCH = "{call USAT_SUBSRIPTION_OFFER_SEARCH(?,?,?,?,?)}";

	// public Collection<SubscriptionTermsIntf> getGenesysOffer(String pubCode,
	// String accountNumber, String keyCode, String
	// fodCode, String renewalCode) throws UsatException {

	public SubscriptionOfferIntf getGenesysOfferTerms(String pubCode, String accountNumber, String keyCode, String fodCode,
			String renewalCode) throws UsatException {

		AvailableOffers oApi = new AvailableOffers();
		SubscriptionOfferTO offer = new SubscriptionOfferTO(keyCode, pubCode);

		Collection<SubscriptionTermsIntf> terms = null;
		// Collection<SubscriptionOfferIntf> terms = null;

		try {

			String sCodeGenesys = String.valueOf(keyCode.charAt(0));
			String pCodeGenesys = keyCode.substring(1, 3);
			String cCodeGenesys = keyCode.substring(3, 5);

			String responseJson = oApi.getAvailableOffers(pubCode, accountNumber, sCodeGenesys, pCodeGenesys, cCodeGenesys,
					fodCode, renewalCode);
			Collection<RatesTerms> ratesterms = AvailableOffers.jsonToRatesTerms(responseJson);

			terms = SubscriptionTermsDAO.termsObjectFactoryGenesys(ratesterms);
			offer.setTerms(terms);
			offer.setBillMeAllowed(false);
			offer.setClubNumberRequired(false);
			offer.setDailyRate(0.0);
			offer.setForceEZPay(true);
			for (RatesTerms termsBean : ratesterms) {
				if (termsBean.getEzpayRequired().equals("false")) {
					offer.setForceEZPay(false);
				}
				if (IconAPIContext.debugMode) {
					System.out.println("Rates Terms: " + termsBean.toString());
				}
			}
			// Now set renewal terms for this offer
			responseJson = oApi.getAvailableOffers(pubCode, accountNumber, sCodeGenesys, pCodeGenesys, cCodeGenesys, fodCode, "Y");
			ratesterms = AvailableOffers.jsonToRatesTerms(responseJson);
			terms = SubscriptionTermsDAO.termsObjectFactoryGenesys(ratesterms);
			offer.setRenewalTerms(terms);
			if (IconAPIContext.debugMode) {
				System.out.println("back to json....");
			}
			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

			Gson gson2 = builder.create();
			if (IconAPIContext.debugMode) {
				System.out.println(gson2.toJson(ratesterms));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return offer;

	}

	@SuppressWarnings("unused")
	private synchronized static Collection<SubscriptionTermsIntf> termsObjectFactoryGenesys(Collection<RatesTerms> ratesTerms)
			throws UsatException {
		Collection<SubscriptionTermsIntf> terms = new ArrayList<SubscriptionTermsIntf>();
		try {
			String ezPayTermFound = "";
			boolean disclaimerFound = false;
			// Make a pass and check if there are any EZPay rates
			for (RatesTerms termsBean : ratesTerms) {
				if (termsBean.getEzpayRequired() != null && !termsBean.getEzpayRequired().trim().equals("")) {
					if (termsBean.getEzpayRequired().equals("true")) {
						// Once an ezpay rate is found, save it to later get its
						// disclaimer
						ezPayTermFound = termsBean.getPiaRateCode();
					}
				}
			}
			// Make another pass and process each rate
			for (RatesTerms termsBean : ratesTerms) {
				if (IconAPIContext.debugMode) {
					System.out.println("Terms: " + termsBean.toString());
					// iterate over result set and build objects
				}
				String pubCode = "";
				String description = "";
				double price = 0.0;
				String durationInWeeks = "";
				String piaRateCode = "";
				String relatedRateCode = "";
				String renewalRateCode = "";
				String renewalPeriodLength = "";
				String duration = "";
				String durationType = "";
				String fodCode = "";
				boolean requiresEZPAY = false;
				String disclaimerText = "";
				// These are for later to add the error message received from
				// ICON API
				Collection<String> errorMessages = null;
				Errors errors = null;
				// String ezPay=null;

				/*
				 * boolean billMeAllowed = false; boolean forceEZPay = false;
				 * boolean forceBillMe = false; boolean ClubNumberRequired =
				 * false; double price = 0.0;
				 */
				// String tempStr = "";

				if (termsBean.getPiaRateCode() != null && !termsBean.getPiaRateCode().trim().equals("")) {
					piaRateCode = termsBean.getPiaRateCode().trim();
				}

				if (termsBean.getRatePeriod() != null && !termsBean.getRatePeriod().trim().equals("")) {
					durationType = termsBean.getRatePeriod();
				}

				if (termsBean.getRenewalPeriodTermLength() != null && !termsBean.getRenewalPeriodTermLength().trim().equals("")) {
					// renewalPeriodLength =
					// termsBean.getRenewalPeriodTermLength().trim();
					renewalPeriodLength = termsBean.getRenewalPeriodTermLength().trim();
				} else {
					renewalPeriodLength = termsBean.getTerm_length().trim();
				}

				if (termsBean.getRenewalRateCode() != null && !termsBean.getRenewalRateCode().trim().equals("")) {
					renewalRateCode = termsBean.getRenewalRateCode().trim();
				}

				try {
					// price is stored without a decimal in the database.
					if (termsBean.getAmount() != null && !termsBean.getAmount().trim().equals("")) {
						price = Double.parseDouble(termsBean.getAmount());
					}
				} catch (Exception exp) {
					System.out.println("SubscriptionTermsDAO::termsObjectFactoryGenesys() - Exception getting price: "
							+ exp.getMessage());
					price = -100.00;
				}

				if (termsBean.getPiaRateCode() != null && !termsBean.getPiaRateCode().trim().equals("")) {
					piaRateCode = termsBean.getPiaRateCode().trim();
				}

				if (termsBean.getPub_code() != null && !termsBean.getPub_code().trim().equals("")) {
					pubCode = termsBean.getPub_code().trim();
				}

				if (termsBean.getTerm_length() != null && !termsBean.getTerm_length().trim().equals("")) {
					duration = termsBean.getTerm_length().trim();
				}

				if (termsBean.getDescription() != null && !termsBean.getDescription().trim().equals("")) {
					description = termsBean.getDescription().trim();
				}

				if (termsBean.getFod() != null && !termsBean.getFod().trim().equals("")) {
					fodCode = termsBean.getFod().trim();
				}

				if (termsBean.getEzpayRequired() != null && !termsBean.getEzpayRequired().trim().equals("")) {
					if (termsBean.getEzpayRequired().equals("true")) {
						requiresEZPAY = true;
					}
				}

				// These are for later to add the error message received from
				// ICON API
				if (termsBean.containsErrors()) {
					errors = termsBean.getErrors();
					errorMessages = termsBean.getErrorMessages();
				}

				// Now get the first non-blanks disclaimer text for each rate
				if (!disclaimerFound) {
					// Have we found an ezpay rate
					if (!ezPayTermFound.trim().equals("")) {
						// Is the found ezpay rate equal to the current rate
						if (piaRateCode == ezPayTermFound) {
							disclaimerText = getDisclaimerText(pubCode, fodCode, piaRateCode, "C", true);
							// If no disclaimer for delivery method = C (Carrier
							// delivery) try M for mail delivery
							if (disclaimerText == null || disclaimerText.trim().equals("")) {
								disclaimerText = getDisclaimerText(pubCode, fodCode, piaRateCode, "M", true);
							}
							if (disclaimerText != null && !disclaimerText.trim().equals("")) {
								disclaimerFound = true;
							}
						}
						// No ezpay rate was found
					} else {
						disclaimerText = getDisclaimerText(pubCode, fodCode, piaRateCode, "C", true);
						// If no disclaimer for delivery method = C (Carrier
						// delivery) try M for mail delivery
						if (disclaimerText == null || disclaimerText.trim().equals("")) {
							disclaimerText = getDisclaimerText(pubCode, fodCode, piaRateCode, "M", true);
						}
						if (disclaimerText != null && !disclaimerText.trim().equals("")) {
							disclaimerFound = true;
						}
					}
				}
				SubscriptionTermsTO terms1 = new SubscriptionTermsTO(description, durationInWeeks, price, piaRateCode,
						relatedRateCode, renewalRateCode, renewalPeriodLength, pubCode, duration, durationType, fodCode,
						requiresEZPAY, disclaimerText);

				terms.add(terms1);
			}
		} catch (Exception e) {
			System.out.println("SubscriberTermsDAO::objectFactory() - Failed to create Subscription terms because: "
					+ e.getMessage());
		}

		return terms;
	}

	public Collection<KeycodeTO> getOffersForCriteria(String pubCode, String sourceCode, String promoCode, String contestCode,
			String offerCode) throws UsatException {
		java.sql.Connection conn = null;
		Collection<KeycodeTO> offers = new ArrayList<KeycodeTO>();
		try {

			conn = ConnectionManager.getInstance().getConnection();

			CallableStatement query = conn.prepareCall(SubscriptionTermsDAO.SUBSCRIPTION_OFFER_SEARCH);

			query.setString(1, pubCode);
			query.setString(2, sourceCode);
			query.setString(3, promoCode);
			query.setString(4, contestCode);
			query.setString(5, offerCode);

			// execute the SQL
			ResultSet rs = query.executeQuery();

			while (rs.next()) {

				KeycodeTO offer = new KeycodeTO();

				offer.setPubCode(pubCode);

				String srcCode = rs.getString("SrcOrderCode").trim();
				String pCode = rs.getString("PromoCode").trim();
				String cCode = rs.getString("ContestCode").trim();

				offer.setSourceCode(srcCode.trim());
				offer.setPromoCode(pCode.trim());
				offer.setContestCode(cCode.trim());

				String tempStr = rs.getString("ForceEZPay");
				offer.setForceEZPay(false);
				if ((tempStr != null) && (tempStr != "")) {
					try {
						if (tempStr.equalsIgnoreCase("Y")) {
							offer.setForceEZPay(true);
						}
					} catch (Exception e) {
						// ignore
					}
				}

				tempStr = rs.getString("BillMeAllowed");
				offer.setBillMeAllowed(false);
				if (tempStr != null) {
					try {
						tempStr = tempStr.trim();
						if (tempStr.equalsIgnoreCase("Y")) {
							offer.setBillMeAllowed(true);
						} else if (tempStr.equalsIgnoreCase("F")) {
							// force Bill Me
							offer.setBillMeAllowed(true);
							offer.setForceBillMe(true);
						}
					} catch (Exception e) {
						// ignore
					}
				}

				tempStr = rs.getString("ClubNumberRequired");
				offer.setClubNumberRequired(false);
				if ((tempStr != null) && (tempStr != "")) {
					try {
						if (tempStr.equalsIgnoreCase("Y")) {
							offer.setClubNumberRequired(true);
						}
					} catch (Exception e) {
						// ignore
					}
				}

				tempStr = rs.getString("RetailRate");
				if (tempStr != null) { // convert rate to double, AS400 format
										// is 7.5 format
					try {
						java.math.BigDecimal bd = new java.math.BigDecimal(tempStr);
						bd = bd.movePointLeft(5);
						bd = bd.setScale(2, java.math.BigDecimal.ROUND_HALF_UP);
						offer.setDailyRate(bd.doubleValue());
					} catch (Exception e) {
						// ignore
					}
				}

				tempStr = rs.getString("OfferDesc");
				if (tempStr != null) {
					offer.setDescription(tempStr.trim());
				}

				tempStr = rs.getString("OperatorNum");
				if (tempStr != null) {
					offer.setOfferCode(tempStr.trim());
				}

				offers.add(offer);
			}

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println("SubscriptionTermsDAO:getOffersForCriteria() Failed to retrive the offer:  Exception: "
					+ e.getMessage());
			throw new UsatException(e);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("SubscriptionTermsDAO::getOffersForCriteria() Failed to retrive the keycode view:   Exception: "
					+ e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}

		return offers;
	}

	public RenewalOfferIntf getGenesysRenewalOfferTerms(String pubCode, String accountNumber, String keyCode, String fodCode,
			String renewalCode) throws UsatException {

		AvailableOffers oApi = new AvailableOffers();
		RenewalOfferTO offer = new RenewalOfferTO(keyCode, pubCode);

		Collection<SubscriptionTermsIntf> terms = null;

		try {
			if (keyCode != null && !keyCode.trim().equals("") && keyCode.length() > 4) {
				String sCodeGenesys = String.valueOf(keyCode.charAt(0));
				String pCodeGenesys = keyCode.substring(1, 3);
				String cCodeGenesys = keyCode.substring(3, 5);

				String responseJson = oApi.getAvailableOffers(pubCode, accountNumber, sCodeGenesys, pCodeGenesys, cCodeGenesys,
						fodCode, renewalCode);
				Collection<RatesTerms> ratesterms = AvailableOffers.jsonToRatesTerms(responseJson);
				if (ratesterms != null) {
					terms = SubscriptionTermsDAO.termsObjectFactoryGenesys(ratesterms);
					offer.setTerms(terms);
					offer.setPubCode(pubCode);
					offer.setKeyCode(keyCode);
					for (RatesTerms termsBean : ratesterms) {
						offer.setRateCode(termsBean.getRenewalRateCode());
						if (IconAPIContext.debugMode) {
							System.out.println("Rates Terms: " + termsBean.toString());
						}
						break;
					}
					if (IconAPIContext.debugMode) {
						System.out.println("back to json....");
					}
					GsonBuilder builder = new GsonBuilder();
					builder.setPrettyPrinting();
					builder.registerTypeAdapter(Errors.class, new ErrorsSerializer());

					Gson gson2 = builder.create();
					if (IconAPIContext.debugMode) {
						System.out.println(gson2.toJson(ratesterms));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return offer;

	}

	public synchronized static String getDisclaimerText(String pubCode, String fodCode, String piaRateCode, String deliveryMethod,
			boolean requiresEZPAY) throws UsatException {
		String disclaimerText = "";

		Disclaimers discTxt = new Disclaimers();
		// Need to get the disclaimer for ez pay = 'Y'
		String requiresEZPAYString = "";
		if (requiresEZPAY) {
			requiresEZPAYString = "Y";
		}
		// Get disclaimer for carrier delivery and if blanks for mail delivery
		// methods
		String responseJson;
		try {
			responseJson = discTxt.getDisclaimers(pubCode, fodCode, piaRateCode, deliveryMethod, requiresEZPAYString);
			DisclaimersTextsWrapper disclaimerTexts = Disclaimers.jsonToDisclaimerTexts(responseJson);
			if (IconAPIContext.debugMode) {
				System.out.println("Disclaimer text: " + disclaimerTexts.toString());
			}
			disclaimerText = disclaimerTexts.getDisclaimer().getDisclaimer_text();
			if (disclaimerText == null || disclaimerText.trim().equals("")) {
				responseJson = discTxt.getDisclaimers(pubCode, fodCode, piaRateCode, "M", requiresEZPAYString);
				disclaimerTexts = Disclaimers.jsonToDisclaimerTexts(responseJson);
				if (IconAPIContext.debugMode) {
					System.out.println("Disclaimer text: " + disclaimerTexts.toString());
				}
				disclaimerText = disclaimerTexts.getDisclaimer().getDisclaimer_text();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return disclaimerText;
	}

	/**
	 * 
	 * @param keyCode
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	// public SubscriptionOfferIntf getOffer(String keyCode, String pubCode)
	// throws UsatException {
	//
	// java.sql.Connection conn = null;
	// SubscriptionOfferTO offer = null;
	// if (keyCode == null || keyCode.length() < 5 || pubCode == null) {
	// return null;
	// }
	// try {
	//
	// conn = ConnectionManager.getInstance().getConnection();
	//
	// java.sql.PreparedStatement statement =
	// conn.prepareStatement(SubscriptionTermsDAO.SELECT_KEYCODE_DATA);
	//
	// String sCode = String.valueOf(keyCode.charAt(0));
	// String pCode = keyCode.substring(1, 3);
	// String cCode = keyCode.substring(3, 5);
	//
	// statement.setString(1, pubCode);
	// statement.setString(2, sCode);
	// statement.setString(3, pCode);
	// statement.setString(4, cCode);
	//
	// // execute the SQL
	// ResultSet rs = statement.executeQuery();
	//
	// if (rs.next()) {
	// offer = new SubscriptionOfferTO(keyCode, pubCode);
	//
	// String piaRateCode = rs.getString("RateCode");
	// if (piaRateCode != null) {
	// piaRateCode = piaRateCode.trim();
	// }
	//
	// String tempStr = rs.getString("ForceEZPay");
	// offer.setForceEZPay(false);
	// if ((tempStr != null) && (tempStr != "")) {
	// try {
	// if (tempStr.equalsIgnoreCase("Y")) {
	// offer.setForceEZPay(true);
	// }
	// } catch (Exception e) {
	// // ignore
	// }
	// }
	//
	// tempStr = rs.getString("BillMeAllowed");
	// offer.setBillMeAllowed(false);
	// if (tempStr != null) {
	// try {
	// tempStr = tempStr.trim();
	// if (tempStr.equalsIgnoreCase("Y")) {
	// offer.setBillMeAllowed(true);
	// } else if (tempStr.equalsIgnoreCase("F")) {
	// // force Bill Me
	// offer.setBillMeAllowed(true);
	// offer.setForceBillMe(true);
	// }
	// } catch (Exception e) {
	// // ignore
	// }
	// }
	//
	// tempStr = rs.getString("ClubNumberRequired");
	// offer.setClubNumberRequired(false);
	// if ((tempStr != null) && (tempStr != "")) {
	// try {
	// if (tempStr.equalsIgnoreCase("Y")) {
	// offer.setClubNumberRequired(true);
	// }
	// } catch (Exception e) {
	// // ignore
	// }
	// }
	//
	// tempStr = rs.getString("RetailRate");
	// if (tempStr != null) { // convert rate to double, AS400 format is 7.5
	// format
	// try {
	// java.math.BigDecimal bd = new java.math.BigDecimal(tempStr);
	// bd = bd.movePointLeft(5);
	// bd = bd.setScale(2, java.math.BigDecimal.ROUND_HALF_UP);
	// offer.setDailyRate(bd.doubleValue());
	// } catch (Exception e) {
	// // ignore
	// }
	// }
	//
	// offer.setTerms(this.getTerms(pubCode, piaRateCode, conn));
	//
	// offer.setRenewalTerms(this.getRenewalTerms(pubCode, piaRateCode,
	// piaRateCode, conn));
	// }
	//
	// } catch (java.sql.SQLException e) {
	// e.printStackTrace();
	// System.out.println("SubscriptionTermsDAO:getOffer() Failed to retrive the offer: Keycode: "
	// + keyCode + " Exception: "
	// + e.getMessage());
	// throw new UsatException(e);
	// } catch (Exception e) {
	// e.printStackTrace();
	// System.out.println("SubscriptionTermsDAO::getOffer() Failed to retrive the keycode view: Keycode: "
	// + keyCode
	// + " Exception: " + e.getMessage());
	// throw new UsatException(e);
	// } finally {
	// this.cleanupConnection(conn);
	// }
	//
	// return offer;
	// }

	/**
	 * 
	 * @param pubCode
	 * @param offerCode
	 * @return The keyCode associated with the offer code.
	 * @throws UsatException
	 */
	// This method is no longer used after the migration to Genesys.
	// public String getKeycodeForOfferCode(String pubCode, String offerCode)
	// throws UsatException {
	// java.sql.Connection conn = null;
	// String keyCode = null;
	//
	// if (offerCode == null || pubCode == null) {
	// return null;
	// }
	// try {
	//
	// conn = ConnectionManager.getInstance().getConnection();
	//
	// String sql = "select " + SubscriptionTermsDAO.XTRNTKEYRT_OPERID_FIELDS +
	// " from " + SubscriptionTermsDAO.TABLE_NAME
	// + " where PubCode = ?" + " and OperatorNum = ?";
	//
	// PreparedStatement statement = conn.prepareStatement(sql);
	//
	// statement.setString(1, USATodayDAO.escapeApostrophes(pubCode));
	// statement.setString(2, USATodayDAO.escapeApostrophes(offerCode));
	//
	// // execute the SQL
	// ResultSet rs = statement.executeQuery();
	//
	// if (rs.next()) {
	//
	// String sourceCode = rs.getString("SrcOrderCode");
	// String promoCode = rs.getString("PromoCode");
	// String contestCode = rs.getString("ContestCode");
	//
	// keyCode = sourceCode.trim() + promoCode.trim() + contestCode.trim();
	//
	// } else {
	// keyCode = null;
	// }
	//
	// } catch (java.sql.SQLException e) {
	// e.printStackTrace();
	// System.out.println("SubscriptionTermsDAO:getKeycodeForOfferCode() Failed to retrive the offer: Offercode: "
	// + offerCode
	// + " Pub: " + pubCode + " Exception: " + e.getMessage());
	// throw new UsatException(e);
	// } catch (Exception e) {
	// e.printStackTrace();
	// System.out.println("SubscriptionTermsDAO::getKeycodeForOfferCode() Failed to retrive the keycode view: Offercode: "
	// + offerCode + " Pub: " + pubCode + " Exception: " + e.getMessage());
	// throw new UsatException(e);
	// } finally {
	// this.cleanupConnection(conn);
	// }
	//
	// return keyCode;
	// }

	/**
	 * 
	 * @param rateCode
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	// public RenewalOfferIntf getRenewalOffer(String rateCode, String pubCode)
	// throws UsatException {
	// java.sql.Connection conn = null;
	// RenewalOfferTO offer = null;
	// if (rateCode == null || rateCode.length() < 2 || pubCode == null) {
	// return null;
	// }
	// try {
	//
	// offer = new RenewalOfferTO(rateCode, pubCode);
	// offer.setRelatedRateCode(rateCode);
	//
	// // get renewal terms
	// offer.setTerms(this.getRenewalTerms(pubCode, rateCode, rateCode, conn));
	// } finally {
	// this.cleanupConnection(conn);
	// }
	//
	// return offer;
	// }

	/**
	 * 
	 * This method was created specifically for retrival of the Forced EZPAY
	 * term. It may be used for other uses but, it will throw exception if more
	 * than one term meets the criteria.
	 * 
	 * @param rateCode
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	// public SubscriptionTermsIntf getTerm(String rateCode, String pubCode)
	// throws UsatException {
	// java.sql.Connection conn = null;
	// SubscriptionTermsIntf term = null;
	// if (rateCode == null || pubCode == null) {
	// return null;
	// }
	// try {
	//
	// conn = ConnectionManager.getInstance().getConnection();
	// Collection<SubscriptionTermsIntf> terms = this.getTerms(pubCode,
	// rateCode, conn);
	//
	// if (terms.size() == 1) {
	// term = (SubscriptionTermsIntf) terms.iterator().next();
	// } else {
	// throw new UsatException(
	// "Expected a single rate code, but got a different number for this ratecode and pubcode combination: "
	// + rateCode + " " + pubCode + ". Only one term expected. Num Returned: " +
	// terms.size());
	// }
	// } catch (Exception e) {
	// // e.printStackTrace();
	// System.out.println("SubscriptionTermsDAO::getTerm() Failed to retrive the term :  RateCode & Pub : "
	// + rateCode + " "
	// + pubCode + "   Exception: " + e.getMessage());
	// throw new UsatException(e);
	// } finally {
	// this.cleanupConnection(conn);
	// }
	//
	// return term;
	// }

	/**
	 * 
	 * @param keyCode
	 * @param pubCode
	 * @param rateCode
	 * @return
	 */
	// private Collection<SubscriptionTermsIntf> getTerms(String pubCode, String
	// rateCode, Connection conn) throws UsatException {
	// boolean cleanupConn = false;
	// if (conn == null) {
	// conn = ConnectionManager.getInstance().getConnection();
	// cleanupConn = true;
	// }
	// Collection<SubscriptionTermsIntf> terms = null;
	// try {
	// java.sql.PreparedStatement statement =
	// conn.prepareStatement(SubscriptionTermsDAO.SELECT_RELATED_RATES);
	//
	// statement.setString(1, pubCode);
	// statement.setString(2, rateCode);
	//
	// // execute the SQL
	// ResultSet rs = statement.executeQuery();
	//
	// terms = SubscriptionTermsDAO.termsObjectFactory(rs);
	//
	// } catch (java.sql.SQLException e) {
	// e.printStackTrace();
	// System.out.println("SubscriptionTermsDAO:getTerms() Failed to retrive the terms: RateCode: "
	// + rateCode
	// + " Exception: " + e.getMessage());
	// throw new UsatException(e);
	// } catch (Exception e) {
	// e.printStackTrace();
	// System.out.println("SubscriptionTermsDAO::getTerms() Failed to retrive the terms view: RateCode: "
	// + rateCode
	// + " Exception: " + e.getMessage());
	// throw new UsatException(e);
	// } finally {
	// if (cleanupConn) {
	// this.cleanupConnection(conn);
	// }
	// }
	// return terms;
	// }

	/**
	 * 
	 * @param keyCode
	 * @param pubCode
	 * @param rateCode
	 * @return
	 */
	// private Collection<SubscriptionTermsIntf> getRenewalTerms(String pubCode,
	// String rateCode, String renewalRateCode,
	// Connection conn) throws UsatException {
	// boolean cleanupConn = false;
	// if (conn == null) {
	// conn = ConnectionManager.getInstance().getConnection();
	// cleanupConn = true;
	// }
	// Collection<SubscriptionTermsIntf> terms = null;
	// try {
	// java.sql.PreparedStatement statement =
	// conn.prepareStatement(SubscriptionTermsDAO.SELECT_RELATED_RENEWAL_RATE);
	//
	// statement.setString(1, pubCode);
	// statement.setString(2, rateCode);
	// statement.setString(3, rateCode);
	//
	// // execute the SQL
	// ResultSet rs = statement.executeQuery();
	//
	// terms = SubscriptionTermsDAO.termsObjectFactory(rs);
	//
	// if (terms.size() == 0) {
	// throw new UsatException("No Renewal Terms/Rates found for PubCode: " +
	// pubCode + " Rate Code: " + rateCode);
	// }
	// // Renewal rates are collected using the values associated with the rate
	// where rateCode and related Rate code are
	// // identical;
	// SubscriptionTermsIntf term = (SubscriptionTermsIntf)
	// terms.iterator().next();
	//
	// // then query for all rates with that code that have a term greater than
	// the base one
	// statement =
	// conn.prepareStatement(SubscriptionTermsDAO.SELECT_RELATED_RENEWTO_RATES);
	//
	// statement.setString(1, pubCode);
	// statement.setString(2, term.getRenewalRateCode());
	// // statement.setString(3, term.getRenewalPeriodLength());
	//
	// // execute the SQL
	// rs = statement.executeQuery();
	//
	// terms = SubscriptionTermsDAO.termsObjectFactory(rs);
	//
	// } catch (java.sql.SQLException e) {
	// e.printStackTrace();
	// System.out.println("SubscriptionTermsDAO:getRenewalTerms() Failed to retrive the terms: RateCode: "
	// + rateCode
	// + " Exception: " + e.getMessage());
	// throw new UsatException(e);
	// } catch (UsatException ue) {
	// throw ue;
	// } catch (Exception e) {
	// e.printStackTrace();
	// System.out.println("SubscriptionTermsDAO::getRenewalTerms() Failed to retrive the terms view: RateCode: "
	// + rateCode
	// + " Exception: " + e.getMessage());
	// throw new UsatException(e);
	// } finally {
	// if (cleanupConn) {
	// this.cleanupConnection(conn);
	// }
	// }
	// return terms;
	// }

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	// private synchronized static Collection<SubscriptionTermsIntf>
	// termsObjectFactory(ResultSet rs) throws UsatException {
	// Collection<SubscriptionTermsIntf> records = new
	// ArrayList<SubscriptionTermsIntf>();
	//
	// try {
	// // iterate over result set and build objects
	//
	// while (rs.next()) {
	//
	// String desciption = "";
	// String durationInWeeks = "";
	// String piaRateCode = "";
	// double price = 0.0;
	// String releatedRateCode = "";
	// String renewalRateCode = "";
	// String renewalPeriodLength = "";
	// String pubCode = "";
	//
	// desciption = rs.getString("RelOfferDesc");
	// if (desciption != null) {
	// desciption = desciption.trim();
	// }
	//
	// durationInWeeks = rs.getString("RelPeriodLength");
	// if (durationInWeeks != null) {
	// durationInWeeks = durationInWeeks.trim();
	// }
	//
	// piaRateCode = rs.getString("PiaRateCode");
	// if (piaRateCode != null) {
	// piaRateCode = piaRateCode.trim();
	// }
	//
	// try {
	// // price is stored without a decimal in the database.
	// price = rs.getDouble("RelOfferAmount");
	// price = price / 100.00;
	// } catch (Exception exp) {
	// System.out.println("SubscriptionTermsDAO::termsObjectFactory() - Exception getting price: "
	// + exp.getMessage());
	// price = -100.00;
	// }
	//
	// releatedRateCode = rs.getString("RelRateCode");
	// if (releatedRateCode != null) {
	// releatedRateCode = releatedRateCode.trim();
	// }
	//
	// renewalPeriodLength = rs.getString("RenPeriodLength");
	// if (renewalPeriodLength != null) {
	// renewalPeriodLength = renewalPeriodLength.trim();
	// }
	//
	// renewalRateCode = rs.getString("RenRateCode");
	// if (renewalRateCode != null) {
	// renewalRateCode = renewalRateCode.trim();
	// }
	//
	// pubCode = rs.getString("Pubcode");
	// if (pubCode != null) {
	// pubCode = pubCode.trim();
	// }
	//
	// // APE - Manually added the last 3 parms to keep this working as we
	// transition to Genesys
	// SubscriptionTermsTO term = new SubscriptionTermsTO(desciption,
	// durationInWeeks, price, piaRateCode,
	// releatedRateCode, renewalRateCode, renewalPeriodLength, pubCode,
	// durationInWeeks, "W", "DO", false);
	//
	// records.add(term);
	// }
	// } catch (Exception e) {
	// System.out.println("EmailRecordDAO::objectFactory() - Failed to create Term "
	// + e.getMessage());
	// }
	//
	// return records;
	// }
}
