package com.usatoday.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentTypentf;

public class PaymentTypeDAO extends USATodayDAO {

	private static final String CREDIT_CARD_CTRL_FIELDS = "CCType, BankName, FirstDigit, CCNumLength1, CCNumLength2, ImageFile";
	// we don't really use a control file to determine which payment methods are available for which product so use UT to get the
	// base set of payment methods (exclude bill me)
	private static final String SELECT_AVAILABLE_CC_PAYMENT_METHODS = "select " + PaymentTypeDAO.CREDIT_CARD_CTRL_FIELDS
			+ " from XTRNTCRDCTL where pubcode = 'UT' and CCType <> 'B'";

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private synchronized static Collection<CreditCardPaymentTypentf> creditCardObjectFactory(ResultSet rs) throws UsatException {
		Collection<CreditCardPaymentTypentf> records = new ArrayList<CreditCardPaymentTypentf>();

		try {

			while (rs.next()) {
				// iterate over result set and build objects
				String ccType = "";
				String bankName = "";
				int minimumDigits = 0;
				int maximumDigits = 0;
				char firstChar = ' ';
				String imageFile = "";

				String tempStr = rs.getString("CCType");
				if (tempStr != null) {
					ccType = tempStr.trim();
				}

				tempStr = rs.getString("BankName");
				if (tempStr != null) {
					bankName = tempStr.trim();
				}

				tempStr = rs.getString("FirstDigit");
				if (tempStr != null && tempStr.trim().length() == 1) {
					firstChar = tempStr.trim().charAt(0);
				}

				minimumDigits = rs.getInt("CCNumLength1");
				maximumDigits = rs.getInt("CCNumLength2");

				tempStr = rs.getString("ImageFile");
				if (tempStr != null) {
					imageFile = tempStr.trim();
				}

				CreditCardPaymentTypeTO type = new CreditCardPaymentTypeTO(minimumDigits, maximumDigits, bankName, firstChar,
						imageFile, bankName, ccType);

				records.add(type);
			}
		} catch (Exception e) {
			System.out.println("TaxRateDAO::objectFactory() - Failed to create TaxTable Record for zip: " + e.getMessage());
		}

		return records;
	}

	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public Collection<CreditCardPaymentTypentf> getCreditCardPaymentMethodTypes() throws UsatException {
		java.sql.Connection conn = null;
		Collection<CreditCardPaymentTypentf> paymentMethodTypes = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = conn.prepareStatement(PaymentTypeDAO.SELECT_AVAILABLE_CC_PAYMENT_METHODS);

			ResultSet rs = statement.executeQuery();

			paymentMethodTypes = PaymentTypeDAO.creditCardObjectFactory(rs);
		} catch (Exception e) {
			System.out.println("EmailRecordDAO : getTaxRateForZip() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return paymentMethodTypes;
	}
}
