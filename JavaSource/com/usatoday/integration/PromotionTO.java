/*
 * Created on May 5, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;

import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

/**
 * @author aeast
 * @date May 5, 2006
 * @class PromotionTO
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class PromotionTO implements PromotionIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4807114194509498660L;
	private String pubCode = "";
	private String keyCode = "";
	private String type = "";
	private String name = "";
	private String fulfillText = "";
	private String fulfillUrl = "";
	private String altName = "";

	/**
     * 
     */
	public PromotionTO() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.PromotionIntf#getAltName()
	 */
	public String getAltName() {
		return this.altName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.PromotionIntf#getFulfillText()
	 */
	public String getFulfillText() {
		return this.fulfillText;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.PromotionIntf#getFulfillUrl()
	 */
	public String getFulfillUrl() {
		return this.fulfillUrl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.PromotionIntf#getKeycode()
	 */
	public String getKeyCode() {
		return this.keyCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.PromotionIntf#getName()
	 */
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.PromotionIntf#getPubcode()
	 */
	public String getPubCode() {
		return pubCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.PromotionIntf#getType()
	 */
	public String getType() {
		return type;
	}

	public void setAltName(String altName) {
		this.altName = altName;
	}

	public void setFulfillText(String fulfillText) {
		this.fulfillText = fulfillText;
	}

	public void setFulfillUrl(String fulfillUrl) {
		this.fulfillUrl = fulfillUrl;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public void setType(String type) {
		this.type = type;
	}
}
