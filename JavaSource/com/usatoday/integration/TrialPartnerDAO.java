package com.usatoday.integration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.UsatException;

public class TrialPartnerDAO extends USATodayDAO {

	private static final String PARTNET_FIELDS = " id, trial_pubcode, start_date, end_date, post_trial_keycode, launch_page_text, trial_over_text, disclaimer_text, "
			+ "trial_duration_days, partner_id, partner_name, landing_page_url, description, complete_page_text, confirmation_eEmail_template_id, active, "
			+ "show_club_number, club_number_label, logo_image_path, logo_image_link, auth_url, secret_hash_key, send_confirmation_on_insert";

	private static final String SELECT_PARTNER_BY_PARTNERID = "select" + TrialPartnerDAO.PARTNET_FIELDS
			+ " from usat_trial_subscription_partners where partner_id = ?";
	private static final String SELECT_PARTNERS = "select" + TrialPartnerDAO.PARTNET_FIELDS
			+ " from usat_trial_subscription_partners";
	private static final String SELECT_ACTIVE_PARTNERS = "select" + TrialPartnerDAO.PARTNET_FIELDS
			+ " from usat_trial_subscription_partners where active = 'Y'";

	private Collection<TrialPartnerTO> objectFactory(ResultSet rs) throws UsatException {
		ArrayList<TrialPartnerTO> partners = new ArrayList<TrialPartnerTO>();

		try {
			while (rs.next()) {
				TrialPartnerTO to = new TrialPartnerTO();

				int primaryKey = rs.getInt("id");

				to.setID(primaryKey);

				String tempStr = null;

				tempStr = rs.getString("trial_pubcode");
				if (tempStr != null) {
					to.setPubCode(tempStr.trim());
				}

				Timestamp datetime = rs.getTimestamp("start_date");
				if (datetime != null) {
					DateTime d = new DateTime(datetime);
					to.setStartDate(d);
				}

				datetime = rs.getTimestamp("end_date");
				if (datetime != null) {
					DateTime d = new DateTime(datetime);
					to.setEndDate(d);
				}

				tempStr = rs.getString("post_trial_keycode");
				if (tempStr != null) {
					to.setKeyCode(tempStr.trim());
				}

				int durationInDays = 0;
				durationInDays = rs.getInt("trial_duration_days");
				to.setDurationInDays(durationInDays);

				tempStr = rs.getString("partner_id");
				if (tempStr != null) {
					to.setPartnerID(tempStr.trim());
				}

				tempStr = rs.getString("partner_name");
				if (tempStr != null) {
					to.setPartnerName(tempStr.trim());
				}

				tempStr = rs.getString("landing_page_url");
				if (tempStr != null) {
					to.setCustomerLadingPageURL(tempStr.trim());
				}

				tempStr = rs.getString("logo_image_path");
				if (tempStr != null) {
					to.setLogoImageURL(tempStr.trim());
				}

				tempStr = rs.getString("logo_image_link");
				if (tempStr != null) {
					to.setLogoLinkURL(tempStr.trim());
				}

				tempStr = rs.getString("description");
				if (tempStr != null) {
					to.setDescription(tempStr.trim());
				}

				tempStr = rs.getString("complete_page_text");
				if (tempStr != null) {
					to.setCompletePageCustomHTML(tempStr.trim());
				}

				tempStr = rs.getString("launch_page_text");
				if (tempStr != null) {
					to.setLaunchPageCustomHTML(tempStr.trim());
				}

				tempStr = rs.getString("trial_over_text");
				if (tempStr != null) {
					to.setTrialOverCustomHTML(tempStr.trim());
				}

				tempStr = rs.getString("disclaimer_text");
				if (tempStr != null) {
					to.setDisclaimerCustomHTML(tempStr.trim());
				}

				tempStr = rs.getString("club_number_label");
				if (tempStr != null) {
					to.setClubNumberLabel(tempStr.trim());
				}

				tempStr = rs.getString("secret_hash_key");
				if (tempStr != null) {
					to.setAPISecretHashKey(tempStr.trim());
				}

				// auth_url
				tempStr = rs.getString("auth_url");
				if (tempStr != null) {
					to.setAuthLinkURL(tempStr.trim());
				}

				tempStr = rs.getString("confirmation_eEmail_template_id");
				if (tempStr != null) {
					to.setConfirmationEmaiTemplateID(tempStr.trim());
				}

				tempStr = rs.getString("active");
				if (tempStr != null && "Y".equalsIgnoreCase(tempStr.trim())) {
					to.setIsActive(true);
				}

				tempStr = rs.getString("show_club_number");
				if (tempStr != null && "Y".equalsIgnoreCase(tempStr.trim())) {
					to.setIsShowClubNumber(true);
				}

				tempStr = rs.getString("send_confirmation_on_insert");
				if (tempStr != null && "N".equalsIgnoreCase(tempStr.trim())) {
					to.setIsSendConfirmationOnCreation(false);
				}

				partners.add(to);

			}
		} catch (SQLException sqle) {
			throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
		}
		return partners;

	}

	/**
	 * 
	 * @param pid
	 * @return
	 * @throws UsatException
	 */
	public TrialPartnerTO fetchTrialPartnerByPartnerID(String pid) throws UsatException {

		TrialPartnerTO to = null;
		Collection<TrialPartnerTO> records = null;

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(TrialPartnerDAO.SELECT_PARTNER_BY_PARTNERID);

			statement.setString(1, pid);

			ResultSet rs = statement.executeQuery();

			records = this.objectFactory(rs);

			if (records.size() == 1) {
				to = records.iterator().next();
			} else {
				to = null;
			}

		} catch (Exception e) {
			System.out.println("TrialPartnerDAO : fetchTrialPartnerByPartnerID() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return to;
	}

	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public Collection<TrialPartnerTO> fetchActiveTrialPartners() throws UsatException {

		Collection<TrialPartnerTO> records = null;

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(TrialPartnerDAO.SELECT_ACTIVE_PARTNERS);

			ResultSet rs = statement.executeQuery();

			records = this.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("TrialPartnerDAO : fetchActiveTrialPartners() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return records;
	}

	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public Collection<TrialPartnerTO> fetchTrialPartners() throws UsatException {

		Collection<TrialPartnerTO> records = null;

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(TrialPartnerDAO.SELECT_PARTNERS);

			ResultSet rs = statement.executeQuery();

			records = this.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("TrialPartnerDAO : fetchTrialPartners() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return records;
	}
}
