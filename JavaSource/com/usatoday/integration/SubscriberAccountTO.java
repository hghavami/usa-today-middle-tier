package com.usatoday.integration;

import java.io.Serializable;
import java.util.Date;

import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.customer.PersistentSubscriberAccountIntf;
import com.usatoday.business.interfaces.products.RenewalOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;

public class SubscriberAccountTO implements PersistentSubscriberAccountIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4646324895383053122L;
	private String pubCode = null;
	private String transDate = null;
	private String transRecType = null;
	private String lastName = null;
	private String firstName = null;
	private String firmName = null;
	private String halfUnitNum = null;
	private String streetDir = null;
	private String streetName = null;
	private String streetType = null;
	private String streetPostDir = null;
	private String subUnitCode = null;
	private String subUnitNum = null;
	private String unitNum = null;
	private String addlAddr1 = null;
	private String city = null;
	private String state = null;
	private String zip = null;
	private String homePhone = null;
	private String businssPhone = null;
	private String accountNumber = null;
	private boolean expiredSubscriptionAccessRenewed = false;
	private String deliveryMethod = null;
	private String frequencyOfDeliveryCode = null;
	private String billingLastName = null;
	private String billingFirstName = null;
	private String billingFirmName = null;
	private String billingAddress1 = null;
	private String billingAddress2 = null;
	private String billingUnitNum = null;
	private String billingHalfUnitNum = null;
	private String billingStreetDir = null;
	private String billingStreetName = null;
	private String billingStreetType = null;
	private String billingStreetPostDir = null;
	private String billingSubUnitNum = null;
	private String billingSubUnitCode = null;
	private String billingCity = null;
	private String billingState = null;
	private String billingZip = null;
	private String billingHomePhone = null;
	private String billingBusinessPhone = null;
	private Double balance = null;
	private Date expirationDate = null; // piaExpirationDate
	private Date startDate = null;
	private boolean active = false;
	private Date lastPaymentReceivedDate = null;
	private String lastStopCode = null;
	private boolean subscriptionSuspended = false;
	private boolean onEZPay = false;
	private boolean donatingToNIE = false;
	private boolean billingSameAsDelivery = false;
	private boolean oneTimeBill = false;
	private boolean showDeliveryAlert = false;
	private String renewalTypeAlpha = null;
	private String donationCode = null;
	private String rateCode = null;
	private String keyCode = null;
	private int numberOfPapers = 1;
	private String creditCardNum = null;
	private String creditCardType = null;
	private String creditCardExp = null;

	public SubscriberAccountTO(String pubCode, String transDate, String transRecType, String lastName, String firstName,
			String firmName, String halfUnitNum, String streetDir, String streetName, String streetType, String streetPostDir,
			String subUnitCode, String subUnitNum, String unitNum, String addlAddr1, String city, String state, String zip,
			String homePhone, String businssPhone, String accountNumber, String deliveryMethod, String billingLastName,
			String billingFirstName, String billingFirmName, String billingAddress1, String billingAddress2, String billingUnitNum,
			String billingHalfUnitNum, String billingStreetDir, String billingStreetName, String billingStreetType,
			String billingStreetPostDir, String billingSubUnitNum, String billingSubUnitCode, String billingCity,
			String billingState, String billingZip, Double balance, Date expirationDate, Date startDate, boolean active,
			Date lastPaymentReceivedDate, String lastStopCode, boolean subscriptionSuspended, boolean onEZPay, boolean donating,
			String donationCode, String renewalTypeAlpha, String rateCode, String keyCode, boolean oneTimeBill,
			boolean showDeliveryAlert, int numberOfPapers, String billingHomePhone, String billingBusinessPhone,
			String creditCardNum, String creditCardType, String creditCardExp, boolean expiredSubscriptionAccessRenewed,
			String frequencyOfDeliveryCode) {
		super();
		this.pubCode = pubCode;
		this.transDate = transDate;
		this.transRecType = transRecType;
		this.lastName = lastName;
		this.firstName = firstName;
		this.firmName = firmName;
		this.halfUnitNum = halfUnitNum;
		this.streetDir = streetDir;
		this.streetName = streetName;
		this.streetType = streetType;
		this.streetPostDir = streetPostDir;
		this.subUnitCode = subUnitCode;
		this.subUnitNum = subUnitNum;
		this.unitNum = unitNum;
		this.addlAddr1 = addlAddr1;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.homePhone = homePhone;
		this.businssPhone = businssPhone;
		this.accountNumber = accountNumber;
		this.deliveryMethod = deliveryMethod;
		this.billingLastName = billingLastName;
		this.frequencyOfDeliveryCode = frequencyOfDeliveryCode;
		this.billingFirstName = billingFirstName;
		this.billingFirmName = billingFirmName;
		this.billingAddress1 = billingAddress1;
		this.billingAddress2 = billingAddress2;
		this.billingUnitNum = billingUnitNum;
		this.billingHalfUnitNum = billingHalfUnitNum;
		this.billingStreetDir = billingStreetDir;
		this.billingStreetName = billingStreetName;
		this.billingStreetType = billingStreetType;
		this.billingStreetPostDir = billingStreetPostDir;
		this.billingSubUnitNum = billingSubUnitNum;
		this.billingSubUnitCode = billingSubUnitCode;
		this.billingCity = billingCity;
		this.billingState = billingState;
		this.billingZip = billingZip;
		this.billingHomePhone = billingHomePhone;
		this.billingBusinessPhone = billingBusinessPhone;
		this.balance = balance;
		this.expirationDate = expirationDate;
		this.startDate = startDate;
		this.active = active;
		this.lastPaymentReceivedDate = lastPaymentReceivedDate;
		this.lastStopCode = lastStopCode;
		this.subscriptionSuspended = subscriptionSuspended;
		this.onEZPay = onEZPay;
		this.renewalTypeAlpha = renewalTypeAlpha;
		this.rateCode = rateCode;
		this.keyCode = keyCode;
		this.oneTimeBill = oneTimeBill;
		this.showDeliveryAlert = showDeliveryAlert;
		this.numberOfPapers = numberOfPapers;
		this.donatingToNIE = donating;
		this.donationCode = donationCode;
		this.creditCardNum = creditCardNum;
		this.creditCardType = creditCardType;
		this.creditCardExp = creditCardExp;
		this.expiredSubscriptionAccessRenewed = expiredSubscriptionAccessRenewed;

		if (this.billingZip == null || this.billingZip.trim().length() == 0) {
			this.billingSameAsDelivery = true;
		}
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public boolean isActive() {
		return this.active;
	}

	public Double getBalance() {
		return balance;
	}

	public String getBillingAddress1() {
		return this.billingAddress1;
	}

	public String getBillingAddress2() {
		return this.billingAddress2;
	}

	public String getBillingCity() {
		return this.billingCity;
	}

	public String getBillingFirmName() {
		return this.billingFirmName;
	}

	public String getBillingFirstName() {
		return this.billingFirstName;
	}

	public String getBillingHalfUnitNum() {
		return this.billingHalfUnitNum;
	}

	public String getBillingLastName() {
		return this.billingLastName;
	}

	public String getBillingState() {
		return this.billingState;
	}

	public String getBillingStreetDir() {
		return this.billingStreetDir;
	}

	public String getBillingStreetName() {
		return this.billingStreetName;
	}

	public String getBillingStreetPostDir() {
		return this.billingStreetPostDir;
	}

	public String getBillingStreetType() {
		return this.billingStreetType;
	}

	public String getBillingSubUnitCode() {
		return this.billingSubUnitCode;
	}

	public String getBillingSubUnitNum() {
		return this.billingSubUnitNum;
	}

	public String getBillingUnitNum() {
		return this.billingUnitNum;
	}

	public String getBillingZip() {
		return this.billingZip;
	}

	public String getBusinssPhone() {
		return this.businssPhone;
	}

	public String getCity() {
		return this.city;
	}

	public Date getExpirationDate() {
		return this.expirationDate;
	}

	public String getFirmName() {
		return this.firmName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getHalfUnitNum() {
		return this.halfUnitNum;
	}

	public String getHomePhone() {
		return this.homePhone;
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public String getLastName() {
		return this.lastName;
	}

	public Date getLastPaymentReceivedDate() {
		return this.lastPaymentReceivedDate;
	}

	public String getLastStopCode() {
		return this.lastStopCode;
	}

	public boolean isOneTimeBill() {
		return this.oneTimeBill;
	}

	public boolean isShowDeliveryAlert() {
		return this.showDeliveryAlert;
	}

	public boolean isOnEZPay() {
		return this.onEZPay;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public String getRateCode() {
		return this.rateCode;
	}

	public String getRenewalTypeAlpha() {
		return this.renewalTypeAlpha;
	}

	public String getDonationCode() {
		return this.donationCode;
	}

	public String getState() {
		return this.state;
	}

	public String getStreetDir() {
		return this.streetDir;
	}

	public String getStreetName() {
		return this.streetName;
	}

	public String getStreetPostDir() {
		return this.streetPostDir;
	}

	public String getStreetType() {
		return this.streetType;
	}

	public boolean isSubscriptionSuspended() {
		return this.subscriptionSuspended;
	}

	public String getSubUnitCode() {
		return this.subUnitCode;
	}

	public String getSubUnitNum() {
		return this.subUnitNum;
	}

	public String getTransDate() {
		return this.transDate;
	}

	public String getUnitNum() {
		return this.unitNum;
	}

	public String getZip() {
		return this.zip;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public int getNumberOfPapers() {
		return this.numberOfPapers;
	}

	public ContactIntf getBillingContact() {
		// This will be done in the Business object
		return null;
	}

	public RenewalOfferIntf getDefaultRenewalOffer() {
		// This will be done in the business object
		return null;
	}

	public ContactIntf getDeliveryContact() {
		// This will be done in the business object
		return null;
	}

	public String getBillingHomePhone() {
		return this.billingHomePhone;
	}

	public String getBillingBusinessPhone() {
		return this.billingBusinessPhone;
	}

	public String getAddlAddr1() {
		return this.addlAddr1;
	}

	public boolean isBillingSameAsDelivery() {
		return this.billingSameAsDelivery;
	}

	public boolean isDonatingToNIE() {
		return donatingToNIE;
	}

	/**
	 * @return Returns the deliveryMethod.
	 */
	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	/**
	 * @param deliveryMethod
	 *            The deliveryMethod to set.
	 */
	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	/**
	 * @return Returns the creditCardExp.
	 */
	public String getCreditCardExp() {
		return creditCardExp;
	}

	/**
	 * @param creditCardExp
	 *            The creditCardExp to set.
	 */
	public void setCreditCardExp(String creditCardExp) {
		this.creditCardExp = creditCardExp;
	}

	/**
	 * @return Returns the creditCardNum.
	 */
	public String getCreditCardNum() {
		return creditCardNum;
	}

	/**
	 * @param creditCardNum
	 *            The creditCardNum to set.
	 */
	public void setCreditCardNum(String creditCardNum) {
		this.creditCardNum = creditCardNum;
	}

	/**
	 * @return Returns the creditCardType.
	 */
	public String getCreditCardType() {
		return creditCardType;
	}

	/**
	 * @param creditCardType
	 *            The creditCardType to set.
	 */
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	public SubscriptionProductIntf getProduct() {

		return null;
	}

	public boolean isExpiredSubscriptionAccessRenewed() {
		return expiredSubscriptionAccessRenewed;
	}

	public void setExpiredSubscriptionAccessRenewed(boolean expiredSubscriptionAccessRenewed) {
		this.expiredSubscriptionAccessRenewed = expiredSubscriptionAccessRenewed;
	}

	public String getTransRecType() {
		return transRecType;
	}

	public void setTransRecType(String transRecType) {
		this.transRecType = transRecType;
	}

	@Override
	public String getCreditCardExpMonth() {
		// this method is here due to my bad design. I should not have had this class derive from the bo version so return null;
		return null;
	}

	@Override
	public String getCreditCardExpYear() {
		// this method is here due to my bad design. I should not have had this class derive from the bo version so return null;
		return null;
	}

	@Override
	public String getFrequencyOfDeliveryCode() {
		return this.frequencyOfDeliveryCode;
	}

	public void setFrequencyOfDeliveryCode(String frequencyOfDeliveryCode) {
		this.frequencyOfDeliveryCode = frequencyOfDeliveryCode;
	}

}
