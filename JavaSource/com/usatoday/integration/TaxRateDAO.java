/*
 * Created on Apr 19, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.UsatException;

/**
 * @author aeast
 * @date Apr 19, 2006
 * @class TaxRateDAO
 * 
 *        This class interacts with out Tax Table
 * 
 */
public class TaxRateDAO extends USATodayDAO {

	// Pub code equates to the product id or tax rate code
	public final static String TAXTAB_FIELDS = "PubCode, DeliveryTypeCode, TaxRate1, TaxRate2, TaxRate3, TaxRate4, TaxRate5";
	public final static java.lang.String SELECT_TAX_RATES = "SELECT " + TaxRateDAO.TAXTAB_FIELDS
			+ " from TAXTAB where FromZip <= ? and ToZip > ? and ACTIVE = 'Y'";

	private synchronized static Collection<TaxRateTO> objectFactory(ResultSet rs) throws UsatException {
		Collection<TaxRateTO> records = new ArrayList<TaxRateTO>();

		try {
			// iterate over result set and build objects
			String taxCode = "";
			String deliveryTypeCode = "";
			double rate1 = 0.0;
			double rate2 = 0.0;
			double rate3 = 0.0;
			double rate4 = 0.0;
			double rate5 = 0.0;

			while (rs.next()) {

				taxCode = rs.getString("PubCode");
				deliveryTypeCode = rs.getString("DeliveryTypeCode");
				rate1 = (new Double(rs.getString("TaxRate1")).doubleValue());
				rate2 = (new Double(rs.getString("TaxRate2")).doubleValue());
				rate3 = (new Double(rs.getString("TaxRate3")).doubleValue());
				rate4 = (new Double(rs.getString("TaxRate4")).doubleValue());
				rate5 = (new Double(rs.getString("TaxRate5")).doubleValue());

				TaxRateTO rate = new TaxRateTO(taxCode, deliveryTypeCode, rate1, rate2, rate3, rate4, rate5);

				records.add(rate);
			}
		} catch (Exception e) {
			System.out.println("TaxRateDAO::objectFactory() - Failed to create TaxTable Record for zip: " + e.getMessage());
		}

		return records;
	}

	/**
	 * 
	 * @param taxCode
	 * @param zip
	 * @return
	 * @throws UsatException
	 */
	public TaxRateForZipTO getTaxRatesForZip(String zip) throws UsatException {
		TaxRateForZipTO rate = new TaxRateForZipTO();

		if (zip == null) {
			return null;
		}
		rate.setZip(zip);
		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = conn.prepareStatement(TaxRateDAO.SELECT_TAX_RATES);
			statement.setString(1, zip);
			statement.setString(2, zip);

			ResultSet rs = statement.executeQuery();

			Collection<TaxRateTO> records = TaxRateDAO.objectFactory(rs);

			rate.setTaxRates(records);
		} catch (Exception e) {
			System.out.println("EmailRecordDAO : getTaxRateForZip() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return rate;
	}

}
