package com.usatoday.integration;

import java.io.Serializable;

import com.usatoday.business.interfaces.products.suppliers.SupplierTOIntf;

public class SupplierTO implements SupplierTOIntf, Serializable {
	private int id = -1;
	private String name = null;
	private String secretKey = null;
	private String loginUrl = null;
	private String productUrl = null;
	private boolean active = false;
	private String description = null;
	private String sampleURL = null;
	private String tutorialURL = null;
	private String alternateLoginUrl = null;
	private String alteranteProductUrl = null;

	/**
	 * 
	 */
	private static final long serialVersionUID = 3366608535077212972L;

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setID(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSecretKey(String key) {
		this.secretKey = key;
	}

	public String getDescription() {
		return this.description;
	}

	public int getID() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getSecretKey() {
		return this.secretKey;
	}

	public boolean isActive() {
		return this.active;
	}

	public String getLoginURL() {
		return this.loginUrl;
	}

	public String getProductURL() {
		return this.productUrl;
	}

	public void setLoginURL(String url) {
		this.loginUrl = url;

	}

	public void setProductURL(String url) {
		this.productUrl = url;
	}

	public void setSampleURL(String url) {
		this.sampleURL = url;
	}

	public String getSampleURL() {
		return this.sampleURL;
	}

	public void setTutorialURL(String url) {
		this.tutorialURL = url;
	}

	public String getTutorialURL() {
		return this.tutorialURL;
	}

	@Override
	public String getAlternateLoginURL() {
		return this.alternateLoginUrl;
	}

	@Override
	public String getAlternateProductURL() {

		return this.alteranteProductUrl;
	}

	public void setAlternateLoginURL(String alternateLoginUrl) {
		this.alternateLoginUrl = alternateLoginUrl;
	}

	public void setAlteranteProductURL(String alteranteProductUrl) {
		this.alteranteProductUrl = alteranteProductUrl;
	}

}
