/*
 * Created on Sep 23, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.integration;

import java.io.Serializable;
import java.sql.Timestamp;

import com.usatoday.business.interfaces.customer.EeditionTrialAccessIntf;

/**
 * @author aeast
 * 
 * 
 */
public class EeditionTrialAccessTO implements EeditionTrialAccessIntf, Serializable {

	private static final long serialVersionUID = -3998876729457698785L;

	private String active = null;
	private String additionalAddress = null;
	private String address1 = null;
	private String address2 = null;
	private String aptSuite = null;
	private String city = null;
	private String clientIP = null;
	private String companyName = null;
	private String country = null;
	private String emailAddress = null;
	private Timestamp endDate = null;
	private String firstName = null;
	private Timestamp insertDateTime = null;
	private String keyCode = null;
	private String lastName = null;
	private String partnerID = null;
	private String partnerNumber = null;
	private String password = null;
	private String phone = null;
	private String pubCode = null;
	private String repID = null;
	private Timestamp startDate = null;
	private String state = null;
	private String subscribed = null;
	private long transID = 0;
	private String zip = null;

	/**
     *  
     */
	public EeditionTrialAccessTO() {
		super();
	}

	public String getActive() {
		return active;
	}

	public String getAdditionalAddress() {
		return additionalAddress;
	}

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}

	public String getAptSuite() {
		return aptSuite;
	}

	public String getCity() {
		return city;
	}

	public String getClientIP() {
		return clientIP;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getCountry() {
		return country;
	}

	public Timestamp getDateTime() {
		return insertDateTime;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPartnerID() {
		return partnerID;
	}

	public String getPartnerNumber() {
		return partnerNumber;
	}

	public String getPassword() {
		return password;
	}

	public String getPhone() {
		return phone;
	}

	public String getRepID() {
		return repID;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public String getState() {
		return state;
	}

	public String getSubscribed() {
		return subscribed;
	}

	public long getTransID() {
		return transID;
	}

	public String getZip() {
		return zip;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public void setAdditionalAddress(String additionalAddress) {
		this.additionalAddress = additionalAddress;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setAptSuite(String aptSuite) {
		this.aptSuite = aptSuite;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setInsertDateTime(Timestamp dateTime) {
		this.insertDateTime = dateTime;
	}

	public void setKeyCode(String accountNum) {
		this.keyCode = accountNum;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}

	public void setPartnerNumber(String partnerNumber) {
		this.partnerNumber = partnerNumber;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setPubCode(String publication) {
		this.pubCode = publication;
	}

	public void setRepID(String repID) {
		this.repID = repID;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setSubscribed(String subscribed) {
		this.subscribed = subscribed;
	}

	public void setTransID(long transID) {
		this.transID = transID;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Timestamp getInsertDateTime() {
		// TODO Auto-generated method stub
		return insertDateTime;
	}

	public String getKeyCode() {
		// TODO Auto-generated method stub
		return keyCode;
	}

	public String getPubCode() {
		// TODO Auto-generated method stub
		return pubCode;
	}

}