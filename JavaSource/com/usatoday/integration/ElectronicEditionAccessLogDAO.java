package com.usatoday.integration;

import java.sql.Connection;
import java.sql.Timestamp;
import java.sql.Types;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.logging.ElectronicEditionLogTOIntf;

public class ElectronicEditionAccessLogDAO extends USATodayDAO {

	private static final String INSERT_LOG = "insert into usat_eEdition_access_log (pubCode, accountNumber, date_time, clientIP, emailAddress, trial_access) values "
			+ "(?, ?, ?, ?, ?, ?);";

	public void insert(ElectronicEditionLogTOIntf eLog) throws UsatException {
		Connection conn = null;

		if (eLog == null) {
			return;
		}

		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(ElectronicEditionAccessLogDAO.INSERT_LOG);

			// pubCode, accountNumber, date_time, clientIP, emailAddress, partner_id, partner_number, trial_access,
			if (eLog.getPubCode() != null) {
				statement.setString(1, eLog.getPubCode());
			} else {
				statement.setNull(1, Types.CHAR);
			}

			if (eLog.getAccountNumber() != null) {
				statement.setString(2, eLog.getAccountNumber());
			} else {
				statement.setNull(2, Types.CHAR);
			}

			Timestamp ts = new Timestamp(System.currentTimeMillis());
			statement.setTimestamp(3, ts);

			if (eLog.getClientIP() != null) {
				statement.setString(4, eLog.getClientIP());
			} else {
				statement.setNull(4, Types.CHAR);
			}

			if (eLog.getEmailAddress() != null) {
				statement.setString(5, eLog.getEmailAddress());
			} else {
				statement.setNull(5, Types.CHAR);
			}

			if (eLog.isTrialAccess()) {
				statement.setString(6, "Y");
			} else {
				statement.setString(6, "N");
			}

			// execute the SQL
			statement.execute();

			if (statement.getUpdateCount() == 0) {
				throw new UsatException("ElectronicEditionAccessLogDAO: No Records Inserted and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

}
