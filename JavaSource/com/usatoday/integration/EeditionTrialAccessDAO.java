package com.usatoday.integration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.EeditionTrialAccessIntf;

public class EeditionTrialAccessDAO extends USATodayDAO {

	// Delete a record into the usat_eedition_trial_access table
	/*
	 * public void delete(EeditionTrialAccessIntf EeditionTrialAccessIntf) throws UsatException { int rowsAffected = 0;
	 * 
	 * if (EeditionTrialAccessIntf == null) { throw new UsatException("Null USAT Tech Guide Order object passed in for insert."); }
	 * 
	 * java.sql.Connection conn = null; try {
	 * 
	 * conn = ConnectionManager.getInstance().getConnection();
	 * 
	 * java.sql.Statement statement = conn.createStatement();
	 * 
	 * StringBuffer sql = new StringBuffer("delete from usat_eedition_trial_access where ID = " +
	 * EeditionTrialAccessIntf.getTransID()); // Fill in the SQL statement
	 * 
	 * rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);
	 * 
	 * if (rowsAffected != 1) { throw new UsatException(
	 * "No Eedition Trial Access Object Updated Or other error and yet no exception condition exists: Number rows affected: " +
	 * rowsAffected); }
	 * 
	 * if (statement.getUpdateCount() == 0) { throw new NullPointerException(
	 * "usat_eedition_trial_access: No Records Deleted and yet no exception condition exists"); }
	 * 
	 * statement.close();
	 * 
	 * } catch (java.sql.SQLException e) { e.printStackTrace(); System.out.println(e.getMessage()); throw new
	 * UsatException(e.getClass().getName() + " : " + e.getMessage()); } catch (Exception e) { e.printStackTrace();
	 * System.out.println(e.getMessage()); throw new UsatException(e.getClass().getName() + " : " + e.getMessage()); } finally {
	 * this.cleanupConnection(conn); }
	 * 
	 * }
	 */
	// Update a record into the usat_eEdition_trial_access table
	public void selectForFTP(EeditionTrialAccessIntf EeditionTrialAccessIntf) throws UsatException {
		int rowsAffected = 0;

		if (EeditionTrialAccessIntf == null) {
			throw new UsatException("Null USAT Tech Guide Order object passed in for insert.");
		}

		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();
			// Fill in the SQL statement
			StringBuffer sql = new StringBuffer("select * from usat_eedition_trial_access");

			rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Eedition Access Log Object Updated Or other error and yet no exception condition exists: Number rows affected: "
								+ rowsAffected);
			}

			if (statement.getUpdateCount() == 0) {
				throw new NullPointerException(
						"usat_eedition_trial_access: No Records Updated and yet no exception condition exists");
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

	}

	/**
	 * 
	 * @param numDays
	 * @return
	 * @throws UsatException
	 */
	/*
	 * public int deleteTrialAccessRecords() throws UsatException { int rowsAffected = 0; Connection conn = null; try {
	 * 
	 * conn = ConnectionManager.getInstance().getConnection();
	 * 
	 * java.sql.PreparedStatement statement =
	 * conn.prepareStatement(EeditionTrialAccessDAO.DELETE_TRIAL_ACCESS_ARRTIBUTES_IN_DELETE_STATE_OLDER_THAN_DATE);
	 * 
	 * rowsAffected = statement.executeUpdate(); } catch (java.sql.SQLException e) { e.printStackTrace(); throw new
	 * UsatException(e.getClass().getName() + " : " + e.getMessage()); } catch (Exception e) { e.printStackTrace(); throw new
	 * UsatException(e.getClass().getName() + " : " + e.getMessage()); } finally { this.cleanupConnection(conn); } return
	 * rowsAffected; }
	 */
	private Collection<EeditionTrialAccessIntf> fetchTrialAccess() throws UsatException {
		Collection<EeditionTrialAccessIntf> records = null;
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn
					.prepareStatement(EeditionTrialAccessDAO.FETCH_TRIAL_ACCESS_ATTRIBUTES_IN_STATE);

			ResultSet rs = statement.executeQuery();

			records = EeditionTrialAccessDAO.objectFactory(rs);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return records;
	}

	public Collection<EeditionTrialAccessIntf> fetchTrialAccessRecordsForBatchProcessing() throws UsatException {
		return this.fetchTrialAccess();
	}

	// private static final String DELETE_TRIAL_ACCESS_ARRTIBUTES_IN_DELETE_STATE_OLDER_THAN_DATE =
	// "delete from usat_eedition_trial_access";
	private static final String FETCH_TRIAL_ACCESS_ATTRIBUTES_IN_STATE = "select * from usat_eedition_trial_access";

	private synchronized static Collection<EeditionTrialAccessIntf> objectFactory(ResultSet rs) throws UsatException {
		Collection<EeditionTrialAccessIntf> records = new ArrayList<EeditionTrialAccessIntf>();

		try {
			// iterate over result set and build objects
			while (rs.next()) {

				long primaryKey = rs.getLong("id");

				String tempStr = null;

				EeditionTrialAccessTO attribute = new EeditionTrialAccessTO();

				attribute.setTransID(primaryKey);

				tempStr = rs.getString("subscribed");
				if (tempStr != null) {
					attribute.setSubscribed(tempStr.trim());
				}

				tempStr = rs.getString("pubcode");
				if (tempStr != null) {
					attribute.setPubCode(tempStr.trim());
				}

				tempStr = rs.getString("email");
				if (tempStr != null) {
					attribute.setEmailAddress(tempStr.trim());
				}

				tempStr = rs.getString("password");
				if (tempStr != null) {
					attribute.setPassword(tempStr.trim());
				}

				Timestamp ts = rs.getTimestamp("start_date");
				if (ts != null) {
					attribute.setStartDate(ts);
				}

				ts = rs.getTimestamp("end_date");
				if (ts != null) {
					attribute.setEndDate(ts);
				}

				ts = rs.getTimestamp("insert_timestamp");
				if (ts != null) {
					attribute.setInsertDateTime(ts);
				}

				tempStr = rs.getString("keycode");
				if (tempStr != null) {
					attribute.setKeyCode(tempStr.trim());
				}

				tempStr = rs.getString("first_name");
				if (tempStr != null) {
					attribute.setFirstName(tempStr.trim());
				}

				tempStr = rs.getString("last_name");
				if (tempStr != null) {
					attribute.setLastName(tempStr.trim());
				}

				tempStr = rs.getString("company_name");
				if (tempStr != null) {
					attribute.setCompanyName(tempStr.trim());
				}

				tempStr = rs.getString("address1");
				if (tempStr != null) {
					attribute.setAddress1(tempStr.trim());
				}

				tempStr = rs.getString("address2");
				if (tempStr != null) {
					attribute.setAddress2(tempStr.trim());
				}

				tempStr = rs.getString("apt_suite");
				if (tempStr != null) {
					attribute.setAptSuite(tempStr.trim());
				}

				tempStr = rs.getString("additionalAddress");
				if (tempStr != null) {
					attribute.setAdditionalAddress(tempStr.trim());
				}

				tempStr = rs.getString("city");
				if (tempStr != null) {
					attribute.setCity(tempStr.trim());
				}

				tempStr = rs.getString("state");
				if (tempStr != null) {
					attribute.setState(tempStr.trim());
				}

				tempStr = rs.getString("zip");
				if (tempStr != null) {
					attribute.setZip(tempStr);
				}

				tempStr = rs.getString("country");
				if (tempStr != null) {
					attribute.setCountry(tempStr.trim());
				}

				tempStr = rs.getString("phone");
				if (tempStr != null) {
					attribute.setPhone(tempStr);
				}

				tempStr = rs.getString("ip");
				if (tempStr != null) {
					attribute.setClientIP(tempStr);
				}

				tempStr = rs.getString("partner_id");
				if (tempStr != null) {
					attribute.setPartnerID(tempStr);
				}

				tempStr = rs.getString("partner_number");
				if (tempStr != null) {
					attribute.setPartnerNumber(tempStr);
				}

				tempStr = rs.getString("active");
				if (tempStr != null) {
					attribute.setActive(tempStr);
				}

				tempStr = rs.getString("rep_id");
				if (tempStr != null) {
					attribute.setRepID(tempStr);
				}

				records.add(attribute);
			}
		} catch (Exception e) {
			System.out.println("EeditionTrialAccessDAO::objectFactory() - Failed to create Eedition Access Log Record TO : "
					+ e.getMessage());
		}

		return records;
	}

}
