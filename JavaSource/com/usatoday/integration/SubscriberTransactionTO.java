package com.usatoday.integration;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.subscriptionTransactions.ExtranetSubscriberTransactionIntf;

public class SubscriberTransactionTO implements ExtranetSubscriberTransactionIntf, Serializable {

	private static final long serialVersionUID = -6641749071146570152L;
	private long id = -1; // [id]
	private String accountNum = ""; // CurrAcctNum
	private String aBCCode = ""; // ABCCode
	private String active = ""; // ACTIVE
	private String additionalAddr1 = ""; // AddAddr1
	private String additionalAddr2 = ""; // AddAddr2
	private String addrType = ""; // AddrTyp
	private String agencyName = ""; // AgencyName
	private String appFlag = ""; // AppFlag
	private String autoRegComm = ""; // AutoRegComm
	private String badSubAddress = ""; // BadSubAddress
	private String badTrnGftAddress = ""; // BadTrnGftAddress
	private String bankAccountNum = ""; // BankActNum
	private String bankRouteNum = ""; // BankRtNum
	private String batchNum = ""; // BatchNum
	private String billChargePd = ""; // BillChargePd
	private String billingAddress1 = ""; // BillAddr1
	private String billingAddress2 = ""; // BillAddr2
	private String billingBusinessPhone = ""; // BillBusPhone
	private String billingCity = ""; // BillCity
	private String billingShortCity = ""; // BillShortCity
	private String billingFirmName = ""; // BillFirmName
	private String billingFirstName = ""; // BillFirstName
	private String billingHalfUnitNum = ""; // BillHalfUnitNum
	private String billingHomePhone = ""; // BillHomePhone
	private String billingLastName = ""; // BillLastName
	private String billingState = ""; // BillState
	private String billingStreetDir = ""; // BillStreetDir
	private String billingStreetName = ""; // BillStreetName
	private String billingStreetPostDir = ""; // BillStreetPostDir
	private String billingStreetType = ""; // BillStreetType
	private String billingSubUnitCode = ""; // BillSubUnitCode
	private String billingSubUnitNum = ""; // BillSubUnitNum
	private String billingUnitNum = ""; // BillUnitNum
	private String billingZip5 = ""; // BillZip5
	private String billingZip4 = ""; // BillZip4
	private String billingDelPtBarCode = ""; // BillDelPtBarCode
	private String billingCarrRteSort = ""; // BillCarrRteSort
	private String billingAddrType = ""; // BillAddrType
	private String businessPhone = ""; // BusPhone
	private String carrRouteSort = ""; // CarrRteSort
	private String cCCIDCode = ""; // CCCIDCode
	private String checkNum = ""; // CheckNum
	private String censusTract = ""; // CensusTract
	private String censusBlkGrp = ""; // CensusBlkGrp
	private String city = ""; // City
	private String shortCity = ""; // ShortCity
	private String clubNum = ""; // ClubNum
	private String clubType = ""; // ClubType
	private String code1ErrorCode = ""; // Code1ErrCode
	private String comment = ""; // Comment
	private String contestCode = ""; // ContestCode
	private String creditCardAuthCode = ""; // CCAuthCode
	private String creditCardAuthDate = ""; // CCAuthDate
	private String creditCardBatchProcess = ""; // CCBatchProc
	private String creditCardExpirationDate = ""; // CCExpireDt
	private String creditCardNumber = ""; // CCNum
	private String creditCardType = ""; // CCType
	private String creditCardAuthRejDesc = ""; // CCAuthRejDesc
	private String currAddrNum = ""; // CurrAddrNum
	private String currMarketCode = ""; // CurrMktcode
	private String currPrtSiteCode = ""; // CurrPrtSiteCode
	private String creditAdjAmount = ""; // CreditAdjAmount
	private String debtAdjAmount = ""; // DebtAdjAmount
	private String deliveryPreference = ""; // DelPref
	private String delPtBarCode = ""; // DelPtBarCode
	private String deptRef = ""; // DeptRef
	private String district = ""; // District
	private String discAgncyFlg = ""; // DiscAgncyFlg
	private String effectiveDate = ""; // EffDate1
	private String effectiveDate2 = ""; // EffDate2
	private String emailAddress = ""; // EmailAddress
	private String emailType = ""; // EmailType
	private String entryMethod = ""; // EntryMethod
	private String filler = ""; // Filler
	private String firmName = ""; // FirmName
	private String firstName = ""; // FirstName
	private String fIPSCounty = ""; // FIPSCounty
	private String fIPSState = ""; // FIPSState
	private String frequencyOfDelivery = ""; // FreqDel
	private String fulfillItem = ""; // FulFillItem
	private String gatedCommFlg = ""; // GatedCommFlg
	private String gateSecCode = ""; // GateSecCode
	private String giftCard = ""; // GiftCard
	private String giftPayerTMC = ""; // GiftPayerTMC
	private String halfUnitNum = ""; // HalfUnitNum
	private String issuePdUnpd = ""; // IssuePdUnpd
	private String infoRTY2Based = ""; // InfoRTY2Based
	private String homePhone = ""; // HomePhone
	private String lastName = ""; // LastName
	private String latitude = ""; // Latitude
	private String longitude = ""; // Longtitude (misspelled database column)
	private String marketCode = ""; // MarketCode
	private String marketSeg = ""; // MktSeg
	private String micrTranCode = ""; // MicrTranCode
	private String newAddlAddr1 = ""; // NewAddlAddr1
	private String newAddlAddr2 = ""; // NewAddlAddr2
	private String numPaper = ""; // NumPaper
	private String oneTimeBill = ""; // OneTimeBill
	private String operatorID = ""; // OperID
	private String overrideCode1 = ""; // OvrCode1
	private String payAmount = ""; // PayAmount
	private String perpCCFlag = ""; // PerpCCFlag
	private String piaTranCode = ""; // PIATranCode
	private String pndLookSts = ""; // PndLookSts
	private String premium = ""; // Premium
	private String premiumType = ""; // PremType
	private String premiumAttr = ""; // PremAttr
	private String printFlag = ""; // PrintFlag
	private String prizmCode = ""; // PrizmCode
	private String prizmGroup = ""; // PrizmGrp
	private String promoCode = ""; // PromoCode
	private String pubCode = ""; // PubCode
	private String purchaseOrderNum = ""; // PurOrdNum
	private String rateCode = ""; // RateCode
	private String rebillCode = ""; // RebillCode
	private String rejComment = ""; // RejComm
	private String radPostdDt = ""; // RadPostdDt
	private String radPostdTm = ""; // RadPostdTm
	private String route = ""; // Route
	private String salesRep = ""; // SalesRep
	private String specialRunCode = ""; // SpecialRunCode
	private String srcOrdCode = ""; // SrcOrdCode
	private String state = ""; // State
	private String status = ""; // Status
	private String soldBy = ""; // SoldBy
	private String streetDir = ""; // StreetDir
	private String streetName = ""; // StreetName
	private String streetPostDir = ""; // StreetPstDir
	private String streetType = ""; // StreetTyp
	private String subscriptionAmount = ""; // SubsAmount
	private String subscriptionDuration = ""; // SubsDur
	private String subsType = ""; // SubsType
	private String subUnitCode = ""; // SubUnitCode
	private String subUnitNum = ""; // SubUnitNum
	private String taxExempt = ""; // TaxExempt
	private String transactionCode = ""; // TranCode
	private String transactionDate = ""; // [TranDate]
	private String tranSeqNum = ""; // [TranSeqNum]
	private String transactionRecType = ""; // TranRecTyp
	private int transactionState = 0; // TransactionState
	private String transferType = ""; // TranferType
	private String tRFSecRecType = ""; // TRFSecRecType
	private String transactionType = ""; // TranType
	private String ttlMktCov = ""; // TtlMktCov
	private String unitNum = ""; // UnitNum
	private String zip5 = ""; // Zip5
	private String zip4 = ""; // Zip4
	private String newAddrType = ""; // NewAddrType
	private String NewLastName = ""; // NewLastName
	private String NewFirstName = ""; // NewFirstName
	private String NewFirmName = ""; // NewFirmName
	private String NewUnitNum = ""; // NewUnitNum
	private String NewHalfUnitNum = ""; // NewHalfUnitNum
	private String NewStreetDir = ""; // NewStreetDir
	private String NewStreetName = ""; // NewStreetName
	private String NewStreetType = ""; // NewStreetType
	private String NewStreetPostDir = ""; // NewStreetPostDir
	private String NewSubUnitNum = ""; // NewSubUnitNum
	private String NewSubUnitCode = ""; // NewSubUnitCode
	private String NewCity = ""; // NewCity
	private String NewShortCity = ""; // NewShortCity
	private String NewState = ""; // NewState
	private String NewZip = ""; // NewZip5
	private String NewZip4 = ""; // NewZip4
	private String NewPhoneNum = ""; // NewPhoneNum
	private String NewBusPhoneNum = ""; // NewBusPhoneNum
	private String NewCarrRteSort = ""; // NewCarrRteSort
	private String NewDelPtBarCode = ""; // NewDelPtBarCode
	private String NewGatedCommFlag = ""; // NewGatedCommFlag
	private String NewGatedCommSecCode = ""; // NewGatedCommSecCode
	private String NewAcctNum = ""; // NewAcctNum
	private String NewAddrNum = ""; // NewAddrNum
	private String NewCensusBlkGrp = ""; // NewCensusBlkGrp
	private String NewCensusTrack = ""; // NewCensusTrack
	private String NewDelMethod = ""; // NewDelMethod
	private String NewFIPSCounty = ""; // NewFIPSCounty
	private String NewFIPSState = ""; // NewFIPSState
	private String NewLatitude = ""; // NewLatitude
	private String NewLongitude = ""; // NewLongitude
	private String NewMktcode = ""; // NewMktcode
	private String NewMktSeg = ""; // NewMktSeg
	private String NewPrintsiteCode = ""; // NewPrintsiteCode
	private String NewPrizmGrp = ""; // NewPrizmGrp
	private String NewPrizmCode = ""; // NewPrizmCode
	private String NewSiteCode = ""; // NewSiteCode
	private String deleteGiftPayer = ""; // DltGiftPayer
	private String creditDays1 = ""; // CreditDay1
	private String creditDays2 = ""; // CreditDay2
	private String creditDays3 = ""; // CreditDay3
	private String creditDays4 = ""; // CreditDay4
	private String creditDays5 = ""; // CreditDay5
	private String creditDays6 = ""; // CreditDay6
	private String creditDays7 = ""; // CreditDay7
	private String contactCustomer = ""; // ContactCust
	private String existingDeliveryMethod = ""; // ExstDelMethod
	private String VIPLabel = ""; // VIPLabel
	private DateTime insertTimestamp = null; // insert_timestamp
	private DateTime updateTimestamp = null; // update_timestamp
	private String batchID = ""; // batch_id
	private String VicsSrcOrdCode = ""; // VicsSrcOrdCode
	private String VicsSubsType = ""; // VicsSubsType
	private String VicsFreqDel = ""; // VicsFreqDel
	private String VicsNumPaper = ""; // VicsNumPaper
	private String VicsRateCode = ""; // VicsRateCode
	private String VicsSubsLngth = ""; // VicsSubsLngth
	private String VicsSoldBy = ""; // VicsSoldBy
	private String VicsPromoCode = ""; // VicsPromoCode
	private String VicsContestCode = ""; // VicsContestCode
	private String VicsRebilCode = ""; // VicsRebilCode
	private String VicsSpecialRunCode = ""; // VicsSpecialRunCode
	private String WebID = ""; // WebID
	private String Zone = ""; // Zone

	public String getAccountNum() {
		return accountNum;
	}

	public String getAddAddr1() {
		return this.additionalAddr1;
	}

	public String getAddAddr2() {
		return this.additionalAddr2;
	}

	public String getAdditionalAddr1() {
		return additionalAddr1;
	}

	public String getAdditionalAddr2() {
		return additionalAddr2;
	}

	public String getAutoRegComm() {
		return this.autoRegComm;
	}

	public String getBadSubAddress() {
		return this.badSubAddress;
	}

	public String getBadTrnGftAddress() {
		return this.badTrnGftAddress;
	}

	public String getBillChargePd() {
		return this.billChargePd;
	}

	public String getBillingAddress1() {
		return this.billingAddress1;
	}

	public String getBillingAddress2() {
		return this.billingAddress2;
	}

	public String getBillingBusinessPhone() {
		return this.billingBusinessPhone;
	}

	public String getBillingCity() {
		return this.billingCity;
	}

	public String getBillingFirmName() {
		return this.billingFirmName;
	}

	public String getBillingFirstName() {
		return this.billingFirstName;
	}

	public String getBillingHalfUnitNum() {
		return this.billingHalfUnitNum;
	}

	public String getBillingHomePhone() {
		return this.billingHomePhone;
	}

	public String getBillingLastName() {
		return this.billingLastName;
	}

	public String getBillingState() {
		return this.billingState;
	}

	public String getBillingStreetDir() {
		return this.billingStreetDir;
	}

	public String getBillingStreetName() {
		return this.billingStreetName;
	}

	public String getBillingStreetPostDir() {
		return this.billingStreetPostDir;
	}

	public String getBillingStreetType() {
		return this.billingStreetType;
	}

	public String getBillingSubUnitCode() {
		return this.billingSubUnitCode;
	}

	public String getBillingSubUnitNum() {
		return this.billingSubUnitNum;
	}

	public String getBillingUnitNum() {
		return this.billingUnitNum;
	}

	public String getBillingZip5() {
		return this.billingZip5;
	}

	public String getBusinessPhone() {
		return this.businessPhone;
	}

	public String getCity() {
		return this.city;
	}

	public String getClubNum() {
		return this.clubNum;
	}

	public String getCode1ErrorCode() {
		return this.code1ErrorCode;
	}

	public String getComment() {
		return this.comment;
	}

	public String getContestCode() {
		return this.contestCode;
	}

	public String getCreditCardAuthCode() {
		return this.creditCardAuthCode;
	}

	public String getCreditCardAuthDate() {
		return this.creditCardAuthDate;
	}

	public String getCreditCardBatchProcess() {
		return this.creditCardBatchProcess;
	}

	public String getCreditCardExpirationDate() {
		return this.creditCardExpirationDate;
	}

	public String getCreditCardNumber() {
		return this.creditCardNumber;
	}

	public String getCreditCardType() {
		return this.creditCardType;
	}

	public String getDeliveryPreference() {
		return this.deliveryPreference;
	}

	public String getEffectiveDate() {
		return this.effectiveDate;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public String getEmailType() {
		return this.emailType;
	}

	public String getFiller() {
		return this.filler;
	}

	public String getFirmName() {
		return this.firmName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getHalfUnitNum() {
		return this.halfUnitNum;
	}

	public String getHomePhone() {
		return this.homePhone;
	}

	public String getLastName() {
		return this.lastName;
	}

	public String getNewAddlAddr1() {
		return this.newAddlAddr1;
	}

	public String getNewAddlAddr2() {
		return this.newAddlAddr2;
	}

	public String getNumPaper() {
		return this.numPaper;
	}

	public String getOneTimeBill() {
		return this.oneTimeBill;
	}

	public String getPayAmount() {
		return this.payAmount;
	}

	public String getPerpCCFlag() {
		return this.perpCCFlag;
	}

	public String getPremium() {
		return this.premium;
	}

	public String getPrintFlag() {
		return this.printFlag;
	}

	public String getPromoCode() {
		return this.promoCode;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public String getRateCode() {
		return this.rateCode;
	}

	public String getRejComment() {
		return this.rejComment;
	}

	public String getSrcOrdCode() {
		return this.srcOrdCode;
	}

	public String getState() {
		return this.state;
	}

	public String getStatus() {
		return this.status;
	}

	public String getStreetDir() {
		return this.streetDir;
	}

	public String getStreetName() {
		return this.streetName;
	}

	public String getStreetPostDir() {
		return this.streetPostDir;
	}

	public String getStreetType() {
		return this.streetType;
	}

	public String getSubscriptionAmount() {
		return this.subscriptionAmount;
	}

	public String getSubscriptionDuration() {
		return this.subscriptionDuration;
	}

	public String getSubUnitCode() {
		return this.subUnitCode;
	}

	public String getSubUnitNum() {
		return this.subUnitNum;
	}

	public String getTransactionCode() {
		return this.transactionCode;
	}

	public String getTransactionDate() {
		return this.transactionDate;
	}

	public String getTransactionRecType() {
		return this.transactionRecType;
	}

	public String getTransactionType() {
		return this.transactionType;
	}

	public String getTtlMktCov() {
		return this.ttlMktCov;
	}

	public String getUnitNum() {
		return this.unitNum;
	}

	public String getZip5() {
		return this.zip5;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public void setAddAddr1(String additionalAddr1) {
		this.additionalAddr1 = additionalAddr1;
	}

	public void setAddAddr2(String additionalAddr2) {
		this.additionalAddr2 = additionalAddr2;
	}

	public void setAdditionalAddr1(String additionalAddr1) {
		this.additionalAddr1 = additionalAddr1;
	}

	public void setAdditionalAddr2(String additionalAddr2) {
		this.additionalAddr2 = additionalAddr2;
	}

	public void setAutoRegComm(String autoRegComm) {
		this.autoRegComm = autoRegComm;
	}

	public void setBadSubAddress(String badSubAddress) {
		this.badSubAddress = badSubAddress;
	}

	public void setBadTrnGftAddress(String badTrnGftAddress) {
		this.badTrnGftAddress = badTrnGftAddress;
	}

	public void setBillChargePd(String billChargePd) {
		this.billChargePd = billChargePd;
	}

	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}

	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}

	public void setBillingBusinessPhone(String billingBusinessPhone) {
		this.billingBusinessPhone = billingBusinessPhone;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public void setBillingFirmName(String billingFirmName) {
		this.billingFirmName = billingFirmName;
	}

	public void setBillingFirstName(String billingFirstName) {
		this.billingFirstName = billingFirstName;
	}

	public void setBillingHalfUnitNum(String billingHalfUnitNum) {
		this.billingHalfUnitNum = billingHalfUnitNum;
	}

	public void setBillingHomePhone(String billingHomePhone) {
		this.billingHomePhone = billingHomePhone;
	}

	public void setBillingLastName(String billingLastName) {
		this.billingLastName = billingLastName;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public void setBillingStreetDir(String billingStreetDir) {
		this.billingStreetDir = billingStreetDir;
	}

	public void setBillingStreetName(String billingStreetName) {
		this.billingStreetName = billingStreetName;
	}

	public void setBillingStreetPostDir(String billingStreetPostDir) {
		this.billingStreetPostDir = billingStreetPostDir;
	}

	public void setBillingStreetType(String billingStreetType) {
		this.billingStreetType = billingStreetType;
	}

	public void setBillingSubUnitCode(String billingSubUnitCode) {
		this.billingSubUnitCode = billingSubUnitCode;
	}

	public void setBillingSubUnitNum(String billingSubUnitNum) {
		this.billingSubUnitNum = billingSubUnitNum;
	}

	public void setBillingUnitNum(String billingUnitNum) {
		this.billingUnitNum = billingUnitNum;
	}

	public void setBillingZip5(String billingZip5) {
		this.billingZip5 = billingZip5;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setClubNum(String clubNum) {
		this.clubNum = clubNum;
	}

	public void setCode1ErrorCode(String code1ErrorCode) {
		this.code1ErrorCode = code1ErrorCode;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setContestCode(String contestCode) {
		this.contestCode = contestCode;
	}

	public void setCreditCardAuthCode(String creditCardAuthCode) {
		this.creditCardAuthCode = creditCardAuthCode;
	}

	public void setCreditCardAuthDate(String creditCardAuthDate) {
		this.creditCardAuthDate = creditCardAuthDate;
	}

	public void setCreditCardBatchProcess(String creditCardBatchProcess) {
		this.creditCardBatchProcess = creditCardBatchProcess;
	}

	public void setCreditCardExpirationDate(String creditCardExpirationDate) {
		this.creditCardExpirationDate = creditCardExpirationDate;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	public void setDeliveryPreference(String deliveryPreference) {
		this.deliveryPreference = deliveryPreference;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public void setFiller(String filler) {
		this.filler = filler;
	}

	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setHalfUnitNum(String halfUnitNum) {
		this.halfUnitNum = halfUnitNum;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setNewAddlAddr1(String newAddlAddr1) {
		this.newAddlAddr1 = newAddlAddr1;
	}

	public void setNewAddlAddr2(String newAddlAddr2) {
		this.newAddlAddr2 = newAddlAddr2;
	}

	public void setNumPaper(String numPaper) {
		this.numPaper = numPaper;
	}

	public void setOneTimeBill(String oneTimeBill) {
		this.oneTimeBill = oneTimeBill;
	}

	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}

	public void setPerpCCFlag(String perpCCFlag) {
		this.perpCCFlag = perpCCFlag;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public void setPrintFlag(String printFlag) {
		this.printFlag = printFlag;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public void setRejComment(String rejComment) {
		this.rejComment = rejComment;
	}

	public void setSrcOrdCode(String srcOrdCode) {
		this.srcOrdCode = srcOrdCode;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setStreetDir(String streetDir) {
		this.streetDir = streetDir;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public void setStreetPostDir(String streetPostDir) {
		this.streetPostDir = streetPostDir;
	}

	public void setStreetType(String streetType) {
		this.streetType = streetType;
	}

	public void setSubscriptionAmount(String subscriptionAmount) {
		this.subscriptionAmount = subscriptionAmount;
	}

	public void setSubscriptionDuration(String subscriptionDuration) {
		this.subscriptionDuration = subscriptionDuration;
	}

	public void setSubUnitCode(String subUnitCode) {
		this.subUnitCode = subUnitCode;
	}

	public void setSubUnitNum(String subUnitNum) {
		this.subUnitNum = subUnitNum;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public void setTransactionRecType(String transactionRecType) {
		this.transactionRecType = transactionRecType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public void setTtlMktCov(String ttlMktCov) {
		this.ttlMktCov = ttlMktCov;
	}

	public void setUnitNum(String unitNum) {
		this.unitNum = unitNum;
	}

	public void setZip5(String zip5) {
		this.zip5 = zip5;
	}

	public String getNewLastName() {
		return NewLastName;
	}

	public String getNewFirstName() {
		return NewFirstName;
	}

	public String getNewFirmName() {
		return NewFirmName;
	}

	public String getNewUnitNum() {
		return NewUnitNum;
	}

	public String getNewHalfUnitNum() {
		return NewHalfUnitNum;
	}

	public String getNewStreetDir() {
		return NewStreetDir;
	}

	public String getNewStreetName() {
		return NewStreetName;
	}

	public String getNewStreetType() {
		return NewStreetType;
	}

	public String getNewStreetPostDir() {
		return NewStreetPostDir;
	}

	public String getNewSubUnitNum() {
		return NewSubUnitNum;
	}

	public String getNewSubUnitCode() {
		return NewSubUnitCode;
	}

	public String getNewCity() {
		return NewCity;
	}

	public String getNewState() {
		return NewState;
	}

	public String getNewZip() {
		return NewZip;
	}

	public String getNewPhoneNum() {
		return NewPhoneNum;
	}

	public String getNewBusPhoneNum() {
		return NewBusPhoneNum;
	}

	public void setNewLastName(String newLastName) {
		NewLastName = newLastName;
	}

	public void setNewFirstName(String newFirstName) {
		NewFirstName = newFirstName;
	}

	public void setNewFirmName(String newFirmName) {
		NewFirmName = newFirmName;
	}

	public void setNewUnitNum(String newUnitNum) {
		NewUnitNum = newUnitNum;
	}

	public void setNewHalfUnitNum(String newHalfUnitNum) {
		NewHalfUnitNum = newHalfUnitNum;
	}

	public void setNewStreetDir(String newStreetDir) {
		NewStreetDir = newStreetDir;
	}

	public void setNewStreetName(String newStreetName) {
		NewStreetName = newStreetName;
	}

	public void setNewStreetType(String newStreetType) {
		NewStreetType = newStreetType;
	}

	public void setNewStreetPostDir(String newStreetPostDir) {
		NewStreetPostDir = newStreetPostDir;
	}

	public void setNewSubUnitNum(String newSubUnitNum) {
		NewSubUnitNum = newSubUnitNum;
	}

	public void setNewSubUnitCode(String newSubUnitCode) {
		NewSubUnitCode = newSubUnitCode;
	}

	public void setNewCity(String newCity) {
		NewCity = newCity;
	}

	public void setNewState(String newState) {
		NewState = newState;
	}

	public void setNewZip(String newZip) {
		NewZip = newZip;
	}

	public void setNewPhoneNum(String newPhoneNum) {
		NewPhoneNum = newPhoneNum;
	}

	public void setNewBusPhoneNum(String newBusPhoneNum) {
		NewBusPhoneNum = newBusPhoneNum;
	}

	public String getDeleteGiftPayer() {
		return deleteGiftPayer;
	}

	public void setDeleteGiftPayer(String deleteGiftPayer) {
		this.deleteGiftPayer = deleteGiftPayer;
	}

	public String getMicrTranCode() {
		return micrTranCode;
	}

	public void setMicrTranCode(String micrTranCode) {
		this.micrTranCode = micrTranCode;
	}

	public String getCreditDays1() {
		return creditDays1;
	}

	public void setCreditDays1(String creditDays1) {
		this.creditDays1 = creditDays1;
	}

	public String getCreditDays3() {
		return creditDays3;
	}

	public void setCreditDays3(String creditDays3) {
		this.creditDays3 = creditDays3;
	}

	public String getContactCustomer() {
		return contactCustomer;
	}

	public void setContactCustomer(String contactCustomer) {
		this.contactCustomer = contactCustomer;
	}

	@Override
	public String getExistingDeliveryMethod() {
		return this.existingDeliveryMethod;
	}

	public void setExistingDeliveryMethod(String method) {
		this.existingDeliveryMethod = method;
	}

	@Override
	public DateTime getInsertTimestamp() {
		return this.insertTimestamp;
	}

	public void setInsertTimestamp(DateTime timestamp) {
		this.insertTimestamp = timestamp;
	}

	@Override
	public DateTime getUpdateTimestamp() {
		return this.updateTimestamp;
	}

	public void setUpdateTimestamp(DateTime timestamp) {
		this.updateTimestamp = timestamp;
	}

	@Override
	public String getBatchID() {
		return this.batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	@Override
	public long getID() {
		return this.id;
	}

	public void setID(long primaryKey) {
		this.id = primaryKey;
	}

	/**
	 * The order of the string build in this method is critical. It is used to build the fixed format file sent to the iSeries for
	 * Transaction downloads. Use Extreme caution when modifying.
	 **/
	@Override
	public String getTransactionCSVExportString() throws UsatException {

		if (this.id < 0) {
			throw new UsatException("Only saved records may be exported.");
		}

		StringBuilder sb = new StringBuilder();

		try {

			sb.append(String.format("%-2.2s", this.getPubCode()));
			sb.append(String.format("%8.8s", this.getTransactionDate()));
			sb.append(String.format("%5.5s", this.getTranSeqNum())); // transaction sequence #
			sb.append(String.format("%-2.2s", this.getTransactionRecType()));
			sb.append(String.format("%-1.1s", this.getAddrType())); // addrtyp
			sb.append(String.format("%-15.15s", this.getLastName()));
			sb.append(String.format("%-10.10s", this.getFirstName()));
			sb.append(String.format("%-28.28s", this.getFirmName()));
			sb.append(String.format("%-28.28s", this.getAddAddr1()));
			sb.append(String.format("%-28.28s", this.getAddAddr2()));
			sb.append(String.format("%-10.10s", this.getUnitNum()));
			sb.append(String.format("%-3.3s", this.getHalfUnitNum()));
			sb.append(String.format("%-2.2s", this.getStreetDir()));
			sb.append(String.format("%-28.28s", this.getStreetName()));
			sb.append(String.format("%-4.4s", this.getStreetType()));
			sb.append(String.format("%-2.2s", this.getStreetPostDir()));
			sb.append(String.format("%-4.4s", this.getSubUnitCode()));
			sb.append(String.format("%-8.8s", this.getSubUnitNum()));
			sb.append(String.format("%-28.28s", this.getCity()));
			sb.append(String.format("%-13.13s", this.getShortCity())); // short city
			sb.append(String.format("%-2.2s", this.getState()));
			sb.append(String.format("%-5.5s", this.getZip5()));
			sb.append(String.format("%-4.4s", this.getZip4())); // zip 4
			sb.append(String.format("%-2.2s", this.getDelPtBarCode())); // DelPtBarCode
			sb.append(String.format("%-4.4s", this.getCarrRouteSort())); // CarrRteSort
			sb.append(String.format("%10.10s", this.getHomePhone()));
			sb.append(String.format("%10.10s", this.getBusinessPhone()));
			sb.append(String.format("%-1.1s", this.getGatedCommFlg())); // GatedCommFlg
			sb.append(String.format("%-5.5s", this.getGateSecCode())); // GateSecCode
			sb.append(String.format("%-4.4s", this.getCode1ErrorCode()));
			sb.append(String.format("%-4.4s", this.getPiaTranCode())); // PIATranCode
			sb.append(String.format("%-4.4s", this.getBatchNum())); // BatchNum
			sb.append(String.format("%-2.2s", this.getCurrPrtSiteCode())); // CurrPrtSiteCode
			sb.append(String.format("%-4.4s", this.getCurrMarketCode())); // CurrMktCode
			String currentAccountNum = this.getAccountNum();
			if (currentAccountNum.length() == 9) { // remove market id (34)
				currentAccountNum = currentAccountNum.substring(2);
			}
			sb.append(String.format("%7.7s", currentAccountNum));

			sb.append(String.format("%7.7s", this.getCurrAddrNum())); // CurrAddrNum
			sb.append(String.format("%2.2s", this.getZone())); // zone
			sb.append(String.format("%2.2s", this.getDistrict())); // district
			sb.append(String.format("%3.3s", this.getRoute())); // route
			sb.append(String.format("%-1.1s", this.getTransactionType()));
			sb.append(String.format("%-2.2s", this.getTransactionCode()));

			sb.append(String.format("%7.7s", this.getRadPostdDt())); // RadPostdDt
			sb.append(String.format("%6.6s", this.getRadPostdTm())); // RadPostdTm
			sb.append(String.format("%-2.2s", this.getOperatorID())); // OperID
			sb.append(String.format("%-2.2s", this.getMarketSeg())); // MktSeg
			sb.append(String.format("%-2.2s", this.getPrizmCode())); // PrizmCode
			sb.append(String.format("%-2.2s", this.getPrizmGroup())); // PrizmGrp
			sb.append(String.format("%-2.2s", this.getfIPSState())); // FIPSState
			sb.append(String.format("%-3.3s", this.getfIPSCounty())); // FIPSCounty
			sb.append(String.format("%-6.6s", this.getCensusTract())); // CensusTract
			sb.append(String.format("%-1.1s", this.getCensusBlkGrp())); // CensusBlkGrp
			sb.append(String.format("%-8.8s", this.getLatitude())); // Latitude
			sb.append(String.format("%-8.8s", this.getLongitude())); // Longitude
			sb.append(String.format("%-1.1s", this.getVicsSrcOrdCode())); // VicsSrcOrdCode
			sb.append(String.format("%-1.1s", this.getVicsSubsType())); // VicsSubsType
			sb.append(String.format("%-2.2s", this.getVicsFreqDel())); // VicsFreqDel
			sb.append(String.format("%3.3s", this.getVicsNumPaper())); // VicsNumPaper
			sb.append(String.format("%-2.2s", this.getVicsRateCode())); // VicsRateCode
			sb.append(String.format("%3.3s", this.getVicsSubsLngth())); // $VicsSubsLngth
			sb.append(String.format("%-11.11s", this.getVicsSoldBy())); // VicsSoldBy
			sb.append(String.format("%-2.2s", this.getVicsPromoCode())); // VicsPromoCode
			sb.append(String.format("%-2.2s", this.getVicsContestCode())); // VicsContestCode
			sb.append(String.format("%1.1s", this.getVicsRebilCode())); // VicsRebilCode
			sb.append(String.format("%-2.2s", this.getVicsSpecialRunCode())); // VicsSpecialRunCode
			sb.append(String.format("%-2.2s", this.getFrequencyOfDelivery())); // FreqDel
			sb.append(String.format("%-1.1s", this.getExistingDeliveryMethod()));
			sb.append(String.format("%-2.2s", this.getRateCode()));
			sb.append(String.format("%-2.2s", this.getPromoCode()));
			sb.append(String.format("%3.3s", this.getNumPaper().trim()));
			sb.append(String.format("%-2.2s", this.getContestCode()));
			sb.append(String.format("%-1.1s", this.getSrcOrdCode()));
			sb.append(String.format("%-2.2s", this.getSpecialRunCode())); // SpecialRunCode
			sb.append(String.format("%1.1s", this.getRebillCode())); // RebillCode
			sb.append(String.format("%-1.1s", this.getSubsType())); // Substype
			sb.append(String.format("%7.7s", this.getSubscriptionAmount().trim()));
			sb.append(String.format("%3.3s", this.getSubscriptionDuration().trim()));
			sb.append(String.format("%-2.2s", this.getPremium()));
			sb.append(String.format("%7.7s", this.getPayAmount().trim()));
			sb.append(String.format("%-1.1s", this.getIssuePdUnpd())); // IssuePdUnpd
			sb.append(String.format("%-20.20s", this.getTaxExempt())); // TaxExempt
			sb.append(String.format("%-11.11s", this.getSoldBy())); // SoldBy
			sb.append(String.format("%-3.3s", this.getMarketCode())); // MarketCode
			sb.append(String.format("%-1.1s", this.getStatus()));
			sb.append(String.format("%-1.1s", this.getEntryMethod())); // EntryMethod
			sb.append(String.format("%-4.4s", this.getAgencyName())); // AgencyName
			sb.append(String.format("%-1.1s", this.getPrintFlag()));
			sb.append(String.format("%-1.1s", this.getPndLookSts())); // PndLookSts
			sb.append(String.format("%-1.1s", this.getVIPLabel())); // VIPLabel
			sb.append(String.format("%-1.1s", this.getTtlMktCov()));
			sb.append(String.format("%-3.3s", this.getDeptRef())); // DeptRef
			sb.append(String.format("%-3.3s", this.getaBCCode())); // ABCCode
			sb.append(String.format("%-15.15s", this.getClubNum()));
			sb.append(String.format("%-1.1s", this.getClubType())); // ClubType
			sb.append(String.format("%-35.35s", this.getPurchaseOrderNum())); // PurOrdNum
			sb.append(String.format("%-4.4s", this.getSalesRep())); // SalesRep
			sb.append(String.format("%-1.1s", this.getOverrideCode1())); // OvrCode1
			sb.append(String.format("%-1.1s", this.getDeleteGiftPayer()));
			sb.append(String.format("%-1.1s", this.getPerpCCFlag()));
			sb.append(String.format("%-1.1s", this.getBillChargePd()));
			sb.append(String.format("%-1.1s", this.getOneTimeBill()));
			sb.append(String.format("%-1.1s", this.getCreditCardType()));
			sb.append(String.format("%-200.200s", this.getCreditCardNumber()));
			sb.append(String.format("%-4.4s", this.getCreditCardExpirationDate()));
			sb.append(String.format("%-1.1s", this.getNewAddrType())); // NewAddrType
			sb.append(String.format("%-15.15s", this.getNewLastName()));
			sb.append(String.format("%-10.10s", this.getNewFirstName()));
			sb.append(String.format("%-28.28s", this.getNewFirmName()));
			sb.append(String.format("%-28.28s", this.getNewAddlAddr1()));
			sb.append(String.format("%-28.28s", this.getNewAddlAddr2()));
			sb.append(String.format("%-10.10s", this.getNewUnitNum()));
			sb.append(String.format("%-3.3s", this.getNewHalfUnitNum()));
			sb.append(String.format("%-2.2s", this.getNewStreetDir()));
			sb.append(String.format("%-28.28s", this.getNewStreetName()));
			sb.append(String.format("%-4.4s", this.getNewStreetType()));
			sb.append(String.format("%-2.2s", this.getNewStreetPostDir()));
			sb.append(String.format("%-8.8s", this.getNewSubUnitNum()));
			sb.append(String.format("%-4.4s", this.getNewSubUnitCode()));
			sb.append(String.format("%-28.28s", this.getNewCity()));
			sb.append(String.format("%-13.13s", this.getNewShortCity())); // NewShortCity
			sb.append(String.format("%-2.2s", this.getNewState()));
			sb.append(String.format("%-5.5s", this.getNewZip()));
			sb.append(String.format("%-4.4s", this.getNewZip4())); // NewZip4
			sb.append(String.format("%-2.2s", this.getNewDelPtBarCode())); // NewDelPtBarCode
			sb.append(String.format("%-4.4s", this.getNewCarrRteSort())); // NewCarrRteSort
			sb.append(String.format("%-1.1s", this.getNewGatedCommFlag())); // NewGatedCommFlag
			sb.append(String.format("%-5.5s", this.getNewGatedCommSecCode())); // NewGatedCommSecCode
			sb.append(String.format("%10.10s", this.getNewPhoneNum()));
			sb.append(String.format("%10.10s", this.getNewBusPhoneNum()));
			sb.append(String.format("%7.7s", this.getNewAddrNum())); // NewAddrNum
			sb.append(String.format("%-2.2s", this.gettRFSecRecType())); // TRFSecRecType
			sb.append(String.format("%7.7s", this.getInfoRTY2Based())); // InfoRTY2Based
			sb.append(String.format("%7.7s", this.getNewAcctNum())); // NewAcctNum
			sb.append(String.format("%-1.1s", this.getTransferType())); // TranferType
			sb.append(String.format("%-2.2s", this.getNewPrintsiteCode())); // NewPrintsiteCode
			sb.append(String.format("%-4.4s", this.getNewMktcode())); // NewMktcode
			sb.append(String.format("%-3.3s", this.getNewSiteCode())); // NewSiteCode
			sb.append(String.format("%-1.1s", this.getNewDelMethod())); // NewDelMethod
			sb.append(String.format("%-2.2s", this.getNewMktSeg())); // NewMktSeg
			sb.append(String.format("%-2.2s", this.getNewPrizmCode())); // NewPrizmCode
			sb.append(String.format("%-2.2s", this.getNewPrizmGrp())); // NewPrizmGrp
			sb.append(String.format("%-2.2s", this.getNewFIPSState())); // NewFIPSState
			sb.append(String.format("%-3.3s", this.getNewFIPSCounty())); // NewFIPSCounty
			sb.append(String.format("%-6.6s", this.getNewCensusTrack())); // NewCensusTrack
			sb.append(String.format("%-1.1s", this.getNewCensusBlkGrp())); // NewCensusBlkGrp
			sb.append(String.format("%-8.8s", this.getNewLatitude())); // NewLatitude
			sb.append(String.format("%-8.8s", this.getNewLongitude())); // NewLongitude
			sb.append(String.format("%1.1s", getContactCustomer()));
			sb.append(String.format("%2.2s", this.getCreditDays1().trim()));
			sb.append(String.format("%2.2s", this.getCreditDays2().trim())); // $CreditDay2
			sb.append(String.format("%2.2s", this.getCreditDays3().trim()));
			sb.append(String.format("%2.2s", this.getCreditDays4().trim())); // $CreditDay4
			sb.append(String.format("%2.2s", this.getCreditDays5().trim())); // $CreditDay5
			sb.append(String.format("%2.2s", this.getCreditDays6().trim())); // $CreditDay6
			sb.append(String.format("%2.2s", this.getCreditDays7().trim())); // $CreditDay7
			sb.append(String.format("%11.11s", this.getDebtAdjAmount().trim())); // $DebtAdjAmount
			sb.append(String.format("%11.11s", this.getCreditAdjAmount().trim())); // $CreditAdjAmount
			sb.append(String.format("%-1.1s", this.getDeliveryPreference()));
			sb.append(String.format("%-38.38s", this.getComment()));
			sb.append(String.format("%-50.50s", this.getRejComment()));
			sb.append(String.format("%-50.50s", this.getAutoRegComm()));
			sb.append(String.format("%8.8s", this.getEffectiveDate()));
			sb.append(String.format("%8.8s", this.getEffectiveDate2())); // EffDate2 not used
			sb.append(String.format("%-1.1s", this.getBillingAddrType())); // BillAddrType
			sb.append(String.format("%-15.15s", getBillingLastName()));
			sb.append(String.format("%-10.10s", getBillingFirstName()));
			sb.append(String.format("%-28.28s", getBillingFirmName()));
			sb.append(String.format("%-28.28s", this.getBillingAddress1()));
			sb.append(String.format("%-28.28s", this.getBillingAddress2()));
			sb.append(String.format("%-10.10s", this.getBillingUnitNum()));
			sb.append(String.format("%-3.3s", this.getBillingHalfUnitNum()));
			sb.append(String.format("%-2.2s", this.getBillingStreetDir()));
			sb.append(String.format("%-28.28s", this.getBillingStreetName()));
			sb.append(String.format("%-4.4s", this.getBillingStreetType()));
			sb.append(String.format("%-2.2s", this.getBillingStreetPostDir()));
			sb.append(String.format("%-8.8s", this.getBillingSubUnitNum()));
			sb.append(String.format("%-4.4s", this.getBillingSubUnitCode()));
			sb.append(String.format("%-28.28s", this.getBillingCity()));
			sb.append(String.format("%-13.13s", this.getBillingShortCity())); // BillShortCity
			sb.append(String.format("%-2.2s", this.getBillingState()));
			sb.append(String.format("%-5.5s", this.getBillingZip5()));
			sb.append(String.format("%-4.4s", this.getBillingZip4())); // BillZip4
			sb.append(String.format("%-2.2s", this.getBillingDelPtBarCode())); // BillDelPtBarCode
			sb.append(String.format("%-4.4s", this.getBillingCarrRteSort())); // BillCarrRteSort
			sb.append(String.format("%10.10s", this.getBillingBusinessPhone()));
			sb.append(String.format("%10.10s", this.getBillingHomePhone()));
			sb.append(String.format("%-6.6s", this.getCreditCardAuthCode()));
			sb.append(String.format("%-8.8s", this.getCreditCardAuthDate()));
			sb.append(String.format("%-1.1s", this.getCreditCardBatchProcess()));
			sb.append(String.format("%-75.75s", this.getCreditCardAuthRejDesc())); // CCAuthRejDesc
			sb.append(String.format("%-4.4s", this.getcCCIDCode())); // CCCIDCode
			sb.append(String.format("%-1.1s", this.getAppFlag())); // AppFlag
			sb.append(String.format("%-9.9s", this.getBankRouteNum())); // BankRtNum
			sb.append(String.format("%-13.13s", this.getBankAccountNum())); // BankActNum
			sb.append(String.format("%-12.12s", this.getCheckNum())); // CheckNum
			sb.append(String.format("%-6.6s", this.getMicrTranCode()));
			sb.append(String.format("%-20.20s", this.getWebID())); // WebID
			sb.append(String.format("%-1.1s", this.getDiscAgncyFlg())); // DiscAgncyFlg
			sb.append(String.format("%-1.1s", this.getEmailType()));
			sb.append(String.format("%-60.60s", this.getEmailAddress()));
			sb.append(String.format("%-1.1s", this.getGiftCard())); // GiftCard
			sb.append(String.format("%-56.56s", this.getBadSubAddress()));
			sb.append(String.format("%-56.56s", this.getBadTrnGftAddress()));
			sb.append(String.format("%-1.1s", this.getFulfillItem())); // FulFillItem
			sb.append(String.format("%-1.1s", this.getGiftPayerTMC())); // GiftPayerTMC
			sb.append(String.format("%-2.2s", this.getPremiumType())); // PremType
			sb.append(String.format("%-2.2s", this.getPremiumAttr())); // PremAttr
			sb.append(String.format("%-474.474s", this.getFiller()));

		} catch (Exception e) {
			throw new UsatException(e);
		}
		String exportVal = sb.toString();

		return exportVal;
	}

	public String getaBCCode() {
		return aBCCode;
	}

	public void setaBCCode(String aBCCode) {
		this.aBCCode = aBCCode;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getAddrType() {
		return addrType;
	}

	public void setAddrType(String addrType) {
		this.addrType = addrType;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getAppFlag() {
		return appFlag;
	}

	public void setAppFlag(String appFlag) {
		this.appFlag = appFlag;
	}

	public String getBankAccountNum() {
		return bankAccountNum;
	}

	public void setBankAccountNum(String bankAccountNum) {
		this.bankAccountNum = bankAccountNum;
	}

	public String getBankRouteNum() {
		return bankRouteNum;
	}

	public void setBankRouteNum(String bankRouteNum) {
		this.bankRouteNum = bankRouteNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getBillingShortCity() {
		return billingShortCity;
	}

	public void setBillingShortCity(String billingShortCity) {
		this.billingShortCity = billingShortCity;
	}

	public String getBillingZip4() {
		return billingZip4;
	}

	public void setBillingZip4(String billingZip4) {
		this.billingZip4 = billingZip4;
	}

	public String getBillingDelPtBarCode() {
		return billingDelPtBarCode;
	}

	public void setBillingDelPtBarCode(String billingDelPtBarCode) {
		this.billingDelPtBarCode = billingDelPtBarCode;
	}

	public String getBillingCarrRteSort() {
		return billingCarrRteSort;
	}

	public void setBillingCarrRteSort(String billingCarrRteSort) {
		this.billingCarrRteSort = billingCarrRteSort;
	}

	public String getBillingAddrType() {
		return billingAddrType;
	}

	public void setBillingAddrType(String billingAddrType) {
		this.billingAddrType = billingAddrType;
	}

	public String getCarrRouteSort() {
		return carrRouteSort;
	}

	public void setCarrRouteSort(String carrRouteSort) {
		this.carrRouteSort = carrRouteSort;
	}

	public String getcCCIDCode() {
		return cCCIDCode;
	}

	public void setcCCIDCode(String cCCIDCode) {
		this.cCCIDCode = cCCIDCode;
	}

	public String getCheckNum() {
		return checkNum;
	}

	public void setCheckNum(String checkNum) {
		this.checkNum = checkNum;
	}

	public String getCensusTract() {
		return censusTract;
	}

	public void setCensusTract(String censusTract) {
		this.censusTract = censusTract;
	}

	public String getCensusBlkGrp() {
		return censusBlkGrp;
	}

	public void setCensusBlkGrp(String censusBlkGrp) {
		this.censusBlkGrp = censusBlkGrp;
	}

	public String getShortCity() {
		return shortCity;
	}

	public void setShortCity(String shortCity) {
		this.shortCity = shortCity;
	}

	public String getClubType() {
		return clubType;
	}

	public void setClubType(String clubType) {
		this.clubType = clubType;
	}

	public String getCreditCardAuthRejDesc() {
		return creditCardAuthRejDesc;
	}

	public void setCreditCardAuthRejDesc(String creditCardAuthRejDesc) {
		this.creditCardAuthRejDesc = creditCardAuthRejDesc;
	}

	public String getCurrAddrNum() {
		return currAddrNum;
	}

	public void setCurrAddrNum(String currAddrNum) {
		this.currAddrNum = currAddrNum;
	}

	public String getCurrMarketCode() {
		return currMarketCode;
	}

	public void setCurrMarketCode(String currMarketCode) {
		this.currMarketCode = currMarketCode;
	}

	public String getZip4() {
		return zip4;
	}

	public void setZip4(String zip4) {
		this.zip4 = zip4;
	}

	public String getCurrPrtSiteCode() {
		return currPrtSiteCode;
	}

	public void setCurrPrtSiteCode(String currPrtSiteCode) {
		this.currPrtSiteCode = currPrtSiteCode;
	}

	public String getCreditAdjAmount() {
		return creditAdjAmount;
	}

	public void setCreditAdjAmount(String creditAdjAmount) {
		this.creditAdjAmount = creditAdjAmount;
	}

	public String getDebtAdjAmount() {
		return debtAdjAmount;
	}

	public void setDebtAdjAmount(String debtAdjAmount) {
		this.debtAdjAmount = debtAdjAmount;
	}

	public String getDelPtBarCode() {
		return delPtBarCode;
	}

	public void setDelPtBarCode(String delPtBarCode) {
		this.delPtBarCode = delPtBarCode;
	}

	public String getDeptRef() {
		return deptRef;
	}

	public void setDeptRef(String deptRef) {
		this.deptRef = deptRef;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getDiscAgncyFlg() {
		return discAgncyFlg;
	}

	public void setDiscAgncyFlg(String discAgncyFlg) {
		this.discAgncyFlg = discAgncyFlg;
	}

	public String getEffectiveDate2() {
		return effectiveDate2;
	}

	public void setEffectiveDate2(String effectiveDate2) {
		this.effectiveDate2 = effectiveDate2;
	}

	public String getEntryMethod() {
		return entryMethod;
	}

	public void setEntryMethod(String entryMethod) {
		this.entryMethod = entryMethod;
	}

	public String getfIPSCounty() {
		return fIPSCounty;
	}

	public void setfIPSCounty(String fIPSCounty) {
		this.fIPSCounty = fIPSCounty;
	}

	public String getfIPSState() {
		return fIPSState;
	}

	public void setfIPSState(String fIPSState) {
		this.fIPSState = fIPSState;
	}

	public String getFrequencyOfDelivery() {
		return frequencyOfDelivery;
	}

	public void setFrequencyOfDelivery(String frequencyOfDelivery) {
		this.frequencyOfDelivery = frequencyOfDelivery;
	}

	public String getFulfillItem() {
		return fulfillItem;
	}

	public void setFulfillItem(String fulfillItem) {
		this.fulfillItem = fulfillItem;
	}

	public String getGatedCommFlg() {
		return gatedCommFlg;
	}

	public void setGatedCommFlg(String gatedCommFlg) {
		this.gatedCommFlg = gatedCommFlg;
	}

	public String getGateSecCode() {
		return gateSecCode;
	}

	public void setGateSecCode(String gateSecCode) {
		this.gateSecCode = gateSecCode;
	}

	public String getGiftCard() {
		return giftCard;
	}

	public void setGiftCard(String giftCard) {
		this.giftCard = giftCard;
	}

	public String getGiftPayerTMC() {
		return giftPayerTMC;
	}

	public void setGiftPayerTMC(String giftPayerTMC) {
		this.giftPayerTMC = giftPayerTMC;
	}

	public String getIssuePdUnpd() {
		return issuePdUnpd;
	}

	public void setIssuePdUnpd(String issuePdUnpd) {
		this.issuePdUnpd = issuePdUnpd;
	}

	public String getInfoRTY2Based() {
		return infoRTY2Based;
	}

	public void setInfoRTY2Based(String infoRTY2Based) {
		this.infoRTY2Based = infoRTY2Based;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getMarketCode() {
		return marketCode;
	}

	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}

	public String getMarketSeg() {
		return marketSeg;
	}

	public void setMarketSeg(String marketSeg) {
		this.marketSeg = marketSeg;
	}

	public String getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(String operatorID) {
		this.operatorID = operatorID;
	}

	public String getOverrideCode1() {
		return overrideCode1;
	}

	public void setOverrideCode1(String overrideCode1) {
		this.overrideCode1 = overrideCode1;
	}

	public String getPiaTranCode() {
		return piaTranCode;
	}

	public void setPiaTranCode(String piaTranCode) {
		this.piaTranCode = piaTranCode;
	}

	public String getPndLookSts() {
		return pndLookSts;
	}

	public void setPndLookSts(String pndLookSts) {
		this.pndLookSts = pndLookSts;
	}

	public String getPremiumType() {
		return premiumType;
	}

	public void setPremiumType(String premiumType) {
		this.premiumType = premiumType;
	}

	public String getPremiumAttr() {
		return premiumAttr;
	}

	public void setPremiumAttr(String premiumAttr) {
		this.premiumAttr = premiumAttr;
	}

	public String getPrizmCode() {
		return prizmCode;
	}

	public void setPrizmCode(String prizmCode) {
		this.prizmCode = prizmCode;
	}

	public String getPrizmGroup() {
		return prizmGroup;
	}

	public void setPrizmGroup(String prizmGroup) {
		this.prizmGroup = prizmGroup;
	}

	public String getPurchaseOrderNum() {
		return purchaseOrderNum;
	}

	public void setPurchaseOrderNum(String purchaseOrderNum) {
		this.purchaseOrderNum = purchaseOrderNum;
	}

	public String getRebillCode() {
		return rebillCode;
	}

	public void setRebillCode(String rebillCode) {
		this.rebillCode = rebillCode;
	}

	public String getRadPostdDt() {
		return radPostdDt;
	}

	public void setRadPostdDt(String radPostdDt) {
		this.radPostdDt = radPostdDt;
	}

	public String getRadPostdTm() {
		return radPostdTm;
	}

	public void setRadPostdTm(String radPostdTm) {
		this.radPostdTm = radPostdTm;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getSalesRep() {
		return salesRep;
	}

	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}

	public String getSpecialRunCode() {
		return specialRunCode;
	}

	public void setSpecialRunCode(String specialRunCode) {
		this.specialRunCode = specialRunCode;
	}

	public String getSoldBy() {
		return soldBy;
	}

	public void setSoldBy(String soldBy) {
		this.soldBy = soldBy;
	}

	public String getSubsType() {
		return subsType;
	}

	public void setSubsType(String subsType) {
		this.subsType = subsType;
	}

	public String getTaxExempt() {
		return taxExempt;
	}

	public void setTaxExempt(String taxExempt) {
		this.taxExempt = taxExempt;
	}

	public String getTranSeqNum() {
		return tranSeqNum;
	}

	public void setTranSeqNum(String tranSeqNum) {
		this.tranSeqNum = tranSeqNum;
	}

	public int getTransactionState() {
		return transactionState;
	}

	public void setTransactionState(int transactionState) {
		this.transactionState = transactionState;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String tranferType) {
		this.transferType = tranferType;
	}

	public String gettRFSecRecType() {
		return tRFSecRecType;
	}

	public void settRFSecRecType(String tRFSecRecType) {
		this.tRFSecRecType = tRFSecRecType;
	}

	public String getNewAddrType() {
		return newAddrType;
	}

	public void setNewAddrType(String newAddrType) {
		this.newAddrType = newAddrType;
	}

	public String getNewShortCity() {
		return NewShortCity;
	}

	public void setNewShortCity(String newShortCity) {
		NewShortCity = newShortCity;
	}

	public String getNewZip4() {
		return NewZip4;
	}

	public void setNewZip4(String newZip4) {
		NewZip4 = newZip4;
	}

	public String getNewCarrRteSort() {
		return NewCarrRteSort;
	}

	public void setNewCarrRteSort(String newCarrRteSort) {
		NewCarrRteSort = newCarrRteSort;
	}

	public String getNewDelPtBarCode() {
		return NewDelPtBarCode;
	}

	public void setNewDelPtBarCode(String newDelPtBarCode) {
		NewDelPtBarCode = newDelPtBarCode;
	}

	public String getNewGatedCommFlag() {
		return NewGatedCommFlag;
	}

	public void setNewGatedCommFlag(String newGatedCommFlag) {
		NewGatedCommFlag = newGatedCommFlag;
	}

	public String getNewGatedCommSecCode() {
		return NewGatedCommSecCode;
	}

	public void setNewGatedCommSecCode(String newGatedCommSecCode) {
		NewGatedCommSecCode = newGatedCommSecCode;
	}

	public String getNewAcctNum() {
		return NewAcctNum;
	}

	public void setNewAcctNum(String newAcctNum) {
		NewAcctNum = newAcctNum;
	}

	public String getNewAddrNum() {
		return NewAddrNum;
	}

	public void setNewAddrNum(String newAddrNum) {
		NewAddrNum = newAddrNum;
	}

	public String getNewCensusBlkGrp() {
		return NewCensusBlkGrp;
	}

	public void setNewCensusBlkGrp(String newCensusBlkGrp) {
		NewCensusBlkGrp = newCensusBlkGrp;
	}

	public String getNewCensusTrack() {
		return NewCensusTrack;
	}

	public void setNewCensusTrack(String newCensusTrack) {
		NewCensusTrack = newCensusTrack;
	}

	public String getNewDelMethod() {
		return NewDelMethod;
	}

	public void setNewDelMethod(String newDelMethod) {
		NewDelMethod = newDelMethod;
	}

	public String getNewFIPSCounty() {
		return NewFIPSCounty;
	}

	public void setNewFIPSCounty(String newFIPSCounty) {
		NewFIPSCounty = newFIPSCounty;
	}

	public String getNewFIPSState() {
		return NewFIPSState;
	}

	public void setNewFIPSState(String newFIPSState) {
		NewFIPSState = newFIPSState;
	}

	public String getNewLatitude() {
		return NewLatitude;
	}

	public void setNewLatitude(String newLatitude) {
		NewLatitude = newLatitude;
	}

	public String getNewLongitude() {
		return NewLongitude;
	}

	public void setNewLongitude(String newLongitude) {
		NewLongitude = newLongitude;
	}

	public String getNewMktcode() {
		return NewMktcode;
	}

	public void setNewMktcode(String newMktcode) {
		NewMktcode = newMktcode;
	}

	public String getNewMktSeg() {
		return NewMktSeg;
	}

	public void setNewMktSeg(String newMktSeg) {
		NewMktSeg = newMktSeg;
	}

	public String getNewPrintsiteCode() {
		return NewPrintsiteCode;
	}

	public void setNewPrintsiteCode(String newPrintsiteCode) {
		NewPrintsiteCode = newPrintsiteCode;
	}

	public String getNewPrizmGrp() {
		return NewPrizmGrp;
	}

	public void setNewPrizmGrp(String newPrizmGrp) {
		NewPrizmGrp = newPrizmGrp;
	}

	public String getNewPrizmCode() {
		return NewPrizmCode;
	}

	public void setNewPrizmCode(String newPrizmCode) {
		NewPrizmCode = newPrizmCode;
	}

	public String getNewSiteCode() {
		return NewSiteCode;
	}

	public void setNewSiteCode(String newSiteCode) {
		NewSiteCode = newSiteCode;
	}

	public String getCreditDays2() {
		return creditDays2;
	}

	public void setCreditDays2(String creditDays2) {
		this.creditDays2 = creditDays2;
	}

	public String getCreditDays4() {
		return creditDays4;
	}

	public void setCreditDays4(String creditDays4) {
		this.creditDays4 = creditDays4;
	}

	public String getCreditDays5() {
		return creditDays5;
	}

	public void setCreditDays5(String creditDays5) {
		this.creditDays5 = creditDays5;
	}

	public String getCreditDays6() {
		return creditDays6;
	}

	public void setCreditDays6(String creditDays6) {
		this.creditDays6 = creditDays6;
	}

	public String getCreditDays7() {
		return creditDays7;
	}

	public void setCreditDays7(String creditDays7) {
		this.creditDays7 = creditDays7;
	}

	public String getVIPLabel() {
		return VIPLabel;
	}

	public void setVIPLabel(String vIPLabel) {
		VIPLabel = vIPLabel;
	}

	public String getVicsSrcOrdCode() {
		return VicsSrcOrdCode;
	}

	public void setVicsSrcOrdCode(String vicsSrcOrdCode) {
		VicsSrcOrdCode = vicsSrcOrdCode;
	}

	public String getVicsSubsType() {
		return VicsSubsType;
	}

	public void setVicsSubsType(String vicsSubsType) {
		VicsSubsType = vicsSubsType;
	}

	public String getVicsFreqDel() {
		return VicsFreqDel;
	}

	public void setVicsFreqDel(String vicsFreqDel) {
		VicsFreqDel = vicsFreqDel;
	}

	public String getVicsNumPaper() {
		return VicsNumPaper;
	}

	public void setVicsNumPaper(String vicsNumPaper) {
		VicsNumPaper = vicsNumPaper;
	}

	public String getVicsRateCode() {
		return VicsRateCode;
	}

	public void setVicsRateCode(String vicsRateCode) {
		VicsRateCode = vicsRateCode;
	}

	public String getVicsSubsLngth() {
		return VicsSubsLngth;
	}

	public void setVicsSubsLngth(String vicsSubsLngth) {
		VicsSubsLngth = vicsSubsLngth;
	}

	public String getVicsSoldBy() {
		return VicsSoldBy;
	}

	public void setVicsSoldBy(String vicsSoldBy) {
		VicsSoldBy = vicsSoldBy;
	}

	public String getVicsPromoCode() {
		return VicsPromoCode;
	}

	public void setVicsPromoCode(String vicsPromoCode) {
		VicsPromoCode = vicsPromoCode;
	}

	public String getVicsContestCode() {
		return VicsContestCode;
	}

	public void setVicsContestCode(String vicsContestCode) {
		VicsContestCode = vicsContestCode;
	}

	public String getVicsRebilCode() {
		return VicsRebilCode;
	}

	public void setVicsRebilCode(String vicsRebilCode) {
		VicsRebilCode = vicsRebilCode;
	}

	public String getVicsSpecialRunCode() {
		return VicsSpecialRunCode;
	}

	public void setVicsSpecialRunCode(String vicsSpecialRunCode) {
		VicsSpecialRunCode = vicsSpecialRunCode;
	}

	public String getWebID() {
		return WebID;
	}

	public void setWebID(String webID) {
		WebID = webID;
	}

	public String getZone() {
		return Zone;
	}

	public void setZone(String zone) {
		Zone = zone;
	}
}
