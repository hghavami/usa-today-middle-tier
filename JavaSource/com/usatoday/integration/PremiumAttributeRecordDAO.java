/*
 * Created on Nov 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf;

/**
 * @author aeast
 * @date Nov 9, 2006
 * @class PremiumAttributeRecordDAO
 * 
 *        This class is deprecated with the move to Genesys
 * @deprecated
 * 
 */
public class PremiumAttributeRecordDAO extends USATodayDAO {
	// static SQL strings
	private static final String UPDATE_MPF_ATTRIBUTE = "update PREMTRANSDETAIL set PubCode=?, AccountNum=?, PremPromoCode=?, PremCode=?, PremType=?, AttributeName=?, AttributeValue=?, TransID=?, DateUpdated=?, TimeUpdated=?, State=?, UpdateTimestamp=? GiftPayerReceive=? where PremKey = ?";
	private static final String UPDATE_MPF_BATCH_STATE = "update PREMTRANSDETAIL set State=?, UpdateTimestamp=? where State = ?";
	// private static final String FETCH_MPF_ATTRIBUTES_FOR_ORDER =
	// "select PremKey, PubCode, AccountNum, PremPromoCode, PremCode, PremType, AttributeName, AttributeValue, GiftPayerReceive, TransID, DateUpdated, TimeUpdated, State, UpdateTimestamp from PREMTRANSDETAIL where TransID = ?";
	private static final String FETCH_MPF_ATTRIBUTES_IN_STATE = "select PremKey, PubCode, AccountNum, PremPromoCode, PremCode, PremType, AttributeName, AttributeValue, GiftPayerReceive, TransID, DateUpdated, TimeUpdated, State, UpdateTimestamp from PREMTRANSDETAIL where State = ?";
	private static final String DELETE_MPF_ARRTIBUTE = "delete from PREMTRANSDETAIL where PremKey = ?";
	private static final String DELETE_MPF_ARRTIBUTES_IN_DELETE_STATE_OLDER_THAN_DATE = "delete from PREMTRANSDETAIL where state = 3 and updateTimestamp <= ?";

	private synchronized static Collection<PremiumAttributeIntf> objectFactory(ResultSet rs) throws UsatException {
		Collection<PremiumAttributeIntf> records = new ArrayList<PremiumAttributeIntf>();

		try {
			// iterate over result set and build objects
			while (rs.next()) {

				long primaryKey = rs.getLong("PremKey");
				int state = rs.getInt("State");

				String tempStr = null;

				PremiumAttributeRecordTO attribute = new PremiumAttributeRecordTO();

				attribute.setKey(primaryKey);
				attribute.setState(state);

				tempStr = rs.getString("AccountNum");
				if (tempStr != null) {
					attribute.setAccountNum(tempStr.trim());
				}

				tempStr = rs.getString("PubCode");
				if (tempStr != null) {
					attribute.setPubCode(tempStr.trim());
				}

				tempStr = rs.getString("PremPromoCode");
				if (tempStr != null) {
					attribute.setPremiumPromotionCode(tempStr.trim());
				}

				tempStr = rs.getString("PremCode");
				if (tempStr != null) {
					attribute.setPremiumCode(tempStr.trim());
				}

				tempStr = rs.getString("PremType");
				if (tempStr != null) {
					attribute.setPremiumType(tempStr.trim());
				}

				tempStr = rs.getString("GiftPayerReceive");
				if (tempStr != null) {
					attribute.setGiftPayerReceive(tempStr.trim());
				}

				tempStr = rs.getString("AttributeName");
				if (tempStr != null) {
					attribute.setAttributeName(tempStr.trim());
				}

				tempStr = rs.getString("AttributeValue");
				if (tempStr != null) {
					attribute.setAttributeValue(tempStr.trim());
				}

				tempStr = rs.getString("TransID");
				if (tempStr != null) {
					attribute.setOrderId(tempStr.trim());
				}

				tempStr = rs.getString("DateUpdated");
				if (tempStr != null) {
					attribute.setDateUpdated(tempStr.trim());
				}

				tempStr = rs.getString("TimeUpdated");
				if (tempStr != null) {
					attribute.setTimeUpdated(tempStr.trim());
				}

				Timestamp ts = rs.getTimestamp("UpdateTimestamp");
				if (ts != null) {
					attribute.setUpdateTimestamp(ts);
				}

				records.add(attribute);
			}
		} catch (Exception e) {
			System.out.println("PremiumAttributeRecordDAO::objectFactory() - Failed to create PremiumAttribute Record TO : "
					+ e.getMessage());
		}

		return records;
	}

	/**
	 * 
	 * @param attribute
	 * @return The attribute record with the primary key updated.
	 * @throws UsatException
	 */
	public PremiumAttributeIntf insert(PremiumAttributeIntf attribute) throws UsatException {

		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();

			// insert into PREMTRANSDETAIL (PubCode, AccountNum, PremPromoCode,
			// PremCode, PremType, AttributeName, AttributeValue, TransID,
			// DateUpdated, TimeUpdated, State, UpdateTimestamp )values
			// (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
			StringBuffer sql = new StringBuffer(
					"insert into PREMTRANSDETAIL (PubCode, AccountNum, PremPromoCode,PremCode, PremType, AttributeName, AttributeValue, GiftPayerReceive, TransID,DateUpdated, TimeUpdated, State, UpdateTimestamp ) values (");

			if (attribute.getPubCode() != null) {
				sql.append("'").append(attribute.getPubCode()).append("',");
			} else {
				sql.append("null,");
			}
			if (attribute.getAccountNum() != null) {
				sql.append("'").append(attribute.getAccountNum()).append("',");
			} else {
				sql.append("null,");
			}

			if (attribute.getPremiumPromotionCode() != null) {
				sql.append("'").append(attribute.getPremiumPromotionCode()).append("',");
			} else {
				sql.append("null,");
			}

			if (attribute.getPremiumCode() != null) {
				sql.append("'").append(attribute.getPremiumCode()).append("',");
			} else {
				sql.append("null,");
			}

			if (attribute.getPremiumType() != null) {
				sql.append("'").append(attribute.getPremiumType()).append("',");
			} else {
				sql.append("null,");
			}

			if (attribute.getAttributeName() != null) {
				sql.append("'").append(attribute.getAttributeName()).append("',");
			} else {
				sql.append("null,");
			}

			if (attribute.getAttributeValue() != null) {
				sql.append("'").append(attribute.getAttributeValue()).append("',");
			} else {
				sql.append("null,");
			}

			if (attribute.getGiftPayerReceive() != null) {
				sql.append("'").append(attribute.getGiftPayerReceive()).append("',");
			} else {
				sql.append("null,");
			}

			if (attribute.getOrderId() != null) {
				sql.append("'").append(attribute.getOrderId()).append("',");
			} else {
				sql.append("null,");
			}

			if (attribute.getDateUpdated() != null) {
				sql.append("'").append(attribute.getDateUpdated()).append("',");
			} else {
				sql.append("null,");
			}

			if (attribute.getTimeUpdated() != null) {
				sql.append("'").append(attribute.getTimeUpdated()).append("',");
			} else {
				sql.append("null,");
			}

			sql.append(attribute.getState()).append(",'");

			Timestamp ts = new Timestamp(System.currentTimeMillis());
			sql.append(ts.toString());
			sql.append("')");

			// execute the SQL
			int rowcount = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowcount != 1) {
				throw new UsatException(
						"No Premium Attribute Record Inserted Or other error and yet no exception condition exists: Number rows affected: "
								+ rowcount);
			}

			ResultSet rs = statement.getGeneratedKeys();

			rs.next();
			long primaryKey = rs.getLong(1);

			attribute.setKey(primaryKey);

			statement.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return attribute;
	}

	/**
	 * 
	 * @param attribute
	 * @throws UsatException
	 */
	public void update(PremiumAttributeIntf attribute) throws UsatException {
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(PremiumAttributeRecordDAO.UPDATE_MPF_ATTRIBUTE);

			// order of colums
			// update ..
			// PubCode=?, AccountNum=?, PremPromoCode=?, PremCode=?, PremType=?,
			// AttributeName=?, AttributeValue=?, TransID=?, DateUpdated=?,
			// TimeUpdated=?, State=?, UpdateTimestamp=? where PremKey = ?
			if (attribute.getPubCode() != null) {
				statement.setString(1, attribute.getPubCode());
			} else {
				statement.setNull(1, Types.CHAR);
			}

			if (attribute.getAccountNum() != null) {
				statement.setString(2, attribute.getAccountNum());
			} else {
				statement.setNull(2, Types.CHAR);
			}

			if (attribute.getPremiumPromotionCode() != null) {
				statement.setString(3, attribute.getPremiumPromotionCode());
			} else {
				statement.setNull(3, Types.CHAR);
			}

			if (attribute.getPremiumCode() != null) {
				statement.setString(4, attribute.getPremiumCode());
			} else {
				statement.setNull(4, Types.CHAR);
			}

			if (attribute.getPremiumType() != null) {
				statement.setString(5, attribute.getPremiumType());
			} else {
				statement.setNull(5, Types.CHAR);
			}

			if (attribute.getAttributeName() != null) {
				statement.setString(6, attribute.getAttributeName());
			} else {
				statement.setNull(6, Types.CHAR);
			}

			if (attribute.getAttributeValue() != null) {
				statement.setString(7, attribute.getAttributeValue());
			} else {
				statement.setNull(7, Types.CHAR);
			}

			if (attribute.getOrderId() != null) {
				statement.setString(8, attribute.getOrderId());
			} else {
				statement.setNull(8, Types.CHAR);
			}

			if (attribute.getDateUpdated() != null) {
				statement.setString(9, attribute.getDateUpdated());
			} else {
				statement.setNull(9, Types.CHAR);
			}

			if (attribute.getTimeUpdated() != null) {
				statement.setString(10, attribute.getTimeUpdated());
			} else {
				statement.setNull(10, Types.CHAR);
			}

			statement.setInt(11, attribute.getState());

			Timestamp ts = new Timestamp(System.currentTimeMillis());
			statement.setTimestamp(12, ts);

			if (attribute.getGiftPayerReceive() != null) {
				statement.setString(13, attribute.getGiftPayerReceive());
			} else {
				statement.setNull(13, Types.CHAR);
			}

			// where clause
			statement.setLong(14, attribute.getKey());

			// execute the SQL
			int rowsAffected = statement.executeUpdate();

			if (rowsAffected != 1) {
				throw new UsatException(
						"No Premium Attribute Record Updated Or too many rows updated and yet no exception condition exists: Number rows affected: "
								+ statement.getUpdateCount());
			}

			statement.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
	}

	/**
	 * 
	 * @param key
	 * @return
	 * @throws UsatException
	 */
	public int delete(long key) throws UsatException {
		int rowsAffected = 0;
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(PremiumAttributeRecordDAO.DELETE_MPF_ARRTIBUTE);

			statement.setLong(1, key);

			rowsAffected = statement.executeUpdate();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return rowsAffected;
	}

	/**
	 * 
	 * @param numDays
	 * @return
	 * @throws UsatException
	 */
	public int deleteAttributesOlderThanSpecifiedDays(int numDays) throws UsatException {
		int rowsAffected = 0;
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn
					.prepareStatement(PremiumAttributeRecordDAO.DELETE_MPF_ARRTIBUTES_IN_DELETE_STATE_OLDER_THAN_DATE);

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.add(Calendar.DAY_OF_YEAR, (-1 * numDays));

			Timestamp ts = new Timestamp(cal.getTimeInMillis());

			statement.setTimestamp(1, ts);

			rowsAffected = statement.executeUpdate();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return rowsAffected;
	}

	private Collection<PremiumAttributeIntf> fetchAttributesInState(int state) throws UsatException {
		Collection<PremiumAttributeIntf> records = null;
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(PremiumAttributeRecordDAO.FETCH_MPF_ATTRIBUTES_IN_STATE);

			statement.setInt(1, state);

			ResultSet rs = statement.executeQuery();

			records = PremiumAttributeRecordDAO.objectFactory(rs);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return records;
	}

	public Collection<PremiumAttributeIntf> fetchAttributesRecordsForBatchProcessing() throws UsatException {
		return this.fetchAttributesInState(PremiumAttributeIntf.BATCH_IN_PROGRESS);
	}

	/**
	 * 
	 * @param oldState
	 * @param newState
	 * @return number or rows affected
	 * @throws UsatException
	 */
	private int changeState(int oldState, int newState) throws UsatException {
		int numRowsAffected = 0;
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(PremiumAttributeRecordDAO.UPDATE_MPF_BATCH_STATE);

			// "update PREMTRANSDETAIL set State=?, UpdateTimestamp=? where State = ?"
			statement.setInt(1, newState);
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			statement.setTimestamp(2, ts);
			statement.setInt(3, oldState);

			numRowsAffected = statement.executeUpdate();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return numRowsAffected;
	}

	/**
	 * 
	 * @return Number of records affected
	 * @throws UsatException
	 */
	public int changeStateToInBatchProcessing() throws UsatException {
		return this.changeState(PremiumAttributeIntf.READY_FOR_BATCH, PremiumAttributeIntf.BATCH_IN_PROGRESS);
	}

	/**
	 * 
	 * @return Number of records affected
	 * @throws UsatException
	 */
	public int changeStateToReadyForDelete() throws UsatException {
		return this.changeState(PremiumAttributeIntf.BATCH_IN_PROGRESS, PremiumAttributeIntf.READY_FOR_DELETION);
	}

}
