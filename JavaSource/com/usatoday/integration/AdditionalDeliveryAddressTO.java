/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;
import java.sql.Timestamp;

import com.usatoday.business.interfaces.customer.AdditionalDeliveryAddressIntf;

/**
 * @author hghavami
 * @date Mar 14, 2011
 * @class AdditionalAddressTO
 * 
 *        This class contains all fields required for the change delivery address screen.
 * 
 */
public class AdditionalDeliveryAddressTO implements AdditionalDeliveryAddressIntf, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private int id = -1;
	private String currAcctNum = "";
	private String firstName = "";
	private String lastName = "";
	private String firmName = "";
	private String streetAddress = "";
	private String aptDeptSuite = "";
	private String addlAddr1 = "";
	private String addlAddr2 = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String homePhone = "";
	private String busPhone = "";
	private Timestamp insertTimeStamp = null;
	private int emailID = -1;

	public String getAddlAddr1() {
		return addlAddr1;
	}

	public String getAddlAddr2() {
		return addlAddr2;
	}

	public String getAptDeptSuite() {
		return aptDeptSuite;
	}

	public String getBusPhone() {
		return busPhone;
	}

	public String getCity() {
		return city;
	}

	public String getCurrAcctNum() {
		return currAcctNum;
	}

	public int getEmailID() {
		return emailID;
	}

	public String getFirmName() {
		return firmName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public int getId() {
		return id;
	}

	public Timestamp getInsertTimeStamp() {
		return insertTimeStamp;
	}

	public String getLastName() {
		return lastName;
	}

	public String getState() {
		return state;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public String getZip() {
		return zip;
	}

	public void setAddlAddr1(String addlAddr1) {
		this.addlAddr1 = addlAddr1;
	}

	public void setAddlAddr2(String addlAddr2) {
		this.addlAddr2 = addlAddr2;
	}

	public void setAptDeptSuite(String aptDeptSuite) {
		this.aptDeptSuite = aptDeptSuite;
	}

	public void setBusPhone(String busPhone) {
		this.busPhone = busPhone;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCurrAcctNum(String currAcctnum) {
		this.currAcctNum = currAcctnum;
	}

	public void setEmailID(int emailID) {
		this.emailID = emailID;
	}

	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setInsertTimeStamp(Timestamp insertTimeStamp) {
		this.insertTimeStamp = insertTimeStamp;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

}
