package com.usatoday.integration;

import java.io.Serializable;
import java.util.Collection;

import com.usatoday.business.interfaces.CancelSubscriptionIntf;

public class CancelSubscriptionTO implements CancelSubscriptionIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<CancelSubscriptionIntf> cancelSubsReasons = null;
	private String cancelSubsReasonCode = null;
	private String cancelSubsReasonDesc = null;

	public CancelSubscriptionTO(String cancelSubsReasonCode, String cancelSubsReasonDesc) {
		super();
		this.cancelSubsReasonCode = cancelSubsReasonCode;
		this.cancelSubsReasonDesc = cancelSubsReasonDesc;
	}

	public String getCancelSubsReasonCode() {
		// TODO Auto-generated method stub
		return this.cancelSubsReasonCode;
	}

	public String getCancelSubsReasonDesc() {
		// TODO Auto-generated method stub
		return this.cancelSubsReasonDesc;
	}

	public Collection<CancelSubscriptionIntf> getSubsCancelReasons() {
		return cancelSubsReasons;
	}

}
