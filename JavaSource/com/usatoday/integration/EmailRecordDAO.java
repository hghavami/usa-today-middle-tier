package com.usatoday.integration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.gannett.usat.userserviceapi.client.CreateUser;
import com.gannett.usat.userserviceapi.client.GetUser;
import com.gannett.usat.userserviceapi.client.Login;
import com.gannett.usat.userserviceapi.client.UpdateUser;
import com.gannett.usat.userserviceapi.domainbeans.getUser.GetUserResponse;
import com.gannett.usat.userserviceapi.domainbeans.login.LoginResponse;
import com.gannett.usat.userserviceapi.domainbeans.users.UsersResponse;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;

public class EmailRecordDAO extends USATodayDAO {

	/*
	 * private static final String SELECT_EMAIL_DATA_BY_EMAIL = "{call USAT_EMAIL_PULL_BY_EMAIL(?)}"; private static final String
	 * SELECT_EMAIL_DATA_BY_EMAIL_LIKE = "{call USAT_EMAIL_PULL_BY_EMAIL_LIKE(?)}"; private static final String
	 * SELECT_EMAIL_COUNT_BY_EMAIL_LIKE = "{call USAT_EMAIL_PULL_COUNT_BY_EMAIL_LIKE(?)}"; private static final String
	 * SELECT_EMAIL_DATA_BY_SERIALNUMBER = "{call USAT_EMAIL_PULL_BY_SERIAL_NUMBER(?)}"; private static final String
	 * SELECT_EMAIL_RECORD_DATA_BY_SERIALNUMBER = "{call USAT_EMAIL_RECORD_PULL_BY_SERIAL_NUMBER(?)}"; private static final String
	 * SELECT_EMAIL_DATA_BY_EMAIL_AND_PASSWORD_AND_PUB = "{call USAT_EMAIL_LOGIN(?,?,?)}"; private static final String
	 * SELECT_EMAIL_BY_ACCOUNT_NUM_AND_PUB = "{call USAT_EMAIL_PULL_BY_ACCOUNT_AND_PUB(?,?)}"; private static final String
	 * SELECT_EMAIL_BY_EMAIL_PWD_ACCOUNT = "{call USAT_EMAIL_PULL_BY_EMAIL_PWD_ACCOUNT(?,?,?)}"; private static final String
	 * SELECT_EMAIL_BY_PUB_EMAIL_ACCOUNT = "{call USAT_EMAIL_PULL_BY_PUB_EMAIL_ACCOUNT(?,?,?)}"; private static final String
	 * SELECT_EMAIL_BY_EMAIL_ACCOUNT = "{call USAT_EMAIL_PULL_BY_EMAIL_ACCOUNT(?,?)}";
	 */

	public static java.lang.String INSERT = "INSERT INTO XTRNTEMAIL "
			+ "(PubCode, AccountNum, WebID, EmailAddress, WebPassword, AgencyName, TransType, PermStartDate, DateUpdated, "
			+ "TimeUpdated, OptIn, Zip, Active, EmailSent) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static java.lang.String UPDATE = "UPDATE XTRNTEMAIL set EmailAddress = ?, WebPassword = ?, "
			+ "PermStartDate = ?, DateUpdated = ?, TimeUpdated = ?, Active = ? where serialNum = ?";

	/**
	 * 
	 * This method generates transfer objects for each record in the result set.
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	/*
	 * private synchronized static Collection<EmailRecordIntf> objectFactory(ResultSet rs) throws UsatException {
	 * Collection<EmailRecordIntf> records = new ArrayList<EmailRecordIntf>(); String emailAddress = null;
	 * 
	 * try { // iterate over result set and build objects String password = null; String startDate = null; String tempStr = null;
	 * Date updated = null; boolean active = true;
	 * 
	 * while (rs.next()) {
	 * 
	 * emailAddress = rs.getString("EmailAddress"); if (emailAddress != null) { emailAddress = emailAddress.trim(); }
	 * 
	 * int serialNum = rs.getInt("SerialNum");
	 * 
	 * password = rs.getString("WebPassword"); if (password != null) { password = password.trim(); }
	 * 
	 * startDate = rs.getString("PermStartDate"); if (startDate != null) { startDate = startDate.trim(); }
	 * 
	 * active = false; tempStr = rs.getString("ACTIVE"); if (tempStr != null && "Y".equalsIgnoreCase(tempStr)) { active = true; }
	 * 
	 * updated = null; // Convert the udpated date/time to a Date object - kludge tempStr = rs.getString("DateUpdated"); // YYYYMMDD
	 * format, or null or all zeros if (tempStr != null) { tempStr = tempStr.trim(); if (tempStr.length() == 8) { if
	 * (!"00000000".equalsIgnoreCase(tempStr)) {
	 * 
	 * // Date date = null; try { // StringBuffer formattedDate = new StringBuffer("");
	 * 
	 * String year = (tempStr.substring(0, 4)); String month = (tempStr.substring(4, 6)); String day = (tempStr.substring(6));
	 * String hour = "0"; String minute = "0"; String second = "0";
	 * 
	 * tempStr = rs.getString("TimeUpdated"); if (tempStr != null && tempStr.trim().length() > 0) { tempStr = tempStr.trim(); try {
	 * hour = tempStr.substring(0, 2); minute = tempStr.substring(2, 4); second = tempStr.substring(4, 6); } catch (Exception e) {
	 * // ignore } }
	 * 
	 * DateTime dt = new DateTime(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day), Integer.parseInt(hour),
	 * Integer.parseInt(minute), Integer.parseInt(second), 0);
	 * 
	 * updated = dt.toDate();
	 * 
	 * } catch (Exception e) { updated = null; } } } }
	 * 
	 * 
	 * String pubCode, String accountNumber, String orderID, String emailAddress, String password, boolean active, boolean gift, int
	 * serialNumber, Date dateUpdated, String emailSent String permStart
	 * 
	 * EmailRecordTO emailRecord = new EmailRecordTO("", "", "", emailAddress, password, active, false, serialNum, updated, "",
	 * startDate, "", "");
	 * 
	 * records.add(emailRecord); } } catch (Exception e) {
	 * System.out.println("EmailRecordDAO::objectFactory() - Failed to create Email Record for address: " + emailAddress + "  " +
	 * e.getMessage()); }
	 * 
	 * return records; }
	 */
	/**
	 * 
	 * @param accountNumber
	 * @return
	 * @throws UsatException
	 */
	public Collection<EmailRecordIntf> getEmailRecordsForAccountNumber(String accountNumber) throws UsatException {
		return this.getEmailRecordsForAccountNumberAndPub(accountNumber, null);
	}

	/**
	 * 
	 * @param emailAddress
	 * @return
	 * @throws UsatException
	 */
	public Collection<EmailRecordIntf> getEmailRecordsForEmailAddress(String emailAddress) throws UsatException {
		Collection<EmailRecordIntf> records = null;
		// java.sql.Connection conn = null;
		try {

			GetUserResponse response = null;
			GetUser gUser = new GetUser();
			response = gUser.getUsers(emailAddress);
			if (!response.containsErrors()) {
				records = EmailRecordDAO.objectFactory(emailAddress, response.getFireflyUserId());
			} else {
				if (response.getErrorCode() == 303) { // Reset password is required
					records = EmailRecordDAO.objectFactory(emailAddress, "303"); // Normally the second parm contains FireFlyUserId,
																				 // but no where else to return error code
				}
			}

			/*
			 * conn = ConnectionManager.getInstance().getConnection(); java.sql.CallableStatement statement =
			 * conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_DATA_BY_EMAIL); statement.setString(1, emailAddress); ResultSet rs =
			 * statement.executeQuery(); records = EmailRecordDAO.objectFactory(rs);
			 */
		} catch (Exception e) {
			// System.out.println("EmailRecordDAO : getEmailRecordsForEmailAddress() " + e.getMessage());
			System.out.println("userservice GetUser API had issues retrieving the user " + e.getMessage());
			throw new UsatException(e);
		} finally {
			// if (conn != null) {
			// this.cleanupConnection(conn);
			// }
		}

		return records;
	}

	/**
	 * 
	 * @param emailAddress
	 * @return
	 * @throws UsatException
	 */
	public Collection<EmailRecordIntf> getEmailRecordsForEmailAddressLike(String emailAddress) throws UsatException {

		Collection<EmailRecordIntf> records = getEmailRecordsForEmailAddress(emailAddress);

		/*
		 * Collection<EmailRecordIntf> records = null; java.sql.Connection conn = null; try { conn =
		 * ConnectionManager.getInstance().getConnection();
		 * 
		 * String filterString = null; if (emailAddress != null) { filterString = emailAddress; } else { throw new
		 * UsatException("No filter provided. Please narrow search."); }
		 * 
		 * CallableStatement countStatment = conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_COUNT_BY_EMAIL_LIKE); // pull count first
		 * int numberRecords = 0;
		 * 
		 * countStatment.setString(1, filterString);
		 * 
		 * ResultSet countRS = countStatment.executeQuery();
		 * 
		 * if (countRS.next()) { numberRecords = countRS.getInt(1); }
		 * 
		 * countStatment.close();
		 * 
		 * if (numberRecords > 500) { throw new UsatException("Too many records will return (" + numberRecords +
		 * "). Please narrow search."); }
		 * 
		 * if (numberRecords > 0) { java.sql.CallableStatement statement =
		 * conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_DATA_BY_EMAIL_LIKE);
		 * 
		 * statement.setString(1, filterString); ResultSet rs = statement.executeQuery();
		 * 
		 * records = EmailRecordDAO.objectFactory(rs); } else { records = new ArrayList<EmailRecordIntf>(); }
		 * 
		 * } catch (Exception e) { System.out.println("EmailRecordDAO : getEmailRecordsForEmailAddress() " + e.getMessage()); throw
		 * new UsatException(e); } finally { if (conn != null) { this.cleanupConnection(conn); } }
		 */
		return records;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws UsatException
	 */
	public Collection<EmailRecordIntf> getEmailRecordsForEmailAddressAndPassword(String emailAddress, String password)
			throws UsatException {

		return this.getEmailRecordsForEmailAddressAndPasswordAndPub(emailAddress, password, null);
	}

	/**
	 * 
	 * @param accountNumber
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	public Collection<EmailRecordIntf> getEmailRecordsForAccountNumberAndPub(String accountNumber, String pubCode)
			throws UsatException {
		Collection<EmailRecordIntf> records = null;
		/*
		 * java.sql.Connection conn = null; try {
		 * 
		 * conn = ConnectionManager.getInstance().getConnection();
		 * 
		 * java.sql.CallableStatement statement = conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_BY_ACCOUNT_NUM_AND_PUB);
		 * 
		 * statement.setString(1, accountNumber); if (pubCode != null) { statement.setString(2, pubCode); } else {
		 * statement.setNull(2, Types.CHAR); }
		 * 
		 * ResultSet rs = statement.executeQuery();
		 * 
		 * records = EmailRecordDAO.objectFactory(rs);
		 * 
		 * } catch (Exception e) { System.out.println("EmailRecordDAO : getEmailRecordsForAccountNumberAndPub() " + e.getMessage());
		 * throw new UsatException(e); } finally { if (conn != null) { this.cleanupConnection(conn); } }
		 */
		return records;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	public Collection<EmailRecordIntf> getEmailRecordsForEmailAddressAndPasswordAndPub(String emailAddress, String password,
			String pubCode) throws UsatException {

		Collection<EmailRecordIntf> records = getLoginEmailRecordsForEmailAddress(emailAddress, password);
		/*
		 * Collection<EmailRecordIntf> records = null; java.sql.Connection conn = null; try {
		 * 
		 * conn = ConnectionManager.getInstance().getConnection();
		 * 
		 * java.sql.CallableStatement statement = conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_DATA_BY_EMAIL_AND_PASSWORD_AND_PUB);
		 * 
		 * statement.setString(1, emailAddress); statement.setString(2, password); if (pubCode != null) { statement.setString(3,
		 * pubCode); } else { statement.setNull(3, Types.CHAR); }
		 * 
		 * ResultSet rs = statement.executeQuery();
		 * 
		 * records = EmailRecordDAO.objectFactory(rs); } catch (Exception e) {
		 * System.out.println("EmailRecordDAO : getEmailRecordsForEmailAddressAndPasswordAndPub() " + e.getMessage()); throw new
		 * UsatException(e); } finally { if (conn != null) { this.cleanupConnection(conn); } }
		 */
		return records;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param account
	 * @return
	 * @throws UsatException
	 */
	public Collection<EmailRecordIntf> getEmailRecordsForEmailAddressAndPasswordAndAccount(String emailAddress, String password,
			String account) throws UsatException {

		Collection<EmailRecordIntf> records = getLoginEmailRecordsForEmailAddress(emailAddress, password);
		/*
		 * Collection<EmailRecordIntf> records = null; java.sql.Connection conn = null;
		 * 
		 * try { GetUserResponse response = null; GetUser gUser = new GetUser(); response = gUser.getGetUser(emailAddress); if
		 * (!response.containsErrors()) { records = EmailRecordDAO.objectFactory(emailAddress, response.getUserId()); }
		 * 
		 * conn = ConnectionManager.getInstance().getConnection();
		 * 
		 * java.sql.CallableStatement statement = conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_BY_EMAIL_PWD_ACCOUNT);
		 * 
		 * statement.setString(1, emailAddress); statement.setString(2, password); statement.setString(3, account);
		 * 
		 * ResultSet rs = statement.executeQuery();
		 * 
		 * records = EmailRecordDAO.objectFactory(rs); } catch (Exception e) {
		 * System.out.println("EmailRecordDAO : getEmailRecordsForEmailAddressAndPasswordAndAccount() " + e.getMessage()); throw new
		 * UsatException(e); } finally { if (conn != null) { this.cleanupConnection(conn); } }
		 */
		return records;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param account
	 * @return
	 * @throws UsatException
	 */
	public Collection<EmailRecordIntf> getEmailRecordsForPubAndEmailAddressAndAccount(String pubCode, String emailAddress,
			String account) throws UsatException {

		Collection<EmailRecordIntf> records = getEmailRecordsForEmailAddress(emailAddress);

		/*
		 * Collection<EmailRecordIntf> records = null; java.sql.Connection conn = null; try { GetUserResponse response = null;
		 * GetUser gUser = new GetUser(); response = gUser.getGetUser(emailAddress); if (!response.containsErrors()) { records =
		 * EmailRecordDAO.objectFactory(emailAddress, response.getUserId()); }
		 * 
		 * conn = ConnectionManager.getInstance().getConnection();
		 * 
		 * java.sql.CallableStatement statement = conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_BY_PUB_EMAIL_ACCOUNT);
		 * 
		 * statement.setString(1, pubCode); statement.setString(2, emailAddress); statement.setString(3, account);
		 * 
		 * ResultSet rs = statement.executeQuery();
		 * 
		 * records = EmailRecordDAO.objectFactory(rs); } catch (Exception e) {
		 * System.out.println("EmailRecordDAO : getEmailRecordsForPubAndEmailAddressAndAccount() " + e.getMessage()); throw new
		 * UsatException(e); } finally { if (conn != null) { this.cleanupConnection(conn); } }
		 */
		return records;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param account
	 * @return
	 * @throws UsatException
	 */
	public Collection<EmailRecordIntf> getEmailRecordsForEmailAddressAndAccount(String emailAddress, String account)
			throws UsatException {

		Collection<EmailRecordIntf> records = getEmailRecordsForEmailAddress(emailAddress);

		/*
		 * Collection<EmailRecordIntf> records = null; java.sql.Connection conn = null; try {
		 * 
		 * conn = ConnectionManager.getInstance().getConnection();
		 * 
		 * java.sql.CallableStatement statement = conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_BY_EMAIL_ACCOUNT);
		 * 
		 * statement.setString(1, emailAddress); statement.setString(2, account);
		 * 
		 * ResultSet rs = statement.executeQuery();
		 * 
		 * records = EmailRecordDAO.objectFactory(rs); } catch (Exception e) {
		 * System.out.println("EmailRecordDAO : getEmailRecordsForPubAndEmailAddressAndAccount() " + e.getMessage()); throw new
		 * UsatException(e); } finally { if (conn != null) { this.cleanupConnection(conn); } }
		 */
		return records;
	}

	/**
	 * 
	 * @param serialNumber
	 * @return The email address tied to the serial number
	 * @throws UsatException
	 */
	public String getEmailAddressForSerialNumber(int serialNumber) throws UsatException {
		// java.sql.Connection conn = null;
		String emailAddress = null;
		/*
		 * try {
		 * 
		 * conn = ConnectionManager.getInstance().getConnection();
		 * 
		 * java.sql.CallableStatement statement = conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_DATA_BY_SERIALNUMBER);
		 * statement.setInt(1, serialNumber);
		 * 
		 * ResultSet rs = statement.executeQuery();
		 * 
		 * if (rs.next()) { emailAddress = rs.getString(1); if (emailAddress != null) { emailAddress = emailAddress.trim(); } }
		 * 
		 * } catch (Exception e) { System.out.println("EmailRecordDAO : getEmailAddressForSerialNumber() " + e.getMessage()); throw
		 * new UsatException(e); } finally { if (conn != null) { this.cleanupConnection(conn); } }
		 */
		return emailAddress;
	}

	/**
	 * 
	 * @param serialNumber
	 * @return
	 * @throws UsatException
	 */
	public EmailRecordIntf getEmailRecordForSerialNumber(int serialNumber) throws UsatException {
		// java.sql.Connection conn = null;
		// Collection<EmailRecordIntf> records = null;
		EmailRecordIntf emailRecord = null;
		/*
		 * try {
		 * 
		 * conn = ConnectionManager.getInstance().getConnection();
		 * 
		 * java.sql.CallableStatement statement = conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_RECORD_DATA_BY_SERIALNUMBER);
		 * statement.setInt(1, serialNumber);
		 * 
		 * ResultSet rs = statement.executeQuery();
		 * 
		 * records = EmailRecordDAO.objectFactory(rs);
		 * 
		 * if (records.size() == 1) { emailRecord = records.iterator().next(); }
		 * 
		 * } catch (Exception e) { System.out.println("EmailRecordDAO : getEmailRecordForSerialNumber() " + e.getMessage()); throw
		 * new UsatException(e); } finally { if (conn != null) { this.cleanupConnection(conn); } }
		 */
		return emailRecord;
	}

	/**
	 * 
	 * @param email
	 * @return primary key of inserted record.
	 * @throws UsatException
	 */
	public int insert(EmailRecordIntf email) throws UsatException {
		java.sql.Connection conn = null;
		int primKey = -1;
		try {

			UsersResponse response = null;
			CreateUser cUser = new CreateUser();
			response = cUser.createUser(email.getFirstName(), email.getLastName(), email.getEmailAddress(), email.getPassword()); // Creating
																																	// user,
																																	// so
																																	// no
																																	// user
																																	// ID
																																	// is
																																	// provided
			if (!response.containsErrors()) {
				primKey = Integer.parseInt(response.getFireflyUserId());
			} else {
				System.out.println(response.getErrorMessages().toString());
			}

			/*
			 * conn = ConnectionManager.getInstance().getConnection();
			 * 
			 * java.sql.Statement statement = conn.createStatement();
			 * 
			 * StringBuilder sqlInsert = new StringBuilder(
			 * "insert into XTRNTEMAIL (EmailAddress, WebPassword, PermStartDate, DateUpdated, TimeUpdated, Active) values ("); if
			 * (email.getEmailAddress() != null) {
			 * sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(email.getEmailAddress())).append("',"); } else {
			 * sqlInsert.append("null,"); }
			 * 
			 * if (email.getPassword() != null) {
			 * sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(email.getPassword())).append("',"); } else {
			 * sqlInsert.append("null,"); }
			 * 
			 * String date = UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat(); String startDate = null; if
			 * (email.getPermStartDate() == null || email.getPermStartDate().length() != 8) { startDate = date; } else { startDate =
			 * email.getPermStartDate(); }
			 * 
			 * sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(startDate)).append("',");
			 * sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(date)).append("',");
			 * sqlInsert.append("'").append(UsatDateTimeFormatter.getSystemTimeHHMMSS()).append("',"); if (email.isActive()) {
			 * sqlInsert.append("'Y'"); } else { sqlInsert.append("'N'"); }
			 * 
			 * sqlInsert.append(")");
			 * 
			 * // execute the SQL int rowsAffected = 0;
			 * 
			 * if (UsaTodayConstants.debug) { System.out.println(sqlInsert.toString()); }
			 * 
			 * rowsAffected = statement.executeUpdate(sqlInsert.toString(), Statement.RETURN_GENERATED_KEYS);
			 * 
			 * if (rowsAffected != 1) { throw new UsatException(
			 * "EmailRecordDAO: No Records Inserted and yet no exception condition exists : Number rows affected: " + rowsAffected);
			 * }
			 * 
			 * ResultSet rs = statement.getGeneratedKeys();
			 * 
			 * rs.next(); primKey = rs.getInt(1);
			 */
		} catch (Exception e) {
			System.out.println("EmailRecordDAO : insert() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}
		return primKey;
	}

	/**
	 * 
	 * @param email
	 * @throws UsatException
	 */
	public void update(EmailRecordIntf email) throws UsatException {
		java.sql.Connection conn = null;
		try {

			// Use users API to update user
			UsersResponse response = null;
			UpdateUser cUser = new UpdateUser();
			response = cUser.updateUser(email.getFirstName(), email.getLastName(), email.getEmailAddress(),
					email.getFireflyUserId());

			if (response.containsErrors()) {
				System.out.println(response.getErrorMessages().toString());
			}

			/*
			 * conn = ConnectionManager.getInstance().getConnection();
			 * 
			 * java.sql.PreparedStatement statement = conn.prepareStatement(EmailRecordDAO.UPDATE);
			 * 
			 * // 1 PubCode, AccountNum, WebID, EmailAddress, WebPassword, // 6 AgencyName, TransType, PermStartDate, DateUpdaetd,
			 * TimeUpdated, OptIn, Zip statement.setString(1, email.getEmailAddress()); statement.setString(2, email.getPassword());
			 * String date = UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat();
			 * 
			 * if (email.getPermStartDate() == null || email.getPermStartDate().length() != 8) { statement.setString(3, date); }
			 * else { statement.setString(3, email.getPermStartDate()); } statement.setString(4, date); statement.setString(5,
			 * UsatDateTimeFormatter.getSystemTimeHHMMSS()); if (email.isActive()) { statement.setString(6, "Y"); } else {
			 * statement.setString(6, "N"); }
			 * 
			 * statement.setInt(7, email.getSerialNumber());
			 * 
			 * statement.executeUpdate();
			 */
		} catch (Exception e) {
			System.out.println("EmailRecordDAO : update() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}
	}

	/**
	 * 
	 * @param emailAddress
	 * @return
	 * @throws UsatException
	 */
	public Collection<EmailRecordIntf> getLoginEmailRecordsForEmailAddress(String emailAddress, String password)
			throws UsatException {
		Collection<EmailRecordIntf> records = null;
		// java.sql.Connection conn = null;
		try {

			LoginResponse response = null;
			Login gUser = new Login();
			response = gUser.getLogin(emailAddress, password);
			if (!response.containsErrors()) {
				records = EmailRecordDAO.objectFactoryLogin(emailAddress, response.getUserId(), response.getAutoLoginHash(),
						response.getSessionKey());
			} else {
				if (response.getErrorCode() == 303) { // Reset password is required
					records = EmailRecordDAO.objectFactory(emailAddress, "303");
				}
			}

			/*
			 * conn = ConnectionManager.getInstance().getConnection(); java.sql.CallableStatement statement =
			 * conn.prepareCall(EmailRecordDAO.SELECT_EMAIL_DATA_BY_EMAIL); statement.setString(1, emailAddress); ResultSet rs =
			 * statement.executeQuery(); records = EmailRecordDAO.objectFactory(rs);
			 */
		} catch (Exception e) {
			// System.out.println("EmailRecordDAO : getEmailRecordsForEmailAddress() " + e.getMessage());
			System.out.println("userservice GetUser API had issues retrieving the user " + e.getMessage());
			throw new UsatException(e);
		} finally {
			// if (conn != null) {
			// this.cleanupConnection(conn);
			// }
		}

		return records;
	}

	/**
	 * 
	 * This method generates transfer objects for each record in the result set.
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private synchronized static Collection<EmailRecordIntf> objectFactory(String emailAddress, String userId) throws UsatException {
		Collection<EmailRecordIntf> records = new ArrayList<EmailRecordIntf>();

		try {
			// iterate over result set and build objects
			String password = null;
			String startDate = null;
			Date updated = null;
			boolean active = true;
			int serialNum = 0;
			if (!userId.trim().equals("")) {
				serialNum = Integer.parseInt(userId);
			}
			password = "";
			startDate = UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat();
			active = true;
			updated = new Date();

			/*
			 * String pubCode, String accountNumber, String orderID, String emailAddress, String password, boolean active, boolean
			 * gift, int serialNumber, Date dateUpdated, String emailSent String permStart
			 */
			EmailRecordTO emailRecord = new EmailRecordTO("", "", "", emailAddress, password, active, false, serialNum, updated,
					"", startDate, "", userId, "", "", "", "");

			records.add(emailRecord);
		} catch (Exception e) {
			System.out.println("EmailRecordDAO::objectFactory() - Failed to create Email Record for address: " + emailAddress
					+ "  " + e.getMessage());
		}

		return records;
	}

	/**
	 * 
	 * This method generates transfer objects for each record in the result set.
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private synchronized static Collection<EmailRecordIntf> objectFactoryLogin(String emailAddress, String userId,
			String autoLogin, String sessionKey) throws UsatException {
		Collection<EmailRecordIntf> records = new ArrayList<EmailRecordIntf>();

		try {
			// iterate over result set and build objects
			String password = null;
			String startDate = null;
			Date updated = null;
			boolean active = true;
			int serialNum = 0;
			if (!userId.trim().equals("")) {
				serialNum = Integer.parseInt(userId);
			}
			password = "";
			startDate = UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat();
			active = true;
			updated = new Date();

			/*
			 * String pubCode, String accountNumber, String orderID, String emailAddress, String password, boolean active, boolean
			 * gift, int serialNumber, Date dateUpdated, String emailSent String permStart
			 */
			EmailRecordTO emailRecord = new EmailRecordTO("", "", "", emailAddress, password, active, false, serialNum, updated,
					"", startDate, "", userId, "", "", autoLogin, sessionKey);

			records.add(emailRecord);
		} catch (Exception e) {
			System.out.println("EmailRecordDAO::objectFactory() - Failed to create Email Record for address: " + emailAddress
					+ "  " + e.getMessage());
		}

		return records;
	}

}
