/*
 * Created on May 2, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import com.usatoday.UsatException;

/**
 * @author aeast
 * @date May 2, 2006
 * @class Zip5DAO
 * 
 *        This class communicates witht he persistence layer for GUI Address Zip codes.
 * 
 */
public class Zip5DAO extends USATodayDAO {

	public static final String GET_ZIPCODES = "SELECT distinct ZipCode, PubCode from ZIP5GUI where Active = 'Y'";

	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public HashMap<String, String> getGUIZips() throws UsatException {
		Connection conn = null;
		HashMap<String, String> results = new HashMap<String, String>();
		try {
			// get a connection to the database
			conn = ConnectionManager.getInstance().getConnection();

			// create a prepared statement and substitute the parameters
			PreparedStatement statement = conn.prepareStatement(Zip5DAO.GET_ZIPCODES);

			// get the result
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				String zip = rs.getString("ZipCode");
				String pub = rs.getString("PubCode");
				// for now we don't really have a value
				// the sheer presence of the key means it's a GUI Address
				results.put(pub + zip, zip);
			}

		} catch (Exception e) {
			System.out.println("EmailRecordDAO : getTaxRateForZip() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}
		return results;
	}
}
