package com.usatoday.integration;

import java.io.Serializable;

public class KeycodeTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String pubCode = null;
	private String sourceCode = null;
	private String promoCode = null;
	private String contestCode = null;
	private String description = null;
	private String offerCode = null;
	private boolean billMeAllowed = false;
	private boolean forceEZPay = false;
	private boolean forceBillMe = false;
	private boolean ClubNumberRequired = false;
	private double dailyRate = 0.0;

	public String getPubCode() {
		return pubCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getContestCode() {
		return contestCode;
	}

	public void setContestCode(String contestCode) {
		this.contestCode = contestCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOfferCode() {
		return offerCode;
	}

	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}

	public boolean isBillMeAllowed() {
		return billMeAllowed;
	}

	public void setBillMeAllowed(boolean billMeAllowed) {
		this.billMeAllowed = billMeAllowed;
	}

	public boolean isForceEZPay() {
		return forceEZPay;
	}

	public void setForceEZPay(boolean forceEZPay) {
		this.forceEZPay = forceEZPay;
	}

	public boolean isForceBillMe() {
		return forceBillMe;
	}

	public void setForceBillMe(boolean forceBillMe) {
		this.forceBillMe = forceBillMe;
	}

	public boolean isClubNumberRequired() {
		return ClubNumberRequired;
	}

	public void setClubNumberRequired(boolean clubNumberRequired) {
		ClubNumberRequired = clubNumberRequired;
	}

	public double getDailyRate() {
		return dailyRate;
	}

	public void setDailyRate(double dailyRate) {
		this.dailyRate = dailyRate;
	}

	public String getKeyCode() {
		String kCode = "";
		if (this.sourceCode != null) {
			kCode = this.sourceCode + this.promoCode + this.contestCode;
		}
		return kCode;
	}

}
