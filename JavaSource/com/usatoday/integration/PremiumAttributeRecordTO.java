/*
 * Created on Nov 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;
import java.sql.Timestamp;

import com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf;

/**
 * @author aeast
 * @date Nov 9, 2006
 * @class PremiumAttributeRecordTO
 * 
 *        NO MPF in Genesys
 * 
 * @deprecated
 * 
 */
public class PremiumAttributeRecordTO implements Serializable, PremiumAttributeIntf {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4696332014285354489L;
	// persistent fields
	private String pubCode = null;
	private String accountNum = null;
	private String premiumPromotionCode = null;
	private String premiumCode = null;
	private String premiumType = null;
	private String giftPayerReceive = null;
	private String attributeName = null;
	private String attributeValue = null;
	private String orderId = null;
	private String dateUpdated = null;
	private String timeUpdated = null;
	private Timestamp updateTimestamp = null;
	private long key = 0;
	private int state = PremiumAttributeIntf.NEW;
	private boolean clubNumber = false;

	/**
     * 
     */
	public PremiumAttributeRecordTO() {
		super();
	}

	/**
	 * @return Returns the accountNum.
	 */
	public String getAccountNum() {
		return this.accountNum;
	}

	/**
	 * @param accountNum
	 *            The accountNum to set.
	 */
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	/**
	 * @return Returns the attributeName.
	 */
	public String getAttributeName() {
		return this.attributeName;
	}

	/**
	 * @param attributeName
	 *            The attributeName to set.
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	/**
	 * @return Returns the attributeValue.
	 */
	public String getAttributeValue() {
		return this.attributeValue;
	}

	/**
	 * @param attributeValue
	 *            The attributeValue to set.
	 */
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}

	/**
	 * @return Returns the clubNumber.
	 */
	public boolean isClubNumber() {
		return this.clubNumber;
	}

	/**
	 * @param clubNumber
	 *            The clubNumber to set.
	 */
	public void setClubNumber(boolean clubNumber) {
		this.clubNumber = clubNumber;
	}

	/**
	 * @return Returns the dateUpdated.
	 */
	public String getDateUpdated() {
		return this.dateUpdated;
	}

	/**
	 * @param dateUpdated
	 *            The dateUpdated to set.
	 */
	public void setDateUpdated(String dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	/**
	 * @return Returns the giftPayerReceive.
	 */
	public String getGiftPayerReceive() {
		return this.giftPayerReceive;
	}

	/**
	 * @param giftPayerReceive
	 *            The giftPayerReceive to set.
	 */
	public void setGiftPayerReceive(String giftPayerReceive) {
		this.giftPayerReceive = giftPayerReceive;
	}

	/**
	 * @return Returns the key.
	 */
	public long getKey() {
		return this.key;
	}

	/**
	 * @param key
	 *            The key to set.
	 */
	public void setKey(long key) {
		this.key = key;
	}

	/**
	 * @return Returns the orderId.
	 */
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * @param orderId
	 *            The orderId to set.
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return Returns the premiumCode.
	 */
	public String getPremiumCode() {
		return this.premiumCode;
	}

	/**
	 * @param premiumCode
	 *            The premiumCode to set.
	 */
	public void setPremiumCode(String premiumCode) {
		this.premiumCode = premiumCode;
	}

	/**
	 * @return Returns the premiumPromotionCode.
	 */
	public String getPremiumPromotionCode() {
		return this.premiumPromotionCode;
	}

	/**
	 * @param premiumPromotionCode
	 *            The premiumPromotionCode to set.
	 */
	public void setPremiumPromotionCode(String premiumPromotionCode) {
		this.premiumPromotionCode = premiumPromotionCode;
	}

	/**
	 * @return Returns the premiumType.
	 */
	public String getPremiumType() {
		return this.premiumType;
	}

	/**
	 * @param premiumType
	 *            The premiumType to set.
	 */
	public void setPremiumType(String premiumType) {
		this.premiumType = premiumType;
	}

	/**
	 * @return Returns the pubCode.
	 */
	public String getPubCode() {
		return this.pubCode;
	}

	/**
	 * @param pubCode
	 *            The pubCode to set.
	 */
	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	/**
	 * @return Returns the state.
	 */
	public int getState() {
		return this.state;
	}

	/**
	 * @param state
	 *            The state to set.
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return Returns the timeUpdated.
	 */
	public String getTimeUpdated() {
		return this.timeUpdated;
	}

	/**
	 * @param timeUpdated
	 *            The timeUpdated to set.
	 */
	public void setTimeUpdated(String timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	/**
	 * @return Returns the updateTimestamp.
	 */
	public Timestamp getUpdateTimestamp() {
		return this.updateTimestamp;
	}

	/**
	 * @param updateTimestamp
	 *            The updateTimestamp to set.
	 */
	public void setUpdateTimestamp(Timestamp updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
}
