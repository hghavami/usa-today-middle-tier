/*
 * Created on Apr 19, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;

/**
 * @author aeast
 * @date Apr 19, 2006
 * @class TaxRateTO
 * 
 *        Represents the tax rates for a zip
 * 
 */
public class TaxRateTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8612183001074617481L;
	private String taxCode = null;
	private String deliveryTypeCode = null;
	private double taxRate1 = 0.0;
	private double taxRate2 = 0.0;
	private double taxRate3 = 0.0;
	private double taxRate4 = 0.0;
	private double taxRate5 = 0.0;

	public TaxRateTO(String taxCode, String deliveryTypeCode, double taxRate1, double taxRate2, double taxRate3, double taxRate4,
			double taxRate5) {
		super();
		this.taxCode = taxCode;
		this.deliveryTypeCode = deliveryTypeCode;
		this.taxRate1 = taxRate1;
		this.taxRate2 = taxRate2;
		this.taxRate3 = taxRate3;
		this.taxRate4 = taxRate4;
		this.taxRate5 = taxRate5;
	}

	public double getTaxRate1() {
		return this.taxRate1;
	}

	public double getTaxRate2() {
		return this.taxRate2;
	}

	public double getTaxRate3() {
		return this.taxRate3;
	}

	public double getTaxRate4() {
		return this.taxRate4;
	}

	public double getTaxRate5() {
		return this.taxRate5;
	}

	public String getTaxCode() {
		return this.taxCode;
	}

	public String getDeliveryTypeCode() {
		return this.deliveryTypeCode;
	}
}
