/*
 * Created on Apr 19, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author aeast
 * @date Apr 19, 2006
 * @class TaxRateForZipTO
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class TaxRateForZipTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7682545551189118719L;
	private String zip = null;
	Collection<TaxRateTO> taxRates = null;

	public Collection<TaxRateTO> getTaxRates() {
		return this.taxRates;
	}

	public String getZip() {
		return this.zip;
	}

	public void setTaxRates(Collection<TaxRateTO> taxRates) {
		this.taxRates = taxRates;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
}
