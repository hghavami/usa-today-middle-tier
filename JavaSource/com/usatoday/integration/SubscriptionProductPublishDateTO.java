package com.usatoday.integration;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.products.SubscriptionPublishDateIntf;

public class SubscriptionProductPublishDateTO implements SubscriptionPublishDateIntf, Serializable {

	private String pubCode = null;
	private DateTime date = null;
	private String dayOfWeekName = null;
	private boolean isPublishingDay = false;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DateTime getDate() {
		return this.date;
	}

	public String getDayOfWeekName() {
		return this.dayOfWeekName;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public boolean isPublishedOnThisDay() {
		return this.isPublishingDay;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public void setDayOfWeekName(String dayOfWeekName) {
		this.dayOfWeekName = dayOfWeekName;
	}

	public void setPublishingDay(boolean isPublishingDay) {
		this.isPublishingDay = isPublishingDay;
	}

}
