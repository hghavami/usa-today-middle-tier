package com.usatoday.integration;

import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;

public class EmailRecordTO implements EmailRecordIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -890892268908466305L;
	private String pubCode = null;
	private String accountNumber = null;
	private String orderID = null;
	private String emailAddress = null;
	private String password = null;
	private boolean active = true;
	private boolean gift = false;
	private String permStartDate = null;
	private int serialNumber = 0;
	private Date dateUpdated = null;
	private String zip = null;
	private String transType = null;
	private String confirmationEmailSent = null;
	private String userId = null;
	private String firstName = null;
	private String lastName = null;
	private String autoLogin = null;
	private String sessionKey = null;

	public EmailRecordTO(String pubCode, String accountNumber, String orderID, String emailAddress, String password,
			boolean active, boolean gift, int serialNumber, Date dateUpdated, String confirmEmailSent, String permStart,
			String zip, String userId, String firstName, String lastName, String autoLogin, String sessionKey) {
		super();
		this.pubCode = pubCode;
		this.accountNumber = accountNumber;
		this.orderID = orderID;
		this.emailAddress = emailAddress;
		this.password = password;
		this.active = active;
		this.gift = gift;
		this.serialNumber = serialNumber;
		this.dateUpdated = dateUpdated;
		this.confirmationEmailSent = confirmEmailSent;
		this.permStartDate = permStart;
		this.zip = zip;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.autoLogin = autoLogin;
		this.sessionKey = sessionKey;
	}

	public String getPassword() {
		return this.password;
	}

	public int getSerialNumber() {
		return this.serialNumber;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getOrderID() {
		return this.orderID;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Date getLastUpdated() {
		return dateUpdated;
	}

	public void setLastUpdated(Date date) {
		this.dateUpdated = date;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean activeFlag) {
		this.active = activeFlag;
	}

	public boolean isGift() {
		return this.gift;
	}

	public void setGift(boolean giftFlag) {
		this.gift = giftFlag;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String email) {
		this.emailAddress = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPermStartDate() {
		return this.permStartDate;
	}

	public void setPermStartDate(String date) {
		this.permStartDate = date;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return Returns the transType.
	 */
	public String getTransType() {
		return transType;
	}

	/**
	 * @param transType
	 *            The transType to set.
	 */
	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getConfirmationEmailSent() {
		return this.confirmationEmailSent;
	}

	public void setConfirmationEmailSent(String c) {
		this.confirmationEmailSent = c;
	}

	public boolean isStartDateInFuture() {
		boolean startDateInFuture = false;

		if (this.permStartDate != null && this.permStartDate.length() == 8 && !this.permStartDate.equalsIgnoreCase("00000000")) {
			DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(this.permStartDate);
			if (dt.isAfterNow()) {
				startDateInFuture = true;
			}
		}

		return startDateInFuture;
	}

	public String getFireflyUserId() {
		return userId;
	}

	public void setFireflyUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAutoLogin() {
		return autoLogin;
	}

	public void setAutoLogin(String autoLogin) {
		this.autoLogin = autoLogin;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
}
