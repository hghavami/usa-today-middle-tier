package com.usatoday.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.UsatException;

public class SubscriptionProductPublishDateDAO extends USATodayDAO {

	private static final String SELECT_ALL_PUBLISH_DATES_GREATEREQUAL_TO_DATE = "select PubCode, PubDate, DayName, Publish from XTRNTPUBDATE where PubDate >=  ?";
	private static final String SELECT_ALL_PUBLISH_DATES = "select PubCode, PubDate, DayName, Publish from XTRNTPUBDATE";

	private Collection<SubscriptionProductPublishDateTO> objectFactory(ResultSet rs) throws UsatException {

		ArrayList<SubscriptionProductPublishDateTO> dates = new ArrayList<SubscriptionProductPublishDateTO>();

		try {
			// iterate over result set and build objects

			while (rs.next()) {

				SubscriptionProductPublishDateTO date = new SubscriptionProductPublishDateTO();

				String temp = rs.getString("PubCode");
				if (temp != null) {
					date.setPubCode(temp.trim());
				}

				java.sql.Timestamp ts = rs.getTimestamp("PubDate");
				if (ts != null) {
					DateTime dt = new DateTime(ts.getTime());
					date.setDate(dt);
				}

				temp = rs.getString("DayName");
				if (temp != null) {
					date.setDayOfWeekName(temp.trim());
				}

				temp = rs.getString("Publish");
				if (temp != null && temp.trim().equalsIgnoreCase("TRUE")) {
					date.setPublishingDay(true);
				} else {
					date.setPublishingDay(false);
				}

				dates.add(date);
			}
		} catch (Exception e) {
			System.out.println("SubscriptionProductPublishDateDAO::objectFactory() - Failed to create Publish Date Record: "
					+ e.getMessage());
		}

		return dates;

	}

	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public Collection<SubscriptionProductPublishDateTO> fetchFuturePublishingDates() throws UsatException {

		Collection<SubscriptionProductPublishDateTO> dates = null;
		Connection conn = null;

		try {

			DateTime dt = new DateTime();

			conn = ConnectionManager.getInstance().getConnection();
			PreparedStatement ps = conn
					.prepareStatement(SubscriptionProductPublishDateDAO.SELECT_ALL_PUBLISH_DATES_GREATEREQUAL_TO_DATE);

			ps.setString(1, dt.toString("M/dd/yyyy"));

			ResultSet rs = ps.executeQuery();
			dates = objectFactory(rs);
		} catch (UsatException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return dates;
	}

	/**
	 * 
	 * @return Collection of all publishing dates in db
	 * @throws UsatException
	 */
	public Collection<SubscriptionProductPublishDateTO> fetchPublishingDates() throws UsatException {

		Collection<SubscriptionProductPublishDateTO> dates = null;
		Connection conn = null;

		try {

			conn = ConnectionManager.getInstance().getConnection();
			PreparedStatement ps = conn.prepareStatement(SubscriptionProductPublishDateDAO.SELECT_ALL_PUBLISH_DATES);

			ResultSet rs = ps.executeQuery();
			dates = objectFactory(rs);
		} catch (UsatException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}
		return dates;
	}
}
