package com.usatoday.integration;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.customer.TrialCustomerTOIntf;
import com.usatoday.business.interfaces.customer.TrialInstanceIntf;
import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.businessObjects.customer.ContactBO;

public class TrialCustomerTO implements TrialCustomerTOIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -64893709117714498L;

	private long id = -1;

	private String address1 = null;
	private String address2 = null;
	private String apartmentSuite = null;
	private String city = null;
	private String clubNumber = null;
	private String companyName = null;
	private String email = null;
	private DateTime startDate = null;
	private DateTime endDate = null;
	private String firstName = null;
	private boolean active = false;
	private boolean subscribed = false;
	private String keyCode = null;
	private String lastName = null;
	private String partnerID = null;
	private String phone = null;
	private String pubCode = null;
	private String additionalAddress = null;
	private String state = null;
	private String zip = null;
	private DateTime inserted = null;
	// private DateTime updated = null;
	private String password = null;
	private String clientIP = null;
	private String ncsRepID = null;
	private String confirmationEmailSent = null;

	public TrialCustomerTO() {
		super();
	}

	/**
	 * 
	 * @param source
	 *            The object to insert or update.
	 */
	public TrialCustomerTO(TrialInstanceIntf source) {
		super();

		this.id = source.getID();
		this.active = source.getIsActive();
		this.subscribed = source.getIsSubscribed();
		this.ncsRepID = source.getNcsRepID();

		if (source.getContactInformation() != null) {
			ContactBO contact = source.getContactInformation();
			this.address1 = contact.getUIAddress().getAddress1();
			this.address2 = contact.getUIAddress().getAddress2();
			this.additionalAddress = "";
			this.apartmentSuite = contact.getUIAddress().getAptSuite();
			this.city = contact.getUIAddress().getCity();
			this.state = contact.getUIAddress().getState();
			this.zip = contact.getUIAddress().getZip();
			this.companyName = contact.getFirmName();
			this.email = contact.getEmailAddress();
			this.phone = contact.getHomePhone();
			this.firstName = contact.getFirstName();
			this.lastName = contact.getLastName();
			this.password = contact.getPassword();
		}

		this.clientIP = source.getClientIP();
		this.keyCode = source.getKeyCode();

		// pull partner
		TrialPartnerIntf p = source.getPartner();
		if (p != null) {
			this.partnerID = p.getPartnerID();
			this.pubCode = p.getPubCode();
		}

		this.clubNumber = source.getClubNumber();
		this.startDate = source.getStartDate();
		this.endDate = source.getEndDate();
		this.confirmationEmailSent = source.getConfirmationEmailSent();

	}

	public String getAdditionalAddress() {
		return this.additionalAddress;
	}

	public String getAddress1() {
		return this.address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public String getApartmentSuite() {
		return this.apartmentSuite;
	}

	public String getCity() {
		return this.city;
	}

	public String getClubNumber() {
		return this.clubNumber;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public String getEmail() {
		return this.email;
	}

	public DateTime getEndDate() {
		return this.endDate;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public long getID() {
		return this.id;
	}

	public boolean getIsActive() {
		return this.active;
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public String getLastName() {
		return this.lastName;
	}

	public String getPartnerID() {
		return this.partnerID;
	}

	public String getPhone() {
		return this.phone;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public DateTime getStartDate() {
		return this.startDate;
	}

	public String getState() {
		return this.state;
	}

	public String getZip() {
		return this.zip;
	}

	public void setAdditionalAddress(String addlAddress) {
		this.additionalAddress = addlAddress;
	}

	public void setAddress1(String addr1) {
		this.address1 = addr1;
	}

	public void setAddress2(String addr2) {
		this.address2 = addr2;
	}

	public void setApartmentSuite(String apt) {
		this.apartmentSuite = apt;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setClubNumber(String number) {
		this.clubNumber = number;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	public void setFirstName(String name) {
		this.firstName = name;
	}

	public void setID(long id) {
		this.id = id;
	}

	public void setIsActive(boolean activeFlag) {
		this.active = activeFlag;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public void setLastName(String name) {
		this.lastName = name;
	}

	public void setPartnerID(String pid) {
		this.partnerID = pid;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setPubCode(String pub) {
		this.pubCode = pub;
	}

	public void setStartDate(DateTime start) {
		this.startDate = start;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public DateTime getInsertedDate() {
		return this.inserted;
	}

	public void setInsertedDate(DateTime inserted) {
		this.inserted = inserted;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getIsSubscribed() {
		return this.subscribed;
	}

	public void setIsSubscribed(boolean subscribedFlag) {
		this.subscribed = subscribedFlag;
	}

	public String getClientIP() {
		return this.clientIP;
	}

	public void setClientIP(String ip) {
		this.clientIP = ip;
	}

	public String getNcsRepID() {
		return ncsRepID;
	}

	public void setNcsRepID(String id) {
		this.ncsRepID = id;
	}

	public String getConfirmationEmailSent() {
		return this.confirmationEmailSent;
	}

	public void setConfirmationEmailSent(String flag) {
		if (flag == null || flag.trim().length() > 1) {
			this.confirmationEmailSent = "Y";
		} else {
			this.confirmationEmailSent = flag.trim();
		}
	}

}
