/*
 * Created on Apr 14, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;
import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.RenewalOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.businessObjects.products.promotions.PromotionSet;

/**
 * @author aeast
 * @date Apr 14, 2006
 * @class RenewalOfferTO
 * 
 *        Change this clas to implement a non BO interface..just something for storage.
 * 
 */
public class RenewalOfferTO implements RenewalOfferIntf, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3691032801646262635L;
	private String rateCode = "";
	private String relatedRateCode = "";
	private Collection<SubscriptionTermsIntf> terms = null;
	private String pubCode = "";
	private String keyCode = "";

	// public RenewalOfferTO(String rateCode, String pubCode) {
	// super();
	// this.rateCode = rateCode;
	// this.pubCode = pubCode;
	// }
	//
	public RenewalOfferTO(String keyCode, String pubCode) {
		super();
		this.keyCode = keyCode;
		this.pubCode = pubCode;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public String getRateCode() {
		return this.rateCode;
	}

	public String getRelatedRateCode() {
		return this.relatedRateCode;
	}

	public Collection<SubscriptionTermsIntf> getTerms() {
		return this.terms;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public void setRelatedRateCode(String relatedRateCode) {
		this.relatedRateCode = relatedRateCode;
	}

	public void setTerms(Collection<SubscriptionTermsIntf> terms) {
		this.terms = terms;
	}

	public String getKey() {
		return this.getRateCode() + this.getRelatedRateCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.OfferIntf#getTerm(java.lang.String)
	 */
	public SubscriptionTermsIntf getTerm(String termLength) {
		// method not implemented in TO
		return null;
	}

	public SubscriptionTermsIntf findTermUsingTermLength(String termLength) {
		// This method implemented in business object only
		return null;
	}

	@Override
	public PromotionSet getPromotionSet() {
		// currently on promotions for renewals
		return null;
	}

	@Override
	public boolean isForceEZPay() {
		// Bo Method only
		return false;
	}

	@Override
	public ProductIntf getProduct() throws UsatException {
		// BO method only
		return null;
	}

	@Override
	public SubscriptionTermsIntf getRenewalTerm(String termAsString) {
		// method not implemented in TO
		return null;
	}

	public String getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}
}
