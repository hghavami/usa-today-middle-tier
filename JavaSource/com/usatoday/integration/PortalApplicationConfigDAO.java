package com.usatoday.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.UsatException;

public class PortalApplicationConfigDAO extends USATodayDAO {

	// query
	private static final String SELECT_ALL = "select id, [group], [key], value, description from XTRNT_APP_CONFIG order by [group]";

	// private static final String SELECT_SETTING_BY_KEY =
	// "select id, update_count, updated, group, key, value, description from XTRNT_APP_CONFIG where key = ?";

	private Collection<PortalApplicationSettingTO> objectFactory(ResultSet rs) throws UsatException {
		ArrayList<PortalApplicationSettingTO> al = new ArrayList<PortalApplicationSettingTO>();

		try {
			while (rs.next()) {
				PortalApplicationSettingTO config = new PortalApplicationSettingTO();

				String tempStr = null;

				int tempInt = rs.getInt("id");
				config.setID(tempInt);

				tempStr = rs.getString("group");
				if (tempStr == null) {
					tempStr = "";
				}
				config.setGroup(tempStr.trim());

				tempStr = rs.getString("key");
				if (tempStr == null) {
					tempStr = "";
				}
				config.setKey(tempStr.trim());

				tempStr = rs.getString("value");
				if (tempStr == null) {
					tempStr = "";
				}
				config.setValue(tempStr.trim());

				tempStr = rs.getString("description");
				if (tempStr == null) {
					tempStr = "";
				}
				config.setDescription(tempStr.trim());

				al.add(config);
			}
		} catch (SQLException sqle) {
			throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
		}
		return al;
	}

	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public Collection<PortalApplicationSettingTO> getApplicationConfigurations() throws UsatException {
		Collection<PortalApplicationSettingTO> results = null;

		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			PreparedStatement ps = conn.prepareStatement(PortalApplicationConfigDAO.SELECT_ALL);

			ResultSet rs = ps.executeQuery();

			results = this.objectFactory(rs);
		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}
		return results;
	}

}
