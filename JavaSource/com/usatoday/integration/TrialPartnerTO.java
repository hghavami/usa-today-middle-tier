package com.usatoday.integration;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.partners.TrialPartnerTOIntf;

public class TrialPartnerTO implements TrialPartnerTOIntf, Serializable {

	private int id = -1;
	private boolean active = false;
	private String confirmationEmailTemplateID = null;
	private String customLandingPageURL = null;
	private String completePageCustomHTML = null;
	private String launchPageCustomHTML = null;
	private String trialOverCustomHTML = null;
	private String disclaimerCustomHTML = null;
	private String description = null;
	private int durationInDays = 0;
	private DateTime startDate = null;
	private DateTime endDate = null;
	private String pubCode = null;
	private String keyCode = null;
	private String partnerName = null;
	private String partnerID = null;
	private String apiSecretHashKey = "";

	private boolean showClubNumber = false;
	private String clubNumberLabel = null;

	private boolean sendConfirmationOnCreation = true;

	private String logoImageURL = null;
	private String logoLinkURL = null;

	private String authLinkURL = null;

	/**
	 * 
	 */
	private static final long serialVersionUID = -4821916274764965378L;

	public String getConfirmationEmailTemplateID() {
		return this.confirmationEmailTemplateID;
	}

	public String getCustomLandingPageURL() {

		return this.customLandingPageURL;
	}

	public String getDescription() {
		return this.description;
	}

	public int getDurationInDays() {
		return this.durationInDays;
	}

	public DateTime getEndDate() {

		return this.endDate;
	}

	public int getID() {

		return this.id;
	}

	public boolean getIsActive() {
		return this.active;
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public String getPartnerID() {
		return this.partnerID;
	}

	public String getPartnerName() {
		return this.partnerName;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public DateTime getStartDate() {
		return this.startDate;
	}

	public void setConfirmationEmaiTemplateID(String id) {
		this.confirmationEmailTemplateID = id;

	}

	public void setCustomerLadingPageURL(String url) {
		this.customLandingPageURL = url;

	}

	public void setDescription(String description) {
		this.description = description;

	}

	public void setDurationInDays(int numDays) {
		this.durationInDays = numDays;

	}

	public void setEndDate(DateTime date) {
		this.endDate = date;

	}

	public void setID(int id) {
		this.id = id;

	}

	public void setIsActive(boolean active) {
		this.active = active;

	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;

	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;

	}

	public void setPartnerID(String pid) {
		this.partnerID = pid;

	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public void setStartDate(DateTime date) {
		this.startDate = date;
	}

	public String getCompletePageCustomHTML() {

		return this.completePageCustomHTML;
	}

	public void setCompletePageCustomHTML(String html) {
		this.completePageCustomHTML = html;
	}

	public String getLaunchPageCustomHTML() {
		return launchPageCustomHTML;
	}

	public void setLaunchPageCustomHTML(String launchPageCustomHTML) {
		this.launchPageCustomHTML = launchPageCustomHTML;
	}

	public String getTrialOverCustomHTML() {
		return trialOverCustomHTML;
	}

	public void setTrialOverCustomHTML(String trialOverCustomHTML) {
		this.trialOverCustomHTML = trialOverCustomHTML;
	}

	public String getDisclaimerCustomHTML() {
		return disclaimerCustomHTML;
	}

	public void setDisclaimerCustomHTML(String disclaimerCustomHTML) {
		this.disclaimerCustomHTML = disclaimerCustomHTML;
	}

	public String getClubNumberLabel() {
		if (this.clubNumberLabel != null) {
			return this.clubNumberLabel;
		} else {
			return "";
		}
	}

	public void setClubNumberLabel(String label) {
		this.clubNumberLabel = label;
	}

	public boolean getIsShowClubNumber() {
		return this.showClubNumber;
	}

	public void setIsShowClubNumber(boolean showClubNum) {
		this.showClubNumber = showClubNum;
	}

	public String getLogoImageURL() {
		return this.logoImageURL;
	}

	public void setLogoImageURL(String url) {
		this.logoImageURL = url;
	}

	public String getLogoLinkURL() {
		return this.logoLinkURL;
	}

	public void setLogoLinkURL(String url) {
		this.logoLinkURL = url;
	}

	public String getAuthLinkURL() {
		return this.authLinkURL;
	}

	public void setAuthLinkURL(String url) {
		this.authLinkURL = url;
	}

	public String getAPISecretHashKey() {
		return this.apiSecretHashKey;
	}

	public void setAPISecretHashKey(String key) {
		if (key != null && key.length() > 0) {
			this.apiSecretHashKey = key;
		} else {
			this.apiSecretHashKey = "";
		}

	}

	public boolean getIsSendConfirmationOnCreation() {
		return this.sendConfirmationOnCreation;
	}

	public void setIsSendConfirmationOnCreation(boolean sendConfirmation) {
		this.sendConfirmationOnCreation = sendConfirmation;

	}

}
