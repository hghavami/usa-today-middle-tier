package com.usatoday.integration;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.products.logging.ElectronicEditionLogTOIntf;

public class ElectronicEditionAccessLogTO implements ElectronicEditionLogTOIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -106524345512856671L;
	private DateTime accessedTime = null;
	private String accountNumber = null;
	private String emailAddress = null;
	private long id = -1;
	private String pubCode = null;
	private String clientIP = null;
	private boolean trialAccess = false;

	public void setAccessedTime(DateTime dt) {
		this.accessedTime = dt;
	}

	public void setAccountNumber(String acctNumber) {
		if (acctNumber != null && acctNumber.length() > 10) {
			// truncate account number
			acctNumber = acctNumber.substring(0, 10);
		}
		this.accountNumber = acctNumber;
	}

	public void setEmailAddress(String address) {
		if (address != null && address.length() > 60) {
			// truncate account number
			address = address.substring(0, 60);
		}
		this.emailAddress = address;
	}

	public void setID(long id) {
		this.id = id;
	}

	public void setPubCode(String pub) {
		if (pub != null && pub.length() > 4) {
			// truncate account number
			pub = pub.substring(0, 4);
		}
		this.pubCode = pub;
	}

	public void setClientIP(String ip) {
		if (ip != null && ip.length() > 30) {
			// truncate account number
			ip = ip.substring(0, 30);
		}
		this.clientIP = ip;
	}

	public String getClientIP() {
		return this.clientIP;
	}

	public DateTime getAccessedTime() {
		return this.accessedTime;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public long getID() {
		return this.id;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public void setTrialAccess(boolean trial) {
		this.trialAccess = trial;
	}

	public boolean isTrialAccess() {
		return this.trialAccess;
	}

}
