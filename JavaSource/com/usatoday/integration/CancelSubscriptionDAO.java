package com.usatoday.integration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.CancelSubscriptionIntf;

public class CancelSubscriptionDAO extends USATodayDAO {

	private static final String GET_CANCEL_SUBSCRIPTION_REASONS = "{call USAT_SUBS_CANCEL_REASONS(?)}";

	public Collection<CancelSubscriptionIntf> getCancelSubscriptionReasons(String pubCode) throws UsatException {

		java.sql.Connection conn = null;
		String cancelSubsReasonCode = "";
		String cancelSubsReasonDesc = "";
		Collection<CancelSubscriptionIntf> records = new ArrayList<CancelSubscriptionIntf>();

		try {
			conn = ConnectionManager.getInstance().getConnection();
			java.sql.CallableStatement statement = conn.prepareCall(CancelSubscriptionDAO.GET_CANCEL_SUBSCRIPTION_REASONS);
			statement.setString(1, pubCode);

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				cancelSubsReasonCode = rs.getString("CancelReasonCode");
				cancelSubsReasonDesc = rs.getString("CancelReasonDesc");
				CancelSubscriptionTO cancelSubsReasons = new CancelSubscriptionTO(cancelSubsReasonCode, cancelSubsReasonDesc);
				;
				records.add(cancelSubsReasons);
			}
		} catch (SQLException sqle) {
			System.out.println("CancelSubscriptionReasons : getCancelSubscriptionReasons() " + sqle.getMessage());

			throw new UsatException(sqle);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return records;

	}
}
