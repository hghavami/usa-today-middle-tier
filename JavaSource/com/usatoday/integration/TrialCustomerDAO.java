package com.usatoday.integration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.businessObjects.customer.LoginException;
import com.usatoday.util.constants.UsaTodayConstants;

public class TrialCustomerDAO extends USATodayDAO {

	// selection
	private static final String SELECT_TRIAL_CUSTOMER_DATA_BY_EMAIL = "{call USAT_ETRIAL_PULL_BY_EMAIL(?)}";
	private static final String SELECT_TRIAL_CUSTOMER_DATA_BY_EMAIL_LIKE = "{call USAT_ETRIAL_PULL_BY_EMAIL_LIKE(?)}";
	private static final String SELECT_TRIAL_CUSTOMER_DATA_BY_EMAIL_PWD = "{call USAT_ETRIAL_PULL_BY_EMAIL_PWD(?,?)}";

	// private static final String SELECT_INSTANCES_BY_CONFIRMATION_EMAIL_STATUS =
	// "{call USAT_ETRIAL_PULL_BY_EMAIL_CONFIRMATION_STATUS(?)}";
	// private static final String SELECT_INSTANCES_BY_CONFIRMATION_EMAIL_STATUS_FOR_PROCESSING =
	// "{call USAT_ETRIAL_PULL_BY_EMAIL_CONFIRMATION_STATUS_AND_SET_IN_PROGRESS()}";

	// update
	private static final String UPDATE_TRIAL_CUST = "update usat_eedition_trial_access set pubcode = ?, email = ?, password = ?, start_date = ?, end_date = ?, keycode = ?, first_name = ?, last_name = ?, company_name = ?, address1 = ?, address2 = ?, apt_suite = ?, additionalAddress = ?, city = ?, state = ?, zip = ?, country = ?, phone = ?, partner_id = ?, partner_number = ?, active = ?, subscribed = ?, rep_id = ?, confirmation_email_sent = ?, update_timestamp = ? where id = ?";

	// private static final String UPDATE_TRIAL_CUST_EMAIL_SENT =
	// "update usat_eedition_trial_access set confirmation_email_sent = ? where confirmation_email_sent = ?";

	private Collection<TrialCustomerTO> objectFactory(ResultSet rs) throws UsatException {
		ArrayList<TrialCustomerTO> customers = new ArrayList<TrialCustomerTO>();

		try {
			while (rs.next()) {
				TrialCustomerTO to = new TrialCustomerTO();

				long primaryKey = rs.getLong("id");

				to.setID(primaryKey);

				String tempStr = null;

				tempStr = rs.getString("pubcode");
				if (tempStr != null) {
					to.setPubCode(tempStr.trim());
				}

				tempStr = rs.getString("email");
				if (tempStr != null) {
					to.setEmail(tempStr.trim());
				}

				tempStr = rs.getString("password");
				if (tempStr != null) {
					to.setPassword(tempStr.trim());
				}

				java.sql.Timestamp ts = rs.getTimestamp("insert_timestamp");
				if (ts != null) {
					DateTime dt = new DateTime(ts.getTime());
					to.setInsertedDate(dt);
				}

				ts = rs.getTimestamp("start_date");
				if (ts != null) {
					DateTime dt = new DateTime(ts.getTime());
					to.setStartDate(dt);
				}

				ts = rs.getTimestamp("end_date");
				if (ts != null) {
					DateTime dt = new DateTime(ts.getTime());
					to.setEndDate(dt);
				}

				tempStr = rs.getString("keycode");
				if (tempStr != null) {
					to.setKeyCode(tempStr.trim());
				}

				tempStr = rs.getString("first_name");
				if (tempStr != null) {
					to.setFirstName(tempStr.trim());
				}

				tempStr = rs.getString("last_name");
				if (tempStr != null) {
					to.setLastName(tempStr.trim());
				}

				tempStr = rs.getString("company_name");
				if (tempStr != null) {
					to.setCompanyName(tempStr.trim());
				}

				tempStr = rs.getString("address1");
				if (tempStr != null) {
					to.setAddress1(tempStr.trim());
				}

				tempStr = rs.getString("address2");
				if (tempStr != null) {
					to.setAddress2(tempStr.trim());
				}

				tempStr = rs.getString("apt_suite");
				if (tempStr != null) {
					to.setApartmentSuite(tempStr.trim());
				}

				tempStr = rs.getString("additionalAddress");
				if (tempStr != null) {
					to.setAdditionalAddress(tempStr.trim());
				}

				tempStr = rs.getString("city");
				if (tempStr != null) {
					to.setCity(tempStr.trim());
				}

				tempStr = rs.getString("state");
				if (tempStr != null) {
					to.setState(tempStr.trim());
				}

				tempStr = rs.getString("zip");
				if (tempStr != null) {
					to.setZip(tempStr.trim());
				}

				tempStr = rs.getString("phone");
				if (tempStr != null) {
					to.setPhone(tempStr.trim());
				}

				tempStr = rs.getString("ip");
				if (tempStr != null) {
					to.setClientIP(tempStr.trim());
				}

				tempStr = rs.getString("partner_id");
				if (tempStr != null) {
					to.setPartnerID(tempStr.trim());
				}

				// partner number = club number in iSeries speak
				tempStr = rs.getString("partner_number");
				if (tempStr != null) {
					to.setClubNumber(tempStr.trim());
				}

				tempStr = rs.getString("rep_id");
				if (tempStr != null) {
					to.setNcsRepID(tempStr.trim());
				}

				tempStr = rs.getString("active");
				if (tempStr != null && "Y".equalsIgnoreCase(tempStr.trim())) {
					to.setIsActive(true);
				}

				tempStr = rs.getString("subscribed");
				if (tempStr != null && "Y".equalsIgnoreCase(tempStr.trim())) {
					to.setIsSubscribed(true);
				}

				tempStr = rs.getString("subscribed");
				if (tempStr != null && "Y".equalsIgnoreCase(tempStr.trim())) {
					to.setIsSubscribed(true);
				}

				tempStr = rs.getString("confirmation_email_sent");
				if (tempStr == null || "Y".equalsIgnoreCase(tempStr.trim())) {
					to.setConfirmationEmailSent("Y");
				} else {
					to.setConfirmationEmailSent("N");
				}

				customers.add(to);

			}
		} catch (SQLException sqle) {
			throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
		}
		return customers;
	}

	/**
	 * 
	 * @param emailAddress
	 * @return Collection of trial customer TO records matching request
	 * @throws UsatException
	 */
	public Collection<TrialCustomerTO> getTrialRecordsForEmailAddress(String emailAddress) throws UsatException {
		Collection<TrialCustomerTO> records = null;
		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement statement = conn.prepareCall(TrialCustomerDAO.SELECT_TRIAL_CUSTOMER_DATA_BY_EMAIL);

			statement.setString(1, emailAddress);

			ResultSet rs = statement.executeQuery();

			records = this.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("TrialCustomerDAO : getTrialRecordsForEmailAddress() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return records;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws UsatException
	 */
	public Collection<TrialCustomerTO> getTrialRecordsForEmailAddressPassword(String emailAddress, String password)
			throws UsatException {
		Collection<TrialCustomerTO> records = null;
		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement statement = conn.prepareCall(TrialCustomerDAO.SELECT_TRIAL_CUSTOMER_DATA_BY_EMAIL_PWD);

			statement.setString(1, emailAddress);

			statement.setString(2, password);

			ResultSet rs = statement.executeQuery();

			records = this.objectFactory(rs);

			statement.close();

			if (records.size() == 0) {
				this.cleanupConnection(conn);
				records = this.getTrialRecordsForEmailAddress(emailAddress);

				if (records.size() == 0) {
					throw new LoginException(LoginException.NO_ACCOUNT_SETUP);
				} else {
					throw new LoginException(LoginException.INVALID_LOGIN_ATTEMPT);
				}
			}
		} catch (LoginException le) {
			throw le;
		} catch (Exception e) {
			System.out.println("TrialCustomerDAO : getTrialRecordsForEmailAddressPassword() " + e.getMessage());
			throw new UsatException(e.getMessage());
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return records;
	}

	/**
	 * 
	 * @param emailAddress
	 * @return Collection of trial customer TO records matching request
	 * @throws UsatException
	 */
	public Collection<TrialCustomerTO> getTrialRecordsForEmailAddressLike(String emailAddress) throws UsatException {
		Collection<TrialCustomerTO> records = null;
		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement statement = conn.prepareCall(TrialCustomerDAO.SELECT_TRIAL_CUSTOMER_DATA_BY_EMAIL_LIKE);

			statement.setString(1, emailAddress);

			ResultSet rs = statement.executeQuery();

			records = this.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("TrialCustomerDAO : getTrialRecordsForEmailAddressLike() " + e.getMessage());
			throw new UsatException(e);
		} finally {
			if (conn != null) {
				this.cleanupConnection(conn);
			}
		}

		return records;
	}

	/**
	 * 
	 * @param customer
	 *            - updated with ID from db
	 * @return
	 * @throws UsatException
	 */
	public TrialCustomerTO insert(TrialCustomerTO customer) throws UsatException {
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.Statement statement = conn.createStatement();

			StringBuilder sqlInsert = new StringBuilder(
					"insert into usat_eedition_trial_access (pubcode, email, password, start_date, end_date, insert_timestamp, keycode, first_name, last_name, company_name, address1, address2, apt_suite, additionalAddress, city, state, zip, country, phone, partner_id, partner_number, active, ip, rep_id, confirmation_email_sent, update_timestamp) values (");
			// "insert into usat_eedition_trial_access (pubcode, email, password, start_date, end_date, insert_timestamp, keycode, first_name, last_name, company_name, address1, address2, apt_suite, additionalAddress, city, state, zip, country, phone, partner_id, partner_number, active, subscribed, rep_id) values "
			// +
			// "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
			// insert fields in order:
			// pubcode, email, password, start_date, end_date, insert_timestamp,
			// keycode, first_name, last_name, company_name, address1, address2,
			// apt_suite, additionalAddress, city, state, zip, country, phone,
			// partner_id, partner_number, active, rep_id, confirmation_email_sent, update_timestamp
			if (customer.getPubCode() != null) {
				if (customer.getPubCode().length() > 10) {
					throw new UsatException("PubCode exceeds max length");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getPubCode())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getEmail() != null) {
				if (customer.getEmail().length() > 60) {
					throw new UsatException("Email address is bigger than maximum of 60 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getEmail())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getPassword() != null) {
				if (customer.getPassword().length() > 10) {
					throw new UsatException("Password is bigger than maximum of 10 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getPassword())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getStartDate() != null) {
				Timestamp startStamp = new Timestamp(customer.getStartDate().getMillis());
				sqlInsert.append("'").append(startStamp).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getEndDate() != null) {
				Timestamp endStamp = new Timestamp(customer.getEndDate().getMillis());
				sqlInsert.append("'").append(endStamp).append("',");
			} else {
				sqlInsert.append("null,");
			}

			// insert timestamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			sqlInsert.append("'").append(ts).append("',");

			if (customer.getKeyCode() != null) {
				if (customer.getKeyCode().length() > 10) {
					throw new UsatException("Keycode is bigger than maximum of 10 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getKeyCode())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getFirstName() != null) {
				if (customer.getFirstName().length() > 10) {
					throw new UsatException("First Name is bigger than maximum of 10 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getFirstName())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getLastName() != null) {
				if (customer.getLastName().length() > 15) {
					throw new UsatException("Last Name is bigger than maximum of 15 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getLastName())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getCompanyName() != null) {
				if (customer.getCompanyName().length() > 128) {
					throw new UsatException("Company Name is bigger than maximum of 128 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getCompanyName())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getAddress1() != null) {
				if (customer.getAddress1().length() > 128) {
					throw new UsatException("Address1 is bigger than maximum of 128 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getAddress1())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getAddress2() != null) {
				if (customer.getAddress2().length() > 128) {
					throw new UsatException("Address2 is bigger than maximum of 128 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getAddress2())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getApartmentSuite() != null) {
				if (customer.getApartmentSuite().length() > 28) {
					throw new UsatException("Apartment/Suite Number is bigger than maximum of 28 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getApartmentSuite())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getAdditionalAddress() != null) {
				if (customer.getAdditionalAddress().length() > 128) {
					throw new UsatException("Additional Address is bigger than maximum of 128 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getAdditionalAddress())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getCity() != null) {
				if (customer.getCity().length() > 128) {
					throw new UsatException("City is bigger than maximum of 128 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getCity())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getState() != null) {
				if (customer.getState().length() > 30) {
					throw new UsatException("State is bigger than maximum of 30 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getState())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getZip() != null) {
				if (customer.getZip().length() > 10) {
					throw new UsatException("Zip Code is bigger than maximum of 10 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getZip())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			// don't support country yet so just set to null. Later replace with commented section.
			sqlInsert.append("null,");
			// if (customer.getCountry() != null) {
			// sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getCountry())).append("',");
			// }
			// else {
			// sqlInsert.append("null,");
			// }

			if (customer.getPhone() != null) {
				if (customer.getPhone().length() > 15) {
					throw new UsatException("Phone is bigger than maximum of 15 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getPhone())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getPartnerID() != null) {
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getPartnerID())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getClubNumber() != null) {
				if (customer.getClubNumber().length() > 128) {
					throw new UsatException("Club/partner number is bigger than maximum of 128 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getClubNumber())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			sqlInsert.append("'Y',");

			if (customer.getClientIP() != null) {
				if (customer.getClientIP().length() > 32) {
					throw new UsatException("IP Address is bigger than maximum of 32 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getClientIP())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getNcsRepID() != null) {
				if (customer.getNcsRepID().length() > 64) {
					throw new UsatException("RepID is bigger than maximum of 64 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getNcsRepID())).append("',");
			} else {
				sqlInsert.append("null,");
			}

			if (customer.getConfirmationEmailSent() != null) {
				if (customer.getConfirmationEmailSent().length() > 1) {
					throw new UsatException("confirmationEmailSentFlag is bigger than maximum of 1 characters");
				}
				sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getConfirmationEmailSent())).append("',");
			} else {
				sqlInsert.append("null,"); // assume we did
			}

			// update timestamp
			sqlInsert.append("'").append(ts).append("'");

			sqlInsert.append(")");

			// execute the SQL
			int rowsAffected = 0;

			if (UsaTodayConstants.debug) {
				System.out.println(sqlInsert.toString());
			}

			rowsAffected = statement.executeUpdate(sqlInsert.toString(), Statement.RETURN_GENERATED_KEYS);

			if (rowsAffected != 1) {
				throw new UsatException(
						"TrialCustomerDAO: No Records Inserted and yet no exception condition exists : Number rows affected: "
								+ rowsAffected);
			}

			ResultSet rs = statement.getGeneratedKeys();

			rs.next();
			long primaryKey = rs.getLong(1);

			// update customer TO
			customer.setID(primaryKey);
			customer.setInsertedDate(new DateTime(ts.getTime()));

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

		return customer;
	}

	/**
	 * 
	 * @param customer
	 * @return
	 * @throws UsatException
	 */
	public TrialCustomerTO update(TrialCustomerTO customer) throws UsatException {
		Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(TrialCustomerDAO.UPDATE_TRIAL_CUST);

			// pubcode = ?, email = ?, password = ?, start_date = ?, end_date = ?,

			if (customer.getPubCode() != null) {
				statement.setString(1, USATodayDAO.escapeApostrophes(customer.getPubCode()));
			} else {
				statement.setNull(1, Types.CHAR);
			}

			if (customer.getEmail() != null) {
				statement.setString(2, USATodayDAO.escapeApostrophes(customer.getEmail()));
			} else {
				statement.setNull(2, Types.CHAR);
			}

			if (customer.getPassword() != null) {
				statement.setString(3, USATodayDAO.escapeApostrophes(customer.getPassword()));
			} else {
				statement.setNull(3, Types.CHAR);
			}

			if (customer.getStartDate() != null) {
				Timestamp startStamp = new Timestamp(customer.getStartDate().getMillis());
				statement.setTimestamp(4, startStamp);
			} else {
				statement.setNull(4, Types.TIMESTAMP);
			}

			if (customer.getEndDate() != null) {
				Timestamp endStamp = new Timestamp(customer.getEndDate().getMillis());
				statement.setTimestamp(5, endStamp);
			} else {
				statement.setNull(5, Types.TIMESTAMP);
			}

			// keycode = ?, first_name = ?, last_name = ?, company_name = ?,
			if (customer.getKeyCode() != null) {
				statement.setString(6, USATodayDAO.escapeApostrophes(customer.getKeyCode()));
			} else {
				statement.setNull(6, Types.CHAR);
			}

			if (customer.getFirstName() != null) {
				statement.setString(7, USATodayDAO.escapeApostrophes(customer.getFirstName()));
			} else {
				statement.setNull(7, Types.CHAR);
			}

			if (customer.getLastName() != null) {
				statement.setString(8, USATodayDAO.escapeApostrophes(customer.getLastName()));
			} else {
				statement.setNull(8, Types.CHAR);
			}

			if (customer.getCompanyName() != null) {
				statement.setString(9, USATodayDAO.escapeApostrophes(customer.getCompanyName()));
			} else {
				statement.setNull(9, Types.CHAR);
			}

			// address1 = ?, address2 = ?, apt_suite = ?, additionalAddress = ?,
			if (customer.getAddress1() != null) {
				statement.setString(10, USATodayDAO.escapeApostrophes(customer.getAddress1()));
			} else {
				statement.setNull(10, Types.CHAR);
			}

			if (customer.getAddress2() != null) {
				statement.setString(11, USATodayDAO.escapeApostrophes(customer.getAddress2()));
			} else {
				statement.setNull(11, Types.CHAR);
			}

			if (customer.getApartmentSuite() != null) {
				statement.setString(12, USATodayDAO.escapeApostrophes(customer.getApartmentSuite()));
			} else {
				statement.setNull(12, Types.CHAR);
			}

			if (customer.getAdditionalAddress() != null) {
				statement.setString(13, USATodayDAO.escapeApostrophes(customer.getAdditionalAddress()));
			} else {
				statement.setNull(13, Types.CHAR);
			}

			// city = ?, state = ?, zip = ?, country = ?, phone = ?, partner_id = ?,
			if (customer.getCity() != null) {
				statement.setString(14, USATodayDAO.escapeApostrophes(customer.getCity()));
			} else {
				statement.setNull(14, Types.CHAR);
			}

			if (customer.getState() != null) {
				statement.setString(15, USATodayDAO.escapeApostrophes(customer.getState()));
			} else {
				statement.setNull(15, Types.CHAR);
			}

			if (customer.getZip() != null) {
				statement.setString(16, USATodayDAO.escapeApostrophes(customer.getZip()));
			} else {
				statement.setNull(16, Types.CHAR);
			}

			// don't support country yet so just set to null. Later replace with commented section.
			statement.setNull(17, Types.CHAR);
			// if (customer.getCountry() != null) {
			// sqlInsert.append("'").append(USATodayDAO.escapeApostrophes(customer.getCountry())).append("',");
			// }
			// else {
			// sqlInsert.append("null,");
			// }

			if (customer.getPhone() != null) {
				statement.setString(18, USATodayDAO.escapeApostrophes(customer.getPhone()));
			} else {
				statement.setNull(18, Types.CHAR);
			}

			if (customer.getPartnerID() != null) {
				statement.setString(19, USATodayDAO.escapeApostrophes(customer.getPartnerID()));
			} else {
				statement.setNull(19, Types.CHAR);
			}

			// partner_number = ?, active = ?, subscribed = ?, rep_id = ?) where id = ?
			if (customer.getClubNumber() != null) {
				statement.setString(20, USATodayDAO.escapeApostrophes(customer.getClubNumber()));
			} else {
				statement.setNull(20, Types.CHAR);
			}

			if (customer.getIsActive()) {
				statement.setString(21, "Y");
			} else {
				statement.setString(21, "N");
			}

			if (customer.getIsSubscribed()) {
				statement.setString(22, "Y");
			} else {
				statement.setString(22, "N");
			}

			if (customer.getNcsRepID() != null) {
				statement.setString(23, customer.getNcsRepID());
			} else {
				statement.setNull(23, Types.CHAR);
			}

			if (customer.getConfirmationEmailSent() != null) {
				statement.setString(24, customer.getConfirmationEmailSent());
			} else {
				statement.setNull(24, Types.CHAR);
			}

			// update timestamp
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			statement.setTimestamp(25, ts);

			statement.setLong(26, customer.getID());

			// execute the SQL
			int rowsAffected = 0;

			rowsAffected = statement.executeUpdate();

			if (rowsAffected != 1) {
				throw new UsatException(
						"TrialCustomerDAO: No Records update and yet no exception condition exists : Number rows affected: "
								+ rowsAffected);
			}

			statement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		} finally {
			this.cleanupConnection(conn);
		}

		return customer;
	}

	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	/*
	 * public Collection<TrialCustomerTO> getTrialsThatNeedConfirmationEmailSentAndUpdateStatusToInProgress() throws UsatException {
	 * Collection<TrialCustomerTO> records = null; java.sql.Connection conn = null; try {
	 * 
	 * conn = ConnectionManager.getInstance().getConnection();
	 * 
	 * java.sql.CallableStatement statement =
	 * conn.prepareCall(TrialCustomerDAO.SELECT_INSTANCES_BY_CONFIRMATION_EMAIL_STATUS_FOR_PROCESSING);
	 * 
	 * ResultSet rs = statement.executeQuery();
	 * 
	 * records = this.objectFactory(rs);
	 * 
	 * 
	 * } catch (Exception e) {
	 * System.out.println("TrialCustomerDAO : getTrialsThatNeedConfirmationEmailSentAndUpdateStatusToInProgress() " +
	 * e.getMessage()); throw new UsatException(e); } finally { if (conn != null){ this.cleanupConnection(conn); } }
	 * 
	 * return records; }
	 */
	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	/*
	 * public Collection<TrialCustomerTO> getTrialsThatNeedConfirmationEmailSent() throws UsatException {
	 * Collection<TrialCustomerTO> records = null; java.sql.Connection conn = null; try {
	 * 
	 * conn = ConnectionManager.getInstance().getConnection();
	 * 
	 * java.sql.CallableStatement statement = conn.prepareCall(TrialCustomerDAO.SELECT_INSTANCES_BY_CONFIRMATION_EMAIL_STATUS);
	 * 
	 * statement.setString(1, "N");
	 * 
	 * ResultSet rs = statement.executeQuery();
	 * 
	 * records = this.objectFactory(rs);
	 * 
	 * } catch (Exception e) { System.out.println("TrialCustomerDAO : getTrialsThatNeedConfirmationEmailSent() " + e.getMessage());
	 * throw new UsatException(e); } finally { if (conn != null){ this.cleanupConnection(conn); } }
	 * 
	 * return records; }
	 */
}
