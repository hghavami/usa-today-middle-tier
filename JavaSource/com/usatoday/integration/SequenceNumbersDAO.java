/*
 * Created on May 3, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.sql.Date;
import java.sql.ResultSet;

/**
 * @author aeast
 * @date May 3, 2006
 * @class SequenceNumbersDAO
 * 
 *        This class works with the order id sequence
 * 
 */
public class SequenceNumbersDAO extends USATodayDAO {

	private static String selectSequenceStartStr = "select sequence_start from XTRNT_SEQUENCE where seq_id = 'LinkShare'";
	private static String getNextSequnceStr = "{call USAT_GET_NEXT_LINKSHARE_SEQUENCE_NUMBER}";
	private static String resetSequenceStr = "update XTRNT_SEQUENCE set current_value = 0, sequence_start = ? where seq_id = 'LinkShare'";

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public Date getCurrentSequenceDate() throws Exception {
		java.sql.Connection conn = null;

		Date date = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			// get sequence time
			java.sql.PreparedStatement statement = conn.prepareStatement(SequenceNumbersDAO.selectSequenceStartStr);

			ResultSet rs = statement.executeQuery();

			if (rs.next()) {
				date = rs.getDate(1);

				rs.close();
			} else {
				throw new Exception("No Sequence Found in Database");
			}

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new Exception(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw e;
		} finally {
			this.cleanupConnection(conn);
		}
		return date;
	}

	/**
	 * 
	 * @param time
	 * @throws Exception
	 */
	public void resetSequence(long time) throws Exception {
		java.sql.Connection conn = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			// reset sequence
			java.sql.PreparedStatement resetStatement = conn.prepareStatement(SequenceNumbersDAO.resetSequenceStr);

			resetStatement.setDate(1, new java.sql.Date(time));

			resetStatement.executeUpdate();

			resetStatement.close();

		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new Exception(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw e;
		} finally {
			this.cleanupConnection(conn);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getNextSequenceNumber() throws Exception {
		java.sql.Connection conn = null;
		int nextSequenceNumber = -1;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.CallableStatement cs = conn.prepareCall(SequenceNumbersDAO.getNextSequnceStr);
			java.sql.ResultSet nextValueRS = cs.executeQuery();

			nextValueRS.next();
			nextSequenceNumber = nextValueRS.getInt(1);

			nextValueRS.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new Exception(e.getClass().getName() + " : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw e;
		} finally {
			this.cleanupConnection(conn);
		}
		return nextSequenceNumber;
	}

}
