/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.products.PersistentProductIntf;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.suppliers.SupplierIntf;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class ProductTO
 * 
 */
public class ProductTO implements PersistentProductIntf, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4213386301117411582L;
	private String productCode = null;
	private String name = null;
	private String description = null;
	private String brandingPubCode = "UT";
	private String detailedDescription = null;
	private DateTime startDate = null;
	private DateTime endDate = null;
	private int minQuantityPerOrder = 1;
	private int maxQuantityPerOrder = 0;
	private int id = -1;
	private int initialInventory = 0;
	private int inventory = 0;
	private String taxRateCode = null;
	private double unitPrice = 0.0;
	private DateTime insertTimestamp = null;
	private DateTime updateTimestamp = null;
	private boolean taxable = false;
	private int fulfillmentMethod = -1;
	private int supplierID = -1;
	private String customerServicePhone = "1-800-872-0001";
	private String defaultKeycode = null;
	private String expiredOfferKeycode = null;
	private String defaultRenewalKeycode = null;
	private int maxDaysInFutureBeforeFulfillment = 90;

	private int holdDaysDelay = -1;

	private int productType = 0;
	public String getBrandingPubCode() {
		return this.brandingPubCode;
	}
	public String getCustomerServicePhone() {
		return customerServicePhone;
	}

	public String getDefaultKeycode() {
		return defaultKeycode;
	}

	public String getDefaultRenewalKeycode() {
		return defaultRenewalKeycode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#getDescription()
	 */
	public String getDescription() {
		return this.description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#getDetailedDescription()
	 */
	public String getDetailedDescription() {
		return this.detailedDescription;
	}

	public DateTime getEndDate() {
		return this.endDate;
	}

	public String getExpiredOfferKeycode() {
		return expiredOfferKeycode;
	}

	public int getFulfillmentMethod() {
		return this.fulfillmentMethod;
	}

	public int getHoldDaysDelay() {
		return holdDaysDelay;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#getID()
	 */
	public int getID() {
		return this.id;
	}

	public int getInitialInventory() {
		return this.initialInventory;
	}

	public DateTime getInsertTimestamp() {
		return this.insertTimestamp;
	}

	public int getInventory() {
		return this.inventory;
	}

	public int getMaxDaysInFutureBeforeFulfillment() {
		return this.maxDaysInFutureBeforeFulfillment;
	}

	public int getMaxQuantityPerOrder() {
		return this.maxQuantityPerOrder;
	}

	public int getMinQuantityPerOrder() {
		return this.minQuantityPerOrder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#getName()
	 */
	public String getName() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#getProductCode()
	 */
	public String getProductCode() {
		return this.productCode;
	}

	@Override
	public int getProductType() {
		return this.productType;
	}

	public DateTime getStartDate() {
		return this.startDate;
	}

	public SupplierIntf getSupplier() {
		// Not implemented in TO
		return null;
	}

	public int getSupplierID() {
		return this.supplierID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#getTaxRateCode()
	 */
	public String getTaxRateCode() {
		return this.taxRateCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#getUnitPrice()
	 */
	public double getUnitPrice() {
		return this.unitPrice;
	}

	public DateTime getUpdateTimestamp() {
		return this.updateTimestamp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#isAvailable()
	 */
	public boolean isAvailable() {
		return true;
	}

	public boolean isElectronicDelivery() {
		if (this.fulfillmentMethod == ProductIntf.ELECTRONIC_DELIVERY) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#isTaxable()
	 */
	public boolean isTaxable() {
		return this.taxable;
	}

	public void setBrandingPubCode(String pubCode) {
		this.brandingPubCode = pubCode;
	}

	public void setCustomerServicePhone(String customerServicePhone) {
		this.customerServicePhone = customerServicePhone;
	}

	public void setDefaultKeycode(String defaultKeycode) {
		this.defaultKeycode = defaultKeycode;
	}

	public void setDefaultRenewalKeycode(String defaultRenewalKeycode) {
		this.defaultRenewalKeycode = defaultRenewalKeycode;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDetailedDescription(String detailedDescription) {
		this.detailedDescription = detailedDescription;
	}

	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	public void setExpiredOfferKeycode(String expiredOfferKeycode) {
		this.expiredOfferKeycode = expiredOfferKeycode;
	}

	public void setFulfillmentMethod(int method) {
		this.fulfillmentMethod = method;
	}

	public void setHoldDaysDelay(int days) {
		this.holdDaysDelay = days;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setInitialInventory(int initialInventory) {
		this.initialInventory = initialInventory;
	}

	public void setInsertTimestamp(DateTime insertTimestamp) {
		this.insertTimestamp = insertTimestamp;
	}

	public void setInventory(int inventory) {
		this.inventory = inventory;
	}

	public void setMaxDaysInFutureBeforeFulfillment(int days) {
		this.maxDaysInFutureBeforeFulfillment = days;
	}

	public void setMaxQuantityPerOrder(int maxQuantityPerOrder) {
		this.maxQuantityPerOrder = maxQuantityPerOrder;
	}

	public void setMinQuantityPerOrder(int minQuantityPerOrder) {
		this.minQuantityPerOrder = minQuantityPerOrder;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public void setProductType(int type) {
		this.productType = type;
	}

	public void setStartDate(DateTime startDate) {
		this.startDate = startDate;
	}

	public void setSupplierID(int id) {
		this.supplierID = id;
	}

	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}

	public void setTaxRateCode(String taxRateCode) {
		this.taxRateCode = taxRateCode;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public void setUpdateTimestamp(DateTime updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#updateInventory(int)
	 */
	public int updateInventory(int delta) {
		return 0;
	}
}
