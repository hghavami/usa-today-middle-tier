package com.usatoday.integration;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.gannett.usat.userserviceapi.client.AutoLogin;
import com.gannett.usat.userserviceapi.domainbeans.autoLogin.AutoLoginResponse;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.PersistentSubscriberAccountIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.businessObjects.customer.CustomerBO;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.customer.LoginException;
import com.usatoday.businessObjects.customer.SubscriberAccountBO;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;
import com.usatoday.util.constants.UsaTodayConstants;

public class CustomerDAO extends USATodayDAO {

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws UsatException
	 */
	public CustomerBO getCustomerByEmailAndPassword(String emailAddress, String password) throws UsatException {
		// return this.getCustomerByEmailAndPasswordAndPub(emailAddress, password, null);
		return this.getGenesysCustomerByEmailAndPasswordAndPub(emailAddress, password, null);
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	public CustomerBO getGenesysCustomerByEmailAndPasswordAndPub(String emailAddress, String password, String pubCode)
			throws UsatException {

		EmailRecordDAO emailDAO = new EmailRecordDAO();
		SubscriberAccountDAO saDAO = new SubscriberAccountDAO();

		HashMap<String, EmailRecordIntf> emailRecordMap = new HashMap<String, EmailRecordIntf>();
		HashMap<String, SubscriberAccountIntf> accountMap = new HashMap<String, SubscriberAccountIntf>();

		Collection<EmailRecordIntf> emailRecords = null;
		// emailRecords = emailDAO.getEmailRecordsForEmailAddressAndPasswordAndPub(emailAddress, password, pubCode);
		emailRecords = emailDAO.getLoginEmailRecordsForEmailAddress(emailAddress, password);
		// login failed if no records returned.
		if (emailRecords == null || emailRecords.size() == 0) {
			emailRecords = emailDAO.getEmailRecordsForEmailAddress(emailAddress);
			LoginException le = null;
			if (emailRecords == null || emailRecords.size() == 0) {
				le = new LoginException(LoginException.NO_ACCOUNT_SETUP);
			} else {
				le = new LoginException(LoginException.INVALID_LOGIN_ATTEMPT);
			}
			throw le;
		} else {
			EmailRecordIntf email = null;
			Iterator<EmailRecordIntf> eItr = emailRecords.iterator();
			while (eItr.hasNext()) {
				email = eItr.next();
				if (email.getFireflyUserId().trim().equals("303")) {
					LoginException le = null;
					le = new LoginException(LoginException.RESET_PASSWORD_REQUIRED);
					throw le;
				}
			}
		}

		// Collection<PersistentSubscriberAccountIntf> subscriberAccounts = saDAO.getAccountForCustomer(emailAddress, password);
		Collection<PersistentSubscriberAccountIntf> subscriberAccounts = saDAO.getGenesysAccountForCustomer(emailAddress);

		Iterator<PersistentSubscriberAccountIntf> aItr = subscriberAccounts.iterator();
		while (aItr.hasNext()) {
			PersistentSubscriberAccountIntf sa = aItr.next();

			EmailRecordIntf email = null;
			Iterator<EmailRecordIntf> eItr = emailRecords.iterator();
			while (eItr.hasNext()) {
				email = eItr.next();
				email.setAccountNumber(sa.getAccountNumber());
				email.setPubCode(sa.getPubCode());
				break;
			}
			// convert to EmailRecordBO
			EmailRecordBO eBO = new EmailRecordBO(email);
			emailRecordMap.put(email.getAccountNumber(), eBO);

			SubscriberAccountBO saBO = new SubscriberAccountBO(sa);
			accountMap.put(sa.getAccountNumber(), saBO);
		}
		// end new

		// If no email records remain then the user's account does not exist in Genesys
		if (accountMap.size() == 0) {
			// Check if account is pending
			int newAccountsCount = 0;
			Iterator<EmailRecordIntf> eIterator = emailRecords.iterator();
			while (eIterator.hasNext()) {
				EmailRecordIntf emailRec = eIterator.next();

				if (emailRec.getPermStartDate() != null && emailRec.getPermStartDate().length() == 8
						&& !emailRec.getPermStartDate().equalsIgnoreCase("00000000")) {
					DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(emailRec.getPermStartDate());

					// only add eEditions without an account number for up to X
					// days.
					if (dt.plusDays(UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS).isAfterNow()) {
						newAccountsCount++;
						StringBuilder tempAcctNum = new StringBuilder("Pending (");
						tempAcctNum.append(newAccountsCount).append(")");
						EmailRecordBO eBO = new EmailRecordBO(emailRec);
						emailRecordMap.put(tempAcctNum.toString(), eBO);
						// Expired account
					} else {
						throw new LoginException(LoginException.EMAIL_RECORDS_BUT_NO_ACCOUNTS);
					}
				}
				break;
			}
			// no corresponding accounts for email address
			throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP);
		}

		CustomerBO customer = null;
		if (accountMap.size() > 0) {
			customer = new CustomerBO(accountMap, emailRecordMap);
		}

		return customer;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	public CustomerBO getCustomerByEmailAndPasswordAndPub(String emailAddress, String password, String pubCode)
			throws UsatException {

		EmailRecordDAO emailDAO = new EmailRecordDAO();
		SubscriberAccountDAO saDAO = new SubscriberAccountDAO();

		HashMap<String, EmailRecordIntf> emailRecordMap = new HashMap<String, EmailRecordIntf>();
		HashMap<String, SubscriberAccountIntf> accountMap = new HashMap<String, SubscriberAccountIntf>();

		Collection<EmailRecordIntf> emailRecords = null;
		emailRecords = emailDAO.getEmailRecordsForEmailAddressAndPasswordAndPub(emailAddress, password, pubCode);

		// login failed if no records returned.
		if (emailRecords == null || emailRecords.size() == 0) {
			emailRecords = emailDAO.getEmailRecordsForEmailAddress(emailAddress);
			LoginException le = null;
			if (emailRecords.size() == 0) {
				le = new LoginException(LoginException.NO_ACCOUNT_SETUP);
			} else {
				le = new LoginException(LoginException.INVALID_LOGIN_ATTEMPT);
			}
			throw le;
		}

		boolean hasExpiredAccounts = false;
		boolean hasExpiredNewStarts = false;
		Iterator<EmailRecordIntf> eIterator = emailRecords.iterator();
		if (emailRecords.size() == 1) { // if single account association then
										// use optimized query
			while (eIterator.hasNext()) {
				EmailRecordIntf e = eIterator.next();
				// skip new accounts that have not been completely set up
				if ((e.getAccountNumber().trim().length() == 0) || e.getAccountNumber().equalsIgnoreCase("0000000")) {
					// if no account number assigned yet but less than 3 days
					// old..let them sign in.
					if (e.getPermStartDate() != null && e.getPermStartDate().length() == 8
							&& !e.getPermStartDate().equalsIgnoreCase("00000000")) {
						DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(e.getPermStartDate());

						// only add eEditions without an account number for up
						// to X days after purchase.
						if (dt.plusDays(UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS).isAfterNow()) {
							EmailRecordBO eBO = new EmailRecordBO(e);
							emailRecordMap.put(e.getAccountNumber(), eBO);
						} else {
							hasExpiredNewStarts = true;
						}
					}
					continue;
				}

				Collection<PersistentSubscriberAccountIntf> accounts = saDAO.getAccountByAccountNumberAndPub(e.getAccountNumber(),
						e.getPubCode());
				PersistentSubscriberAccountIntf acct = null;
				if (accounts.size() == 1) {
					acct = (PersistentSubscriberAccountIntf) accounts.toArray()[0];
				}

				if (acct != null) {
					EmailRecordBO eBO = new EmailRecordBO(e);
					emailRecordMap.put(e.getAccountNumber(), eBO);

					SubscriberAccountBO saBO = new SubscriberAccountBO(acct);
					accountMap.put(e.getAccountNumber(), saBO);
				} else {
					if (e.getPermStartDate() != null && e.getPermStartDate().length() == 8
							&& !e.getPermStartDate().equalsIgnoreCase("00000000")) {
						DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(e.getPermStartDate());

						// only add eEditions without an account number for up
						// to X days after purchase.
						if (dt.plusDays(UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS).isAfterNow()) {
							EmailRecordBO eBO = new EmailRecordBO(e);
							emailRecordMap.put(e.getAccountNumber(), eBO);
						} else {
							if (e.getAccountNumber().length() == 9 && !e.getAccountNumber().equalsIgnoreCase("0000000")) {
								// expird accounts.
								hasExpiredAccounts = true;
							} else {
								hasExpiredNewStarts = true;
							}
						}
					}
				}

			}
		} // end if single account
		else { // multiple accounts
				// process any accounts that have not been assigned an account
				// number yet.
			int newAccountsCount = 0;
			for (EmailRecordIntf emailRec : emailRecords) {
				if (emailRec.getAccountNumber().trim().length() == 0 || emailRec.getAccountNumber().equalsIgnoreCase("0000000")) {
					// account number not set yet
					if (emailRec.getPermStartDate() != null && emailRec.getPermStartDate().length() == 8
							&& !emailRec.getPermStartDate().equalsIgnoreCase("00000000")) {
						DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(emailRec.getPermStartDate());

						// only add eEditions without an account number for up
						// to X days.
						if (dt.plusDays(UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS).isAfterNow()) {
							newAccountsCount++;
							StringBuilder tempAcctNum = new StringBuilder("Pending (");
							tempAcctNum.append(newAccountsCount).append(")");

							EmailRecordBO eBO = new EmailRecordBO(emailRec);

							emailRecordMap.put(tempAcctNum.toString(), eBO);
						}
					}
					continue;
				}
			}

			// Collection<PersistentSubscriberAccountIntf> subscriberAccounts =
			// saDAO.getAccountForCustomer(emailAddress, password);
			Collection<PersistentSubscriberAccountIntf> subscriberAccounts = saDAO.getGenesysAccountForCustomer(emailAddress);

			if ((subscriberAccounts.size() + newAccountsCount) != emailRecords.size()) {
				hasExpiredAccounts = true;
			}

			Iterator<PersistentSubscriberAccountIntf> aItr = subscriberAccounts.iterator();
			while (aItr.hasNext()) {
				PersistentSubscriberAccountIntf sa = aItr.next();

				EmailRecordIntf email = null;
				Iterator<EmailRecordIntf> eItr = emailRecords.iterator();
				while (eItr.hasNext()) {
					email = eItr.next();
					break;
				}
				EmailRecordBO eBO = new EmailRecordBO(email);
				emailRecordMap.put(sa.getAccountNumber(), eBO);

				SubscriberAccountBO saBO = new SubscriberAccountBO(sa);
				accountMap.put(sa.getAccountNumber(), saBO);
			}
		} // end else multiples
			// end new

		// If not email records remain then the user's account does not exist in
		// XTRNTUPLD
		if (emailRecordMap.size() == 0) {
			if (hasExpiredAccounts) {
				throw new LoginException(LoginException.EMAIL_RECORDS_BUT_NO_ACCOUNTS);
			} else if (hasExpiredNewStarts) {
				throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP_EDITION_ACCESS_EXPIRED);
			} else {
				// no corresponding accounts for email address
				throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP);
			}
		}

		CustomerBO customer = null;
		if (emailRecordMap.size() > 0) {
			customer = new CustomerBO(accountMap, emailRecordMap);
		}

		return customer;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	public CustomerBO getGenesysCustomerByEmailAndAccountAndPub(String emailAddress, String account, String pubCode)
			throws UsatException {

		EmailRecordDAO emailDAO = new EmailRecordDAO();
		SubscriberAccountDAO saDAO = new SubscriberAccountDAO();

		HashMap<String, EmailRecordIntf> emailRecordMap = new HashMap<String, EmailRecordIntf>();
		HashMap<String, SubscriberAccountIntf> accountMap = new HashMap<String, SubscriberAccountIntf>();

		Collection<EmailRecordIntf> emailRecords = null;
		emailRecords = emailDAO.getEmailRecordsForEmailAddress(emailAddress);

		// login failed if no records returned.
		if (emailRecords == null || emailRecords.size() == 0) {
			emailRecords = emailDAO.getEmailRecordsForEmailAddress(emailAddress);
			LoginException le = null;
			if (emailRecords.size() == 0) {
				le = new LoginException(LoginException.NO_ACCOUNT_SETUP);
			} else {
				le = new LoginException(LoginException.INVALID_LOGIN_ATTEMPT);
			}
			throw le;
		}

		Collection<PersistentSubscriberAccountIntf> subscriberAccounts = saDAO.getGenesysAccountByAccountNumber(account, pubCode);
		Iterator<PersistentSubscriberAccountIntf> aItr = subscriberAccounts.iterator();
		while (aItr.hasNext()) {
			PersistentSubscriberAccountIntf sa = (PersistentSubscriberAccountIntf) aItr.next();

			EmailRecordIntf email = null;
			Iterator<EmailRecordIntf> eItr = emailRecords.iterator();
			while (eItr.hasNext()) {
				email = eItr.next();
				email.setAccountNumber(sa.getAccountNumber());
				email.setPubCode(sa.getPubCode());
				break;
			}
			// convert to EmailRecordBO
			EmailRecordBO eBO = new EmailRecordBO(email);
			emailRecordMap.put(email.getAccountNumber(), eBO);

			SubscriberAccountBO saBO = new SubscriberAccountBO(sa);
			accountMap.put(sa.getAccountNumber(), saBO);
		}
		// end new

		// If not email records remain then the user's account does not exist in
		// XTRNTUPLD
		if (emailRecordMap.size() == 0) {
			// no corresponding accounts for email address
			throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP);
		}

		CustomerBO customer = null;
		if (emailRecordMap.size() > 0) {
			customer = new CustomerBO(accountMap, emailRecordMap);
		}

		return customer;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	public CustomerBO getCustomerByEmailAndAccountAndPub(String emailAddress, String account, String pubCode) throws UsatException {

		EmailRecordDAO emailDAO = new EmailRecordDAO();
		SubscriberAccountDAO saDAO = new SubscriberAccountDAO();

		HashMap<String, EmailRecordIntf> emailRecordMap = new HashMap<String, EmailRecordIntf>();
		HashMap<String, SubscriberAccountIntf> accountMap = new HashMap<String, SubscriberAccountIntf>();

		Collection<EmailRecordIntf> emailRecords = null;
		emailRecords = emailDAO.getEmailRecordsForPubAndEmailAddressAndAccount(pubCode, emailAddress, account);

		// login failed if no records returned.
		if (emailRecords == null || emailRecords.size() == 0) {
			emailRecords = emailDAO.getEmailRecordsForEmailAddress(emailAddress);
			LoginException le = null;
			if (emailRecords.size() == 0) {
				le = new LoginException(LoginException.NO_ACCOUNT_SETUP);
			} else {
				le = new LoginException(LoginException.INVALID_LOGIN_ATTEMPT);
			}
			throw le;
		}

		Iterator<EmailRecordIntf> eIterator = emailRecords.iterator();
		if (emailRecords.size() == 1) { // if single account association then
										// use optimized query
			while (eIterator.hasNext()) {
				EmailRecordIntf e = eIterator.next();
				// skip new accounts that have not been completely set up
				if (e.getAccountNumber().equalsIgnoreCase("0000000") || e.getAccountNumber().trim().length() == 0) {
					continue;
				}
				Collection<PersistentSubscriberAccountIntf> accounts = saDAO.getAccountByAccountNumberAndPub(e.getAccountNumber(),
						e.getPubCode());
				PersistentSubscriberAccountIntf acct = null;
				if (accounts.size() == 1) {
					acct = (PersistentSubscriberAccountIntf) accounts.toArray()[0];
				}

				if (acct != null) {
					EmailRecordBO eBO = new EmailRecordBO(e);
					emailRecordMap.put(e.getAccountNumber(), eBO);

					SubscriberAccountBO saBO = new SubscriberAccountBO(acct);
					accountMap.put(e.getAccountNumber(), saBO);
				}
			}
		} // end if single account
		else { // multiple accounts
			Collection<PersistentSubscriberAccountIntf> subscriberAccounts = saDAO
					.getAccountByAccountNumberAndPub(account, pubCode);
			Iterator<PersistentSubscriberAccountIntf> aItr = subscriberAccounts.iterator();
			while (aItr.hasNext()) {
				PersistentSubscriberAccountIntf sa = (PersistentSubscriberAccountIntf) aItr.next();

				EmailRecordIntf email = null;
				Iterator<EmailRecordIntf> eItr = emailRecords.iterator();
				while (eItr.hasNext()) {
					email = eItr.next();
					if (email.getAccountNumber().equalsIgnoreCase(sa.getAccountNumber())) {
						break;
					} else {
						email = null;
					}
				}
				if (email != null) {
					// convert to EmailRecordBO
					// TBD
					EmailRecordBO eBO = new EmailRecordBO(email);
					emailRecordMap.put(email.getAccountNumber(), eBO);

					SubscriberAccountBO saBO = new SubscriberAccountBO(sa);
					accountMap.put(email.getAccountNumber(), saBO);
				}
			}
		} // end else multiples
			// end new

		// If not email records remain then the user's account does not exist in
		// XTRNTUPLD
		if (emailRecordMap.size() == 0) {
			// no corresponding accounts for email address
			throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP);
		}

		CustomerBO customer = null;
		if (emailRecordMap.size() > 0) {
			customer = new CustomerBO(accountMap, emailRecordMap);
		}

		return customer;
	}

	/**
	 * 
	 * @param identifier
	 * @return
	 * @throws UsatException
	 */
	public CustomerBO getGenesysCustomerByUniqueIdentifier(String identifier) throws UsatException {

		EmailRecordDAO emailDAO = new EmailRecordDAO();
		SubscriberAccountDAO saDAO = new SubscriberAccountDAO();

		HashMap<String, EmailRecordIntf> emailRecordMap = new HashMap<String, EmailRecordIntf>();
		HashMap<String, SubscriberAccountIntf> accountMap = new HashMap<String, SubscriberAccountIntf>();

		int id = -1;
		try {
			id = Integer.parseInt(identifier);
		} catch (Exception e) {
			throw new UsatException("CustomerBO::getCustomByUniqueIdentifier() Invalid Identifier: " + identifier);
		}

		String accountNumber = null;

		Collection<EmailRecordIntf> emailRecords = null;
		String emailAddress = null;
		emailAddress = emailDAO.getEmailAddressForSerialNumber(id);

		// login failed if no records returned.
		if (emailAddress == null) {
			LoginException le = new LoginException(LoginException.INVALID_LOGIN_ATTEMPT);
			throw le;
		}

		emailRecords = emailDAO.getEmailRecordsForEmailAddress(emailAddress);
		PersistentSubscriberAccountIntf sa = null;

		Collection<PersistentSubscriberAccountIntf> subscriberAccounts = saDAO.getGenesysAccountForCustomer(emailAddress);
		Iterator<PersistentSubscriberAccountIntf> aItr = subscriberAccounts.iterator();
		while (aItr.hasNext()) {
			sa = aItr.next();

			EmailRecordIntf email = null;
			Iterator<EmailRecordIntf> eItr = emailRecords.iterator();
			while (eItr.hasNext()) {
				email = eItr.next();
				email.setAccountNumber(sa.getAccountNumber());
				email.setPubCode(sa.getPubCode());
				break;
			}
			// convert to EmailRecordBO
			EmailRecordBO eBO = new EmailRecordBO(email);
			emailRecordMap.put(email.getAccountNumber(), eBO);

			SubscriberAccountBO saBO = new SubscriberAccountBO(sa);
			accountMap.put(sa.getAccountNumber(), saBO);
			// save the account number of associated with the id used for setting current account.
			accountNumber = sa.getAccountNumber();
		}

		// If not email records remain then the user's account does not exist in
		// XTRNTUPLD
		if (emailRecordMap.size() == 0) {
			// no corresponding accounts for email address
			throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP);
		}

		CustomerBO customer = null;
		if (emailRecordMap.size() > 0) {
			customer = new CustomerBO(accountMap, emailRecordMap);
			if (accountNumber != null) {
				customer.setCurrentAccount(accountNumber);
			}
		}

		return customer;
	}

	/**
	 * 
	 * @param identifier
	 * @return
	 * @throws UsatException
	 */
	public CustomerBO getCustomerByUniqueIdentifier(String identifier) throws UsatException {

		EmailRecordDAO emailDAO = new EmailRecordDAO();
		SubscriberAccountDAO saDAO = new SubscriberAccountDAO();

		HashMap<String, EmailRecordIntf> emailRecordMap = new HashMap<String, EmailRecordIntf>();
		HashMap<String, SubscriberAccountIntf> accountMap = new HashMap<String, SubscriberAccountIntf>();

		int id = -1;
		try {
			id = Integer.parseInt(identifier);
		} catch (Exception e) {
			throw new UsatException("CustomerBO::getCustomByUniqueIdentifier() Invalid Identifier: " + identifier);
		}

		String accountNumber = null;

		Collection<EmailRecordIntf> emailRecords = null;
		String emailAddress = null;
		emailAddress = emailDAO.getEmailAddressForSerialNumber(id);

		// login failed if no records returned.
		if (emailAddress == null) {
			LoginException le = new LoginException(LoginException.INVALID_LOGIN_ATTEMPT);
			throw le;
		}

		emailRecords = emailDAO.getEmailRecordsForEmailAddress(emailAddress);

		Iterator<EmailRecordIntf> eIterator = emailRecords.iterator();
		while (eIterator.hasNext()) {
			EmailRecordIntf e = eIterator.next();

			if (e.getAccountNumber().equalsIgnoreCase("0000000") || e.getAccountNumber().trim().length() == 0) {
				continue;
			}

			Collection<PersistentSubscriberAccountIntf> accounts = saDAO.getAccountByAccountNumberAndPub(e.getAccountNumber(),
					e.getPubCode());
			PersistentSubscriberAccountIntf acct = null;
			if (accounts.size() == 1) {
				acct = (PersistentSubscriberAccountIntf) accounts.toArray()[0];
			}

			// if matching account found for email record
			if (acct != null) {
				EmailRecordBO eBO = new EmailRecordBO(e);
				emailRecordMap.put(e.getAccountNumber(), eBO);

				SubscriberAccountBO saBO = new SubscriberAccountBO(acct);
				accountMap.put(e.getAccountNumber(), saBO);

				// save the account number of associated with the id used for
				// setting current account.
				if (eBO.getSerialNumber() == id) {
					accountNumber = eBO.getAccountNumber();
				}
			}
		}

		// If not email records remain then the user's account does not exist in
		// XTRNTUPLD
		if (emailRecordMap.size() == 0) {
			// no corresponding accounts for email address
			throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP);
		}

		CustomerBO customer = null;
		if (emailRecordMap.size() > 0) {
			customer = new CustomerBO(accountMap, emailRecordMap);
			if (accountNumber != null) {
				customer.setCurrentAccount(accountNumber);
			}
		}

		return customer;
	}

	/**
	 * This method should only be used based on Cookie Validations It is the only case where the number of email records may be
	 * greater than the number of account records because we do not require that the account be set up for eEdition accounts.
	 * 
	 * @param emailAddress
	 * @return
	 * @throws UsatException
	 */
	public CustomerBO getGenesysCustomerByEmail(String emailAddress) throws UsatException {

		EmailRecordDAO emailDAO = new EmailRecordDAO();
		SubscriberAccountDAO saDAO = new SubscriberAccountDAO();

		HashMap<String, EmailRecordIntf> emailRecordMap = new HashMap<String, EmailRecordIntf>();
		HashMap<String, SubscriberAccountIntf> accountMap = new HashMap<String, SubscriberAccountIntf>();

		Collection<EmailRecordIntf> emailRecords = null;
		emailRecords = emailDAO.getEmailRecordsForEmailAddress(emailAddress);

		boolean hasExpiredNewStarts = false;

		// login failed if no records returned.
		if (emailRecords == null || emailRecords.size() == 0) {
			LoginException le = null;
			le = new LoginException(LoginException.NO_ACCOUNT_SETUP);
			throw le;
		}

		Iterator<EmailRecordIntf> eIterator = emailRecords.iterator();
		while (eIterator.hasNext()) {
			EmailRecordIntf emailRec = eIterator.next();

			/*
			 * if (emailRec.getPermStartDate() != null && emailRec.getPermStartDate().length() == 8 &&
			 * !emailRec.getPermStartDate().equalsIgnoreCase("00000000")) { DateTime dt =
			 * UsatDateTimeFormatter.convertYYYYMMDDToDateTime(emailRec.getPermStartDate());
			 * 
			 * // only add eEditions without an account number for up to X // days. if
			 * (dt.plusDays(UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS).isAfterNow()) {
			 */EmailRecordBO eBO = new EmailRecordBO(emailRec);
			emailRecordMap.put(emailRec.getAccountNumber(), eBO);
			/*
			 * } else { hasExpiredNewStarts = true; } }
			 */break;
		}

		// Collection<PersistentSubscriberAccountIntf> subscriberAccounts = saDAO.getAccountForCustomer(emailAddress, password);
		Collection<PersistentSubscriberAccountIntf> subscriberAccounts = saDAO.getGenesysAccountForCustomer(emailAddress);

		Iterator<PersistentSubscriberAccountIntf> aItr = subscriberAccounts.iterator();
		while (aItr.hasNext()) {
			PersistentSubscriberAccountIntf sa = aItr.next();

			EmailRecordIntf email = null;
			Iterator<EmailRecordIntf> eItr = emailRecords.iterator();
			while (eItr.hasNext()) {
				email = eItr.next();
				email.setAccountNumber(sa.getAccountNumber());
				email.setPubCode(sa.getPubCode());
				break;
			}
			// convert to EmailRecordBO
			EmailRecordBO eBO = new EmailRecordBO(email);
			emailRecordMap.put(email.getAccountNumber(), eBO);

			SubscriberAccountBO saBO = new SubscriberAccountBO(sa);
			accountMap.put(sa.getAccountNumber(), saBO);
		}
		// end new

		// If not email records remain then the user's account does not exist in
		// XTRNTUPLD or they don't have any new accounts
		if (emailRecordMap.size() == 0) {
			if (hasExpiredNewStarts) {
				throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP_EDITION_ACCESS_EXPIRED);
			} else {
				// no corresponding accounts for email address
				throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP);
			}
		}

		CustomerBO customer = null;
		if (emailRecordMap.size() > 0) {
			customer = new CustomerBO(accountMap, emailRecordMap);
		}

		return customer;
	}

	public boolean getAutoLogin(String autoLogin) throws Exception {

		boolean result = false;
		try {
			AutoLoginResponse response = null;
			AutoLogin gUser = new AutoLogin();
			response = gUser.getAutoLogin(autoLogin);
			if (!response.containsErrors()) {
				result = true;
			}
		} catch (Exception e) {
			System.out.println("Firefly Atypon Auto Login API had issues retrieving the user " + e.getMessage());
		}
		return result;
	}

	/**
	 * This method should only be used based on Cookie Validations It is the only case where the number of email records may be
	 * greater than the number of account records because we do not require that the account be set up for eEdition accounts.
	 * 
	 * @param emailAddress
	 * @return
	 * @throws UsatException
	 */
	public CustomerBO getCustomerByEmail(String emailAddress) throws UsatException {

		EmailRecordDAO emailDAO = new EmailRecordDAO();
		SubscriberAccountDAO saDAO = new SubscriberAccountDAO();

		HashMap<String, EmailRecordIntf> emailRecordMap = new HashMap<String, EmailRecordIntf>();
		HashMap<String, SubscriberAccountIntf> accountMap = new HashMap<String, SubscriberAccountIntf>();

		Collection<EmailRecordIntf> emailRecords = null;
		emailRecords = emailDAO.getEmailRecordsForEmailAddress(emailAddress);

		boolean hasExpiredNewStarts = false;

		// login failed if no records returned.
		if (emailRecords == null || emailRecords.size() == 0) {
			LoginException le = null;
			le = new LoginException(LoginException.NO_ACCOUNT_SETUP);
			throw le;
		}

		Iterator<EmailRecordIntf> eIterator = emailRecords.iterator();
		if (emailRecords.size() == 1) { // if single account association then
										// use optimized query
			while (eIterator.hasNext()) {
				EmailRecordIntf e = eIterator.next();
				// skip new accounts that have not been completely set up
				if ((e.getAccountNumber().trim().length() == 0) || e.getAccountNumber().equalsIgnoreCase("0000000")) {
					// if no account number assigned yet but less than 3 days
					// old..let them sign in.
					if (e.getPermStartDate() != null && e.getPermStartDate().length() == 8
							&& !e.getPermStartDate().equalsIgnoreCase("00000000")) {
						DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(e.getPermStartDate());

						// only add eEditions without an account number for up
						// to X days after purchase.
						if (dt.plusDays(UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS).isAfterNow()) {
							EmailRecordBO eBO = new EmailRecordBO(e);
							emailRecordMap.put(e.getAccountNumber(), eBO);
						} else {
							hasExpiredNewStarts = true;
						}
					}
					continue;
				}

				Collection<PersistentSubscriberAccountIntf> accounts = saDAO.getAccountByAccountNumberAndPub(e.getAccountNumber(),
						e.getPubCode());
				PersistentSubscriberAccountIntf acct = null;
				if (accounts.size() == 1) {
					acct = (PersistentSubscriberAccountIntf) accounts.toArray()[0];
				}

				if (acct != null) {
					EmailRecordBO eBO = new EmailRecordBO(e);
					emailRecordMap.put(e.getAccountNumber(), eBO);

					SubscriberAccountBO saBO = new SubscriberAccountBO(acct);
					accountMap.put(e.getAccountNumber(), saBO);
				} else {
					// use perm start date on email record to make sure still
					// within grace
					if (e.getPermStartDate() != null && e.getPermStartDate().length() == 8
							&& !e.getPermStartDate().equalsIgnoreCase("00000000")) {
						DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(e.getPermStartDate());

						// only add eEditions without an account number for up
						// to X days after purchase.
						if (dt.plusDays(UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS).isAfterNow()) {
							EmailRecordBO eBO = new EmailRecordBO(e);
							emailRecordMap.put(e.getAccountNumber(), eBO);
						} else {
							hasExpiredNewStarts = true;
						}
					}
				}
			}
		} // end if single account
		else { // multiple accounts
			int newAccountsCount = 0;
			for (EmailRecordIntf emailRec : emailRecords) {

				if (emailRec.getAccountNumber().trim().length() == 0 || emailRec.getAccountNumber().equalsIgnoreCase("0000000")) {
					// account number not set yet
					if (emailRec.getPermStartDate() != null) {
						DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(emailRec.getPermStartDate());

						// only add eEditions without an account number for up
						// to X days.
						if (dt.plusDays(UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS).isAfterNow()) {
							newAccountsCount++;
							StringBuilder tempAcctNum = new StringBuilder("Pending (");
							tempAcctNum.append(newAccountsCount).append(")");

							EmailRecordBO eBO = new EmailRecordBO(emailRec);
							emailRecordMap.put(tempAcctNum.toString(), eBO);
						}
					}
					continue;
				}

				Collection<PersistentSubscriberAccountIntf> subscriberAccounts = saDAO.getAccountByAccountNumberAndPub(
						emailRec.getAccountNumber(), emailRec.getPubCode());

				Iterator<PersistentSubscriberAccountIntf> aItr = subscriberAccounts.iterator();
				while (aItr.hasNext()) {
					PersistentSubscriberAccountIntf sa = aItr.next();

					// convert to EmailRecordBO
					EmailRecordBO eBO = new EmailRecordBO(emailRec);
					emailRecordMap.put(emailRec.getAccountNumber(), eBO);

					SubscriberAccountBO saBO = new SubscriberAccountBO(sa);
					accountMap.put(emailRec.getAccountNumber(), saBO);
				} // end while

			}
		} // end else multiples
			// end new

		// If not email records remain then the user's account does not exist in
		// XTRNTUPLD or they don't have any new accounts
		if (emailRecordMap.size() == 0) {
			if (hasExpiredNewStarts) {
				throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP_EDITION_ACCESS_EXPIRED);
			} else {
				// no corresponding accounts for email address
				throw new LoginException(LoginException.NO_AS400_ACCOUNT_SETUP);
			}
		}

		CustomerBO customer = null;
		if (emailRecordMap.size() > 0) {
			customer = new CustomerBO(accountMap, emailRecordMap);
		}

		return customer;
	}
}
