package com.usatoday.integration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.UsatException;

public class SupplierDAO extends USATodayDAO {

	private static final String SUPPLIER_FIELDS = "id, vendor_name, secret_key, active, login_url, product_url, sample_url, tutorial_url, vendor_description, alternate_product_url, alternate_login_url";
	private static final String SELECT_ALL = "select " + SupplierDAO.SUPPLIER_FIELDS + " from usat_supplier";
	private static final String SELECT_BY_ID = "select " + SupplierDAO.SUPPLIER_FIELDS + " from usat_supplier where id = ?";

	private static Collection<SupplierTO> objectFactory(ResultSet rs) throws UsatException {
		Collection<SupplierTO> records = new ArrayList<SupplierTO>();
		try {
			while (rs.next()) {

				SupplierTO supplier = new SupplierTO();

				int primaryKey = rs.getInt("id");
				supplier.setID(primaryKey);

				String tempStr = null;

				tempStr = rs.getString("vendor_name");
				if (tempStr != null) {
					supplier.setName(tempStr.trim());
				}

				tempStr = rs.getString("secret_key");
				if (tempStr != null) {
					supplier.setSecretKey(tempStr.trim());
				}

				tempStr = rs.getString("login_url");
				if (tempStr != null) {
					supplier.setLoginURL(tempStr.trim());
				}

				tempStr = rs.getString("alternate_login_url");
				if (tempStr != null) {
					supplier.setAlternateLoginURL(tempStr.trim());
				}

				tempStr = rs.getString("product_url");
				if (tempStr != null) {
					supplier.setProductURL(tempStr.trim());
				}

				tempStr = rs.getString("alternate_product_url");
				if (tempStr != null) {
					supplier.setAlteranteProductURL(tempStr.trim());
				}

				tempStr = rs.getString("sample_url");
				if (tempStr != null) {
					supplier.setSampleURL(tempStr.trim());
				}

				tempStr = rs.getString("tutorial_url");
				if (tempStr != null) {
					supplier.setTutorialURL(tempStr.trim());
				}

				tempStr = rs.getString("active");
				if (tempStr != null && tempStr.trim().equalsIgnoreCase("Y")) {
					supplier.setActive(true);
				} else {
					supplier.setActive(false);
				}

				tempStr = rs.getString("vendor_description");
				if (tempStr != null) {
					supplier.setDescription(tempStr.trim());
				}

				records.add(supplier);
			}
		} catch (SQLException sqle) {
			throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
		}
		return records;
	}

	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public Collection<SupplierTO> fetchSuppliers() throws UsatException {
		java.sql.Connection conn = null;
		Collection<SupplierTO> records = null;
		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(SupplierDAO.SELECT_ALL);

			ResultSet rs = statement.executeQuery();

			records = SupplierDAO.objectFactory(rs);

		} catch (Exception e) {
			System.out.println("SupplierDAO:fetchSuppliers " + e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}

		return records;
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws UsatException
	 */
	public SupplierTO fetchSupplierByID(int id) throws UsatException {
		java.sql.Connection conn = null;
		Collection<SupplierTO> records = null;
		SupplierTO supplier = null;

		try {

			conn = ConnectionManager.getInstance().getConnection();

			java.sql.PreparedStatement statement = conn.prepareStatement(SupplierDAO.SELECT_BY_ID);

			statement.setInt(1, id);

			ResultSet rs = statement.executeQuery();

			records = SupplierDAO.objectFactory(rs);

			if (records.size() > 1) {
				throw new UsatException("Problem retrieving supplier with id: " + id);
			} else {
				for (SupplierTO s : records) {
					supplier = s;
				}
			}

		} catch (Exception e) {
			System.out.println("SupplierDAO:fetchSupplierByID " + e.getMessage());
			throw new UsatException(e);
		} finally {
			this.cleanupConnection(conn);
		}

		return supplier;
	}

}
