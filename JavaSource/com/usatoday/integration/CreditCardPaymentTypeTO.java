/*
 * Created on Apr 27, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;

import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentTypentf;

/**
 * @author aeast
 * @date Apr 27, 2006
 * @class PaymentTypeTO
 * 
 *        This class describes teh paymentType available..
 * 
 */
public class CreditCardPaymentTypeTO implements CreditCardPaymentTypentf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2385567347021176693L;
	private int validLength1 = 0;
	private int validLength2 = 0;
	private String description = "";
	private char firstDigit = ' ';
	private String imagePath = "";
	private String label = "";
	private String type = "";

	public CreditCardPaymentTypeTO(int validLength1, int validLength2, String description, char firstDigit, String imagePath,
			String label, String type) {
		super();
		this.validLength1 = validLength1;
		this.validLength2 = validLength2;
		this.description = description;
		this.firstDigit = firstDigit;
		this.imagePath = imagePath;
		this.label = label;
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#getType()
	 */
	public String getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#getLabel()
	 */
	public String getLabel() {
		return label;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#getDescription()
	 */
	public String getDescription() {
		return description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#getAccountNumberMinimumDigits()
	 */
	public int getValidLength1() {
		return validLength1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#getAccountNumMaximumDigits()
	 */
	public int getValidLength2() {
		return validLength2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#getFirstDigit()
	 */
	public char getFirstDigit() {
		return firstDigit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#getImagePath()
	 */
	public String getImagePath() {
		return imagePath;
	}

	public boolean requiresPaymentProcessing() {
		return true;
	}
}
