/*
 * Created on Apr 13, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.integration;

import java.io.Serializable;
import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.businessObjects.products.promotions.PromotionSet;

/**
 * @author aeast
 * @date Apr 13, 2006
 * @class SubscriptionTermsTO
 * 
 *        I need to create a different interface for the TO that does not include methods for BO's a TOIntf.
 * 
 */
public class SubscriptionOfferTO implements SubscriptionOfferIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6600176217476773773L;

	public String getPercentOffBasePrice(double basePrice) {
		return "Invoke method on business object not TO object";
	}

	private String keyCode = "";
	private Collection<SubscriptionTermsIntf> terms = null;
	private Collection<SubscriptionTermsIntf> renewalTerms = null;
	private String pubCode = "";
	private boolean billMeAllowed = false;
	private boolean forceEZPay = false;
	private boolean forceBillMe = false;
	private boolean ClubNumberRequired = false;
	private double dailyRate = 0.0;

	public SubscriptionOfferTO(String keyCode, String pubCode) {
		super();
		this.keyCode = keyCode;
		this.pubCode = pubCode;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public void setTerms(Collection<SubscriptionTermsIntf> terms) {
		this.terms = terms;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionOfferIntf#getKeycode()
	 */
	public String getKeyCode() {
		return keyCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionOfferIntf#getTerms()
	 */
	public Collection<SubscriptionTermsIntf> getTerms() {
		return terms;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionOfferIntf#getPubCode()
	 */
	public String getPubCode() {
		return pubCode;
	}

	public boolean isBillMeAllowed() {
		return this.billMeAllowed;
	}

	public boolean isForceEZPay() {
		return this.forceEZPay;
	}

	public void setBillMeAllowed(boolean billMeAllowed) {
		this.billMeAllowed = billMeAllowed;
	}

	public void setForceEZPay(boolean forceEZPay) {
		this.forceEZPay = forceEZPay;
	}

	public Collection<SubscriptionTermsIntf> getRenewalTerms() {
		return this.renewalTerms;
	}

	public void setRenewalTerms(Collection<SubscriptionTermsIntf> renewalTerms) {
		this.renewalTerms = renewalTerms;
	}

	public String getKey() {
		return this.getKeyCode() + this.getPubCode();
	}

	public double getDailyRate() {
		return this.dailyRate;
	}

	public SubscriptionTermsIntf getRenewalTerm(String termLength) {
		return null;
	}

	public void setDailyRate(double dailyRate) {
		this.dailyRate = dailyRate;
	}

	public PromotionSet getPromotionSet() {
		// This method implemented in business object only
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.OfferIntf#getTerm(java.lang.String)
	 */
	public SubscriptionTermsIntf getTerm(String termLength) {
		// This method implemented in business object only
		return null;
	}

	/**
	 * @param clubNumberRequired
	 *            the clubNumberRequired to set
	 */
	public void setClubNumberRequired(boolean clubNumberRequired) {
		ClubNumberRequired = clubNumberRequired;
	}

	/**
	 * @return the clubNumberRequired
	 */
	public boolean isClubNumberRequired() {
		return ClubNumberRequired;
	}

	public SubscriptionProductIntf getSubscriptionProduct() {
		// Method not implemented in TO
		return null;
	}

	public boolean isForceBillMe() {
		return forceBillMe;
	}

	public void setForceBillMe(boolean forceBillMe) {
		this.forceBillMe = forceBillMe;
	}

	public SubscriptionTermsIntf findTermUsingTermLength(String termLength) {
		// This method implemented in business object only
		return null;
	}

	@Override
	public ProductIntf getProduct() throws UsatException {
		// call on bo object only - Andy's bad design.
		return null;
	}
}
