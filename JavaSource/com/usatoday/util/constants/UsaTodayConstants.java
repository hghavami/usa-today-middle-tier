/*
 * Created on Apr 13, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.util.constants;

import java.util.Properties;

import com.usatoday.businessObjects.util.crypto.USATCreditCardEncryptor;

/**
 * @author aeast
 * @date Apr 13, 2006
 * @class UsaTodayConstants
 * 
 *        This class provides a place to store application constants.
 * 
 */
public class UsaTodayConstants {

	// Application Defaults
	public static final String UT_PUBCODE = "UT";
	public static final String SW_PUBCODE = "BW";
	public static final String BW_PUBCODE = UsaTodayConstants.SW_PUBCODE;

	public static String DEFAULT_UT_EE_PUBCODE = "EE"; // ut electronic edition pubcode
	public static String DEFAULT_SW_EE_PUBCODE = ""; // Doesn't Exist

	public static String DEFAULT_UT_RENEWAL_RATE_CODE = "DG";
	public static String DEFAULT_SW_RENEWAL_RATE_CODE = "BA";
	public static String DEFAULT_EE_RENEWAL_RATE_CODE = "";

	// Electronic Edition Settings
	public static boolean UT_EE_DUAL_OFFER_ACTIVE = false;
	public static boolean BW_EE_DUAL_OFFER_ACTIVE = false;
	public static String DEFAULT_TRIAL_PID = "USAT001";

	public static boolean UT_EE_COMPANION_ACTIVE = false;
	public static boolean BW_EE_COMPANION_ACTIVE = false;

	// Force EZ-PAY Default terms config
	public static boolean UT_FORCE_EZPAY_TERM_ENABLED = true;
	public static String UT_FORCE_EZPAY_RATECODE = "";
	public static boolean SW_FORCE_EZPAY_TERM_ENABLED = false;
	public static String SW_FORCE_EZPAY_RATECODE = "";
	public static boolean EE_FORCE_EZPAY_TERM_ENABLED = true;
	public static String EE_FORCE_EZPAY_RATECODE = "";

	// Payment Configurations
	public static boolean AUTHORIZE_CREDIT_CARDS = true;
	public static String PAYMENT_PROCESSING_CONFIG_FILE = "/USAT/extranet/pti.properties";
	public static boolean BATCH_PROCESS_CC_NETWORK_FAILURES = true;
	public static boolean CRYPTO_STRONG_ENCRYPT_CARDS = true;
	public static boolean CRYPTO_WEAK_ENCRYPT_CARDS = true;
	public static String CRYPTO_KEYSTORE = "/USAT/extranet/usat.keystore";
	public static String CRYPTO_KEYSTORE_PWD = "portalencryptstore";
	public static String CRYPTO_KEY_ALIAS = "usatoday.midrange.com";

	// Address Validation Configurations
	public static boolean VALIDATE_POSTAL_ADDRESSES = true;
	public static boolean ALLOW_FAILED_ADDRESSES = true;
	public static boolean DELIVERY_NOTIFY = false;
	public static String CODE1_PROPERTY_FILE = "/USAT/extranet/code1.properties";
	public static boolean EDELIVERY_NOTIFY = false;
	public static String EDELIVERY_EMP_ID = "";
	public static String EDELIVERY_TYPE = "";
	public static String EDELIVERY_NS_CONFIRM_MSG = "";
	public static String DATE_RANGE_ALLOWED = "";
	public static boolean EE_CANCELS_ALLOWED = false;
	public static boolean EE_CANCELS_ALLOWED_PROP = false;

	// Email Configurations
	public static String EMAIL_SERVER = "10.209.4.25";
	public static String EMAIL_DEFAULT_FROM_ADDRESS = "*USA TODAY*<hghavami@usatoday.com>";
	public static String EMAIL_CONFIRMATIONS_COPYTO_ADDRESS = "hghavami@usatoday.com";
	public static String EMAIL_ALERT_RECEIVER_EMAILS = "";
	public static String EMAIL_ALERT_SENDER = "hghavami@usatoday.com";
	public static boolean EMAIL_ALERTS_ENABLED = true;
	public static String EMAIL_GLOBAL_PROMO_TEXT = "";
	public static String EMAIL_UT_PROMO_TEXT = "";
	public static String EMAIL_BW_PROMO_TEXT = "";
	public static boolean UPDATE_EMAIL_FILE = false;

	// eEdition link to the USAT site
	public static String EEDITION_UT_LINK = "";
	public static String EEDITION_BW_LINK = "";
	public static String EEDITION_NEWSTART_ID = "";
	public static String EEDITION_CANCELS_ID = "";
	public static String EEDITION_NEWSTART_DESC = "";
	public static String EEDITION_NEWSTART_GIFT_ID = "";
	public static String EEDITION_NEWSTART_GIFT_DESC = "";
	public static String EEDITION_NEWSTART_CARRIER_ID = "";
	public static String EEDITION_NEWSTART_CARRIER_DESC = "";
	public static String EEDITION_EE_NEWSTART_ID = "";
	public static String EEDITION_GIFT_CARD_ID = "";
	public static String EEDITION_ORDER_GIFT_CARD_ID = "";
	public static String EEDITION_EE_NEWSTART_DESC = "";
	public static int EEDITION_GRACE_LOGIN_DAYS = 5;
	public static String EEDITION_OLIVE_PARTNER_ERROR_PAGE = "";
	public static String EEDITION_CHOICE_OLIVE_PARTNER_ERROR_PAGE = "";
	public static String OLIVE_PARTNERS_URL = "";
	public static String OLIVE_PARTNERS_ACCESS_FILE_PATH = "";
	public static String OLIVE_PARTNERS_ACCESS_FILE_NAME = "";
	public static String OLIVE_PARTNERS_ACCESS_FILE_EXT = "";
	public static String OLIVE_PARTNER_ID = "";
	public static String OLIVE_PARTNERS_ACCESS_EMAIL_SUBJECT = "";
	public static String OLIVE_PARTNERS_CONTACT_EMAIL = "";
	public static int OLIVE_PARTNER_ACCESS_CODE_DURATION = 10;
	public static int OLIVE_DAILY_PARTNER_ACCESS_CODE_DURATION = 1;
	public static String HUDSON_NEWS_OLIVE_PARTNER_ID = "";
	public static String CHOICE_OLIVE_PARTNER_ID = "";
	
	
	public static int MAX_DAYS_IN_FUTURE_FOR_UT_BRAND_SUBSCRIPTION_START = 90;
	public static int MAX_DAYS_IN_FUTURE_FOR_SW_BRAND_SUBSCRIPTION_START = 90;
	public static String LIST_SERVICES_UT_KEYCODES = "";
	public static String LIST_SERVICES_EE_KEYCODES = "";
	public static String CJ_CUST_SERV_PHONE_CK_SRC_PROP = "";

	public static String alarmRecipients = "";
	public static boolean alarmsEnabled = true;
	public static String alarmSender = "";
	public static String as400Server = null;
	public static long backEndCheckFrequency = 60000;
	public static boolean debug = false;
	public static String smtpServer = null;
	public static boolean webAnalyticsActive = false;
 

	// Following values may be overridden from the usat_application.properties file when system initalizes
	// public static String MOBILE_DEVICES_1 =
	// ".*(android.+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino).*";
	// public static String MOBILE_DEVICES_2 =
	// "1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|e\\-|e\\/|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\\-|2|g)|yas\\-|your|zeto|zte\\-";
	public static String MOBILE_DEVICES_1 = "";
	public static String MOBILE_DEVICES_2 = "";

	public static void loadProperties() {
		try {
			Properties props = new USATApplicationPropertyFileLoader().loadapplicationProperties();

			String tempStr = null;

			tempStr = props.getProperty("com.usatoday.runtime.debug");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("true")) {
				UsaTodayConstants.debug = true;
			} else {
				UsaTodayConstants.debug = false;
			}

			tempStr = props.getProperty("com.usatoday.AS400.server");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.as400Server = tempStr.trim();
			} else {
				UsaTodayConstants.as400Server = "";
			}

			tempStr = props.getProperty("com.usatoday.AS400.server.checkFrequency");
			if (tempStr != null && tempStr.trim().length() > 0) {
				try {
					UsaTodayConstants.backEndCheckFrequency = Long.parseLong(tempStr.trim());
					if (UsaTodayConstants.backEndCheckFrequency < 1000) {
						UsaTodayConstants.backEndCheckFrequency = 60000; // if unaceptable go to default
					}
				} catch (Exception e) {
					; // ignore and use default of one minute
				}
			} else {
				UsaTodayConstants.as400Server = "";
			}

			// default e-Edition companion keycodes
			tempStr = props.getProperty("usat.ut.companion.pubCode");
			if (tempStr != null) {
				UsaTodayConstants.DEFAULT_UT_EE_PUBCODE = tempStr.trim();
			}

			tempStr = props.getProperty("usat.sw.companion.pubCode");
			if (tempStr != null) {
				UsaTodayConstants.DEFAULT_SW_EE_PUBCODE = tempStr.trim();
			}

			tempStr = props.getProperty("usat.ut.default.renewal.ratecode");
			if (tempStr != null) {
				UsaTodayConstants.DEFAULT_UT_RENEWAL_RATE_CODE = tempStr.trim();
			}

			tempStr = props.getProperty("usat.sw.default.renewal.ratecode");
			if (tempStr != null) {
				UsaTodayConstants.DEFAULT_SW_RENEWAL_RATE_CODE = tempStr.trim();
			}

			// usat.ee.default.renewal.ratecode
			tempStr = props.getProperty("usat.ee.default.renewal.ratecode");
			if (tempStr != null) {
				UsaTodayConstants.DEFAULT_EE_RENEWAL_RATE_CODE = tempStr.trim();
			}

			// electronic edition dual offer page settings
			tempStr = props.getProperty("usat.ut.ee.dualOffer.active");
			if (tempStr == null || tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.UT_EE_DUAL_OFFER_ACTIVE = false;
			} else {
				UsaTodayConstants.UT_EE_DUAL_OFFER_ACTIVE = true;
			}

			tempStr = props.getProperty("usat.sw.ee.dualOffer.active");
			if (tempStr == null || tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.BW_EE_DUAL_OFFER_ACTIVE = false;
			} else {
				UsaTodayConstants.BW_EE_DUAL_OFFER_ACTIVE = true;
			}

			// EE Companion product program
			tempStr = props.getProperty("usat.ut.companion.active");
			if (tempStr == null || tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.UT_EE_COMPANION_ACTIVE = false;
			} else {
				UsaTodayConstants.UT_EE_COMPANION_ACTIVE = true;
			}

			tempStr = props.getProperty("usat.sw.companion.active");
			if (tempStr == null || tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.BW_EE_COMPANION_ACTIVE = false;
			} else {
				UsaTodayConstants.BW_EE_COMPANION_ACTIVE = true;
			}

			// EE Grace Login Days
			tempStr = props.getProperty("usat.ut.ee.numberOfLoginGraceDays");
			if (tempStr != null && tempStr.trim().length() > 0) {
				try {
					UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS = Integer.parseInt(tempStr.trim());
					if (UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS < 1) {
						UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS = 5; // if unaceptable go to default
					}
				} catch (Exception e) {
					; // ignore and use default of one minute
					UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS = 5;
				}
			}

			// Max Future Start Date UT Brand
			tempStr = props.getProperty("usat.ut.futureStartLimit");
			if (tempStr != null && tempStr.trim().length() > 0) {
				try {
					UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_UT_BRAND_SUBSCRIPTION_START = Integer.parseInt(tempStr.trim());
					if (UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_UT_BRAND_SUBSCRIPTION_START < 1) {
						UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_UT_BRAND_SUBSCRIPTION_START = 90; // if unaceptable go to default
					}
				} catch (Exception e) {
					; // ignore and use default of one minute
					UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_UT_BRAND_SUBSCRIPTION_START = 90;
				}
			}

			// Max Future Start Date BW Brand
			tempStr = props.getProperty("usat.sw.futureStartLimit");
			if (tempStr != null && tempStr.trim().length() > 0) {
				try {
					UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_SW_BRAND_SUBSCRIPTION_START = Integer.parseInt(tempStr.trim());
					if (UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_SW_BRAND_SUBSCRIPTION_START < 1) {
						UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_SW_BRAND_SUBSCRIPTION_START = 90; // if unaceptable go to default
					}
				} catch (Exception e) {
					; // ignore and use default of one minute
					UsaTodayConstants.MAX_DAYS_IN_FUTURE_FOR_SW_BRAND_SUBSCRIPTION_START = 90;
				}
			}

			tempStr = props.getProperty("usat.ut.default.forceEZPAY.enabled");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.UT_FORCE_EZPAY_TERM_ENABLED = false;
			} else {
				UsaTodayConstants.UT_FORCE_EZPAY_TERM_ENABLED = true;
				// get the rate code for ut
				tempStr = props.getProperty("usat.ut.default.forceEZPAY.rateCode");
				if (tempStr != null && tempStr.trim().length() > 0) {
					UsaTodayConstants.UT_FORCE_EZPAY_RATECODE = tempStr.trim();
				} else {
					UsaTodayConstants.UT_FORCE_EZPAY_RATECODE = null;
				}
			}
			// EE_FORCE_EZPAY_TERM_ENABLED
			tempStr = props.getProperty("usat.ee.default.forceEZPAY.enabled");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.EE_FORCE_EZPAY_TERM_ENABLED = false;
			} else {
				UsaTodayConstants.EE_FORCE_EZPAY_TERM_ENABLED = true;
				// get the rate code for ut
				tempStr = props.getProperty("usat.ee.default.forceEZPAY.rateCode");
				if (tempStr != null && tempStr.trim().length() > 0) {
					UsaTodayConstants.EE_FORCE_EZPAY_RATECODE = tempStr.trim();
				} else {
					UsaTodayConstants.EE_FORCE_EZPAY_RATECODE = null;
				}
			}

			tempStr = props.getProperty("usat.sw.default.forceEZPAY.enabled");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.SW_FORCE_EZPAY_TERM_ENABLED = false;
			} else {
				UsaTodayConstants.SW_FORCE_EZPAY_TERM_ENABLED = true;
				// get the rate code for ut
				tempStr = props.getProperty("usat.sw.default.forceEZPAY.rateCode");
				if (tempStr != null && tempStr.trim().length() > 0) {
					UsaTodayConstants.SW_FORCE_EZPAY_RATECODE = tempStr.trim();
				} else {
					UsaTodayConstants.SW_FORCE_EZPAY_RATECODE = null;
				}
			}

			tempStr = props.getProperty("com.usatoday.payments.ptiConfigFile");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.PAYMENT_PROCESSING_CONFIG_FILE = tempStr.trim();
			}
			tempStr = props.getProperty("com.usatoday.payments.validateCC");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.AUTHORIZE_CREDIT_CARDS = false;
			} else {
				UsaTodayConstants.AUTHORIZE_CREDIT_CARDS = true;
			}

			tempStr = props.getProperty("com.usatoday.payments.batchProcessNetworkFailures");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.BATCH_PROCESS_CC_NETWORK_FAILURES = false;
			} else {
				UsaTodayConstants.BATCH_PROCESS_CC_NETWORK_FAILURES = true;
			}

			tempStr = props.getProperty("com.usatoday.address.validateAddress");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.VALIDATE_POSTAL_ADDRESSES = false;
			} else {
				UsaTodayConstants.VALIDATE_POSTAL_ADDRESSES = true;
			}

			tempStr = props.getProperty("com.usatoday.address.code1ConfigFile");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.CODE1_PROPERTY_FILE = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.address.allowInvalidAddresses");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.ALLOW_FAILED_ADDRESSES = false;
			} else {
				UsaTodayConstants.ALLOW_FAILED_ADDRESSES = true;
			}

			tempStr = props.getProperty("com.usatoday.delivery.notify");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.DELIVERY_NOTIFY = false;
			} else {
				UsaTodayConstants.DELIVERY_NOTIFY = true;
			}

			tempStr = props.getProperty("com.usatoday.edelivery.notify");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.EDELIVERY_NOTIFY = false;
			} else {
				UsaTodayConstants.EDELIVERY_NOTIFY = true;
			}

			tempStr = props.getProperty("com.usatoday.edelivery.emp_id");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EDELIVERY_EMP_ID = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.edelivery.type");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EDELIVERY_TYPE = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.edelivery.newsstand_confirmation_message");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EDELIVERY_NS_CONFIRM_MSG = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.date.range.allowed");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.DATE_RANGE_ALLOWED = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.ee.cancels.allowed");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.EE_CANCELS_ALLOWED_PROP = false;
			} else {
				UsaTodayConstants.EE_CANCELS_ALLOWED_PROP = true;
			}

			tempStr = props.getProperty("com.usatoday.payments.crypto.strong.enabled");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.CRYPTO_STRONG_ENCRYPT_CARDS = false;
			} else {
				UsaTodayConstants.CRYPTO_STRONG_ENCRYPT_CARDS = true;
			}

			tempStr = props.getProperty("com.usatoday.payments.crypto.weak.enabled");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.CRYPTO_WEAK_ENCRYPT_CARDS = false;
			} else {
				UsaTodayConstants.CRYPTO_WEAK_ENCRYPT_CARDS = true;
			}

			tempStr = props.getProperty("com.usatoday.payments.crypto.kstore");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.CRYPTO_KEYSTORE = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.payments.crypto.kPwd");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.CRYPTO_KEYSTORE_PWD = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.payments.crypto.kAlias");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.CRYPTO_KEY_ALIAS = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.mail.smtpserver");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EMAIL_SERVER = tempStr.trim();
			}
			tempStr = props.getProperty("com.usatoday.mail.defaultfrommail");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS = tempStr.trim();
			}
			tempStr = props.getProperty("com.usatoday.mail.global_promo_text");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT = tempStr.trim();
			}
			tempStr = props.getProperty("com.usatoday.mail.ut_promo_text");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EMAIL_UT_PROMO_TEXT = tempStr.trim();
			}
			tempStr = props.getProperty("com.usatoday.mail.bw_promo_text");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EMAIL_BW_PROMO_TEXT = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.mail.confirmationCopyToAddress");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EMAIL_CONFIRMATIONS_COPYTO_ADDRESS = tempStr.trim();
			}
			tempStr = props.getProperty("com.usatoday.mail.alertreceiveremail");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS = tempStr.trim();
			}
			tempStr = props.getProperty("com.usatoday.mail.alertsender");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EMAIL_ALERT_SENDER = tempStr.trim();
			}
			tempStr = props.getProperty("com.usatoday.eedition.ut_link");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_UT_LINK = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.bw_link");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_BW_LINK = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.CancelsID");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_CANCELS_ID = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.newStartID");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_NEWSTART_ID = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.newStartDESC");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_NEWSTART_DESC = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.carrier.newStartID");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_NEWSTART_CARRIER_ID = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.carrier.newStartDESC");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_NEWSTART_CARRIER_DESC = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.gift.orderGiftCardID");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_ORDER_GIFT_CARD_ID = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.gift.giftCardID");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_GIFT_CARD_ID = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.gift.newStartID");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_NEWSTART_GIFT_ID = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.gift.newStartDESC");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_NEWSTART_GIFT_DESC = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.ee_newStartID");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_EE_NEWSTART_ID = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.eedition.ee_newStartDESC");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_EE_NEWSTART_DESC = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.mail.enablealerts");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.EMAIL_ALERTS_ENABLED = false;
			} else {
				UsaTodayConstants.EMAIL_ALERTS_ENABLED = true;
			}

			tempStr = props.getProperty("com.usatoday.mail.update_email_file");
			if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
				UsaTodayConstants.UPDATE_EMAIL_FILE = false;
			} else {
				UsaTodayConstants.UPDATE_EMAIL_FILE = true;
			}

			tempStr = props.getProperty("com.usatoday.ut.listserviceskeycodes");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.LIST_SERVICES_UT_KEYCODES = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.ee.listserviceskeycodes");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.LIST_SERVICES_EE_KEYCODES = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.util.mobiledevice1");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.MOBILE_DEVICES_1 = tempStr.trim();
			}
			tempStr = props.getProperty("com.usatoday.util.mobiledevice2");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.MOBILE_DEVICES_2 = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.olive.partner.errorpage");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_OLIVE_PARTNER_ERROR_PAGE = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.choice.olive.partner.errorpage");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.EEDITION_CHOICE_OLIVE_PARTNER_ERROR_PAGE = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.olive.partners.url");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.OLIVE_PARTNERS_URL = tempStr.trim();
			}
			
			tempStr = props.getProperty("com.usatoday.olive.partners.access.file.path");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.OLIVE_PARTNERS_ACCESS_FILE_PATH = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.olive.partners.access.file.name");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.OLIVE_PARTNERS_ACCESS_FILE_NAME = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.olive.partners.access.file.ext");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.OLIVE_PARTNERS_ACCESS_FILE_EXT = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.olive.partners.access.email.subject");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.OLIVE_PARTNERS_ACCESS_EMAIL_SUBJECT = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.olive.partners.contact.email");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.OLIVE_PARTNERS_CONTACT_EMAIL = tempStr.trim();
			}

			tempStr = props.getProperty("com.usatoday.olive.partners.access.code.duration");
			if (tempStr != null && tempStr.trim().length() > 0) {
				try {
					UsaTodayConstants.OLIVE_PARTNER_ACCESS_CODE_DURATION = Integer.parseInt(tempStr.trim());
					if (UsaTodayConstants.OLIVE_PARTNER_ACCESS_CODE_DURATION < 1) {
						UsaTodayConstants.OLIVE_PARTNER_ACCESS_CODE_DURATION = 10; // if unaceptable go to default
					}
				} catch (Exception e) {
					; // ignore and use default of 10 days
					UsaTodayConstants.OLIVE_PARTNER_ACCESS_CODE_DURATION = 10;
				}
			}

			tempStr = props.getProperty("com.usatoday.olive.daily.partners.access.code.duration");
			if (tempStr != null && tempStr.trim().length() > 0) {
				try {
					UsaTodayConstants.OLIVE_DAILY_PARTNER_ACCESS_CODE_DURATION = Integer.parseInt(tempStr.trim());
					if (UsaTodayConstants.OLIVE_DAILY_PARTNER_ACCESS_CODE_DURATION < 1) {
						UsaTodayConstants.OLIVE_DAILY_PARTNER_ACCESS_CODE_DURATION = 1; // if unaceptable go to default
					}
				} catch (Exception e) {
					; // ignore and use default of 10 days
					UsaTodayConstants.OLIVE_DAILY_PARTNER_ACCESS_CODE_DURATION = 1;
				}
			}

			tempStr = props.getProperty("com.usatoday.hudson.news.olive.partner.id");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.HUDSON_NEWS_OLIVE_PARTNER_ID = tempStr.trim();
			}
		
			tempStr = props.getProperty("com.usatoday.choice.olive.partner.id");
			if (tempStr != null && tempStr.trim().length() > 0) {
				UsaTodayConstants.CHOICE_OLIVE_PARTNER_ID = tempStr.trim();
			}
		
		} catch (Exception e) {
			System.out.println("FAILED TO LOAD APPLICATION PROPERTIES");
			e.printStackTrace();
		}
		USATCreditCardEncryptor.setPublicKeyAlias(UsaTodayConstants.CRYPTO_KEY_ALIAS);
		USATCreditCardEncryptor.setKeyStorePassword(UsaTodayConstants.CRYPTO_KEYSTORE_PWD);
		USATCreditCardEncryptor.setKeyStorePath(UsaTodayConstants.CRYPTO_KEYSTORE);
		Properties p = System.getProperties();
		String t = p.getProperty("mail.smtp.host");
		if (t == null || "localhost".equalsIgnoreCase(t)) {
			p.put("mail.smtp.host", UsaTodayConstants.EMAIL_SERVER);
		}

	}

}
