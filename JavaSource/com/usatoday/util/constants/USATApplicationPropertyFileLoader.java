/*
 * Created on Apr 21, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.util.constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author aeast
 * @date Apr 21, 2006
 * @class USATApplicationPropertyFileLoader
 * 
 *        This class loads property files from the classpath or the file system.
 * 
 */
public class USATApplicationPropertyFileLoader {

	/**
	 * This method attempts to load the usat_application.properties file from one of these places in the followig order: 1. from the
	 * file specified in the System property com.usatoday.usatAppPropertiesFile 2. From the file system in
	 * /USAT/extranet/usat_application.properties 3. /com/usatoday folder of classpath 4. current directory that the
	 * USATApplicationPropertyFileLoader was loaded from 5. From the /WEB-INF folder
	 * 
	 * @return The application properties
	 * @throws Exception
	 */
	public Properties loadapplicationProperties() throws Exception {
		Properties config = new Properties();

		// attempt to load from file system
		File f = null;
		InputStream s = null;
		String path = System.getProperty("com.usatoday.usatAppPropertiesFile");
		if (path != null && path.trim().length() > 0) {
			f = new File(path.trim());
			if (f.exists()) {
				s = new FileInputStream(f);
				System.out.println("Using File System Property File From system property: com.usatoday.extranetini: "
						+ f.getAbsolutePath());
			}
		}
		if (s == null) {
			f = new File("/USAT/extranet/usat_application.properties");
			if (f.exists()) {
				s = new FileInputStream(f);
				System.out.println("Using File Default System Property File From system property:: " + f.getAbsolutePath());
			}
		}

		// check in class path /com/usatoday for the file
		if (s == null) {
			s = this.getClass().getResourceAsStream("/com/usatoday/usat_application.properties");
			// look in the location where this class is loaded from.
			if (s == null) {
				s = this.getClass().getResourceAsStream("usat_application.properties");
				if (s == null) {
					// try in a WEB-INF folder
					s = this.getClass().getResourceAsStream("/WEB-INF/usat_application.properties");
					if (s != null) {
						System.out.println("Using property file '/WEB-INF/usat_application.properties'");
					}
				} else {
					System.out.println("Using property file :" + this.getClass().getPackage() + "/usat_application.properties");
				}
			} else {
				System.out.println("Using property file /com/usatoday/usat_application.properties");
			}
		}

		config.load(s);
		return config;
	}

	/**
	 * 
	 * @param file
	 *            The full path (within the class path) of the property file to be loaded If you do not begin with / then the
	 *            current directory will be searched If no file is found in the classpath, the file system will be checked for file.
	 * @return
	 * @throws Exception
	 */
	public Properties loadPropertyFile(String file) throws Exception {
		Properties config = new Properties();
		// first check in class path /com/usatoday for the file
		InputStream s = this.getClass().getResourceAsStream(file);
		if (s == null) {
			File f = new File(file);
			if (f.exists()) {
				s = new FileInputStream(f);
				System.out.println("Loading File System Property File: " + f.getAbsolutePath());
			}
		}
		config.load(s);
		return config;
	}
}
