package com.usatoday.business.interfaces.partners;

import org.joda.time.DateTime;

public interface TrialPartnerIntf {
	public int getID();

	public boolean getActive();

	public String getPartnerID();

	public int getDurationInDays();

	public DateTime getStartDate();

	public DateTime getEndDate();

	public String getPubCode();

	public String getKeyCode();

	public String getPartnerName();

	public String getDescription();

	public String getCustomLandingPageURL();

	public String getConfirmationEmailTemplateID();

	public String getCompletePageCustomHTML();

	public String getLaunchPageCustomHTML();

	public String getTrialOverCustomHTML();

	public String getDisclaimerCustomHTML();

	/**
	 * This method should factor in both the Active flag And the start and end date.
	 * 
	 * @return
	 */
	public boolean getIsOfferCurrentlyRunning();

	public boolean getIsShowClubNumber();

	public String getClubNumberLabel();

	public String getLogoImageURL();

	public String getLogoLinkURL();

	public String getAuthLinkURL();

	public String getAPISecretHashKey();

	public boolean getIsSendConfirmationOnCreation();
}
