package com.usatoday.business.interfaces.partners;

import org.joda.time.DateTime;

public interface TrialPartnerTOIntf {
	public int getID();

	public void setID(int id);

	public boolean getIsActive();

	public void setIsActive(boolean active);

	public String getPartnerID();

	public void setPartnerID(String pID);

	public int getDurationInDays();

	public void setDurationInDays(int numDays);

	public DateTime getStartDate();

	public void setStartDate(DateTime date);

	public DateTime getEndDate();

	public void setEndDate(DateTime date);

	public String getPubCode();

	public void setPubCode(String pubCode);

	public String getKeyCode();

	public void setKeyCode(String keyCode);

	public String getPartnerName();

	public void setPartnerName(String partnerName);

	public String getDescription();

	public void setDescription(String description);

	public String getCustomLandingPageURL();

	public void setCustomerLadingPageURL(String url);

	public String getConfirmationEmailTemplateID();

	public void setConfirmationEmaiTemplateID(String eId);

	public String getCompletePageCustomHTML();

	public void setCompletePageCustomHTML(String html);

	public String getLaunchPageCustomHTML();

	public void setLaunchPageCustomHTML(String html);

	public String getTrialOverCustomHTML();

	public void setTrialOverCustomHTML(String html);

	public String getDisclaimerCustomHTML();

	public void setDisclaimerCustomHTML(String html);

	public boolean getIsShowClubNumber();

	public void setIsShowClubNumber(boolean showClubNum);

	public String getClubNumberLabel();

	public void setClubNumberLabel(String label);

	public String getLogoImageURL();

	public void setLogoImageURL(String url);

	public String getLogoLinkURL();

	public void setLogoLinkURL(String url);

	public String getAuthLinkURL();

	public void setAuthLinkURL(String url);

	public String getAPISecretHashKey();

	public void setAPISecretHashKey(String key);

	public boolean getIsSendConfirmationOnCreation();

	public void setIsSendConfirmationOnCreation(boolean sendConfirmation);

}
