package com.usatoday.business.interfaces.shopping;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;

public interface SubscriptionOrderItemIntf extends OrderItemIntf {

	public static final String EZPAY = "AUTOPAY_PLAN";
	public static final String BILL_ME = "BILL_ME";
	public static final String ONE_TIME_BILL = "ONE_TIME_BILL_GIFT";

	/**
     */
	public abstract void setSelectedTerm(SubscriptionTermsIntf term);

	public abstract SubscriptionTermsIntf getSelectedTerm();

	public abstract void setOffer(OfferIntf offer);

	public abstract OfferIntf getOffer();

	public abstract String getStartDate();

	public abstract DateTime getStartDateObj();

	public abstract String getKeyCode();

	public abstract String getSubscriptionAmountForPersistence();

	/*
	 * return String representation of (quantity * unit price) + tax
	 */
	public abstract String getSubscriptionTotalAmountForPersistence();

	public abstract String getOrderID();

	public abstract void setOrderID(String orderID);

	public abstract boolean isOneTimeBill();

	public abstract boolean isChoosingEZPay();

	public abstract String getDeliveryEmailAddress();

	public abstract String getBillingEmailAddress();

	public abstract String getPremiumCode();

	public abstract void setPremiumCode(String premiumCode);

	public abstract String getClubNumber();

	public abstract void setClubNumber(String clubNumber);

	public abstract void setDeliveryMethod(String method);

	public abstract String getDeliveryMethod();
}
