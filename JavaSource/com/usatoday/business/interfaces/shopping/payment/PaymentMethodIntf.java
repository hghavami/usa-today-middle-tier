package com.usatoday.business.interfaces.shopping.payment;

import com.usatoday.UsatException;

public interface PaymentMethodIntf {

	public abstract PaymentTypeIntf getPaymentType();

	/**
     */
	public abstract String getLabel();

	/**
     */
	public abstract String getDisplayValue();

	/**
     */
	public abstract void setPaymentType(PaymentTypeIntf type) throws UsatException;

	/**
     */
	public abstract String getAccountNumberForPersistence();

}
