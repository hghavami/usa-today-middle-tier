/*
 * Created on Apr 27, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.shopping.payment;

/**
 * @author aeast
 * @date Apr 27, 2006
 * @class CreditCardPaymentTypentf
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public interface CreditCardPaymentTypentf extends PaymentTypeIntf {

	public abstract int getValidLength1();

	public abstract int getValidLength2();

	/**
     */
	public abstract char getFirstDigit();

	/**
     */
	public abstract String getImagePath();

}
