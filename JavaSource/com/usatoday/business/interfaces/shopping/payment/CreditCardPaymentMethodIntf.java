package com.usatoday.business.interfaces.shopping.payment;

import com.usatoday.UsatException;

public interface CreditCardPaymentMethodIntf extends com.usatoday.business.interfaces.shopping.payment.PaymentMethodIntf {

	public static final String CREDITCARD_MONTHS[] = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };

	/**
     */
	public abstract void setCardNumber(String number) throws UsatException;

	/**
     */
	public abstract String getCardNumber();

	/**
     */
	public abstract void setCVV(String cvv);

	/**
     */
	public abstract String getCVV();

	/**
     */
	public abstract void setExpirationMonth(String month);

	/**
     */
	public abstract String getExpirationMonth();

	/**
     */
	public abstract void setExpirationYear(String year);

	/**
     */
	public abstract String getExpirationYear();

	/**
     */
	public abstract void setNameOnCard(String name);

	/**
     */
	public abstract String getNameOnCard();

	public abstract String getAuthCode();

	public abstract void setAuthCode(String auth);

	public abstract String getAuthDate();

	public abstract boolean wasAuthorizedSuccessfully();

	public abstract void setAuthorizedSuccessfully(boolean status);

	public String getExpirationDateForPersistence();

	public String getCreditCardTypeCode() throws UsatException;
}
