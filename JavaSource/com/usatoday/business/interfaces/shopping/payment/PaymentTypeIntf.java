package com.usatoday.business.interfaces.shopping.payment;

public interface PaymentTypeIntf {

	public static String INVOICE_PAYMENT_TYPE = "B";

	/**
     */
	public abstract String getType();

	/**
     */
	public abstract String getLabel();

	/**
     */
	public abstract String getDescription();

	/**
     */

	public abstract boolean requiresPaymentProcessing();
}
