package com.usatoday.business.interfaces.shopping;

import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;

public interface RenewalOrderItemIntf extends SubscriptionOrderItemIntf {

	public abstract String getRenewalTrackingCode();

	public abstract String getExistingDeliveryMethod();

	public abstract SubscriberAccountIntf getAccount();

	public abstract void setAccount(SubscriberAccountIntf account);
}
