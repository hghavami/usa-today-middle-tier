/*
 * Created on Apr 24, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.shopping;

/**
 * @author aeast
 * @date Apr 24, 2006
 * @class CCLogIntf
 * 
 *        This interface represents a Credit Card Log object.
 * 
 */
public interface CCLogIntf {

	/**
	 * @return
	 */
	public String getAmount();

	/**
	 * @return
	 */
	public String getCCNum();

	/**
	 * @return
	 */
	public String getCcType();

	/**
	 * @return
	 */
	public String getEmail();

	/**
	 * @return
	 */
	public String getExpMonth();

	/**
	 * @return
	 */
	public String getExpYear();

	/**
	 * @return
	 */
	public String getFirstName();

	/**
	 * @return
	 */
	public String getLastName();

	/**
	 * @return
	 */
	public String getPubCode();

	/**
	 * @return
	 */
	public String getResponseCode();

	/**
	 * @return
	 */
	public String getResponseMessage();

	/**
	 * @return
	 */
	public String getZip();

	/**
	 * @return
	 */
	public String getAuthCode();

	/**
	 * @return
	 */
	public String getVendorTranID();

	/**
	 * @return
	 */
	public String getAvsRespCode();

	/**
	 * @return
	 */
	public String getAvsRespMessage();

	public String getOrderID();

	public String getClientIPAddress();

	public String getCVVResponseCode();

	public String getCVVResponseMessage();

	public String getExternalKey();

	public String getReasonCode();

	public String getRequestType();
}
