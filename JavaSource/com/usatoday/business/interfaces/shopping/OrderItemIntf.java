package com.usatoday.business.interfaces.shopping;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.ProductIntf;

public interface OrderItemIntf {

	/**
	 * 
	 * @param product
	 *            The product object
	 */
	public abstract void setProduct(ProductIntf product);

	/**
	 * 
	 * @return The quanitity of this item
	 */
	public abstract int getQuantity();

	/**
	 * @param qty
	 *            The quantity desired.
	 */
	public abstract void setQuantity(int qty);

	/**
	 * @return The product desired
	 */
	public abstract ProductIntf getProduct();

	/**
	 * @return The Subtotal for this item (quantity * price);
	 */
	public abstract double getSubTotal() throws UsatException;

	/**
	 * @return The total for this cart item (quantity * price) + tax;
	 */
	public abstract double getTotal() throws UsatException;

	/**
	 * 
	 * @return The cart identifier
	 */
	public abstract Long getItemID();

	/**
	 * @return Flag indicating if this is a gift item
	 */
	public abstract boolean isGiftItem();

	/**
	 * @param The
	 *            gift flat True if gift, false if not a gift
	 */
	public abstract void setGiftItem(boolean isGift);

	public abstract void setDeliveryZip(String zip);

	public abstract String getDeliveryZip();

	/**
	 * 
	 * @param deliveryZip
	 * @return The tax amount for this order item
	 */
	public abstract double getTaxAmount(String deliveryZip) throws UsatException;

}
