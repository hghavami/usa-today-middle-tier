package com.usatoday.business.interfaces.shopping;

import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.shopping.payment.PaymentMethodIntf;

public interface ShoppingCartIntf {

	/**
     */
	public abstract void addItem(OrderItemIntf item);

	/**
     */
	public abstract void removeItem(OrderItemIntf item);

	/**
     */
	public abstract void removeItem(Long itemID);

	/**
     */
	public abstract double getSubTotal() throws UsatException;

	/**
     */
	public abstract double getSalesTax();

	/**
     */
	public abstract double getTotal() throws UsatException;

	/**
	 */
	public abstract Collection<OrderItemIntf> getItems();

	/**
     */
	public abstract void setOwningCustomer(CustomerIntf cusotmer);

	/**
     */
	public abstract CustomerIntf getOwningCustomer();

	public abstract void setBillingContact(ContactIntf contact);

	/**
     */
	public abstract void setPaymentMethod(PaymentMethodIntf method);

	public abstract PaymentMethodIntf getPaymentMethod();

	/**
     */
	public abstract void setDeliveryContact(ContactIntf contact);

	/**
     * 
     *
     */
	public abstract void clearItems();

	public abstract boolean isReadyForCheckOut();

	public abstract ContactIntf getDeliveryContact();

	public abstract ContactIntf getBillingContact();

	public abstract String getCheckOutErrorMessage();

	public abstract void setCheckOutErrorMessage(String message);

	public abstract boolean isCheckoutInProcess();

	public abstract void setCheckoutInProcess(boolean checkoutInProcess);

	public String getClientIPAddress();

	public void setClientIPAddress(String ip);

}
