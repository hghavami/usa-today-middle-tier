/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.shopping;

import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.shopping.payment.PaymentMethodIntf;
import com.usatoday.businessObjects.customer.EmailRecordBO;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class OrderIntf
 * 
 *        This class represents a placed order. It should be created once a customer checks out.
 * 
 */
public interface OrderIntf {

	public static final String[] ORDER_FULFILLMENT_STATUS_STRING = { "In Process", "Unshipped", "Ready For Shipping", "Shipped",
			"Cancelled" };
	public static final String[] ORDER_PAYMENT_STATUS_STRING = { "Authorized", "Authorization Pending", "Unbilled", "Billed",
			"Paid", "Refunded" };
	public static final int PAYMENT_AUTHORIZED = 0;
	public static final int PAYMENT_AUTH_PENDING = 1;
	public static final int PAYMENT_UNBILLED = 2;
	public static final int PAYMENT_BILLED = 3;
	public static final int PAYMENT_PAID = 4;
	public static final int PAYMENT_REFUNDED = 5;
	public static final int FULFILLMENT_IN_PROCESS = 0;
	public static final int FULFILLMENT_UNSHIPPED = 1;
	public static final int FULFILLMENT_SHIP_READY = 2;
	public static final int FULFILLMENT_SHIPPED = 3;
	public static final int FULFILLMENT_CANCELLED = 4;

	public abstract String getOrderID();

	public abstract DateTime getDateTimePlaced();

	public abstract Collection<OrderItemIntf> getOrderedItems();

	public abstract ContactIntf getDeliveryContact();

	public abstract ContactIntf getBillingContact();

	public abstract boolean isBillingSameAsDelivery();

	public abstract String getOrderFulfillmentStatusString();

	public abstract String getOrderPaymentStatusString();

	public abstract int getOrderFulfillmentStatus();

	public abstract int getOrderPaymentStatus();

	public abstract PaymentMethodIntf getPaymentMethod();

	public abstract String getAuthCode();

	public abstract void sendSubscriptionConfirmationEmail() throws UsatException;

	public abstract double getAmountCharged();

	public abstract double getTaxAmount();

	public abstract double getTotal();

	public abstract double getSubTotal();

	public Collection<EmailRecordBO> getSavedEmailRecords();

	public void addSavedEmailRecord(EmailRecordBO rec);
}
