/*
 * Created on Mar 4, 2011
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.customer;

import java.sql.Timestamp;

/**
 * @author hghavami
 * @date Mar 4, 2011
 * @class AdditionalAddressBO
 * 
 *        This class contains all fields required for the change delivery address screen.
 * 
 */

public interface AdditionalDeliveryAddressIntf {

	public int getId();

	public String getCurrAcctNum();

	public String getFirstName();

	public String getLastName();

	public String getFirmName();

	public String getStreetAddress();

	public String getAptDeptSuite();

	public String getAddlAddr1();

	public String getCity();

	public String getState();

	public String getZip();

	public String getHomePhone();

	public String getBusPhone();

	public Timestamp getInsertTimeStamp();

	public int getEmailID();

	public void setId(int id);
}
