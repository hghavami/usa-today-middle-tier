package com.usatoday.business.interfaces.customer;

import org.joda.time.DateTime;

public interface TrialCustomerTOIntf {
	public long getID();

	public void setID(long id);

	public String getPubCode();

	public void setPubCode(String pub);

	public DateTime getEndDate();

	public void setEndDate(DateTime endDate);

	public DateTime getStartDate();

	public void setStartDate(DateTime start);

	public DateTime getInsertedDate();

	public void setInsertedDate(DateTime inserted);

	public String getKeyCode();

	public void setKeyCode(String keyCode);

	public String getPartnerID();

	public void setPartnerID(String pid);

	public String getClubNumber();

	public void setClubNumber(String number);

	public boolean getIsActive();

	public void setIsActive(boolean activeFlag);

	public String getEmail();

	public void setEmail(String email);

	public String getPassword();

	public void setPassword(String pwd);

	public String getFirstName();

	public void setFirstName(String name);

	public String getLastName();

	public void setLastName(String name);

	public String getCompanyName();

	public void setCompanyName(String companyName);

	public String getAddress1();

	public void setAddress1(String addr1);

	public String getAddress2();

	public void setAddress2(String addr2);

	public String getApartmentSuite();

	public void setApartmentSuite(String apt);

	public String getAdditionalAddress();

	public void setAdditionalAddress(String addlAddress);

	public String getCity();

	public void setCity(String city);

	public String getState();

	public void setState(String state);

	public String getZip();

	public void setZip(String zip);

	public String getPhone();

	public void setPhone(String phone);

	public boolean getIsSubscribed();

	public void setIsSubscribed(boolean subscribedFlag);

	public String getClientIP();

	public void setClientIP(String ip);

	public String getNcsRepID();

	public void setNcsRepID(String id);

	public String getConfirmationEmailSent();

	public void setConfirmationEmailSent(String flag);
}
