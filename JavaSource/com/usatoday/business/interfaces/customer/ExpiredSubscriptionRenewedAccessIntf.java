/*
 * Created on Apr 24, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.customer;

import java.sql.Timestamp;

/**
 * @author hghavami
 * @date July 7, 2006
 * @class CustomerInterimInfoIntf
 * 
 *        This interface represents a customer interim object.
 * 
 */
public interface ExpiredSubscriptionRenewedAccessIntf {

	/**
	 * @return Returns the email.
	 */
	public String getEmail();

	/**
	 * @return Returns the firmName.
	 */
	public String getFirmName();

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName();

	/**
	 * @return Returns the insertedTimeStamp.
	 */
	public Timestamp getInsertedTimeStamp();

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName();

	/**
	 * @return Returns the orderID.
	 */
	public long getOrderID();

	/**
	 * @return Returns the publication.
	 */
	public String getPublication();

	/**
	 * @return Returns the startDate.
	 */
	public String getAccountNum();

	public void setOrderID(long orderID);

	public static final int BATCH_ERROR = 4;
	public static final int BATCH_IN_PROGRESS = 1;
	public static final int READY_FOR_BATCH = 0;
	public static final int READY_FOR_DELETION = 2;
}
