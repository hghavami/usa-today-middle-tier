/*
 * Created on Apr 24, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.customer;

import java.sql.Timestamp;

/**
 * @author hghavami
 * @date July 7, 2006
 * @class EeditionAccessLogIntf
 * 
 *        This interface represents a customer interim object.
 * 
 */
public interface EeditionAccessLogIntf {

	public long getTransID();

	public String getPublication();

	public String getAccountNum();

	public String getClientIP();

	public Timestamp getDateTime();

	public String getEmailAddress();

	public String getTrialAccess();

	public void setTransID(long transID);
}
