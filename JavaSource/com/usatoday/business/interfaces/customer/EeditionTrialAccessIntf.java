/*
 * Created on Apr 24, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.customer;

import java.sql.Timestamp;

/**
 * @author hghavami
 * @date July 7, 2006
 * @class EeditionAccessLogIntf
 * 
 *        This interface represents a customer interim object.
 * 
 */
public interface EeditionTrialAccessIntf {

	public String getActive();

	public String getAdditionalAddress();

	public String getAddress1();

	public String getAddress2();

	public String getAptSuite();

	public String getCity();

	public String getClientIP();

	public String getCompanyName();

	public String getCountry();

	public String getEmailAddress();

	public Timestamp getEndDate();

	public String getFirstName();

	public Timestamp getInsertDateTime();

	public String getKeyCode();

	public String getLastName();

	public String getPartnerID();

	public String getPartnerNumber();

	public String getPassword();

	public String getPhone();

	public String getPubCode();

	public String getRepID();

	public Timestamp getStartDate();

	public String getState();

	public String getSubscribed();

	public long getTransID();

	public String getZip();

	public void setTransID(long transID);
}
