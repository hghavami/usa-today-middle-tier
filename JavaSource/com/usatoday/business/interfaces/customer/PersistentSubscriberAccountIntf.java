/*
 * Created on Apr 12, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.customer;

/**
 * @author aeast
 * @date Apr 12, 2006
 * @class PersistentSubscriberAccountIntf
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public interface PersistentSubscriberAccountIntf extends SubscriberAccountIntf {
	public String getBillingAddress1();

	public String getBillingAddress2();

	public String getBillingCity();

	public String getBillingFirmName();

	public String getBillingFirstName();

	public String getBillingHalfUnitNum();

	public String getBillingLastName();

	public String getBillingState();

	public String getBillingStreetDir();

	public String getBillingStreetName();

	public String getBillingStreetPostDir();

	public String getBillingStreetType();

	public String getBillingSubUnitCode();

	public String getBillingSubUnitNum();

	public String getBillingUnitNum();

	public String getBillingZip();

	public String getBusinssPhone();

	public String getCity();

	public String getFirmName();

	public String getFirstName();

	public String getHalfUnitNum();

	public String getHomePhone();

	public String getLastName();

	public String getState();

	public String getStreetDir();

	public String getStreetName();

	public String getStreetPostDir();

	public String getStreetType();

	public String getSubUnitCode();

	public String getSubUnitNum();

	public String getUnitNum();

	public String getZip();

	public String getBillingHomePhone();

	public String getBillingBusinessPhone();

	public String getAddlAddr1();

	public String getRenewalTypeAlpha();

	public String getDonationCode();

	public String getRateCode();

	public String getLastStopCode();
}
