package com.usatoday.business.interfaces.customer;

import java.util.Date;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;

public interface SubscriberAccountIntf {

	/**
     */
	public abstract String getPubCode();

	/**
     */
	public abstract String getAccountNumber();

	/**
     */
	public abstract String getDeliveryMethod();

	public abstract String getFrequencyOfDeliveryCode();

	/**
     */
	public abstract Double getBalance();

	/**
     */
	public abstract ContactIntf getDeliveryContact();

	/**
     */
	public abstract ContactIntf getBillingContact();

	/**
     */
	public abstract String getKeyCode();

	/**
     */
	public abstract int getNumberOfPapers();

	/**
     */
	public abstract Date getLastPaymentReceivedDate();

	/**
     */
	public abstract Date getStartDate();

	/**
     */
	public abstract Date getExpirationDate();

	/**
     */
	public abstract boolean isActive();

	/**
     */
	public abstract boolean isOnEZPay();

	public abstract OfferIntf getDefaultRenewalOffer() throws UsatException;

	public abstract boolean isSubscriptionSuspended();

	/**
     */
	public abstract boolean isOneTimeBill();

	// hd-cons109
	// public abstract boolean isShowDeliveryAlert();

	public abstract boolean isBillingSameAsDelivery();

	public abstract boolean isDonatingToNIE();

	public abstract String getRenewalTypeAlpha();

	public abstract String getDonationCode();

	public abstract String getCreditCardNum();

	public abstract String getCreditCardType();

	public abstract String getCreditCardExp();

	public abstract String getCreditCardExpMonth();

	public abstract String getCreditCardExpYear();

	public SubscriptionProductIntf getProduct();

	public boolean isExpiredSubscriptionAccessRenewed();

	public abstract String getTransRecType();
}
