package com.usatoday.business.interfaces.customer;

import java.util.Collection;

import com.usatoday.UsatException;

public interface TrialCustomerIntf {
	public static int MAX_TRIAL_INSTANCES_PER_EMAIL = 5;

	public String getEmailAddress();

	public void setEmailAddress(String eAddress);

	public String getPassword();

	public void setPassword(String pWord);

	/**
	 * Saves all trial instances for this user.
	 * 
	 * @throws UsatException
	 */
	public void save() throws UsatException;

	/**
	 * 
	 * @return All trial instances for this email address
	 */
	public Collection<TrialInstanceIntf> getAllTrialInstances();

	/**
	 * Add a new trial to a customer
	 * 
	 * @param newTrial
	 */
	public void addTrialInstance(TrialInstanceIntf newTrial) throws UsatException;

	/**
	 * get the current trial instance (most people will have a single instance)
	 * 
	 * @return
	 */
	public TrialInstanceIntf getCurrentInstance();

	/**
	 * 
	 * @param instance
	 * @return true if instance exists
	 */
	public boolean setCurrentInstance(TrialInstanceIntf instance);

	/**
	 * 
	 * @param id
	 * @return true if instance exists
	 */
	public boolean setCurrentInstance(String partnerID);

}
