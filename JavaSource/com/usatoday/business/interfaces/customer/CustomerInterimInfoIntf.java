/*
 * Created on Apr 24, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.customer;

import java.sql.Timestamp;

/**
 * @author hghavami
 * @date July 7, 2006
 * @class CustomerInterimInfoIntf
 * 
 *        This interface represents a customer interim object.
 * 
 */
public interface CustomerInterimInfoIntf {

	/**
	 * @return Returns the address1.
	 */
	public String getAddress1();

	/**
	 * @return Returns the address2.
	 */
	public String getAddress2();

	/**
	 * @return Returns the billAddress1.
	 */
	public String getBillAddress1();

	/**
	 * @return Returns the billAddress2.
	 */
	public String getBillAddress2();

	/**
	 * @return Returns the billCity.
	 */
	public String getBillCity();

	/**
	 * @return Returns the billFirmName.
	 */
	public String getBillFirmName();

	/**
	 * @return Returns the billFirstName.
	 */
	public String getBillFirstName();

	/**
	 * @return Returns the billLastName.
	 */
	public String getBillLastName();

	/**
	 * @return Returns the billPhone.
	 */
	public String getBillPhone();

	/**
	 * @return Returns the billState.
	 */
	public String getBillState();

	/**
	 * @return Returns the delvMethod.
	 */
	public String getDelvMethod();

	/**
	 * @return Returns the billZip.
	 */
	public String getBillZip();

	/**
	 * @return Returns the city.
	 */
	public String getCity();

	/**
	 * @return Returns the contestCode.
	 */
	public String getContestCode();

	/**
	 * @return Returns the email.
	 */
	public String getEmail();

	/**
	 * @return Returns the firmName.
	 */
	public String getFirmName();

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName();

	/**
	 * @return Returns the homePhone.
	 */
	public String getHomePhone();

	/**
	 * @return Returns the insertedTimeStamp.
	 */
	public Timestamp getInsertedTimeStamp();

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName();

	/**
	 * @return Returns the orderID.
	 */
	public long getOrderID();

	public int getProcessState();

	/**
	 * @return Returns the promoCode.
	 */
	public String getPromoCode();

	/**
	 * @return Returns the publication.
	 */
	public String getPublication();

	/**
	 * @return Returns the rateCode.
	 */
	public String getRateCode();

	/**
	 * @return Returns the srcOrdCode.
	 */
	public String getSrcOrdCode();

	/**
	 * @return Returns the startDate.
	 */
	public String getStartDate();

	/**
	 * @return Returns the State.
	 */
	public String getState();

	/**
	 * @return Returns the subsAmount.
	 */
	public String getSubsAmount();

	/**
	 * @return Returns the subsDur.
	 */
	public String getSubsDur();

	/**
	 * @return Returns the zip.
	 */
	public String getZip();

	public void setOrderID(long orderID);

	/**
	 * @return Returns the homePhone.
	 */
	public String getWorkPhone();

	public static final int BATCH_ERROR = 4;
	public static final int BATCH_IN_PROGRESS = 1;
	public static final int READY_FOR_BATCH = 0;
	public static final int READY_FOR_DELETION = 2;
}
