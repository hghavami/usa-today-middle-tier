package com.usatoday.business.interfaces.customer;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.PersistentAddressIntf;
import com.usatoday.business.interfaces.UIAddressIntf;

public interface ContactIntf {

	/**
     */
	public abstract String getLastName();

	/**
     */
	public abstract String getFirstName();

	/**
     */
	public abstract String getFirmName();

	/**
     */
	public abstract UIAddressIntf getUIAddress();

	/**
     */
	public abstract PersistentAddressIntf getPersistentAddress();

	/**
     */
	public abstract String getHomePhone();

	/**
     */
	public abstract String getBusinessPhone();

	/**
     */
	public abstract String getEmailAddress();

	public abstract boolean validateContact() throws UsatException;

	public abstract boolean isAddressVerified();

	public abstract boolean isQuestionableAddress();

	public String getHomePhoneNumberFormatted();

	public String getWorkPhoneNumberFormatted();

	public String getPassword();

	public boolean validateContactEmailAddress() throws UsatException;

}
