package com.usatoday.business.interfaces.customer;

import java.util.HashMap;

public interface CustomerIntf {

	public abstract SubscriberAccountIntf getCurrentAccount();

	public abstract void setCurrentAccount(String accountNumber);

	public abstract void setAdditionalDeliveryAddress(HashMap<String, AdditionalDeliveryAddressIntf> additionalDeliveryAddress);

	public HashMap<String, AdditionalDeliveryAddressIntf> getAdditionalDeliveryAddress();

	/**
     */
	public abstract int getNumberOfAccounts();

	public boolean getIsElectronicEditionOnlyCustomer();

	public abstract HashMap<String, SubscriberAccountIntf> getAccounts();

	public abstract EmailRecordIntf getCurrentEmailRecord();

	/**
     */
	public abstract HashMap<String, EmailRecordIntf> getEmailRecords();

	public abstract boolean hasAccountWithNoAccountNumber();

	public boolean getHasActiveAccounts();

	public boolean getHasFutureStarts();
}
