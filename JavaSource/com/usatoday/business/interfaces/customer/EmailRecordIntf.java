package com.usatoday.business.interfaces.customer;

import java.util.Date;

public interface EmailRecordIntf {

	/**
     */
	public abstract String getPassword();

	/**
     */
	public abstract int getSerialNumber();

	/**
     */
	public abstract void setOrderID(String orderID);

	/**
     */
	public abstract String getOrderID();

	/**
     */
	public abstract String getAccountNumber();

	/**
     */
	public abstract Date getLastUpdated();

	public abstract boolean isActive();

	/**
     */
	public abstract void setActive(boolean activeFlag);

	/**
     */
	public abstract boolean isGift();

	/**
     */
	public abstract void setGift(boolean giftFlag);

	/**
     */
	public abstract String getPubCode();

	/**
     */
	public abstract void setPubCode(String pubCode);

	/**
     */
	public abstract String getEmailAddress();

	/**
     */
	public abstract void setEmailAddress(String email);

	/**
     */
	public abstract void setPassword(String password);

	/**
     */
	public abstract String getPermStartDate();

	/**
     */
	public abstract void setPermStartDate(String date);

	/**
     */
	public abstract String getZip();

	/**
     */
	public abstract void setZip(String zip);

	/**
     */
	public abstract void setAccountNumber(String accountNumber);

	/**
     * 
     */
	public void setConfirmationEmailSent(String c);

	public String getConfirmationEmailSent();

	public boolean isStartDateInFuture();
	public String getFireflyUserId();
	public String getFirstName();
	public String getLastName();
	public String getAutoLogin();
	public String getSessionKey();
	
	public abstract void setFireflyUserId(String fireflyUserId);
	public abstract void setFirstName(String firstName);
	public abstract void setLastName(String lastName);
}
