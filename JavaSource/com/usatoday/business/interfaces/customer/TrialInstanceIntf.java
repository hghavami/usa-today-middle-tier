package com.usatoday.business.interfaces.customer;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.customer.ContactBO;

/**
 * This class represents an instance of a trial sign up, because one person could sign up for multiple trials.
 * 
 * @author aeast
 * 
 */
public interface TrialInstanceIntf {

	public long getID();

	public String getPubCode();

	public void setPubCode(String pubCode);

	public DateTime getStartDate();

	public void setStartDate(DateTime start);

	public DateTime getEndDate();

	public void setEndDate(DateTime endDate);

	public String getKeyCode();

	public void setKeyCode(String kCode);

	public ContactBO getContactInformation();

	public void setContactInformation(ContactBO contact);

	public TrialPartnerIntf getPartner();

	public void setPartner(TrialPartnerIntf partner);

	public String getClubNumber();

	public void setClubNumber(String number);

	public boolean getIsActive();

	public void save() throws UsatException;

	public SubscriptionProductIntf getProduct() throws UsatException;

	public boolean getIsTrialPeriodExpired();

	public boolean getIsSubscribed();

	public void setIsSubscribed(boolean subscribed);

	public boolean sendConfirmationEmail();

	public String getClientIP();

	public void setClientIP(String ip);

	public String getNcsRepID();

	public void setNcsRepID(String id);

	public String getConfirmationEmailSent();

	public void setConfirmationEmailSent(String flag);

	public String getTrialSubType();

	public void setTrialSubType(String trialSubType);

}
