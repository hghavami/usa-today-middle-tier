/*
 * Created on Feb 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces;

import java.io.Serializable;

import com.usatoday.UsatException;

/**
 * @author aeast
 * @date Feb 2, 2007
 * @class UserIntf
 * 
 *        This interface represents the base public interface to the User's of the Single Copy (Champion) Web Portal.
 * 
 */
public interface CreditCardInfoIntf extends Serializable {

	/**
	 * 
	 * @return user's password
	 */
	public String getCreditCardNewGift();

	public String getCreditCardNum();

	public String getCreditCardExp();

	/**
	 * 
	 * @param password
	 */
	public void setCreditCardNewGift(String creditCardNewGift) throws UsatException;

	public void setCreditCardNum(String creditCardNum) throws UsatException;

	public void setCreditCardExp(String creditCardExp) throws UsatException;

}
