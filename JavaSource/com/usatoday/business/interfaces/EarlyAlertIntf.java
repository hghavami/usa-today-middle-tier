/*
 * Created on Feb 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces;

import java.io.Serializable;

import com.usatoday.UsatException;

/**
 * 
 */
public interface EarlyAlertIntf extends Serializable {

	/**
	 * 
	 * @return user's password
	 */
	public String getEarlyAlert();

	/**
	 * 
	 * @param password
	 */
	public void setEarlyAlert(String earlyAlert) throws UsatException;

	public String getEarlyAlertDelay();
}
