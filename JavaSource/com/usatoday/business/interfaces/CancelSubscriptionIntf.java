package com.usatoday.business.interfaces;

import java.io.Serializable;

public interface CancelSubscriptionIntf extends Serializable {

	public String getCancelSubsReasonCode();

	public String getCancelSubsReasonDesc();

}
