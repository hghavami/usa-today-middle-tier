package com.usatoday.business.interfaces;

public interface UIAddressIntf extends PostalAddressIntf {

	/**
     */
	public abstract String getAddress1();

	/**
     */
	public abstract String getAptSuite();
}
