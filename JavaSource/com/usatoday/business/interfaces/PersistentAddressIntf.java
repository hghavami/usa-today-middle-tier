package com.usatoday.business.interfaces;

import com.usatoday.businessObjects.customer.UIAddressBO;

public interface PersistentAddressIntf extends PostalAddressIntf {

	/**
     */
	public abstract String getStreetDir();

	/**
     */
	public abstract String getHalfUnitNum();

	/**
     */
	public abstract String getStreetName();

	/**
     */
	public abstract String getStreetType();

	/**
     */
	public abstract String getStreetPostDir();

	/**
     */
	public abstract String getSubUnitCode();

	/**
     */
	public abstract String getSubUnitNum();

	/**
     */
	public abstract String getUnitNum();

	public UIAddressBO convertToUIAddress();

	public abstract boolean isMilitaryAddress();

	public abstract boolean isGUIAddress();

}
