package com.usatoday.business.interfaces;

public interface PostalAddressIntf {

	/**
     */
	public abstract String getZip();

	/**
     */
	public abstract String getCity();

	/**
     */
	public abstract String getState();

	/**
     */
	public abstract String getAddress2();

	public abstract boolean isValidated();

	public abstract void setValidated(boolean valid);
}
