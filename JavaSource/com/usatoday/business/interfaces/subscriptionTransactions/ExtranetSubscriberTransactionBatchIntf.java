package com.usatoday.business.interfaces.subscriptionTransactions;

import java.util.Collection;

import com.usatoday.UsatException;

public interface ExtranetSubscriberTransactionBatchIntf {

	public int getCountOfNewTransactions() throws UsatException;

	public int getCountofTransactionsInProcessing() throws UsatException;

	public Collection<ExtranetSubscriberTransactionIntf> getNewTransactionsForProcessing() throws UsatException;

	// public void resetStatusToNew(Collection<ExtranetSubscriberTransactionIntf> trans) throws UsatException;
	public int setTransactionStateFromProcessingToProcessed() throws UsatException;

	public Collection<ExtranetSubscriberTransactionIntf> getTransactionsInBatch(String batchID) throws UsatException;

	public int setTransactionStateOfBatchToReProcessed(String batchID) throws UsatException;

	public int purgeTransactionsOlderThan(int numDays) throws UsatException;

}
