package com.usatoday.business.interfaces.subscriptionTransactions;

import org.joda.time.DateTime;

import com.usatoday.UsatException;

public interface ExtranetSubscriberTransactionIntf {

	public abstract String getTransactionRecType(); // TranRecTyp 01,02, etc..

	public abstract String getPubCode(); // PubCode

	public abstract String getAccountNum();

	public abstract String getTransactionDate(); // Trandate

	public abstract String getCode1ErrorCode(); // Code1ErrCode

	public abstract String getLastName(); // LastName

	public abstract String getFirstName(); // FirstName

	public abstract String getFirmName(); // FirmName

	public abstract String getAddAddr1(); // AddAddr1

	public abstract String getAddAddr2(); // AddAddr2

	public abstract String getUnitNum(); // UnitNum

	public abstract String getHalfUnitNum(); // HalfUnitNum

	public abstract String getStreetDir(); // StreetDir

	public abstract String getStreetName(); // StreetName

	public abstract String getStreetType(); // StreetTyp

	public abstract String getStreetPostDir(); // StreetPstDir

	public abstract String getSubUnitCode(); // SubUnitCode

	public abstract String getSubUnitNum(); // SubUnitNum

	public abstract String getCity(); // City

	public abstract String getState(); // State

	public abstract String getZip5(); // Zip5

	public abstract String getHomePhone(); // HomePhone

	public abstract String getBusinessPhone(); // BusPhone

	public abstract String getTransactionType(); // TranType 8 for new order?

	public abstract String getTransactionCode(); // TranCode

	public abstract String getBillingLastName(); // BillLastName

	public abstract String getBillingFirstName(); // BillFirstName

	public abstract String getBillingFirmName(); // BillFirmName

	public abstract String getBillingAddress1(); // BillAddr1

	public abstract String getBillingAddress2(); // BillAddr2

	public abstract String getBillingUnitNum(); // BillUnitNum

	public abstract String getBillingHalfUnitNum(); // BillHalfUnitNum

	public abstract String getBillingStreetDir(); // BillStreetDir

	public abstract String getBillingStreetName(); // BillStreetName

	public abstract String getBillingStreetType(); // BillStreetType

	public abstract String getBillingStreetPostDir(); // BillStreetPostDir

	public abstract String getBillingSubUnitCode(); // BillSubUnitCode

	public abstract String getBillingSubUnitNum(); // BillSubUnitNum

	public abstract String getBillingCity(); // BillCity

	public abstract String getBillingState(); // BillState

	public abstract String getBillingZip5(); // BillZip5

	public abstract String getBillingHomePhone(); // BillHomePhone

	public abstract String getBillingBusinessPhone(); // BillBusPhone

	public abstract String getSubscriptionDuration(); // SubsDur

	public abstract String getCreditCardType(); // CCType

	public abstract String getCreditCardNumber(); // CCNum

	public abstract String getCreditCardExpirationDate(); // CCExpireDt

	public abstract String getEffectiveDate(); // EffDate1

	public abstract String getPromoCode(); // PromoCode

	public abstract String getComment(); // Comment

	public abstract String getTtlMktCov(); // TtlMktCov

	public abstract String getSubscriptionAmount(); // SubsAmount

	public abstract String getRejComment(); // RejComm

	public abstract String getAutoRegComm(); // AutoRegComm

	public abstract String getPrintFlag(); // PrintFlag

	public abstract String getFiller(); // Filler

	public abstract String getSrcOrdCode(); // SrcOrdCode

	public abstract String getContestCode(); // ContestCode

	public abstract String getDeliveryPreference(); // DelPref

	public abstract String getOneTimeBill(); // OneTimeBill

	public abstract String getRateCode(); // RateCode

	public abstract String getPerpCCFlag(); // PerpCCFlag

	public abstract String getBillChargePd(); // BillChargePd

	public abstract String getNewAddlAddr1(); // NewAddlAddr1

	public abstract String getNewAddlAddr2(); // NewAddlAddr2

	public abstract String getCreditCardAuthCode(); // CCAuthCode

	public abstract String getCreditCardAuthDate(); // CCAuthDate

	public abstract String getEmailType(); // EmailType

	public abstract String getEmailAddress(); // EmailAddress

	public abstract String getBadSubAddress(); // BadSubAddress

	public abstract String getBadTrnGftAddress(); // BadTrnGftAddress

	public abstract String getCreditCardBatchProcess(); // CCBatchProc

	public abstract String getStatus(); // Status

	public abstract String getNumPaper(); // NumPaper

	public abstract String getPayAmount(); // PayAmount

	public abstract String getPremium(); // Premium

	public abstract String getClubNum(); // ClubNum
	// New delivery address information

	public abstract String getNewLastName();

	public abstract String getNewFirstName();

	public abstract String getNewFirmName();

	public abstract String getNewUnitNum();

	public abstract String getNewHalfUnitNum();

	public abstract String getNewStreetDir();

	public abstract String getNewStreetName();

	public abstract String getNewStreetType();

	public abstract String getNewStreetPostDir();

	public abstract String getNewSubUnitNum();

	public abstract String getNewSubUnitCode();

	public abstract String getNewCity();

	public abstract String getNewState();

	public abstract String getNewZip();

	public abstract String getNewPhoneNum();

	public abstract String getNewBusPhoneNum();

	public abstract String getDeleteGiftPayer();

	public abstract String getMicrTranCode();

	public abstract String getCreditDays1();

	public abstract String getCreditDays3();

	public abstract String getContactCustomer();

	public abstract String getExistingDeliveryMethod();

	public abstract DateTime getInsertTimestamp();

	public abstract DateTime getUpdateTimestamp();

	public abstract String getBatchID();

	public abstract long getID();

	public String getTransactionCSVExportString() throws UsatException;
}
