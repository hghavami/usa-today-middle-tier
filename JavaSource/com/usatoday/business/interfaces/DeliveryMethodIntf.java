/*
 * Created on Feb 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces;

import java.io.Serializable;

import com.usatoday.UsatException;

/**
 * 
 */
public interface DeliveryMethodIntf extends Serializable {

	/**
	 * 
	 * @return user's password
	 */
	public String getDeliveryMethod();

	/**
	 * 
	 * @param password
	 */
	public void setDeliveryMethod(String deliveryMethod) throws UsatException;

	public String getDeliveryDayDelay();
}
