/*
 * Created on Apr 13, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.products;

import java.util.Collection;

/**
 * @author aeast
 * @date Apr 13, 2006
 * @class SubscriptionOfferIntf
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public interface SubscriptionOfferIntf extends OfferIntf {

	public String getKeyCode();

	public Collection<SubscriptionTermsIntf> getRenewalTerms();

	public boolean isBillMeAllowed();

	public boolean isForceBillMe();

	public double getDailyRate();

	public String getPercentOffBasePrice(double basePrice);

	public boolean isClubNumberRequired();

	public abstract SubscriptionTermsIntf getRenewalTerm(String termAsString);

	public SubscriptionProductIntf getSubscriptionProduct();
}
