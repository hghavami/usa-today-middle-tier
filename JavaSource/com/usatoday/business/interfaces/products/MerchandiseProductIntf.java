package com.usatoday.business.interfaces.products;

public interface MerchandiseProductIntf extends com.usatoday.business.interfaces.products.ProductIntf {

	/**
		 */
	public abstract int getInventory();

}
