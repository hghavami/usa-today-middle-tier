/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.products;

import org.joda.time.DateTime;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class PersistentProductIntf
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public interface PersistentProductIntf extends ProductIntf {
	public DateTime getStartDate();

	public DateTime getEndDate();

	public int getInventory();

	public int getInitialInventory();

	public DateTime getInsertTimestamp();

	public DateTime getUpdateTimestamp();

	public void setUpdateTimestamp(DateTime ts);

	public int getFulfillmentMethod();

	public int getSupplierID();

	public int getHoldDaysDelay();

	public String getDefaultKeycode();

	public String getExpiredOfferKeycode();
	
	public String getDefaultRenewalKeycode();

	public int getMaxDaysInFutureBeforeFulfillment();
}
