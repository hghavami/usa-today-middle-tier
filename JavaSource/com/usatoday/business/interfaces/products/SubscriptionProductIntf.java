package com.usatoday.business.interfaces.products;

import org.joda.time.DateTime;

import com.usatoday.UsatException;

public interface SubscriptionProductIntf extends com.usatoday.business.interfaces.products.ProductIntf {

	/**
     */
	public abstract SubscriptionOfferIntf getOffer(String keycode) throws UsatException;

	/**
     */
	public abstract SubscriptionOfferIntf getDefaultOffer() throws UsatException;

	public abstract void applyTerm(SubscriptionTermsIntf term);

	public abstract void applyOffer(OfferIntf offer);

	public abstract double getBasePrice() throws UsatException;

	public DateTime getEarliestPossibleStartDate();

	public DateTime getLatestPossibleStartDate();

	public DateTime getEarliestPossibleHoldStartDate();

	public boolean isHoldDateValid(DateTime requestedHoldDate) throws UsatException;

	public DateTime getFirstValidStartDateAfter(DateTime requestedDate);

	public DateTime getFirstValidStartDateBefore(DateTime requestedDate);

	public boolean isStartDateValid(DateTime requestedStartDate) throws UsatException;
}
