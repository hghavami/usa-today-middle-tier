/*
 * Created on Apr 14, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.products;

/**
 * @author aeast
 * @date Apr 14, 2006
 * @class RenewalOfferIntf
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public interface RenewalOfferIntf extends OfferIntf {

	public String getRateCode();

	public String getRelatedRateCode();

	public String getKeyCode();
}
