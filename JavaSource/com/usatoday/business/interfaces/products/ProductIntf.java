package com.usatoday.business.interfaces.products;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.suppliers.SupplierIntf;

public interface ProductIntf {

	public static final int PRINT_DELIVERY = 0;
	public static final int ELECTRONIC_DELIVERY = 1;
	public static final int THIRD_PARTY_DELIVERY = 2;
	public static final int ONE_TIME_DELIVERY = 3;

	public static final int SUBSCRIPTION_PRODUCT = 0;
	public static final int ONE_TIME_PURCHASE_PRODUCT = 1;

	/**
     */
	public abstract String getProductCode();

	/**
     */
	public abstract String getName();

	public abstract String getDescription();

	public String getDefaultKeycode();

	public String getExpiredOfferKeycode();
	
	public String getDefaultRenewalKeycode();

	/**
     */
	public abstract String getDetailedDescription();

	/**
	 * 
	 * @return
	 */
	public abstract boolean isTaxable();

	/**
	 * 
	 * @return
	 */
	public String getTaxRateCode();

	/**
     */
	public abstract int getID();

	/**
     */
	public abstract boolean isAvailable();

	/**
     */
	public abstract double getUnitPrice() throws UsatException;

	public abstract int updateInventory(int delta);

	public abstract int getMinQuantityPerOrder();

	public abstract int getMaxQuantityPerOrder();

	public abstract boolean isElectronicDelivery();

	public abstract int getFulfillmentMethod();

	public abstract String getBrandingPubCode();

	public abstract SupplierIntf getSupplier();

	public abstract String getCustomerServicePhone();

	public abstract int getMaxDaysInFutureBeforeFulfillment();

	public abstract int getHoldDaysDelay();

	public abstract int getProductType();

}
