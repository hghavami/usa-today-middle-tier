/*
 * Created on Apr 14, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.products;

import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.businessObjects.products.promotions.PromotionSet;

/**
 * @author aeast
 * @date Apr 14, 2006
 * @class OfferIntf
 * 
 * 
 * 
 */
public interface OfferIntf {

	public abstract String getKey();

	public abstract Collection<SubscriptionTermsIntf> getTerms();

	public abstract String getPubCode();

	public abstract SubscriptionTermsIntf getTerm(String termAsString);

	public abstract SubscriptionTermsIntf getRenewalTerm(String termAsString);

	public abstract SubscriptionTermsIntf findTermUsingTermLength(String termLength);

	public abstract PromotionSet getPromotionSet();

	public boolean isForceEZPay();

	public ProductIntf getProduct() throws UsatException;

}
