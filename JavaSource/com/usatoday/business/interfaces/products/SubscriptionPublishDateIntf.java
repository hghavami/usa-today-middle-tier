package com.usatoday.business.interfaces.products;

import org.joda.time.DateTime;

public interface SubscriptionPublishDateIntf {

	public String getPubCode();

	public DateTime getDate();

	public String getDayOfWeekName();

	public boolean isPublishedOnThisDay();
}
