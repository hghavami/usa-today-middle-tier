package com.usatoday.business.interfaces.products.logging;

import org.joda.time.DateTime;

public interface ElectronicEditionLogIntf {

	/**
	 * 
	 * @return The primary key
	 */
	public long getID();

	/**
	 * 
	 * @return The timestamp of when the user clicked to read
	 */
	public DateTime getAccessedTime();

	/**
	 * 
	 * @return The customer's account number
	 */
	public String getAccountNumber();

	/**
	 * 
	 * @return The publication the user is currently signed into
	 */
	public String getPubCode();

	/**
	 * 
	 * @return The person's user id
	 */
	public String getEmailAddress();

	/**
	 * 
	 * @return The IP address that the client used to access the system
	 */
	public String getClientIP();

	/**
	 * 
	 * @return true, if this was a trial edition access or false if not a trial.
	 */
	public boolean isTrialAccess();
}
