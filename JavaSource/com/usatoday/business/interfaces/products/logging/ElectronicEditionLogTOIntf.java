package com.usatoday.business.interfaces.products.logging;

import org.joda.time.DateTime;

public interface ElectronicEditionLogTOIntf extends ElectronicEditionLogIntf {
	public void setID(long id);

	public void setAccessedTime(DateTime dt);

	public void setAccountNumber(String acctNumber);

	public void setPubCode(String pub);

	public void setEmailAddress(String eAddress);

	public void setClientIP(String ip);

	public void setTrialAccess(boolean trial);

}
