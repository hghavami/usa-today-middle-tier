package com.usatoday.business.interfaces.products;

public interface SubscriptionTermsIntf {

	/**
     */
	public abstract String getDescription();

	/**
	 * 
	 * @deprecated should use get Duration instead
	 */
	@Deprecated
	public abstract String getDurationInWeeks();

	// New with Genesys API
	public abstract String getDuration();

	// New with Genesys API
	// W for weeks M for months
	public abstract String getDurationType();

	// New with Genesys API
	public abstract String getFODCode();

	/**
	 * 
	 * @return The term in the format [PUB]_[KEYCODE]_[RATE CODE]_[DURATION IN WEEKS]
	 */
	public abstract String getTermAsString();

	/**
     */
	public abstract double getPrice();

	public abstract String getPriceAsString();

	/**
     */
	public abstract double getDailyRate();

	/**
     */
	public abstract String getPiaRateCode();

	/**
     */
	public abstract String getRelatedRateCode();

	/**
     */
	public abstract String getRenewalRateCode();

	/**
     */
	public abstract String getRenewalPeriodLength();

	/**
	 * 
	 * @return
	 */
	public abstract boolean requiresEZPAY();

	public abstract String getPubCode();

	public abstract SubscriptionOfferIntf getParentOffer();
	
	public abstract String getDisclaimerText();

}
