/*
 * Created on May 5, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.products.promotions;

/**
 * @author aeast
 * @date May 5, 2006
 * @class PromotionIntf
 * 
 *        This class represents a read only interface to a promotional record.
 * 
 */
public interface PromotionIntf {
	public static final String DEFAULT_CAMPAIGN_KEYCODE = "00000";

	public static final String PRODUCT_IMAGE_1 = "PRODUCT_IMG_1";

	public static final String LANDING_PAGE_PROMO_TEXT = "HEADERMAIN";
	public static final String LANDING_PAGE_PROMO_IMAGE_1 = "LANDING_PAGE_IMAGE_1";
	public static final String LANDING_PAGE_PROMO_IMAGE_2 = "LANDING_PAGE_IMAGE_2";
	public static final String LANDING_PAGE_PROMO_IMAGE_3 = "LANDING_PAGE_IMAGE_3";
	public static final String LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90 = "LANDING_IMAGE_180x90";
	public static final String LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120 = "LANDING_IMAGE_180x120";
	public static final String TEMPLATE_NAV_PROMO_150x90 = "TEMPLATE_NAV_PROMO_150x90";
	public static final String TERMS_AND_CONDITIONS_TEXT = "PROMO_TERMS_COND_TEXT";

	public static final String DYNAMIC_NAVIGATION_ORDER_ENTRY = "DYNAMIC_NAV_OE";
	public static final String DYNAMIC_NAVIGATION_CUSTSERV = "DYNAMIC_NAV_CS";

	// payment settings
	public static final String CVV_REQUIRED_FOR_ORDER = "CVV_REQUIRED_FOR_ORDER";
	public static final String AVS_REQUIRED_FOR_ORDER = "AVS_REQUIRED_FOR_ORDER";

	// One Page Order Entry
	public static final String ONE_PAGE_DISCLAIMER_TEXT = "DISCLAIMER_TEXT";
	public static final String ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1 = "PROMO_ONE_PAGE_TEXT_1";
	public static final String ONE_PAGE_FORCE_EZPAY_TERMS_TEXT = "ONE_PAGE_FORCE_EZPAY_OFFER_TEXT";
	public static final String ALLOW_OFFER_OVERRIDE = "ALLOW_OFFER_OVERRIDE";
	public static final String EZPAY_FREE_WEEKS = "EZPAY_FREE_WEEKS";

	public static final String ONE_PAGE_RIGHT_COL_IMAGE_SPOT_1 = "ONE_PAGE_R1_IMAGE";
	public static final String ONE_PAGE_RIGHT_COL_HTML_SPOT_1 = "ONE_PAGE_R1_HTML";
	public static final String ONE_PAGE_RIGHT_COL_IMAGE_SPOT_2 = "ONE_PAGE_R2_IMAGE";
	public static final String ONE_PAGE_RIGHT_COL_HTML_SPOT_2 = "ONE_PAGE_R2_HTML";
	public static final String ONE_PAGE_RIGHT_COL_IMAGE_SPOT_3 = "ONE_PAGE_R3_IMAGE";
	public static final String ONE_PAGE_RIGHT_COL_VIDEO_POP_SPOT_1 = "ONE_PAGE_RV1_IMAGE";
	public static final String ONE_PAGE_RIGHT_COL_HTML_VIDEO_SPOT_1 = "ONE_PAGE_RV1_HTML";
	public static final String ONE_PAGE_LEFT_COL_IMAGE_SPOT_1 = "ONE_PAGE_L1_IMAGE";
	public static final String ONE_PAGE_LEFT_COL_IMAGE_SPOT_2 = "ONE_PAGE_L2_IMAGE";
	public static final String ONE_PAGE_LEFT_COL_VIDEO_POP_SPOT_1 = "ONE_PAGE_LV1_IMAGE";
	public static final String ONE_PAGE_LEFT_COL_HTML_VIDEO_SPOT_1 = "ONE_PAGE_LV1_HTML";
	public static final String ONE_PAGE_LEFT_COL_HTML_SPOT_1 = "ONE_PAGE_L1_HTML";
	public static final String ONE_PAGE_SCRIPT_1 = "ONE_PAGE_SCRIPT_1";
	public static final String ONE_PAGE_SCRIPT_2 = "ONE_PAGE_SCRIPT_2";
	public static final String ONE_PAGE_SCRIPT_3 = "ONE_PAGE_SCRIPT_3";
	public static final String ONE_PAGE_SCRIPT_4 = "ONE_PAGE_SCRIPT_4";
	public static final String ONE_PAGE_SCRIPT_5 = "ONE_PAGE_SCRIPT_5";
	public static final String ONE_PAGE_SCRIPT_TEALIUM = "ONE_PAGE_SCRIPT_TEALIUM";
	public static final String TERMS_DESCRIPTION_TEXT_EZPAY = "TERMS_DESCRIPTION_TEXT_EZPAY";
	public static final String TERMS_DESCRIPTION_TEXT = "TERMS_DESCRIPTION_TEXT";

	public static final String ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_1 = "ONE_PAGE_THANKYOU_R1_IMAGE";
	public static final String ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_1 = "ONE_PAGE_THANKYOU_R1_HTML";
	public static final String ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_2 = "ONE_PAGE_THANKYOU_R2_IMAGE";
	public static final String ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_2 = "ONE_PAGE_THANKYOU_R2_HTML";
	public static final String ONE_PAGE_THANK_YOU_LEFT_COL_IMAGE_SPOT_1 = "ONE_PAGE_THANKYOU_L1_IMAGE";
	public static final String ONE_PAGE_THANK_YOU_LEFT_COL_HTML_SPOT_1 = "ONE_PAGE_THANKYOU_L1_HTML";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_1 = "ONE_PAGE_THANKYOU_SCRIPT_1";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_2 = "ONE_PAGE_THANKYOU_SCRIPT_2";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_3 = "ONE_PAGE_THANKYOU_SCRIPT_3";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_4 = "ONE_PAGE_THANKYOU_SCRIPT_4";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_5 = "ONE_PAGE_THANKYOU_SCRIPT_5";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_6 = "ONE_PAGE_THANKYOU_SCRIPT_6";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_7 = "ONE_PAGE_THANKYOU_SCRIPT_7";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_8 = "ONE_PAGE_THANKYOU_SCRIPT_8";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_9 = "ONE_PAGE_THANKYOU_SCRIPT_9";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_10 = "ONE_PAGE_THANKYOU_SCRIPT_10";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_11 = "ONE_PAGE_THANKYOU_SCRIPT_11";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_12 = "ONE_PAGE_THANKYOU_SCRIPT_12";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_13 = "ONE_PAGE_THANKYOU_SCRIPT_13";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_14 = "ONE_PAGE_THANKYOU_SCRIPT_14";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_15 = "ONE_PAGE_THANKYOU_SCRIPT_15";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_16 = "ONE_PAGE_THANKYOU_SCRIPT_16";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_17 = "ONE_PAGE_THANKYOU_SCRIPT_17";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_18 = "ONE_PAGE_THANKYOU_SCRIPT_18";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_19 = "ONE_PAGE_THANKYOU_SCRIPT_19";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_20 = "ONE_PAGE_THANKYOU_SCRIPT_20";
	public static final String ONE_PAGE_THANKYOU_SCRIPT_TEALIUM = "ONE_PAGE_THANKYOU_SCRIPT_TEALIUM";
	
	// public static final String HEADER_TEXT_TERMS = "HEADER";
	// public static final String IMAGE_TERMS = "ORDER";
	public static final String IMAGE_COMPLETE = "COMPLETENEW";
	public static final String ACCOUNT_INFO_IMAGE_SPOT1 = "ACCOUNTINFO_IMAGE_SPOT1";
	public static final String ACCOUNT_INFO_IMAGE_SPOT2 = "ACCOUNTINFO_IMAGE_SPOT2";
	public static final String ACCOUNT_INFO_IMAGE_SPOT3 = "ACCOUNTINFO_IMAGE_SPOT3";
	public static final String MYPOINTS_NEW_SUB = "MYPOINTS_New";
	public static final String CREDIT_CARD = "CREDITCARD";
	public static final String EZPAY_PROMO = "AUTO_PAY";
	// public static final String IMAGE_TOP = "IMAGE_TOP";
	// public static final String IMAGE_LEFT = "IMAGE_LEFT";
	// public static final String IMAGE_RIGHT = "IMAGE_RIGHT";
	public static final String IMAGE_BOTTOM = "IMAGE_BOTTOM";
	// public static final String EZPAY_EMAIL_TEXT = "EZPAY_EM_TXT";
	// public static final String CONFIRM_EMAIL_TEXT = "CONFIRM_EM_TXT";
	// public static final String AB_TEST_A = "AB_SPLIT_A_PATH";
	// public static final String AB_TEST_B = "AB_SPLIT_B_PATH";
	public static final String FORCE_EZPAY_OFFER = "FORCE_EZPAY_OFFER";
	public static final String DELIVERY_NOTIFICATION = "DELIVERY_NOTIFY";
	public static final String EE_CANCELS_ALLOWED = "EE_CANCELS_ALLOWED";
	public static final String CUSTOMER_SERVICE_PHONE_CK_SRC = "CUST_SERV_PHONE_CK_SRC";
	public static final String CUSTOMER_SERVICE_PHONE_CK_SRC0 = "CUST_SERV_PHONE_CK_SRC0";
	public static final String CUSTOMER_SERVICE_PHONE_CK_SRC1 = "CUST_SERV_PHONE_CK_SRC1";
	public static final String CUSTOMER_SERVICE_PHONE_CK_SRC2 = "CUST_SERV_PHONE_CK_SRC2";
	public static final String CUSTOMER_SERVICE_PHONE_CK_SRC3 = "CUST_SERV_PHONE_CK_SRC3";
	public static final String CUSTOMER_SERVICE_PHONE_CK_SRC4 = "CUST_SERV_PHONE_CK_SRC4";
	public static final String CUSTOMER_SERVICE_PHONE_CK_SRC_EXCLUDE = "CUST_SERV_PHONE_CK_SRC_EXCLUDE";

	// POP UP and OVERLAYS
	// public static final String SUPPRESS_POPUPS = "KEYCODE_NOPOP_CK";
	// public static final String POPUPURL = "WELPOPUP";
	// public static final String SUBSCRIBE_POPUP = "SUBPOPUP";
	// public static final String SHOW_KEYCODE_POPUP = "KEYCODE_POP_CK";
	public static final String ORDER_PATH_POP_OVERLAY = "ORDER_PATH_POP_OVERLAY";
	public static final String CUTOMER_SERVICE_PATH_POP_OVERLAY = "CS_PATH_POP_OVERLAY";

	/**
	 * @return
	 */
	public String getAltName();

	/**
	 * @return
	 */
	public String getFulfillText();

	/**
	 * @return
	 */
	public String getFulfillUrl();

	/**
	 * @return
	 */
	public String getKeyCode();

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @return
	 */
	public String getPubCode();

	/**
	 * @return
	 */
	public String getType();

}
