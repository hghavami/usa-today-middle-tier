/*
 * Created on May 5, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.products.promotions;

/**
 * @author aeast
 * @date May 5, 2006
 * @class EZPayPromotionIntf
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public interface EZPayPromotionIntf extends PromotionIntf {

	public abstract String getCustomText();
}
