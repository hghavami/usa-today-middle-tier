package com.usatoday.business.interfaces.products.promotions;

public interface JavaScriptPromotionIntf extends PromotionIntf {

	public String getDynamicJavaScript(String orderID, double orderTotal, double orderSubTotal, String orderKeycode,
			String orderProductCode, String email);

	public String getTealiumDynamicJavaScript(String orderID, double orderTotal, double orderSubTotal, String orderKeycode,
			String orderProductCode, String email, String pageName, String rateCode, String rateDescription, boolean requiresEZPay,
			String rateDuration);

	public String getJavaScript();
}
