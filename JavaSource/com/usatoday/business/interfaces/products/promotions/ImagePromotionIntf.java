/*
 * Created on Aug 18, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.products.promotions;

/**
 * @author aeast
 * @date Aug 18, 2006
 * @class ImagePromotionIntf
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public interface ImagePromotionIntf extends PromotionIntf {

	public abstract String getImagePathString();

	public abstract String getImageLinkToURL();

	public abstract String getImageAltText();

}
