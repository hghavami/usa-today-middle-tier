/*
 * Created on May 5, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.products.promotions;

import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentTypeBean;

/**
 * @author aeast
 * @date May 5, 2006
 * @class CreditCardPromotionIntf
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public interface CreditCardPromotionIntf extends PromotionIntf {
	public static final int AMEX = 0;
	public static final int VISA = 1;
	public static final int MC = 2;
	public static final int DISCOVERY = 3;
	public static final int DINERS = 4;

	public static final String[] BANK_NAMES = { "American Express", "VISA", "Master Card", "Discovery", "Diner's Club" };

	public abstract boolean acceptsAmEx();

	public abstract boolean acceptsDiners();

	public abstract boolean acceptsDiscovery();

	public abstract boolean acceptsMasterCard();

	public abstract boolean acceptsVisa();

	public abstract Collection<CreditCardPaymentTypeBean> getCreditCardPaymentTypesBeans() throws UsatException;

	/**
	 * 
	 * @param type
	 *            The type of card we are checking from the XTRNTCRDCTL table values
	 * @return true if this promotion allows that card otherwise false
	 */
	public abstract boolean cardInPromotion(String type);

	public abstract String getAcceptedCardsString();
}
