/*
 * Created on Nov 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.products.promotions.premiums;

import java.sql.Timestamp;

/**
 * @author aeast
 * @date Nov 9, 2006
 * @class PremiumAttributeIntf
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public interface PremiumAttributeIntf {

	public static final int NEW = 0;
	public static final int READY_FOR_BATCH = 1;
	public static final int BATCH_IN_PROGRESS = 2;
	public static final int READY_FOR_DELETION = 3;
	public static final int BATCH_ERROR = 4;

	/**
	 * @return Returns the accountNum.
	 */
	public String getAccountNum();

	/**
	 * @param accountNum
	 *            The accountNum to set.
	 */
	public void setAccountNum(String accountNum);

	/**
	 * @return Returns the attributeName.
	 */
	public String getAttributeName();

	/**
	 * @param attributeName
	 *            The attributeName to set.
	 */
	public void setAttributeName(String attributeName);

	/**
	 * @return Returns the attributeValue.
	 */
	public String getAttributeValue();

	/**
	 * @param attributeValue
	 *            The attributeValue to set.
	 */
	public void setAttributeValue(String attributeValue);

	/**
	 * @return Returns the clubNumber.
	 */
	public boolean isClubNumber();

	/**
	 * @param clubNumber
	 *            The clubNumber to set.
	 */
	public void setClubNumber(boolean clubNumber);

	/**
	 * @return Returns the dateUpdated.
	 */
	public String getDateUpdated();

	/**
	 * @param dateUpdated
	 *            The dateUpdated to set.
	 */
	public void setDateUpdated(String dateUpdated);

	/**
	 * @return Returns the giftPayerReceive.
	 */
	public String getGiftPayerReceive();

	/**
	 * @param giftPayerReceive
	 *            The giftPayerReceive to set.
	 */
	public void setGiftPayerReceive(String giftPayerReceive);

	/**
	 * @return Returns the key.
	 */
	public long getKey();

	/**
	 * @param key
	 *            The key to set.
	 */
	public void setKey(long key);

	/**
	 * @return Returns the orderId.
	 */
	public String getOrderId();

	/**
	 * @param orderId
	 *            The orderId to set.
	 */
	public void setOrderId(String orderId);

	/**
	 * @return Returns the premiumCode.
	 */
	public String getPremiumCode();

	/**
	 * @param premiumCode
	 *            The premiumCode to set.
	 */
	public void setPremiumCode(String premiumCode);

	/**
	 * @return Returns the premiumPromotionCode.
	 */
	public String getPremiumPromotionCode();

	/**
	 * @param premiumPromotionCode
	 *            The premiumPromotionCode to set.
	 */
	public void setPremiumPromotionCode(String premiumPromotionCode);

	/**
	 * @return Returns the premiumType.
	 */
	public String getPremiumType();

	/**
	 * @param premiumType
	 *            The premiumType to set.
	 */
	public void setPremiumType(String premiumType);

	/**
	 * @return Returns the pubCode.
	 */
	public String getPubCode();

	/**
	 * @param pubCode
	 *            The pubCode to set.
	 */
	public void setPubCode(String pubCode);

	/**
	 * @return Returns the state.
	 */
	public int getState();

	/**
	 * @param state
	 *            The state to set.
	 */
	public void setState(int state);

	/**
	 * @return Returns the timeUpdated.
	 */
	public String getTimeUpdated();

	/**
	 * @param timeUpdated
	 *            The timeUpdated to set.
	 */
	public void setTimeUpdated(String timeUpdated);

	/**
	 * @return Returns the updateTimestamp.
	 */
	public Timestamp getUpdateTimestamp();

	/**
	 * @param updateTimestamp
	 *            The updateTimestamp to set.
	 */
	public void setUpdateTimestamp(Timestamp updateTimestamp);

}
