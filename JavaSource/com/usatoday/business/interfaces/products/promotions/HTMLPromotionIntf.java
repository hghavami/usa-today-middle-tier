/*
 * Created on Aug 18, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.business.interfaces.products.promotions;

/**
 * @author aeast
 * @date Aug 18, 2006
 * @class HTMLPromotionIntf
 * 
 *        Wrapper interface for easy access to text base promotions
 * 
 */
public interface HTMLPromotionIntf extends PromotionIntf {

	public abstract String getPromotionalHTML();
}
