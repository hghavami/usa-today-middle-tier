package com.usatoday.business.interfaces.products.suppliers;

public interface SupplierTOIntf extends SupplierIntf {

	public void setID(int id);

	public void setName(String name);

	public void setSecretKey(String key);

	public void setActive(boolean active);

	public void setLoginURL(String url);

	public void setProductURL(String url);

	public void setDescription(String description);

	public void setSampleURL(String url);

	public void setTutorialURL(String url);

}
