package com.usatoday.business.interfaces.products.suppliers;

public interface SupplierIntf {

	public int getID();

	public String getName();

	public String getSecretKey();

	public boolean isActive();

	public String getLoginURL();

	public String getProductURL();

	public String getDescription();

	public String getSampleURL();

	public String getTutorialURL();

	public String getAlternateLoginURL();

	public String getAlternateProductURL();

}
