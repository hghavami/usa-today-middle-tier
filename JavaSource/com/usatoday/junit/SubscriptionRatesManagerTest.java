/*
 * Created on Apr 13, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.junit;

import java.util.Collection;
import java.util.Iterator;

import junit.framework.TestCase;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.promotions.PromotionSet;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Apr 13, 2006
 * @class SubscriptionRatesManagerTest
 * 
 * 
 */
public class SubscriptionRatesManagerTest extends TestCase {

	public static void main(String[] args) {

		SubscriptionOfferManager mngr = null;

		try {
			UsaTodayConstants.loadProperties();

			mngr = SubscriptionOfferManager.getInstance();

			SubscriptionProductIntf usatProd = SubscriptionProductBO.getUsaTodaySubscriptionProduct();

			SubscriptionOfferIntf offer = usatProd.getDefaultOffer();

			System.out.println("Got Offer: " + offer.getKeyCode() + "  Daily Rate: " + offer.getDailyRate()
					+ "  Savings Off Newstand: " + offer.getPercentOffBasePrice(usatProd.getBasePrice()) + "%");
			System.out.println("\tRates:");
			Collection<SubscriptionTermsIntf> terms = offer.getTerms();
			Iterator<SubscriptionTermsIntf> itr = terms.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
				System.out.println("\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
			}
			System.out.println("\t\tRenewal Rates:");
			terms = offer.getRenewalTerms();
			itr = terms.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
				System.out.println("\t\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
			}

			PromotionSet ps = offer.getPromotionSet();
			if (ps != null) {
				System.out.println("\t\tPROMO COUNT: " + ps.getPromoConfigs().size());
				Iterator<PromotionIntf> itr1 = ps.getPromoConfigs().values().iterator();
				while (itr1.hasNext()) {
					PromotionIntf promo = (PromotionIntf) itr.next();
					System.out.println("\t\t" + promo.toString());
				}
			} else {
				System.out.println("NO PROMOTIONAL RECORDS FOR OFFER.");
			}

			// NEXT
			offer = mngr.getOffer("YRDXG", "UT");
			System.out.println("Got Offer: " + offer.getKeyCode());
			System.out.println("\tRates:");
			terms = offer.getTerms();
			itr = terms.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
				System.out.println("\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
			}
			System.out.println("\t\tRenewal Rates:");
			terms = offer.getRenewalTerms();
			itr = terms.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
				System.out.println("\t\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
			}
			ps = offer.getPromotionSet();
			if (ps != null) {
				System.out.println("\t\tPROMO COUNT: " + ps.getPromoConfigs().size());
				Iterator<PromotionIntf> itr1 = ps.getPromoConfigs().values().iterator();
				while (itr1.hasNext()) {
					PromotionIntf promo = (PromotionIntf) itr.next();
					System.out.println("\t\t" + promo.toString());

				}
			} else {
				System.out.println("NO PROMOTIONAL RECORDS FOR OFFER.");
			}

			// Next
			offer = mngr.getOffer("RBCCA", "BW");
			System.out.println("Got Offer: " + offer.getKeyCode());
			System.out.println("\tRates:");
			terms = offer.getTerms();
			itr = terms.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
				System.out.println("\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
			}
			System.out.println("\t\tRenewal Rates:");
			terms = offer.getRenewalTerms();
			itr = terms.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
				System.out.println("\t\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
			}
			ps = offer.getPromotionSet();
			if (ps != null) {
				System.out.println("\t\tPROMO COUNT: " + ps.getPromoConfigs().size());
				Iterator<PromotionIntf> itr1 = ps.getPromoConfigs().values().iterator();
				while (itr1.hasNext()) {
					PromotionIntf promo = (PromotionIntf) itr.next();
					System.out.println("\t\t" + promo.toString());

				}
			} else {
				System.out.println("NO PROMOTIONAL RECORDS FOR OFFER.");
			}

			offer = mngr.getOffer("URX8S", "UT");
			System.out.println("Got Offer: " + offer.getKeyCode());
			System.out.println("\tRates:");
			terms = offer.getTerms();
			itr = terms.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
				System.out.println("\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
			}
			System.out.println("\t\tRenewal Rates:");
			terms = offer.getRenewalTerms();
			itr = terms.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
				System.out.println("\t\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
			}
			ps = offer.getPromotionSet();
			if (ps != null) {
				System.out.println("\t\tPROMO COUNT: " + ps.getPromoConfigs().size());
				Iterator<PromotionIntf> itr1 = ps.getPromoConfigs().values().iterator();
				while (itr1.hasNext()) {
					PromotionIntf promo = (PromotionIntf) itr.next();
					System.out.println("\t\t" + promo.toString());

				}
			} else {
				System.out.println("NO PROMOTIONAL RECORDS FOR OFFER.");
			}

			// NEXT
			SubscriptionProductIntf bwProd = SubscriptionProductBO.getSportsWeeklySubscriptionProduct();

			offer = mngr.getOffer(bwProd.getDefaultKeycode(), "BW");
			System.out.println("Got Offer: " + offer.getKeyCode());
			System.out.println("\tRates:");
			terms = offer.getTerms();
			itr = terms.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
				System.out.println("\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
			}
			System.out.println("\t\tRenewal Rates:");
			terms = offer.getRenewalTerms();
			itr = terms.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
				System.out.println("\t\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
			}
			ps = offer.getPromotionSet();
			if (ps != null) {
				System.out.println("\t\tPROMO COUNT: " + ps.getPromoConfigs().size());
				Iterator<PromotionIntf> itr1 = ps.getPromoConfigs().values().iterator();
				while (itr1.hasNext()) {
					PromotionIntf promo = (PromotionIntf) itr.next();
					System.out.println("\t\t" + promo.toString());

				}
			} else {
				System.out.println("NO PROMOTIONAL RECORDS FOR OFFER.");
			}

			// NEXT
			offer = mngr.getOffer("URXBS", "UT");
			if (offer != null) {
				System.out.println("Got Offer: " + offer.getKeyCode());
				System.out.println("\tRates:");
				terms = offer.getTerms();
				itr = terms.iterator();
				while (itr.hasNext()) {
					SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
					System.out.println("\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
				}
				System.out.println("\t\tRenewal Rates:");
				terms = offer.getRenewalTerms();
				itr = terms.iterator();
				while (itr.hasNext()) {
					SubscriptionTermsIntf term = (SubscriptionTermsIntf) itr.next();
					System.out.println("\t\t" + term.getDescription() + " Daily Rate: " + term.getDailyRate());
				}
			} else {
				System.out.println("No Offer Found for Keycode YHN2C");
			}
			ps = offer.getPromotionSet();
			if (ps != null) {
				System.out.println("\t\tPROMO COUNT: " + ps.getPromoConfigs().size());
				Iterator<PromotionIntf> itr1 = ps.getPromoConfigs().values().iterator();
				while (itr1.hasNext()) {
					PromotionIntf promo = (PromotionIntf) itr.next();
					System.out.println("\t\t" + promo.toString());

				}
			} else {
				System.out.println("NO PROMOTIONAL RECORDS FOR OFFER.");
			}

		} catch (UsatException ue) {
			System.out.println("Exception: " + ue.getMessage());
		} catch (Exception e) {
			System.out.println("Exception : " + e.getMessage());
		}
	}

	public void testGetInstance() {
	}

	public void testGetOffer() {
	}

}
