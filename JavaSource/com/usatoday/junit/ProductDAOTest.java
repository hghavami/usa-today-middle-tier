/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.junit;

import java.util.Collection;
import java.util.Iterator;

import junit.framework.TestCase;

import com.usatoday.business.interfaces.products.PersistentProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.integration.ProductDAO;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class ProductDAOTest
 * 
 */
public class ProductDAOTest extends TestCase {

	public static void main(String[] args) {
		ProductDAOTest pDaoTest = new ProductDAOTest();

		pDaoTest.testGetProduct();

		pDaoTest.testGetProductsByProductCode();

		pDaoTest.testSearchForProducts();

		System.out.println("Application terminating.");
	}

	public void testGetProduct() {
		ProductDAO dao = new ProductDAO();

		try {
			int id = 2;
			PersistentProductIntf prod = dao.getProduct(id);

			System.out.println("Product Name: " + prod.getName() + " Description: " + prod.getDescription() + "  Base Price: "
					+ prod.getUnitPrice());

			SubscriptionProductBO p = new SubscriptionProductBO(prod);

			SubscriptionOfferIntf offer = p.getDefaultOffer();
			Collection<SubscriptionTermsIntf> terms = offer.getTerms();
			Iterator<SubscriptionTermsIntf> itr2 = terms.iterator();
			SubscriptionTermsIntf term = null;
			while (itr2.hasNext()) {
				term = itr2.next();
				System.out.println("\tTerm Selected: " + term.getDescription());
				break;
			}

			p.applyOffer(offer);
			p.applyTerm(term);

			System.out.println("Subscription Price for term " + term.getDescription() + " is " + p.getUnitPrice());

		} catch (Exception e) {
			System.out.println("Exception : " + e.getMessage());
			e.printStackTrace();
		}
		System.out.println("...........Leaving testGetProduct........");
	}

	public void testGetProductsByProductCode() {
		ProductDAO dao = new ProductDAO();

		try {

			Collection<PersistentProductIntf> products = dao.getProductsByProductCode("BW");

			Iterator<PersistentProductIntf> itr = products.iterator();
			while (itr.hasNext()) {
				PersistentProductIntf prod = itr.next();
				SubscriptionProductBO p = new SubscriptionProductBO(prod);

				System.out.println("Product Name: " + p.getName() + " Description: " + p.getDescription() + "  Base Price: "
						+ p.getBasePrice());
				SubscriptionOfferIntf offer = p.getDefaultOffer();

				System.out.println("Got Default Offer: " + offer.getKeyCode());
				System.out.println("\tRates:");
				Collection<SubscriptionTermsIntf> terms = offer.getTerms();
				Iterator<SubscriptionTermsIntf> itr2 = terms.iterator();
				while (itr2.hasNext()) {
					SubscriptionTermsIntf term = itr2.next();
					System.out.println("\t" + term.getDescription());
				}
				System.out.println("\t\tRenewal Rates:");
				terms = offer.getRenewalTerms();
				itr2 = terms.iterator();
				while (itr2.hasNext()) {
					SubscriptionTermsIntf term = itr2.next();
					System.out.println("\t\t" + term.getDescription());
				}

			}
		} catch (Exception e) {
			System.out.println("Exception : " + e.getMessage());
			e.printStackTrace();
		}
		System.out.println(".....Leaving testGetProductsByProductCode....");
	}

	public void testSearchForProducts() {
		ProductDAO dao = new ProductDAO();

		try {
			// int id = 2;
			Collection<PersistentProductIntf> products = dao.searchForProducts("USA");

			Iterator<PersistentProductIntf> itr = products.iterator();
			while (itr.hasNext()) {
				PersistentProductIntf prod = (PersistentProductIntf) itr.next();
				// SubscriptionProductBO p = new SubscriptionProductBO(prod);

				System.out.println("Product Name: " + prod.getName() + " Description: " + prod.getDescription() + "  Price: "
						+ prod.getUnitPrice());
			}

		} catch (Exception e) {
			System.out.println("Exception : " + e.getMessage());
			e.printStackTrace();
		}
		System.out.println("...........Leaving testSearchForProducts........");
	}

}
