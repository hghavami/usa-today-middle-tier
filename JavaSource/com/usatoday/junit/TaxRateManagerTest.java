/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.junit;

import com.usatoday.businessObjects.util.TaxRateBO;
import com.usatoday.businessObjects.util.TaxRateManager;

import junit.framework.TestCase;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class TaxRateManagerTest
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class TaxRateManagerTest extends TestCase {

	public static void main(String[] args) {
		TaxRateManagerTest test = new TaxRateManagerTest();

		test.testGetTaxRatesForZip();
	}

	public void testGetTaxRatesForZip() {

		TaxRateManager taxManager = TaxRateManager.getInstance();

		String zip = "20141";
		TaxRateBO taxRates = taxManager.getTaxRatesForZip(zip);
		double taxAmount = taxRates.computeTax("UT", 10000.00);
		System.out.println("Taxes for zip: " + zip + " on $10,000 = " + taxAmount + "  Tax Rate: "
				+ taxRates.getTaxRatePercentage("UT") + "%");
		System.out.println();

		zip = "30224";
		taxRates = taxManager.getTaxRatesForZip(zip);
		taxAmount = taxRates.computeTax("UT", 10000.00);
		System.out.println("Taxes for zip: " + zip + " on $10,000 = " + taxAmount + "  Tax Rate: "
				+ taxRates.getTaxRatePercentage("UT") + "%");
		System.out.println();

		zip = "65402";
		taxRates = taxManager.getTaxRatesForZip(zip);
		taxAmount = taxRates.computeTax("UT", 10000.00);
		System.out.println("Taxes for zip: " + zip + " on $10,000 = " + taxAmount + "  Tax Rate: "
				+ taxRates.getTaxRatePercentage("UT") + "%");
		System.out.println();

		zip = "95438";
		taxRates = taxManager.getTaxRatesForZip(zip);
		taxAmount = taxRates.computeTax("UT", 10000.00);
		System.out.println("Taxes for zip: " + zip + " on $10,000 = " + taxAmount + "  Tax Rate: "
				+ taxRates.getTaxRatePercentage("UT") + "%");
		System.out.println();

		zip = "65402";
		taxRates = taxManager.getTaxRatesForZip(zip);
		taxAmount = taxRates.computeTax("UT", 10000.00);
		System.out.println("Taxes for zip: " + zip + " on $10,000 = " + taxAmount + "  Tax Rate: "
				+ taxRates.getTaxRatePercentage("UT") + "%");
		System.out.println();

	}

}
