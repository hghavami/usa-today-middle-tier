/*
 * Created on Apr 10, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.junit;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.businessObjects.customer.CustomerBO;
import com.usatoday.businessObjects.customer.LoginException;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;

/**
 * @author aeast
 * @date Apr 10, 2006
 * @class CustomerBOTest
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class CustomerBOTest {

	public static void main(String[] args) {

		CustomerBOTest cboTest = new CustomerBOTest();

		// cboTest.testLoginCustomerStringStringString();
		// cboTest.testLoginCustomerStringString();
		cboTest.testLoginCustomerIdentifier();

	}

	/*
	 * Class under test for CustomerBO loginCustomer(String, String)
	 */
	public void testLoginCustomerStringString() {
		CustomerIntf customer = null;

		try {
			customer = CustomerBO.loginCustomer("aeast@usatoday.com", "password");

			System.out.println("Got the customer. Account Num: " + customer.getCurrentAccount().getAccountNumber());
			System.out.println("Number of accounts: " + customer.getNumberOfAccounts());

			HashMap<String, SubscriberAccountIntf> accounts = customer.getAccounts();

			System.out.println("Account Size: " + accounts.size());

		} catch (LoginException le) {
			System.out.println(le.getErrorMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public void testLoginCustomerIdentifier() {
		CustomerIntf customer = null;

		try {
			customer = CustomerBO.loginCustomerByUniqueIdentifier("159970");

			System.out.println("Got the customer. Account Num: " + customer.getCurrentAccount().getAccountNumber());
			System.out.println("Number of accounts: " + customer.getNumberOfAccounts());

			HashMap<String, SubscriberAccountIntf> accounts = customer.getAccounts();

			System.out.println("Account Size: " + accounts.size());

		} catch (LoginException le) {
			System.out.println(le.getErrorMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	/*
	 * Class under test for CustomerBO loginCustomer(String, String, String)
	 */
	public void testLoginCustomerStringStringString() {

		CustomerIntf customer = null;

		try {
			customer = CustomerBO.loginCustomer("hghavami@usatoday.com", "password", "UT");
			// customer = CustomerBO.loginCustomer("andyeast@adelphia.net", "password", "UT");

			System.out.println("Got the customer.");
			System.out.println("Number of accounts: " + customer.getNumberOfAccounts());

			HashMap<String, SubscriberAccountIntf> accounts = customer.getAccounts();

			System.out.println("Account Size: " + accounts.size());
			Set<String> keySet = accounts.keySet();
			System.out.println("Key set size: " + keySet.size());
			Iterator<String> itr = accounts.keySet().iterator();
			while (itr.hasNext()) {
				String currentAccountNum = (String) itr.next();

				customer.setCurrentAccount(currentAccountNum);

				SubscriberAccountIntf account = customer.getCurrentAccount();
				EmailRecordIntf email = customer.getCurrentEmailRecord();

				String lastUpdatedStr = (email.getLastUpdated() == null) ? "NOT SET" : email.getLastUpdated().toString();

				System.out.println("CurrentAccountNumber: " + currentAccountNum + "; Email Address: " + email.getEmailAddress()
						+ "; Password: " + email.getPassword() + "; OrderID: " + email.getOrderID() + "; Last Updated: "
						+ lastUpdatedStr + "; Delivery Name: " + account.getDeliveryContact().getFirstName() + " "
						+ account.getDeliveryContact().getLastName() + "; Balance: " + account.getBalance());
				UIAddressIntf deliveryAddr = account.getDeliveryContact().getUIAddress();
				System.out.println("\t" + account.getDeliveryContact().getFirstName() + " "
						+ account.getDeliveryContact().getLastName());
				System.out.println("\t" + deliveryAddr.getAddress1() + " " + deliveryAddr.getAptSuite());
				String address2 = deliveryAddr.getAddress2();
				if (address2 != null && address2.length() > 0) {
					System.out.println("\t" + address2);
				}
				System.out.println("\t" + deliveryAddr.getCity() + ", " + deliveryAddr.getState() + "    " + deliveryAddr.getZip());

				try {
					OfferIntf offer = account.getDefaultRenewalOffer();
					if (offer != null) {
						Collection<SubscriptionTermsIntf> terms = null;
						System.out.println("\tRenewal Rates:");
						terms = offer.getTerms();
						Iterator<SubscriptionTermsIntf> ritr = terms.iterator();
						while (ritr.hasNext()) {
							SubscriptionTermsIntf term = ritr.next();
							System.out.println("\t\t" + term.getDescription());
						}
					}
				} catch (Exception e) {
					System.out.println("Exception getting renewal rates: " + e.getMessage());
				}

			}

			int numberCached = SubscriptionOfferManager.getInstance().getNumberUTRenewalRatesCached();
			System.out.println("Number of UT Renewal Rates Cached: " + numberCached);

			System.out.println("Test Terminating.");

		} catch (LoginException le) {
			System.out.println(le.getErrorMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public CustomerIntf testLogin(String userid, String password, String pub) {

		CustomerIntf customer = null;

		try {
			customer = CustomerBO.loginCustomer(userid, password, pub);

			// System.out.println("Got the customer.");
			// System.out.println("Number of accounts: " + customer.getNumberOfAccounts());

			// HashMap accounts = customer.getAccounts();

		} catch (LoginException le) {
			System.out.println(le.getErrorMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return customer;
	}
}
