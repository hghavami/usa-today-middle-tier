/*
 * Created on May 4, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.junit;

import java.util.Collection;
import java.util.Iterator;

import junit.framework.TestCase;

import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.shopping.OrderIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentTypentf;
import com.usatoday.business.interfaces.shopping.payment.PaymentMethodIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.customer.UIAddressBO;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.shopping.CheckOutService;
import com.usatoday.businessObjects.shopping.ShoppingCart;
import com.usatoday.businessObjects.shopping.SubscriptionOrderItemBO;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentMethodBO;
import com.usatoday.integration.PaymentTypeDAO;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date May 4, 2006
 * @class ShoppingCartTest
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class ShoppingCartTest extends TestCase {

	public static void main(String[] args) {

		ShoppingCartTest test = new ShoppingCartTest();

		test.doTest();

		System.out.println("Test Terminated Normally");
	}

	/**
     * 
     *
     */
	private void doTest() {

		try {
			UsaTodayConstants.loadProperties();

			SubscriptionProductIntf sProd = SubscriptionProductBO.getUsaTodaySubscriptionProduct();

			ContactIntf deliveryContact = this.createDeliveryContact();

			if (deliveryContact.validateContact()) {
				System.out.println("Contact is valid.");
			}

			if (deliveryContact.isQuestionableAddress()) {
				System.out.println("We got us a questionable Address.");
				System.out.println(deliveryContact.getPersistentAddress().toString());
			}

			ContactIntf billingContact = this.createBillingContact();
			if (billingContact.validateContact()) {
				System.out.println("Billing Contact is valid.");
			}

			if (billingContact.isQuestionableAddress()) {
				System.out.println("We got us a questionable Billing Address.");
				System.out.println(billingContact.getPersistentAddress().toString());
			}

			SubscriptionOfferIntf offer = sProd.getDefaultOffer();
			SubscriptionTermsIntf term = (SubscriptionTermsIntf) offer.getTerms().iterator().next();

			sProd.applyOffer(offer);
			sProd.applyTerm(term);

			SubscriptionOrderItemBO subscriptionItem = new SubscriptionOrderItemBO();
			subscriptionItem.setChoosingEZPay(true);
			subscriptionItem.setClubNumber("");
			subscriptionItem.setDeliveryZip(deliveryContact.getUIAddress().getZip());
			subscriptionItem.setDeliveryEmailAddress(deliveryContact.getEmailAddress());
			subscriptionItem.setGiftItem(true);
			subscriptionItem.setKeyCode(offer.getKeyCode());
			subscriptionItem.setOneTimeBill(false);
			subscriptionItem.setPremiumCode(""); // no premium
			subscriptionItem.setProduct(sProd);
			subscriptionItem.setQuantity(7);
			subscriptionItem.setSelectedTerm(term);
			subscriptionItem.setStartDate("20060516");

			ShoppingCart cart = new ShoppingCart();

			cart.setDeliveryContact(deliveryContact);
			if (billingContact != null) {
				cart.setBillingContact(billingContact);
			}

			System.out.println("Adding item to cart: Prod Desc: " + subscriptionItem.getProduct().getDescription() + "; Quantity: "
					+ subscriptionItem.getQuantity() + "; Unit Price: " + subscriptionItem.getProduct().getUnitPrice()
					+ "; Tax Rate: " + subscriptionItem.getTaxRatePercentage(deliveryContact.getUIAddress().getZip()));

			cart.addItem(subscriptionItem);

			// set up a payment method
			PaymentMethodIntf paymentMethod = this.createPaymentMethod();
			cart.setPaymentMethod(paymentMethod);

			System.out.println("Checking out cart with " + cart.getItems().size() + " items in it Subtotal: $" + cart.getSubTotal()
					+ " Tax: $" + cart.getSalesTax() + " Total: $" + cart.getTotal());

			CheckOutService checkout = new CheckOutService();
			OrderIntf myOrder = checkout.checkOutShoppingCart(cart, UsaTodayConstants.BATCH_PROCESS_CC_NETWORK_FAILURES);

			System.out.println("Successfully checked out.");
			System.out.println("Order ID: " + myOrder.getOrderID());
			System.out.println("Order Fulfillment Status: " + myOrder.getOrderFulfillmentStatusString());
			System.out.println("Order Payment Status: " + myOrder.getOrderPaymentStatusString());
			System.out.println("Credit Card Auth Code: " + myOrder.getAuthCode());

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	/**
	 * 
	 * @return a sample delivery contact
	 */
	private ContactIntf createDeliveryContact() {
		ContactBO contact = new ContactBO();

		contact.setBusinessPhone("7038545407");
		contact.setEmailAddress("aeast@usatoday.com");
		contact.setFirmName("");
		contact.setFirstName("Andy");
		contact.setHomePhone("5403385280");
		contact.setLastName("EastDelivery");

		UIAddressIntf address = this.createPostalAddress();
		contact.setUIAddress(address);
		return contact;
	}

	/**
	 * 
	 * @return a sample billing contact
	 */
	private ContactIntf createBillingContact() {
		ContactBO contact = new ContactBO();

		contact.setBusinessPhone("8008710001");
		contact.setEmailAddress("aeast@usatoday.com");
		contact.setFirmName("USA TODAY");
		contact.setFirstName("Andy");
		contact.setHomePhone("5403387590");
		contact.setLastName("EastBilling");

		UIAddressIntf address = this.createBillingPostalAddress();
		contact.setUIAddress(address);
		return contact;
	}

	/**
	 * A sample postal address as taken from a UI screen
	 * 
	 * @return
	 */
	private UIAddressIntf createPostalAddress() {
		UIAddressBO address = new UIAddressBO();

		// non taxable valid address
		address.setAddress1("17545 Brookville Ct");
		address.setAddress2("");
		address.setAptSuite("");
		address.setCity("Round Hill");
		address.setState("VA");
		address.setZip("20141");

		// Taxable address
		// address.setAddress1("1756 Hollywood Ave");
		// address.setAddress2("");
		// address.setAptSuite("");
		// address.setCity("Winter Park");
		// address.setState("FL");
		// address.setZip("32789");

		// Questionable Address
		// address.setAddress1("500 Red Brook Lane");
		// address.setAddress2("");
		// address.setAptSuite("#1300");
		// address.setCity("Owings Mills");
		// address.setState("MD");
		// address.setZip("21117");

		return address;
	}

	/**
	 * A sample postal address as taken from a UI screen
	 * 
	 * @return
	 */
	private UIAddressIntf createBillingPostalAddress() {
		UIAddressBO address = new UIAddressBO();

		// non taxable valid address
		address.setAddress1("7950 Jones Branch Dr");
		address.setAddress2("");
		address.setAptSuite("U-4");
		address.setCity("McLean");
		address.setState("VA");
		address.setZip("22107");

		// Taxable address
		// address.setAddress1("1756 Hollywood Ave");
		// address.setAddress2("");
		// address.setAptSuite("");
		// address.setCity("Winter Park");
		// address.setState("FL");
		// address.setZip("32789");

		// Questionable Address
		// address.setAddress1("500 Red Brook Lane");
		// address.setAddress2("");
		// address.setAptSuite("#1300");
		// address.setCity("Owings Mills");
		// address.setState("MD");
		// address.setZip("21117");

		return address;
	}

	/**
	 * 
	 * @return a sample payment method
	 */
	private PaymentMethodIntf createPaymentMethod() throws Exception {
		CreditCardPaymentMethodBO payment = new CreditCardPaymentMethodBO();

		PaymentTypeDAO dao = new PaymentTypeDAO();

		Collection<CreditCardPaymentTypentf> ccPaymentOptions = dao.getCreditCardPaymentMethodTypes();
		CreditCardPaymentTypentf paymentType = null;
		Iterator<CreditCardPaymentTypentf> itr = ccPaymentOptions.iterator();
		while (itr.hasNext()) {
			paymentType = (CreditCardPaymentTypentf) itr.next();

			if (paymentType.getType().equalsIgnoreCase("V")) {
				break;
			}
		}

		payment.setCardNumber("4111111111111111");
		payment.setPaymentType(paymentType);
		payment.setExpirationMonth("09");
		payment.setExpirationYear("2006");
		payment.setNameOnCard("Andrew East");

		return payment;
	}
}
