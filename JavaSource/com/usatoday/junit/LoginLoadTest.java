/*
 * Created on Dec 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.junit;

import java.util.ArrayList;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.customer.CustomerIntf;

/**
 * @author aeast
 * @date Dec 20, 2006
 * @class LoginLoadTest
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class LoginLoadTest implements Runnable {

	long timeToRun = 0;
	String testResults = null;

	String userId = "aeast@usatoday.com";
	String password = "password";
	String pub = "UT";

	CustomerIntf customer = null;

	int numThreads = 25;

	/**
     *  
     */
	public LoginLoadTest() {
		super();

	}

	public static void main(String[] args) {

		LoginLoadTest lt = new LoginLoadTest();
		lt.runTest();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {

		try {
			DateTime startTime = new DateTime();

			CustomerBOTest custTest = new CustomerBOTest();

			customer = custTest.testLogin(this.getUserId(), this.getPassword(), this.getPub());

			DateTime stopTime = new DateTime();

			this.timeToRun = (stopTime.getMillis() - startTime.getMillis());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @return Returns the timeToRun.
	 */
	public long getTimeToRun() {
		return this.timeToRun;
	}

	/**
	 * @param timeToRun
	 *            The timeToRun to set.
	 */
	public void setTimeToRun(long timeToRun) {
		this.timeToRun = timeToRun;
	}

	public void runTest() {
		ArrayList<Thread> threads = new ArrayList<Thread>();
		ArrayList<LoginLoadTest> customerObjs = new ArrayList<LoginLoadTest>();

		int testSize = this.getNumThreads();

		for (int i = 0; i < testSize; i++) {
			LoginLoadTest lt = new LoginLoadTest();

			Thread t = new Thread(lt);

			lt.setPassword(this.getPassword());
			lt.setUserId(this.getUserId());

			threads.add(t);
			customerObjs.add(lt);
		}

		// Start the threads
		for (int j = 0; j < threads.size(); j++) {
			Thread t = threads.get(j);
			t.start();
		}

		// join the threads
		for (int j = 0; j < threads.size(); j++) {
			Thread t = threads.get(j);
			try {
				t.join();
			} catch (Exception e) {
				System.out.println("Thread run method interrupted.." + e.getMessage());
			}
		}

		long time = 0;

		StringBuffer outputBuf = new StringBuffer();
		Iterator<LoginLoadTest> itr = customerObjs.iterator();
		while (itr.hasNext()) {
			LoginLoadTest lt = itr.next();

			time += lt.getTimeToRun();
			outputBuf.append("Time for thread: " + lt.getTimeToRun() + "\n");
			// System.out.println("Time for thread: " + lt.getTimeToRun());
			this.customer = lt.getCustomer();
		}

		outputBuf.append("\n");
		// System.out.println();
		// System.out.println("It took an average of:  " + (time / testSize));
		outputBuf.append("It took an average of:  " + (time / testSize));
		this.testResults = outputBuf.toString();
	}

	/**
	 * @return Returns the testResults.
	 */
	public String getTestResults() {
		return this.testResults;
	}

	/**
	 * @param testResults
	 *            The testResults to set.
	 */
	public void setTestResults(String testResults) {
		this.testResults = testResults;
	}

	/**
	 * @return Returns the numThreads.
	 */
	public int getNumThreads() {
		return this.numThreads;
	}

	/**
	 * @param numThreads
	 *            The numThreads to set.
	 */
	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
	}

	/**
	 * @return Returns the password.
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * @param password
	 *            The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return Returns the pub.
	 */
	public String getPub() {
		return this.pub;
	}

	/**
	 * @param pub
	 *            The pub to set.
	 */
	public void setPub(String pub) {
		this.pub = pub;
	}

	/**
	 * @return Returns the userId.
	 */
	public String getUserId() {
		return this.userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the customer.
	 */
	public CustomerIntf getCustomer() {
		return this.customer;
	}
}
