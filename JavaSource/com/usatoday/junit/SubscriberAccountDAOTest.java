/*
 * Created on Apr 7, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.junit;

import java.util.Collection;

import com.usatoday.business.interfaces.customer.PersistentSubscriberAccountIntf;
import com.usatoday.businessObjects.customer.SubscriberAccountBO;
import com.usatoday.integration.SubscriberAccountDAO;

/**
 * @author aeast
 * @date Apr 7, 2006
 * @class SubscriberAccountDAOTest
 * 
 *        Test out Subscriber Account DAO.
 * 
 */
public class SubscriberAccountDAOTest {

	public static void main(String[] args) {

		SubscriberAccountDAOTest test = new SubscriberAccountDAOTest();

		test.testGetAccountByAccountNumber("5350060");

		// home phone
		test.testGetAccountByPhoneNumberAndZip("7038545833", "21771");
		// billing phone
		test.testGetAccountByPhoneNumberAndZip("7038545833", "21146");
	}

	public void testGetAccountByAccountNumber(String accountNum) {

		try {
			SubscriberAccountDAO dao = new SubscriberAccountDAO();
			Collection<PersistentSubscriberAccountIntf> accounts = dao.getAccountByAccountNumberAndPub(accountNum, null);
			PersistentSubscriberAccountIntf subscriber = null;

			if (accounts.size() == 1) {
				subscriber = (PersistentSubscriberAccountIntf) accounts.toArray()[0];
			}

			if (subscriber != null) {
				SubscriberAccountBO bo = new SubscriberAccountBO(subscriber);
				System.out.println("Got Account - By Account Number!");
				System.out.println("ACCT NUM: " + bo.getAccountNumber() + "Delivery Contact: "
						+ bo.getDeliveryContact().getUIAddress().toString());
			} else {
				System.out.println("No Account Found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	/**
	 * 
	 * @param phone
	 * @param zip
	 */
	public void testGetAccountByPhoneNumberAndZip(String phone, String zip) {

		try {
			SubscriberAccountDAO dao = new SubscriberAccountDAO();
			Collection<PersistentSubscriberAccountIntf> subscribers = dao.getAccountByDeliveryPhoneNumberAndZip(phone, zip);

			if (subscribers != null && subscribers.size() > 0) {
				System.out.println("Got Accounts - Home Phone!  Number Returned: " + subscribers.size());
			} else {
				subscribers = dao.getAccountByBillingPhoneNumberAndZip(phone, zip);
				if (subscribers != null && subscribers.size() > 0) {
					System.out.println("Got Accounts - Billing Phone!  Number Returned: " + subscribers.size());
				} else {
					System.out.println("No Account Found");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

}
