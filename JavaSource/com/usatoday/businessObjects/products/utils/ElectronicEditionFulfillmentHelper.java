package com.usatoday.businessObjects.products.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.suppliers.SupplierIntf;
import com.usatoday.util.constants.UsaTodayConstants;

public class ElectronicEditionFulfillmentHelper {

	private static final char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static String buildOliveFulfillmentURL(ProductIntf product, String userID, String readerURL, String restrictions,
			boolean useAlternateIfAvailable) {
		StringBuilder productURL = new StringBuilder();
		SupplierIntf supplier = product.getSupplier();

		if (supplier != null) {
			// Reader = olive skin link URLEncoded
			// Id = user's id
			// d = UTC Date
			// r = restrictions
			// c = MD5 hash
			String hashInput = "";

			// if an alternate login url is available and we should use it.
			if (useAlternateIfAvailable && supplier.getAlternateLoginURL() != null) {
				productURL.append(supplier.getAlternateLoginURL());
			} else {
				productURL.append(supplier.getLoginURL());
			}

			productURL.append("?Reader=");

			String reader = "";
			try {
				// if readerURL exists, use it otherwise use the one tied to supplier.
				if (readerURL != null) {
					reader = URLEncoder.encode(readerURL, "UTF-8");
					productURL.append(reader);
				} else {
					if (useAlternateIfAvailable && supplier.getAlternateProductURL() != null) {
						reader = URLEncoder.encode(supplier.getAlternateProductURL(), "UTF-8");
					} else {
						reader = URLEncoder.encode(supplier.getProductURL(), "UTF-8");
					}
					productURL.append(reader);
				}
			} catch (UnsupportedEncodingException use) {
				use.printStackTrace();
			}
			productURL.append("&Id=");
			// productURL.append(URLEncoder.encode(userID, "UTF-8"));
			productURL.append(userID);
			hashInput += userID;

			DateTime dt = new DateTime(DateTimeZone.UTC);
			String d = dt.toString("YYYY-MM-dd");

			productURL.append("&d=");
			productURL.append(d);
			hashInput += d;
			String r = "none";
			if (restrictions != null) {
				r = restrictions;
			}
			productURL.append("&r=");
			productURL.append(r);
			hashInput += r;

			hashInput += supplier.getSecretKey();

			String c = ElectronicEditionFulfillmentHelper.getMD5Hash(hashInput);

			productURL.append("&c=").append(c);

			if (UsaTodayConstants.debug) {
				System.out.println("Finished URL:" + productURL.toString());
			}

		}
		return productURL.toString();
	}

	public static String getMD5Hash(String value) {
		String hash = null;
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");

			byte[] hashBytes = md5.digest(value.getBytes());

			String hex = "";
			int msb;
			int lsb = 0;
			int i;

			// MSB maps to idx 0
			for (i = 0; i < hashBytes.length; i++) {

				msb = ((int) hashBytes[i] & 0x000000FF) / 16;

				lsb = ((int) hashBytes[i] & 0x000000FF) % 16;
				hex = hex + hexChars[msb] + hexChars[lsb];
			}
			hash = hex;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return hash;
	}

	public static String buildOliveFulfillmentURL(String loginURL, String alternateLoginURL, String secretKey, String userID,
			String readerURL, String restrictions, boolean useAlternateIfAvailable) {
		StringBuilder productURL = new StringBuilder();

		// Reader = olive skin link URLEncoded
		// Id = user's id
		// d = UTC Date
		// r = restrictions
		// c = MD5 hash
		String hashInput = "";

		// if an alternate login url is available and we should use it.
		if (useAlternateIfAvailable && alternateLoginURL != null) {
			productURL.append(useAlternateIfAvailable);
		} else {
			productURL.append(loginURL);
		}

		productURL.append("?Reader=");

		String reader = "";
		try {
			// if readerURL exists, use it otherwise use the one tied to supplier.
			if (readerURL != null) {
				reader = URLEncoder.encode(readerURL, "UTF-8");
				productURL.append(reader);
			} else {
				if (useAlternateIfAvailable && alternateLoginURL != null) {
					reader = URLEncoder.encode(alternateLoginURL, "UTF-8");
				} else {
					reader = URLEncoder.encode(loginURL, "UTF-8");
				}
				productURL.append(reader);
			}
		} catch (UnsupportedEncodingException use) {
			use.printStackTrace();
		}
		productURL.append("&Id=");
		// productURL.append(URLEncoder.encode(userID, "UTF-8"));
		productURL.append(userID);
		hashInput += userID;

		DateTime dt = new DateTime(DateTimeZone.UTC);
		String d = dt.toString("YYYY-MM-dd");

		productURL.append("&d=");
		productURL.append(d);
		hashInput += d;
		String r = "none";
		if (restrictions != null) {
			r = restrictions;
		}
		productURL.append("&r=");
		productURL.append(r);
		hashInput += r;

		hashInput += secretKey;

		String c = ElectronicEditionFulfillmentHelper.getMD5Hash(hashInput);

		productURL.append("&c=").append(c);

		if (UsaTodayConstants.debug) {
			System.out.println("Finished URL:" + productURL.toString());
		}

		return productURL.toString();
	}
}
