/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.PersistentProductIntf;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.integration.ProductTO;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class SubscriptionProductBO
 * 
 *        This class represents a subscription product.
 * 
 */
public class SubscriptionProductBO extends ProductBO implements SubscriptionProductIntf {

	SubscriptionTermsIntf offerTerm = null;
	OfferIntf offer = null;

	public SubscriptionProductBO(PersistentProductIntf prod) {
		super(prod);
	}

	/**
	 * 
	 * @return The USA TODAY subscription product.
	 * @throws UsatException
	 */
	public static SubscriptionProductIntf getUsaTodaySubscriptionProduct() throws UsatException {
		SubscriptionProductIntf sProd = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.UT_PUBCODE);

		return sProd;
	}

	/**
	 * 
	 * @return The Sports Weekly subscription product
	 * @throws UsatException
	 */
	public static SubscriptionProductIntf getSportsWeeklySubscriptionProduct() throws UsatException {
		SubscriptionProductIntf sProd = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.SW_PUBCODE);

		return sProd;
	}

	/**
	 * 
	 * @param productCode
	 * @return
	 * @throws UsatException
	 */
	public static SubscriptionProductIntf getSubscriptionProduct(String productCode) throws UsatException {
		SubscriptionProductIntf sProd = null;

		// must return a copy of the product!
		if (productCode == null) {
			return null;
		}

		ProductIntf p = ProductBO.fetchProduct(productCode);

		if (p instanceof SubscriptionProductIntf) {
			sProd = (SubscriptionProductIntf) p;
		} else {
			throw new UsatException("Product Code is not a subscription type of product: " + productCode);
		}

		return sProd;

	}

	/**
	 * @deprecated use ProductBO.clearCachedProducts
	 */
	@Deprecated
	public static void clearCachedProducts() {

		// method left for backward compatibility, should call ProductBO.clearCahcedProducts();
		ProductBO.clearCachedProducts();
	}

	/**
	 * 
	 * @return
	 */
	protected SubscriptionProductIntf getCopyOfObject() {
		SubscriptionProductIntf prod = null;

		try {
			ProductTO prodTO = new ProductTO();
			prodTO.setDescription(this.getDescription());
			prodTO.setDetailedDescription(this.getDetailedDescription());
			prodTO.setEndDate(this.getEndDate());
			prodTO.setId(this.getID());
			prodTO.setInventory(this.inventory);
			prodTO.setMaxQuantityPerOrder(this.getMaxQuantityPerOrder());
			prodTO.setMinQuantityPerOrder(this.getMinQuantityPerOrder());
			prodTO.setName(this.getName());
			prodTO.setProductCode(this.getProductCode());
			prodTO.setStartDate(this.getStartDate());
			prodTO.setTaxable(this.isTaxable());
			prodTO.setTaxRateCode(this.getTaxRateCode());
			prodTO.setUnitPrice(this.unitPrice);
			prodTO.setFulfillmentMethod(this.getFulfillmentMethod());
			prodTO.setBrandingPubCode(this.getBrandingPubCode());
			prodTO.setSupplierID(this.getSupplierID());
			prodTO.setCustomerServicePhone(this.getCustomerServicePhone());
			prodTO.setDefaultKeycode(this.getDefaultKeycode());
			prodTO.setExpiredOfferKeycode(this.getExpiredOfferKeycode());
			prodTO.setDefaultRenewalKeycode(this.getDefaultRenewalKeycode());
			prodTO.setMaxDaysInFutureBeforeFulfillment(this.getMaxDaysInFutureBeforeFulfillment());
			prodTO.setHoldDaysDelay(this.getHoldDaysDelay());
			prodTO.setProductType(this.getProductType());

			SubscriptionProductBO pBO = new SubscriptionProductBO(prodTO);
			if (this.supplier != null) {
				pBO.supplier = this.supplier;
			}
			prod = pBO;
		} catch (Exception e) {
			e.printStackTrace();
			prod = null;
		}
		return prod;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#isAvailable()
	 */
	public boolean isAvailable() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.ProductIntf#updateInventory(int)
	 */
	public int updateInventory(int delta) {
		// there is an unlimited inventory of subscriptions
		return 100000;
	}

	public SubscriptionOfferIntf getDefaultOffer() throws UsatException {
		SubscriptionOfferIntf offer = SubscriptionOfferManager.getInstance().getDefaultOffer(this.getProductCode());
		return offer;
	}

	public SubscriptionOfferIntf getOffer(String keycode) throws UsatException {
		SubscriptionOfferIntf offer = SubscriptionOfferManager.getInstance().getOffer(keycode, this.getProductCode());
		return offer;
	}

	public double getBasePrice() throws UsatException {
		return super.getUnitPrice();
	}

	/**
	 * Subscription classes override the unit price since the unit price is based on the term selected from the offer provided.
	 * 
	 */
	public double getUnitPrice() throws UsatException {
		double uPrice = 0.0;

		if (this.offerTerm == null) {
			throw new UsatException("No term applied to this product. Please choose a term.");
		}

		uPrice = this.offerTerm.getPrice();

		return uPrice;
	}

	/**
	 * 
	 * @param term
	 *            The term to apply to this instance of a subscription
	 */
	public void applyTerm(SubscriptionTermsIntf term) {
		this.offerTerm = term;
	}

	public void applyOffer(OfferIntf offer) {
		this.offer = offer;
	}

	public OfferIntf getAppliedOffer() {
		return offer;
	}

	public SubscriptionTermsIntf getAppliedTerm() {
		return this.offerTerm;
	}

	public DateTime getEarliestPossibleStartDate() {

		DateTime today = new DateTime();
		today = today.minusSeconds((today.getSecondOfDay() - 1));
		String dow = today.dayOfWeek().getAsShortText();
		int hod = new DateTime().getHourOfDay();
		// Electronic deliveries can start immediately
		if (this.isElectronicDelivery()) {
			return today;
		}

		SubscriptionPublishDateUtility publishDateUtils = SubscriptionPublishDateUtility.getInstance();

		// print products can't start until first publishing day past threshold of (4) currently
		// need to increase for any holidays

		DateTime earliestStartFromToday = today;

		int maxCount = 90;
		int count = 0;

		boolean foundDate = false;

		if (this.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
			// for UT go out X publishing days from today (currently 4)

			for (int i = 0; (i < SubscriptionPublishDateUtility.EARLIEST_START_FROM_TODAY) && (count < maxCount);) {
				count++;
				earliestStartFromToday = earliestStartFromToday.plusDays(1);
				// if it's not a publishing day then we need to bump up the count if it's a holiday
				if (publishDateUtils.isValidPublishingDate(this.getBrandingPubCode(), earliestStartFromToday)) {
					i++;
				}
			}

		} else if (this.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
			// for SW
			// If Fri past 1pm or Sat-Sun take the first publishing day past 6 days. For Mon-Thu take it past 5 days.
			if (dow.equals("Sat") || dow.equals("Sun") || (dow.equals("Fri") && hod > 11)) {
//			if (dow.equals("Sat") || dow.equals("Sun") || (dow.equals("Mon") && hod > 11)) {
				earliestStartFromToday = earliestStartFromToday
						.plusDays(SubscriptionPublishDateUtility.EARLIEST_SW_START_FROM_FRI_SUN);
			} else {
				earliestStartFromToday = earliestStartFromToday
						.plusDays(SubscriptionPublishDateUtility.EARLIEST_SW_START_FROM_MON_THU);
			}
			// earliestStartFromToday = earliestStartFromToday.plusDays(SubscriptionPublishDateUtility.EARLIEST_START_FROM_TODAY);
			while (!foundDate && (count < maxCount)) {
				count++;
				if (publishDateUtils.isValidPublishingDate(this.getBrandingPubCode(), earliestStartFromToday)) {
					// if first day forwarded
					break;
				}
				earliestStartFromToday = earliestStartFromToday.plusDays(1);
			}
		} else {
			// be default just push out 4 days
			earliestStartFromToday = today.plusDays(SubscriptionPublishDateUtility.EARLIEST_START_FROM_TODAY);
		}

		if (count == maxCount) {
			// default to 4 days out
			earliestStartFromToday = today.plusDays(SubscriptionPublishDateUtility.EARLIEST_START_FROM_TODAY);
		}

		return earliestStartFromToday;
	}

	public DateTime getLatestPossibleStartDate() {

		DateTime dt = new DateTime();
		dt = dt.minusSeconds((dt.getSecondOfDay() - 1));

		dt = dt.plusDays(this.getMaxDaysInFutureBeforeFulfillment());

		return dt;
	}

	public boolean isStartDateValid(DateTime requestedStartDate) throws UsatException {
		boolean isStartValid = false;

		try {

			// first confirm that the day is a publishing day
			DateTime earliestStart = this.getEarliestPossibleStartDate();
			DateTime earliestStartMinus1 = earliestStart.minusDays(1);
			DateTime latestStart = this.getLatestPossibleStartDate();
			DateTime latestStartPlus1 = latestStart.plusDays(1);

			// Now confirm it fits withing legal limits for starting subscription
			if (requestedStartDate.isAfter(earliestStartMinus1) && requestedStartDate.isBefore(latestStartPlus1)) {

				if (this.isElectronicDelivery()) {
					isStartValid = true;
				} else {
					SubscriptionPublishDateUtility pubDateUtils = SubscriptionPublishDateUtility.getInstance();
					if (pubDateUtils.isValidPublishingDate(this.getBrandingPubCode(), requestedStartDate)) {
						isStartValid = true;
					} else {
						StringBuilder message = new StringBuilder();
						message.append("We are unable to start your ");
						message.append(this.getName()).append(" subscription on ")
								.append(requestedStartDate.toString("MM/dd/yyyy"));
						if (this.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
							message.append(". ")
									.append(this.getName())
									.append(" is published M-F except certain holidays. Please choose a non holiday weekday between ")
									.append(earliestStart.toString("MM/dd/yyyy"));
							message.append(" and ").append(latestStart.toString("MM/dd/yyyy")).append(".");

						} else {
							message.append(". ")
									.append(this.getName())
									.append(" is published on Wednesdays only. Please choose the first valid Wednesday before of after your requested date.");
						}
						throw new UsatException(message.toString());
					}
				}
			} else {
				StringBuilder message = new StringBuilder();
				message.append("We are unable to start your ");
				message.append(this.getName()).append(" subscription on ").append(requestedStartDate.toString("MM/dd/yyyy"));
				message.append(". Please choose a date between ").append(earliestStart.toString("MM/dd/yyyy"));
				message.append(" and ").append(latestStart.toString("MM/dd/yyyy")).append(".");
				throw new UsatException(message.toString());
			}

		} catch (UsatException ue) {
			throw ue;
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(e);
		}

		return isStartValid;
	}

	/**
	 * 
	 */
	public DateTime getFirstValidStartDateAfter(DateTime requestedDate) {
		DateTime nextDate = requestedDate;
		DateTime earliest = this.getEarliestPossibleStartDate();
		DateTime latest = this.getLatestPossibleStartDate();

		try {
			if (requestedDate.isBefore(earliest)) {
				return earliest;
			}

			if (requestedDate.isAfter(latest)) {
				return latest;
			}

			if (this.isElectronicDelivery()) {
				nextDate = nextDate.plusDays(1);
			} else {
				int count = 0;
				int maxDaysForward = 10;

				SubscriptionPublishDateUtility dateUtil = SubscriptionPublishDateUtility.getInstance();

				while (count < maxDaysForward) {
					nextDate = nextDate.plusDays(1);
					if (dateUtil.isValidPublishingDate(this.getBrandingPubCode(), nextDate)) {
						break;
					}
					count++;
				}

				if (count == maxDaysForward) {
					nextDate = latest;
				}

			}
		} catch (Exception e) {
			nextDate = earliest;
			e.printStackTrace();
		}
		return nextDate;
	}

	public DateTime getFirstValidStartDateBefore(DateTime requestedDate) {
		DateTime nextDate = requestedDate;
		DateTime earliest = this.getEarliestPossibleStartDate();
		DateTime latest = this.getLatestPossibleStartDate();

		try {
			if (requestedDate.isBefore(earliest)) {
				return earliest;
			}

			if (requestedDate.isAfter(latest)) {
				return latest;
			}

			if (this.isElectronicDelivery()) {
				nextDate = nextDate.minusDays(1);
			} else {
				int count = 0;
				int maxDaysBackward = 10;

				SubscriptionPublishDateUtility dateUtil = SubscriptionPublishDateUtility.getInstance();

				while (count < maxDaysBackward) {
					nextDate = nextDate.minusDays(1);
					if (dateUtil.isValidPublishingDate(this.getBrandingPubCode(), nextDate)) {
						break;
					}
				}

				if (count == maxDaysBackward) {
					nextDate = earliest;
				}

			}
		} catch (Exception e) {
			nextDate = earliest;
			e.printStackTrace();
		}
		return nextDate;
	}

	public DateTime getEarliestPossibleHoldStartDate() {
		DateTime today = new DateTime();
		today = today.minusSeconds((today.getSecondOfDay() - 1));

		// value less than zero means no delay for processing (0 means first publishing day)
		if (this.getHoldDaysDelay() < 0) {
			return today;
		}

		SubscriptionPublishDateUtility publishDateUtils = SubscriptionPublishDateUtility.getInstance();

		int maxCount = 31;
		int count = 0;
		int pubDateCount = 0;

		boolean foundDate = false;

		// bump out the minimum time before we can place a hold
		// DateTime earliestHoldDate = today.plusDays(this.getHoldDaysDelay());
		DateTime earliestHoldDate = today;

		while (!foundDate && (count < maxCount)) {

			boolean validPubDate = publishDateUtils.isValidPublishingDate(this.getBrandingPubCode(), earliestHoldDate);

			count++;

			// first determine if the current day is a publishing day
			if (validPubDate) {
				pubDateCount++;
			} else {
				earliestHoldDate = earliestHoldDate.plusDays(1);
				continue;
			}

			// if we haven't gotten past the number of delay days
			// simply advance date and continue;
			if (pubDateCount <= this.getHoldDaysDelay()) {
				earliestHoldDate = earliestHoldDate.plusDays(1);
				continue;
			} else {
				// if we get here, we know it's a valid publishing date and we are beyond the delay threshold.
				foundDate = true;
			}
		}

		if (count == maxCount) {
			// default to X days out if there was a problem determining
			earliestHoldDate = today.plusDays(this.getHoldDaysDelay());
		}

		return earliestHoldDate;

	}

	public boolean isHoldDateValid(DateTime requestedHoldDate) throws UsatException {

		// valid if it's a publishing day, and it's >= today + minim delay
		SubscriptionPublishDateUtility publishDateUtils = SubscriptionPublishDateUtility.getInstance();

		if (publishDateUtils.isValidPublishingDate(this.getBrandingPubCode(), requestedHoldDate)) {

			DateTime thresholdDate = new DateTime();
			thresholdDate = thresholdDate.plusDays(this.getHoldDaysDelay() - 1);

			if (requestedHoldDate.isAfter(thresholdDate)) {
				return true;
			}

		}

		return false;
	}

	public int getHoldDaysDelay() {
		return holdDaysDelay;
	}
}
