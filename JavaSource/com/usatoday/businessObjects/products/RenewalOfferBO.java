/*
 * Created on Apr 14, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.business.interfaces.products.RenewalOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.businessObjects.products.promotions.PromotionManager;
import com.usatoday.businessObjects.products.promotions.PromotionSet;

/**
 * @author aeast
 * @date Apr 14, 2006
 * @class RenewalOfferBO
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class RenewalOfferBO extends OfferBO implements RenewalOfferIntf {

	private String rateCode = "";
	private String relatedRateCode = "";
	private String keyCode = "";

	public RenewalOfferBO(RenewalOfferIntf offer) {
		super();
		this.rateCode = offer.getRateCode();
		this.relatedRateCode = offer.getRelatedRateCode();
		this.pubCode = offer.getPubCode();
		this.keyCode = offer.getKeyCode();

		Collection<SubscriptionTermsIntf> termsSource = offer.getTerms();
		this.terms = new ArrayList<SubscriptionTermsIntf>();
		if (termsSource != null) {
			Iterator<SubscriptionTermsIntf> itr = termsSource.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf termIntf = itr.next();
				SubscriptionTermsBO term = new SubscriptionTermsBO(termIntf, null);
				this.terms.add(term);
			}
		}
	}

	public RenewalOfferBO(String rateCode, String relatedRateCode, Collection<SubscriptionTermsIntf> terms, String pubCode,
			String keyCode) {
		super();
		this.rateCode = rateCode;
		this.relatedRateCode = relatedRateCode;
		this.terms = terms;
		this.pubCode = pubCode;
		this.keyCode = keyCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.RenewalOfferIntf#getRateCode()
	 */
	public String getRateCode() {
		return rateCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.RenewalOfferIntf#getRelatedRateCode()
	 */
	public String getRelatedRateCode() {
		return relatedRateCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.OfferIntf#getKey()
	 */
	public String getKey() {
		// return this.pubCode + this.getRateCode() + this.getRelatedRateCode();
		return this.keyCode + this.pubCode;
	}

	@Override
	public PromotionSet getPromotionSet() {
		// renewals use only the default promos for the current product
		PromotionManager mngr = PromotionManager.getInstance();

		PromotionSet set = null;
		try {
			set = mngr.getDefaultPromotionSetForProduct(this.pubCode);
		} catch (Exception e) {
			System.out.println("RenewalOfferBO::getPromotionSet() - Exception : " + e.getMessage());
		}
		return set;
	}

	/**
	 * For a default renewal offer (not keycode based), we determine if it's forced if and only if all terms are forced.
	 */
	@Override
	public boolean isForceEZPay() {
		boolean requiresEZPay = true;
		if (this.terms != null && this.terms.size() > 0) {

			for (SubscriptionTermsIntf term : this.terms) {
				if (!term.requiresEZPAY()) {
					// if even one term doesn't require EZ-Pay then the renewal offer doesn't require it.
					requiresEZPay = false;
					break;
				}
			}

		} else {
			requiresEZPay = false; // no terms
		}
		return requiresEZPay;
	}

	@Override
	public SubscriptionTermsIntf getRenewalTerm(String termAsString) {
		return this.getTerm(termAsString);
	}

	public String getKeyCode() {
		return keyCode;
	}

}
