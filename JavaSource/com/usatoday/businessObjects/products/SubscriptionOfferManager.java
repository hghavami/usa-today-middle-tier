/*
 * Created on Apr 12, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products;

import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.RenewalOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.cache.CacheManager;
import com.usatoday.integration.SubscriptionTermsDAO;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Apr 12, 2006
 * @class SubscriptionRatesManager
 * 
 *        This singleton class caches the Subscription Offers associated with keycodes. It reduces the calls to the database by
 *        keeping frequently accessed data in memory.
 * 
 *        The clearRates() method should be called to clear the cache on some periodic basis to prevent the data from getting stale.
 * 
 */
public class SubscriptionOfferManager implements CacheManager {

	// static instance
	private static SubscriptionOfferManager srm = null;

	HashMap<String, SubscriptionOfferIntf> usatOffers = new HashMap<String, SubscriptionOfferIntf>();
	HashMap<String, SubscriptionOfferIntf> swOffers = new HashMap<String, SubscriptionOfferIntf>();
	HashMap<String, RenewalOfferIntf> usatRenewalOffers = new HashMap<String, RenewalOfferIntf>();
	HashMap<String, RenewalOfferIntf> swRenewalOffers = new HashMap<String, RenewalOfferIntf>();

	DateTime lastClearedTime = null;

	private String defaultUTKeyCode = "";
	private String defaultSWKeyCode = "";

	private SubscriptionOfferManager() {
		super();
		this.lastClearedTime = new DateTime();

		this.initSingleton();
	}

	/**
     * 
     *
     */
	private void initSingleton() {
		try {
			// initialize default offers
			SubscriptionProductIntf p = SubscriptionProductBO.getUsaTodaySubscriptionProduct();
			this.getUTGenesysOffer(p.getDefaultKeycode());
			this.getDefaultRenewalOffer(UsaTodayConstants.UT_PUBCODE);
		} catch (Exception exp) {
			// this exception is occuring
			System.out.println("SubscriptionRatesManager Constructor() - Exception loading default offers for UT: "
					+ exp.getMessage());
			exp.printStackTrace();
		}

		// EE
		try {
			SubscriptionProductIntf p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
			// getEEOffer(p.getDefaultKeycode());
			this.getEEGenesysOffer(p.getDefaultKeycode());
			this.getDefaultRenewalOffer(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
		} catch (Exception exp) {
			System.out.println("SubscriptionRatesManager Constructor() - Exception loading default offers for EE: "
					+ exp.getMessage());
			exp.printStackTrace();
		}
		// BW
		try {
			SubscriptionProductIntf p = SubscriptionProductBO.getSportsWeeklySubscriptionProduct();
			// getSWOffer(p.getDefaultKeycode());
			this.getSWGenesysOffer(p.getDefaultKeycode());
			this.getDefaultRenewalOffer(UsaTodayConstants.SW_PUBCODE);
		}

		catch (Exception exp) {
			System.out.println("SubscriptionRatesManager Constructor() - Exception loading default offers for BW: "
					+ exp.getMessage());
			exp.printStackTrace();
		}
	}

	/**
	 * Singleton retriever.
	 * 
	 * @return The instance of the Rates Manager
	 */
	public static synchronized final SubscriptionOfferManager getInstance() {
		if (SubscriptionOfferManager.srm == null) {
			SubscriptionOfferManager.srm = new SubscriptionOfferManager();
		}
		return SubscriptionOfferManager.srm;
	}

	/**
	 * Method to reset the cache so that offers will be updated from the database on subsequent requests.
	 * 
	 */
	public void clearCache() {
		synchronized (usatOffers) {
			usatOffers.clear();
		}
		synchronized (swOffers) {
			swOffers.clear();
		}
		synchronized (usatRenewalOffers) {
			usatRenewalOffers.clear();
		}
		synchronized (swRenewalOffers) {
			swRenewalOffers.clear();
		}

		this.initSingleton();

		this.lastClearedTime = new DateTime();
	}

	/**
	 * 
	 * @param keyCode
	 * @return The offer object for the specified keycode and UT pub
	 */
	public SubscriptionOfferIntf getUTOffer(String keyCode) throws UsatException {
		return this.getOffer(keyCode, UsaTodayConstants.UT_PUBCODE);
	}

	/**
	 * 
	 * @param keyCode
	 * @return The offer object for the specified keycode and UT pub
	 */

	// hdcons-153 start here
	// no account number during init process
	String accountNumber = null;
	String fodCode = null;
	String renewal = null;

	public SubscriptionOfferIntf getUTGenesysOffer(String keyCode) throws UsatException {
		return this.getGenesysOffer(keyCode, UsaTodayConstants.UT_PUBCODE, this.accountNumber, this.fodCode, this.renewal);

	}

	public RenewalOfferIntf getUTGenesysRenewalOffer(String keyCode) throws UsatException {
		return this.getGenesysRenewalOffer(keyCode, UsaTodayConstants.UT_PUBCODE, this.accountNumber, this.fodCode, "Y");

	}

	public SubscriptionOfferIntf getEEGenesysOffer(String keyCode) throws UsatException {
		return this.getGenesysOffer(keyCode, UsaTodayConstants.DEFAULT_UT_EE_PUBCODE, this.accountNumber, this.fodCode,
				this.renewal);
	}

	public RenewalOfferIntf getEEGenesysRenewalOffer(String keyCode) throws UsatException {
		return this.getGenesysRenewalOffer(keyCode, UsaTodayConstants.DEFAULT_UT_EE_PUBCODE, this.accountNumber, this.fodCode, "Y");
	}

	/**
	 * 
	 * @param keyCode
	 * @return The offer object for the specified keycode and SW pub
	 */
	public SubscriptionOfferIntf getSWGenesysOffer(String keyCode) throws UsatException {
		return this.getGenesysOffer(keyCode, UsaTodayConstants.SW_PUBCODE, this.accountNumber, this.fodCode, this.renewal);
	}

	public RenewalOfferIntf getSWGenesysRenewalOffer(String keyCode) throws UsatException {
		return this.getGenesysRenewalOffer(keyCode, UsaTodayConstants.SW_PUBCODE, this.accountNumber, this.fodCode, "Y");
	}

	/**
	 * 
	 * @param keyCode
	 * @param pubCode
	 * @return The offer object for the specified keycode and pubCode
	 */
	public RenewalOfferIntf getGenesysRenewalOffer(String keyCode, String pubCode, String accountNumber, String fodCode,
			String renewalCode) throws UsatException {
		RenewalOfferIntf offer = null;
		if (keyCode != null && pubCode != null) {

			SubscriptionProductIntf product = null;
			pubCode = pubCode.toUpperCase();

			product = SubscriptionProductBO.getSubscriptionProduct(pubCode);

			String brandingPub = product.getBrandingPubCode();
			// offers stored by branding pubcode not acutal pubcode
			if (brandingPub == null || UsaTodayConstants.UT_PUBCODE.equalsIgnoreCase(brandingPub)) {
				offer = (RenewalOfferIntf) this.usatRenewalOffers.get(keyCode + pubCode);
				// if (offer == null || offer.getTerms().isEmpty()) {
				if (offer != null) {
					this.usatRenewalOffers.remove(keyCode + pubCode);
				}
				offer = this.loadGenesysRenewalOffer(keyCode, pubCode, accountNumber, fodCode, renewalCode);
				if (offer != null && !offer.getTerms().isEmpty()) {
					this.usatRenewalOffers.put(offer.getKey(), offer);
				}
			} else { // Sports Weekly
				if (UsaTodayConstants.SW_PUBCODE.equalsIgnoreCase(product.getBrandingPubCode())) {
					offer = (RenewalOfferIntf) this.swRenewalOffers.get(keyCode + pubCode);
					// if (offer == null || offer.getTerms().isEmpty()) {
					if (offer != null) {
						this.swRenewalOffers.remove(keyCode + pubCode);
					}
					offer = this.loadGenesysRenewalOffer(keyCode, pubCode, accountNumber, fodCode, renewalCode);
					if (offer != null && !offer.getTerms().isEmpty()) {
						this.swRenewalOffers.put(offer.getKey(), offer);
					}
				}
			}
		}
		return offer;
	}

	/**
	 * 
	 * @param keyCode
	 * @param pubCode
	 * @return The offer object for the specified keycode and pubCode
	 */
	public SubscriptionOfferIntf getOffer(String keyCode, String pubCode) throws UsatException {

		SubscriptionOfferIntf offer = null;
		if (keyCode != null && pubCode != null) {
			offer = this.getGenesysOffer(keyCode, pubCode, this.accountNumber, this.fodCode, this.renewal);
			// SubscriptionProductIntf product = null;
			// pubCode = pubCode.toUpperCase();
			// product = SubscriptionProductBO.getSubscriptionProduct(pubCode);
			// String brandingPub = product.getBrandingPubCode();
			// // offers stored by branding pubcode not acutal pubcode
			// if (brandingPub == null || UsaTodayConstants.UT_PUBCODE.equalsIgnoreCase(brandingPub) ) {
			// offer = (SubscriptionOfferIntf)this.usatOffers.get(keyCode+pubCode);
			// if (offer == null) {
			// offer = this.loadOffer(keyCode, pubCode);
			// this.usatOffers.put(offer.getKey(), offer);
			// }
			// }
			// else { // Sports Weekly
			// if (UsaTodayConstants.SW_PUBCODE.equalsIgnoreCase(product.getBrandingPubCode()) ) {
			// offer = (SubscriptionOfferIntf)this.swOffers.get(keyCode+pubCode);
			// if (offer == null) {
			// offer = this.loadOffer(keyCode, pubCode);
			// this.swOffers.put(offer.getKey(), offer);
			// }
			// }
			// }
		}
		return offer;
	}

	/**
	 * 
	 * @param pubCode
	 * @param offerCode
	 * @return
	 * @throws UsatException
	 */
	// This method is no longer used after the migration to Genesys.
	// public SubscriptionOfferIntf getOfferForOfferCode(String pubCode, String offerCode) throws UsatException {
	// SubscriptionOfferIntf offer = null;
	// if (offerCode != null && pubCode != null) {
	// SubscriptionTermsDAO dao = new SubscriptionTermsDAO();
	//
	// String keyCode = dao.getKeycodeForOfferCode(pubCode, offerCode);
	//
	// if (keyCode != null) {
	// offer = this.getOffer(keyCode, pubCode);
	// }
	// }
	// return offer;
	// }

	/**
	 * This method loads an offer from the database
	 * 
	 * @param keyCode
	 * @param pubCode
	 * @return The offer that was loaded from thte database
	 */
	private synchronized SubscriptionOfferIntf loadGenesysOffer(String keyCode, String pubCode, String accountNumber,
			String fodCode, String renewalFlag) throws UsatException {
		SubscriptionOfferIntf offer = null;
		SubscriptionTermsDAO dao = new SubscriptionTermsDAO();

		SubscriptionOfferIntf offerIntf = (SubscriptionOfferIntf) dao.getGenesysOfferTerms(pubCode, accountNumber, keyCode,
				fodCode, renewalFlag);

		offer = new SubscriptionOfferBO(offerIntf);
		return offer;
	}

	private synchronized RenewalOfferIntf loadGenesysRenewalOffer(String keyCode, String pubCode, String accountNumber,
			String fodCode, String renewalFlag) throws UsatException {
		RenewalOfferIntf offer = null;
		SubscriptionTermsDAO dao = new SubscriptionTermsDAO();

		RenewalOfferIntf offerIntf = (RenewalOfferIntf) dao.getGenesysRenewalOfferTerms(pubCode, accountNumber, keyCode, fodCode,
				renewalFlag);

		offer = new RenewalOfferBO(offerIntf);
		return offer;
	}

	/**
	 * This method loads an offer from the database
	 * 
	 * @param keyCode
	 * @param pubCode
	 * @return The offer that was loaded from thte database
	 */
	// @SuppressWarnings("unused")
	// private synchronized SubscriptionOfferIntf loadOffer(String keyCode, String pubCode) throws UsatException {
	// SubscriptionOfferIntf offer = null;
	// SubscriptionTermsDAO dao = new SubscriptionTermsDAO();
	//
	// SubscriptionOfferIntf offerIntf = dao.getOffer(keyCode, pubCode);
	//
	// if (offerIntf == null) {
	// throw new UsatException("No Keycode Defined In Database for: " + keyCode + " Pub: " + pubCode);
	// }
	//
	// offer = new SubscriptionOfferBO(offerIntf);
	// return offer;
	// }

	/**
	 * 
	 * @param rateCode
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	// private synchronized RenewalOfferIntf loadRenewalOffer(String rateCode, String pubCode) throws UsatException {
	// RenewalOfferIntf offer = null;
	// SubscriptionTermsDAO dao = new SubscriptionTermsDAO();
	//
	// RenewalOfferIntf offerIntf = dao.getRenewalOffer(rateCode, pubCode);
	//
	// if (offerIntf == null) {
	// throw new UsatException("No Renewal Terms Defined In Database for: " + rateCode + " Pub: " + pubCode);
	// }
	//
	// offer = new RenewalOfferBO(offerIntf);
	// return offer;
	// }

	/**
	 * A method to find out the last time the cache was cleared.
	 * 
	 * @return Time the cache last cleared.
	 */
	public DateTime getLastClearedTime() {
		return this.lastClearedTime;
	}

	/**
	 * Method to get the number of offers currently cached for SW
	 * 
	 * @return
	 */
	public int getNumberSWRatesCached() {
		return this.swOffers.size();
	}

	public int getNumberSWRenewalRatesCached() {
		return this.swRenewalOffers.size();
	}

	/**
	 * Method to get the number of offers currently cached for UT
	 * 
	 * @return
	 */
	public int getNumberUTRatesCached() {
		return this.usatOffers.size();
	}

	public int getNumberUTRenewalRatesCached() {
		return this.usatRenewalOffers.size();
	}

	public String getDefaultSWKeyCode() {
		try {
			SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.BW_PUBCODE);
			this.defaultSWKeyCode = product.getDefaultKeycode();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return this.defaultSWKeyCode;
	}

	public String getDefaultUTKeyCode() {
		try {
			SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.UT_PUBCODE);
			this.defaultUTKeyCode = product.getDefaultKeycode();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.defaultUTKeyCode;
	}

	/**
	 * 
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	public SubscriptionOfferIntf getDefaultOffer(String pubCode) throws UsatException {

		SubscriptionOfferIntf offer = null;
		SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(pubCode);

		if (pubCode == null || UsaTodayConstants.UT_PUBCODE.equalsIgnoreCase(pubCode)) {
			// offer = this.getUTOffer(product.getDefaultKeycode());
			offer = this.getUTGenesysOffer(product.getDefaultKeycode());
		} else if (UsaTodayConstants.DEFAULT_UT_EE_PUBCODE.equalsIgnoreCase(pubCode)) {
			// offer = this.getEEOffer(product.getDefaultKeycode());
			offer = this.getEEGenesysOffer(product.getDefaultKeycode());
		} else {
			// offer = this.getSWOffer(product.getDefaultKeycode());
			offer = this.getSWGenesysOffer(product.getDefaultKeycode());
		}

		return offer;
	}

	/**
	 * 
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	public RenewalOfferIntf getDefaultRenewalOffer(String pubCode) throws UsatException {
		RenewalOfferIntf offer = null;
		SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(pubCode);
		if (pubCode == null || UsaTodayConstants.UT_PUBCODE.equalsIgnoreCase(pubCode)) {
			// offer = this.getRenewalOffer(UsaTodayConstants.DEFAULT_UT_RENEWAL_RATE_CODE, UsaTodayConstants.UT_PUBCODE);
//			offer = this.getUTGenesysRenewalOffer(product.getDefaultKeycode());
			offer = this.getUTGenesysRenewalOffer(product.getDefaultRenewalKeycode());
		} else {
//			offer = this.getSWGenesysRenewalOffer(product.getDefaultKeycode());
			offer = this.getSWGenesysRenewalOffer(product.getDefaultRenewalKeycode());
		}

		return offer;
	}

	/**
	 * 
	 * @param pubCode
	 * @param rateCode
	 * @return
	 */
	// public RenewalOfferIntf getRenewalOffer(String rateCode, String pubCode) throws UsatException {
	// RenewalOfferIntf offer = null;
	// if (rateCode != null && pubCode != null) {
	//
	// SubscriptionProductIntf product = null;
	// pubCode = pubCode.toUpperCase();
	//
	// product = SubscriptionProductBO.getSubscriptionProduct(pubCode);
	//
	// // offers stored by branding pubcode not acutal pubcode
	// if (UsaTodayConstants.UT_PUBCODE.equalsIgnoreCase(product.getBrandingPubCode())) {
	// offer = (RenewalOfferIntf) this.usatRenewalOffers.get(pubCode + rateCode + rateCode);
	// if (offer == null) {
	// offer = this.loadRenewalOffer(rateCode, pubCode);
	// this.usatRenewalOffers.put(offer.getKey(), offer);
	// }
	// } else { // Sports Weekly
	// if (UsaTodayConstants.SW_PUBCODE.equalsIgnoreCase(product.getBrandingPubCode())) {
	// offer = (RenewalOfferIntf) this.swRenewalOffers.get(pubCode + rateCode + rateCode);
	// if (offer == null) {
	// offer = this.loadRenewalOffer(rateCode, pubCode);
	// this.swRenewalOffers.put(offer.getKey(), offer);
	// }
	// }
	// }
	// }
	// return offer;
	// }

	public SubscriptionOfferIntf getEEOffer(String keyCode) throws UsatException {
		return this.getOffer(keyCode, UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
	}

	/**
	 * 
	 * @param keyCode
	 * @return The offer object for the specified keycode and SW pub
	 */
	public SubscriptionOfferIntf getSWOffer(String keyCode) throws UsatException {
		return this.getOffer(keyCode, UsaTodayConstants.SW_PUBCODE);
	}

	/**
	 * 
	 * @param keyCode
	 * @param pubCode
	 * @return The offer object for the specified keycode and pubCode
	 */
	public SubscriptionOfferIntf getGenesysOffer(String keyCode, String pubCode, String accountNumber, String fodCode,
			String renewalCode) throws UsatException {
		SubscriptionOfferIntf offer = null;
		if (keyCode != null && pubCode != null) {

			SubscriptionProductIntf product = null;
			pubCode = pubCode.toUpperCase();
			product = SubscriptionProductBO.getSubscriptionProduct(pubCode);
			String brandingPub = product.getBrandingPubCode();
			// offers stored by branding pubcode not acutal pubcode
			if (brandingPub == null || UsaTodayConstants.UT_PUBCODE.equalsIgnoreCase(brandingPub)) {
				offer = (SubscriptionOfferIntf) this.usatOffers.get(keyCode + pubCode);
				if (offer == null) {
					offer = this.loadGenesysOffer(keyCode, pubCode, accountNumber, fodCode, renewalCode);
					if (offer.getTerms().isEmpty()) {
						// Set the not found keycode values to be the same as the one for expired keycodes
						offer = (SubscriptionOfferIntf) this.usatOffers.get(product.getExpiredOfferKeycode() + pubCode);
						if (offer !=null && !offer.getTerms().isEmpty()) {
							this.usatOffers.put(keyCode +  pubCode, offer);							
						}
						offer = null;
//						offer = this.loadGenesysOffer(product.getExpiredOfferKeycode(), pubCode, accountNumber, fodCode, renewalCode);
					} else {
						this.usatOffers.put(offer.getKey(), offer);						
					}
				}
			} else { // Sports Weekly
				if (UsaTodayConstants.SW_PUBCODE.equalsIgnoreCase(product.getBrandingPubCode())) {
					offer = (SubscriptionOfferIntf) this.swOffers.get(keyCode + pubCode);
					if (offer == null) {
						offer = this.loadGenesysOffer(keyCode, pubCode, accountNumber, fodCode, renewalCode);
						if (offer.getTerms().isEmpty()) {
							// Set the not found keycode values to be the same as the one for expired keycodes
							offer = (SubscriptionOfferIntf) this.swOffers.get(product.getExpiredOfferKeycode() + pubCode);
							if (offer !=null && !offer.getTerms().isEmpty()) {
								this.swOffers.put(keyCode +  pubCode, offer);							
							}
							offer = null;
//							offer = this.loadGenesysOffer(product.getDefaultKeycode(), pubCode, accountNumber, fodCode, renewalCode);
						} else {
							this.swOffers.put(offer.getKey(), offer);							
						}
					}
				}
			}
		}
		return offer;
	}
}
