/*
 * Created on Apr 12, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.businessObjects.products.promotions.PromotionManager;
import com.usatoday.businessObjects.products.promotions.PromotionSet;

/**
 * @author aeast
 * @date Apr 12, 2006
 * @class SubscriptionOffer
 * 
 *        This read only class is used for retrieval of rates and terms for a given keycode/pubcode combination.
 * 
 */
public class SubscriptionOfferBO extends OfferBO implements SubscriptionOfferIntf {
	private boolean billMeAllowed = false;
	private double dailyRate = 0.0;
	private boolean forceEZPay = false;
	private boolean forceBillMe = false;
	private String keyCode = null;
	private boolean clubNumberRequired = false;
	private Collection<SubscriptionTermsIntf> renewalTerms = null;

	public SubscriptionOfferBO(Collection<SubscriptionTermsIntf> terms, Collection<SubscriptionTermsIntf> renewalTerms,
			String keyCode, String pubCode, boolean forceEZPay, boolean billMeAllowed, boolean clubNumberRequired,
			double dailyRate, boolean forceBillMe) {
		super();
		this.terms = terms;
		this.renewalTerms = renewalTerms;
		this.keyCode = keyCode;
		this.pubCode = pubCode;
		this.forceEZPay = forceEZPay;
		this.billMeAllowed = billMeAllowed;
		this.clubNumberRequired = clubNumberRequired;
		this.dailyRate = dailyRate;
		this.forceBillMe = forceBillMe;

		// call this last
		// this.processForceEZPAYTerm();
	}

	public SubscriptionOfferBO(SubscriptionOfferIntf offer) {
		super();

		this.terms = new ArrayList<SubscriptionTermsIntf>();

		ArrayList<SubscriptionTermsBO> tempTerms = new ArrayList<SubscriptionTermsBO>();

		// convert terms to BO
		Collection<SubscriptionTermsIntf> termsSource = offer.getTerms();
		if (termsSource != null) {
			Iterator<SubscriptionTermsIntf> itr = termsSource.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf termIntf = itr.next();
				SubscriptionTermsBO term = new SubscriptionTermsBO(termIntf, this);
				tempTerms.add(term);
			}

			this.terms.addAll(tempTerms);

		}

		this.renewalTerms = new ArrayList<SubscriptionTermsIntf>();

		// clear tempTerms for renewal term processing.
		tempTerms.clear();

		termsSource = offer.getRenewalTerms();
		if (termsSource != null) {
			Iterator<SubscriptionTermsIntf> itr = termsSource.iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf termIntf = itr.next();
				SubscriptionTermsBO term = new SubscriptionTermsBO(termIntf, this);
				tempTerms.add(term);
			}

			Collections.sort(tempTerms);
			// Collections.sort(tempTerms, Collections.reverseOrder());
			this.renewalTerms.addAll(tempTerms);

		}
		tempTerms.clear();

		this.keyCode = offer.getKeyCode();
		this.pubCode = offer.getPubCode();
		this.forceEZPay = offer.isForceEZPay();
		this.billMeAllowed = offer.isBillMeAllowed();
		this.forceBillMe = offer.isForceBillMe();
		this.clubNumberRequired = offer.isClubNumberRequired();
		this.dailyRate = offer.getDailyRate();

		// call this last
		// this.processForceEZPAYTerm();

		// re-sort terms
		for (SubscriptionTermsIntf term : this.terms) {
			SubscriptionTermsBO bo = (SubscriptionTermsBO) term;
			tempTerms.add(bo);
		}

		Collections.sort(tempTerms);
		// Collections.sort(tempTerms, Collections.reverseOrder());

		this.terms.clear();
		this.terms.addAll(tempTerms);

	}

	public double getDailyRate() {
		return this.dailyRate;
	}

	public String getKey() {
		return this.getKeyCode() + this.getPubCode();
	}

	public String getKeyCode() {
		return keyCode;
	}

	/**
	 * 
	 * @param basePrice
	 * @return The percentage savings, for example the String '25' for 25% off.
	 */
	public String getPercentOffBasePrice(double basePrice) {
		String savings = "";
		try {
			double baseProductPrice = basePrice;

			double dailyPrice = this.getDailyRate();

			double savingsDouble = 1.0 - (dailyPrice / baseProductPrice);

			java.math.BigDecimal bd = new java.math.BigDecimal(savingsDouble);

			bd = bd.multiply(new BigDecimal(100));

			bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP);

			savings = bd.toString();

		} catch (Exception e) {
			System.out.println("Exception calculating savings percentage: " + e.getMessage());
		}
		return savings;
	}

	/**
	 * Return the promotional objects related to this offer or null if no default exists The promotions manager will handle caching
	 * of the promtions.
	 * 
	 */
	public PromotionSet getPromotionSet() {
		PromotionManager mngr = PromotionManager.getInstance();

		PromotionSet set = null;
		try {
			set = mngr.getPromotionsForOffer(this);
		} catch (Exception e) {
			System.out.println("SubscriptionOfferBO::getPromotionSet() - Exception : " + e.getMessage());
		}
		return set;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.OfferIntf#getTerm(java.lang.String)
	 */
	public SubscriptionTermsIntf getRenewalTerm(String termAsString) {
		SubscriptionTermsIntf term = null;

		if (termAsString == null) {
			return term;
		}
		if (this.getRenewalTerms() != null) {
			Iterator<SubscriptionTermsIntf> itr = this.getRenewalTerms().iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf cTerm = (SubscriptionTermsIntf) itr.next();
				if (termAsString.equalsIgnoreCase(cTerm.getTermAsString())) {
					term = cTerm;
					break;
				}
			}
		}
		return term;
	}

	public Collection<SubscriptionTermsIntf> getRenewalTerms() {
		return this.renewalTerms;
	}

	public boolean isBillMeAllowed() {
		return this.billMeAllowed;
	}

	public boolean isForceEZPay() {
		return this.forceEZPay;
	}

	/**
	 * This method added as part of 2006 Extranet Phase II redesign. It supports the addition of a default term to be applied to any
	 * offer under certain conditions.
	 * 
	 * Condition1: Force EZPAY term feature is enabled for the publication Condition2: There is no current term with the same
	 * duration as the default Condition3: There is no XTRNTPROMO record indicating that the offer should be supressed.
	 * 
	 */
	// private void processForceEZPAYTerm() {
	// boolean programEnabled = false;
	// boolean forUT = true;
	//
	// // skip processing of default force ezpay offer if this is a force bill me keycode
	// // force bill me keycodes cannot have force ez-pay terms.
	// if (this.forceBillMe) {
	// return;
	// }
	//
	// String ezPayOVRD_Length = "";
	// if (this.pubCode.equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
	// programEnabled = UsaTodayConstants.UT_FORCE_EZPAY_TERM_ENABLED;
	// } else if (this.pubCode.equalsIgnoreCase(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE)) {
	// programEnabled = UsaTodayConstants.EE_FORCE_EZPAY_TERM_ENABLED;
	// } else {
	// programEnabled = UsaTodayConstants.SW_FORCE_EZPAY_TERM_ENABLED;
	// forUT = false;
	// }
	//
	// PromotionSet pSet = this.getPromotionSet();
	// PromotionIntf p1 = null;
	//
	// // For electronic products, the branding pub default promo set may be returend. Never use force EZ-Pay Setting of default
	// // pub
	// if (pSet != null && !pSet.getPubCode().equalsIgnoreCase(this.pubCode)) {
	// pSet = null;
	// }
	//
	// if (pSet != null) {
	// p1 = pSet.getForceEZPAY();
	// }
	//
	// if (programEnabled) {
	//
	// if (p1 != null) {
	// // Check for terms
	// SubscriptionTermsIntf fTerm_Default = null;
	//
	// if ((p1.getFulfillText().length() == 4) && (p1.getFulfillText().substring(0, 4).equals("OVRD"))) {
	// ezPayOVRD_Length = "OVRD";
	// fTerm_Default = SubscriptionTermsBO.getForceEZPAYTerm(this.pubCode, p1.getFulfillUrl());
	//
	// } else {
	// if ((p1.getFulfillText().length() == 7) && (p1.getFulfillText().substring(0, 4).equals("OVRD"))) {
	// ezPayOVRD_Length = p1.getFulfillText().substring(4, 7);
	// fTerm_Default = SubscriptionTermsBO.getForceEZPAYTerm(this.pubCode, p1.getFulfillUrl());
	//
	// }
	// }
	//
	// // if we get a Default Force EZ-PAY term
	// if (fTerm_Default != null) {
	// // Make sure a term of this length does not already exist
	// Iterator<SubscriptionTermsIntf> itr = this.getTerms().iterator();
	// while (itr.hasNext()) {
	// SubscriptionTermsIntf ct = (SubscriptionTermsIntf) itr.next();
	// if (ezPayOVRD_Length.equals("OVRD")) {
	// this.terms.add(fTerm_Default);
	// break;
	// } else {
	// if (ct.getDuration().equalsIgnoreCase(ezPayOVRD_Length)) {
	// this.terms.remove(ct);
	// this.terms.add(fTerm_Default);
	// break;
	// } else {
	// // TAKE NO ACTION
	// }
	//
	// }
	//
	// }// END WHILE
	// }
	// // }
	// } else {
	//
	// // Retrieve the term
	// SubscriptionTermsIntf fTerm = null;
	// if (forUT) {
	//
	// if (this.pubCode.equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
	// fTerm = SubscriptionTermsBO.getUtForceEZPAYTerm();
	// } else {
	// fTerm = SubscriptionTermsBO.getEEForceEZPAYTerm();
	// }
	// } else {
	// fTerm = SubscriptionTermsBO.getSwForceEZPAYTerm();
	// }
	//
	// // if we get a Force EZ-PAY term
	// if (fTerm != null) {
	// // Make sure a term of this length does not already exist
	// Iterator<SubscriptionTermsIntf> itr = this.getTerms().iterator();
	// boolean addTerm = true;
	// while (itr.hasNext()) {
	// SubscriptionTermsIntf ct = (SubscriptionTermsIntf) itr.next();
	// if (ct.getDuration() != null && ct.getDuration().equalsIgnoreCase(fTerm.getDuration())) {
	// addTerm = false;
	// break;
	// }
	// } // end while
	//
	// if (addTerm) {
	// // finally check that no promotion override exists for this keycode
	// // PromotionSet pSet = this.getPromotionSet();
	//
	// if (pSet == null) {
	// this.terms.add(fTerm);
	// } else {
	// PromotionIntf p = pSet.getForceEZPAY();
	// if (p == null || p.getFulfillText().equalsIgnoreCase("show")) {
	// this.terms.add(fTerm);
	// }
	// }
	// }
	// }
	// }
	// } // end program enabled
	// }

	public void setBillMeAllowed(boolean billMeAllowed) {
		this.billMeAllowed = billMeAllowed;
	}

	public void setForceEZPay(boolean forceEZPay) {
		this.forceEZPay = forceEZPay;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public void setRenewalTerms(Collection<SubscriptionTermsIntf> renewalTerms) {
		this.renewalTerms = renewalTerms;
	}

	public void setTerms(Collection<SubscriptionTermsIntf> terms) {
		this.terms = terms;
	}

	/**
	 * @param clubNumberRequired
	 *            the clubNumberRequired to set
	 */
	public void setClubNumberRequired(boolean clubNumberRequired) {
		this.clubNumberRequired = clubNumberRequired;
	}

	/**
	 * @return the clubNumberRequired
	 */
	public boolean isClubNumberRequired() {
		return this.clubNumberRequired;
	}

	public SubscriptionProductIntf getSubscriptionProduct() {
		SubscriptionProductIntf prod = null;
		try {
			if (this.pubCode != null) {
				prod = SubscriptionProductBO.getSubscriptionProduct(this.pubCode);
			}
		} catch (Exception e) {
		}
		return prod;
	}

	public boolean isForceBillMe() {
		return this.forceBillMe;
	}

	public void setForceBillMe(boolean forceBillMe) {
		this.forceBillMe = forceBillMe;
	}
}
