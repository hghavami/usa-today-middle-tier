/*
 * Created on May 19, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products;

import java.util.Collection;
import java.util.Iterator;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;

/**
 * @author aeast
 * @date May 19, 2006
 * @class OfferBO
 * 
 *        Abstract Offer Base class.
 * 
 */
public abstract class OfferBO implements OfferIntf {
	protected Collection<SubscriptionTermsIntf> terms = null;
	protected String pubCode = null;

	private ProductIntf product = null;

	/**
     * 
     */
	public OfferBO() {
		super();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.OfferIntf#getKey()
	 */
	public abstract String getKey();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.OfferIntf#getTerm(java.lang.String)
	 */
	public SubscriptionTermsIntf getTerm(String termAsString) {
		SubscriptionTermsIntf term = null;

		if (termAsString == null) {
			return term;
		}
		if (this.getTerms() != null) {
			Iterator<SubscriptionTermsIntf> itr = this.getTerms().iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf cTerm = (SubscriptionTermsIntf) itr.next();
				if (termAsString.equalsIgnoreCase(cTerm.getTermAsString())) {
					term = cTerm;
					break;
				}
			}
		}
		return term;
	}

	/**
     * 
     */
	public Collection<SubscriptionTermsIntf> getTerms() {
		return terms;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
		if (this.product != null) {
			this.product = null;
		}
	}

	/**
	 * method for backward compatibility...this is not bullet proof since some offers may have duplicate term length offers
	 */
	public SubscriptionTermsIntf findTermUsingTermLength(String termLength) {
		SubscriptionTermsIntf term = null;

		if (termLength == null) {
			return term;
		}
		if (this.getTerms() != null) {
			Iterator<SubscriptionTermsIntf> itr = this.getTerms().iterator();
			while (itr.hasNext()) {
				SubscriptionTermsIntf cTerm = (SubscriptionTermsIntf) itr.next();
				if (termLength.equalsIgnoreCase(cTerm.getDuration())) {
					term = cTerm;
					break;
				}
			}
		}
		return term;
	}

	public ProductIntf getProduct() throws UsatException {
		if (this.pubCode != null) {
			if (this.product != null) {
				return this.product;
			} else {
				this.product = ProductBO.fetchProduct(this.pubCode);
			}
		} else {
			return null;
		}
		return this.product;
	}
}
