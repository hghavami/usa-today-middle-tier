/*
 * Created on Apr 13, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products;

import java.math.BigDecimal;
import java.text.NumberFormat;

import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Apr 13, 2006
 * @class SubscriptionTermsBO
 * 
 *        This class represents a single subscription term
 * 
 */
public class SubscriptionTermsBO implements SubscriptionTermsIntf, Comparable<SubscriptionTermsIntf> {
	private String description = "";
	// TODO HOUMAN or SAM Delete the durationInWeeks and use duration since it may be months or weeks in the future. The duration
	// type will be W for weeks and M for months
	private String durationInWeeks = "";
	private double price = 0.0;
	private String piaRateCode = "";
	private String relatedRateCode = "";
	private String renewalRateCode = "";
	private String renewalPeriodLength = "";
	private boolean requiresEZPAY = false;
	private String pubCode = "";

	private String duration = "";
	private String durationType = ""; // Indicates Months or Weeks
	private String fodCode = "";

	private SubscriptionOfferIntf parentOffer = null;

	private String termAsString = null;
	private String disclaimerText = "";

	// business attributes
	// private static SubscriptionTermsBO utForceEZPAYTerm = null;
	// private static SubscriptionTermsBO swForceEZPAYTerm = null;
	// private static SubscriptionTermsBO eeForceEZPAYTerm = null;

	public String getDisclaimerText() {
		return disclaimerText;
	}

	public void setDisclaimerText(String disclaimerText) {
		this.disclaimerText = disclaimerText;
	}

	public SubscriptionTermsBO(SubscriptionTermsIntf term, SubscriptionOfferIntf parent) {
		super();
		this.description = term.getDescription();
		this.durationInWeeks = term.getDuration();
		this.price = term.getPrice();
		this.piaRateCode = term.getPiaRateCode();
		this.relatedRateCode = term.getRelatedRateCode();
		this.renewalRateCode = term.getRenewalRateCode();
		this.renewalPeriodLength = term.getRenewalPeriodLength();
		this.pubCode = term.getPubCode();
		this.requiresEZPAY = term.requiresEZPAY();
		this.duration = term.getDuration();
		this.durationType = term.getDurationType();
		this.fodCode = term.getFODCode();

		this.parentOffer = parent;
		this.disclaimerText = term.getDisclaimerText();

	}

	public SubscriptionTermsBO(String description, String durationInWeeks, double price, String piaRateCode,
			String relatedRateCode, String renewalRateCode, String renewalPeriodLength, String pubCode, String duration,
			String durationType, String fodCode, String disclaimerText) {
		super();
		this.description = description;
		this.durationInWeeks = durationInWeeks;
		this.price = price;
		this.piaRateCode = piaRateCode;
		this.relatedRateCode = relatedRateCode;
		this.renewalRateCode = renewalRateCode;
		this.renewalPeriodLength = renewalPeriodLength;
		this.pubCode = pubCode;
		this.duration = duration;
		this.durationType = durationType;
		this.fodCode = fodCode;
		this.disclaimerText = disclaimerText;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getDescription()
	 */
	public String getDescription() {
		return this.description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getDurationInWeeks()
	 */
	public String getDurationInWeeks() {
		return this.durationInWeeks;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getPrice()
	 */
	public double getPrice() {
		return price;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getPiaRateCode()
	 */
	public String getPiaRateCode() {
		return piaRateCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getRelatedRateCode()
	 */
	public String getRelatedRateCode() {
		return this.relatedRateCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getRenewalRateCode()
	 */
	public String getRenewalRateCode() {
		return renewalRateCode;
	}

	public String getRenewalPeriodLength() {
		return this.renewalPeriodLength;
	}

	public void setRenewalPeriodLength(String renewalPeriodLength) {
		this.renewalPeriodLength = renewalPeriodLength;
	}

	/**
	 * 
	 * @param product
	 * @return
	 */
	public String getSavingsPercentageString(SubscriptionProductIntf product) {
		if (product != null) {
			try {
				return this.getSavingsPercentageString(product.getBasePrice());
			} catch (Exception e) {
				// ignore
			}
		}
		return "";
	}

	/**
	 * 
	 * @param basePrice
	 * @return
	 */
	public String getSavingsPercentageString(double basePrice) {
		String savings = "";
		try {
			double baseProductPrice = basePrice;

			int termInWeeks = Integer.parseInt(this.getDurationInWeeks());

			int termInDays = 0;
			if (termInWeeks > 0) {
				termInDays = termInWeeks * 5;
			}

			double dailyPrice = 0.0;
			if (termInDays > 0) {
				dailyPrice = this.getPrice() / termInDays;
			}

			double savingsDouble = 1.0 - (dailyPrice / baseProductPrice);

			java.math.BigDecimal bd = new java.math.BigDecimal(savingsDouble);

			bd = bd.multiply(new BigDecimal(100));

			bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP);

			savings = bd.toString();

		} catch (Exception e) {
			System.out.println("Exception calculating savings percentage: " + e.getMessage());
		}
		return savings;
	}

	/**
	 * @return Returns the requiresEZPAY.
	 */
	public boolean requiresEZPAY() {
		return this.requiresEZPAY;
	}

	/**
	 * @param requiresEZPAY
	 *            The requiresEZPAY to set. Only set to true for the default Force EZ-Pay Term
	 */
	protected void setRequiresEZPAY(boolean requiresEZPAY) {
		this.requiresEZPAY = requiresEZPAY;
	}

	/**
	 * @return Returns the swForceEZPAYTerm.
	 */
	// public static synchronized SubscriptionTermsIntf getSwForceEZPAYTerm() {
	// SubscriptionTermsIntf term = null;
	// if (SubscriptionTermsBO.swForceEZPAYTerm != null) {
	// // copy cached value
	// term = new SubscriptionTermsBO(SubscriptionTermsBO.swForceEZPAYTerm, null);
	// }
	// else {
	// term = SubscriptionTermsBO.getForceEZPAYTerm(UsaTodayConstants.SW_PUBCODE,UsaTodayConstants.SW_FORCE_EZPAY_RATECODE );
	// if (term != null) {
	// SubscriptionTermsBO.swForceEZPAYTerm = (SubscriptionTermsBO)term;
	// }
	// }
	// return term;
	// }
	/**
	 * @return Returns the ForceEZPAYTerm.
	 */
	// public static SubscriptionTermsIntf getForceEZPAYTerm(String pub, String ratecode) {
	// SubscriptionTermsIntf term = null;
	// SubscriptionTermsBO ovrdForceEZPAYTerm = null;
	// try {
	// // load it from DAO
	// SubscriptionTermsDAO dao = new SubscriptionTermsDAO();
	// SubscriptionTermsIntf t = dao.getTerm(ratecode,pub);
	//
	// if (t != null) {
	// ovrdForceEZPAYTerm = new SubscriptionTermsBO(t, null);
	// ovrdForceEZPAYTerm.setRequiresEZPAY(true);
	// }
	// }
	// catch (UsatException e) {
	// System.out.println("Unable to retrieve the Force EZPay Override Term. None will be used.");
	// ovrdForceEZPAYTerm = null;
	// }
	// term = ovrdForceEZPAYTerm;
	// return term;
	// }

	/**
	 * @return Returns the utForceEZPAYTerm.
	 */
	// public static synchronized SubscriptionTermsIntf getUtForceEZPAYTerm() {
	// SubscriptionTermsIntf term = null;
	// if (SubscriptionTermsBO.utForceEZPAYTerm != null) {
	// // make a copy of the saved term
	// term = new SubscriptionTermsBO(SubscriptionTermsBO.utForceEZPAYTerm, null);
	// }
	// else {
	// term = SubscriptionTermsBO.getForceEZPAYTerm( UsaTodayConstants.UT_PUBCODE,UsaTodayConstants.UT_FORCE_EZPAY_RATECODE);
	// if (term != null) {
	// SubscriptionTermsBO.utForceEZPAYTerm = (SubscriptionTermsBO)term;
	// }
	// }
	// return term;
	// }

	// public static synchronized SubscriptionTermsIntf getEEForceEZPAYTerm() {
	// SubscriptionTermsIntf term = null;
	// if (SubscriptionTermsBO.eeForceEZPAYTerm != null) {
	// // copy cached term
	// term = new SubscriptionTermsBO(SubscriptionTermsBO.eeForceEZPAYTerm, null);
	// }
	// else {
	// term = SubscriptionTermsBO.getForceEZPAYTerm(
	// UsaTodayConstants.DEFAULT_UT_EE_PUBCODE,UsaTodayConstants.EE_FORCE_EZPAY_RATECODE);
	// if (term != null) {
	// SubscriptionTermsBO.eeForceEZPAYTerm = (SubscriptionTermsBO)term;
	// }
	// }
	// return term;
	// }

	/**
	 * This method may be used during a configuration reset to clear the cached defaults.
	 * 
	 */
	// public static void resetDefaultForceEZPAYTerms() {
	// SubscriptionTermsBO.swForceEZPAYTerm = null;
	// SubscriptionTermsBO.utForceEZPAYTerm = null;
	// SubscriptionTermsBO.eeForceEZPAYTerm = null;
	//
	// }

	public double getDailyRate() {
		// TODO: Change this so publishing frequency is not hardcoded.
		// We may need to add this field instead of calc it if we mess
		// with Freq of delivery in the future. (Check DB to see if this is stored on account)

		double dailyRate = 0.0;
		int publishingFrequecy = 5;
		if (this.getPubCode().equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
			publishingFrequecy = 1;
		}

		int durationInWeeksInt = Integer.parseInt(this.getDurationInWeeks());
		int issues = durationInWeeksInt * publishingFrequecy;

		if (issues > 0) {
			dailyRate = this.getPrice() / issues;
		}

		BigDecimal bd = new BigDecimal(dailyRate);
		bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);

		return bd.doubleValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.SubscriptionTermsIntf#getPubCode()
	 */
	public String getPubCode() {
		return this.pubCode;
	}

	/**
	 * The term in the format [PUB]_[RATE CODE]_[DURATION IN WEEKS]_[REQUIRES EZPAY]
	 */
	public String getTermAsString() {
		if (this.termAsString == null) {
			StringBuilder sb = new StringBuilder();

			sb.append(this.getPubCode()).append("_");
			if (this.getParentOffer() != null) {
				sb.append(this.getParentOffer().getKeyCode()).append("_");
			} else {
				// no parent keycode specified.
				sb.append("00000_");
			}

			sb.append(this.getPiaRateCode()).append("_");
			sb.append(this.getDurationInWeeks()).append("_");
			sb.append(this.getPriceAsString()).append("_");
			if (this.requiresEZPAY() || (this.parentOffer != null && this.parentOffer.isForceEZPay())) {
				sb.append("true");
			} else {
				sb.append("false");
			}

			sb.append("_" + this.getRenewalPeriodLength());

			this.termAsString = sb.toString();
		}

		return this.termAsString;
	}

	public SubscriptionOfferIntf getParentOffer() {
		return parentOffer;
	}

	public void setParentOffer(SubscriptionOfferIntf parentOffer) {
		this.parentOffer = parentOffer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(SubscriptionTermsIntf o) {
		SubscriptionTermsIntf otherObj = (SubscriptionTermsIntf) o;
		Integer sourceInt = new Integer(this.getDuration());
		Integer compareToInt = new Integer(otherObj.getDuration());

		return sourceInt.compareTo(compareToInt);
	}

	@Override
	public String getPriceAsString() {

		double price = this.price;
		String priceString = null;

		try {
			NumberFormat format = NumberFormat.getCurrencyInstance();
			priceString = format.format(price);
		} catch (Exception e) {
			priceString = "";
		}
		return priceString;
	}

	@Override
	public String getDuration() {
		return this.duration;
	}

	@Override
	public String getDurationType() {
		return this.durationType;
	}

	@Override
	public String getFODCode() {
		return this.fodCode;
	}
}
