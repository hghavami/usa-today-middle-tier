/*
 * Created on Aug 18, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products.promotions;

import com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

/**
 * @author aeast
 * @date Aug 18, 2006
 * @class ImagePromotionBO
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class ImagePromotionBO extends PromotionBO implements ImagePromotionIntf {

	/**
	 * @param promotion
	 */
	public ImagePromotionBO(PromotionIntf promotion) {
		super(promotion);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf#getImagePathString()
	 */
	public String getImagePathString() {
		// the image path is stored in the name field
		return this.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf#getImageAltText()
	 */
	public String getImageAltText() {
		return this.getAltName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf#getImageLinkToURL()
	 */
	public String getImageLinkToURL() {
		return this.getFulfillUrl();
	}
}
