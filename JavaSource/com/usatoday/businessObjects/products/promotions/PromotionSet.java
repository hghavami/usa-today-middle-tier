/*
 * Created on May 5, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products.promotions;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf;
import com.usatoday.business.interfaces.products.promotions.JavaScriptPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;

/**
 * @author aeast
 * @date May 5, 2006
 * @class PromotionSet
 * 
 *        This class represents promotional text and graphics that may be used
 *        with an image.
 * 
 */
public class PromotionSet {

	private PromotionSet defaultBrandingPubSet = null;
	private PromotionSet defaultPubCodeSet = null;

	private String keyCode = null;
	private HashMap<String, PromotionIntf> promoConfigs = new HashMap<String, PromotionIntf>();
	private String pubCode = null;

	/**
     * 
     */
	public PromotionSet(Collection<PromotionIntf> promotions, String pubCode, String keyCode) {
		super();

		this.pubCode = pubCode;
		this.keyCode = keyCode;

		try {
			// get this product
			SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(pubCode);
			// get branding product (may be the same)
			SubscriptionProductIntf brandingProduct = SubscriptionProductBO.getSubscriptionProduct(product.getBrandingPubCode());

			// if product code doesn't match the branding pub code set up both
			// defaults
			if (!product.getProductCode().equalsIgnoreCase(product.getBrandingPubCode())) {
				// set the default branding promotions set
				this.defaultBrandingPubSet = PromotionManager.getInstance().getPromotionsForOffer(brandingProduct.getProductCode(),
						PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE);

			}

			// set default set if this offer is not for the default keycode
			if (!this.keyCode.equalsIgnoreCase(PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE)) {
				this.defaultPubCodeSet = PromotionManager.getInstance().getPromotionsForOffer(product.getProductCode(),
						PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (promotions != null) {
			Iterator<PromotionIntf> itr = promotions.iterator();

			while (itr.hasNext()) {
				PromotionIntf p = itr.next();
				if (this.promoConfigs.containsKey(p.getType())) {
					if (p.getType().equalsIgnoreCase(PromotionIntf.CREDIT_CARD)) {
						CreditCardPromotionBO cBO = (CreditCardPromotionBO) this.promoConfigs.get(p.getType());
						try {
							cBO.addCard(p);
						} catch (Exception e) {
							System.out.println("Failed to add card to promotion set: " + e.getMessage());
						}
					}
				} else {
					try {
						this.promoConfigs.put(p.getType(), PromotionBO.createPromotionBO(p));
					} catch (Exception e) {
						System.out.println("Failed to create PromotionBO: " + e.getMessage());
					}
				}
			}
		} // end if promos not null;
	}

	public PromotionIntf getAllowOfferOverrides() {
		PromotionIntf promo = null;
		try {

			promo = this.getPromoConfigs().get(PromotionIntf.ALLOW_OFFER_OVERRIDE);

			if (promo == null) {
				promo = this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.ALLOW_OFFER_OVERRIDE);
			}
		} catch (Exception e) {
			return null;
		}
		return promo;
	}

	public ImagePromotionIntf getCustomerServicePathPopOverlayPromoImage() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.CUTOMER_SERVICE_PATH_POP_OVERLAY);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.CUTOMER_SERVICE_PATH_POP_OVERLAY);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public PromotionIntf getCustomerServicePhoneCheck() {
		PromotionIntf promo = null;
		//
		try {
			promo = this.getCustomerServicePhoneCheck0();
			if (promo == null) {
				promo = this.getCustomerServicePhoneCheck1();
				if (promo == null) {
					promo = this.getCustomerServicePhoneCheck2();
					if (promo == null) {
						promo = this.getCustomerServicePhoneCheck3();
						if (promo == null) {
							promo = this.getCustomerServicePhoneCheck4();
							if (promo == null) {
								// check most default
								promo = this.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC);
								if (promo == null) {
									if (this.defaultPubCodeSet != null) {
										promo = this.defaultPubCodeSet.getPromoConfigs().get(
												PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC);
									}
								}
								if (promo != null) {
									// if no key code specific setting apply
									if (promo.getFulfillUrl() == null || promo.getFulfillUrl().equalsIgnoreCase("ALL")) {
										return promo;
									} else {
										if (this.getKeyCode().substring(0, 1).equalsIgnoreCase(promo.getFulfillUrl().trim())) {
											// this promo applies to this
											// keycode
											return promo;
										} else {
											promo = null;
										}
									}

								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			promo = null;
		}
		return promo;

	}

	protected PromotionIntf getCustomerServicePhoneCheck0() {
		PromotionIntf promo = null;
		try {
			promo = this.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC0);

			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC0);
				}
			}
			// //
			if (promo != null) {
				// if no key code specific setting apply
				if (promo.getFulfillUrl() == null || promo.getFulfillUrl().trim().length() == 0
						|| promo.getFulfillUrl().equalsIgnoreCase("ALL")) {
					return promo;
				} else {
					if (this.getKeyCode().substring(0, 1).equalsIgnoreCase(promo.getFulfillUrl().trim())) {
						// this promo applies to this keycode
						return promo;
					} else {
						promo = null;
					}
				}

			}
		} catch (Exception e) {
			promo = null;
		}
		return promo;
	}

	protected PromotionIntf getCustomerServicePhoneCheck1() {
		PromotionIntf promo = null;
		try {
			promo = this.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC1);

			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC1);
				}
			}
			// //
			if (promo != null) {
				// if no key code specific setting apply
				if (promo.getFulfillUrl() == null || promo.getFulfillUrl().trim().length() == 0
						|| promo.getFulfillUrl().equalsIgnoreCase("ALL")) {
					return promo;
				} else {
					if (this.getKeyCode().substring(0, 1).equalsIgnoreCase(promo.getFulfillUrl().trim())) {
						// this promo applies to this keycode
						return promo;
					} else {
						promo = null;
					}
				}

			}
		} catch (Exception e) {
			promo = null;
		}
		return promo;
	}

	protected PromotionIntf getCustomerServicePhoneCheck2() {
		PromotionIntf promo = null;
		try {
			promo = this.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC2);

			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC2);
				}
			}
			// //
			if (promo != null) {
				// if no key code specific setting apply
				if (promo.getFulfillUrl() == null || promo.getFulfillUrl().trim().length() == 0
						|| promo.getFulfillUrl().equalsIgnoreCase("ALL")) {
					return promo;
				} else {
					if (this.getKeyCode().substring(0, 1).equalsIgnoreCase(promo.getFulfillUrl().trim())) {
						// this promo applies to this keycode
						return promo;
					} else {
						promo = null;
					}
				}

			}
		} catch (Exception e) {
			promo = null;
		}
		return promo;
	}

	protected PromotionIntf getCustomerServicePhoneCheck3() {
		PromotionIntf promo = null;
		try {
			promo = this.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC3);

			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC3);
				}
			}
			// //
			if (promo != null) {
				// if no key code specific setting apply
				if (promo.getFulfillUrl() == null || promo.getFulfillUrl().trim().length() == 0
						|| promo.getFulfillUrl().equalsIgnoreCase("ALL")) {
					return promo;
				} else {
					if (this.getKeyCode().substring(0, 1).equalsIgnoreCase(promo.getFulfillUrl().trim())) {
						// this promo applies to this keycode
						return promo;
					} else {
						promo = null;
					}
				}

			}
		} catch (Exception e) {
			promo = null;
		}
		return promo;
	}

	protected PromotionIntf getCustomerServicePhoneCheck4() {
		PromotionIntf promo = null;
		try {
			promo = this.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC4);

			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC4);
				}
			}
			// //
			if (promo != null) {
				// if no key code specific setting apply
				if (promo.getFulfillUrl() == null || promo.getFulfillUrl().trim().length() == 0
						|| promo.getFulfillUrl().equalsIgnoreCase("ALL")) {
					return promo;
				} else {
					if (this.getKeyCode().substring(0, 1).equalsIgnoreCase(promo.getFulfillUrl().trim())) {
						// this promo applies to this keycode
						return promo;
					} else {
						promo = null;
					}
				}

			}
		} catch (Exception e) {
			promo = null;
		}
		return promo;
	}

	/**
	 * 
	 * @return
	 */
	public PromotionIntf getCVVRequiredOverride() {
		PromotionIntf promo = null;
		try {

			// this setting can only ever be set at a keycode level since cvv is
			// always required.
			promo = this.getPromoConfigs().get(PromotionIntf.CVV_REQUIRED_FOR_ORDER);

		} catch (Exception e) {
			return null;
		}
		return promo;
	}

	/**
	 * 
	 * @return
	 */
	public PromotionIntf getAVSRequiredOverride() {
		PromotionIntf promo = null;
		try {

			// this setting can only ever be set at a keycode level since avs is
			// always required.
			promo = this.getPromoConfigs().get(PromotionIntf.AVS_REQUIRED_FOR_ORDER);

		} catch (Exception e) {
			return null;
		}
		return promo;
	}

	public PromotionIntf getDeliveryNotification() {
		try {
			PromotionIntf promo = this.getPromoConfigs().get(PromotionIntf.DELIVERY_NOTIFICATION);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.DELIVERY_NOTIFICATION);
				}
				// don't check branding pub for this as it should be more
				// product specific
				// if (promo == null && this.defaultBrandingPubSet != null) {
				// promo =
				// (HTMLPromotionIntf)this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.DELIVERY_NOTIFICATION);
				// }
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public PromotionIntf getEZPayFreeWeeks() {
		try {
			PromotionIntf promo = this.getPromoConfigs().get(PromotionIntf.EZPAY_FREE_WEEKS);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.EZPAY_FREE_WEEKS);
				}
				// don't check branding pub for this as it should be more
				// product specific
				// if (promo == null && this.defaultBrandingPubSet != null) {
				// promo =
				// (HTMLPromotionIntf)this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.DELIVERY_NOTIFICATION);
				// }
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public PromotionIntf getEECancelsAllowed() {
		try {
			return this.getPromoConfigs().get(PromotionIntf.EE_CANCELS_ALLOWED);
		} catch (Exception e) {
			return null;
		}
	}

	public PromotionIntf getForceEZPAY() {
		try {
			return this.getPromoConfigs().get(PromotionIntf.FORCE_EZPAY_OFFER);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @return Returns the keyCode.
	 */
	public String getKeyCode() {
		return this.keyCode;
	}

	public ImagePromotionIntf getLandingPagePromoImage1() {
		try {
			return (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_1);
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getLandingPagePromoImage2() {
		try {
			return (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_2);
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getLandingPagePromoImage3() {
		try {
			return (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_3);
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getOrderPathPopOverlayPromoImage() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.ORDER_PATH_POP_OVERLAY);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.ORDER_PATH_POP_OVERLAY);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ORDER_PATH_POP_OVERLAY);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoAccountInfoImageSpot1() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.ACCOUNT_INFO_IMAGE_SPOT1);
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoAccountInfoImageSpot2() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.ACCOUNT_INFO_IMAGE_SPOT2);
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoAccountInfoImageSpot3() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.ACCOUNT_INFO_IMAGE_SPOT3);
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public HashMap<String, PromotionIntf> getPromoConfigs() {
		return this.promoConfigs;
	}

	/**
	 * 
	 * @return
	 */
	public CreditCardPromotionIntf getPromoCreditCard() {
		try {
			CreditCardPromotionIntf promo = (CreditCardPromotionIntf) this.getPromoConfigs().get(PromotionIntf.CREDIT_CARD);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (CreditCardPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.CREDIT_CARD);
				}
				if (promo == null) {
					promo = (CreditCardPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.CREDIT_CARD);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoEZPayCustomText() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.EZPAY_PROMO);
			if (promo == null) {
				promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.EZPAY_PROMO);
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoHeaderTextLandingPage() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.LANDING_PAGE_PROMO_TEXT);
			if (promo == null) {
				promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.LANDING_PAGE_PROMO_TEXT);
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoImageComplete() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.IMAGE_COMPLETE);
			if (promo == null) {
				promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.IMAGE_COMPLETE);
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getDynamicNavigationCustServiceHTML() {
		HTMLPromotionIntf promo = null;
		try {
			promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.DYNAMIC_NAVIGATION_CUSTSERV);
			if (this.defaultPubCodeSet != null) {
				promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.DYNAMIC_NAVIGATION_CUSTSERV);
			}
		} catch (Exception e) {
			return null;
		}
		return promo;
	}

	public HTMLPromotionIntf getDynamicNavigationOrderEntryHTML() {
		HTMLPromotionIntf promo = null;
		try {
			promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.DYNAMIC_NAVIGATION_ORDER_ENTRY);
			if (this.defaultPubCodeSet != null) {
				promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
						PromotionIntf.DYNAMIC_NAVIGATION_ORDER_ENTRY);
			}
		} catch (Exception e) {
			return null;
		}
		return promo;
	}

	public HTMLPromotionIntf getPromoLandingPageText() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.LANDING_PAGE_PROMO_TEXT);
			if (promo == null) {
				promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.LANDING_PAGE_PROMO_TEXT);
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoOnePageDisclaimerText() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_DISCLAIMER_TEXT);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs()
							.get(PromotionIntf.ONE_PAGE_DISCLAIMER_TEXT);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_DISCLAIMER_TEXT);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoOnePageForceEZPayCustomTermsText() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_FORCE_EZPAY_TERMS_TEXT);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_FORCE_EZPAY_TERMS_TEXT);
				}
				// don't check branding pub for this as it should be more
				// product specific
				// if (promo == null && this.defaultBrandingPubSet != null) {
				// promo =
				// (HTMLPromotionIntf)this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_FORCE_EZPAY_TERMS_TEXT);
				// }
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageJavaScript1Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_1);
				}
				if (promo == null) {
					promo = (JavaScriptPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_SCRIPT_1);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageJavaScript2Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_2);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_2);
				}
				if (promo == null) {
					promo = (JavaScriptPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_SCRIPT_2);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageJavaScript3Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_3);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_3);
				}
				if (promo == null) {
					promo = (JavaScriptPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_SCRIPT_3);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public String getPromoOnePageJavaScriptTealiumText() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_TEALIUM);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_TEALIUM);
				}
				if (promo == null) {
					promo = (JavaScriptPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_SCRIPT_TEALIUM);
				}
			}
			// This script is used for Tealium, therefore, populate pub code and keycode
			String tempString = promo.getJavaScript();
			tempString = StringUtils.replace(tempString, "keycode:\"\"", "keycode:\"" + keyCode + "\"");
			tempString = StringUtils.replace(tempString, "pubcode:\"\"", "pubcode:\"" + pubCode + "\"");
			return tempString;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageJavaScript4Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_4);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_4);
				}
				if (promo == null) {
					promo = (JavaScriptPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_SCRIPT_4);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageJavaScript5Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_5);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_SCRIPT_5);
				}
				if (promo == null) {
					promo = (JavaScriptPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_SCRIPT_5);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoOnePageLeftColSpot1HTML() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_LEFT_COL_HTML_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_LEFT_COL_HTML_SPOT_1);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_LEFT_COL_HTML_SPOT_1);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getTermsDescriptionTextEzpay() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.TERMS_DESCRIPTION_TEXT_EZPAY);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.TERMS_DESCRIPTION_TEXT_EZPAY);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.TERMS_DESCRIPTION_TEXT_EZPAY);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getTermsDescriptionText() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.TERMS_DESCRIPTION_TEXT);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.TERMS_DESCRIPTION_TEXT);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.TERMS_DESCRIPTION_TEXT);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoOnePageLeftColSpot1Image() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs()
					.get(PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_1);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_1);
				}
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoOnePageLeftColSpot2Image() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs()
					.get(PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_2);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_2);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_2);
				}
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}


	public HTMLPromotionIntf getPromoOnePageLeftColVideoSpot1HTML() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_LEFT_COL_HTML_VIDEO_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_LEFT_COL_HTML_VIDEO_SPOT_1);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_LEFT_COL_HTML_VIDEO_SPOT_1);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoOnePageLeftColVideoSpot1Image() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_LEFT_COL_VIDEO_POP_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_LEFT_COL_VIDEO_POP_SPOT_1);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_LEFT_COL_VIDEO_POP_SPOT_1);
				}
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoOnePageRightColSpot1HTML() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_SPOT_1);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_SPOT_1);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoOnePageRightColSpot1Image() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_1);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_1);
				}
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoOnePageRightColSpot2HTML() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_SPOT_2);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_SPOT_2);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_SPOT_2);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoOnePageRightColSpot2Image() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_2);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_2);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_2);
				}
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoOnePageRightColSpot3Image() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_3);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_3);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_3);
				}
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoOnePageRightColVideoSpot1HTML() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_VIDEO_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_VIDEO_SPOT_1);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_VIDEO_SPOT_1);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoOnePageRightColVideoSpot1Image() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_RIGHT_COL_VIDEO_POP_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_VIDEO_POP_SPOT_1);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_RIGHT_COL_VIDEO_POP_SPOT_1);
				}
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoOnePageText1() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1);
				}
				if (promo == null && this.defaultBrandingPubSet != null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript10Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_10);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_10);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript1Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_1);
				}
				// don't use branding pub settings for javascript
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript2Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_2);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_2);
				}
				// don't use branding pub settings for javascript
				// if (promo == null) {
				// promo =
				// (JavaScriptPromotionIntf)this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_2);
				// }
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript3Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_3);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_3);
				}
				// don't use branding pub settings for javascript
				// if (promo == null) {
				// promo =
				// (JavaScriptPromotionIntf)this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_2);
				// }
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript4Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_4);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_4);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript5Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_5);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_5);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript6Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_6);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_6);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript7Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_7);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_7);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript8Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_8);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_8);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript9Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_9);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_9);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoOnePageThankYouLeftColSpot1HTML() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANK_YOU_LEFT_COL_HTML_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_LEFT_COL_HTML_SPOT_1);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_LEFT_COL_HTML_SPOT_1);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoOnePageThankYouLeftColSpot1Image() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANK_YOU_LEFT_COL_IMAGE_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_LEFT_COL_IMAGE_SPOT_1);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_LEFT_COL_IMAGE_SPOT_1);
				}
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoOnePageThankYouRightColSpot1HTML() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_1);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_1);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoOnePageThankYouRightColSpot1Image() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_1);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_1);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_1);
				}
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getPromoOnePageThankYouRightColSpot2HTML() {
		try {
			HTMLPromotionIntf promo = (HTMLPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_2);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (HTMLPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_2);
				}
				if (promo == null) {
					promo = (HTMLPromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_2);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getPromoOnePageThankYouRightColSpot2Image() {
		try {
			ImagePromotionIntf promo = (ImagePromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_2);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_2);
				}
				if (promo == null) {
					promo = (ImagePromotionIntf) this.defaultBrandingPubSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_2);
				}
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @return Returns the pubCode.
	 */
	public String getPubCode() {
		return this.pubCode;
	}

	public ImagePromotionIntf getProductImage1() {
		ImagePromotionIntf promo = null;
		try {
			promo = (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.PRODUCT_IMAGE_1);

			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (ImagePromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(PromotionIntf.PRODUCT_IMAGE_1);
				}
				// don't check branding pub for this as it should be more
				// product specific
				// if (promo == null && this.defaultBrandingPubSet != null) {
				// promo =
				// (ImagePromotionIntf)this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.PRODUCAT_IMAGE_1);
				// }
			}
			return promo;

		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getRightColumnLowerPromoImageLandingPage() {
		try {
			return (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120);
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getRightColumnUpperPromoImageLandingPage() {
		try {
			return (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90);
		} catch (Exception e) {
			return null;
		}
	}

	public ImagePromotionIntf getTemplateNavigationPromoImage() {
		try {
			return (ImagePromotionIntf) this.getPromoConfigs().get(PromotionIntf.TEMPLATE_NAV_PROMO_150x90);
		} catch (Exception e) {
			return null;
		}
	}

	public HTMLPromotionIntf getTermsAndConditionsText() {
		try {
			return (HTMLPromotionIntf) this.getPromoConfigs().get(PromotionIntf.TERMS_AND_CONDITIONS_TEXT);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param keyCode
	 *            The keyCode to set.
	 */
	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	void setPromoConfigs(HashMap<String, PromotionIntf> promoConfigs) {
		this.promoConfigs = promoConfigs;
	}

	/**
	 * @param pubCode
	 *            The pubCode to set.
	 */
	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript20Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_20);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_20);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript11Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_11);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_11);
				}
				// don't use branding pub settings for javascript
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript12Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_12);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_12);
				}
				// don't use branding pub settings for javascript
				// if (promo == null) {
				// promo =
				// (JavaScriptPromotionIntf)this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_2);
				// }
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript13Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_13);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_13);
				}
				// don't use branding pub settings for javascript
				// if (promo == null) {
				// promo =
				// (JavaScriptPromotionIntf)this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_2);
				// }
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript14Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_14);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_14);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript15Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_15);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_15);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript16Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_16);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_16);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript17Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_17);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_17);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript18Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_18);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_18);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScript19Text() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_19);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_19);
				}
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

	public JavaScriptPromotionIntf getPromoOnePageThankYouJavaScriptTealiumText() {
		try {
			JavaScriptPromotionIntf promo = (JavaScriptPromotionIntf) this.getPromoConfigs().get(
					PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_TEALIUM);
			if (promo == null) {
				if (this.defaultPubCodeSet != null) {
					promo = (JavaScriptPromotionIntf) this.defaultPubCodeSet.getPromoConfigs().get(
							PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_TEALIUM);
				}
				// don't use branding pub settings for javascript
				// if (promo == null) {
				// promo =
				// (JavaScriptPromotionIntf)this.defaultBrandingPubSet.getPromoConfigs().get(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_2);
				// }
			}
			return promo;
		} catch (Exception e) {
			return null;
		}
	}

}
