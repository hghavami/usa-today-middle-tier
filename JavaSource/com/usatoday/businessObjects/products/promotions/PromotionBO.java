/*
 * Created on May 5, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products.promotions;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

/**
 * @author aeast
 * @date May 5, 2006
 * @class PromotionBO
 * 
 *        This class represents a record in the XTRNTPROMO Table for read only purposes.
 * 
 */
public class PromotionBO implements PromotionIntf {

	private String pubCode = "";
	private String keyCode = "";
	private String type = "";
	private String name = "";
	private String fulfillText = "";
	private String fulfillUrl = "";
	private String altName = "";

	/**
     * 
     */
	public PromotionBO(PromotionIntf promotion) {
		super();
		this.pubCode = promotion.getPubCode();
		this.keyCode = promotion.getKeyCode();
		this.type = promotion.getType();
		this.name = promotion.getName();
		this.fulfillText = promotion.getFulfillText();
		this.fulfillUrl = promotion.getFulfillUrl();
		this.altName = promotion.getAltName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.PromotionIntf#getAltName()
	 */
	public String getAltName() {
		return this.altName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.PromotionIntf#getFulfillText()
	 */
	public String getFulfillText() {
		return this.fulfillText;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.PromotionIntf#getFulfillUrl()
	 */
	public String getFulfillUrl() {
		return this.fulfillUrl;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.PromotionIntf#getKeyCode()
	 */
	public String getKeyCode() {
		return this.keyCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.PromotionIntf#getName()
	 */
	public String getName() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.PromotionIntf#getPubCode()
	 */
	public String getPubCode() {
		return this.pubCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.PromotionIntf#getType()
	 */
	public String getType() {
		return this.type;
	}

	void setAltName(String altName) {
		this.altName = altName;
	}

	void setFulfillText(String fulfillText) {
		this.fulfillText = fulfillText;
	}

	void setFulfillUrl(String fulfillUrl) {
		this.fulfillUrl = fulfillUrl;
	}

	void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	void setName(String name) {
		this.name = name;
	}

	void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	void setType(String type) {
		this.type = type;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UsatException
	 */
	public static PromotionIntf createPromotionBO(PromotionIntf source) throws UsatException {

		if (source == null) {
			return null;
		}

		PromotionIntf promo = null;

		if (source.getType().equalsIgnoreCase(PromotionIntf.CREDIT_CARD)) {
			// and the card
			CreditCardPromotionBO ccp = new CreditCardPromotionBO(source);
			promo = ccp;
		} else if (source.getType().equalsIgnoreCase(PromotionIntf.EZPAY_PROMO)
				|| source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_TEXT)
				|| source.getType().equalsIgnoreCase(PromotionIntf.TERMS_AND_CONDITIONS_TEXT)
				|| source.getType().equalsIgnoreCase(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC0)
				|| source.getType().equalsIgnoreCase(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC2)
				|| source.getType().equalsIgnoreCase(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC3)
				|| source.getType().equalsIgnoreCase(PromotionIntf.CUSTOMER_SERVICE_PHONE_CK_SRC4)				
				|| source.getType().equalsIgnoreCase(PromotionIntf.TERMS_DESCRIPTION_TEXT_EZPAY)
				|| source.getType().equalsIgnoreCase(PromotionIntf.TERMS_DESCRIPTION_TEXT)				
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_DISCLAIMER_TEXT)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_FORCE_EZPAY_TERMS_TEXT)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_SPOT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_SPOT_2)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_VIDEO_SPOT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_HTML_VIDEO_SPOT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_HTML_SPOT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.DYNAMIC_NAVIGATION_CUSTSERV)
				|| source.getType().equalsIgnoreCase(PromotionIntf.DYNAMIC_NAVIGATION_ORDER_ENTRY)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_2)) {
			HTMLPromotionBO hp = new HTMLPromotionBO(source);
			promo = hp;
		} else if (source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_SCRIPT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_SCRIPT_2)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_SCRIPT_3)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_SCRIPT_4)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_SCRIPT_5)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_SCRIPT_TEALIUM)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_2)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_3)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_4)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_5)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_6)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_7)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_8)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_9)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_10)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_11)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_12)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_13)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_14)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_15)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_16)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_17)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_18)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_19)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_20)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANKYOU_SCRIPT_TEALIUM)) {
			JavaScriptPromotionBO js = new JavaScriptPromotionBO(source);
			promo = js;
		} else if (source.getType().equalsIgnoreCase(PromotionIntf.PRODUCT_IMAGE_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_2)
				|| source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_3)
				|| source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120)
				|| source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90)
				|| source.getType().equalsIgnoreCase(PromotionIntf.TEMPLATE_NAV_PROMO_150x90)
				|| source.getType().equalsIgnoreCase(PromotionIntf.IMAGE_BOTTOM)
				|| source.getType().equalsIgnoreCase(PromotionIntf.IMAGE_COMPLETE)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ACCOUNT_INFO_IMAGE_SPOT1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ACCOUNT_INFO_IMAGE_SPOT2)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ACCOUNT_INFO_IMAGE_SPOT3)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ORDER_PATH_POP_OVERLAY)
				|| source.getType().equalsIgnoreCase(PromotionIntf.CUTOMER_SERVICE_PATH_POP_OVERLAY)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_2)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_3)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_VIDEO_POP_SPOT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_2)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_VIDEO_POP_SPOT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_1)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_2)
				|| source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_LEFT_COL_IMAGE_SPOT_1)) {
			ImagePromotionBO ip = new ImagePromotionBO(source);
			promo = ip;
		} else {
			PromotionBO p = new PromotionBO(source);
			promo = p;
		}
		return promo;
	}

	/**
     * 
     */
	public String toString() {
		StringBuffer data = new StringBuffer("Promo Record: ");

		data.append(" PubCode: " + this.getPubCode());
		data.append(" KeyCode: " + this.getKeyCode());
		data.append(" Type: " + this.getType());
		data.append(" Name: " + this.getName());
		data.append(" Alt Name: " + this.getAltName());
		data.append(" Fulfill Text: " + this.getFulfillText());
		data.append(" Fulfill URL: " + this.getFulfillUrl());

		return data.toString();
	}
}
