/*
 * Created on Aug 18, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products.promotions;

import com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

/**
 * @author aeast
 * @date Aug 18, 2006
 * @class HTMLPromotionBO
 * 
 *        Helper class to make it easier to get the value for this promotion
 * 
 */
public class HTMLPromotionBO extends PromotionBO implements HTMLPromotionIntf {

	/**
	 * @param promotion
	 */
	public HTMLPromotionBO(PromotionIntf promotion) {
		super(promotion);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf#getPromotionalHTML()
	 */
	public String getPromotionalHTML() {
		return this.getFulfillText();
	}
}
