/*
 * Created on May 5, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products.promotions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentTypentf;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentTypeBO;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentTypeBean;

/**
 * @author aeast
 * @date May 5, 2006
 * @class CreditCardPromotionBO
 * 
 *        Since card promos are stored as individual records, we collapse them into this BO to determin if the type is accepted.
 * 
 */
public class CreditCardPromotionBO extends PromotionBO implements CreditCardPromotionIntf {
	private boolean acceptVisa = false;
	private boolean acceptMasterCard = false;
	private boolean acceptAmericanExpress = false;
	private boolean acceptDiners = false;
	private boolean acceptDiscovery = false;

	private HashMap<String, PromotionIntf> creditCards = new HashMap<String, PromotionIntf>();

	/**
	 * @param promotion
	 */
	public CreditCardPromotionBO(PromotionIntf promotion) {
		super(promotion);
		super.setType(PromotionIntf.CREDIT_CARD);
		try {
			// add this card to list of cards
			this.addCard(promotion);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf#acceptsAmEx()
	 */
	public boolean acceptsAmEx() {
		return acceptAmericanExpress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf#acceptsDiners()
	 */
	public boolean acceptsDiners() {
		return acceptDiners;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf#acceptsDiscovery()
	 */
	public boolean acceptsDiscovery() {
		return acceptDiscovery;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf#acceptsMasterCard()
	 */
	public boolean acceptsMasterCard() {
		return acceptMasterCard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf#acceptsVisa()
	 */
	public boolean acceptsVisa() {
		return acceptVisa;
	}

	public void addCard(PromotionIntf promotion) throws UsatException {
		if (promotion == null || !promotion.getType().equalsIgnoreCase(PromotionIntf.CREDIT_CARD)) {
			throw new UsatException("CreditCardPromo-addCardToPromo() - invalid card");
		}

		PromotionBO newP = new PromotionBO(promotion);
		newP.setType(PromotionIntf.CREDIT_CARD);
		newP.setName(promotion.getName());
		newP.setFulfillText(promotion.getFulfillText());

		this.creditCards.put(newP.getName(), newP);

		int type = Integer.parseInt(promotion.getName());
		switch (type) {
		case CreditCardPromotionIntf.AMEX:
			this.acceptAmericanExpress = true;
			break;
		case CreditCardPromotionIntf.VISA:
			this.acceptVisa = true;
			break;
		case CreditCardPromotionIntf.MC:
			this.acceptMasterCard = true;
			break;
		case CreditCardPromotionIntf.DISCOVERY:
			this.acceptDiscovery = true;
			break;
		case CreditCardPromotionIntf.DINERS:
			this.acceptDiners = true;
			break;
		default:
			break;
		}

	}

	/**
     * 
     */
	public Collection<CreditCardPaymentTypeBean> getCreditCardPaymentTypesBeans() throws UsatException {

		Collection<PromotionIntf> ccBO = this.creditCards.values();

		ArrayList<CreditCardPaymentTypeBean> beans = new ArrayList<CreditCardPaymentTypeBean>();
		if (ccBO != null) {
			Iterator<PromotionIntf> itr = ccBO.iterator();
			while (itr.hasNext()) {
				PromotionBO bo = (PromotionBO) itr.next();
				CreditCardPaymentTypeBean bean = new CreditCardPaymentTypeBean();
				bean.setLabel(bo.getFulfillText());
				bean.setValue(bo.getName());
				// figure out type and set image file accordingly.
				Collection<CreditCardPaymentTypentf> ccTypes = CreditCardPaymentTypeBO.getCreditCardPaymentTypes();
				Iterator<CreditCardPaymentTypentf> cItr = ccTypes.iterator();
				while (cItr.hasNext()) {
					Object obj = cItr.next();
					CreditCardPaymentTypeBO cbo = (CreditCardPaymentTypeBO) obj;
					if (cbo.getDescription().equalsIgnoreCase(bo.getFulfillText())) { // PROMOTION OBJECTS STORE CC NAME
						bean.setImageFile(cbo.getImagePath());
						break;
					}
				}
				// bean.setImageFile(bo.get)
				beans.add(bean);
			}
		}
		return beans;
	}

	private int convertStringTypeToIntType(String type) {
		int ccTypeInt = -1;

		if (type.equalsIgnoreCase("A")) {
			ccTypeInt = CreditCardPromotionIntf.AMEX;
		} else if (type.equalsIgnoreCase("V")) {
			ccTypeInt = CreditCardPromotionIntf.VISA;
		} else if (type.equalsIgnoreCase("M")) {
			ccTypeInt = CreditCardPromotionIntf.MC;
		} else if (type.equalsIgnoreCase("D")) {
			ccTypeInt = CreditCardPromotionIntf.DISCOVERY;
		} else if (type.equalsIgnoreCase("C")) {
			ccTypeInt = CreditCardPromotionIntf.DINERS;
		}

		return ccTypeInt;

	}

	/**
	 * 
	 * @param type
	 *            The type of card we are checking from the XTRNTCRDCTL table values
	 * @return true if this promotion allows that card otherwise false
	 */
	public boolean cardInPromotion(String type) {
		boolean inPromotion = false;

		int typeInt = this.convertStringTypeToIntType(type);
		switch (typeInt) {
		case CreditCardPromotionIntf.VISA:
			inPromotion = this.acceptsVisa();
			break;
		case CreditCardPromotionIntf.MC:
			inPromotion = this.acceptsMasterCard();
			break;
		case CreditCardPromotionIntf.AMEX:
			inPromotion = this.acceptsAmEx();
			break;
		case CreditCardPromotionIntf.DISCOVERY:
			inPromotion = this.acceptsDiscovery();
			break;
		case CreditCardPromotionIntf.DINERS:
			inPromotion = this.acceptsDiners();
			break;
		default:
			inPromotion = false;
		}
		return inPromotion;
	}

	public String getAcceptedCardsString() {
		StringBuffer sBuf = new StringBuffer();
		boolean needComma = false;
		if (this.acceptsAmEx()) {
			sBuf.append(CreditCardPromotionIntf.BANK_NAMES[CreditCardPromotionIntf.AMEX]);
			needComma = true;
		}
		if (this.acceptsVisa()) {
			if (needComma) {
				sBuf.append(", ");
			}
			sBuf.append(CreditCardPromotionIntf.BANK_NAMES[CreditCardPromotionIntf.VISA]);
			needComma = true;
		}
		if (this.acceptsMasterCard()) {
			if (needComma) {
				sBuf.append(", ");
			}
			sBuf.append(CreditCardPromotionIntf.BANK_NAMES[CreditCardPromotionIntf.MC]);
			needComma = true;
		}
		if (this.acceptsDiscovery()) {
			if (needComma) {
				sBuf.append(", ");
			}
			sBuf.append(CreditCardPromotionIntf.BANK_NAMES[CreditCardPromotionIntf.DISCOVERY]);
			needComma = true;
		}
		if (this.acceptsDiners()) {
			if (needComma) {
				sBuf.append(", ");
			}
			sBuf.append(CreditCardPromotionIntf.BANK_NAMES[CreditCardPromotionIntf.DINERS]);
		}

		return sBuf.toString();
	}
}
