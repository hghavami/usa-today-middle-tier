package com.usatoday.businessObjects.products.promotions;

import org.apache.commons.lang3.StringUtils;

import com.usatoday.business.interfaces.products.promotions.JavaScriptPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

public class JavaScriptPromotionBO extends PromotionBO implements JavaScriptPromotionIntf {

	public JavaScriptPromotionBO(PromotionIntf promotion) {
		super(promotion);

	}

	public String getJavaScript() {

		return this.getFulfillText();
	}

	/**
	 * This method will relace the place holders with the values passed as paramaters if they exists.
	 * 
	 */
	public String getDynamicJavaScript(String orderID, double orderTotal, double orderSubTotal, String orderKeycode,
			String orderProductCode, String email) {

		java.text.NumberFormat format = java.text.NumberFormat.getInstance();
		format.setMaximumFractionDigits(2);
		format.setMinimumFractionDigits(2);

		String tempString = this.getFulfillText();

		try {
			// order id
			tempString = StringUtils.replace(tempString, "[OID]", orderID);
			tempString = StringUtils.replace(tempString, "|order-id|", "|" + orderID + "|");
			tempString = StringUtils.replace(tempString, "[TOTAL]", format.format(orderTotal));
			tempString = StringUtils.replace(tempString, "|total|", "|" + format.format(orderTotal) + "|");
			tempString = StringUtils.replace(tempString, "|price|", "|" + format.format(orderTotal) + "|");
			tempString = StringUtils.replace(tempString, "[SUBTOTAL]", format.format(orderSubTotal));
			tempString = StringUtils.replace(tempString, "[KEYCODE]", orderKeycode);
			tempString = StringUtils.replace(tempString, "[PUBCODE]", orderProductCode);
			tempString = StringUtils.replace(tempString, "[EMAIL]", email);
			tempString = StringUtils.replace(tempString, "order_id:\"\"", "order_id:\"" + orderID + "\"");
			tempString = StringUtils.replace(tempString, "total_price:\"\"", "total_price:\"" + format.format(orderTotal) + "\"");
			tempString = StringUtils.replace(tempString, "subtotal:\"\"", "subtotal:\"" + format.format(orderSubTotal) + "\"");
			tempString = StringUtils.replace(tempString, "keycode:\"\"", "keycode:\"" + orderKeycode + "\"");
			tempString = StringUtils.replace(tempString, "pubcode:\"\"", "pubcode:\"" + orderProductCode + "\"");
			tempString = StringUtils.replace(tempString, "email_address:\"\"", "email_address:\"" + email + "\"");
			tempString = StringUtils.replace(tempString, "0.00", format.format(orderTotal));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return tempString;
	}

	/**
	 * This method will replace the place holders with the values passed as parameters if they exists.
	 * 
	 */
	public String getTealiumDynamicJavaScript(String orderID, double orderTotal, double orderSubTotal, String orderKeycode,
			String orderProductCode, String email, String pageName, String rateCode, String rateDescription, boolean requiresEZPay,
			String rateDuration) {
	
		java.text.NumberFormat format = java.text.NumberFormat.getInstance();
		format.setMaximumFractionDigits(2);
		format.setMinimumFractionDigits(2);
	
		String tempString = this.getFulfillText();
	
		try {
			// order id
			tempString = StringUtils.replace(tempString, "order_id:\"\"", "order_id:\"" + orderID + "\"");
			tempString = StringUtils.replace(tempString, "total_price:\"\"", "total_price:\"" + format.format(orderTotal) + "\"");
			tempString = StringUtils.replace(tempString, "subtotal:\"\"", "subtotal:\"" + format.format(orderSubTotal) + "\"");
			tempString = StringUtils.replace(tempString, "keycode:\"\"", "keycode:\"" + orderKeycode + "\"");
			tempString = StringUtils.replace(tempString, "pubcode:\"\"", "pubcode:\"" + orderProductCode + "\"");
			tempString = StringUtils.replace(tempString, "email_address:\"\"", "email_address:\"" + email + "\"");
			tempString = StringUtils.replace(tempString, "page_name:\"\"", "page_name:\"" + pageName + "\"");			
			tempString = StringUtils.replace(tempString, "sub_rate_code:\"\"", "sub_rate_code:\"" + rateCode + "\"");
			tempString = StringUtils.replace(tempString, "sub_rate_name:\"\"", "sub_rate_name:\"" + rateDescription + "\"");
			tempString = StringUtils.replace(tempString, "sub_recurring:\"\"", "sub_recurring:\"" + Boolean.toString(requiresEZPay) + "\"");
			tempString = StringUtils.replace(tempString, "sub_term:\"\"", "sub_term:\"" + rateDuration + "\"");
			tempString = StringUtils.replace(tempString, "promo_id:\"\"", "promo_id:\"" + orderKeycode + "\"");
			tempString = StringUtils.replace(tempString, "promo_name:\"\"", "promo_name:\"" + rateDescription + "\"");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return tempString;
	}

}
