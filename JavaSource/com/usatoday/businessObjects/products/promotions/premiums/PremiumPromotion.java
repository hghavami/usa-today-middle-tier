/*
 * Created on Nov 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products.promotions.premiums;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf;

/**
 * @author aeast
 * @date Nov 9, 2006
 * @class PremiumPromotion
 * 
 *        This class represents a promotional offer instance.
 * 
 * @deprecated
 */
public class PremiumPromotion {
	private Collection<PremiumAttributeIntf> attributes = null;
	private String descriptionText = "";
	private String emailText = "";
	private String premiumPromoCode = null;
	private String landingPage = null;
	private boolean sendToGiftPayer = false;
	private boolean hasClubNumber = false;
	private String clubNumber = null;

	/**
     * 
     */
	public PremiumPromotion() {
		super();
	}

	/**
	 * @return Returns the attributes.
	 */
	public Collection<PremiumAttributeIntf> getAttributes() {
		if (this.attributes == null) {
			this.attributes = new ArrayList<PremiumAttributeIntf>();
		}
		return this.attributes;
	}

	/**
	 * @param attributes
	 *            The attributes to set.
	 */
	public void setAttributes(Collection<PremiumAttributeIntf> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return Returns the clubNumber.
	 */
	public String getClubNumber() {
		return this.clubNumber;
	}

	/**
	 * @param clubNumber
	 *            The clubNumber to set.
	 */
	public void setClubNumber(String clubNumber) {
		this.clubNumber = clubNumber;
	}

	/**
	 * @return Returns the descriptionText.
	 */
	public String getDescriptionText() {
		return this.descriptionText;
	}

	/**
	 * @param descriptionText
	 *            The descriptionText to set.
	 */
	public void setDescriptionText(String descriptionText) {
		this.descriptionText = descriptionText;
	}

	/**
	 * @return Returns the emailText.
	 */
	public String getEmailText() {
		return this.emailText;
	}

	/**
	 * @param emailText
	 *            The emailText to set.
	 */
	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}

	/**
	 * @return Returns the hasClubNumber.
	 */
	public boolean isHasClubNumber() {
		return this.hasClubNumber;
	}

	/**
	 * @param hasClubNumber
	 *            The hasClubNumber to set.
	 */
	public void setHasClubNumber(boolean hasClubNumber) {
		this.hasClubNumber = hasClubNumber;
	}

	/**
	 * @return Returns the landingPage.
	 */
	public String getLandingPage() {
		return this.landingPage;
	}

	/**
	 * @param landingPage
	 *            The landingPage to set.
	 */
	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}

	/**
	 * @return Returns the premiumPromoCode.
	 */
	public String getPremiumPromoCode() {
		return this.premiumPromoCode;
	}

	/**
	 * @param premiumPromoCode
	 *            The premiumPromoCode to set.
	 */
	public void setPremiumPromoCode(String premiumPromoCode) {
		this.premiumPromoCode = premiumPromoCode;
	}

	/**
	 * @return Returns the sendToGiftPayer.
	 */
	public boolean isSendToGiftPayer() {
		return this.sendToGiftPayer;
	}

	/**
	 * @param sendToGiftPayer
	 *            The sendToGiftPayer to set.
	 */
	public void setSendToGiftPayer(boolean sendToGiftPayer) {
		this.sendToGiftPayer = sendToGiftPayer;
	}

	/**
	 * 
	 * @param attribute
	 */
	public void addAttribute(PremiumAttributeBO attribute) {
		if (attribute == null) {
			return;
		}

		// if the attribute is a club number then keep the value in
		// this object for easy synching with the subscription transaction
		if (attribute.isClubNumber()) {
			this.setHasClubNumber(true);
			this.setClubNumber(attribute.getAttributeValue());
		}
		this.getAttributes().add(attribute);
	}

	/**
	 * 
	 * @param orderID
	 */
	public void applyOrderID(String orderID) {
		if (orderID == null) {
			return;
		}
		Iterator<PremiumAttributeIntf> iter = this.getAttributes().iterator();
		while (iter.hasNext()) {
			PremiumAttributeIntf element = iter.next();
			element.setOrderId(orderID);
		}
	}

	public void applyAccountNumber(String accountNum) {
		if (accountNum == null) {
			return;
		}
		Iterator<PremiumAttributeIntf> iter = this.getAttributes().iterator();
		while (iter.hasNext()) {
			PremiumAttributeIntf element = (PremiumAttributeIntf) iter.next();
			element.setAccountNum(accountNum);
		}
	}

	/**
	 * 
	 * @param pubCode
	 */
	public void applyPubcode(String pubCode) {
		if (pubCode == null) {
			return;
		}
		Iterator<PremiumAttributeIntf> iter = this.getAttributes().iterator();
		while (iter.hasNext()) {
			PremiumAttributeIntf element = (PremiumAttributeIntf) iter.next();
			element.setPubCode(pubCode);
		}
	}

	/**
     * 
     *
     */
	public void applyReadyForFulfillmentProcessingState() {
		Iterator<PremiumAttributeIntf> iter = this.getAttributes().iterator();
		while (iter.hasNext()) {
			PremiumAttributeIntf element = (PremiumAttributeIntf) iter.next();
			element.setState(PremiumAttributeIntf.READY_FOR_BATCH);
		}
	}

	/**
	 * Saves all attributes in this promotion instance
	 * 
	 * @return int The number of attributes saved.
	 */
	public int saveAttributes() {
		int numSaved = 0;
		Iterator<PremiumAttributeIntf> itr = this.getAttributes().iterator();

		while (itr.hasNext()) {
			PremiumAttributeBO attr = (PremiumAttributeBO) itr.next();
			try {
				attr.save();
				numSaved++;
			} catch (UsatException ue) {
				System.out.println("PremiumPromotion:saveAttributes() - Failed to save an attribute: " + ue.getMessage()
						+ " ORDER ID: " + attr.getOrderId() + "  Account Num: " + attr.getAccountNum());
			}
		}
		return numSaved;
	}
}
