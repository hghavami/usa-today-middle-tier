/*
 * Created on Nov 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products.promotions.premiums;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf;
import com.usatoday.businessObjects.util.ZeroFilledNumeric;
import com.usatoday.integration.PremiumAttributeRecordDAO;

/**
 * @author aeast
 * @date Nov 9, 2006
 * @class PremiumAttributeBO
 * 
 *        This clas represents an attribute of a premium promotion. For example, a color choice of blue or a engraving.
 * 
 * @deprecated
 */
public class PremiumAttributeBO implements PremiumAttributeIntf {

	private String pubCode = null;
	private String accountNum = null;
	private String premiumPromotionCode = null;
	private String premiumCode = null;
	private String premiumType = null;
	private String giftPayerReceive = null;
	private String attributeName = null;
	private String attributeValue = null;
	private String orderId = null;
	private String dateUpdated = null;
	private String timeUpdated = null;
	private Timestamp updateTimestamp = null;
	private long key = 0;
	private int state = PremiumAttributeIntf.NEW;
	private boolean clubNumber = false;

	/**
     * 
     */
	public PremiumAttributeBO() {
		super();
	}

	public PremiumAttributeBO(PremiumAttributeIntf source) {
		super();
		this.setAccountNum(source.getAccountNum());
		this.setAttributeName(source.getAttributeName());
		this.setAttributeValue(source.getAttributeValue());
		this.setClubNumber(source.isClubNumber());
		this.setDateUpdated(source.getDateUpdated());
		this.setGiftPayerReceive(source.getGiftPayerReceive());
		this.setKey(source.getKey());
		this.setOrderId(source.getOrderId());
		this.setPremiumCode(source.getPremiumCode());
		this.setPremiumPromotionCode(source.getPremiumPromotionCode());
		this.setPremiumType(source.getPremiumType());
		this.setPubCode(source.getPubCode());
		this.setState(source.getState());
		this.setTimeUpdated(source.getTimeUpdated());
		this.setUpdateTimestamp(source.getUpdateTimestamp());
	}

	public static Collection<PremiumAttributeIntf> fetchAttributesRecordsForBatchProcessing() throws UsatException {
		Collection<PremiumAttributeIntf> attributeBOCollection = new ArrayList<PremiumAttributeIntf>();

		PremiumAttributeRecordDAO dao = new PremiumAttributeRecordDAO();
		Collection<PremiumAttributeIntf> transferObjects = dao.fetchAttributesRecordsForBatchProcessing();

		Iterator<PremiumAttributeIntf> itr = transferObjects.iterator();
		while (itr.hasNext()) {
			PremiumAttributeIntf attrIntf = itr.next();
			PremiumAttributeBO bo = new PremiumAttributeBO(attrIntf);
			attributeBOCollection.add(bo);
		}

		return attributeBOCollection;
	}

	public static int changeStateToInBatchProcessing() throws UsatException {
		PremiumAttributeRecordDAO dao = new PremiumAttributeRecordDAO();
		return dao.changeStateToInBatchProcessing();
	}

	public static int changeStateToReadyForDelete() throws UsatException {
		PremiumAttributeRecordDAO dao = new PremiumAttributeRecordDAO();
		return dao.changeStateToReadyForDelete();
	}

	public static int deleteAttributesOlderThanSpecifiedDays(int numDays) throws UsatException {
		if (numDays < 0) {
			throw new UsatException("PremiumAttributeBO::deleteAttributesOlderThanSpecifiedDays() - Invalid number of days: "
					+ numDays);
		}
		PremiumAttributeRecordDAO dao = new PremiumAttributeRecordDAO();
		return dao.deleteAttributesOlderThanSpecifiedDays(numDays);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getAccountNum()
	 */
	public String getAccountNum() {
		return this.accountNum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setAccountNum(java.lang.String)
	 */
	public void setAccountNum(String accountNum) {

		// validate that the size of the account number is acceptable
		if (accountNum != null && accountNum.length() <= 9) {
			this.accountNum = accountNum;
		} else {
			if (accountNum == null) {
				this.accountNum = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getAttributeName()
	 */
	public String getAttributeName() {
		return this.attributeName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setAttributeName(java.lang.String)
	 */
	public void setAttributeName(String attributeName) {
		if (attributeName != null && attributeName.length() <= 10) {
			this.attributeName = attributeName;
		} else {
			if (attributeName == null) {
				this.attributeName = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getAttributeValue()
	 */
	public String getAttributeValue() {
		return this.attributeValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setAttributeValue(java.lang.String)
	 */
	public void setAttributeValue(String attributeValue) {
		if (attributeValue != null && attributeValue.length() <= 50) {
			if (attributeValue.indexOf("'") >= 0) {
				attributeValue = attributeValue.replaceAll("'", "");
			}
			if (attributeValue.indexOf("\"") >= 0) {
				attributeValue = attributeValue.replaceAll("\"", "");
			}

			this.attributeValue = attributeValue;
		} else {
			if (attributeValue == null) {
				this.attributeValue = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#isClubNumber()
	 */
	public boolean isClubNumber() {
		return this.clubNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setClubNumber(boolean)
	 */
	public void setClubNumber(boolean clubNumber) {
		this.clubNumber = clubNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getDateUpdated()
	 */
	public String getDateUpdated() {
		return this.dateUpdated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setDateUpdated(java.lang.String)
	 */
	public void setDateUpdated(String dateUpdated) {
		if (dateUpdated != null && dateUpdated.length() == 8) {
			this.dateUpdated = dateUpdated;
		} else {
			if (dateUpdated == null) {
				this.dateUpdated = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getGiftPayerReceive()
	 */
	public String getGiftPayerReceive() {
		return this.giftPayerReceive;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setGiftPayerReceive(java.lang.String)
	 */
	public void setGiftPayerReceive(String giftPayerReceive) {
		if (giftPayerReceive != null && giftPayerReceive.length() <= 1) {
			this.giftPayerReceive = giftPayerReceive;
		} else {
			if (giftPayerReceive == null) {
				this.giftPayerReceive = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getKey()
	 */
	public long getKey() {
		return this.key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setKey(long)
	 */
	public void setKey(long key) {
		// can't change primary key unless it's never been set.
		if (this.key == 0) {
			this.key = key;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getOrderId()
	 */
	public String getOrderId() {
		return this.orderId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setOrderId(java.lang.String)
	 */
	public void setOrderId(String orderId) {
		if (orderId != null && orderId.length() <= 20) {
			this.orderId = orderId;
		} else {
			if (orderId == null) {
				this.orderId = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getPremiumCode()
	 */
	public String getPremiumCode() {
		return this.premiumCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setPremiumCode(java.lang.String)
	 */
	public void setPremiumCode(String premiumCode) {
		if (premiumCode != null && premiumCode.length() <= 2) {
			this.premiumCode = premiumCode;
		} else {
			if (premiumCode == null) {
				this.premiumCode = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getPremiumPromotionCode()
	 */
	public String getPremiumPromotionCode() {
		return premiumPromotionCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setPremiumPromotionCode(java.lang.String)
	 */
	public void setPremiumPromotionCode(String premiumPromotionCode) {
		if (premiumPromotionCode != null && premiumPromotionCode.length() <= 2) {
			this.premiumPromotionCode = premiumPromotionCode;
		} else {
			if (premiumPromotionCode == null) {
				this.premiumPromotionCode = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getPremiumType()
	 */
	public String getPremiumType() {
		return premiumType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setPremiumType(java.lang.String)
	 */
	public void setPremiumType(String premiumType) {
		if (premiumType != null && premiumType.length() <= 10) {
			this.premiumType = premiumType;
			// flag this attibute if it contains a club number
			if (this.premiumType.equalsIgnoreCase("CLUB")) {
				this.setClubNumber(true);
			}
		} else {
			if (premiumType == null) {
				this.premiumType = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getPubCode()
	 */
	public String getPubCode() {
		return this.pubCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setPubCode(java.lang.String)
	 */
	public void setPubCode(String pubCode) {
		if (pubCode != null && pubCode.length() <= 2) {
			this.pubCode = pubCode;
		} else {
			if (pubCode == null) {
				this.pubCode = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getState()
	 */
	public int getState() {
		return this.state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setState(int)
	 */
	public void setState(int state) {
		this.state = state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getTimeUpdated()
	 */
	public String getTimeUpdated() {
		return timeUpdated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setTimeUpdated(java.lang.String)
	 */
	public void setTimeUpdated(String timeUpdated) {
		if (timeUpdated != null && timeUpdated.length() == 6) {
			this.timeUpdated = timeUpdated;
		} else {
			if (timeUpdated == null) {
				this.timeUpdated = "";
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#getUpdateTimestamp()
	 */
	public Timestamp getUpdateTimestamp() {
		return this.updateTimestamp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.usatoday.business.interfaces.products.promotions.premiums.PremiumAttributeIntf#setUpdateTimestamp(java.sql.Timestamp)
	 */
	public void setUpdateTimestamp(Timestamp updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	/**
	 * Output this object in the form: Field Attributes Field Text Input/Output PFPUB Char 2 PUBLICATION CODE 1 PFACT# Znd 7, 0
	 * SUBSCRIBER ACCT # 3 PFPREM Char 2 Prem Promo Code 10 PFPRMC Char 2 PREMIUM OFFER CODE 12 PFPRTY Char 10 Premium Type 14
	 * PFOPTC Char 10 Prem Type Opt Code 24 PFOPVL Char 50 Option Value 34 PFGPPR Char 1 Gift Payer Premium 84 PFTRID Char 20
	 * Transaction ID 85 PFSTAT Char 1 MPF State 86 PFUDAT Znd 8, 0 Record Last Updated 106 PFUTIM Znd 6, 0 Record Time Updated 114
	 * 
	 * @return
	 */
	public String getBatchFormattedString() throws com.usatoday.UsatException {
		StringBuffer buf = new StringBuffer();
		try {

			this.appendPaddedString(buf, 2, this.getPubCode());

			// hd-cons92 sew

			if (this.getAccountNum() != null) {
				// remove leading '34' from account number
				// this.appendPaddedString(buf, 7, this.getAccountNum().substring(2));
				this.appendPaddedString(buf, 7, this.getAccountNum());
			} else {
				this.appendPaddedString(buf, 7, null);
			}

			this.appendPaddedString(buf, 2, this.getPremiumPromotionCode());
			this.appendPaddedString(buf, 2, this.getPremiumCode());
			this.appendPaddedString(buf, 10, this.getPremiumType());
			this.appendPaddedString(buf, 10, this.getAttributeName());
			this.appendPaddedString(buf, 50, this.getAttributeValue());
			this.appendPaddedString(buf, 1, this.getGiftPayerReceive());
			this.appendPaddedString(buf, 20, this.getOrderId());
			this.appendPaddedString(buf, 1, " "); // order state is always blank
			this.appendPaddedString(buf, 8, this.getDateUpdated());
			this.appendPaddedString(buf, 6, this.getTimeUpdated());

		} catch (Exception e) {
			System.out.println("PremiumAttributeBO:getBatchFormattedString() - Failed to create batch record: " + e.getMessage());
			throw new com.usatoday.UsatException(e);
		}

		return buf.toString();
	}

	/**
	 * 
	 * @param buf
	 * @param fieldLength
	 * @param field
	 */
	private void appendPaddedString(StringBuffer buf, int fieldLength, String field) {
		if (buf == null) {
			return;
		}

		if (field == null) {
			for (int i = 0; i < fieldLength; i++) {
				buf.append(" ");
			}
			return;
		}

		int currentFieldLength = field.length();

		if (currentFieldLength <= fieldLength) {
			// no truncation
			buf.append(field);
			if (currentFieldLength != fieldLength) {
				// buffer remaining field with blanks
				for (int i = currentFieldLength; i < fieldLength; i++) {
					buf.append(" ");
				}
			}
		} else {
			// truncate
			buf.append(field.substring(0, fieldLength));
		}
	}

	public void save() throws UsatException {

		try {
			java.util.Calendar cal = java.util.Calendar.getInstance();
			java.util.Date date = cal.getTime();
			String dateStr = new java.text.SimpleDateFormat("yyyyMMdd").format(date);
			String timeStr = ZeroFilledNumeric.getValue(2, new java.text.SimpleDateFormat("H").format(date))
					+ ZeroFilledNumeric.getValue(2, new java.text.SimpleDateFormat("m").format(date))
					+ ZeroFilledNumeric.getValue(2, new java.text.SimpleDateFormat("s").format(date));
			this.setDateUpdated(dateStr);
			this.setTimeUpdated(timeStr);
		} catch (Exception e) {
			System.out.println("Failed to set update time stamp. " + e.getMessage());
		}
		PremiumAttributeRecordDAO dao = new PremiumAttributeRecordDAO();
		if (this.getKey() > 0) {
			// update
			dao.update(this);
		} else {
			// insert
			dao.insert(this);
		}
	}
}
