/*
 * Created on May 5, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.products.promotions;

import java.util.Collection;
import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.businessObjects.cache.CacheManager;
import com.usatoday.integration.PromotionDAO;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date May 5, 2006
 * @class PromotionManager
 * 
 *        This class maintains a cache of PromotionSets for a given keycode/pubcode combination.
 * 
 */
public class PromotionManager implements CacheManager {
	// static instance
	private static PromotionManager pm = null;

	HashMap<String, PromotionSet> promotionSets = new HashMap<String, PromotionSet>();

	DateTime lastClearedTime = null;

	/**
     * 
     */
	private PromotionManager() {
		super();
		lastClearedTime = new DateTime();
	}

	/**
	 * 
	 * @return the VM promotion manager
	 */
	public static synchronized PromotionManager getInstance() {
		if (PromotionManager.pm == null) {
			PromotionManager.pm = new PromotionManager();
		}
		return PromotionManager.pm;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.businessObjects.cache.CacheManager#clearCache()
	 */
	public void clearCache() {
		this.promotionSets.clear();
		this.lastClearedTime = new DateTime();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.businessObjects.cache.CacheManager#getLastClearedTime()
	 */
	public DateTime getLastClearedTime() {
		return this.lastClearedTime;
	}

	/**
	 * 
	 * @param offer
	 * @return A set of promotion objects or null
	 * @throws UsatException
	 */
	public PromotionSet getPromotionsForOffer(SubscriptionOfferIntf offer) throws UsatException {
		if (offer == null) {
			return null;
		}
		return this.getPromotionsForOffer(offer.getPubCode(), offer.getKeyCode());
	}

	/*
     * 
     */
	public PromotionSet getPromotionsForOffer(String pubCode, String keyCode) throws UsatException {
		String key = pubCode + keyCode;
		PromotionSet set = null;
		if (this.promotionSets.containsKey(key)) {
			set = (PromotionSet) this.promotionSets.get(key);
		} else {
			// load the promotion set
			set = this.loadPromotionSet(pubCode, keyCode);
			this.promotionSets.put(key, set);
		}

		return set;
	}

	/**
	 * 
	 * @param pubCode
	 * @param keycode
	 * @return
	 */
	private PromotionSet loadPromotionSet(String pubCode, String keyCode) throws UsatException {
		PromotionDAO dao = new PromotionDAO();

		Collection<PromotionIntf> promos = dao.fetchPromosByPubcodeKeycode(pubCode, keyCode);

		PromotionSet set = null;

		set = new PromotionSet(promos, pubCode, keyCode);

		return set;
	}

	public PromotionSet getDefaultPromotionSetForUSAT() throws UsatException {
		return this.getPromotionsForOffer(UsaTodayConstants.UT_PUBCODE, PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE);
	}

	public PromotionSet getDefaultPromotionSetForSW() throws UsatException {
		return this.getPromotionsForOffer(UsaTodayConstants.SW_PUBCODE, PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE);
	}

	public PromotionSet getDefaultPromotionSetForProduct(String pubCode) throws UsatException {
		return this.getPromotionsForOffer(pubCode, PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE);
	}

	public static int getNumberCached() {
		return getInstance().promotionSets.size();
	}
}
