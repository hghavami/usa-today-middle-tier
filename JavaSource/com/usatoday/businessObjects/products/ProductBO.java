package com.usatoday.businessObjects.products;

import java.util.Collection;
import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.PersistentProductIntf;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.suppliers.SupplierIntf;
import com.usatoday.business.interfaces.products.suppliers.SupplierTOIntf;
import com.usatoday.businessObjects.products.suppliers.SupplierBO;
import com.usatoday.integration.ProductDAO;
import com.usatoday.integration.SupplierDAO;
import com.usatoday.util.constants.UsaTodayConstants;

public abstract class ProductBO implements ProductIntf {

	private static HashMap<String, ProductIntf> productCache = new HashMap<String, ProductIntf>();

	public static void clearCachedProducts() {

		ProductBO.productCache.clear();
		try {
			// re-retrieve the main products
			SubscriptionProductBO.getUsaTodaySubscriptionProduct();
			SubscriptionProductBO.getSportsWeeklySubscriptionProduct();
			SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static final ProductIntf fetchProduct(String productCode) throws UsatException {
		ProductIntf prod = null;
		// must return a copy of the product!
		if (productCode == null) {
			return null;
		}

		if (ProductBO.productCache.containsKey(productCode)) {
			prod = ((ProductBO) ProductBO.productCache.get(productCode)).getCopyOfObject();
		} else {
			// pull Product
			ProductDAO pDAO = new ProductDAO();
			Collection<PersistentProductIntf> products = pDAO.getProductsByProductCode(productCode);
			if (products.size() == 1) {
				try {
					PersistentProductIntf pProd = (PersistentProductIntf) products.iterator().next();
					ProductIntf p = ProductBO.productFactory(pProd);

					// SubscriptionProductBO tempProd = new
					// SubscriptionProductBO(pProd);
					ProductBO.productCache.put(productCode, p);
					prod = ((ProductBO) ProductBO.productCache.get(productCode)).getCopyOfObject();
				} catch (Exception e) {
					throw new UsatException(e);
				}
			} else {
				throw new UsatException("Unable to retrieve the product from DB. Number rows returned for pubcode " + productCode
						+ " is: " + products.size());
			}
		}

		return prod;
	}
	public static final ProductIntf fetchProductById(int id) throws UsatException {
		ProductIntf prod = null;

		// pull Product
		ProductDAO pDAO = new ProductDAO();
		try {
			PersistentProductIntf product = pDAO.getProduct(id);

			prod = ProductBO.productFactory(product);

		} catch (Exception e) {
			throw new UsatException(e);
		}

		return prod;
	}
	public static final ProductIntf fetchProductIncludeInactive(String productCode) throws UsatException {
		ProductIntf prod = null;
		// must return a copy of the product!
		if (productCode == null) {
			return null;
		}

		ProductDAO pDAO = new ProductDAO();
		Collection<PersistentProductIntf> products = pDAO.getProductsByProductCodeIncludeInactive(productCode);
		if (products.size() == 1) {
			try {
				PersistentProductIntf pProd = (PersistentProductIntf) products.iterator().next();
				prod = ProductBO.productFactory(pProd);
			} catch (Exception e) {
				throw new UsatException(e);
			}
		} else {
			throw new UsatException("Unable to retrieve the product from DB. Number rows returned for pubcode " + productCode
					+ " is: " + products.size());
		}

		return prod;
	}
	private static final ProductIntf productFactory(PersistentProductIntf prod) throws UsatException {
		ProductIntf p = null;

		try {

			int pType = prod.getProductType();

			switch (pType) {
			case ProductIntf.SUBSCRIPTION_PRODUCT:
				p = new SubscriptionProductBO(prod);
				break;

			default:
				throw new UsatException("ProductBO Product Type not supported. ProdCode: " + prod.getProductCode());
			}
		} catch (UsatException ue) {
			throw ue;
		} catch (Exception e) {
			throw new UsatException(e);
		}

		return p;
	}
	private String taxRateCode = "";
	private String productCode = "";
	private String brandingPubCode = "UT";
	private String name = "";
	private String description = "";
	private String detailedDescription = "";
	private int minQuantityPerOrder = 1;
	private int maxQuantityPerOrder = 0; // 0 indicates no maximum
	private int id = 0;
	protected boolean taxable = true;
	private int fulfillmentMethod = -1;
	protected int inventory = 0;
	protected double unitPrice = 0.0;
	private DateTime startDate = null;
	private DateTime endDate = null;
	private int supplierID = -1;

	private String customerServicePhone = null;

	private String defaultKeycode = null;
	private String expiredOfferKeycode = null;

	private String defaultRenewalKeycode = null;

	private int maxDaysInFutureBeforeFulfillment = 90;

	protected int holdDaysDelay = -1;

	private int productType = 0;

	protected SupplierIntf supplier = null;

	public ProductBO(PersistentProductIntf prod) {
		super();
		if (prod == null) {
			return;
		}
		this.taxRateCode = prod.getTaxRateCode();
		this.productCode = prod.getProductCode();
		this.brandingPubCode = prod.getBrandingPubCode();
		this.name = prod.getName();
		this.description = prod.getDescription();
		this.detailedDescription = prod.getDetailedDescription();
		this.id = prod.getID();
		this.minQuantityPerOrder = prod.getMinQuantityPerOrder();
		this.maxQuantityPerOrder = prod.getMaxQuantityPerOrder();
		this.taxable = prod.isTaxable();
		this.inventory = prod.getInventory();
		this.fulfillmentMethod = prod.getFulfillmentMethod();
		this.supplierID = prod.getSupplierID();
		this.customerServicePhone = prod.getCustomerServicePhone();
		this.defaultKeycode = prod.getDefaultKeycode();
		this.expiredOfferKeycode = prod.getExpiredOfferKeycode();
		this.defaultRenewalKeycode = prod.getDefaultRenewalKeycode();
		this.maxDaysInFutureBeforeFulfillment = prod.getMaxDaysInFutureBeforeFulfillment();
		this.holdDaysDelay = prod.getHoldDaysDelay();
		this.productType = prod.getProductType();

		try {
			this.unitPrice = prod.getUnitPrice();
		} catch (UsatException e) {
			// should never get here;
		}
		this.startDate = prod.getStartDate();
		this.endDate = prod.getEndDate();
	}

	public String getBrandingPubCode() {
		return this.brandingPubCode;
	}

	protected abstract ProductIntf getCopyOfObject();

	public String getCustomerServicePhone() {
		return this.customerServicePhone;
	}

	public String getDefaultRenewalKeycode() {
		return defaultRenewalKeycode;
	}

	public String getDefaultKeycode() {
		return defaultKeycode;
	}

	public String getDescription() {
		return this.description;
	}

	public String getDetailedDescription() {
		return this.detailedDescription;
	}

	protected DateTime getEndDate() {
		return this.endDate;
	}

	public String getExpiredOfferKeycode() {
		return expiredOfferKeycode;
	}

	public int getFulfillmentMethod() {
		return this.fulfillmentMethod;
	}

	public int getID() {
		return this.id;
	}

	public int getMaxDaysInFutureBeforeFulfillment() {

		return this.maxDaysInFutureBeforeFulfillment;
	}

	public int getMaxQuantityPerOrder() {
		return this.maxQuantityPerOrder;
	}

	public int getMinQuantityPerOrder() {
		return this.minQuantityPerOrder;
	}

	public String getName() {
		return this.name;
	}

	public String getProductCode() {
		return this.productCode;
	}

	@Override
	public int getProductType() {
		return this.productType;
	}

	protected DateTime getStartDate() {
		return this.startDate;
	}

	public SupplierIntf getSupplier() {
		//
		if (this.supplier == null && this.supplierID > 0) {
			// try to retrive from db
			try {
				SupplierDAO sDao = new SupplierDAO();

				SupplierTOIntf sTO = sDao.fetchSupplierByID(this.supplierID);

				SupplierBO sBO = new SupplierBO(sTO);

				this.supplier = sBO;

			} catch (Exception e) {
				; // ignore
			}
		}
		return this.supplier;
	}

	public int getSupplierID() {
		return supplierID;
	}

	public String getTaxRateCode() {
		return this.taxRateCode;
	}

	public double getUnitPrice() throws UsatException {
		return this.unitPrice;
	}

	/**
	 * Sub classes should add additional checks if necessary or override this
	 * method completely.
	 */
	public boolean isAvailable() {
		boolean available = false;
		// if no inventory then product is not available
		if (this.inventory > 0) {
			// if no start and end date restrictions then it is available
			if (this.startDate == null && this.endDate == null) {
				available = true;
			} else if (this.startDate != null && this.endDate != null) {
				// if start and end date restrictions make sure we fall between
				// them
				if (this.startDate.isBeforeNow() && this.endDate.isAfterNow()) {
					available = true;
				}
			} else if (startDate != null) {
				// if just a start date restriction make sure we are after it.
				if (this.startDate.isBeforeNow()) {
					available = true;
				}
			} else {
				// if just an end date restriction make sure we are before it
				if (this.endDate.isAfterNow()) {
					available = true;
				}
			}

		}
		return available;
	}

	public boolean isElectronicDelivery() {
		if (this.fulfillmentMethod == ProductIntf.ELECTRONIC_DELIVERY) {
			return true;
		}
		return false;
	}

	public boolean isTaxable() {
		return this.taxable;
	}

	public void setDefaultRenewalKeycode(String defaultRenewalKeycode) {
		this.defaultRenewalKeycode = defaultRenewalKeycode;
	}

	public abstract int updateInventory(int delta);

}
