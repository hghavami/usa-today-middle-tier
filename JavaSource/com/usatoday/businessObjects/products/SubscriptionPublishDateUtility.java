package com.usatoday.businessObjects.products;

import java.util.Collection;
import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.integration.SubscriptionProductPublishDateDAO;
import com.usatoday.integration.SubscriptionProductPublishDateTO;

public class SubscriptionPublishDateUtility {

	public static final String publishDateFormat = "yyyyMMdd";
	public static final int EARLIEST_START_FROM_TODAY = 3;
	public static final int EARLIEST_SW_START_FROM_MON_THU = 5;
	public static final int EARLIEST_SW_START_FROM_FRI_SUN = 6;
	public static final int UT_HOLD_DAYS_OUT = 3; // 2 business days
	public static final int BW_HOLD_DAYS_OUT = 1; // since published weekly, must be more than one day since if the next day is a
													// pub day the next possible is a week away

	private static SubscriptionPublishDateUtility _instance = null;

	private HashMap<String, HashMap<String, SubscriptionProductPublishDateTO>> publishingDates = new HashMap<String, HashMap<String, SubscriptionProductPublishDateTO>>();

	private SubscriptionPublishDateUtility() {
		// load the cache
		this.loadCache();
	}

	public static final SubscriptionPublishDateUtility getInstance() {
		if (SubscriptionPublishDateUtility._instance == null) {
			SubscriptionPublishDateUtility._instance = new SubscriptionPublishDateUtility();
		}
		return SubscriptionPublishDateUtility._instance;
	}

	/**
	 * 
	 */
	public static final void resetCache() {
		if (SubscriptionPublishDateUtility._instance != null) {
			SubscriptionPublishDateUtility._instance = null;
		}
	}

	private void loadCache() {
		try {
			SubscriptionProductPublishDateDAO dao = new SubscriptionProductPublishDateDAO();

			// Collection<SubscriptionProductPublishDateTO> dateTOs = dao.fetchFuturePublishingDates();
			Collection<SubscriptionProductPublishDateTO> dateTOs = dao.fetchPublishingDates();

			for (SubscriptionProductPublishDateTO pubDate : dateTOs) {
				HashMap<String, SubscriptionProductPublishDateTO> datesForPub = this.publishingDates.get(pubDate.getPubCode());

				if (datesForPub == null) {
					datesForPub = new HashMap<String, SubscriptionProductPublishDateTO>();
					// add new Hash of dates to cache
					this.publishingDates.put(pubDate.getPubCode(), datesForPub);
				}

				String key = pubDate.getDate().toString(SubscriptionPublishDateUtility.publishDateFormat);

				if (datesForPub.containsKey(key)) {
					System.out.println("Duplicate Pub Date for pub/day" + pubDate.getPubCode() + "/" + key);
				} else {
					datesForPub.put(key, pubDate);
				}

			}
		} catch (Exception e) {
			System.out.println("Exceptin loading publishing dates: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param productCode
	 * @param date
	 * @return
	 */
	public boolean isValidPublishingDate(String productCode, DateTime date) {

		boolean validDate = false;
		try {
			validDate = this.isValidPublishingDate(productCode, date.toString(SubscriptionPublishDateUtility.publishDateFormat));
		} catch (Exception e) {
			e.printStackTrace();
			validDate = false;
		}
		return validDate;
	}

	/**
	 * 
	 * @param productCode
	 * @param date
	 *            In yyyyMMdd format
	 * @return
	 */
	public boolean isValidPublishingDate(String productCode, String date) {

		boolean validDate = false;
		try {
			HashMap<String, SubscriptionProductPublishDateTO> pubDatesForPub = null;

			pubDatesForPub = this.publishingDates.get(productCode);
			if (pubDatesForPub != null) {
				SubscriptionProductPublishDateTO pubDate = pubDatesForPub.get(date);
				if (pubDate != null) {
					validDate = pubDate.isPublishedOnThisDay();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			validDate = false;
		}
		return validDate;
	}

}
