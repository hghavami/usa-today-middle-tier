package com.usatoday.businessObjects.products.suppliers;

import com.usatoday.business.interfaces.products.suppliers.SupplierIntf;
import com.usatoday.business.interfaces.products.suppliers.SupplierTOIntf;

public class SupplierBO implements SupplierIntf {

	private int id = -1;
	private String name = null;
	private String secretKey = null;
	private String loginUrl = null;
	private String productUrl = null;
	private boolean active = false;
	private String description = null;
	private String sampleUrl = null;
	private String tutorialUrl = null;
	private String alternateProductUrl = null;
	private String alternateLoginUrl = null;

	@SuppressWarnings("unused")
	private SupplierBO() {
		super();
	}

	public SupplierBO(SupplierTOIntf source) {
		super();
		this.id = source.getID();
		this.name = source.getName();
		this.secretKey = source.getSecretKey();
		this.loginUrl = source.getLoginURL();
		this.productUrl = source.getProductURL();
		this.active = source.isActive();
		this.description = source.getDescription();
		this.sampleUrl = source.getSampleURL();
		this.tutorialUrl = source.getTutorialURL();
		this.alternateLoginUrl = source.getAlternateLoginURL();
		this.alternateProductUrl = source.getAlternateProductURL();
	}

	public String getDescription() {
		return this.description;
	}

	public int getID() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getSecretKey() {
		return this.secretKey;
	}

	public boolean isActive() {
		return this.active;
	}

	public String getLoginURL() {
		return this.loginUrl;
	}

	public String getProductURL() {
		return this.productUrl;
	}

	public String getSampleURL() {
		return this.sampleUrl;
	}

	public String getTutorialURL() {
		return tutorialUrl;
	}

	@Override
	public String getAlternateLoginURL() {

		return this.alternateLoginUrl;
	}

	@Override
	public String getAlternateProductURL() {

		return this.alternateProductUrl;
	}

	public void setAlternateProductURL(String alternateProductUrl) {
		this.alternateProductUrl = alternateProductUrl;
	}

	public void setAlternateLoginURL(String alternateLoginUrl) {
		this.alternateLoginUrl = alternateLoginUrl;
	}

}
