/*
 * Created on Apr 19, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.cache;

import org.joda.time.DateTime;

/**
 * @author aeast
 * @date Apr 19, 2006
 * @class CacheManager
 * 
 *        Any class that caches database data should implement this inteface
 * 
 */
public interface CacheManager {
	/**
	 * Method to reset the cache so that offers will be updated from the database on subsequent requests.
	 * 
	 */
	public abstract void clearCache();

	/**
	 * A method to find out the last time the cache was cleared.
	 * 
	 * @return Time the cache last cleared.
	 */
	public abstract DateTime getLastClearedTime();
}