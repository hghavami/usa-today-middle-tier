package com.usatoday.businessObjects.util;

import java.security.MessageDigest;

public class MD5OneWayHash {

	private static final char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static String getMD5Hash(String value) {
		String hash = null;
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");

			byte[] hashBytes = md5.digest(value.getBytes());

			String hex = "";
			int msb;
			int lsb = 0;
			int i;

			// MSB maps to idx 0
			for (i = 0; i < hashBytes.length; i++) {

				msb = ((int) hashBytes[i] & 0x000000FF) / 16;

				lsb = ((int) hashBytes[i] & 0x000000FF) % 16;
				hex = hex + hexChars[msb] + hexChars[lsb];
			}
			hash = hex;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return hash;
	}

}
