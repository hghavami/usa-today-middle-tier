package com.usatoday.businessObjects.util;

public class AS400CurrentStatus {
	private static boolean jdbcActive = true;

	private static boolean dataQActive = false;
	private static boolean ifsActive = false;
	private static boolean recordLevelActive = false;
	private static boolean commandActive = false;

	/**
	 * 
	 */
	public AS400CurrentStatus() {
		super();
	}

	/**
	 * @return Returns the commandActive.
	 */
	public static final boolean getCommandActive() {
		return commandActive;
	}

	/**
	 * @param commandActive
	 *            The commandActive to set.
	 */
	public static synchronized final void setCommandActive(boolean commandActive) {
		if (commandActive) {
			AS400CurrentStatus.commandActive = true;
		} else {
			AS400CurrentStatus.commandActive = false;
		}
	}

	/**
	 * @return Returns the dataQActive.
	 */
	public static final boolean getDataQActive() {
		return dataQActive;
	}

	/**
	 * @param dataQActive
	 *            The dataQActive to set.
	 */
	public static synchronized final void setDataQActive(boolean dataQActive) {
		if (dataQActive) {
			AS400CurrentStatus.dataQActive = true;
		} else {
			AS400CurrentStatus.dataQActive = false;
		}
	}

	/**
	 * @return Returns the ifsActive.
	 */
	public static final boolean getIfsActive() {
		return ifsActive;
	}

	/**
	 * @param ifsActive
	 *            The ifsActive to set.
	 */
	public static synchronized final void setIfsActive(boolean ifsActive) {
		if (ifsActive) {
			AS400CurrentStatus.ifsActive = true;
		} else {
			AS400CurrentStatus.ifsActive = false;
		}
	}

	/**
	 * @return Returns the jdbcActive.
	 */
	public static final boolean getJdbcActive() {
		return jdbcActive;
	}

	/**
	 * @param jdbcActive
	 *            The jdbcActive to set.
	 */
	public static synchronized final void setJdbcActive(boolean jdbcActive) {
		if (jdbcActive) {
			AS400CurrentStatus.jdbcActive = true;
		} else {
			AS400CurrentStatus.jdbcActive = false;
		}
	}

	/**
	 * @return Returns the recordLevelActive.
	 */
	public static final boolean getRecordLevelActive() {
		return recordLevelActive;
	}

	/**
	 * @param recordLevelActive
	 *            The recordLevelActive to set.
	 */
	public static synchronized final void setRecordLevelActive(boolean recordLevelActive) {
		if (recordLevelActive) {
			AS400CurrentStatus.recordLevelActive = true;
		} else {
			AS400CurrentStatus.recordLevelActive = false;
		}
	}

}
