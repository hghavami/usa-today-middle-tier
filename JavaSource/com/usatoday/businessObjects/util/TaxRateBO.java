/*
 * Created on Apr 19, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.integration.TaxRateForZipTO;
import com.usatoday.integration.TaxRateTO;

/**
 * @author aeast
 * @date Apr 19, 2006
 * @class TaxRateBO
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class TaxRateBO implements TaxRateIntf {

	// use a single instance to represent no tax
	private static final TaxRateTO NO_TAX = new TaxRateTO("", "", 0.0, 0.0, 0.0, 0.0, 0.0);
	private static TaxRateBO zeroTaxBO = null;

	private String zip = null;
	private Collection<TaxRateTO> rates = null;

	public TaxRateBO(TaxRateForZipTO rate) {
		super();
		this.zip = rate.getZip();
		this.rates = rate.getTaxRates();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.util.TaxRateIntf#getZip()
	 */
	public String getZip() {
		return this.zip;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.util.TaxRateIntf#computeTax(java.lang.String, double)
	 */
	/**
	 * @param taxRateCode
	 *            - The code tax or PubCode to use to determint rates
	 * @param amount
	 *            - The amount that should be taxed.
	 * @return - The total taxes
	 */
	public double computeTax(String taxRateCode, double amount) {

		TaxRateTO rate = TaxRateBO.NO_TAX;

		Iterator<TaxRateTO> itr = rates.iterator();
		while (itr.hasNext()) {
			TaxRateTO cRate = (TaxRateTO) itr.next();
			if (cRate.getTaxCode().equalsIgnoreCase(taxRateCode)) {
				rate = cRate;
				break;
			}
		}

		double totalTaxRate = (rate.getTaxRate1() + rate.getTaxRate2() + rate.getTaxRate3() + rate.getTaxRate4() + rate
				.getTaxRate5()) / 100;
		double taxAmount = totalTaxRate * amount;

		java.math.BigDecimal bd = new java.math.BigDecimal(taxAmount);

		bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);

		taxAmount = bd.doubleValue();

		return taxAmount;
	}

	/**
	 * 
	 * @param taxRateCode
	 * @return
	 */
	public double getTaxRatePercentage(String taxRateCode) {
		TaxRateTO rate = TaxRateBO.NO_TAX;

		Iterator<TaxRateTO> itr = rates.iterator();
		while (itr.hasNext()) {
			TaxRateTO cRate = (TaxRateTO) itr.next();
			if (cRate.getTaxCode().equalsIgnoreCase(taxRateCode)) {
				rate = cRate;
				break;
			}
		}

		double totalTaxRatePercentage = (rate.getTaxRate1() + rate.getTaxRate2() + rate.getTaxRate3() + rate.getTaxRate4() + rate
				.getTaxRate5());

		return totalTaxRatePercentage;
	}

	/**
	 * 
	 * @return
	 */
	public static TaxRateBO getNoTaxRate() {

		if (TaxRateBO.zeroTaxBO == null) {
			ArrayList<TaxRateTO> rateList = new ArrayList<TaxRateTO>();
			TaxRateForZipTO to = new TaxRateForZipTO();
			to.setTaxRates(rateList);
			to.setZip("");
			TaxRateBO.zeroTaxBO = new TaxRateBO(to);
		}
		return TaxRateBO.zeroTaxBO;
	}
}
