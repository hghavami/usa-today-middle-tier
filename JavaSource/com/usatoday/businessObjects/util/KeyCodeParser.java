/*
 * Created on Apr 28, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.util;

/**
 * @author aeast
 * @date Apr 28, 2006
 * @class KeyCodeParser
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class KeyCodeParser {

	String keyCode = "";

	/**
     * 
     */
	public KeyCodeParser() {
		super();
	}

	public KeyCodeParser(String kCode) {
		super();
		if (kCode != null) {
			this.keyCode = kCode.toUpperCase();
		}
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public void setKeyCode(String kCode) {
		if (kCode != null) {
			this.keyCode = kCode.toUpperCase();
		}
	}

	public String getPromoCode() {
		if (this.keyCode != null && this.keyCode.length() == 5) {
			return keyCode.substring(1, 3);
		} else {
			return "";
		}
	}

	public String getSourceCode() {
		if (this.keyCode != null && this.keyCode.length() == 5) {
			return keyCode.substring(0, 1);
		} else {
			return "";
		}
	}

	public String getContestCode() {
		if (this.keyCode != null && this.keyCode.length() == 5) {
			return keyCode.substring(3);
		} else {
			return "";
		}
	}
}
