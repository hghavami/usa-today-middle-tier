/*
 * Created on Jul 27, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.businessObjects.util.crypto;

import java.security.KeyStore;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;

import javax.crypto.Cipher;

import com.usatoday.UsatException;
import com.usatoday.businessObjects.util.mail.EmailAlert;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * 
 *         This class is used to apply Strong encryption on Credit Cards
 * 
 */
public class USATCreditCardEncryptor {

	private static PublicKey publicKey = null;
	private static KeyStore keystore = null;

	private static String publicKeyAlias = null;
	private static String keyStorePath = null;
	private static String keyStorePassword = null;
	private static boolean providersAdded = false;

	private static boolean alertSent = false;

	/**
	 * 
	 */
	public USATCreditCardEncryptor() {
		super();

		if (USATCreditCardEncryptor.providersAdded == false) {
			USATCreditCardEncryptor.providersAdded = true;
			addProviders();
		}
		this.getPublicKey();
	}

	/**
	 * 
	 * @param plainBytes
	 * @return
	 */
	public byte[] encryptBytes(byte[] plainBytes) throws UsatException {
		byte[] cipherBytes = null;
		try {

			PublicKey key = this.getPublicKey();

			Cipher cipher = Cipher.getInstance(key.getAlgorithm());

			cipher.init(Cipher.ENCRYPT_MODE, key);

			cipherBytes = cipher.doFinal(plainBytes);

		} catch (Exception e) {
			System.out.println("Failed to encrypt a string: " + e.getClass().getName() + "\tMessage:" + e.getMessage());
			throw new UsatException(e);
		}
		return cipherBytes;
	}

	/**
	 * 
	 * @param plainText
	 *            The text that needs encrypting
	 * @return
	 */
	public byte[] encryptString(String plainText) throws UsatException {
		byte[] encryptedBytes = null;

		if (plainText != null) {
			encryptedBytes = this.encryptBytes(plainText.getBytes());
		} else {
			throw new UsatException("Null text passed to encryption routine");
		}
		return encryptedBytes;
	}

	/**
	 * 
	 * @param bytes
	 * @return A base 64 encoded string with newlines every 1024 characters
	 */
	public String encodeBytes(byte[] bytes) throws UsatException {
		String encodedString = null;

		if (bytes != null && bytes.length > 0) {
			USATBase64Encoder b64Encoder = new USATBase64Encoder();
			encodedString = b64Encoder.encode(bytes);
		}

		return encodedString;
	}

	/**
	 * 
	 * @param plainText
	 * @return The base 64 encoded, encrypted, string with newlines every 1024 characaters
	 */
	public String encryptAndEncodeString(String plainText) throws UsatException {

		byte[] encryptedBytes = null;

		if (plainText != null) {
			encryptedBytes = this.encryptBytes(plainText.getBytes());
		} else {
			throw new UsatException("Null text passed to encryption routine");
		}

		return this.encodeBytes(encryptedBytes);
	}

	/**
	 * @return
	 */
	public static String getPublicKeyAlias() {
		return publicKeyAlias;
	}

	/**
	 * @param string
	 */
	public static void setPublicKeyAlias(String string) {
		if (publicKeyAlias == null || !publicKeyAlias.equalsIgnoreCase(string)) {
			publicKeyAlias = string;
			USATCreditCardEncryptor.publicKey = null;
		}
	}

	/**
	 * 
	 * @return
	 */
	private final KeyStore getKeystore() {
		if (USATCreditCardEncryptor.keystore == null) {
			try {
				USATCreditCardEncryptor.keystore = KeyStore.getInstance(KeyStore.getDefaultType());

				String password = USATCreditCardEncryptor.getKeyStorePassword();
				java.io.FileInputStream fis = new java.io.FileInputStream(USATCreditCardEncryptor.getKeyStorePath());

				USATCreditCardEncryptor.keystore.load(fis, password.toCharArray());
				fis.close();
			} catch (Exception e) {
				System.out.println("Error creating keystore: " + e.getMessage());
				USATCreditCardEncryptor.keystore = null;
			}
		}
		return USATCreditCardEncryptor.keystore;
	}

	/**
	 * 
	 * @return
	 */
	private final PublicKey getPublicKey() {
		if (USATCreditCardEncryptor.publicKey == null) {
			try {

				KeyStore ks = this.getKeystore();

				java.security.cert.Certificate cert = ks.getCertificate(USATCreditCardEncryptor.getPublicKeyAlias());
				if (cert == null) {
					throw new Exception("Certificate is null for public key: " + USATCreditCardEncryptor.getPublicKeyAlias());
				}

				USATCreditCardEncryptor.publicKey = cert.getPublicKey();

				// make sure we have successfully pulled key in case fails later.
				USATCreditCardEncryptor.alertSent = false;
			} catch (Exception e) {
				System.out.println("exception getting public key: " + e.getMessage());

				if (USATCreditCardEncryptor.alertSent == false) {
					USATCreditCardEncryptor.alertSent = true;
					EmailAlert eAlert = new EmailAlert();
					eAlert.setSender(UsaTodayConstants.EMAIL_ALERT_SENDER);
					eAlert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
					eAlert.setSubject("Strong Encryption Failed to initialize.");
					eAlert.setBodyText("Failed to retrieve Encryption key from keystore: Exception: " + e.getMessage()
							+ "; Key Alias: " + USATCreditCardEncryptor.publicKeyAlias);
					eAlert.sendAlert();
				}
			}
		}
		return USATCreditCardEncryptor.publicKey;
	}

	/**
     * 
     *
     */
	private void addProviders() {

		try {
			Cipher.getInstance("RSA");
		} catch (Exception e) {
			System.out.println("Installing IBM JCE provider");
			Provider ibmjce = new com.ibm.crypto.provider.IBMJCE();
			Security.addProvider(ibmjce);
		}

	}

	/**
	 * @return
	 */
	public static String getKeyStorePath() {
		return USATCreditCardEncryptor.keyStorePath;
	}

	/**
	 * @param string
	 */
	public static void setKeyStorePath(String string) {
		if (USATCreditCardEncryptor.keyStorePath == null || !USATCreditCardEncryptor.keyStorePath.equalsIgnoreCase(string)) {
			USATCreditCardEncryptor.keyStorePath = string;
			USATCreditCardEncryptor.keystore = null;
		}
	}

	/**
	 * @return
	 */
	public static String getKeyStorePassword() {
		return USATCreditCardEncryptor.keyStorePassword;
	}

	/**
	 * @param string
	 */
	public static void setKeyStorePassword(String string) {
		USATCreditCardEncryptor.keyStorePassword = string;
	}

}
