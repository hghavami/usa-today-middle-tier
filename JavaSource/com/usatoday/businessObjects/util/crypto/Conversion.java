package com.usatoday.businessObjects.util.crypto;

/**
 * Insert the type's description here. Creation date: (11/28/2000 6:32:27 PM)
 * 
 * @author:
 */
/*
 * import java.sql.*; import java.util.*; import java.text.*; import java.io.*; import javax.servlet.*; import javax.servlet.http.*;
 */
public class Conversion {
	// Number Conversion 0 1 2 3 4 5 6 7 8 9
	private static String KEY1[] = { "Q", "C", "X", "U", "K", "G", "M", "F", "A", "Z" };

	private static String KEY2[] = { "M", "U", "J", "A", "F", "S", "Z", "G", "P", "C" };

	private static String KEY3[] = { "P", "S", "A", "M", "Z", "Q", "K", "J", "X", "G" };

	private static String KEY4[] = { "C", "F", "U", "Q", "Z", "P", "J", "X", "K", "S" };

	// Method to Encrypt Credit Cards
	public static String convertdata(String expiration, String ccnumber) {

		// Init Values
		String month = null;
		// String year = null;
		StringBuffer changed = new StringBuffer("");
		int start = 0;

		// Expect expiration date in MMYY format
		int expirelength = expiration.length();
		if (expirelength == 4) {
			month = expiration.substring(0, 2);
			// year = expiration.substring(2,4);
		} else {
			month = expiration.substring(0, 1);
			// year = expiration.substring(1,3);
		}

		// Determine length of CC Number
		String original = ccnumber;
		int cclength = (original.length() - 1); // Minus 1 because a zero based array is used

		// Convert CC Expiration date into 1 of 4 Keys

		int monthint = Integer.parseInt(month);
		int sum = monthint + 5; // Add 5

		// Divide sum by 3 to create key
		int key = sum / 3;
		key = key - 1; // Minus 1

		// Select Key algorithm
		switch (key) {

		case 1:
			while (start <= cclength) {

				// Get characters
				String currentchar = "" + original.charAt(start);

				// Convert to integer
				int currentint = (Integer.parseInt(currentchar));

				// Get encrypted value
				String newValue = KEY1[currentint];

				// Write encrypted value to StringBuffer
				changed.append(newValue);

				// Increment char position
				start = start + 1;
			}
			break;
		case 2:
			while (start <= cclength) {

				// Get characters
				String currentchar = "" + original.charAt(start);

				// Convert to integer
				int currentint = (Integer.parseInt(currentchar));

				// Get encrypted value
				String newValue = KEY2[currentint];

				// Write encrypted value to StringBuffer
				changed.append(newValue);

				// Increment char position
				start = start + 1;
			}
			break;

		case 3:
			while (start <= cclength) {

				// Get characters
				String currentchar = "" + original.charAt(start);

				// Convert to integer
				int currentint = (Integer.parseInt(currentchar));

				// Get encrypted value
				String newValue = KEY3[currentint];

				// Write encrypted value to StringBuffer
				changed.append(newValue);

				// Increment char position
				start = start + 1;
			}
			break;

		case 4:
			while (start <= cclength) {

				// Get characters
				String currentchar = "" + original.charAt(start);

				// Convert to integer
				int currentint = (Integer.parseInt(currentchar));

				// Get encrypted value
				String newValue = KEY4[currentint];

				// Write encrypted value to StringBuffer
				changed.append(newValue);

				// Increment char position
				start = start + 1;
			}
			break;

		default:
			// we shouldn't get here!

		}
		String converted = changed.toString();
		return converted;

	}

	// Method to Encrypt Credit Cards
	public static String convertexp(String expiration) {

		// Init Values
		String month = null;
		String year = null;
		StringBuffer changed = new StringBuffer("");

		// Expect expiration date in MMYY format
		int expirelength = expiration.length();
		if (expirelength == 4) {
			month = expiration.substring(0, 2);
			year = expiration.substring(2, 4);
		} else {
			month = expiration.substring(0, 1);
			year = expiration.substring(1, 3);
		}

		// Add year to month
		int monthint = Integer.parseInt(month);
		int yearint = Integer.parseInt(year);
		int sum = monthint + yearint;

		// Create changed expiration date
		// Add year to month to hide 'monthkey' value
		// New format is [(month + year), year]
		String monthstring = Integer.toString(sum);

		if (sum < 10) {
			changed.append("0");
		}

		changed.append(monthstring);
		changed.append(year);

		String converted = changed.toString();

		return converted;

	}
}
