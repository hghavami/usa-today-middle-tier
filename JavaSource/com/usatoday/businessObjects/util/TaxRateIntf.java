/*
 * Created on Apr 19, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.util;

/**
 * @author aeast
 * @date Apr 19, 2006
 * @class TaxRateIntf
 * 
 *        This class represents a tax rate.
 * 
 */
public interface TaxRateIntf {

	/**
	 * 
	 * @return The zip code that this rate applies to
	 */
	public abstract String getZip();

	/**
	 * 
	 * @param taxRateCode
	 *            The tax code to use (UT/BW/Other)
	 * @param amount
	 *            the amount that needs to be taxed
	 * @return The tax for this amount
	 */
	public abstract double computeTax(String taxRateCode, double amount);
}
