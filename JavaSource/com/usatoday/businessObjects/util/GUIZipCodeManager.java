/*
 * Created on May 2, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.util;

import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.businessObjects.cache.CacheManager;
import com.usatoday.integration.Zip5DAO;

/**
 * @author aeast
 * @date May 2, 2006
 * @class GUIZipCodeManager
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class GUIZipCodeManager implements CacheManager {

	private static GUIZipCodeManager _instance = null;

	HashMap<String, String> guiZipCache = new HashMap<String, String>();
	DateTime lastClearedTime = null;

	/**
     * 
     */
	private GUIZipCodeManager() {
		super();
		this.lastClearedTime = new DateTime();
		this.loadData();
	}

	public static synchronized final GUIZipCodeManager getInstance() {
		if (GUIZipCodeManager._instance == null) {
			GUIZipCodeManager._instance = new GUIZipCodeManager();
		}
		return GUIZipCodeManager._instance;
	}

	private void loadData() {
		Zip5DAO dao = new Zip5DAO();
		try {
			this.guiZipCache = dao.getGUIZips();
		} catch (UsatException ue) {
			System.out.println("EXCEPTION GUIZipCodeManager:loadData() - Failed to load GUI Zip Code Data : " + ue.getMessage());
			System.out.println("NO GUI Address validation will take place.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.businessObjects.cache.CacheManager#clearCache()
	 */
	public void clearCache() {
		this.guiZipCache.clear();
		this.loadData();
		this.lastClearedTime = new DateTime();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.businessObjects.cache.CacheManager#getLastClearedTime()
	 */
	public DateTime getLastClearedTime() {
		return lastClearedTime;
	}

	/**
	 * 
	 * @param pubcode
	 *            The publication code
	 * @param zipcode
	 *            The zip code to check
	 * @return true if it is a Gannett Unit otherwise false
	 */
	public boolean isGUIZip(String zipcode) {
		boolean GUIZip = false;

		if (this.guiZipCache.containsValue(zipcode)) {
			GUIZip = true;
		}

		return GUIZip;
	}

	/**
	 * 
	 * @param pubcode
	 *            The publication code
	 * @param zipcode
	 *            The zip code to check
	 * @return true if it is a Gannett Unit otherwise false
	 */
	public boolean isGUIZipForPub(String pub, String zipcode) {
		boolean GUIZip = false;

		if (this.guiZipCache.containsKey(pub + zipcode)) {
			GUIZip = true;
		}

		return GUIZip;
	}

	public static int getNumberCached() {
		return getInstance().guiZipCache.size();
	}
}
