package com.usatoday.businessObjects.util.mail;

import com.exacttarget.*;
import java.rmi.RemoteException;

import javax.servlet.http.HttpSession;

import com.exacttarget.APIObject;
import com.exacttarget.Email;
import com.exacttarget.SystemStatusRequestMsg;
import com.exacttarget.TriggeredSendStatusEnum;
import com.usatoday.util.constants.UsaTodayConstants;

public class ExactTargetOrderGiftCard {
	/**
	 * 
	 */
	private Integer emailID = null;
	private String emailAddress = null;
	private String firstName = null;
	private String lastName = null;
	private String refEmail = null;
	private String refMsg = null;
	private String customerKey = null; // interaction
	private String emailDesc = null;
	private String returnCode = null;
	private String gpName = null;
	private String term = null;
	private String pub = null;
	private String startDate = null;
	Attribute[] attArray = new Attribute[8];
	Subscriber[] subArray = new Subscriber[1];

	public ExactTargetOrderGiftCard() {
		super();

	}

	/**
	 * /**
	 * 
	 * @param string
	 */
	public void setPub(String string) {
		this.pub = string;
	}

	/**
	 * @return
	 */
	public String getPub() {
		return this.pub;
	}

	/**
	 * /**
	 * 
	 * @param string
	 */
	public void setStartDate(String string) {
		this.startDate = string;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * /**
	 * 
	 * @param string
	 */
	public void setTerm(String string) {
		this.term = string;
	}

	/**
	 * @return
	 */
	public String getTerm() {
		return this.term;
	}

	/**
	 * /**
	 * 
	 * @param string
	 */
	public void setRefMsg(String string) {
		this.refMsg = string;
	}

	/**
	 * @return
	 */
	public String getRefMsg() {
		return this.refMsg;
	}

	/**
	 * @return
	 */
	public String getGPFullName() {
		return this.gpName;
	}

	/**
	 * @param string
	 */
	public void setGPFullName(String string) {
		this.gpName = string;
	}

	/**
	 * @return
	 */
	public String getCustomerKey() {
		return this.customerKey;
	}

	/**
	 * @param string
	 */
	public void setCustomerKey(String string) {
		this.customerKey = string;
	}

	/**
	 * @return
	 */
	public String getEmailDesc() {
		return this.emailDesc;
	}

	/**
	 * @param string
	 */
	public void setEmailDesc(String string) {
		this.emailDesc = string;
	}

	/**
	 * @return
	 */
	public String getRefEmail() {
		return this.refEmail;
	}

	/**
	 * @param string
	 */
	public void setRefEmail(String string) {
		this.refEmail = string;
	}

	/**
	 * @return
	 */
	public String getEmailAddress() {
		return this.emailAddress;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * @param string
	 */
	public void setEmailAddress(String string) {
		this.emailAddress = string;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		this.firstName = string;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		this.lastName = string;
	}

	public static String checkExactTargetWebService(HttpSession session1) throws Exception {

		if (UsaTodayConstants.debug) {
			System.out.println("Begin checkExactTargetWebService...");
		}

		String returnCode = null;
		ExactTargetOrderGiftCard etCheck = new ExactTargetOrderGiftCard();

		if (etCheck.checkSystemStatus().equalsIgnoreCase("OK")) {
			returnCode = "OK";
		} else {
			returnCode = "Error";
		}

		// a returnCode of OK means the web service api is available
		if (UsaTodayConstants.debug) {
			System.out.println("returnCode =   " + returnCode);

			System.out.println("End checkExactTargetWebService...");
		}

		return returnCode;
	}

	public String checkSystemStatus() {

		returnCode = null;
		// configure proxy
		SoapProxy webservice = new SoapProxy();
		SystemStatusRequestMsg statusMsg = new SystemStatusRequestMsg();
		SystemStatusResponseMsg responseMsg = new SystemStatusResponseMsg();
		SystemStatusOptions options = new SystemStatusOptions();

		statusMsg.setOptions(options);

		// call the webservice
		try {
			responseMsg = webservice.getSystemStatus(statusMsg);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			if (UsaTodayConstants.debug) {
				System.out.println("[webservice status message] " + "Error checking ET Webservice");
			}
			e.printStackTrace();

		}

		// process the webservice call results
		SystemStatusResult[] results = responseMsg.getResults();

		String systemSystemStatus = results[0].getStatusCode().toString();

		if (systemSystemStatus.equalsIgnoreCase("OK")) {
			returnCode = "OK";

		} else {
			if (UsaTodayConstants.debug) {
				System.out.println("[webservice status message] " + returnCode);
				System.out.println("[webservice status message] Exact Target Web Service Down");
			}
			returnCode = "Error";
		}
		return returnCode;
	}

	public static String sendExactTargetNoDates(String toEmail, String firstName, String lastName, String gpname, String sEmail,
			String refMsg, String eEditionID, String pub, String term, String startDate) throws Exception {

		String returnCode = null;

		if (UsaTodayConstants.debug) {
			System.out.println("Generating Exact Target Email...Starting");
		}

		ExactTargetOrderGiftCard etSend2 = new ExactTargetOrderGiftCard();

		// email is the only field required by the system
		etSend2.setEmailAddress(toEmail);

		// attributes linked in the outgoing email are required by the email

		// CustomerKey refers to an 'Interaction' defined on the Exact Target management site
		// The "eEditionKey" interaction indicates which email is sent, and which list is written to

		etSend2.setCustomerKey(eEditionID);

		// attributes required for subscriber management process on Exact Target are
		// will need to be added when available.

		String firstNL = firstName.toLowerCase();
		String firstNameU = firstNL.substring(0, 1).toUpperCase() + firstNL.substring(1);
		String lastNL = lastName.toLowerCase();
		String lastNameU = lastNL.substring(0, 1).toUpperCase() + lastNL.substring(1);
		etSend2.setFirstName(firstNameU);
		etSend2.setLastName(lastNameU);

		etSend2.setGPFullName(gpname);

		etSend2.setRefEmail(sEmail);
		etSend2.setRefMsg(refMsg);
		etSend2.setPub(pub);
		etSend2.setTerm(term);
		etSend2.setStartDate(startDate);

		if (etSend2.sendExactTargetEmailNoDates().equalsIgnoreCase("OK")) {
			returnCode = "OK";
		}

		if (UsaTodayConstants.debug) {
			System.out.println("return code..  " + returnCode);

			System.out.println("Exact Target Email Generated...Completed");
		}
		return returnCode;
	}

	public String sendExactTargetEmailNoDates() {
		returnCode = null;
		Subscriber[] subscriberArray = new Subscriber[1];

		// configure soap proxy
		SoapProxy webservice = new SoapProxy();
		SystemStatusRequestMsg statusMsg = new SystemStatusRequestMsg();
		SystemStatusOptions options = new SystemStatusOptions();

		statusMsg.setOptions(options);

		// create Subscriber object
		Subscriber subscriber = new Subscriber();
		subscriber.setEmailAddress(this.emailAddress);
		subscriber.setSubscriberKey(this.emailAddress);

		// assign values to attributes
		Attribute attribute1 = new Attribute();
		attribute1.setName("First Name");
		attribute1.setValue(this.firstName);

		Attribute attribute2 = new Attribute();
		attribute2.setName("Last Name");
		attribute2.setValue(this.lastName);

		Attribute attribute3 = new Attribute();
		attribute3.setName("GP Full Name");
		attribute3.setValue(this.gpName);

		Attribute attribute4 = new Attribute();
		attribute4.setName("Gift Card email");
		attribute4.setValue(this.refEmail);

		Attribute attribute5 = new Attribute();
		attribute5.setName("Gift Card Message");
		attribute5.setValue(this.refMsg);

		Attribute attribute6 = new Attribute();
		attribute6.setName("Pub");
		attribute6.setValue(this.pub);

		Attribute attribute7 = new Attribute();
		attribute7.setName("Term");
		attribute7.setValue(this.term);

		Attribute attribute8 = new Attribute();
		attribute8.setName("Start Date");
		attribute8.setValue(this.startDate);

		// Populate attribute array
		attArray[0] = attribute1;
		attArray[1] = attribute2;
		attArray[2] = attribute3;
		attArray[3] = attribute4;
		attArray[4] = attribute5;
		attArray[5] = attribute6;
		attArray[6] = attribute7;
		attArray[7] = attribute8;

		// assign attribute array to subscriber object
		subscriber.setAttributes(attArray);

		Email etEmail = new Email();
		etEmail.setID(emailID);

		TriggeredSendStatusEnum Active = TriggeredSendStatusEnum.Active;
		TriggeredSendTypeEnum eType = TriggeredSendTypeEnum.Continuous;

		// define the triggered send status definition
		TriggeredSendDefinition tsd = new TriggeredSendDefinition();
		tsd.setCustomerKey(customerKey);

		tsd.setTriggeredSendStatus(Active);
		tsd.setDescription(emailDesc);
		tsd.setEmail(etEmail);
		tsd.setTriggeredSendType(eType);

		// define the triggered send
		TriggeredSend ts = new TriggeredSend();
		ts.setTriggeredSendDefinition(tsd);

		// Populate array of Subscribers
		subscriberArray[0] = subscriber;

		// set subscriber array to triggered send
		ts.setSubscribers(subscriberArray);

		// execute request
		CreateRequest cRequest = new CreateRequest();

		CreateOptions cOptions = new CreateOptions();

		cRequest.setOptions(cOptions);
		cRequest.setObjects(new APIObject[] { ts });

		CreateResponse cResponse;
		try {
			cResponse = webservice.create(cRequest);

			// overall status
			if (cResponse.getOverallStatus().equalsIgnoreCase("OK")) {
				returnCode = "OK";
			} else {
				returnCode = "Error";
			}

			// individual results
			CreateResult[] createResult = cResponse.getResults();

			for (CreateResult dumpStepThrough : createResult) {

				if (UsaTodayConstants.debug) {
					System.out.println("[create status message] " + dumpStepThrough.getStatusMessage());
					System.out.println("[create status code] " + dumpStepThrough.getStatusCode());
				}
			}

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			if (UsaTodayConstants.debug) {
				System.out.println("[Error Exact Target Triggered Send].  Call IT Developer.");
			}
			returnCode = "Error";
			e.printStackTrace();
		}

		return returnCode;
	}

}
