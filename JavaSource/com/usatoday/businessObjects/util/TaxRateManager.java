/*
 * Created on Apr 19, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.util;

import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.businessObjects.cache.CacheManager;
import com.usatoday.integration.TaxRateDAO;
import com.usatoday.integration.TaxRateForZipTO;

/**
 * @author aeast
 * @date Apr 19, 2006
 * @class TaxRateManager
 * 
 *        TODO Add a brief description of this class.
 * 
 */
public class TaxRateManager implements CacheManager {

	private static TaxRateManager mngr = null;

	HashMap<String, TaxRateBO> taxRateCache = new HashMap<String, TaxRateBO>();
	DateTime lastClearedTime = null;

	private TaxRateManager() {
		super();

		this.lastClearedTime = new DateTime();

	}

	/**
	 * 
	 * @return The Singleton instance of this class
	 */
	public static synchronized final TaxRateManager getInstance() {
		if (TaxRateManager.mngr == null) {
			TaxRateManager.mngr = new TaxRateManager();
		}
		return TaxRateManager.mngr;
	}

	private TaxRateBO loadRate(String zip) {
		TaxRateBO rate = null;

		TaxRateDAO dao = new TaxRateDAO();
		try {
			TaxRateForZipTO to = dao.getTaxRatesForZip(zip);

			rate = new TaxRateBO(to);
		} catch (UsatException ue) {
			System.out.println("Failed to load rate for zip: " + zip + " Exception: " + ue.getMessage());
		}

		return rate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.businessObjects.cache.CacheManager#clearCache()
	 */
	public void clearCache() {
		synchronized (this.taxRateCache) {
			this.taxRateCache.clear();
		}
		this.lastClearedTime = new DateTime();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.businessObjects.cache.CacheManager#getLastClearedTime()
	 */
	public DateTime getLastClearedTime() {
		return lastClearedTime;
	}

	/**
	 * 
	 * @param zip
	 * @return
	 */
	public TaxRateBO getTaxRatesForZip(String zip) {
		TaxRateBO rate = null;

		if (zip == null) {
			return null;
		}

		rate = (TaxRateBO) this.taxRateCache.get(zip);
		if (rate == null) {
			// pull it from DB
			rate = this.loadRate(zip);
			if (rate != null) {
				this.taxRateCache.put(zip, rate);
			}
		}
		return rate;
	}

	public static int getNumberCached() {
		return getInstance().taxRateCache.size();
	}
}
