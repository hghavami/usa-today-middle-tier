/*
 * Created on May 2, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.usatoday.UsatException;

/**
 * @author aeast
 * @date May 2, 2006
 * @class UsatDateFormatter
 * 
 *        This class contains date time formatting functions.
 * 
 */
public class UsatDateTimeFormatter {

	/**
     * 
     */
	public UsatDateTimeFormatter() {
		super();
	}

	/**
	 * 
	 * @return The Date in YYYYMMDD Format
	 */
	public static String getCurrentDateInYYYYMMDDFormat() {
		String systemdate = "";

		try {
			SimpleDateFormat updatedate = new SimpleDateFormat("yyyyMMdd");
			Date sysdate = new Date();
			systemdate = updatedate.format(sysdate);
		} catch (Exception e) {
			System.out.println("Error getting date: " + e.getMessage());
			e.printStackTrace();
			systemdate = "ERR";
		}
		return systemdate;
	}

	/**
	 * 
	 * @return The time in HHMMSS format.
	 */
	public static String getSystemTimeHHMMSS() {

		SimpleDateFormat updatetime = new SimpleDateFormat("hhmmss");
		Date systime = new Date();
		String systemtime = updatetime.format(systime);

		return systemtime;
	}

	/**
	 * 
	 * @param date
	 *            in either MMDDYYYY or MM/DD/YYYY format
	 * @return date in YYYYMMDD format
	 */
	public static String convertDateStringFromMMDDYYYYtoYYYYMMDD(String date) throws UsatException {
		StringBuffer newDate = new StringBuffer();
		if (date == null) {
			throw new UsatException("Invalid date sent to convert: Must be in MMDDYYYY or MM/DD/YYYY format.");
		}

		date = date.replaceAll("/", "");

		if (date.length() != 8) {
			throw new UsatException("Invalid date sent to convert: Must be in MMDDYYYY or MM/DD/YYYY format.");
		}

		newDate.append(date.substring(4));
		newDate.append(date.substring(0, 2));
		newDate.append(date.substring(2, 4));

		return newDate.toString();

	}

	/**
	 * 
	 * Resulting date will be in format MM/DD/YYYY
	 * 
	 * @param date
	 *            - format YYYYMMDD
	 * @return - date string in format MM/DD/YYYY
	 * @throws UsatException
	 */
	public static String convertDateStringFromYYYYMMDDtoMMDDYYYY(String date) throws UsatException {
		StringBuffer newDate = new StringBuffer();
		if (date == null) {
			throw new UsatException("Invalid date sent to convert: Must be in YYYYMMDD format.");
		}

		if (date.length() != 8) {
			throw new UsatException("Invalid date sent to convert: Must be in YYYYMMDD format.");
		}

		newDate.append(date.substring(4, 6));
		newDate.append("/");
		newDate.append(date.substring(6));
		newDate.append("/");
		newDate.append(date.substring(0, 4));

		return newDate.toString();

	}

	public static DateTime convertYYYYMMDDToDateTime(String yyyyMMdd) {

		DateTime dt = null;
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
		try {
			dt = formatter.parseDateTime(yyyyMMdd);
		} catch (Exception e) {
			// failed to format date
			System.out.println("UsatDateTimeFormatter::convertYYYYMMDDToDateTime() Invalid date : " + yyyyMMdd);
		}
		return dt;
	}

	/**
	 * 
	 * @param MMddyyyy
	 *            MM/dd/yyyy format string
	 * @return
	 */
	public static DateTime convertMMDDYYYYToDateTime(String MMddyyyy) {

		DateTime dt = null;
		DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
		try {
			dt = formatter.parseDateTime(MMddyyyy);
		} catch (Exception e) {
			// failed to format date
			System.out.println("UsatDateTimeFormatter::convertMM_slash_DD_slash_YYYY_ToDateTime() Invalid date : " + MMddyyyy);
		}
		return dt;
	}

	public static boolean verifyInputDate(String unknown_date) {

		try {
			String trimdate = unknown_date;
			String chkDate = trimdate.trim();

			int len = chkDate.length();
			if (len < 8) {
				return false;
			}

			String date = chkDate.replace('-', '/');
			int pos = date.indexOf('/');

			if (pos < 0) {
				return false;
			}

			if (pos == 4) // yyyy/mm/dd format
			{
				int year = Integer.parseInt(date.substring(0, pos)); // year
				if (year < 2000) {
					return false;
				}

				int pos_prev = pos;
				pos = date.indexOf('/', pos + 1);

				int month = Integer.parseInt(date.substring((pos_prev + 1), pos)); // month
				if (month > 12) {
					return false;
				}

				int day = Integer.parseInt(date.substring(pos + 1)); // day
				if (day > 31) {
					return false;
				}

			} else { // mm/dd/yyyy format
				int month = Integer.parseInt(date.substring(0, pos)); // month
				if (month > 12) {
					return false;
				}

				int pos_prev = pos;
				pos = date.indexOf('/', pos + 1);

				int day = Integer.parseInt(date.substring((pos_prev + 1), pos)); // day
				if (day > 31) {
					return false;
				}

				int year = Integer.parseInt(date.substring(pos + 1)); // year
				if (year < 2000) {
					return false;
				}

			}

			return true;
		} catch (Exception e) {
			System.out.println("UsatDateTimeFormmatter:: Failed to verify date: " + unknown_date);
		}
		return false;
	}

}
