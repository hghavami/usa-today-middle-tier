/*
 * Created on Apr 26, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.util;

import java.util.Iterator;

import com.g1.dcg.client.Server;
import com.g1.dcg.client.ServerException;
import com.g1.dcg.client.Service;
import com.g1.dcg.message.DataRow;
import com.g1.dcg.message.DataTable;
import com.g1.dcg.message.Message;
import com.g1.dcg.message.MessageProcessingException;
import com.gannett.usat.iconapi.client.AddressDeliveryMethodCode1Validation;
import com.gannett.usat.iconapi.domainbeans.Code1Validation.Code1Validation;
import com.gannett.usat.iconapi.domainbeans.Code1Validation.Code1ValidationWrapper;
import com.usatoday.UsatException;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.customer.PersistentAddressBO;

/**
 * @author aeast
 * @date Apr 26, 2006
 * @class Code1AddressValidator
 * 
 *        This class provides an easy interface to validate an address that is entered on a UI screen. It uses the UIAddress object
 *        of the source contact and validate it returning an address that can be stored as a persistent address.
 * 
 *        Sample call: Code1AddressValidator v = new Code1AddressValidator(); v.setSourceContact(myContactVariable);
 *        myContactVariable = v.validateAddress(); // returns the same contact with a validated address // and the persistent
 *        version of the address set
 * 
 * 
 */
public class Code1AddressValidator {

	ContactBO contact = null;

	boolean questionableAddress = true;
	boolean goodAddress = false;

	public PersistentAddressBO validateAddress() throws UsatException {

		if (this.getSourceContact() == null) {
			throw new UsatException("Can't validate address without setting setSourceContact() first.");
		}

		String zip5 = this.getSourceContact().getUIAddress().getZip();
		String street1 = this.getSourceContact().getUIAddress().getAddress1();
		String street2 = this.getSourceContact().getUIAddress().getAptSuite();
		String city = this.getSourceContact().getUIAddress().getCity();
		String state = this.getSourceContact().getUIAddress().getState();

		// Check and remove any &
		String companyName = this.getSourceContact().getFirmName();
		if (companyName != null) {
			companyName = companyName.replaceAll("&", " ");

		} else {
			companyName = "";
		}

		// Perform set-ups to access Code-1 HotData ver 4.0

		// Create a server instance
		Server server = this.getCode1Server();

		// Connect to the server
		try {
			server.connect();
		} catch (Exception e) {
			// Unable to connect to Group-1's HotData server
			e.printStackTrace();
			throw new UsatException("Failed to conect to Code1 Server: " + e.getMessage());
		}

		// Get service from server
		Service service = null;
		try {
			service = server.getService("ValidateAddress");
		} catch (ServerException e) {
			// Unable to obtain the HotData ValidateAddress service
			e.printStackTrace();
			throw new UsatException("Unable to obtain the HotData ValidateAddress service: " + e.getMessage());
		}
		// Create input message
		Message request = new Message();

		// Format address in data table - sets dataset to the input mesage
		DataTable dataSet = request.getDataTable();

		DataRow AddrRow = dataSet.newRow();
		AddrRow.set("PostalCode", zip5);
		AddrRow.set("AddressLine1", street1);
		AddrRow.set("AddressLine2", street2);
		AddrRow.set("FirmName", companyName);
		AddrRow.set("City", city);
		AddrRow.set("StateProvince", state);
		dataSet.addRow(AddrRow);

		Code1Config config = Code1Config.getInstance();

		// Set Options
		request.putOption("OutputRecordType", config.getCode1OutRecType());
		request.putOption("OutputFormattedOnFail", config.getCode1OutFormat());
		request.putOption("OutputCasting", config.getCode1OutCase());
		request.putOption("OutputPostalCodeSeparator", config.getCode1OutCodeSep());
		request.putOption("OutputMultinationalCharacters", config.getCode1MultiNatChar());
		request.putOption("OutputCountryFormat", config.getCode1OutCountry());
		request.putOption("OutputFieldLevelReturnCodes", config.getCode1OutRetCodes());
		request.putOption("PerformDPV", config.getCode1DPV());
		request.putOption("PerformRDI", config.getCode1RDI());
		request.putOption("PerformESM", config.getCode1ESM());
		request.putOption("StreetMatchingStrictness", config.getCode1StreetStr());
		request.putOption("FirmMatchingStrictness", config.getCode1FirmStr());
		request.putOption("DirectionalMatchingStrictness", config.getCode1DirStr());
		request.putOption("DualAddressLogic", config.getCode1DualAddr());
		request.putOption("OutputShortCityName", config.getCode1OutShortCity());

		// Send the request message to the Server
		Message reply = null;
		try {
			reply = service.process(request);

		} catch (MessageProcessingException e) {
			// Unable to send the HotData message request
			e.printStackTrace();
			throw new UsatException("Failed to process Code1 ValidateAddress request. Code1 MessageProcessingException:  "
					+ e.getMessage());
		}
		// Retrieve the response

		DataTable returnDataTable = reply.getDataTable();
		// String[] columnNames = returnDataTable.getColumnNames();
		Iterator<?> iter = (Iterator<?>) returnDataTable.iterator();

		DataRow row = (DataRow) iter.next();

		// retrieve address fields from Code-1

		String cGenRC = row.get("Status");
		String cMatProbCorr = " ";

		// cMatProbCorr is sometimes a null
		try {
			cMatProbCorr = row.get("Confidence");
		} catch (NullPointerException e) {
			cMatProbCorr = " ";
		}

		String szSALeadDir = row.get("LeadingDirectional");
		String szSAHouseNum = row.get("HouseNumber");

		// Note - in HotData ver 4.0 there is no longer an alternate house
		// number (szA1HouseHum) or
		// street name (szA1StreetName) or street suffix (szA1Sfx) field that
		// can be used if it is not retrieved.

		String szSAStrNam = row.get("StreetName");
		String szSAStrSfx = row.get("StreetSuffix");
		String szSATrailDir = row.get("TrailingDirectional");
		String szSAAptType = row.get("ApartmentLabel");
		String szSAAptNum = row.get("ApartmentNumber");
		String szStdAddr = row.get("AddressLine1");

		// szLongCityName = City when OutputShortCityName option is set to N.
		String szLongCityNam = row.get("City");
		String szStateAbbr = row.get("StateProvince");

		// PostalCode is returned in the format zip5-zip4
		String szFinalZIP = null;
		try {
			szFinalZIP = row.get("PostalCode").substring(0, 5);
		} catch (Exception exp) {
			// likely string index out of bounds
			szFinalZIP = row.get("PostalCode");
			if (szFinalZIP == null) {
				szFinalZIP = "";
			}
		}
		String szSAPOBox = " ";
		try {
			szSAPOBox = row.get("POBox");
		} catch (NullPointerException e) {
			// do nothing
		}
		// szA1RRHCType is no longer supported. Use RRHC - the Rural
		// Route/Highway Contract Indicator
		String szA1RRHCType = row.get("RRHC");

		// Disconnect from the server
		server.disconnect();

		int numcMatProbCorr = 0;

		try {
			numcMatProbCorr = (new Integer(cMatProbCorr)).intValue();
		} catch (NumberFormatException e) {
			numcMatProbCorr = 1;
		}

		boolean isPOBox = false;
		String tempStreetName = "";
		if (szSAStrNam != null) {
			tempStreetName = szSAStrNam.trim();
		}
		if (tempStreetName.length() == 0 && szStdAddr != null) {
			tempStreetName = szStdAddr.trim();
		}

		// Perform processing based on the return code/status. S = Successful.
		if ("S".equalsIgnoreCase(cGenRC)) {
			if (numcMatProbCorr < 91) {
				this.setGoodAddress(true);
				this.setQuestionableAddress(true);
			} else { // address is good and not questionable
				this.setGoodAddress(true);
				this.setQuestionableAddress(false);
			}
			this.getSourceContact().getUiAddress().setValidated(true);

			// if address is a Post Office Box, combine PO Box address with
			// Street Name for AS/400
			try {
				if (szSAPOBox.trim().length() != 0) {
					isPOBox = true;
					try {
						tempStreetName = szSAStrNam.trim() + " " + szStdAddr.trim();
						tempStreetName = tempStreetName.trim();
					} catch (NullPointerException e) {
						tempStreetName = szStdAddr.trim();
					}
				}
			} catch (Exception e) {
				// do nothing
			}

			// if address is a Rural Route (RR), combine RR address with Street
			// Name for AS/400
			try {
				if (szA1RRHCType.trim().length() != 0) {
					try {

						tempStreetName = szSAStrNam.trim() + " " + szStdAddr.trim();
						tempStreetName = tempStreetName.trim();
					} catch (NullPointerException e) {
						tempStreetName = szStdAddr.trim();
					}
				}
			} catch (Exception e) {
				// do nothing
			}
		} else { // did not get a good response back
			this.setGoodAddress(false);
			this.setQuestionableAddress(true);
		}

		// Determine if military address
		boolean militaryAddress = false;
		if ((szLongCityNam != null) && (szStateAbbr != null)) {
			if (szLongCityNam.equals("APO") || szLongCityNam.equals("APF") || szLongCityNam.equals("AA")
					|| szLongCityNam.equals("AE") || szStateAbbr.equals("AP")) {
				militaryAddress = true;
			}
		} else {
			if (city.equals("APO") || city.equals("APF") || state.equals("AA") || state.equals("AE")) {
				militaryAddress = true;
			}
		}

		// Determine if Gannett Unit Address
		// Check for Gannett Unit ID zip code and set its flag accordingly only
		// if no PO Box address is entered
		boolean guiZipFound = false;
		// HotData ver 4.0 - CorrectedStreet1 = "" for an invalid address.
		if (!isPOBox) {
			guiZipFound = GUIZipCodeManager.getInstance().isGUIZip(szFinalZIP);
		}

		// PersistentAddressBO(String streetDir, String halfUnitNum,
		// String city, String state, String streetName, String streetPostDir,
		// String streetType, String subUnitCode, String subUnitNum,
		// String unitNum, String zip, String address2)

		PersistentAddressBO pAddress = null;
		if (this.isGoodAddress()) {
			pAddress = new PersistentAddressBO(szSALeadDir, "", szLongCityNam, szStateAbbr, tempStreetName, szSATrailDir,
					szSAStrSfx, szSAAptType, szSAAptNum, szSAHouseNum, szFinalZIP, this.getSourceContact().getUIAddress()
							.getAddress2(), militaryAddress, guiZipFound);
			pAddress.setValidated(true);
		}

		return pAddress;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	private Server getCode1Server() throws UsatException {
		Server server = new Server();

		Code1Config config = Code1Config.getInstance();

		// Set connection properties to the server
		server.setConnectionProperty(Server.HOST, config.getCode1ServerURL());
		server.setConnectionProperty(Server.PORT, config.getCode1Port());
		server.setConnectionProperty(Server.PATH, config.getCode1Path());
		server.setConnectionProperty(Server.ACCOUNT_ID, config.getCode1AccountId());
		server.setConnectionProperty(Server.ACCOUNT_PASSWORD, config.getCode1AccountPassword());
		server.setConnectionProperty(Server.CONNECTION_TYPE, config.getCode1ConnectionType());
		server.setConnectionProperty(Server.CONNECTION_TIMEOUT, config.getCode1ConnectionTimeout());

		return server;
	}

	public ContactBO getSourceContact() {
		return this.contact;
	}

	public void setSourceContact(ContactBO contact) {
		this.contact = contact;
	}

	public boolean isGoodAddress() {
		return this.goodAddress;
	}

	public boolean isQuestionableAddress() {
		return this.questionableAddress;
	}

	private void setGoodAddress(boolean goodAddress) {
		this.goodAddress = goodAddress;
	}

	private void setQuestionableAddress(boolean questionableAddress) {
		this.questionableAddress = questionableAddress;
	}

	public PersistentAddressBO validateGenesysAddress() throws UsatException {

		if (this.getSourceContact() == null) {
			throw new UsatException("Can't validate address without setting setSourceContact() first.");
		}

		String zip5 = this.getSourceContact().getUIAddress().getZip();
		String street1 = this.getSourceContact().getUIAddress().getAddress1();
		String street2 = this.getSourceContact().getUIAddress().getAptSuite();
		String city = this.getSourceContact().getUIAddress().getCity();
		String state = this.getSourceContact().getUIAddress().getState();

		// Check and remove any &
		String companyName = this.getSourceContact().getFirmName();
		if (companyName != null) {
			companyName = companyName.replaceAll("&", " ");

		} else {
			companyName = "";
		}

		AddressDeliveryMethodCode1Validation sApi = new AddressDeliveryMethodCode1Validation();
		Code1ValidationWrapper addressCvTable = null;

		try {
			addressCvTable = sApi.getAddressCode1ValidationObject("UT", street1, street2, city, state, zip5);
		} catch (Exception e) {
			e.printStackTrace();
			throw new UsatException(
					"Code 1 address validation:: ValidateGenesysAddress - Failed to get Code1 validated address from Genesys, because: "
							+ "  " + e.getMessage());
		}

		// retrieve address fields from Code-1
		Code1Validation cvBean = addressCvTable.getAddress();
		// for (DeliveryMethodCode1Validate dmcvBean : dmcvBean.getDeliveryMethodCode1ValidateTable()) {

//		System.out.println("Account: " + cvBean.toString());
		// iterate over result set and build objects

		String cGenRC = cvBean.getResultant();
		// String cMatProbCorr = " ";

		// cMatProbCorr is sometimes a null
		// try {
		// cMatProbCorr = cvBean.getDelivery_point();
		// } catch (NullPointerException e) {
		// cMatProbCorr = " ";
		// }

		String szSALeadDir = cvBean.getPre_direction();
		String szSAHouseNum = cvBean.getHouse_unit();
		String szSAStrNam = cvBean.getStreet();
		String szSAStrSfx = cvBean.getStreet_type();
		String szSATrailDir = cvBean.getPost_direction();
		String szSAAptType = cvBean.getSub_type();
		String szSAAptNum = cvBean.getSub_number();
		String szStdAddr = cvBean.getStreet();

		// szLongCityName = City when OutputShortCityName option is set to N.
		String szLongCityNam = cvBean.getCity();
		String szStateAbbr = cvBean.getState();

		// PostalCode is returned in the format zip5-zip4
		String szFinalZIP = null;
		try {
			szFinalZIP = cvBean.getZip().substring(0, 5);
		} catch (Exception exp) {
			// likely string index out of bounds
			szFinalZIP = cvBean.getZip();
			if (szFinalZIP == null) {
				szFinalZIP = "";
			}
		}
		String szSAPOBox = " ";
		try {
			szSAPOBox = cvBean.getBox_number();
		} catch (NullPointerException e) {
			// do nothing
		}

		// int numcMatProbCorr = 0;

		// try {
		// numcMatProbCorr = (new Integer(cMatProbCorr)).intValue();
		// } catch (NumberFormatException e) {
		// numcMatProbCorr = 1;
		// }

		String tempStreetName = "";
		if (szSAStrNam != null) {
			tempStreetName = szSAStrNam.trim();
		}
		if (tempStreetName.length() == 0 && !szStdAddr.equals("")) {
			tempStreetName = szStdAddr.trim();
		}

		// Perform processing based on the return code/status. S = Successful.
		if ("C".equalsIgnoreCase(cGenRC) || "Z".equalsIgnoreCase(cGenRC)) {
			if (cvBean.getUsps_delivery().equals("Y")) {
				this.setGoodAddress(true);
				this.setQuestionableAddress(false);
			} else { // address is good but questionable
				this.setGoodAddress(true);
				this.setQuestionableAddress(true);
			}
			this.getSourceContact().getUiAddress().setValidated(true);

			// if address is a Post Office Box, combine PO Box address with
			// Street Name for AS/400
			try {
				if (szSAPOBox.trim().length() != 0) {
					try {
						// tempStreetName = szSAStrNam.trim() + " " + szStdAddr.trim();
						tempStreetName = szSAStrNam.trim() + " " + szSAPOBox.trim();
						tempStreetName = tempStreetName.trim();
					} catch (NullPointerException e) {
						tempStreetName = szStdAddr.trim();
					}
				}
			} catch (Exception e) {
				// do nothing
			}
		} else { // did not get a good response back
			this.setGoodAddress(false);
			this.setQuestionableAddress(true);
		}

		// Determine if military address
		boolean militaryAddress = false;
		if ((szLongCityNam != null) && (szStateAbbr != null)) {
			if (szLongCityNam.equals("APO") || szLongCityNam.equals("APF") || szLongCityNam.equals("AA")
					|| szLongCityNam.equals("AE") || szStateAbbr.equals("AP")) {
				militaryAddress = true;
			}
		} else {
			if (city.equals("APO") || city.equals("APF") || state.equals("AA") || state.equals("AE")) {
				militaryAddress = true;
			}
		}

		// PersistentAddressBO(String streetDir, String halfUnitNum,
		// String city, String state, String streetName, String streetPostDir,
		// String streetType, String subUnitCode, String subUnitNum,
		// String unitNum, String zip, String address2)
		PersistentAddressBO pAddress = null;
		if (this.isGoodAddress()) {
			pAddress = new PersistentAddressBO(szSALeadDir, "", szLongCityNam, szStateAbbr, tempStreetName, szSATrailDir,
					szSAStrSfx, szSAAptType, szSAAptNum, szSAHouseNum, szFinalZIP, this.getSourceContact().getUIAddress()
							.getAddress2(), militaryAddress, false);
			pAddress.setValidated(true);
		}

		return pAddress;
		// }
	}
}
