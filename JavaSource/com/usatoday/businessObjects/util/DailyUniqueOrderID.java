package com.usatoday.businessObjects.util;

import java.net.InetAddress;
import java.sql.Date;
import java.util.Calendar;
import java.util.Random;

import com.usatoday.integration.SequenceNumbersDAO;

/**
 * @author aeast
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable
 *         and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class DailyUniqueOrderID {

	private Calendar resetDate = null;
	private String systemID = "XT";
	private static String machineID = null;

	/**
	 * Constructor for DailyUniqueDBSequence.
	 */
	public DailyUniqueOrderID() {
		super();
		if (DailyUniqueOrderID.machineID == null) {
			try {
				InetAddress serverAddress = InetAddress.getLocalHost();

				String ip = serverAddress.getHostAddress();
				int index = ip.lastIndexOf(".") + 1;
				setMachineID(ip.substring(index));
			} catch (Exception e) {
				System.out.println("Failed to set machine id in daily unique order id.");
				DailyUniqueOrderID.machineID = "999";
			}
		}
	}

	private synchronized int getNextSequenceNumber() throws Exception {
		//
		// check sequence
		//
		// get next value
		//
		// return
		int nextSequenceNumber = 0;

		try {

			SequenceNumbersDAO dao = new SequenceNumbersDAO();

			Date date = dao.getCurrentSequenceDate();
			if (sequenceNeedsResetting(date)) {
				dao.resetSequence(this.resetDate.getTimeInMillis());
			}
			nextSequenceNumber = dao.getNextSequenceNumber();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("failed to retrieve sequence number.");
			nextSequenceNumber = new Random().nextInt();
		}

		return nextSequenceNumber;
	}

	private boolean sequenceNeedsResetting(Date lastResetDate) {
		boolean needToResetDate = false;

		java.util.Calendar currentCal = java.util.Calendar.getInstance();
		currentCal.set(java.util.Calendar.MILLISECOND, 0);
		currentCal.set(java.util.Calendar.SECOND, 0);
		currentCal.set(java.util.Calendar.MINUTE, 0);
		currentCal.set(java.util.Calendar.HOUR_OF_DAY, 0);

		if (currentCal.getTime().after(lastResetDate)) {
			this.resetDate = currentCal;
			needToResetDate = true;
		}
		return needToResetDate;
	}

	public String generateOrderID() {

		StringBuffer orderID = new StringBuffer(this.getSystemID());

		Calendar cal = Calendar.getInstance();
		try {
			orderID.append(cal.get(Calendar.YEAR)).append(ZeroFilledNumeric.getValue(2, (cal.get(Calendar.MONTH) + 1)))
					.append(ZeroFilledNumeric.getValue(2, cal.get(Calendar.DAY_OF_MONTH)));

			orderID.append(ZeroFilledNumeric.getValue(3, this.getMachineID()));

			orderID.append(ZeroFilledNumeric.getValue(5, this.getNextSequenceNumber()));
		} catch (Exception e) {
			e.printStackTrace();
			int random = (new Random()).nextInt();
			orderID.append(random);
			if (orderID.length() > 18) {
				orderID.setLength(18);
			}
		}

		return orderID.toString();
	}

	/**
	 * Returns the machineID.
	 * 
	 * @return String
	 */
	public String getMachineID() {
		return machineID;
	}

	/**
	 * Returns the pubCode.
	 * 
	 * @return String
	 */
	public String getSystemID() {
		return systemID;
	}

	/**
	 * Sets the machineID.
	 * 
	 * @param machineID
	 *            The machineID to set
	 */
	private void setMachineID(String machineID) {
		if (machineID != null) {
			DailyUniqueOrderID.machineID = machineID;
		}
	}

	/**
	 * Sets the pubCode.
	 * 
	 * @param pubCode
	 *            The pubCode to set
	 */
	public void setSystemID(String pubCode) {
		if (pubCode != null) {
			this.systemID = pubCode;
		}
	}

}
