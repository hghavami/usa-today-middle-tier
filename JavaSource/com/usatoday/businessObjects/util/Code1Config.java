/*
 * Created on Apr 26, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.util;

import java.util.Properties;

import com.usatoday.UsatException;
import com.usatoday.util.constants.USATApplicationPropertyFileLoader;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Apr 26, 2006
 * @class Code1Config
 * 
 *        This class contains the code 1 configuration fields for easy reference.
 * 
 */
public class Code1Config {

	private static Code1Config _instance = null;

	// Attributes
	private String code1ServerURL = "asp.g1.com";
	private String code1Port = "8080";
	private String code1Path = "dcg/gateway";
	private String code1AccountId = "500";
	private String code1AccountPassword = "Group1";
	private String code1ConnectionType = "HTTP";
	private String code1ConnectionTimeout = "40000";
	private String code1OutRecType = "AE";
	private String code1OutFormat = "N";
	private String code1OutCase = "M";
	private String code1OutCodeSep = "Y";
	private String code1MultiNatChar = "N";
	private String code1OutCountry = "E";
	private String code1OutRetCodes = "N";
	private String code1DPV = "N";
	private String code1RDI = "N";
	private String code1ESM = "N";
	private String code1StreetStr = "M";
	private String code1FirmStr = "M";
	private String code1DirStr = "M";
	private String code1DualAddr = "N";
	private String code1OutShortCity = "N";

	private Code1Config() throws Exception {
		super();
		USATApplicationPropertyFileLoader loader = new USATApplicationPropertyFileLoader();
		Properties props = loader.loadPropertyFile(UsaTodayConstants.CODE1_PROPERTY_FILE);
		if (props != null) {
			this.initConfig(props);
		} else {
			throw new UsatException("Could not load Code1 configuration file: " + UsaTodayConstants.CODE1_PROPERTY_FILE);
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public static synchronized Code1Config getInstance() throws UsatException {
		if (Code1Config._instance == null) {
			try {
				Code1Config._instance = new Code1Config();
			} catch (Exception e) {
				e.printStackTrace();
				throw new UsatException("Failed to initialize Code1Config: " + e.getMessage());
			}
		}
		return Code1Config._instance;
	}

	/**
	 * 
	 * @param props
	 */
	private void initConfig(Properties props) {

		String tempStr = props.getProperty("code1Port");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1Port = tempStr;
		}

		tempStr = props.getProperty("code1Path");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1Path = tempStr;
		}
		tempStr = props.getProperty("code1ServerURL");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1ServerURL = tempStr;
		}
		tempStr = props.getProperty("code1AccountId");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1AccountId = tempStr;
		}
		tempStr = props.getProperty("code1AccountPassword");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1AccountPassword = tempStr;
		}
		tempStr = props.getProperty("code1ConnectionType");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1ConnectionType = tempStr;
		}
		tempStr = props.getProperty("code1ConnectionTimeout");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1ConnectionTimeout = tempStr;
		}
		tempStr = props.getProperty("code1OutRecType");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1OutRecType = tempStr;
		}
		tempStr = props.getProperty("code1OutCase");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1OutCase = tempStr;
		}
		tempStr = props.getProperty("code1OutCodeSep");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1OutCodeSep = tempStr;
		}
		tempStr = props.getProperty("code1MultiNatChar");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1MultiNatChar = tempStr;
		}
		tempStr = props.getProperty("code1OutCountry");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1OutCountry = tempStr;
		}
		tempStr = props.getProperty("code1OutRetCodes");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1OutRetCodes = tempStr;
		}
		tempStr = props.getProperty("code1DPV");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1DPV = tempStr;
		}
		tempStr = props.getProperty("code1OutFormat");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1OutFormat = tempStr;
		}
		tempStr = props.getProperty("code1RDI");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1RDI = tempStr;
		}
		tempStr = props.getProperty("code1ESM");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1ESM = tempStr;
		}
		tempStr = props.getProperty("code1StreetStr");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1StreetStr = tempStr;
		}
		tempStr = props.getProperty("code1FirmStr");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1FirmStr = tempStr;
		}
		tempStr = props.getProperty("code1DirStr");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1DirStr = tempStr;
		}
		tempStr = props.getProperty("code1DualAddr");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1DualAddr = tempStr;
		}
		tempStr = props.getProperty("code1OutShortCity");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.code1OutShortCity = tempStr;
		}
	}

	public String getCode1AccountId() {
		return this.code1AccountId;
	}

	public String getCode1AccountPassword() {
		return this.code1AccountPassword;
	}

	public String getCode1ConnectionTimeout() {
		return this.code1ConnectionTimeout;
	}

	public String getCode1ConnectionType() {
		return this.code1ConnectionType;
	}

	public String getCode1DirStr() {
		return this.code1DirStr;
	}

	public String getCode1DPV() {
		return this.code1DPV;
	}

	public String getCode1DualAddr() {
		return this.code1DualAddr;
	}

	public String getCode1ESM() {
		return this.code1ESM;
	}

	public String getCode1FirmStr() {
		return this.code1FirmStr;
	}

	public String getCode1MultiNatChar() {
		return this.code1MultiNatChar;
	}

	public String getCode1OutCase() {
		return this.code1OutCase;
	}

	public String getCode1OutCodeSep() {
		return this.code1OutCodeSep;
	}

	public String getCode1OutCountry() {
		return this.code1OutCountry;
	}

	public String getCode1OutFormat() {
		return this.code1OutFormat;
	}

	public String getCode1OutRecType() {
		return this.code1OutRecType;
	}

	public String getCode1OutRetCodes() {
		return this.code1OutRetCodes;
	}

	public String getCode1OutShortCity() {
		return this.code1OutShortCity;
	}

	public String getCode1Path() {
		return this.code1Path;
	}

	public String getCode1Port() {
		return this.code1Port;
	}

	public String getCode1RDI() {
		return this.code1RDI;
	}

	public String getCode1ServerURL() {
		return this.code1ServerURL;
	}

	public String getCode1StreetStr() {
		return this.code1StreetStr;
	}

	public static void resetConfig() {
		Code1Config._instance = null;
		try {
			Code1Config.getInstance();
		} catch (Exception e) {
			System.out.println("Failed to read reset code1 config! " + e.getMessage());
		}
	}
}
