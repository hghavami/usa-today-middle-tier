package com.usatoday.businessObjects.shopping;

import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.shopping.RenewalOrderItemIntf;

public class RenewalOrderItemBO extends SubscriptionOrderItemBO implements RenewalOrderItemIntf {

	private String renewalTrackingCode = "";
	private String existingDeliveryMethod = "";
	private SubscriberAccountIntf account = null;

	@Override
	public String getRenewalTrackingCode() {
		return this.renewalTrackingCode;
	}

	public void setRenewalTrackingCode(String trackingCode) {

		if (trackingCode != null) {
			if (trackingCode.length() > 6) {
				trackingCode = trackingCode.substring(0, 6);
			}
			this.renewalTrackingCode = trackingCode;
		}

	}

	@Override
	public String getExistingDeliveryMethod() {
		return this.existingDeliveryMethod;
	}

	public void setExistingDeliveryMethod(String method) {
		this.existingDeliveryMethod = method;
	}

	@Override
	public SubscriberAccountIntf getAccount() {
		return this.account;
	}

	@Override
	public void setAccount(SubscriberAccountIntf account) {
		this.account = account;
	}
}
