/*
 * Created on Apr 24, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.shopping;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.shopping.OrderIntf;
import com.usatoday.business.interfaces.shopping.OrderItemIntf;
import com.usatoday.business.interfaces.shopping.RenewalOrderItemIntf;
import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentMethodIntf;
import com.usatoday.business.interfaces.shopping.payment.PaymentMethodIntf;
import com.usatoday.business.interfaces.shopping.payment.PaymentTypeIntf;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.util.DailyUniqueOrderID;
import com.usatoday.businessObjects.util.GUIZipCodeManager;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;
import com.usatoday.businessObjects.util.mail.ExactTargetNoDatesNew;
import com.usatoday.businessObjects.util.mail.SmtpMailSender;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Apr 24, 2006
 * @class OrderBO
 * 
 *        This class represents a placed order. It is created following a checkout and may eventually be persisted for reference and
 *        update.
 * 
 */
public class OrderBO implements OrderIntf {
	private String orderID = "";
	private DateTime orderDate = null;
	private ContactIntf deliveryContact = null;
	private ContactIntf billingContact = null;
	private Collection<OrderItemIntf> orderedItems = null;
	private int orderFulfillmentStatus = 0;
	private int orderPaymentStatus = 0;
	private PaymentMethodIntf paymentMethod = null;
	private boolean billingSameAsDelivery = true;
	private double amountCharged = 0.00;
	private double subTotal = 0.00;
	private double taxAmount = 0.00;

	private ArrayList<EmailRecordBO> savedEmailRecords = null;

	// internal use attributes
	private boolean confirmationEmailSent = false;
	private boolean isSWSubscription = false;
	// private boolean isUTSubscription = false;
	private boolean isEESubscription = false;

	public OrderBO() {
		super();

		savedEmailRecords = new ArrayList<EmailRecordBO>();

		DailyUniqueOrderID idGenerator = new DailyUniqueOrderID();

		this.orderID = idGenerator.generateOrderID();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#getOrderID()
	 */
	public String getOrderID() {
		return this.orderID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#getDateTimePlaced()
	 */
	public DateTime getDateTimePlaced() {
		return this.orderDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#getOrderItems()
	 */
	public Collection<OrderItemIntf> getOrderedItems() {
		return this.orderedItems;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#getDeliveryContact()
	 */
	public ContactIntf getDeliveryContact() {
		return this.deliveryContact;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#getBillingContact()
	 */
	public ContactIntf getBillingContact() {
		return this.billingContact;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#isBillingSameAsDelivery ()
	 */
	public boolean isBillingSameAsDelivery() {
		return billingSameAsDelivery;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#getOrderStatusString ()
	 */
	public String getOrderFulfillmentStatusString() {
		if (this.orderFulfillmentStatus <= OrderIntf.ORDER_FULFILLMENT_STATUS_STRING.length) {
			return OrderIntf.ORDER_FULFILLMENT_STATUS_STRING[this.orderFulfillmentStatus];
		}
		return "Unknown";
	}

	public String getOrderPaymentStatusString() {
		if (this.orderPaymentStatus <= OrderIntf.ORDER_PAYMENT_STATUS_STRING.length) {
			return OrderIntf.ORDER_PAYMENT_STATUS_STRING[this.orderPaymentStatus];
		}
		return "Unknown";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#getOrderStatus()
	 */
	public int getOrderFulfillmentStatus() {
		return this.orderFulfillmentStatus;
	}

	public String getAuthCode() {
		if (this.paymentMethod != null && this.paymentMethod instanceof CreditCardPaymentMethodIntf) {
			CreditCardPaymentMethodIntf ccp = (CreditCardPaymentMethodIntf) this.paymentMethod;
			return ccp.getAuthCode();
		}
		return "";
	}

	public PaymentMethodIntf getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setBillingContact(ContactIntf billingContact) {
		this.billingContact = billingContact;
	}

	public void setBillingSameAsDelivery(boolean billingSameAsDelivery) {
		this.billingSameAsDelivery = billingSameAsDelivery;
	}

	public void setDeliveryContact(ContactIntf deliveryContact) {
		this.deliveryContact = deliveryContact;
	}

	public void setOrderDate(DateTime orderDate) {
		this.orderDate = orderDate;
	}

	public void setOrderedItems(Collection<OrderItemIntf> orderedItems) {
		this.orderedItems = orderedItems;
	}

	public void setOrderFulfillmentStatus(int orderStatus) {
		this.orderFulfillmentStatus = orderStatus;
	}

	public void setPaymentMethod(PaymentMethodIntf paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public int getOrderPaymentStatus() {
		return this.orderPaymentStatus;
	}

	public void setOrderPaymentStatus(int orderPaymentStatus) {
		this.orderPaymentStatus = orderPaymentStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#sendConfirmationEmail ()
	 */
	public void sendSubscriptionConfirmationEmail() throws UsatException {

		try {
			boolean renewal = false;
			for (OrderItemIntf item : this.getOrderedItems()) {
				if (item instanceof RenewalOrderItemIntf) {
					renewal = true;
					break;
				}
			}

			if (!renewal) {
				this.sendNewSubscriptionConfirmationEmail();
			}
		} catch (UsatException ue) {
			throw ue;
		} catch (Exception e) {
			throw new UsatException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#sendConfirmationEmail ()
	 */
	protected void sendNewSubscriptionConfirmationEmail() throws UsatException {

		// String eEditionUtLink = UsaTodayConstants.EEDITION_UT_LINK;
		String exactTargetKey = null;
		String eEditionDesc = null;
		String eEditionID_Mail = UsaTodayConstants.EEDITION_NEWSTART_ID;
		String eEditionDesc_Mail = UsaTodayConstants.EEDITION_NEWSTART_DESC;
		String eEditionID_Carrier = UsaTodayConstants.EEDITION_NEWSTART_CARRIER_ID;
		String eEditionDesc_Carrier = UsaTodayConstants.EEDITION_NEWSTART_CARRIER_DESC;

		// String eEditionID_Gift = UsaTodayConstants.EEDITION_NEWSTART_GIFT_ID;
		// String eEditionDesc_Gift = UsaTodayConstants.EEDITION_NEWSTART_GIFT_DESC;
		String eEditionID_EE = UsaTodayConstants.EEDITION_EE_NEWSTART_ID;
		String eEditionDesc_EE = UsaTodayConstants.EEDITION_EE_NEWSTART_DESC;
		Boolean eEditionUTCompanion = UsaTodayConstants.UT_EE_COMPANION_ACTIVE;
		Boolean eEditionBWCompanion = UsaTodayConstants.BW_EE_COMPANION_ACTIVE;
		String eEditionUtLink = UsaTodayConstants.EEDITION_UT_LINK;
		String eEditionBwLink = UsaTodayConstants.EEDITION_BW_LINK;
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		String eEditionLink = null;
		// sub.getTotal()
		String ezpay = null;

		if (!this.confirmationEmailSent) {
			this.confirmationEmailSent = true;

			SmtpMailSender mail = new SmtpMailSender();

			mail.setMailServerHost(UsaTodayConstants.EMAIL_SERVER);
			mail.setSender(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS);

			Collection<OrderItemIntf> items = this.getOrderedItems();
			Iterator<OrderItemIntf> itr = items.iterator();
			SubscriptionOrderItemIntf sub = null;
			String exactTargetEmailPub = null;
			String deliveryMethod = null;

			while (itr.hasNext()) {
				OrderItemIntf item = itr.next();
				if (item instanceof SubscriptionOrderItemIntf) {
					sub = (SubscriptionOrderItemIntf) item;
				}
				if (item.getProduct().getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
					this.isSWSubscription = true;
					exactTargetEmailPub = "BW";
					eEditionLink = eEditionBwLink;
					exactTargetKey = eEditionID_Mail;
					eEditionDesc = eEditionDesc_Mail;
					if (sub.getDeliveryMethod().equals("M")) {
						deliveryMethod = "mail";

					} else {
						deliveryMethod = "carrier";
					}
					break;
				} else {
					if (item.getProduct().getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)
							&& item.getProduct().getProductCode().equalsIgnoreCase("UT")) {
						// this.isUTSubscription = true;
						exactTargetEmailPub = "UT";
						eEditionLink = eEditionUtLink;
						if (sub.getDeliveryMethod().equals("M")) {
							deliveryMethod = "mail";
							exactTargetKey = eEditionID_Mail;
							eEditionDesc = eEditionDesc_Mail;
						} else {
							deliveryMethod = "carrier";
							exactTargetKey = eEditionID_Carrier;
							eEditionDesc = eEditionDesc_Carrier;
						}
						break;
					} else {
						this.isEESubscription = true;
						exactTargetEmailPub = "EE";
						eEditionLink = eEditionUtLink;
						deliveryMethod = "mail";
						exactTargetKey = eEditionID_EE;
						eEditionDesc = eEditionDesc_EE;
						break;
					}
				}
			} // end while more items

			String messageSubject = null;
			if (!this.isSWSubscription) {
				messageSubject = "USA TODAY Subscription Notification";
			} else {
				messageSubject = "Sports Weekly Subscription Notification";
			}
			mail.setMessageSubject(messageSubject);
			String content = this.generateSubscriptionEmailBodyText(sub);
			mail.setMessageText(content);

			String recipientEmail = this.getDeliveryContact().getEmailAddress();

			// send confirmation email to billing contact if it exists.
			if (!this.isBillingSameAsDelivery() && this.billingContact != null && this.billingContact.getEmailAddress() != null
					&& !this.billingContact.getEmailAddress().equalsIgnoreCase(recipientEmail)) {
				recipientEmail = this.billingContact.getEmailAddress();
			}

			mail.addTORecipient(recipientEmail);

			String lastFour = null;
			if (this.getPaymentMethod() instanceof CreditCardPaymentMethodIntf) {
				int index = this.getPaymentMethod().getDisplayValue().length() - 4; // Length
																					// of
																					// astrisk
																					// fill
				lastFour = this.getPaymentMethod().getDisplayValue()
						.substring(index, this.getPaymentMethod().getDisplayValue().length());

			}

			// set ezpay
			if (sub.isChoosingEZPay()) {
				ezpay = "Yes";
			} else {
				ezpay = "No";
			}

			// set gannett units
			String guiZipcode = "false";
			GUIZipCodeManager guiMngr = GUIZipCodeManager.getInstance();
			if (guiMngr.isGUIZipForPub(sub.getProduct().getProductCode(), this.getDeliveryContact().getUIAddress().getZip())) {
				guiZipcode = "true";
			}

			PaymentTypeIntf ccType = this.getPaymentMethod().getPaymentType();

			String creditcard = ccType.getLabel();
			if (creditcard == null) {
				creditcard = "";
			}
			DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
			java.util.Date date = new java.util.Date();
			String dateStr = dateFormat.format(date);

			String startDate = UsatDateTimeFormatter.convertDateStringFromYYYYMMDDtoMMDDYYYY(sub.getStartDate());
			String subscripType = null;
			String confEmail = null;
			String firstName = null;
			String lastName = null;
			String company = null;
			String password = null;
			String phone = null;
			String gpFullName = null;
			String recipientFullName = null;
			String street = null;
			// String billSubscripType = null;
			String billConfEmail = null;
			String billFirstName = null;
			String billLastName = null;

			// String billCompany = null;
			// String billPassword = null;
			// String billPhone = null;
			// String billgpFullName = null;
			// String street = null;

			UIAddressIntf address = null;
			boolean suppressRecipientEmail = false;

			if (sub.isGiftItem()) {
				suppressRecipientEmail = true;
				try {
					if (!this.getDeliveryContact().getEmailAddress().equalsIgnoreCase(this.billingContact.getEmailAddress())) {
						DateTime startDT = sub.getStartDateObj();
						DateTime now = new DateTime();
						if (startDT.isBeforeNow() || (startDT.getDayOfYear() == now.getDayOfYear())) {
							suppressRecipientEmail = false;
						} // end if start date is not today
					} // end if emails are different
				} catch (Exception e) {
					; // ignore
				}

				subscripType = "Gift";
				billConfEmail = this.getBillingContact().getEmailAddress();
				billFirstName = this.getBillingContact().getFirstName();
				billLastName = this.getBillingContact().getLastName();
				String billfirstNL = billFirstName.toLowerCase();

				String billfirstNameU = billfirstNL.substring(0, 1).toUpperCase() + billfirstNL.substring(1);
				String billlastNL = billLastName.toLowerCase();
				String billlastNameU = billlastNL.substring(0, 1).toUpperCase() + billlastNL.substring(1);
				gpFullName = billfirstNameU + " " + billlastNameU;

				String recipFirstName = null;
				String recipLastName = null;
				recipFirstName = this.deliveryContact.getFirstName();
				recipLastName = this.deliveryContact.getLastName();
				String recipfirstNL = recipFirstName.toLowerCase();

				String recipfirstNameU = recipfirstNL.substring(0, 1).toUpperCase() + recipfirstNL.substring(1);
				String reciplastNL = recipLastName.toLowerCase();
				String reciplastNameU = reciplastNL.substring(0, 1).toUpperCase() + reciplastNL.substring(1);
				recipientFullName = recipfirstNameU + " " + reciplastNameU;

				// if (!this.isEESubscription) {
				//
				// exactTargetKey = eEditionID_Gift;
				// eEditionDesc = eEditionDesc_Gift;
				// }

				confEmail = this.getDeliveryContact().getEmailAddress();
				firstName = this.getDeliveryContact().getFirstName();
				lastName = this.getDeliveryContact().getLastName();
				company = this.getDeliveryContact().getFirmName();

				phone = this.getDeliveryContact().getHomePhone();

				address = this.getDeliveryContact().getUIAddress();

				street = this.getDeliveryContact().getUIAddress().getAddress1() + "   "
						+ this.getDeliveryContact().getUIAddress().getAptSuite();

			} else {
				subscripType = "New";
				confEmail = this.getDeliveryContact().getEmailAddress();
				firstName = this.getDeliveryContact().getFirstName();
				lastName = this.getDeliveryContact().getLastName();
				company = this.getDeliveryContact().getFirmName();

				phone = this.getDeliveryContact().getHomePhone();

				address = this.getDeliveryContact().getUIAddress();

				street = this.getDeliveryContact().getUIAddress().getAddress1() + "   "
						+ this.getDeliveryContact().getUIAddress().getAptSuite();

			}
			// if (address == null) {
			// throw new
			// UsatException("OrderBO:generateSubscriptionEmailBodyText() - Address object is null.");
			// }

			String address1 = "";

			boolean isUTBranding = !this.isSWSubscription;

			try {

				if ((isUTBranding && (eEditionUTCompanion == true)) || (this.isSWSubscription && (eEditionBWCompanion == true))
						|| (this.isEESubscription)) {

					try {

						// if gift subscription..check to see if we need to send gift confirmation email.
						if (subscripType.equals("Gift")) {

							if (confEmail.equals(billConfEmail)) {

								subscripType = "Recipient";

							} else {
								try {

									String billReturnCode = "";

									password = this.billingContact.getPassword();

									/*
									 * billReturnCode = ExactTargetNoDates.sendExactTargetNoDates( billConfEmail, eEditionLink,
									 * billFirstName, billLastName, formatter.format(sub.getProduct().getUnitPrice()), creditcard,
									 * address1, billingAddress.getAddress2(), billStreet, exactTargetKey, eEditionDesc,
									 * exactTargetEmailPub, billingAddress.getCity(), company,
									 * sub.getSelectedTerm().getDurationInWeeks(), billingAddress.getZip(), password,
									 * deliveryMethod, formatter.format(sub.getTaxAmount(address.getZip())), ezpay, dateStr,
									 * lastFour, startDate, formatter.format(sub.getSubTotal()), subscripType, phone,
									 * billingAddress.getState(), recipientFullName, guiZipcode, "", dateStr, "", "");
									 */
									// Per Jason, we never send billing address data to ET
									billReturnCode = ExactTargetNoDatesNew.sendExactTargetNoDates(billConfEmail, eEditionLink,
											billFirstName, billLastName, formatter.format(sub.getProduct().getUnitPrice()),
											creditcard, address1, address.getAddress2(), street, exactTargetKey, eEditionDesc,
											exactTargetEmailPub, address.getCity(), company, sub.getSelectedTerm().getDuration(),
											address.getZip(), password, deliveryMethod, formatter.format(this.getTaxAmount()),
											ezpay, dateStr, lastFour, startDate, formatter.format(this.getTotal()), subscripType,
											phone, address.getState(), recipientFullName, guiZipcode, "", dateStr, "", "",
											formatter.format(this.getSubTotal()));

									try {
										// update the billing email record to indicate confirmation sent
										for (EmailRecordBO em : this.savedEmailRecords) {
											if (em.getEmailAddress().equalsIgnoreCase(billConfEmail)) {
												em.setConfirmationEmailSent("Y");
												em.save();
											}
										}

									} catch (Exception e) {
										e.printStackTrace();
										System.out.println("Failed to set EmailSent column to 'Y' for user: " + billConfEmail);
									}
									subscripType = "Recipient";
									if (billReturnCode.equalsIgnoreCase("Error")) {
										try {
											// mail.sendMessage();

										} catch (Exception e) {
											// e-mail not sent

										}
									}
								} catch (Exception e) {
									// mail.sendMessage();

								}

							}

						}

						String returnCode = "";
						// if not a gift and not suppressing.
						if (!sub.isGiftItem() || !suppressRecipientEmail) {

							password = this.deliveryContact.getPassword();

							returnCode = ExactTargetNoDatesNew.sendExactTargetNoDates(confEmail, eEditionLink, firstName, lastName,
									formatter.format(sub.getProduct().getUnitPrice()), creditcard, address1, address.getAddress2(),
									street, exactTargetKey, eEditionDesc, exactTargetEmailPub, address.getCity(), company, sub
											.getSelectedTerm().getDuration(), address.getZip(), password, deliveryMethod, formatter
											.format(this.getTaxAmount()), ezpay, dateStr, lastFour, startDate, formatter
											.format(this.getTotal()), subscripType, phone, address.getState(), gpFullName,
									guiZipcode, "", dateStr, "", "", formatter.format(this.getSubTotal()));
							try {
								// update the billing email record to indicate confirmation sent
								for (EmailRecordBO em : this.savedEmailRecords) {
									if (em.getEmailAddress().equalsIgnoreCase(confEmail)) {
										em.setConfirmationEmailSent("Y");
										em.save();
									}
								}

							} catch (Exception e) {
								e.printStackTrace();
								System.out.println("Failed to set EmailSent column to 'Y' for user: " + confEmail);
							}
						}

						if (returnCode.equalsIgnoreCase("Error")) {
							try {
								if (!sub.isGiftItem() || !suppressRecipientEmail) {
									mail.sendMessage();
									// update the billing email record to indicate confirmation sent
									for (EmailRecordBO em : this.savedEmailRecords) {
										if (em.getEmailAddress().equalsIgnoreCase(confEmail)) {
											em.setConfirmationEmailSent("Y");
											em.save();
										}
									}
								}

							} catch (Exception e) {
								// e-mail not sent

							}
						}
					} catch (Exception e) {
						mail.sendMessage();

					}

				} else {
					mail.sendMessage();
				}

			} catch (Exception e) {
				System.out.println("OrderBO : Failed to send confirmation email: " + e.getMessage());
				System.out.println("To: " + this.getDeliveryContact().getEmailAddress());
				System.out.println("Contents: " + content);
				throw new UsatException(e);
			}

			// send developer copy in old format (not Exact Target generated)
			try {
				// mail.sendMessage();
				// send a copy to us.
				if (UsaTodayConstants.EMAIL_CONFIRMATIONS_COPYTO_ADDRESS != null
						&& UsaTodayConstants.EMAIL_CONFIRMATIONS_COPYTO_ADDRESS.trim().length() > 0) {
					mail.removeToRecipient(recipientEmail);
					mail.addTORecipient(UsaTodayConstants.EMAIL_CONFIRMATIONS_COPYTO_ADDRESS);
					mail.setMessageSubject(messageSubject + " Order #: " + sub.getOrderID());

					StringBuffer newContent = new StringBuffer("This is a copy of an online subscription confirmation email.\n \n");
					newContent.append("Following was sent to customer.\n======= BEGIN Customer Email ==================\n");
					newContent.append(content);
					newContent
							.append("\n \n==============  End Customer Email =================================\n \n##################################\n##  Additional Order information not sent to customer:   ##\n############################\n");
					newContent.append("\tConfirmation Email Address: ").append(recipientEmail).append("\n");
					newContent.append("\tKey Code: ").append(sub.getKeyCode()).append("\n");
					newContent.append("\tTerm: ").append(sub.getSelectedTerm().getDuration()).append("\n");
					newContent.append("\tPIA Rate Code: ").append(sub.getSelectedTerm().getPiaRateCode()).append("\n");
					if (this.isBillingSameAsDelivery()) {
						newContent.append("\tBilling Address Same as Delivery Address.\n");
					} else {
						newContent.append("\tBilling Name: ").append(this.getBillingContact().getFirstName()).append(" ")
								.append(this.getBillingContact().getLastName()).append("\n");
						newContent.append("\tBilling Firm Name: ").append(this.getBillingContact().getFirmName()).append("\n");
						newContent.append("\tBilling Address1: ").append(this.getBillingContact().getUIAddress().getAddress1())
								.append("\n");
						newContent.append("\tBilling Apt/Suite: ").append(this.getBillingContact().getUIAddress().getAptSuite())
								.append("\n");
						newContent.append("\tBilling Address2: ").append(this.getBillingContact().getUIAddress().getAddress2())
								.append("\n");
						newContent.append("\tBilling City: ").append(this.getBillingContact().getUIAddress().getCity())
								.append("\n");
						newContent.append("\tBilling State: ").append(this.getBillingContact().getUIAddress().getState())
								.append("\n");
						newContent.append("\tBilling Zip: ").append(this.getBillingContact().getUIAddress().getZip()).append("\n");
						newContent.append("\tBilling Home Phone: ").append(this.getBillingContact().getHomePhoneNumberFormatted())
								.append("\n");
					}

					mail.setMessageText(newContent.toString());
					// send the copy
					mail.sendMessage();
				}
			} catch (Exception e) {
				System.out.println("OrderBO : Failed to send confirmation email: " + e.getMessage());
				System.out.println("To: " + this.getDeliveryContact().getEmailAddress());
				System.out.println("Contents: " + content);
				throw new UsatException(e);
			}
		} // end if not already sent
	}

	/**
	 * This method is almost an exact copy of UTSubscription::getEmailText(). It should be completely rebuild such that it is not
	 * product dependent in order to achieve maximum flexibility.
	 * 
	 * @return
	 * @throws UsatException
	 */
	private String generateSubscriptionEmailBodyText(SubscriptionOrderItemIntf sub) throws UsatException {

		StringBuffer email = new StringBuffer("\n \n");

		int pubnum = 0;
		String phone = "1-800-872-0001.";
		String subURL = "www.usatodayservice.com";

		if (this.isSWSubscription) {
			phone = "1-800-872-1415.";
			subURL = "www.mysportsweekly.com";
			pubnum = 1;
		}

		boolean invoicePayment = true;
		String ccnum = null;
		// If no initial bill me transaction then get the credit card
		// information
		if (this.getPaymentMethod() instanceof CreditCardPaymentMethodIntf) {
			ccnum = this.getPaymentMethod().getDisplayValue();

			invoicePayment = false;
		}

		NumberFormat formatter = NumberFormat.getCurrencyInstance();

		UIAddressIntf address = null;

		if (this.getDeliveryContact().getPersistentAddress() != null) {
			address = this.getDeliveryContact().getPersistentAddress().convertToUIAddress();
		} else {
			address = this.getDeliveryContact().getUIAddress();
		}

		if (address == null) {
			throw new UsatException("OrderBO:generateSubscriptionEmailBodyText() - Address object is null.");
		}

		String address1 = address.getAddress1();
		if (address.getAptSuite() != null && address.getAptSuite().length() > 0) {
			address1 = address1 + " " + address.getAptSuite();
		}

		String address2 = address.getCity() + ", " + address.getState() + " " + address.getZip();

		String message1 = null;
		String message2 = null;
		String message3 = null;

		if (sub.isGiftItem()) {
			message1 = "Thank you for sending the gift of ";
			message2 = ".  Your gift subscription is scheduled to begin on ";
			message3 = "You have selected to give ";
		} else {
			message1 = "Thank you for subscribing to ";
			message2 = ".  Your subscription will begin on ";
			message3 = "You have selected to receive ";
		}

		email.append(message1);
		email.append(sub.getProduct().getName());
		email.append(message2);
		email.append(UsatDateTimeFormatter.convertDateStringFromYYYYMMDDtoMMDDYYYY(sub.getStartDate()));
		email.append(".\n \n");

		email.append(message3);

		// TODO : HOUMAN or SAM - add support for duration in months getDurationType() == "M"
		String duration = sub.getSelectedTerm().getDuration();
		if (duration.charAt(0) == '0') {
			duration = duration.substring(1);
		}
		email.append(duration);
		String durationType = sub.getSelectedTerm().getDurationType();
		if (durationType.equals("M")) {
			email.append(" months of ");			
		} else {
			email.append(" weeks of ");			
		}
		email.append(sub.getProduct().getName());
		email.append(" for ");
		email.append(formatter.format(sub.getProduct().getUnitPrice()));

		if (invoicePayment) {
			email.append(" (not including any applicable state or local tax)");
		}

		email.append(".\n");

		if (invoicePayment) {
			email.append("You will receive an invoice in the mail for your subscription order.");
		} else {
			// Remove the CC total charge for EZ-Pay customers
			if (!sub.isChoosingEZPay()) {
				if (this.getOrderPaymentStatus() == OrderIntf.PAYMENT_AUTH_PENDING) {
					email.append("Your credit card will be charged for ").append(formatter.format(sub.getProduct().getUnitPrice()))
							.append(" plus any applicable sales tax");
				} else {
					email.append("The total amount billed to your credit card, including taxes of ");
					email.append(formatter.format(this.getTaxAmount()));
	
					if (sub.getQuantity() > 1) {
						email.append(" for " + sub.getQuantity() + " copies");
					}
					email.append(" was ");
					email.append(formatter.format(this.getAmountCharged()));
				}
				email.append(". \n");
			}
			email.append("Credit card account number: ");
			email.append(ccnum).append(".");
		}

		if ((sub.isChoosingEZPay() && this.deliveryContact.getPersistentAddress() == null)
				|| (sub.getProduct().isElectronicDelivery() && sub.isChoosingEZPay())
				|| (sub.isChoosingEZPay() && !(this.deliveryContact.getPersistentAddress().isGUIAddress()))) {
			email.append("\n ");
			email.append("You have enrolled in EZ-PAY>>>. ");
			email.append("\n");
			email.append("At the end of your term, your credit card will be charged automatically to renew "
					+ "the subscription at the current published rate unless you notify us otherwise.");
		}

		email.append("\n \nDelivery Information is as follows: \n \n");

		email.append(this.getDeliveryContact().getFirstName());
		email.append(" ");
		email.append(this.getDeliveryContact().getLastName());
		email.append("\n");

		if ((this.getDeliveryContact().getFirmName() != null) && (!this.getDeliveryContact().getFirmName().trim().equals(""))) {
			email.append(this.getDeliveryContact().getFirmName());
			email.append("\n");
		}

		email.append(address1);
		email.append("\n");
		if ((address.getAddress2() != null) && (!address.getAddress2().trim().equals(""))) {
			email.append(address.getAddress2().trim());
			email.append("\n");
		}
		email.append(address2);

		email.append("\n");

		email.append("Home Phone Number: ");
		email.append(this.getDeliveryContact().getHomePhoneNumberFormatted());
		email.append("\n");

		if ((this.getDeliveryContact().getBusinessPhone() != null)
				&& (!this.getDeliveryContact().getBusinessPhone().trim().equals(""))) {
			email.append("Work Phone Number: ");
			email.append(this.getDeliveryContact().getWorkPhoneNumberFormatted());
			email.append("\n");
		}
		if (sub.getDeliveryMethod() != null) {
			if (sub.getProduct().isElectronicDelivery()) {
				email.append("You will receive your subscription via e-mail notifications.");
				email.append("\n");
			} else if (sub.getDeliveryMethod().equals("M")) {
				email.append("You will receive your subscription via mail delivery.");
				email.append("\n");
			} else if (sub.getDeliveryMethod().equals("C")) {
				email.append("You will receive your subscription via morning delivery.");
				email.append("\n");
			}
		}
		if (!invoicePayment) {
			email.append("\n \n");
			email.append("*Final credit card verification is pending.");
		}
		email.append("\n \n");
		// Insert text for GUI transactions, SW is never delivered by carrier so
		// never put GU text on SW order
		if (this.getDeliveryContact().getPersistentAddress() != null
				&& this.getDeliveryContact().getPersistentAddress().isGUIAddress() && !this.isSWSubscription) {
			email.append("In order to provide you with early morning delivery, the delivery of your newspaper");
			email.append("\n");
			email.append("will be handled by an independent agent. Due to this, you will not be able to interact");
			email.append("\n");
			email.append("with your account on our subscription website. If you have any questions about your subscription,");
			email.append("\n");
			email.append("please contact USA TODAY National Customer Service at " + phone);
		} else {
			email.append("If you have any questions about your subscription, please visit us online at");
			email.append("\n");
			email.append(subURL + " or call us at " + phone);

		}

		if (this.getBillingContact() != null && this.getBillingContact().getEmailAddress() != null
				&& this.getBillingContact().getEmailAddress().trim().length() > 0) {
			email.append("\n\nYour online account user id is: ").append(this.getBillingContact().getEmailAddress());
			email.append("\nYou online account password is: ").append(this.getBillingContact().getPassword());
		} else {
			email.append("\n\nYour online account user id is: ").append(this.getDeliveryContact().getEmailAddress());
			email.append("\nYou online account password is: ").append(this.getDeliveryContact().getPassword());
		}

		// PROMO TEXT
		/*
		 * if (this.isSWSubscription && (UsaTodayConstants.BW_EE_COMPANION_ACTIVE==true)) { if
		 * (UsaTodayConstants.EMAIL_BW_PROMO_TEXT != null) { // email.append("\n\n").append(UsaTodayConstants.EMAIL_BW_PROMO_TEXT);
		 * email.append("\n\n").append("Your Electronic Edition Userid: " + this.getDeliveryContact().getEmailAddress() +
		 * "  Password:  " + this.getDeliveryContact().getPassword()); } } else {
		 * 
		 * if (UsaTodayConstants.UT_EE_COMPANION_ACTIVE==true){ if (UsaTodayConstants.EMAIL_UT_PROMO_TEXT != null) { //
		 * email.append("\n\n").append(UsaTodayConstants.EMAIL_UT_PROMO_TEXT);
		 * email.append("\n\n").append("Your Electronic Edition Userid: " + this.getDeliveryContact().getEmailAddress() +
		 * "  Password:  " + this.getDeliveryContact().getPassword());
		 * 
		 * } } }
		 */

		if (UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT != null) {
			email.append("\n\n").append(UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT);
		}

		email.append("\n \n");
		if (pubnum == 0) {
			email.append("USA TODAY, 7950 Jones Branch Dr, McLean, Virginia, 22108");
		} else {
			email.append("USA TODAY Sports Weekly, 7950 Jones Branch Dr, McLean, Virginia, 22108");
		}

		return email.toString();
	}

	/**
	 * @return Returns the amountCharged.
	 */
	public double getAmountCharged() {
		return this.amountCharged;
	}

	/**
	 * @param amountCharged
	 *            The amountCharged to set.
	 */
	public void setAmountCharged(double amountCharged) {
		this.amountCharged = amountCharged;
	}

	/**
	 * @return Returns the taxAmount.
	 */
	public double getTaxAmount() {
		return this.taxAmount;
	}

	/**
	 * @param taxAmount
	 *            The taxAmount to set.
	 */
	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public double getSubTotal() {
		return this.subTotal;
	}

	public double getTotal() {

		return this.subTotal + this.taxAmount;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public void addSavedEmailRecord(EmailRecordBO rec) {
		if (rec != null) {
			this.savedEmailRecords.add(rec);
		}
	}

	public Collection<EmailRecordBO> getSavedEmailRecords() {
		return savedEmailRecords;
	}

}
