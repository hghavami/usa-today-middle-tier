package com.usatoday.businessObjects.shopping;

import java.math.BigDecimal;
import java.util.Random;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.shopping.OrderItemIntf;
import com.usatoday.businessObjects.util.TaxRateBO;
import com.usatoday.businessObjects.util.TaxRateManager;

public class OrderItemBO implements OrderItemIntf {

	// item ids just need to be unique within a cart
	// they are not persisted and don't have to be unique outside a single cart.

	private static Random itemIDs = new Random(System.currentTimeMillis());

	private int quantity = 1;
	private ProductIntf product = null;
	private boolean giftItem = false;
	private Long itemID = null;
	String deliveryZip = "";

	/**
     * 
     */
	public boolean equals(Object obj) {
		boolean isEqual = false;
		if (obj instanceof OrderItemIntf) {
			OrderItemIntf item = (OrderItemIntf) obj;
			if (item.getItemID().compareTo(this.itemID) == 0) {
				isEqual = true;
			}
		} else {
			isEqual = super.equals(obj);
		}
		return isEqual;
	}

	/**
	 * Default constructor
	 * 
	 */
	public OrderItemBO() {
		super();

		synchronized (OrderItemBO.itemIDs) {
			this.itemID = new Long(OrderItemBO.itemIDs.nextLong());
		}

	}

	/**
	 * @param The
	 *            product desired
	 */
	public void setProduct(ProductIntf product) {
		this.product = product;
	}

	/**
	 * @return the quantity of this product desired
	 */
	public int getQuantity() {
		return this.quantity;
	}

	/**
	 * @param the
	 *            quantity desired
	 */
	public void setQuantity(int qty) {
		if (qty > 0) {
			this.quantity = qty;
		}
	}

	/**
	 * @return The product desired in this line item
	 */
	public ProductIntf getProduct() {
		return this.product;
	}

	/**
	 * @return The subtotal for this order line item (quantity * product price)
	 */
	public double getSubTotal() throws UsatException {
		double subTotal = 0.0;

		if (this.product != null) {

			java.math.BigDecimal bd = new java.math.BigDecimal(this.getProduct().getUnitPrice());

			bd = bd.multiply(new BigDecimal(this.getQuantity()));

			bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);

			subTotal = bd.doubleValue();
		}

		return subTotal;
	}

	/**
     * 
     */
	public double getTaxAmount(String deliveryZip) throws UsatException {

		if (this.getProduct() == null) {
			throw new UsatException("You must specify a product first.");
		}

		// if product not taxable simply return 0
		if (!this.getProduct().isTaxable()) {
			return 0.0;
		}

		if (deliveryZip == null) {
			throw new UsatException("Taxes are based on the delivery zip. Please specify delivery zip first.");
		}

		this.deliveryZip = deliveryZip;

		double taxAmount = 0.0;

		TaxRateManager taxMngr = TaxRateManager.getInstance();
		TaxRateBO taxRates = taxMngr.getTaxRatesForZip(deliveryZip);

		if (taxRates != null) {
			taxAmount = taxRates.computeTax(this.getProduct().getTaxRateCode(), this.getSubTotal());
		}
		return taxAmount;
	}

	public Long getItemID() {
		return this.itemID;
	}

	public boolean isGiftItem() {
		return this.giftItem;
	}

	public void setGiftItem(boolean isGift) {
		this.giftItem = isGift;
	}

	public double getTaxRatePercentage(String deliveryZip) throws UsatException {
		if (deliveryZip == null) {
			throw new UsatException("Taxes are based on the delivery zip. Please specify delivery zip first.");
		}
		TaxRateManager taxMngr = TaxRateManager.getInstance();
		TaxRateBO taxRates = taxMngr.getTaxRatesForZip(deliveryZip);

		double taxRatePercentage = 0.0;

		if (taxRates != null) {
			taxRatePercentage = taxRates.getTaxRatePercentage(this.getProduct().getTaxRateCode());
		}

		return taxRatePercentage;
	}

	public double getTotal() throws UsatException {
		return (this.getSubTotal() + this.getTaxAmount(this.getDeliveryZip()));
	}

	public String getDeliveryZip() {
		return this.deliveryZip;
	}

	public void setDeliveryZip(String deliveryZip) {
		this.deliveryZip = deliveryZip;
	}
}
