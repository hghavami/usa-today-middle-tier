package com.usatoday.businessObjects.shopping.payment;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.shopping.payment.InvoicePaymentMethodIntf;
import com.usatoday.business.interfaces.shopping.payment.InvoicePaymentTypeIntf;
import com.usatoday.business.interfaces.shopping.payment.PaymentTypeIntf;

public class InvoicePaymentMethodBO implements InvoicePaymentMethodIntf {

	InvoicePaymentTypeIntf type = null;

	public InvoicePaymentMethodBO() {
		super();
		type = new InvoicePaymentTypeBO();
	}

	public String getAccountNumberForPersistence() {
		return "";
	}

	public String getDisplayValue() {
		return "";
	}

	public String getLabel() {
		return this.type.getLabel();
	}

	public PaymentTypeIntf getPaymentType() {
		return this.type;
	}

	public void setPaymentType(PaymentTypeIntf type) throws UsatException {

		if (type instanceof InvoicePaymentTypeIntf) {
			this.type = (InvoicePaymentTypeIntf) type;
		} else {
			throw new UsatException(
					"Invoice Payment Method can only accept a payment type of type InvoicePaymentIntf. You sent type: "
							+ type.getClass().getName());
		}

	}

}
