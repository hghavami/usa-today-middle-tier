/*
 * Created on Apr 27, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.shopping.payment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentTypentf;
import com.usatoday.integration.CreditCardPaymentTypeTO;
import com.usatoday.integration.PaymentTypeDAO;

/**
 * @author aeast
 * @date Apr 27, 2006
 * @class CreditCardPaymentTypeBO
 * 
 *        This class represents the Payment Type for Credit Card payments.
 * 
 */
public class CreditCardPaymentTypeBO extends PaymentTypeBO implements CreditCardPaymentTypentf {

	private static Collection<CreditCardPaymentTypentf> ccTypes = null;

	private int validLength1 = 0;
	private int validLength2 = 0;
	private char firstDigitChar = ' ';
	private String imagePath = "";

	public CreditCardPaymentTypeBO(String type, String label, String description, int validLength1, int validLength2,
			char firstDigitChar, String imagePath) {
		super(type, label, description);
		this.validLength1 = validLength1;
		this.validLength2 = validLength2;
		this.firstDigitChar = firstDigitChar;
		this.imagePath = imagePath;
	}

	public CreditCardPaymentTypeBO(CreditCardPaymentTypeTO cType) {
		super(cType.getType(), cType.getLabel(), cType.getDescription());
		this.validLength1 = cType.getValidLength1();
		this.validLength2 = cType.getValidLength2();
		this.firstDigitChar = cType.getFirstDigit();
		this.imagePath = cType.getImagePath();
	}

	/**
	 * 
	 * @return A collection of CreditCardPaymentTypes
	 * @throws UsatException
	 */
	public static synchronized final Collection<CreditCardPaymentTypentf> getCreditCardPaymentTypes() throws UsatException {

		if (CreditCardPaymentTypeBO.ccTypes == null) {
			try {
				Collection<CreditCardPaymentTypentf> creditCards = new ArrayList<CreditCardPaymentTypentf>();
				PaymentTypeDAO dao = new PaymentTypeDAO();

				Collection<CreditCardPaymentTypentf> ccToCollection = dao.getCreditCardPaymentMethodTypes();
				Iterator<CreditCardPaymentTypentf> itr = ccToCollection.iterator();
				while (itr.hasNext()) {
					CreditCardPaymentTypeTO to = (CreditCardPaymentTypeTO) itr.next();
					CreditCardPaymentTypeBO ccBO = new CreditCardPaymentTypeBO(to);
					creditCards.add(ccBO);
				}
				CreditCardPaymentTypeBO.ccTypes = creditCards;
			} catch (UsatException ue) {
				throw ue;
			} catch (Exception e) {
				throw new UsatException(e);
			}
		}
		return CreditCardPaymentTypeBO.ccTypes;
	}

	public static final Collection<CreditCardPaymentTypeBean> getCreditCardPaymentTypesBeans() throws UsatException {

		Collection<CreditCardPaymentTypentf> ccBO = CreditCardPaymentTypeBO.getCreditCardPaymentTypes();

		CreditCardPaymentTypeBean beanArray[] = new CreditCardPaymentTypeBean[ccBO.size()];

		ArrayList<CreditCardPaymentTypeBean> beans = new ArrayList<CreditCardPaymentTypeBean>();
		if (ccBO != null) {
			Iterator<CreditCardPaymentTypentf> itr = ccBO.iterator();
			while (itr.hasNext()) {
				CreditCardPaymentTypeBO bo = (CreditCardPaymentTypeBO) itr.next();
				CreditCardPaymentTypeBean bean = new CreditCardPaymentTypeBean();
				bean.setLabel(bo.getLabel());
				bean.setValue(bo.getType());
				bean.setImageFile(bo.getImagePath());
				if (bo.getType().equalsIgnoreCase("A")) {
					beanArray[0] = bean;
				} else if (bo.getType().equalsIgnoreCase("V")) {
					beanArray[1] = bean;
				} else if (bo.getType().equalsIgnoreCase("M")) {
					beanArray[2] = bean;
				} else if (bo.getType().equalsIgnoreCase("D")) {
					beanArray[3] = bean;
				} else if (bo.getType().equalsIgnoreCase("C")) {
					beanArray[4] = bean;
				} else {
					beans.add(bean);
				}
			} // end while
			for (int i = 0; i < beanArray.length; i++) {
				if (beanArray[i] != null) {
					beans.add(beanArray[i]);
				}
			}
		}
		return beans;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#requiresPaymentProcessing()
	 */
	public boolean requiresPaymentProcessing() {
		// credit cards require payment processing
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentTypentf#getValidLength1()
	 */
	public int getValidLength1() {
		return this.validLength1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentTypentf#getValidLength2()
	 */
	public int getValidLength2() {
		return this.validLength2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentTypentf#getFirstDigit()
	 */
	public char getFirstDigit() {
		return this.firstDigitChar;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentTypentf#getImagePath()
	 */
	public String getImagePath() {
		return this.imagePath;
	}

}
