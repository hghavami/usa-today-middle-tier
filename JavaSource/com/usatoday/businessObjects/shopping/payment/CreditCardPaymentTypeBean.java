/*
 * Created on May 17, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.shopping.payment;

import java.io.Serializable;

/**
 * @author aeast
 * @date May 17, 2006
 * @class CreditCardPaymentTypeBean
 * 
 *        Basic Java Bean for use on forms and such.
 * 
 */
public class CreditCardPaymentTypeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5928557999429504525L;
	private String value = null;
	private String label = null;
	private String imageFile = "/images/shim.gif";

	/**
     * 
     */
	public CreditCardPaymentTypeBean() {
		super();
	}

	public String getLabel() {
		return this.label;
	}

	public String getValue() {
		return this.value;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return Returns the imageFile.
	 */
	public String getImageFile() {
		return this.imageFile;
	}

	/**
	 * @param imageFile
	 *            The imageFile to set.
	 */
	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}
}
