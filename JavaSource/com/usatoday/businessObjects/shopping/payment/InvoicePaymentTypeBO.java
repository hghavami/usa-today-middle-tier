package com.usatoday.businessObjects.shopping.payment;

import com.usatoday.business.interfaces.shopping.payment.InvoicePaymentTypeIntf;

public class InvoicePaymentTypeBO extends PaymentTypeBO implements InvoicePaymentTypeIntf {

	public InvoicePaymentTypeBO() {
		super("B", "Bill Me", "You will receive a paper bill in the mail.");
	}

	public InvoicePaymentTypeBO(String type, String label, String description) {
		super("B", "Bill Me", "You will receive a paper bill in the mail.");
	}

	public boolean requiresPaymentProcessing() {
		return false;
	}

}
