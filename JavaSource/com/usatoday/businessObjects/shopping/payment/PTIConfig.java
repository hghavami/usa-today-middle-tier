/*
 * Created on Apr 24, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.shopping.payment;

import java.util.Properties;

import com.paytec.webapp.util.Configuration;
import com.usatoday.UsatException;
import com.usatoday.util.constants.USATApplicationPropertyFileLoader;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Apr 24, 2006
 * @class PTIConfig
 * 
 *        This class represents the in memory configuration of the PTI server.
 * 
 */
public class PTIConfig {

	// the defaults are for test PTI server
	private String serverURL = "https://qa.chaseloyalty.com/uat2/XmlApi/PortalServlet";
	private String issuerID = "";
	private String credentialValue = "";
	private String credentialUserName = "";
	private String externalIssuerID = "";
	private String externalMerchantID = "";
	private String terminalID = "";
	private String UTClientID = "";
	private String SWClientID = "";
	private String merchandiseClientID = "";
	private String debugMode = "false";
	private boolean failOnCVV = true;
	private boolean failOnAVS = true;
	private String merchantID = "";
	private long traceNumber = 0;

	public long getTraceNumber() {
		return traceNumber;
	}

	public void setTraceNumber(long traceNumber) {
		this.traceNumber = traceNumber;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	private static PTIConfig _instance = null;

	private PTIConfig() throws Exception {
		super();
		USATApplicationPropertyFileLoader loader = new USATApplicationPropertyFileLoader();
		Properties props = loader.loadPropertyFile(UsaTodayConstants.PAYMENT_PROCESSING_CONFIG_FILE);
		if (props != null) {
			this.initConfig(props);
		} else {
			throw new UsatException("Could not load PTI configuration file: " + UsaTodayConstants.PAYMENT_PROCESSING_CONFIG_FILE);
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public static synchronized PTIConfig getInstance() throws Exception {
		if (PTIConfig._instance == null) {
			PTIConfig._instance = new PTIConfig();
		}
		return PTIConfig._instance;
	}

	/**
	 * 
	 * @param props
	 */
	private void initConfig(Properties props) {

		String tempStr = props.getProperty("com.usatoday.payments.PTI_SERVER_URL");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.serverURL = tempStr;
		}
		tempStr = props.getProperty("com.usatoday.payments.PTI_Issuer_ID");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.issuerID = tempStr;
		}
		tempStr = props.getProperty("com.usatoday.payments.PTI_Credential_Value");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.credentialValue = tempStr;
		}
		tempStr = props.getProperty("com.usatoday.payments.PTI_Credential_User_Name");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.credentialUserName = tempStr;
		}
		tempStr = props.getProperty("com.usatoday.payments.PTI_External_Issuer_ID");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.externalIssuerID = tempStr;
		}
		tempStr = props.getProperty("com.usatoday.payments.PTI_External_Merchant_ID");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.externalMerchantID = tempStr;
		}
		tempStr = props.getProperty("com.usatoday.payments.PTI_Terminal_ID");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.terminalID = tempStr;
		}
		tempStr = props.getProperty("com.usatoday.payments.UTClientID");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.UTClientID = tempStr;
		}
		tempStr = props.getProperty("com.usatoday.payments.SWClientID");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.SWClientID = tempStr;
		}
		tempStr = props.getProperty("com.usatoday.payments.MerchandiseClientID");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.merchandiseClientID = tempStr;
		}
		tempStr = props.getProperty("com.usatoday.payments.debug");
		if (tempStr != null && tempStr.trim().equalsIgnoreCase("true")) {
			this.debugMode = "true";
			Configuration.setProperty("debugMode", "true");
		}
		tempStr = props.getProperty("com.usatoday.payments.failifCVVNotMatched");
		if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
			this.failOnCVV = false;
		} else {
			this.failOnCVV = true;
		}
		tempStr = props.getProperty("com.usatoday.payments.failifAVSNotMatched");
		if (tempStr != null && tempStr.trim().equalsIgnoreCase("false")) {
			this.failOnAVS = false;
		} else {
			this.failOnAVS = true;
		}
		tempStr = props.getProperty("com.usatoday.paymentech.merchant_ID");
		if (tempStr != null) {
			tempStr = tempStr.trim();
			this.merchantID = tempStr;
		}
	}

	public static void resetConfig() throws Exception {
		PTIConfig._instance = null;
		PTIConfig._instance = new PTIConfig();
	}

	public String getCredentialUserName() {
		return this.credentialUserName;
	}

	public String getCredentialValue() {
		return this.credentialValue;
	}

	public String getExternalIssuerID() {
		return this.externalIssuerID;
	}

	public String getExternalMerchantID() {
		return this.externalMerchantID;
	}

	public String getIssuerID() {
		return this.issuerID;
	}

	public String getMerchandiseClientID() {
		return this.merchandiseClientID;
	}

	public String getServerURL() {
		return this.serverURL;
	}

	public String getSWClientID() {
		return this.SWClientID;
	}

	public String getTerminalID() {
		return this.terminalID;
	}

	public String getUTClientID() {
		return this.UTClientID;
	}

	public String getDebugMode() {
		return this.debugMode;
	}

	public boolean isFailOnCVV() {
		return this.failOnCVV;
	}

	public boolean isFailOnAVS() {
		return failOnAVS;
	}

	public void setFailOnAVS(boolean failOnAVS) {
		this.failOnAVS = failOnAVS;
	}
}
