package com.usatoday.businessObjects.shopping.payment;

/*
 * Created on Aug 8, 2005
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */

import java.util.Date;

import com.paytec.core.util.GuidFactory;
import com.paytec.webapp.Credentials;
import com.paytec.webapp.business.Address;
import com.paytec.webapp.business.CreditCard;
import com.paytec.webapp.business.PurchaseTransaction;
import com.paytec.webapp.manager.PurchaseManager;
import com.paytec.webapp.responses.PurchaseResponse;
import com.paytec.webapp.util.Configuration;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.shopping.payment.PaymentTypeIntf;
import com.usatoday.businessObjects.util.mail.EmailAlert;
import com.usatoday.integration.CCLogTO;
import com.usatoday.integration.CreditCardLogDAO;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Aug 8, 2005
 * @class PTICreditCardProcessor
 * 
 *        Wrapper class for dealing with PTI.
 * 
 */
public class PTIOldCreditCardProcessor {

	private Address address = null;
	private String cardHolderName = null;
	private String billingFirstName = null;
	private String billingLastName = null;
	private String cardNumber = null;
	private String cvcNumber = null;
	private String expMonth = null;
	private String expYear = null;
	private PurchaseResponse lastResponse = null;
	private PurchaseResponse authResponse = null;
	private String transactionTraceNumber = null;
	private String emailAddress = null;
	private String PTIClientID = null;
	private double chargeAmount = 0.0;
	private String orderID = null;
	private String clientIPAddress = "";
	private String externalKey = null;
	private String serviceTransactionNumber = null;
	private String requestType = "";
	private boolean networkFailure = false;
	private boolean authVoided = false;

	private String authFailureMessage = null;

	private PaymentTypeIntf paymentType = null;

	/**
	 * @return Returns the emailAddress.
	 */
	public String getEmailAddress() {
		return this.emailAddress;
	}

	/**
	 * @param emailAddress
	 *            The emailAddress to set.
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return Returns the pTIClientID.
	 */
	public String getPTIClientID() {
		return this.PTIClientID;
	}

	/**
	 * @param clientID
	 *            The pTIClientID to set.
	 */
	public void setPTIClientID(String clientID) {
		this.PTIClientID = clientID;
	}

	/**
	 * @return Returns the response.
	 */
	public PurchaseResponse getResponse() {
		return this.lastResponse;
	}

	/**
	 * @param response
	 *            The response to set.
	 */
	protected void setResponse(PurchaseResponse response) {
		this.lastResponse = response;
	}

	/**
	 * @return Returns the transactionTraceNumber.
	 */
	public String getTransactionTraceNumber() {
		return this.transactionTraceNumber;
	}

	/**
	 * @param transactionTraceNumber
	 *            The transactionTraceNumber to set.
	 */
	protected void setTransactionTraceNumber(String transactionTraceNumber) {
		this.transactionTraceNumber = transactionTraceNumber;
	}

	/**
	 * @return Returns the expMonth.
	 */
	public String getExpMonth() {
		return this.expMonth;
	}

	/**
	 * @param expMonth
	 *            The expMonth to set.
	 */
	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	/**
	 * @return Returns the expYear.
	 */
	public String getExpYear() {
		return this.expYear;
	}

	/**
	 * @param expYear
	 *            The expYear to set.
	 */
	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	/**
	 * @return Returns the cardHolderName.
	 */
	public String getCardHolderName() {
		return this.cardHolderName;
	}

	/**
	 * @param cardHolderName
	 *            The cardHolderName to set.
	 */
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	/**
	 * Eventually need to rewrite this with using a property file and the PayTech Configuration class
	 * 
	 * @return
	 */
	private Credentials generateCredentials() throws Exception {
		// this information will be provided by PTI after setup in Valexia
		// Should come from initialization parameters

		Credentials theCreds = new Credentials(false);

		PTIConfig config = PTIConfig.getInstance();

		// Configuration.initialize(UTCommon.DEFAULT_PTI_PATH);
		Configuration.setProperty("debugMode", config.getDebugMode());
		theCreds.setExternalIssuerId(config.getExternalIssuerID());
		theCreds.setExternalMerchantId(config.getExternalMerchantID());
		theCreds.setCredentialValue(config.getCredentialValue());
		theCreds.setCredentialType("userpassword");
		theCreds.setServleturl(config.getServerURL());
		theCreds.setCredentialUserName(config.getCredentialUserName());
		// important, must be the service in Valexia you are accessing. In this case "CardService".
		theCreds.setService("CardService");

		return theCreds;
	}

	/**
	 * 
	 * @return
	 */
	private CreditCard generateCreditCard() {

		// construct the credit card used for the transaction
		CreditCard cc = new CreditCard();

		cc.setBillingAddress(this.getAddress());

		cc.setCardHolderName(this.getCardHolderName());

		cc.setCardNumber(this.getCardNumber());

		// cvc/cvv2 number if provided. Not required
		if (this.getCvcNumber() != null) {
			cc.setCardVerificationNumber(this.getCvcNumber());
		}

		cc.setExpirationMonth(this.getExpMonth());
		cc.setExpirationYear(this.getExpYear());

		return cc;
	}

	/**
	 * Invoke the credit Card transaction to do an authorization.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	public void processCCAuthorizationTransaction() throws Exception {
		// set your external key for this transaction, recored on the Valexia transaction
		this.externalKey = GuidFactory.getInstance().getSomewhatUniqueNumber().toString();

		this.processCCTransaction("Auth");

		this.authResponse = lastResponse;

		// if failed for network reason attempt to void, otherwise just set error message
		if (!this.authResponse.isSuccessful()) {
			if (this.authFailureMessage == null) {
				this.authFailureMessage = new String();
			}

			int responseCode = Integer.parseInt(this.authResponse.getResponseCode());

			switch (responseCode) {
			case 1: // declined
				this.authFailureMessage += "The credit card was declined. Please check your card information or use another card.";
				break;
			case 2: // Rejected

				if (this.authResponse.getResponseMessage().equalsIgnoreCase("EXPIRED CARD")) {
					authFailureMessage += "The credit card was declined due to an expired card.";
				} else if (this.authResponse.getResponseMessage().equalsIgnoreCase("INV ACCT NUM")
						|| this.authResponse.getResponseMessage().equalsIgnoreCase("Invalid card number")) {
					authFailureMessage += "The credit card was declined due to an invalid account number.";
				} else {
					authFailureMessage += "The credit card was not accepted. " + this.authResponse.getResponseMessage();
				}

				break;
			case 3: // Referred/ Hold Call Ctr
				authFailureMessage += "The card entered is not valid. Please check the card information and try again.";
				break;
			case 9999: // immediately void transaction
				try {
					authFailureMessage = " 9999 response code " + this.authResponse.getResponseMessage();
					this.processCCVoidTransaction();
				} catch (Exception exp) {
					System.out.println("Failed to void the transaction on the 9999 Failure with Tran ID: "
							+ this.getTransactionTraceNumber() + " : " + exp.getMessage() + "    Order ID: " + orderID);
					authFailureMessage += "\nFailed to void the 9999 transaction Exception: " + exp.getMessage();
				}
				// break; // flow into the default processing on 9999s

			default: // everything else is data or communication error
				authFailureMessage += "Credit Card Authorization Service Temporarily Unavailable";
				this.networkFailure = true;
				StringBuffer bodyText = new StringBuffer();
				// Check for proxy errors and time outs and if so issue a void
				if (responseCode >= 500 && responseCode < 600) {
					boolean voided = false;
					int I = 0;
					// Try to void max 5 times
					while (!voided && I < 5) {
						try {
							I++;
							this.processCCVoidTransaction();
							if (this.lastResponse.isSuccessful()) {
								voided = true;
								bodyText.append("   ######   CREDIT CARD VOIDED SUCCESSFULLY   ####");
								bodyText.append("Credit Card Transaction Voided after a PTI failed to return response");
								System.out.println("Credit Card Transaction Voided after a PTI failed to return response");
								authFailureMessage += ("We were unable to verify your credit card. Your credit card has not been charged.");
							} else {
								try {
									bodyText.append("   ######   FAILED TO VOID CREDIT CARD - After a PTI failed to return response. PTI Response: "
											+ this.getResponse()
											+ "  PTI Code: "
											+ this.getResponse().getResponseCode()
											+ "  PTI Message: " + this.getResponse().getResponseMessage() + "    ####");
									System.out.println("Attempt " + I
											+ " Failed to Void Card after a PTI failed to return response - PTI Response: "
											+ this.getResponse().getResponseCode() + " message: "
											+ this.getResponse().getResponseMessage());
								} catch (Exception eee) {
									System.out.println("Cannot read void response: " + eee.getMessage());
								}
							}
						} catch (Exception exp) {
							System.out.println("FAILED TO Void after network failure: " + bodyText.toString());
						}
					}
					if (!voided) {
						// send alert
						EmailAlert alert = new EmailAlert();
						alert.setSender("CheckOutService@usatoday.com");
						alert.setSubject("Subscription Order Entry Alert - Possible PTI issue");
						bodyText.append(I
								+ " Attempts Failed to Void a Transaction After PTI Failed to Return Response. Manual 'Credit' may be Required. \n"
								+ " \nPTI Response Code: '" + this.getResponse().getResponseCode() + "' Message: "
								+ this.getResponse().getResponseMessage());
						bodyText.append("\n\n Order ID: ").append(orderID);
						alert.setBodyText(bodyText.toString());
						alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
						alert.sendAlert();
					}
				} else {
					try {
						EmailAlert alert = new EmailAlert();
						alert.setSender("CheckOutService@usatoday.com");
						alert.setSubject("Subscription Order Entry Alert - Possible PTI issue");
						bodyText.append("Failed to Process Credit Card: \n" + " \nUnable to parse PTI Response Code: '"
								+ this.getResponse().getResponseCode() + "' Message: " + this.getResponse().getResponseMessage());
						bodyText.append("\n\n Order ID: ").append(orderID);
						if (responseCode == 9999) {
							bodyText.append("\n\nAttempted to void transaction immediately..likely only one CCLOG though.");

						}
						alert.setBodyText(bodyText.toString());
						alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
						alert.sendAlert();
					} catch (Exception e) {
						System.out.println("Failed to send alert: " + bodyText.toString());
					}
					break;
				}
			} // end switch
		} // end if failed

	}

	/**
	 * Invoke the credit Card transaction to do a void.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	public void processCCVoidTransaction() throws Exception {
		if (this.externalKey == null) {
			throw new UsatException("No Previous Authorization to Void.");
		}

		if (this.authVoided) {
			// trying to void something that's already been voided/ignore
			return;
		}
		this.processCCTransaction("Void");

		if (this.lastResponse.isSuccessful()) {
			this.authVoided = true;
		}
	}

	/**
	 * Invoke the credit Card transaction to do a void.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	public void processCCCreditTransaction() throws Exception {
		// if(this.processCCTransaction("Credit")) {
		// return true;
		// }
		throw new UsatException("Credit Not Implemented.");
	}

	/**
	 * Invoke the credit Card transaction to do a settlement.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	public void processCCSettlementTransaction() throws Exception {
		// I think this needs to change because I think settlements cause
		// all pending authorizations to settle. May need to be done differently.
		// See example/doc ... Waiting for response from PTI.
		// if (this.processCCTransaction("Settle")) {
		// return true;
		// }
		throw new UsatException("Settlement Not Implemented.");
	}

	/**
	 * Invoke the credit Card transaction.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	private void processCCTransaction(String requestType) throws Exception {

		PurchaseTransaction trans = new PurchaseTransaction();

		this.requestType = requestType;

		try {
			PTIConfig config = PTIConfig.getInstance();

			trans.setEmailAddress(this.getEmailAddress());
			// set the debit amount

			trans.setAmount(this.getChargeAmount());

			// set the currency code, USD is only supported code at this time.
			trans.setAmountCurrencyCode("USD");
			// set the credit card that will be used for the debit
			trans.setCreditCard(this.generateCreditCard());
			// set the owning issuer id provided by Valexia
			trans.setIssuerId(config.getIssuerID());
			// set if a recurring payment
			trans.setRecurringPurchase(false);
			// set the request type, must be one of the following: Auth/Credit/Settle/Void/
			trans.setRequestType(requestType);
			// set the transaction date
			trans.setTransactionDate(new Date());
			// Set source name to API to distinguish our transactions from VPOS (Virtual Point of Sale) types
			trans.setTransactionSourceName("API");

			trans.setExternalKey(this.externalKey);

			// Set transaction trace number to some unique number
			if (this.getTransactionTraceNumber() != null) {
				trans.setTransactionTraceNumber(this.getTransactionTraceNumber());
			}

			// following should be set if voiding
			if (this.serviceTransactionNumber != null) {
				trans.setServiceTransactionNumber(new Long(this.serviceTransactionNumber));
			}
			// set the transaction order id
			if (this.getOrderID() != null) {
				trans.setOrderId(this.getOrderID());
			} else {
				trans.setOrderId("--" + trans.getTransactionTraceNumber());
			}
			// set the merchant of sale id provided by Valexia based on pub code
			trans.setClientId(this.getPTIClientID());
			// set the Terminal ID for Diners' Club transactions
			trans.setTerminalId(config.getTerminalID());
			// set CardHolderPresent - transaction initiated via the web
			trans.setCardHolderPresent("2");
			// set CardPresent - card NOT present
			trans.setCardPresent("0");
			// set the payment method to credit card
			trans.setPaymentMethod("CC");

			// send the transaction
			this.lastResponse = PurchaseManager.accountlessPurchase(trans, this.generateCredentials(), "Pass through purchase");

			this.serviceTransactionNumber = this.lastResponse.getTransactionTraceNumber();

			if (lastResponse.isSuccessful()) {
				this.transactionTraceNumber = this.lastResponse.getTransactionTraceNumber();
			} else {
				System.out.println("PTI error message: " + this.lastResponse.getResponseMessage());
				System.out.println("PTI error code: " + this.lastResponse.getResponseCode());
			}

			this.persistCCLog();
		} catch (Exception e) {
			// Handle exception TBD
			System.out.println("PTICreditCardProcessor() - Exception hit while processing PTI trans: " + e.getMessage());
			throw e;
		}

	}

	/**
	 * 
	 *
	 */
	private void persistCCLog() {
		CCLogTO log = null;
		try {
			log = this.generateCCLogRecord();

			CreditCardLogDAO dao = new CreditCardLogDAO();
			dao.insert(log);
		} catch (Exception e) {
			if (log != null) {
				System.out.println("Failed to save CCLOG: " + log.toString());
				System.out.println("CCLog Save Exception: " + e.getMessage());
			} else {
				System.out.println("Failed to generate CCLOG: " + e.getMessage());
			}

		}
	}

	/**
	 * @return Returns the address.
	 */
	public Address getAddress() {
		return this.address;
	}

	/**
	 * @param address
	 *            The address to set.
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return Returns the cardNumber.
	 */
	public String getCardNumber() {
		return this.cardNumber;
	}

	/**
	 * @param cardNumber
	 *            The cardNumber to set.
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return Returns the cvcNumber.
	 */
	public String getCvcNumber() {
		return this.cvcNumber;
	}

	/**
	 * @param cvcNumber
	 *            The cvcNumber to set.
	 */
	public void setCvcNumber(String cvcNumber) {
		this.cvcNumber = cvcNumber;
	}

	/**
	 * @return Returns the chargeAmount.
	 */
	public double getChargeAmount() {
		return this.chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            The chargeAmount to set.
	 */
	public void setChargeAmount(double chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return Returns the orderID.
	 */
	public String getOrderID() {
		return this.orderID;
	}

	/**
	 * @param orderID
	 *            The orderID to set.
	 */
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	/**
	 * 
	 * @return
	 */
	public CCLogTO generateCCLogRecord() {
		if (this.lastResponse != null) {
			CCLogTO cLog = new CCLogTO();
			try {
				try {
					cLog.setAuthCode(lastResponse.getAuthorizationCode());
				} catch (Exception e) {
					cLog.setAuthCode("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					String num = this.getCardNumber();
					num = num.substring(num.length() - 4);
					cLog.setCCNum(num);
				} catch (Exception e) {
					cLog.setCCNum("Err");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() setCCNum - " + e.getMessage());
				}
				try {
					String ccType = "";
					ccType = (this.paymentType != null) ? this.paymentType.getType() : "?";
					if (ccType != null && ccType.trim().length() == 1) {
						cLog.setCcType(ccType.trim());
					} else {
						cLog.setCcType("?");
					}
				} catch (Exception e) {
					cLog.setCcType("?");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - ccType " + e.getMessage());
				}
				try {
					cLog.setEmail(this.getEmailAddress());
				} catch (Exception e) {
					cLog.setEmail("Err");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - Email " + e.getMessage());
				}
				try {
					cLog.setExpMonth(this.getExpMonth());
				} catch (Exception e) {
					cLog.setExpMonth("");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - expMonth " + e.getMessage());
				}
				try {
					cLog.setExpYear(this.getExpYear());
				} catch (Exception e) {
					cLog.setExpYear("");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - exp year " + e.getMessage());
				}
				try {
					cLog.setFirstName(this.getBillingFirstName());
				} catch (Exception e) {
					cLog.setFirstName("");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setLastName(this.getBillingLastName());
				} catch (Exception e) {
					cLog.setLastName("");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				// try {
				// cLog.setPubCode(); set in calling method
				// }
				// catch (Exception e) {
				// cLog.setPubCode("");
				// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				// }
				try {
					cLog.setOrderID(this.orderID);
				} catch (Exception e) {
					cLog.setOrderID("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}

				try {
					cLog.setClientIPAddress(this.getClientIPAddress());
				} catch (Exception e) {
					cLog.setClientIPAddress("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}

				try {
					cLog.setResponseCode(lastResponse.getResponseCode());
				} catch (Exception e) {
					cLog.setResponseCode("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}

				try {
					cLog.setResponseMessage(lastResponse.getResponseMessage());
				} catch (Exception e) {
					cLog.setResponseMessage("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}

				try {
					cLog.setCvvResponseCode(lastResponse.getCvv2ResponseCode());
				} catch (Exception e) {
					cLog.setCvvResponseCode("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}

				try {
					cLog.setCvvResponseMessage(lastResponse.getCvv2Response());
				} catch (Exception e) {
					cLog.setCvvResponseMessage("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}

				try {
					String zip = this.getAddress().getPostalCode();
					if (zip != null && zip.length() > 5) {
						zip = zip.replaceAll(" ", "");
						zip = zip.substring(0, 5);
					} else if (zip.trim().length() == 0) {
						zip = "! US"; // international
					}
					cLog.setZip(zip);
				} catch (Exception e) {
					cLog.setZip("");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setVendorTranID(this.lastResponse.getTransactionTraceNumber());
				} catch (Exception e) {
					cLog.setVendorTranID("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setAvsRespCode(lastResponse.getAvsResponseCode());
				} catch (Exception e) {
					cLog.setAvsRespCode("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setAvsRespMessage(lastResponse.getAvsResponse());
				} catch (Exception e) {
					cLog.setAvsRespMessage("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setAmount(String.valueOf(this.getChargeAmount()));
				} catch (Exception e) {
					cLog.setAmount("Err");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setExternalKey(this.externalKey);
				} catch (Exception e) {
					cLog.setExternalKey("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setRequestType(this.requestType);
				} catch (Exception e) {
					cLog.setRequestType("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setReasonCode(lastResponse.getMessageReasonCode());
				} catch (Exception e) {
					cLog.setReasonCode("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
			} catch (Exception e) {
				System.out.println("PTICreditCardProcessor::generateCCLogRecord() - Didn't expect to get here. Excetion Class: "
						+ e.getClass().getName() + "  Message: " + e.getMessage());
			}
			return cLog;
		} else {
			System.out.println("Attempt to get log record without runnig a trasanction");
			return null;
		}
	}

	/**
	 * @return Returns the billingFirstName.
	 */
	public String getBillingFirstName() {
		return this.billingFirstName;
	}

	/**
	 * @param billingFirstName
	 *            The billingFirstName to set.
	 */
	public void setBillingFirstName(String billingFirstName) {
		this.billingFirstName = billingFirstName;
	}

	/**
	 * @return Returns the billingLastName.
	 */
	public String getBillingLastName() {
		return this.billingLastName;
	}

	/**
	 * @param billingLastName
	 *            The billingLastName to set.
	 */
	public void setBillingLastName(String billingLastName) {
		this.billingLastName = billingLastName;
	}

	public PaymentTypeIntf getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(PaymentTypeIntf paymentType) {
		this.paymentType = paymentType;
	}

	public String getClientIPAddress() {
		return this.clientIPAddress;
	}

	public void setClientIPAddress(String clientIPAddress) {
		this.clientIPAddress = clientIPAddress;
	}

	/**
	 * 
	 * @return true if card voided otherwise false
	 * @throws Exception
	 */
	public boolean voidLastAuthorizationIfCVVFailed() throws Exception {
		boolean voided = false;
		try {
			int responseCode = Integer.parseInt(this.authResponse.getResponseCode());
			// authorized but may have failed for CVV
			if (responseCode == 0) {
				String cvvResponse = this.authResponse.getCvv2ResponseCode();
				if (cvvResponse != null) {
					cvvResponse = cvvResponse.trim();
					if (cvvResponse.length() == 0) {
						responseCode = 2;
					} else {
						try {
							responseCode = Integer.parseInt(cvvResponse);
						} catch (Exception e) {
							throw new UsatException("Unexpected CVV Response from Payment Processor: " + responseCode);
						}
					}
				} else {
					// force it to not processed if we got a null back for some reason.
					responseCode = 2;
				}

				// 1 = no match
				if (responseCode == 1) {
					this.processCCVoidTransaction();

					if (!this.lastResponse.isSuccessful()) {
						StringBuffer bodyText = new StringBuffer();
						try {
							EmailAlert alert = new EmailAlert();
							alert.setSender("CheckOutService@usatoday.com");
							alert.setSubject("Subscription Order Entry Alert - Manual Void or Credit may be required.");
							bodyText.append("Failed to Void Credit After CVV Failure Card CCLOG Data (billing contact info): \n"
									+ " \n" + this.generateCCLogRecord().toString());
							bodyText.append("\n\n Order ID: ").append(orderID);
							alert.setBodyText(bodyText.toString());
							alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
							alert.sendAlert();
						} catch (Exception e) {
							System.out.println("Failed to send alert: " + bodyText.toString());
						}
					} else {
						voided = true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return voided;
	}

	/**
	 * 
	 * @return true if card voided otherwise false
	 * @throws Exception
	 */
	public boolean voidLastAuthorizationIfAVSFailed() throws Exception {
		boolean voided = false;
		try {
			int responseCode = Integer.parseInt(this.authResponse.getResponseCode());
			// authorized but may have failed for CVV
			if (responseCode == 0) {

				String responseCodeString = this.authResponse.getAvsResponseCode();

				if (responseCodeString != null) {
					responseCodeString = responseCodeString.trim();

					if (responseCodeString.length() == 0) {
						// force not to fail
						responseCode = 0;
					} else {
						try {
							responseCode = Integer.parseInt(responseCodeString);
						} catch (Exception e) {
							throw new UsatException("Unexpected AVS Response from Payment Processor: " + responseCode);
						}
					}
				} else {
					// force not to fail if response was null
					responseCode = 0;
				}

				// 5/6 are no match on billing address
				if (responseCode > 4 && responseCode < 7) {
					this.processCCVoidTransaction();

					if (!this.lastResponse.isSuccessful()) {
						StringBuffer bodyText = new StringBuffer();
						try {
							EmailAlert alert = new EmailAlert();
							alert.setSender("CheckOutService@usatoday.com");
							alert.setSubject("Subscription Order Entry Alert - Manual Void or Credit may be required.");
							bodyText.append("Failed to Void Credit Card After AVS Failure, CCLOG Data (billing contact info): \n"
									+ " \n" + this.generateCCLogRecord().toString());
							bodyText.append("\n\n Order ID: ").append(orderID);
							alert.setBodyText(bodyText.toString());
							alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
							alert.sendAlert();
						} catch (Exception e) {
							System.out.println("Failed to send alert: " + bodyText.toString());
						}

					} else {
						voided = true;
					}

				} // end if we need to void
			} // end if card was authed successfully
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return voided;
	}

	public PurchaseResponse getAuthResponse() {
		return authResponse;
	}

	public void setAuthResponse(PurchaseResponse authResponse) {
		this.authResponse = authResponse;
	}

	public String getAuthFailureMessage() {
		return authFailureMessage;
	}

	public boolean isNetworkFailure() {
		return this.networkFailure;
	}
}
