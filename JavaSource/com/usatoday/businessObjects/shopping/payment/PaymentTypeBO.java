/*
 * Created on Apr 27, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.shopping.payment;

import com.usatoday.business.interfaces.shopping.payment.PaymentTypeIntf;

/**
 * @author aeast
 * @date Apr 27, 2006
 * @class PaymentTypeBO
 * 
 *        This class represents an abstract payment type. It must be subclassed into a more specific payment type.
 * 
 */
public abstract class PaymentTypeBO implements PaymentTypeIntf {
	private String type = "";
	private String label = "";
	private String description = "";

	public PaymentTypeBO(String type, String label, String description) {
		super();
		this.type = type;
		this.label = label;
		this.description = description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#getType()
	 */
	public String getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#getLabel()
	 */
	public String getLabel() {
		return label;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#getDescription()
	 */
	public String getDescription() {
		return description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentTypeIntf#requiresPaymentProcessing()
	 */
	public abstract boolean requiresPaymentProcessing();
}
