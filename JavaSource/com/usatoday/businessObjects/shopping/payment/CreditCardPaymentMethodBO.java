/*
 * Created on Apr 28, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.shopping.payment;

import java.util.Collection;
import java.util.Iterator;

import com.paytec.webapp.util.CreditCardUtility;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentMethodIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentTypentf;
import com.usatoday.business.interfaces.shopping.payment.PaymentTypeIntf;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;
import com.usatoday.businessObjects.util.crypto.Conversion;
import com.usatoday.businessObjects.util.crypto.USATCreditCardEncryptor;
import com.usatoday.businessObjects.util.mail.EmailAlert;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Apr 28, 2006
 * @class CreditCardPaymentMethodBO
 * 
 * 
 */
public class CreditCardPaymentMethodBO implements CreditCardPaymentMethodIntf {
	private String cardNumber = "";
	private String encryptedValue = null;
	private String expMonth = "";
	private String expYear = "";
	private String cvv = "";
	private String nameOnCard = "";
	private CreditCardPaymentTypentf type = null;
	private String authCode = "";
	private String authDate = "";
	private boolean authorizedSuccessfully = false;

	/**
     * 
     */
	public CreditCardPaymentMethodBO() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentMethodIntf#getAccountNumberForPersistence()
	 */
	public String getAccountNumberForPersistence() {

		if (this.encryptedValue != null) {
			return this.encryptedValue;
		}

		this.encryptedValue = this.getCardNumber();

		if (UsaTodayConstants.CRYPTO_WEAK_ENCRYPT_CARDS) {
			// apply weak encryption
			this.encryptedValue = Conversion.convertdata(this.getExpirationDateForPersistence(), this.getCardNumber());
		}

		if (UsaTodayConstants.CRYPTO_STRONG_ENCRYPT_CARDS) {
			USATCreditCardEncryptor ce = new USATCreditCardEncryptor();
			try {
				this.encryptedValue = ce.encryptAndEncodeString(this.encryptedValue);
				if (this.encryptedValue.length() > 200) {
					throw new UsatException("Credit Card Encrypted Version exceeds 200 characters");
				}
			} catch (UsatException ue) {
				// if strong fails...revert back to weak
				this.encryptedValue = Conversion.convertdata(this.getExpirationDateForPersistence(), this.getCardNumber());
				String errStr = "FAILED TO Strong Encrypt CC. Used weak encryption only. Last Name: " + this.getNameOnCard()
						+ " Card #: " + this.getDisplayValue() + "    EXCEPTION: " + ue.getMessage();
				System.out.println(errStr);
				if (UsaTodayConstants.EMAIL_ALERTS_ENABLED) {
					// send alert
					EmailAlert eAlert = new EmailAlert();
					eAlert.setSender(UsaTodayConstants.EMAIL_ALERT_SENDER);
					eAlert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
					eAlert.setSubject("Extranet CC Encryption Alert - PROBLEM WITH ENCRYPTION");
					eAlert.setBodyText(errStr);
					eAlert.sendAlert();
				}
			}
		}

		return this.encryptedValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#setCardNumber(java.lang.String)
	 */
	public void setCardNumber(String number) throws UsatException {
		if (number == null) {
			this.cardNumber = "";
		} else {
			this.cardNumber = number.trim();
			this.setPaymentType(this.getCreditCardTypeCode());

		}
		// clear any previous encryption setting
		this.encryptedValue = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#setCardNumber(java.lang.String)
	 */
	public void setCurrCardNumber(String number, String type) throws UsatException {
		if (number == null) {
			this.cardNumber = "";
		} else {
			this.cardNumber = number.trim();
			this.setPaymentType(type);

		}
		// clear any previous encryption setting
		this.encryptedValue = "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#getCardNumber()
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#setCVV(java.lang.String)
	 */
	public void setCVV(String cvv) {
		if (cvv == null) {
			this.cvv = "";
		} else {
			this.cvv = cvv.trim();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#getCVV()
	 */
	public String getCVV() {
		return this.cvv;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#setExpirationMonth(java.lang.String)
	 */
	public void setExpirationMonth(String month) {
		if (month == null) {
			this.expMonth = "";
		} else {
			this.expMonth = month.trim();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#getExpirationMonth()
	 */
	public String getExpirationMonth() {
		return this.expMonth;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#setExpirationYear()
	 */
	public void setExpirationYear(String year) {
		if (year == null) {
			this.expYear = "";
		} else {
			this.expYear = year.trim();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#getExpirationYear()
	 */
	public String getExpirationYear() {
		return this.expYear;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#setNameOnCard(java.lang.String)
	 */
	public void setNameOnCard(String name) {
		if (name == null) {
			this.nameOnCard = "";
		} else {
			this.nameOnCard = name.trim();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.CreditCardPaymentMethodIntf#getNameOnCard()
	 */
	public String getNameOnCard() {
		return this.nameOnCard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentMethodIntf#getPaymentType()
	 */
	public PaymentTypeIntf getPaymentType() {
		return this.type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentMethodIntf#getLabel()
	 */
	public String getLabel() {
		return this.type.getLabel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentMethodIntf#getDisplayValue()
	 */
	public String getDisplayValue() {
		StringBuffer displayValue = new StringBuffer();
		if (this.cardNumber != null && this.cardNumber.length() > 0) {
			String digits = "";
			if (this.type != null) {
				int length = 0;
				if (this.cardNumber.length() == this.type.getValidLength1()) {
					length = this.type.getValidLength1();
				} else {
					length = this.type.getValidLength2();
				}
				for (int i = 0; i < length - 4; i++) {
					displayValue.append("*");
				}
			} else {
				// use default
				displayValue.append("************");
			}
			if (this.cardNumber.length() > 4) {
				digits = this.cardNumber.substring(this.cardNumber.length() - 4);
			}
			displayValue.append(digits);
		}

		return displayValue.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.PaymentMethodIntf#setPaymentType(com.usatoday.business.interfaces.shopping.
	 * PaymentTypeIntf)
	 */
	public void setPaymentType(PaymentTypeIntf type) throws UsatException {
		if (type instanceof CreditCardPaymentTypentf) {
			this.type = (CreditCardPaymentTypentf) type;
		} else {
			throw new UsatException(
					"Credit Card Payment Method can only accept a payment type of type CreditCardPaymentIntf. You sent type: "
							+ type.getClass().getName());
		}
	}

	/**
	 * 
	 * @param type
	 * @throws UsatException
	 */
	public void setPaymentType(String type) throws UsatException {
		if (type == null) {
			return;
		}
		Collection<CreditCardPaymentTypentf> paymentTypes = CreditCardPaymentTypeBO.getCreditCardPaymentTypes();
		Iterator<CreditCardPaymentTypentf> itr = paymentTypes.iterator();
		while (itr.hasNext()) {
			CreditCardPaymentTypentf cc = itr.next();
			if (cc.getType().equalsIgnoreCase(type)) {
				this.setPaymentType(cc);
				break;
			}
		}
	}

	public String getAuthCode() {
		return this.authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
		this.setAuthDate();
	}

	public String getExpirationDateForPersistence() {
		return this.getExpirationMonth() + this.getExpirationYear().substring(2);
	}

	public String getAuthDate() {
		return this.authDate;
	}

	/**
	 * Sets auth date to current date in yyyyMMDD format
	 * 
	 */
	private void setAuthDate() {

		this.authDate = UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat();
	}

	/**
	 * @return boolean TRUE if the card was authorized via PTI otherwise false
	 */
	public boolean wasAuthorizedSuccessfully() {
		return this.authorizedSuccessfully;
	}

	/**
	 * 
	 * @param authorized
	 *            True if the card was authorized via PTI
	 */
	public void setAuthorizedSuccessfully(boolean authorized) {
		this.authorizedSuccessfully = authorized;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		try {

			sb.append("Credit Card payment Method: NAME:'");
			sb.append(this.nameOnCard);
			sb.append("' Last 4:'");
			sb.append(this.getDisplayValue());
			sb.append("' Auth:'");
			sb.append(this.authCode);
			sb.append("'  Was Authed: ").append(this.authorizedSuccessfully);
		} catch (Exception e) {
			return super.toString();
		}

		return sb.toString();
	}

	@Override
	public String getCreditCardTypeCode() throws UsatException {
		if (this.cardNumber == null || this.cardNumber.length() == 0) {
			throw new UsatException("Can't determine card type without credit card number.");
		}

		int ccType = CreditCardUtility.getCardType(this.cardNumber);
		String ctype = null;

		switch (ccType) {
		case CreditCardUtility.AMERICAN_EXPRESS:
			ctype = "A";
			break;
		case CreditCardUtility.VISA:
			ctype = "V";
			break;
		case CreditCardUtility.MASTERCARD:
			ctype = "M";
			break;
		case CreditCardUtility.DINERS_CLUB:
			ctype = "C";
			break;
		case CreditCardUtility.DISCOVER:
			ctype = "D";
			break;
		default:
			if (this.cardNumber.substring(0, 1).equals(String.valueOf(CreditCardUtility.DINERS_CLUB))) {
				ctype = "D";
			} else {
				throw new UsatException("Unable to determine type of card.");				
			}
		} // end switch

		return ctype;
	}

}
