package com.usatoday.businessObjects.shopping.payment;

/*
 * Created on Aug 8, 2005
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */

import java.util.Random;

import com.gannett.usat.iconapi.client.IconAPIContext;
import com.paymentech.orbital.sdk.interfaces.RequestIF;
import com.paymentech.orbital.sdk.interfaces.ResponseIF;
import com.paymentech.orbital.sdk.interfaces.TransactionProcessorIF;
import com.paymentech.orbital.sdk.request.FieldNotFoundException;
import com.paymentech.orbital.sdk.request.Request;
import com.paymentech.orbital.sdk.transactionProcessor.TransactionException;
import com.paymentech.orbital.sdk.transactionProcessor.TransactionProcessor;
import com.paymentech.orbital.sdk.util.exceptions.InitializationException;
import com.paytec.core.util.GuidFactory;
import com.paytec.webapp.business.Address;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.shopping.payment.PaymentTypeIntf;
import com.usatoday.businessObjects.util.mail.EmailAlert;
import com.usatoday.integration.CCLogTO;
import com.usatoday.integration.CreditCardLogDAO;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Aug 8, 2005
 * @class PTICreditCardProcessor
 * 
 *        Wrapper class for dealing with PTI.
 * 
 */
public class PTICreditCardProcessor {

	private Address address = null;
	private String cardHolderName = null;
	private String billingFirstName = null;
	private String billingLastName = null;
	private String cardNumber = null;
	private String cvcNumber = null;
	private String expMonth = null;
	private String expYear = null;
	// private PurchaseResponse lastResponse = null;
	// private PurchaseResponse authResponse = null;
	private ResponseIF lastResponse = null;
	private ResponseIF authResponse = null;
	private String transactionTraceNumber = null;
	private String emailAddress = null;
	private String PTIClientID = null;
	private double chargeAmount = 0.0;
	private String orderID = null;
	private String clientIPAddress = "";
	private String externalKey = null;
	private String serviceTransactionNumber = null;
	private String requestType = "";
	private boolean networkFailure = false;
	private boolean authVoided = false;

	private String authFailureMessage = null;

	private PaymentTypeIntf paymentType = null;
	private int traceNumber = 0;

	/**
	 * @return Returns the emailAddress.
	 */
	public String getEmailAddress() {
		return this.emailAddress;
	}

	/**
	 * @param emailAddress
	 *            The emailAddress to set.
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return Returns the pTIClientID.
	 */
	public String getPTIClientID() {
		return this.PTIClientID;
	}

	/**
	 * @param clientID
	 *            The pTIClientID to set.
	 */
	public void setPTIClientID(String clientID) {
		this.PTIClientID = clientID;
	}

	/**
	 * @return Returns the response.
	 */
	public ResponseIF getResponse() {
		return this.lastResponse;
	}

	/**
	 * @param response
	 *            The response to set.
	 */
	protected void setResponse(ResponseIF response) {
		this.lastResponse = response;
	}

	/**
	 * @return Returns the transactionTraceNumber.
	 */
	public String getTransactionTraceNumber() {
		return this.transactionTraceNumber;
	}

	/**
	 * @param transactionTraceNumber
	 *            The transactionTraceNumber to set.
	 */
	protected void setTransactionTraceNumber(String transactionTraceNumber) {
		this.transactionTraceNumber = transactionTraceNumber;
	}

	/**
	 * @return Returns the expMonth.
	 */
	public String getExpMonth() {
		return this.expMonth;
	}

	/**
	 * @param expMonth
	 *            The expMonth to set.
	 */
	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	/**
	 * @return Returns the expYear.
	 */
	public String getExpYear() {
		return this.expYear;
	}

	/**
	 * @param expYear
	 *            The expYear to set.
	 */
	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	/**
	 * @return Returns the cardHolderName.
	 */
	public String getCardHolderName() {
		return this.cardHolderName;
	}

	/**
	 * @param cardHolderName
	 *            The cardHolderName to set.
	 */
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	/**
	 * Invoke the credit Card transaction to do a void.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	public void processCCCreditTransaction() throws Exception {
		// if(this.processCCTransaction("Credit")) {
		// return true;
		// }
		throw new UsatException("Credit Not Implemented.");
	}

	/**
	 * Invoke the credit Card transaction to do a settlement.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	public void processCCSettlementTransaction() throws Exception {
		// I think this needs to change because I think settlements cause
		// all pending authorizations to settle. May need to be done
		// differently.
		// See example/doc ... Waiting for response from PTI.
		// if (this.processCCTransaction("Settle")) {
		// return true;
		// }
		throw new UsatException("Settlement Not Implemented.");
	}

	/**
	 * 
	 *
	 */
	private void persistCCLog() {
		CCLogTO log = null;
		try {
			log = this.generateCCLogRecord();

			CreditCardLogDAO dao = new CreditCardLogDAO();
			dao.insert(log);
		} catch (Exception e) {
			if (log != null) {
				System.out.println("Failed to save CCLOG: " + log.toString());
				System.out.println("CCLog Save Exception: " + e.getMessage());
			} else {
				System.out.println("Failed to generate CCLOG: " + e.getMessage());
			}

		}
	}

	/**
	 * @return Returns the address.
	 */
	public Address getAddress() {
		return this.address;
	}

	/**
	 * @param address
	 *            The address to set.
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return Returns the cardNumber.
	 */
	public String getCardNumber() {
		return this.cardNumber;
	}

	/**
	 * @param cardNumber
	 *            The cardNumber to set.
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return Returns the cvcNumber.
	 */
	public String getCvcNumber() {
		return this.cvcNumber;
	}

	/**
	 * @param cvcNumber
	 *            The cvcNumber to set.
	 */
	public void setCvcNumber(String cvcNumber) {
		this.cvcNumber = cvcNumber;
	}

	/**
	 * @return Returns the chargeAmount.
	 */
	public double getChargeAmount() {
		return this.chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            The chargeAmount to set.
	 */
	public void setChargeAmount(double chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return Returns the orderID.
	 */
	public String getOrderID() {
		return this.orderID;
	}

	/**
	 * @param orderID
	 *            The orderID to set.
	 */
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	/**
	 * 
	 * @return
	 */
	public CCLogTO generateCCLogRecord() {
		if (this.lastResponse != null) {
			CCLogTO cLog = new CCLogTO();
			try {
				try {
					cLog.setAuthCode(lastResponse.getAuthCode());
				} catch (Exception e) {
					cLog.setAuthCode("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}
				try {
					String num = this.getCardNumber();
					num = num.substring(num.length() - 4);
					cLog.setCCNum(num);
				} catch (Exception e) {
					cLog.setCCNum("Err");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() setCCNum - " + e.getMessage());
				}
				try {
					String ccType = "";
					ccType = (this.paymentType != null) ? this.paymentType.getType() : "?";
					if (ccType != null && ccType.trim().length() == 1) {
						cLog.setCcType(ccType.trim());
					} else {
						cLog.setCcType("?");
					}
				} catch (Exception e) {
					cLog.setCcType("?");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - ccType " + e.getMessage());
				}
				try {
					cLog.setEmail(this.getEmailAddress());
				} catch (Exception e) {
					cLog.setEmail("Err");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - Email " + e.getMessage());
				}
				try {
					cLog.setExpMonth(this.getExpMonth());
				} catch (Exception e) {
					cLog.setExpMonth("");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - expMonth " + e.getMessage());
				}
				try {
					cLog.setExpYear(this.getExpYear());
				} catch (Exception e) {
					cLog.setExpYear("");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - exp year " + e.getMessage());
				}
				try {
					cLog.setFirstName(this.getBillingFirstName());
				} catch (Exception e) {
					cLog.setFirstName("");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setLastName(this.getBillingLastName());
				} catch (Exception e) {
					cLog.setLastName("");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				// try {
				// cLog.setPubCode(); set in calling method
				// }
				// catch (Exception e) {
				// cLog.setPubCode("");
				// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
				// + e.getMessage());
				// }
				try {
					cLog.setOrderID(this.orderID);
				} catch (Exception e) {
					cLog.setOrderID("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}

				try {
					cLog.setClientIPAddress(this.getClientIPAddress());
				} catch (Exception e) {
					cLog.setClientIPAddress("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}

				try {
					cLog.setResponseCode(lastResponse.getResponseCode());
				} catch (Exception e) {
					cLog.setResponseCode("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}

				try {
					cLog.setResponseMessage(lastResponse.getMessage());
				} catch (Exception e) {
					cLog.setResponseMessage("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}

				try {
					cLog.setCvvResponseCode(lastResponse.getCVV2RespCode());
				} catch (Exception e) {
					cLog.setCvvResponseCode("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}

				try {
					cLog.setCvvResponseMessage(lastResponse.getCVV2RespCode());
				} catch (Exception e) {
					cLog.setCvvResponseMessage("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}

				try {
					String zip = this.getAddress().getPostalCode();
					if (zip != null && zip.length() > 5) {
						zip = zip.replaceAll(" ", "");
						zip = zip.substring(0, 5);
					} else if (zip.trim().length() == 0) {
						zip = "! US"; // international
					}
					cLog.setZip(zip);
				} catch (Exception e) {
					cLog.setZip("");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setVendorTranID(this.lastResponse.getTxRefNum());
				} catch (Exception e) {
					cLog.setVendorTranID("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}
				try {
					cLog.setAvsRespCode(lastResponse.getCVV2RespCode());
				} catch (Exception e) {
					cLog.setAvsRespCode("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}
				try {
					cLog.setAvsRespMessage(lastResponse.getMessage());
				} catch (Exception e) {
					cLog.setAvsRespMessage("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}
				try {
					cLog.setAmount(String.valueOf(this.getChargeAmount()));
				} catch (Exception e) {
					cLog.setAmount("Err");
					System.out.println("PTICreditCardProcessor::generateCCLogRecord() - " + e.getMessage());
				}
				try {
					cLog.setExternalKey(this.externalKey);
				} catch (Exception e) {
					cLog.setExternalKey("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}
				try {
					cLog.setRequestType(this.requestType);
				} catch (Exception e) {
					cLog.setRequestType("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}
				try {
					cLog.setReasonCode(lastResponse.getAVSResponseCode());
				} catch (Exception e) {
					cLog.setReasonCode("");
					// System.out.println("CreditCardProcessor::generateCCLogRecord() - "
					// + e.getMessage());
				}
			} catch (Exception e) {
				System.out.println("PTICreditCardProcessor::generateCCLogRecord() - Didn't expect to get here. Excetion Class: "
						+ e.getClass().getName() + "  Message: " + e.getMessage());
			}
			return cLog;
		} else {
			System.out.println("Attempt to get log record without runnig a trasanction");
			return null;
		}
	}

	/**
	 * @return Returns the billingFirstName.
	 */
	public String getBillingFirstName() {
		return this.billingFirstName;
	}

	/**
	 * @param billingFirstName
	 *            The billingFirstName to set.
	 */
	public void setBillingFirstName(String billingFirstName) {
		this.billingFirstName = billingFirstName;
	}

	/**
	 * @return Returns the billingLastName.
	 */
	public String getBillingLastName() {
		return this.billingLastName;
	}

	/**
	 * @param billingLastName
	 *            The billingLastName to set.
	 */
	public void setBillingLastName(String billingLastName) {
		this.billingLastName = billingLastName;
	}

	public PaymentTypeIntf getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(PaymentTypeIntf paymentType) {
		this.paymentType = paymentType;
	}

	public String getClientIPAddress() {
		return this.clientIPAddress;
	}

	public void setClientIPAddress(String clientIPAddress) {
		this.clientIPAddress = clientIPAddress;
	}

	/**
	 * 
	 * @return true if card voided otherwise false
	 * @throws Exception
	 */
	public boolean voidLastAuthorizationIfCVVFailed() throws Exception {
		// CVV failure now results in decline of the transaction
		return false;

/*		boolean voided = false;
		try {
			int responseCode = Integer.parseInt(this.authResponse.getResponseCode());
			// authorized but may have failed for CVV
			if (responseCode == 0) {
				String cvvResponse = this.authResponse.getCVV2RespCode();
				if (cvvResponse != null) {
					cvvResponse = cvvResponse.trim();
					if (cvvResponse.length() == 0) {
						responseCode = 2;
					} else {
						try {
							responseCode = Integer.parseInt(cvvResponse);
						} catch (Exception e) {
							throw new UsatException("Unexpected CVV Response from Payment Processor: " + responseCode);
						}
					}
				} else {
					// force it to not processed if we got a null back for some
					// reason.
					responseCode = 2;
				}

				// 1 = no match
				if (responseCode == 1) {
					this.processCCVoidTransaction();

					if (!this.lastResponse.isApproved()) {
						StringBuffer bodyText = new StringBuffer();
						try {
							EmailAlert alert = new EmailAlert();
							alert.setSender("CheckOutService@usatoday.com");
							alert.setSubject("Subscription Order Entry Alert - Manual Void or Credit may be required.");
							bodyText.append("Failed to Void Credit After CVV Failure Card CCLOG Data (billing contact info): \n"
									+ " \n" + this.generateCCLogRecord().toString());
							bodyText.append("\n\n Order ID: ").append(orderID);
							alert.setBodyText(bodyText.toString());
							alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
							alert.sendAlert();
						} catch (Exception e) {
							System.out.println("Failed to send alert: " + bodyText.toString());
						}
					} else {
						voided = true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return voided;
*/	}

	/**
	 * 
	 * @return true if card voided otherwise false
	 * @throws Exception
	 */
	public boolean voidLastAuthorizationIfAVSFailed() throws Exception {
		// AVS failure now results in decline of the transaction
		return false;
		
/*		boolean voided = false;
		try {
			int responseCode = Integer.parseInt(this.authResponse.getResponseCode());
			// authorized but may have failed for CVV
			if (responseCode == 0) {

				String responseCodeString = this.authResponse.getAVSResponseCode();

				if (responseCodeString != null) {
					responseCodeString = responseCodeString.trim();

					if (responseCodeString.length() == 0) {
						// force not to fail
						responseCode = 0;
					} else {
						try {
							responseCode = Integer.parseInt(responseCodeString);
						} catch (Exception e) {
							throw new UsatException("Unexpected AVS Response from Payment Processor: " + responseCode);
						}
					}
				} else {
					// force not to fail if response was null
					responseCode = 0;
				}

				// 5/6 are no match on billing address
				if (responseCode > 4 && responseCode < 7) {
					this.processCCVoidTransaction();

					if (!this.lastResponse.isApproved()) {
						StringBuffer bodyText = new StringBuffer();
						try {
							EmailAlert alert = new EmailAlert();
							alert.setSender("CheckOutService@usatoday.com");
							alert.setSubject("Subscription Order Entry Alert - Manual Void or Credit may be required.");
							bodyText.append("Failed to Void Credit Card After AVS Failure, CCLOG Data (billing contact info): \n"
									+ " \n" + this.generateCCLogRecord().toString());
							bodyText.append("\n\n Order ID: ").append(orderID);
							alert.setBodyText(bodyText.toString());
							alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
							alert.sendAlert();
						} catch (Exception e) {
							System.out.println("Failed to send alert: " + bodyText.toString());
						}

					} else {
						voided = true;
					}

				} // end if we need to void
			} // end if card was authed successfully
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return voided;
*/	}

	public ResponseIF getAuthResponse() {
		return authResponse;
	}

	public void setAuthResponse(ResponseIF authResponse) {
		this.authResponse = authResponse;
	}

	public String getAuthFailureMessage() {
		return authFailureMessage;
	}

	public boolean isNetworkFailure() {
		return this.networkFailure;
	}

	/**
	 * Invoke the credit Card transaction.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	private void processOrbitalCCAuthTransaction() throws Exception {

		// Get values from pti.properties files
		PTIConfig config = PTIConfig.getInstance();
		// Create a Transaction Processor
		// The Transaction Processor acquires and releases resources and
		// executes transactions.
		// It configures a pool of protocol engines, then uses the pool to
		// execute transactions.
		TransactionProcessorIF tp = null;
		try {
			tp = new TransactionProcessor();
		} catch (InitializationException iex) {
			System.err.println("TransactionProcessor failed to initialize");
			System.err.println(iex.getMessage());
			iex.printStackTrace();
		}
		// Create a request object
		// The request object uses the XML templates along with data we provide
		// to generate a String representation of the xml request
		RequestIF request = null;
		try {
			// Tell the request object which template to use (see
			// RequestIF.java)
			request = new Request(RequestIF.NEW_ORDER_TRANSACTION);

			// Assign the "Trace Number", used for automatic retries
			// Definition and format of the Trace Number is defined by the
			// Merchant
			// Trace Numbers must be unique per transaction, otherwise they can
			// be any String
			request.setTraceNumber(String.valueOf(getTraceNumber()));
			// Basic Auth Fields
			request.setFieldValue("IndustryType", "EC");
			request.setFieldValue("MessageType", "A");
			request.setFieldValue("MerchantID", config.getMerchantID());
			request.setFieldValue("BIN", "000001");
			if (this.orderID == null || this.orderID.trim().equals("")) {
				request.setFieldValue("OrderID", "Account_Maintenance");
			} else {
				request.setFieldValue("OrderID", this.orderID);				
			}
			request.setFieldValue("AccountNum", this.getCardNumber());
//			request.setFieldValue("Amount", String.valueOf(this.getChargeAmount()));
			// 0 Dollars for Visa or MC and .01 for AMEX and Disc
			if (this.getPaymentType().getType().equals("V") || this.getPaymentType().getType().equals("M")) {
				request.setFieldValue("Amount", "0");				
			} else {
				request.setFieldValue("Amount", "01");
			}
			request.setFieldValue("Exp", this.getExpMonth().trim() + this.getExpYear().trim().substring(2));
			// AVS Information
			request.setFieldValue("AVSname", this.getCardHolderName());
			request.setFieldValue("AVSaddress1", this.getAddress().getAddressLine1());
			request.setFieldValue("AVScity", this.getAddress().getCity());
			request.setFieldValue("AVSstate", this.getAddress().getStateAbbrev());
			request.setFieldValue("AVSzip", this.getAddress().getPostalCode());
			// Additional Information
			request.setFieldValue("Comments", "Authorize CC");
			request.setFieldValue("CardSecVal", this.getCvcNumber());
			if (IconAPIContext.debugMode) {
				// Display the request
				System.out.println("\nAuth Request:\n" + request.getXML());
			}
		} catch (InitializationException ie) {
			System.err.println("Credit card Auth unable to initialize request object");
			System.err.println(ie.getMessage());
			ie.printStackTrace();
		} catch (FieldNotFoundException fnfe) {
			System.err.println("Credit card Auth unable to find XML field in template");
			System.err.println(fnfe.getMessage());
			fnfe.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// Process the transaction
		// Pass in the request object (created above), and receive a response
		// object.
		// If the resources required by the Transaction Processor have been
		// exhausted,
		// this code will block until the resources become available.
		// The "TransactionProcessor.poolSize" configuration property specifies
		// how many resources
		// will be available. The TransactionProcessor acts as a governor, only
		// allowing
		// up to "poolSize" transactions outstanding at any point in time.
		// As transactions are completed, their resources are placed back in the
		// pool.
		try {
			this.lastResponse = tp.process(request);
		} catch (TransactionException tex) {
			System.err.println("Credit card Auth transaction failed, including retries and failover");
			System.err.println(tex.getMessage());
			tex.printStackTrace();
		}
		// Set the trace number for voiding
		if (this.lastResponse.isApproved()) {
			this.transactionTraceNumber = this.lastResponse.getTxRefNum();
		}
		this.serviceTransactionNumber = this.lastResponse.getTxRefNum();
		this.persistCCLog();
		// Display the this.lastResponse
		// This line displays the entire xml this.lastResponse on the java
		// system
		// console.
		if (IconAPIContext.debugMode) {
			System.out.println("\nResponse:\n" + this.lastResponse.toXmlString() + "\n");
			System.out.println("Response Attributes:");
			System.out.println("isGood=" + this.lastResponse.isGood());
			System.out.println("isError=" + this.lastResponse.isError());
			System.out.println("isQuickResponse=" + this.lastResponse.isQuickResponse());
			System.out.println("isApproved=" + this.lastResponse.isApproved());
			System.out.println("isDeclined=" + this.lastResponse.isDeclined());
			System.out.println("AuthCode=" + this.lastResponse.getAuthCode());
			System.out.println("TxRefNum=" + this.lastResponse.getTxRefNum());
			System.out.println("ResponseCode=" + this.lastResponse.getResponseCode());
			System.out.println("Status=" + this.lastResponse.getStatus());
			System.out.println("Message=" + this.lastResponse.getMessage());
			System.out.println("AVSCode=" + this.lastResponse.getAVSResponseCode());
			System.out.println("CVV2ResponseCode=" + this.lastResponse.getCVV2RespCode());
		}
	}

	/**
	 * Invoke the credit Card transaction.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	private void processOrbitalCCVoidTransaction() throws Exception {

		// Get values from pti.properties files
		PTIConfig config = PTIConfig.getInstance();
		// Create a Transaction Processor
		// The Transaction Processor acquires and releases resources and
		// executes transactions.
		// It configures a pool of protocol engines, then uses the pool to
		// execute transactions.
		TransactionProcessorIF tp = null;
		try {
			tp = new TransactionProcessor();
		} catch (InitializationException iex) {
			System.err.println("TransactionProcessor failed to initialize");
			System.err.println(iex.getMessage());
			iex.printStackTrace();
		}
		// Create a request object
		// The request object uses the XML templates along with data we provide
		// to generate a String representation of the xml request
		RequestIF request = null;
		try {
			request = new Request(RequestIF.REVERSE_TRANSACTION);

			request.setFieldValue("MerchantID", config.getMerchantID());
			request.setFieldValue("BIN", "000001");
			if (this.orderID == null || this.orderID.trim().equals("")) {
				request.setFieldValue("OrderID", "Account_Maintenance");
			} else {
				request.setFieldValue("OrderID", this.orderID);				
			}
			// Note: Copy a TxRefNum from a previously approved auth, and
			// recompile this sample
			if (this.getTransactionTraceNumber() != null) {
				request.setFieldValue("TxRefNum", this.serviceTransactionNumber);
			}
			if (IconAPIContext.debugMode) {
				// Display the request
				System.out.println("\nVoid Request:\n" + request.getXML());
	
			}
		} catch (InitializationException ie) {
			System.err.println("Credit card void unable to initialize request object");
			System.err.println(ie.getMessage());
			ie.printStackTrace();
		} catch (FieldNotFoundException fnfe) {
			System.err.println("Credit card void Unable to find XML field in template");
			System.err.println(fnfe.getMessage());
			fnfe.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// Process the transaction
		// Pass in the request object (created above), and receive a response
		// object.
		// If the resources required by the Transaction Processor have been
		// exhausted,
		// this code will block until the resources become available.
		// The "TransactionProcessor.poolSize" configuration property specifies
		// how many resources
		// will be available. The TransactionProcessor acts as a governor, only
		// allowing
		// up to "poolSize" transactions outstanding at any point in time.
		// As transactions are completed, their resources are placed back in the
		// pool.
		ResponseIF response = null;
		try {
			response = tp.process(request);
		} catch (TransactionException tex) {
			System.err.println("Credit card void transaction failed, including retries and failover");
			System.err.println(tex.getMessage());
			tex.printStackTrace();
		}
		// Display the response
		// This line displays the entire xml response on the java system
		// console.
		if (IconAPIContext.debugMode) {
			System.out.println("\nResponse:\n" + response.toXmlString() + "\n");
		// The lines below report all the various attributes of the response
		// object.
		// It is not necessary to use all of these attributes - use only the
		// ones you need.
		// Also, some of the attributes are meaningful only for specific types
		// of transactions,
		// but for consistency and simplicity in the sample code we dump them
		// all for every transaction type.
			System.out.println("Response Attributes:");
			System.out.println("isGood=" + response.isGood());
			System.out.println("isError=" + response.isError());
			System.out.println("isQuickResponse=" + response.isQuickResponse());
			System.out.println("isApproved=" + response.isApproved());
			System.out.println("isDeclined=" + response.isDeclined());
			System.out.println("AuthCode=" + response.getAuthCode());
			System.out.println("TxRefNum=" + response.getTxRefNum());
			System.out.println("ResponseCode=" + response.getResponseCode());
			System.out.println("Status=" + response.getStatus());
			System.out.println("Message=" + response.getMessage());
			System.out.println("AVSCode=" + response.getAVSResponseCode());
			System.out.println("CVV2ResponseCode=" + response.getCVV2RespCode());
		}
	}

	/**
	 * Invoke the credit Card transaction to do an authorization.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	public void processCCAuthorizationTransaction() throws Exception {
		// set your external key for this transaction, recored on the Valexia
		// transaction
		this.externalKey = GuidFactory.getInstance().getSomewhatUniqueNumber().toString();

		this.processOrbitalCCAuthTransaction();

		this.authResponse = lastResponse;

		// if failed for network reason attempt to void, otherwise just set
		// error message
		if (!this.authResponse.isApproved()) {
			if (this.authFailureMessage == null) {
				this.authFailureMessage = new String();
			}

			if (this.lastResponse.isDeclined()) {
				this.authFailureMessage = "The credit card was not accepted.";
			}
			if (this.lastResponse.isError()) {
				this.authFailureMessage = "The credit card could not be processed, " + this.lastResponse.getMessage();
				// Process unavailable
				if (this.lastResponse.getResponseCode() != null && (this.lastResponse.getResponseCode().equals("L2") || this.lastResponse.getResponseCode().equals("99"))) {
					StringBuffer bodyText = new StringBuffer();
					try {
						EmailAlert alert = new EmailAlert();
						alert.setSender("CheckOutService@usatoday.com");
						alert.setSubject("Subscription Order Entry Alert - Possible PTI issue");
						bodyText.append("Failed to Process Credit Card: \n" + " \nUnable to parse PTI Response Code: '"
								+ this.getResponse().getResponseCode() + "' Message: " + this.getResponse().getMessage());
						bodyText.append("\n\n Order ID: ").append(orderID);
						alert.setBodyText(bodyText.toString());
						alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
						alert.sendAlert();
					} catch (Exception e) {
						System.out.println("Failed to send alert: " + bodyText.toString());
					}

				}

				this.authFailureMessage += "The credit card could not be processed, please retry.";
			}
		}
	}

	/**
	 * Invoke the credit Card transaction to do a void.
	 * 
	 * @param orderData
	 *            The orderData to set
	 */
	public void processCCVoidTransaction() throws Exception {
		if (this.externalKey == null) {
			throw new UsatException("No Previous Authorization to Void.");
		}

		if (this.authVoided) {
			// trying to void something that's already been voided/ignore
			return;
		}

		this.processOrbitalCCVoidTransaction();
		// Need to check that some vendors do not require a void for an auth
		if (this.lastResponse.isApproved() || this.lastResponse.getMessage().trim().equals("Authorization Not Found") || this.lastResponse.isGood()) {
			this.authVoided = true;
		}
	}

	public int getTraceNumber() {
		Random randomGenerator = new Random();
        this.traceNumber = randomGenerator.nextInt();
        if (this.traceNumber < 0) {
        	this.traceNumber = this.traceNumber * -1;
        }
		return this.traceNumber;
	}

	public void setTraceNumber(int traceNumber) {
		this.traceNumber = traceNumber;
	}
}
