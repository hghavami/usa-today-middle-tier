package com.usatoday.businessObjects.shopping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.shopping.OrderItemIntf;
import com.usatoday.business.interfaces.shopping.ShoppingCartIntf;
import com.usatoday.business.interfaces.shopping.payment.PaymentMethodIntf;
import com.usatoday.util.constants.UsaTodayConstants;

public class ShoppingCart implements ShoppingCartIntf {

	// billing contact
	private ContactIntf billingContact = null;

	// delivery/billing flag
	private boolean billingSameAsDelivery = true;

	private String checkOutErrorMessage = "";

	private boolean checkoutInProcess = false;

	private String clientIPAddress = "";

	// returning customer object
	private CustomerIntf customer = null;

	// delivery contact
	private ContactIntf deliveryContact = null;
	// items in cart
	private ArrayList<OrderItemIntf> items = new ArrayList<OrderItemIntf>();

	// Payment method
	private PaymentMethodIntf paymentMethod = null;

	/**
	 * @param item
	 *            The item to be added to the cart
	 */
	public void addItem(OrderItemIntf item) {
		items.add(item);
	}

	public void clearItems() {
		this.items.clear();
	}

	public ContactIntf getBillingContact() {
		return this.billingContact;
	}

	public String getCheckOutErrorMessage() {
		return this.checkOutErrorMessage;
	}

	public String getClientIPAddress() {
		return this.clientIPAddress;
	}

	public CustomerIntf getCustomer() {
		return this.customer;
	}

	public ContactIntf getDeliveryContact() {
		return this.deliveryContact;
	}

	public Collection<OrderItemIntf> getItems() {
		return this.items;
	}

	public CustomerIntf getOwningCustomer() {
		return this.customer;
	}

	public PaymentMethodIntf getPaymentMethod() {
		return this.paymentMethod;
	}

	public double getSalesTax() {
		Iterator<OrderItemIntf> itr = this.getItems().iterator();
		double salesTax = 0.0;
		while (itr.hasNext()) {
			OrderItemIntf item = (OrderItemIntf) itr.next();
			String deliveryZip = "";
			try {
				deliveryZip = this.getDeliveryContact().getUIAddress().getZip();
			} catch (Exception e) {
				deliveryZip = "";
			}
			try {
				salesTax += item.getTaxAmount(deliveryZip);
			} catch (UsatException e) {
				salesTax = 0.0;
			}
		}
		return salesTax;
	}

	/**
	 * @return double The subtotal
	 */
	public double getSubTotal() throws UsatException {
		Iterator<OrderItemIntf> itr = this.getItems().iterator();
		double subTotal = 0.0;
		while (itr.hasNext()) {
			OrderItemIntf item = (OrderItemIntf) itr.next();
			subTotal += item.getSubTotal();
		}
		return subTotal;
	}

	/**
	 * @return The total for the cart
	 */
	public double getTotal() throws UsatException {
		return this.getSalesTax() + this.getSubTotal();
	}

	public boolean isBillingSameAsDelivery() {
		return this.billingSameAsDelivery;
	}

	public boolean isCheckoutInProcess() {
		return this.checkoutInProcess;
	}

	/**
	 * This method determines if all data needed to check out is good.
	 */
	public boolean isReadyForCheckOut() {
		boolean ready = false;

		if (this.getItems().size() > 0) {
			if (this.getDeliveryContact() != null
					&& (this.getDeliveryContact().getUIAddress().isValidated() || UsaTodayConstants.ALLOW_FAILED_ADDRESSES)) {
				if (this.billingSameAsDelivery || (this.billingContact != null)
						&& (this.billingContact.getUIAddress().isValidated() || UsaTodayConstants.ALLOW_FAILED_ADDRESSES)) {
					if (this.paymentMethod != null) {
						ready = true;
					}
				}
			}
		}
		return ready;
	}

	public void removeItem(Long itemID) {
		Iterator<OrderItemIntf> itr = this.getItems().iterator();
		while (itr.hasNext()) {
			OrderItemIntf item = (OrderItemIntf) itr.next();
			if (item.getItemID().compareTo(itemID) == 0) {
				this.getItems().remove(item);
				break;
			}
		}
	}

	/**
	 * @param item
	 *            The item to be removed from the cart
	 */
	public void removeItem(OrderItemIntf item) {
		items.remove(item);
	}

	public void setBillingContact(ContactIntf contact) {
		this.billingContact = contact;
	}

	public void setCheckOutErrorMessage(String checkOutErrorMessage) {
		this.checkOutErrorMessage = checkOutErrorMessage;
	}

	public void setCheckoutInProcess(boolean checkoutInProcess) {
		this.checkoutInProcess = checkoutInProcess;
	}

	public void setClientIPAddress(String ip) {
		if (ip != null) {
			this.clientIPAddress = ip;
		}
	}

	public void setDeliveryContact(ContactIntf contact) {
		this.deliveryContact = contact;
	}

	/**
	 * @param The
	 *            existing customer which will be used to default the billing and delivery addresses
	 */
	public void setOwningCustomer(CustomerIntf existingCustomer) {
		this.customer = existingCustomer;

		if (this.customer != null) {
			SubscriberAccountIntf account = customer.getCurrentAccount();
			if (account != null) {
				this.setDeliveryContact(account.getDeliveryContact());
				if (!account.isBillingSameAsDelivery()) {
					this.setBillingContact(account.getBillingContact());
				} else {
					this.billingSameAsDelivery = true;
				}
			}
		}
	}

	public void setPaymentMethod(PaymentMethodIntf method) {
		this.paymentMethod = method;
	}

}
