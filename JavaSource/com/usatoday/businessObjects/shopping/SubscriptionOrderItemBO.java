/*
 * Created on May 4, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.shopping;

import java.text.NumberFormat;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;

/**
 * @author aeast
 * @date May 4, 2006
 * @class SubscriptionOrderItemBO
 * 
 *        This clas represents a cart item for a subscription product.
 * 
 */
public class SubscriptionOrderItemBO extends OrderItemBO implements SubscriptionOrderItemIntf {

	private SubscriptionTermsIntf selectedTerm = null;
	private String startDate = "";
	private String keyCode = "";
	private String orderID = "";
	private String premiumCode = "";
	private boolean choosingEZPay = false;
	private boolean oneTimeBill = false;
	private String deliveryEmailAddress = "";
	private String billingEmailAddress = "";
	private String clubNumber = "";
	private OfferIntf offer = null;
	private String deliveryMethod = "";
	private boolean deliveryMethodCheck = false;

	/**
     * 
     */
	public SubscriptionOrderItemBO() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#setSelectedTerm(com.usatoday.business.interfaces.products
	 * .SubscriptionTermsIntf)
	 */
	public void setSelectedTerm(SubscriptionTermsIntf term) {
		this.selectedTerm = term;
		if (this.getProduct() != null) {
			((SubscriptionProductIntf) (this.getProduct())).applyTerm(term);
			if (term.requiresEZPAY()) {
				this.choosingEZPay = true;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#getSelectedTerm()
	 */
	public SubscriptionTermsIntf getSelectedTerm() {
		return this.selectedTerm;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#getStartDate()
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#getKeyCode()
	 */
	public String getKeyCode() {
		return this.keyCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#getSubscriptionAmountForPersistence()
	 */
	public String getSubscriptionAmountForPersistence() {
		if (this.selectedTerm == null) {
			return "000";
		}

		double amountToFormat = this.selectedTerm.getPrice();
		String temp = null;

		try {
			temp = this.formatAmountWithoutDecimal(amountToFormat);
			temp = temp.replaceAll(",", "");
		} catch (Exception e) {
			System.out.println("SubscriptionOrderItemBO::getSubscriptionAmountForPersistence() -- Problem calculating total: "
					+ e.getMessage());
			System.out.println("SubscriptionOrderItemBO::getSubscriptionAmountForPersistence() -- Delivery Email: "
					+ this.getDeliveryEmailAddress() + " Order ID: " + this.getOrderID() + " SubsAmount: "
					+ this.selectedTerm.getPrice() + "  Delivery Zip: " + this.getDeliveryZip() + " Copies: " + this.getQuantity());
			temp = "000";
		}

		return temp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#getSubscriptionTotalAmountForPersistence()
	 */
	public String getSubscriptionTotalAmountForPersistence() {
		String temp = null;

		try {
			double amountToFormat = this.getTotal();
			temp = this.formatAmountWithoutDecimal(amountToFormat);
			temp = temp.replaceAll(",", "");
		} catch (Exception e) {
			System.out.println("ExtranetBaseOrder::getSubscriptionTotalAmountForPersistence() -- Problem calculating total: "
					+ e.getMessage());
			System.out.println("SubscriptionOrderItemBO::getSubscriptionTotalAmountForPersistence() -- Email: "
					+ this.getDeliveryEmailAddress() + " Order ID: " + this.getOrderID() + " SubsAmount: "
					+ this.selectedTerm.getPrice() + "  Delivery Zip: " + this.getDeliveryZip() + " Copies: " + this.getQuantity());
			temp = "000";
		}

		return temp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#getOrderID()
	 */
	public String getOrderID() {
		return this.orderID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#isOneTimeBill()
	 */
	public boolean isOneTimeBill() {
		return this.oneTimeBill;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#isChoosingEZPay()
	 */
	public boolean isChoosingEZPay() {
		return this.choosingEZPay;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#getEmailAddress()
	 */
	public String getDeliveryEmailAddress() {
		return this.deliveryEmailAddress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#getPremiumCode()
	 */
	public String getPremiumCode() {
		return this.premiumCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf#getClubNumber()
	 */
	public String getClubNumber() {
		return this.clubNumber;
	}

	public void setChoosingEZPay(boolean choosingEZPay) throws UsatException {
		if (this.selectedTerm == null || !this.selectedTerm.requiresEZPAY()) {
			this.choosingEZPay = choosingEZPay;
		} else {
			if (choosingEZPay == true) {
				this.choosingEZPay = true;
			} else {
				throw new UsatException("The selected term requires the EZ-PAY payment plan.");
			}
		}
	}

	public void setClubNumber(String clubNumber) {
		if (clubNumber != null) {
			if (clubNumber.length() > 15) {
				this.clubNumber = clubNumber.substring(0, 15);
			} else {
				this.clubNumber = clubNumber;
			}
		}
	}

	public void setDeliveryEmailAddress(String emailAddress) {
		this.deliveryEmailAddress = emailAddress;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public void setOneTimeBill(boolean ontTimeBill) {
		this.oneTimeBill = ontTimeBill;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public void setPremiumCode(String premiumCode) {
		this.premiumCode = premiumCode;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * 
	 * @param amount
	 * @return
	 * @throws Exception
	 */
	private String formatAmountWithoutDecimal(double amount) throws Exception {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		String temp = null;

		temp = formatter.format(amount);
		// Drop the $ sign
		temp = temp.substring(1, temp.length());

		int I = 0;
		while ((I < 7) && (temp.charAt(I) != '.')) {
			I++;
		}
		temp = temp.substring(0, I) + temp.substring(I + 1, temp.length());
		if ((temp.length() - I) == 0) {
			temp = temp + "00";
		}
		if ((temp.length() - I) == 1) {
			temp = temp + "0";
		}

		return temp;
	}

	/**
	 * @return Returns the offer.
	 */
	public OfferIntf getOffer() {
		return this.offer;
	}

	/**
	 * @param offer
	 *            The offer to set.
	 */
	public void setOffer(OfferIntf offer) {
		this.offer = offer;

		if (this.offer != null) {
			if (this.offer.isForceEZPay()) {
				this.choosingEZPay = true;
			}
		}
	}

	/**
	 * @return Returns the deliveryMethod.
	 */
	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	/**
	 * @param deliveryMethod
	 *            The deliveryMethod to set.
	 */
	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	/**
	 * @return Returns the deliveryMethodCheck.
	 */
	public boolean isDeliveryMethodCheck() {
		return deliveryMethodCheck;
	}

	/**
	 * @param deliveryMethodCheck
	 *            The deliveryMethodCheck to set.
	 */
	public void setDeliveryMethodCheck(boolean deliveryMethodCheck) {
		this.deliveryMethodCheck = deliveryMethodCheck;
	}

	public String getBillingEmailAddress() {
		return billingEmailAddress;
	}

	public void setBillingEmailAddress(String billingEmailAddress) {
		this.billingEmailAddress = billingEmailAddress;
	}

	public DateTime getStartDateObj() {

		DateTime dt = null;
		if (this.startDate != null) {
			DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
			try {
				dt = formatter.parseDateTime(this.startDate);
			} catch (Exception e) {
			}
		}
		return dt;
	}
}
