/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.shopping;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;
import com.gannett.usat.iconapi.domainbeans.makePayment.MakeAPaymentResponse;
import com.gannett.usat.iconapi.domainbeans.newSubscriptionStart.StartSubscriptionResponse;
import com.paytec.webapp.business.Address;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.shopping.OrderIntf;
import com.usatoday.business.interfaces.shopping.OrderItemIntf;
import com.usatoday.business.interfaces.shopping.RenewalOrderItemIntf;
import com.usatoday.business.interfaces.shopping.ShoppingCartIntf;
import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentMethodIntf;
import com.usatoday.business.interfaces.shopping.payment.PaymentMethodIntf;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.customer.service.CustomerService;
import com.usatoday.businessObjects.shopping.payment.PTIConfig;
import com.usatoday.businessObjects.shopping.payment.PTICreditCardProcessor;
import com.usatoday.businessObjects.subscriptionTransactions.GenesysResponse;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;
import com.usatoday.businessObjects.util.mail.EmailAlert;
import com.usatoday.integration.EmailRecordTO;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class CheckOutService
 * 
 *        This Application Service class simply provides the business process for performing a checkout.
 * 
 */
public class CheckOutService {

	/**
	 * 
	 * @param cart
	 *            The cart to be checked out
	 * @return OrderIntf an instance of the placed order
	 * @throws UsatException
	 *             if the order cannot be processed for any reason
	 */
	public OrderIntf checkOutShoppingCart(ShoppingCartIntf cart, boolean allowBatchPayment) throws UsatException {
		if (!cart.isCheckoutInProcess()) {
			cart.setCheckoutInProcess(true);
		} else {
			throw new UsatException("Cart Check Out Already Initiated. Please wait for it to complete.");
		}
		OrderBO order = null;
		try {

			if (cart == null || cart.getItems().size() == 0 || !cart.isReadyForCheckOut()) {
				throw new UsatException("Your shopping cart is not ready for checkout.");
			}

			// clear any previous failed checkout message
			cart.setCheckOutErrorMessage("");

			// flag to indicate if processing a UT subscription and flag
			// for SW product
			boolean containsUTProduct = false;
			boolean containsSWProduct = false;
			boolean isRenewal = false;

			// convert cart to an order and persist the order to the database
			order = new OrderBO();

			// Generate an order id
			String orderID = order.getOrderID();

			// set up and process payment
			PaymentMethodIntf paymentMethod = cart.getPaymentMethod();
			ContactIntf billingContact = cart.getBillingContact();
			if (billingContact == null) {
				billingContact = cart.getDeliveryContact();
				order.setBillingSameAsDelivery(true);
			} else {
				order.setBillingSameAsDelivery(false);
			}

			// boolean isGannettUnit = false;

			order.setBillingContact(billingContact);
			order.setDeliveryContact(cart.getDeliveryContact());
			order.setOrderedItems(new ArrayList<OrderItemIntf>(cart.getItems()));
			order.setOrderDate(new DateTime());
			order.setPaymentMethod(paymentMethod);

			Collection<OrderItemIntf> orderItems = cart.getItems();
			Iterator<OrderItemIntf> itemItr = orderItems.iterator();

			// check for any ut or sw items and set flags accordingly.
			// This is required because we seem to need to use a different
			// PTI client id depending on product purchased (bad design)
			// also capture the offer for the item
			OfferIntf offer = null;

			while (itemItr.hasNext()) {
				OrderItemIntf item = itemItr.next();

				if (item instanceof RenewalOrderItemIntf) {
					isRenewal = true; // used later
				}

				if (item instanceof SubscriptionOrderItemIntf) {
					SubscriptionOrderItemIntf subscriptionItem = (SubscriptionOrderItemIntf) item;

					offer = subscriptionItem.getOffer();

					if (UsaTodayConstants.UT_PUBCODE.equalsIgnoreCase(subscriptionItem.getProduct().getBrandingPubCode())) {
						containsUTProduct = true;
					} else if (UsaTodayConstants.SW_PUBCODE.equalsIgnoreCase(subscriptionItem.getProduct().getBrandingPubCode())) {
						containsSWProduct = true;
					}

					/*
					 * HDCONS-44 - No more Gannett Units try {
					 * 
					 * GUIZipCodeManager guiZipMngr = GUIZipCodeManager.getInstance();
					 * 
					 * String zip = null; try { zip = cart.getDeliveryContact().getPersistentAddress().getZip(); if (zip == null) {
					 * zip = cart.getDeliveryContact().getUIAddress().getZip(); } } catch (Exception e) { zip =
					 * cart.getDeliveryContact().getUIAddress().getZip(); }
					 * if(guiZipMngr.isGUIZipForPub(subscriptionItem.getProduct().getProductCode(), zip)){ isGannettUnit = true; } }
					 * catch (Exception e) { isGannettUnit = false; }
					 */

					// validate that the card used is available for this offer
					if (paymentMethod.getPaymentType().requiresPaymentProcessing()
							&& subscriptionItem.getOffer().getPromotionSet().getPromoCreditCard() != null
							&& !subscriptionItem.getOffer().getPromotionSet().getPromoCreditCard()
									.cardInPromotion(paymentMethod.getPaymentType().getType())) {
						String keyCode = "";
						if (offer instanceof SubscriptionOfferIntf) {
							keyCode = ((SubscriptionOfferIntf) offer).getKeyCode();
						} else {
							keyCode = "RENEWAL";
						}
						String errMsg = "The offer for keycode: " + keyCode + ", does not accept the card type: "
								+ paymentMethod.getPaymentType().getLabel();
						cart.setCheckOutErrorMessage(errMsg);
						throw new UsatException(errMsg);
					}
				}
			}

			// set totals in order
			order.setSubTotal(cart.getSubTotal());

			CreditCardPaymentMethodIntf ccPayment = null;
			PTICreditCardProcessor cardProcessor = null;

			// if not a bill me then process card
			if (UsaTodayConstants.AUTHORIZE_CREDIT_CARDS /* (HDCONS-44 no GU) && !(isGannettUnit) */
					&& paymentMethod.getPaymentType().requiresPaymentProcessing()
					&& paymentMethod instanceof CreditCardPaymentMethodIntf) {

				boolean voidZeroChargeProduct = false;
				// Identify transaction amount to be voided. In this case, we will void all 0.01 transactions
				// if (cart.getTotal() == 0.00) {
				voidZeroChargeProduct = true;
				// }

				ccPayment = (CreditCardPaymentMethodIntf) paymentMethod;
				Address address = new Address();
				address.setAddressLine1(billingContact.getUIAddress().getAddress1());
				address.setAddressLine2(billingContact.getUIAddress().getAptSuite());
				address.setCity(billingContact.getUIAddress().getCity());
				if (billingContact.getUIAddress().getState().length() == 2) {
					address.setStateAbbrev(billingContact.getUIAddress().getState());
				}
				// USA Country only
				address.setCountryCode("840");
				address.setPostalCode(billingContact.getUIAddress().getZip());

				cardProcessor = new PTICreditCardProcessor();
				cardProcessor.setAddress(address);
				cardProcessor.setClientIPAddress(cart.getClientIPAddress());
				cardProcessor.setPaymentType(paymentMethod.getPaymentType());
				cardProcessor.setBillingFirstName(billingContact.getFirstName());
				cardProcessor.setBillingLastName(billingContact.getLastName());
				cardProcessor.setCardHolderName(ccPayment.getNameOnCard());
				cardProcessor.setCardNumber(ccPayment.getCardNumber());
				// use 0.01 to process the 0.0 rate
				if (voidZeroChargeProduct) {
					cardProcessor.setChargeAmount(1);
				} else {
					cardProcessor.setChargeAmount(cart.getTotal());
				}
				cardProcessor.setCvcNumber(ccPayment.getCVV());
				if (billingContact.getEmailAddress() != null && billingContact.getEmailAddress().length() > 0) {
					cardProcessor.setEmailAddress(billingContact.getEmailAddress());
				} else {
					cardProcessor.setEmailAddress(cart.getDeliveryContact().getEmailAddress());
				}
				cardProcessor.setExpMonth(ccPayment.getExpirationMonth());
				cardProcessor.setExpYear(ccPayment.getExpirationYear());
				cardProcessor.setOrderID(orderID);
				try {
					if (containsUTProduct) {
						cardProcessor.setPTIClientID(PTIConfig.getInstance().getUTClientID());
					} else if (containsSWProduct) {
						cardProcessor.setPTIClientID(PTIConfig.getInstance().getSWClientID());
					} else {
						cardProcessor.setPTIClientID(PTIConfig.getInstance().getMerchandiseClientID());
					}
				} catch (Exception e) {
					System.out.println("CheckOutService::building cardProcessor - Payment processing not properly configured: "
							+ e.getMessage());
				}

				String ccErrorMessage = null;

				// charge the card
				try {
					cardProcessor.processCCAuthorizationTransaction();

					// boolean cardAuthorized = cardProcessor.getAuthResponse().isSuccessful();
					boolean cardAuthorized = cardProcessor.getAuthResponse().isApproved();
					if (cardAuthorized) {
						// card charged
						// but we may void if failed CVV or AVS.

						boolean failOnAVSFailure = true;

						if (isRenewal || (offer != null && offer.getPromotionSet().getAVSRequiredOverride() != null)) {
							// do not fail on AVS for renewals since we don't offer a way to enter billing information currently
							// since AVS by default is required, any existence of a promo for an AVS Override indicates we should
							// not fail on an AVS Failure.
							failOnAVSFailure = false;
						}

						if (PTIConfig.getInstance().isFailOnCVV() && cardProcessor.voidLastAuthorizationIfCVVFailed()) {
							ccErrorMessage = "Error Processing Credit Card: Invalid Card Verification Number";
							cart.setCheckOutErrorMessage(ccErrorMessage);
							throw new UsatException(ccErrorMessage);
						} else if (PTIConfig.getInstance().isFailOnAVS() && failOnAVSFailure
								&& cardProcessor.voidLastAuthorizationIfAVSFailed()) {
							ccErrorMessage = "Error Processing Credit Card: Billing address does not match credit card billing address on file. Please check that your billing address is entered correctly.";
							cart.setCheckOutErrorMessage(ccErrorMessage);
							throw new UsatException(ccErrorMessage);
						} else {
							// Goood charge
							ccPayment.setAuthorizedSuccessfully(true);
							// ccPayment.setAuthCode(cardProcessor.getResponse().getAuthorizationCode());
							ccPayment.setAuthCode(cardProcessor.getResponse().getAuthCode());
							order.setOrderPaymentStatus(OrderIntf.PAYMENT_AUTHORIZED);
							// HDCONS-44 This will be set after the ICON API call
							// order.setTaxAmount(cart.getSalesTax());
							// order.setAmountCharged(cart.getTotal());
							// No voided needed for Visa and MC
							if (!ccPayment.getCreditCardTypeCode().equals("V") && !ccPayment.getCreditCardTypeCode().equals("M")) {
								// done to void 0.01 transaction for 0.0 charges
								if (voidZeroChargeProduct) {

									boolean voided = false;
									try {
										cardProcessor.processCCVoidTransaction();
										// voided = cardProcessor.getResponse().isSuccessful();
										voided = cardProcessor.getResponse().isApproved();
									} catch (Exception e) {
										voided = false;
									}

									if (!voided) {
										// send email if charge not voided correctly

										StringBuffer bodyText = new StringBuffer();
										try {
											EmailAlert alert = new EmailAlert();
											alert.setSender("InternetCheckOutService@usatoday.com");
											alert.setSubject("Order Entry Alert - FAILED to Void 0.01 charge");
											bodyText.append("\n \nCredit Card Transaction NOT voided successfully: CCLOG: "
													+ cardProcessor.generateCCLogRecord().toString());
											bodyText.append("\n \nCredit Card Voided: ").append(voided);
											bodyText.append("\n \nBilling Last Name: " + billingContact.getLastName());
											bodyText.append("\nBilling First Name: " + billingContact.getFirstName());
											bodyText.append("\nBilling Home Phone: " + billingContact.getHomePhone());
											bodyText.append("\nBilling Bus Phone: " + billingContact.getBusinessPhone());
											bodyText.append("\nBilling Address: " + billingContact.getUIAddress().toString());
											bodyText.append("\n \nDelivery Last Name: " + cart.getDeliveryContact().getLastName());
											bodyText.append("\nDelivery First Name: " + cart.getDeliveryContact().getFirstName());
											bodyText.append("\nDelivery Home Phone: " + cart.getDeliveryContact().getHomePhone());
											bodyText.append("\nDelivery Bus Phone: " + cart.getDeliveryContact().getBusinessPhone());
											bodyText.append("\nDelivery Email: " + cart.getDeliveryContact().getEmailAddress());
											bodyText.append("\nDelivery Address: "
													+ cart.getDeliveryContact().getUIAddress().toString());
											bodyText.append("\n\n Order ID: ").append(orderID);
											alert.setBodyText(bodyText.toString());
											alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
											alert.sendAlert();
										} catch (Exception exp) {
											System.out.println("Failed to send alert: " + bodyText.toString());
										}

									} // end if not voided
								}
							}
						}// end else (card charged)
					} else { // card failed to Auth
						ccErrorMessage = "Error Processing Credit Card: " + cardProcessor.getAuthFailureMessage();

						// if we are allowing batch processing then simply flag item
						// for batch processing
						if (cardProcessor.isNetworkFailure() && UsaTodayConstants.BATCH_PROCESS_CC_NETWORK_FAILURES) {
							ccPayment.setAuthorizedSuccessfully(false);
							order.setOrderPaymentStatus(OrderIntf.PAYMENT_AUTH_PENDING);

						} else {
							// otherwise throw exception on non network error or no batching
							cart.setCheckOutErrorMessage(ccErrorMessage);
							throw new UsatException(ccErrorMessage);
						}
					}

				} catch (Exception e) {
					// e.printStackTrace();
					// check for Authorization and void if necessary
					// if (cardProcessor.getAuthResponse().isSuccessful()) {
					if (cardProcessor.getAuthResponse().isApproved()) {
						try {
							cardProcessor.processCCVoidTransaction();
						} catch (Exception eeee) {
							System.out.println("Failed to Void. " + eeee.getMessage());
						}
					}
					throw new UsatException(e);
				}

			} else {
				// if credit card payment but not done
				if (paymentMethod.getPaymentType().requiresPaymentProcessing()) {
					ccPayment = (CreditCardPaymentMethodIntf) paymentMethod;
					ccPayment.setAuthorizedSuccessfully(false);
					order.setOrderPaymentStatus(OrderIntf.PAYMENT_AUTH_PENDING);
				} else {
					throw new UsatException(
							"No Payment Information found. \"Bill Me\" is not a payment option. Please call Customer Service for assistance.");
					// order.setOrderPaymentStatus(OrderIntf.PAYMENT_UNBILLED);
					// order.setAmountCharged(0.0);
				}
			}

			// email records to persist - the method generateNewEmailRecords modified for HDCONS-44
			Collection<EmailRecordIntf> emailRecords = new ArrayList<EmailRecordIntf>();

			itemItr = orderItems.iterator();
			//
			while (itemItr.hasNext()) {
				OrderItemIntf item = itemItr.next();

				if (item instanceof SubscriptionOrderItemIntf) {
					SubscriptionOrderItemIntf subscriptionItem = (SubscriptionOrderItemIntf) item;

					// set the order id in the item
					subscriptionItem.setOrderID(orderID);

					// ---------------------------------------------
					if (!(item instanceof RenewalOrderItemIntf)) { // if not a renewal
						GenesysResponse gr = null;
						if (order.isBillingSameAsDelivery()) {
							try {
								gr = CustomerService.processSubscriptionStart(subscriptionItem, cart.getDeliveryContact(), null,
										ccPayment);

							} catch (Exception e) {
								System.out.println("(Scenario 1) - Failed to create new start: " + e.getMessage() + ".");
								throw new UsatException(e);
							}

						} else {
							try {
								gr = CustomerService.processSubscriptionStart(subscriptionItem, cart.getDeliveryContact(),
										billingContact, ccPayment);
							} catch (Exception e) {
								System.out.println("(Scenario 2) - Failed to create start transaction: " + e.getMessage() + ".");
								throw new UsatException(e);
							}

						} // end else billing different than delivery

						// check for Genesys error messages
						if (gr.isContainsErrors()) {
							StringBuilder sb = new StringBuilder();
							Collection<GenesysBaseAPIResponse> responses = gr.getResponses();
							for (GenesysBaseAPIResponse res : responses) {
								if (res.containsErrors()) {
									if (res.redirectResponse()) {
										sb.append("UNEXPECTED ERROR: " + res.getRawResponse());
									} else {
										// business rule errors exist
										Collection<String> errors = res.getErrorMessages();
										if (errors != null) {
											for (String msg : errors) {
												sb.append(msg).append("   ");
											}
										}
									}
								}
							} // end for responses
							throw new UsatException(sb.toString());
						} else {
							Collection<GenesysBaseAPIResponse> res = gr.getResponses();

							StartSubscriptionResponse sRes = (StartSubscriptionResponse) res.iterator().next();

							double tax = Double.valueOf(sRes.getStart_subscription().getSubscription_tax()).doubleValue();
							double total = Double.valueOf(sRes.getStart_subscription().getSubscription_total()).doubleValue();
							// double subTotal = total - tax;
							order.setTaxAmount(tax);
							order.setAmountCharged(total);

						}

					} // end if new transaction
						// ---------------------------------------------
					else { // renewal transaction
						GenesysResponse gr = null;

						RenewalOrderItemIntf renewalItem = (RenewalOrderItemIntf) item;

						try {
							// trans = SubscriptionTransactionFactory.createSubscriptionTransaction(subscriptionItem,
							// cart.getDeliveryContact(), billingContact, paymentMethod);
							gr = CustomerService.processSubscriptionRenewal(renewalItem, ccPayment);
						} catch (Exception e) {
							System.out.println("(Renewal Scenario 1) - Failed to create renewal transaction: " + e.getMessage()
									+ ".");
							throw new UsatException(e);
						}

						// check for Genesys error messages
						if (gr.isContainsErrors()) {
							StringBuilder sb = new StringBuilder();
							Collection<GenesysBaseAPIResponse> responses = gr.getResponses();
							for (GenesysBaseAPIResponse res : responses) {
								if (res.containsErrors()) {
									if (res.redirectResponse()) {
										sb.append("UNEXPECTED ERROR: " + res.getRawResponse());
									} else {
										// business rule errors exist
										Collection<String> errors = res.getErrorMessages();
										if (errors != null) {
											for (String msg : errors) {
												sb.append(msg).append("   ");
											}
										}
									}
								}
							} // end for responses
							throw new UsatException(sb.toString());
						} else {
							Collection<GenesysBaseAPIResponse> res = gr.getResponses();

							MakeAPaymentResponse sRes = (MakeAPaymentResponse) res.iterator().next();

							try {
								double tax = Double.valueOf(sRes.getMake_a_payment().getSubscription_tax()).doubleValue();
								double subTotal = Double.valueOf(sRes.getMake_a_payment().getSubscription_subtotal()).doubleValue();
								double total = Double.valueOf(sRes.getMake_a_payment().getSubscription_total()).doubleValue();

								order.setTaxAmount(tax);
								order.setSubTotal(subTotal);
								order.setAmountCharged(total);
							} catch (Exception e) {
								// ignore
							}

						}

					} // end if renewal

					try {
						if (!(item instanceof RenewalOrderItemIntf)) {
							// only create email records for new orders
							emailRecords.addAll(this.generateNewEmailRecord(cart, subscriptionItem, orderID));
						}
					} catch (Exception e) {
						e.printStackTrace();
						try {
							StringBuilder bodyText = new StringBuilder();
							EmailAlert alert = new EmailAlert();
							alert.setSender("CheckOutService@usatoday.com");
							alert.setSubject("Subscription Order Entry Alert - Possible Logic Issue");
							bodyText.append("Failed to create Email Records for Transaction. XTRNTDWNLD transaction record created, save pending.");
							bodyText.append("\n\n Order ID: ").append(orderID);
							alert.setBodyText(bodyText.toString());
							alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
							alert.sendAlert();
						} catch (Exception e2) {
							System.out.println(e2.getMessage());
						}

					}

				}

			} // end while more items in cart

			// insert email Record
			if (emailRecords.size() > 0) {
				for (EmailRecordIntf eRec : emailRecords) {
					try {
						EmailRecordBO emailRecordBO = new EmailRecordBO(eRec);
						emailRecordBO.save();
						order.addSavedEmailRecord(emailRecordBO);
					} catch (Exception e) {
						StringBuffer bodyText = new StringBuffer();
						try {
							EmailAlert alert = new EmailAlert();
							alert.setSender("InternetCheckOutService@usatoday.com");
							alert.setSubject("Order Entry Alert - FAILED to Save Email Record to XTRNTEMAIL");
							bodyText.append("Exception: " + e.getMessage());
							bodyText.append("\n \nTransaction record was saved successfully. Email record should be added manually. Possible stuckie scenario if not done before download.");
							bodyText.append("\n \nPubCode: " + eRec.getPubCode());
							bodyText.append("\nEmail Address: " + eRec.getEmailAddress());
							bodyText.append("\n:Order/WebID: " + eRec.getOrderID());
							bodyText.append("\nPerm Start Date: " + eRec.getPermStartDate());
							bodyText.append("\nZip: " + eRec.getZip());
							bodyText.append("\nPassword: use blank password");
							bodyText.append("\nDateUpdated: " + UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());
							bodyText.append("\nTimeUpdated: " + UsatDateTimeFormatter.getSystemTimeHHMMSS());

							alert.setBodyText(bodyText.toString());
							alert.setReceiverList(UsaTodayConstants.EMAIL_ALERT_RECEIVER_EMAILS);
							alert.sendAlert();
						} catch (Exception exp) {
							System.out.println("Failed to send alert: " + bodyText.toString());
						}
					}
				}
			} // end if email records

			// clear the cart if successful
			cart.clearItems();
		} finally {
			// clear the flag indicating a checkout is in process
			cart.setCheckoutInProcess(false);
		}

		return order;
	}

	/**
	 * 
	 * @param cart
	 * @param item
	 * @param orderID
	 * @return
	 */
	private Collection<EmailRecordIntf> generateNewEmailRecord(ShoppingCartIntf cart, SubscriptionOrderItemIntf item, String orderID) {
		ArrayList<EmailRecordIntf> newRecords = new ArrayList<EmailRecordIntf>();

		ContactIntf deliveryContact = cart.getDeliveryContact();
		ContactIntf billingContact = cart.getBillingContact();

		String emailAddress = cart.getDeliveryContact().getEmailAddress();
		String password = cart.getDeliveryContact().getPassword();

		String startDate = "";
		if (item.getStartDate() != null && item.getStartDate().length() == 8) { // YYYYMMDD format
			startDate = item.getStartDate();
		}

		// HDCONS-44 we only need one email record per customer now
		boolean createNewDeliveryEmailRecord = false;
		try {
			Collection<EmailRecordIntf> existingEmails = EmailRecordBO.getEmailRecordsForEmailAddress(emailAddress);
			if (existingEmails.size() == 0) {
				createNewDeliveryEmailRecord = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			createNewDeliveryEmailRecord = true;
		}
		// String pubCode, String accountNumber, String orderID,
		// String emailAddress, String password, boolean active, boolean gift,
		// int serialNumber, Date dateUpdated, String confirmationEmailSent, String permStart, String zip
		EmailRecordIntf newEmailRec = null;
		if (createNewDeliveryEmailRecord) {
			newEmailRec = new EmailRecordTO(item.getProduct().getProductCode(), "", orderID, emailAddress, password, true, false,
					-1, Calendar.getInstance().getTime(), "N", startDate, cart.getDeliveryContact().getUIAddress().getZip(), "",
					cart.getDeliveryContact().getFirstName(), cart.getDeliveryContact().getLastName(), "", "");
			newRecords.add(newEmailRec);
		}

		if (billingContact != null && billingContact.getEmailAddress() != null
				&& !billingContact.getEmailAddress().equalsIgnoreCase(deliveryContact.getEmailAddress())) {

			// HDCONS-44 we only need one email record per customer now
			boolean createNewBillingEmailRecord = false;
			try {
				Collection<EmailRecordIntf> existingEmails = EmailRecordBO.getEmailRecordsForEmailAddress(billingContact
						.getEmailAddress());
				if (existingEmails.size() == 0) {
					createNewBillingEmailRecord = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
				createNewBillingEmailRecord = true;
			}

			if (createNewBillingEmailRecord) {
				newEmailRec = new EmailRecordTO(item.getProduct().getProductCode(), "", orderID, billingContact.getEmailAddress(),
						billingContact.getPassword(), true, true, -1, Calendar.getInstance().getTime(), "N", startDate, cart
								.getBillingContact().getUIAddress().getZip(), "", cart.getBillingContact().getFirstName(), cart
								.getBillingContact().getLastName(), "", "");

				newRecords.add(newEmailRec);
			}
		}

		return newRecords;
	}

}
