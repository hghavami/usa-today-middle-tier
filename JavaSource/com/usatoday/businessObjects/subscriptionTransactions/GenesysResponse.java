package com.usatoday.businessObjects.subscriptionTransactions;

import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.iconapi.domainbeans.GenesysBaseAPIResponse;

public class GenesysResponse {

	private Collection<GenesysBaseAPIResponse> responses = new ArrayList<GenesysBaseAPIResponse>();

	public boolean isContainsErrors() {
		if (responses.size() > 0) {
			boolean containsErrors = false;
			for (GenesysBaseAPIResponse res : responses) {
				if (res.containsErrors()) {
					containsErrors = true;
					break;
				}
			}
			return containsErrors;
		}
		return false;
	}

	public Collection<String> getErrorMessages() {
		ArrayList<String> allMessages = new ArrayList<String>();
		if (this.responses.size() > 0) {
			for (GenesysBaseAPIResponse res : this.responses) {
				if (res.containsErrors()) {
					allMessages.addAll(res.getErrorMessages());
				}
			}
		}
		return allMessages;
	}

	public Collection<GenesysBaseAPIResponse> getResponses() {
		return responses;
	}

	public void addResponse(GenesysBaseAPIResponse response) {
		this.responses.add(response);
	}

}
