/*
 * Created on Apr 26, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.subscriptionTransactions;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.PersistentSubscriberAccountIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.shopping.RenewalOrderItemIntf;
import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentMethodIntf;
import com.usatoday.business.interfaces.shopping.payment.PaymentMethodIntf;
import com.usatoday.business.interfaces.subscriptionTransactions.ExtranetSubscriberTransactionIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.util.GUIZipCodeManager;
import com.usatoday.businessObjects.util.KeyCodeParser;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;
import com.usatoday.integration.SubscriberTransactionTO;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 * @date Apr 26, 2006
 * @class SubscriptionTransactionFactory
 * 
 *        This class converts business object to SubscriberTrasaction objects that are ready for persistent storage.
 * 
 */
public class SubscriptionTransactionFactory {

	/**
	 * 
	 * @param subscriptionOrder
	 *            - The subscription order item
	 * @param deliveryContact
	 *            - Where it should be delivered to
	 * @param billingContact
	 *            - who is paying
	 * @param paymentInfo
	 *            - How they are paying
	 * @return - A XTRNTDWNLD transaction transfer object that is ready for persistence
	 * @throws UsatException
	 */
	public static ExtranetSubscriberTransactionIntf createCCInfoTransaction(PersistentSubscriberAccountIntf subscriberAccountInfo,
			PaymentMethodIntf paymentInfo) throws UsatException {
		SubscriberTransactionTO trans = new SubscriberTransactionTO();

		if (subscriberAccountInfo == null || paymentInfo == null) {
			throw new UsatException(
					"SubscriptionTransactionFactory():createSubscriptionTransaction: Invalid arguments, cannot create subscription transaction.");
		}

		boolean isGannettUnit = false;

		/*
		 * "1 PubCode, TranRecTyp, LastName, FirstName, FirmName, AddAddr1, AddAddr2, 8 UnitNum, HalfUnitNum, StreetDir, StreetName,
		 * StreetTyp, StreetPstDir, SubUnitCode, 15 SubUnitNum, City, State, Zip5, HomePhone, BusPhone, TranType, TranCode,
		 * BillLastName,
		 */
		// New subscription type
		trans.setPubCode(subscriberAccountInfo.getPubCode());
		trans.setTransactionRecType("04");
		trans.setAccountNum("");
		trans.setLastName(subscriberAccountInfo.getLastName().toUpperCase());
		trans.setFirstName(subscriberAccountInfo.getFirstName().toUpperCase());
		trans.setFirmName(subscriberAccountInfo.getFirmName().toUpperCase());
		trans.setHomePhone(subscriberAccountInfo.getHomePhone());
		trans.setBusinessPhone(subscriberAccountInfo.getBusinssPhone());
		trans.setAddAddr1(subscriberAccountInfo.getAddlAddr1().toUpperCase()); // double
																				// check
																				// this
		// addr 2 not used
		trans.setAddAddr2("");
		trans.setUnitNum(subscriberAccountInfo.getUnitNum());
		trans.setHalfUnitNum(subscriberAccountInfo.getHalfUnitNum());
		trans.setStreetDir(subscriberAccountInfo.getStreetDir().toUpperCase());
		trans.setStreetName(subscriberAccountInfo.getStreetName().toUpperCase());
		trans.setStreetType(subscriberAccountInfo.getStreetType().toUpperCase());
		trans.setStreetPostDir(subscriberAccountInfo.getStreetPostDir().toUpperCase());
		trans.setSubUnitCode(subscriberAccountInfo.getSubUnitCode());
		trans.setSubUnitNum(subscriberAccountInfo.getSubUnitNum());
		trans.setCity(subscriberAccountInfo.getCity().toUpperCase());
		trans.setState(subscriberAccountInfo.getState().toUpperCase());
		trans.setZip5(subscriberAccountInfo.getZip());

		// hard code transaction type
		trans.setTransactionType("I");
		// hard code transaction code
		trans.setTransactionCode("SM");
		/*
		 * 24 BillFirstName, BillFirmName, BillAddr1, BillAddr2, BillUnitNum, BillHalfUnitNum, 30 BillStreetDir, BillStreetName,
		 * BillStreetType, BillStreetPostDir, BillSubUnitCode, 35 BillSubUnitNum, BillCity, BillState, BillZip5, BillHomePhone,
		 * BillBusPhone, SubsDur,
		 */
		if (subscriberAccountInfo.getBillingZip() != null) {
			trans.setTransactionRecType("11"); // Type 11 when billing address
			trans.setTransactionType(""); // No Trans Type
			trans.setTransactionCode(""); // No Trans Code
			trans.setBillingLastName(subscriberAccountInfo.getLastName().toUpperCase());
			trans.setBillingFirstName(subscriberAccountInfo.getFirstName().toUpperCase());
			trans.setBillingFirmName(subscriberAccountInfo.getFirmName().toUpperCase());
			trans.setBillingHomePhone(subscriberAccountInfo.getHomePhone());
			trans.setBillingBusinessPhone(subscriberAccountInfo.getBillingBusinessPhone());

			trans.setBillingAddress1(subscriberAccountInfo.getBillingAddress1().toUpperCase());
			// don't use addladdress 2
			trans.setBillingAddress2(subscriberAccountInfo.getBillingAddress2().toUpperCase());
			trans.setBillingUnitNum(subscriberAccountInfo.getUnitNum());
			trans.setBillingHalfUnitNum(subscriberAccountInfo.getHalfUnitNum());
			trans.setBillingStreetDir(subscriberAccountInfo.getStreetDir().toUpperCase());
			trans.setBillingStreetName(subscriberAccountInfo.getStreetName().toUpperCase());
			trans.setBillingStreetType(subscriberAccountInfo.getStreetType().toUpperCase());
			trans.setBillingStreetPostDir(subscriberAccountInfo.getStreetPostDir().toUpperCase());
			trans.setBillingSubUnitCode(subscriberAccountInfo.getSubUnitCode());
			trans.setBillingSubUnitNum(subscriberAccountInfo.getSubUnitNum());
			trans.setBillingCity(subscriberAccountInfo.getCity().toUpperCase());
			trans.setBillingState(subscriberAccountInfo.getState().toUpperCase());
			trans.setBillingZip5(subscriberAccountInfo.getZip());
		}

		// defalault to not a one time bill may be cleared if not CC payment
		trans.setOneTimeBill("");

		if (isGannettUnit) {
			trans.setOneTimeBill("N");
		}

		/*
		 * 42 CCType, CCNum, CCExpireDt, EffDate1, PromoCode, Comment,
		 */
		// if it's a credit card payment type
		CreditCardPaymentMethodIntf ccPayment = (CreditCardPaymentMethodIntf) paymentInfo;

		// set the payment type
		trans.setCreditCardType(paymentInfo.getPaymentType().getType());

		// clear bill charged indicator
		trans.setBillChargePd("");

		if (ccPayment.wasAuthorizedSuccessfully()) {
			trans.setCreditCardAuthCode(ccPayment.getAuthCode());
			trans.setCreditCardAuthDate(ccPayment.getAuthDate());
			// if a Gannett Unit always set the credit card to batch process
			// (just the way it's done ???)
			trans.setCreditCardBatchProcess("N");
		} else {
			trans.setCreditCardBatchProcess("Y");
		}

		trans.setCreditCardNumber(paymentInfo.getAccountNumberForPersistence());
		trans.setCreditCardExpirationDate(ccPayment.getExpirationDateForPersistence());

		// get current date
		java.util.Date startingDate = new java.util.Date();
		String currDate = new SimpleDateFormat("yyyyMMdd").format(startingDate);
		trans.setEffectiveDate(currDate);

		// comments are blanked out
		trans.setComment("");

		/*
		 * 48 TtlMktCov, SubsAmount, RejComm, AutoRegComm, Code1ErrCode, PrintFlag, Filler, SrcOrdCode, ContestCode, DelPref, 58
		 * OneTimeBill, RateCode, PerpCCFlag, BillChargePd, NewAddlAddr1, NewAddlAddr2, CCAuthCode, 65 CCAuthDate, EmailType,
		 * EmailAddress, BadSubAddress, BadTrnGftAddress, CCBatchProc, 71 Status, NumPaper, PayAmount, Premium, ClubNum, Trandate
		 */

		// ttlmktcov Customer wants more infomation - we don't use
		trans.setTtlMktCov("");

		// following two items not used
		trans.setRejComment("");
		trans.setAutoRegComm("");

		// not used
		trans.setPrintFlag("");

		// default orders to carrier delivery preference
		trans.setDeliveryPreference("C");

		// not used
		trans.setNewAddlAddr1("");
		trans.setNewAddlAddr2("");

		/*
		 * 70 CCBatchProc, 71 Status, NumPaper, PayAmount, Premium, ClubNum, Trandate
		 */
		/*
		 * BadTrnGftAddress, CCBatchProc, 71 Status, NumPaper, PayAmount, Premium, ClubNum, Trandate
		 */

		trans.setPerpCCFlag("");
		if (trans.getTransactionType().equals("11")) {
			trans.setPerpCCFlag("P");
		}

		trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		return trans;
	}

	/**
	 * 
	 * @param subscriptionOrder
	 *            - The subscription order item
	 * @param deliveryContact
	 *            - Where it should be delivered to
	 * @return - A XTRNTDWNLD transaction transfer object that is ready for persistence
	 * @throws UsatException
	 */
	public static ExtranetSubscriberTransactionIntf createChangeDeliveryAddressTransaction(
			SubscriberAccountIntf subscriberAccountInfo, ContactBO deliveryContact, String newDeliveryMethod, Date newStartDate,
			boolean transType3) throws UsatException {
		SubscriberTransactionTO trans = new SubscriberTransactionTO();

		if (deliveryContact == null) {
			throw new UsatException(
					"SubscriptionTransactionFactory():createChangeDeliveryTransaction: Invalid arguments, cannot create subscription transaction.");
		}

		if (!deliveryContact.validateContact() && !UsaTodayConstants.ALLOW_FAILED_ADDRESSES) {
			throw new UsatException("The delivery contact is not valid");
		}

		boolean isGannettUnit = false;

		// Populate current customer information
		trans.setPubCode(subscriberAccountInfo.getPubCode());
		trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		trans.setLastName(subscriberAccountInfo.getDeliveryContact().getLastName().toUpperCase());
		trans.setFirstName(subscriberAccountInfo.getDeliveryContact().getFirstName().toUpperCase());
		trans.setFirmName(subscriberAccountInfo.getDeliveryContact().getFirmName().toUpperCase());
		trans.setHomePhone(subscriberAccountInfo.getDeliveryContact().getHomePhone());
		trans.setBusinessPhone(subscriberAccountInfo.getDeliveryContact().getBusinessPhone());
		trans.setAddAddr1(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getAddress2()); // double
		// addr 2 not used
		trans.setAddAddr2("");
		trans.setUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getUnitNum());
		trans.setHalfUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getHalfUnitNum());
		trans.setStreetDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetDir().toUpperCase());
		trans.setStreetName(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetName().toUpperCase());
		trans.setStreetType(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetType().toUpperCase());
		trans.setStreetPostDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetPostDir().toUpperCase());
		trans.setSubUnitCode(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitCode());
		trans.setSubUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitNum());
		trans.setCity(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getCity().toUpperCase());
		trans.setState(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getState().toUpperCase());
		trans.setZip5(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getZip());

		trans.setAccountNum(subscriberAccountInfo.getAccountNumber());

		trans.setNewLastName(deliveryContact.getLastName().toUpperCase());
		trans.setNewFirstName(deliveryContact.getFirstName().toUpperCase());
		trans.setNewFirmName(deliveryContact.getFirmName().toUpperCase());
		trans.setNewPhoneNum(deliveryContact.getHomePhone());
		trans.setNewBusPhoneNum(deliveryContact.getBusinessPhone());
		if (deliveryContact.isAddressVerified()) {

			if (deliveryContact.getPersistentAddress().getAddress2() != null) {
				trans.setNewAddlAddr1(deliveryContact.getPersistentAddress().getAddress2().toUpperCase());
			}

			// addr 2 not used
			trans.setNewAddlAddr2("");
			trans.setNewUnitNum(deliveryContact.getPersistentAddress().getUnitNum());
			trans.setNewHalfUnitNum(deliveryContact.getPersistentAddress().getHalfUnitNum());
			trans.setNewStreetDir(deliveryContact.getPersistentAddress().getStreetDir().toUpperCase());
			trans.setNewStreetName(deliveryContact.getPersistentAddress().getStreetName().toUpperCase());
			if (deliveryContact.getPersistentAddress().getStreetType() != null) {
				trans.setNewStreetType(deliveryContact.getPersistentAddress().getStreetType().toUpperCase());
			}
			if (deliveryContact.getPersistentAddress().getStreetPostDir() != null) {
				trans.setNewStreetPostDir(deliveryContact.getPersistentAddress().getStreetPostDir().toUpperCase());
			}
			if (deliveryContact.getPersistentAddress().getSubUnitCode() != null) {
				trans.setNewSubUnitCode(deliveryContact.getPersistentAddress().getSubUnitCode());
			}
			if (deliveryContact.getPersistentAddress().getSubUnitNum() != null) {
				trans.setNewSubUnitNum(deliveryContact.getPersistentAddress().getSubUnitNum());
			}
			trans.setNewCity(deliveryContact.getPersistentAddress().getCity().toUpperCase());
			trans.setNewState(deliveryContact.getPersistentAddress().getState().toUpperCase());
			trans.setNewZip(deliveryContact.getPersistentAddress().getZip());

			if (deliveryContact.getPersistentAddress().isGUIAddress() && !subscriberAccountInfo.getProduct().isElectronicDelivery()) {
				trans.setStatus("G");
				isGannettUnit = true; // used below
			}
		} else {
			trans.setNewCity(deliveryContact.getUIAddress().getCity().toUpperCase());
			trans.setNewState(deliveryContact.getUIAddress().getState());
			trans.setNewZip(deliveryContact.getUIAddress().getZip());
			// Do we need to set normal delivery fields when not verified????
			String tempStreet1 = "";
			if (deliveryContact.getUIAddress().getAddress1() != null) {
				tempStreet1 = deliveryContact.getUIAddress().getAddress1().trim().toUpperCase();
			}

			String tempStreet2 = "";
			if (deliveryContact.getUIAddress().getAptSuite() != null) {
				tempStreet2 = deliveryContact.getUIAddress().getAptSuite().trim().toUpperCase();
			}

			if ((tempStreet1.length() + tempStreet2.length()) > 54) {
				if (tempStreet1.length() > 27) {
					tempStreet1 = tempStreet1.substring(0, 27);
				}
				if (tempStreet2.length() > 27) {
					tempStreet2 = tempStreet2.substring(0, 27);
				}
			}
			String temp = tempStreet1 + "&&" + tempStreet2;
			if (temp.length() > 56) {
				temp = temp.substring(0, 56);
			}
			if (deliveryContact.getUiAddress().getAddress2() != null) {
				trans.setNewAddlAddr1(deliveryContact.getUIAddress().getAddress2());
			}

			trans.setBadSubAddress(temp);
		}
		// Check for transaction type
		if (transType3) {
			trans.setTransactionRecType("03");
			// hard code transaction type
			trans.setTransactionType("T");
			// hard code transaction code
			trans.setTransactionCode("RN");
		} else {
			trans.setTransactionRecType("04");
			trans.setTransactionType("");
			trans.setTransactionCode("");
		}

		// default to not a one time bill may be cleared if not CC payment
		trans.setOneTimeBill("");
		if (isGannettUnit) {
			trans.setOneTimeBill("N");
		}
		trans.setEffectiveDate(new SimpleDateFormat("yyyyMMdd").format(newStartDate));

		try {
			if (!deliveryContact.getUIAddress().isValidated() || deliveryContact.getPersistentAddress().isMilitaryAddress()) {
				trans.setCode1ErrorCode("B");
			}
		} catch (Exception e) {
			// if for some reason we can't determine if addresses were validated
			// then default to B
			trans.setCode1ErrorCode("B");
		}

		// default orders to delivery method
		trans.setDeliveryPreference(newDeliveryMethod);

		trans.setEmailAddress(subscriberAccountInfo.getDeliveryContact().getEmailAddress());

		trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		return trans;
	}

	/**
	 * 
	 * @param subscriptionOrder
	 *            - The subscription order item
	 * @param deliveryContact
	 *            - Where it should be delivered to
	 * @param billingContact
	 *            - who is paying
	 * @param paymentInfo
	 *            - How they are paying
	 * @return - A XTRNTDWNLD transaction transfer object that is ready for persistence
	 * @throws UsatException
	 */
	public static ExtranetSubscriberTransactionIntf createCustomerInfoTransaction(String transRecType, String transType,
			String transCode, String comment, SubscriberAccountIntf subscriberAccountInfo) throws UsatException {
		SubscriberTransactionTO trans = new SubscriberTransactionTO();

		if (subscriberAccountInfo == null) {
			throw new UsatException(
					"SubscriptionTransactionFactory():createSubscriptionTransaction: Invalid arguments, cannot create customer information transaction.");
		}

		boolean isGannettUnit = false;
		trans.setPubCode(subscriberAccountInfo.getPubCode());
		trans.setTransactionRecType(transRecType);
		trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		trans.setLastName(subscriberAccountInfo.getDeliveryContact().getLastName().toUpperCase());
		trans.setFirstName(subscriberAccountInfo.getDeliveryContact().getFirstName().toUpperCase());
		trans.setFirmName(subscriberAccountInfo.getDeliveryContact().getFirmName().toUpperCase());
		trans.setHomePhone(subscriberAccountInfo.getDeliveryContact().getHomePhone());
		trans.setBusinessPhone(subscriberAccountInfo.getDeliveryContact().getBusinessPhone());
		trans.setAddAddr1(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getAddress2()); // double
		// addr 2 not used
		trans.setAddAddr2("");
		trans.setUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getUnitNum());
		trans.setHalfUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getHalfUnitNum());
		trans.setStreetDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetDir().toUpperCase());
		trans.setStreetName(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetName().toUpperCase());
		trans.setStreetType(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetType().toUpperCase());
		trans.setStreetPostDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetPostDir().toUpperCase());
		trans.setSubUnitCode(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitCode());
		trans.setSubUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitNum());
		trans.setCity(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getCity().toUpperCase());
		trans.setState(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getState().toUpperCase());
		trans.setZip5(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getZip());

		// passed transaction type
		trans.setTransactionType(transType);
		// passed transaction code
		trans.setTransactionCode(transCode);
		trans.setAccountNum(subscriberAccountInfo.getAccountNumber());

		// defalault to not a one time bill may be cleared if not CC payment
		trans.setOneTimeBill("");

		if (isGannettUnit) {
			trans.setOneTimeBill("N");
		}

		// set effective date to the subscription expiration date
		// SimpleDateFormat updatedate = new SimpleDateFormat ("yyyyMMdd");
		// trans.setEffectiveDate(updatedate.format(subscriberAccountInfo.getExpirationDate()));
		// set effective date back to the system date
		trans.setEffectiveDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		// comments are blanked out
		trans.setComment(comment);

		trans.setPerpCCFlag("");
		if (trans.getTransactionType().equals("11")) {
			trans.setPerpCCFlag("P");
		}

		return trans;
	}

	/**
	 * @param transRecType
	 *            - 05 Transaction record type
	 * @param transType
	 *            - 9 Transaction type
	 * @param transCode
	 *            - C... Transaction code
	 * @param comment
	 *            - Customer comment
	 * @param subscriber
	 *            Account Info - The subscription account information
	 * @return - A XTRNTDWNLD transaction transfer object that is ready for persistence
	 * @throws UsatException
	 */
	public static ExtranetSubscriberTransactionIntf createSubscriptionCancelTransaction(String transRecType, String transType,
			String transCode, String comment, SubscriberAccountIntf subscriberAccountInfo) throws UsatException {
		SubscriberTransactionTO trans = new SubscriberTransactionTO();

		if (subscriberAccountInfo == null) {
			throw new UsatException(
					"SubscriptionTransactionFactory():createSubscriptionTransaction: Invalid arguments, cannot create stop subscription transaction.");
		}

		boolean isGannettUnit = false;
		trans.setPubCode(subscriberAccountInfo.getPubCode());
		trans.setTransactionRecType(transRecType);
		trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		trans.setLastName(subscriberAccountInfo.getDeliveryContact().getLastName().toUpperCase());
		trans.setFirstName(subscriberAccountInfo.getDeliveryContact().getFirstName().toUpperCase());
		trans.setFirmName(subscriberAccountInfo.getDeliveryContact().getFirmName().toUpperCase());
		trans.setHomePhone(subscriberAccountInfo.getDeliveryContact().getHomePhone());
		trans.setBusinessPhone(subscriberAccountInfo.getDeliveryContact().getBusinessPhone());
		trans.setAddAddr1(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getAddress2()); // double
		// addr 2 not used
		trans.setAddAddr2("");
		trans.setUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getUnitNum());
		trans.setHalfUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getHalfUnitNum());
		trans.setStreetDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetDir().toUpperCase());
		trans.setStreetName(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetName().toUpperCase());
		trans.setStreetType(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetType().toUpperCase());
		trans.setStreetPostDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetPostDir().toUpperCase());
		trans.setSubUnitCode(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitCode());
		trans.setSubUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitNum());
		trans.setCity(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getCity().toUpperCase());
		trans.setState(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getState().toUpperCase());
		trans.setZip5(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getZip());

		// passed transaction type
		trans.setTransactionType(transType);
		// passed transaction code
		trans.setTransactionCode(transCode);
		trans.setAccountNum(subscriberAccountInfo.getAccountNumber());

		// defalault to not a one time bill may be cleared if not CC payment
		trans.setOneTimeBill("");

		if (isGannettUnit) {
			trans.setOneTimeBill("N");
		}

		// set effective date to the subscription expiration date
		SimpleDateFormat updatedate = new SimpleDateFormat("yyyyMMdd");
		trans.setEffectiveDate(updatedate.format(subscriberAccountInfo.getExpirationDate()));
		// trans.setEffectiveDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());
		// set comment
		trans.setComment(comment);

		// ttlmktcov Customer wants more infomation - we don't use
		trans.setTtlMktCov("");

		// following two items not used
		trans.setRejComment("");
		trans.setAutoRegComm("");

		// not used
		trans.setPrintFlag("");

		// not used
		trans.setNewAddlAddr1("");
		trans.setNewAddlAddr2("");

		trans.setPerpCCFlag("");
		if (trans.getTransactionType().equals("11")) {
			trans.setPerpCCFlag("P");
		}

		return trans;
	}

	/**
	 * 
	 * @param subscriptionOrder
	 *            - The subscription order item
	 * @param deliveryContact
	 *            - Where it should be delivered to
	 * @param billingContact
	 *            - who is paying
	 * @param paymentInfo
	 *            - How they are paying
	 * @return - A XTRNTDWNLD transaction transfer object that is ready for persistence
	 * @throws UsatException
	 */
	public static ExtranetSubscriberTransactionIntf createSubscriptionTransaction(SubscriptionOrderItemIntf subscriptionOrder,
			ContactIntf deliveryContact, ContactIntf billingContact, PaymentMethodIntf paymentInfo) throws UsatException {
		SubscriberTransactionTO trans = new SubscriberTransactionTO();

		if (subscriptionOrder == null || deliveryContact == null || paymentInfo == null) {
			throw new UsatException(
					"SubscriptionTransactionFactory():createSubscriptionTransaction: Invalid arguments, cannot create subscription transaction.");
		}

		if (!deliveryContact.validateContact() && !UsaTodayConstants.ALLOW_FAILED_ADDRESSES) {
			throw new UsatException("The delivery contact is not valid");
		}

		if (billingContact != null && (!billingContact.validateContact() && !UsaTodayConstants.ALLOW_FAILED_ADDRESSES)) {
			throw new UsatException("The billing contact is not valid.");
		}

		try {

			boolean isGannettUnit = false;

			/*
			 * "1 PubCode, TranRecTyp, LastName, FirstName, FirmName, AddAddr1, AddAddr2, 8 UnitNum, HalfUnitNum, StreetDir,
			 * StreetName, StreetTyp, StreetPstDir, SubUnitCode, 15 SubUnitNum, City, State, Zip5, HomePhone, BusPhone, TranType,
			 * TranCode, BillLastName,
			 */
			trans.setPubCode(subscriptionOrder.getProduct().getProductCode());
			// New subscription type
			trans.setTransactionRecType("01");
			trans.setAccountNum("");
			trans.setLastName(deliveryContact.getLastName().toUpperCase());
			trans.setFirstName(deliveryContact.getFirstName().toUpperCase());
			trans.setFirmName(deliveryContact.getFirmName().toUpperCase());
			trans.setHomePhone(deliveryContact.getHomePhone());
			trans.setBusinessPhone(deliveryContact.getBusinessPhone());
			if (deliveryContact.isAddressVerified()) {

				if (deliveryContact.getPersistentAddress().getAddress2() != null) {
					trans.setAddAddr1(deliveryContact.getPersistentAddress().getAddress2().toUpperCase()); // double
				} // check
					// this
					// addr 2 not used
				trans.setAddAddr2("");
				trans.setUnitNum(deliveryContact.getPersistentAddress().getUnitNum());
				trans.setHalfUnitNum(deliveryContact.getPersistentAddress().getHalfUnitNum());
				trans.setStreetDir(deliveryContact.getPersistentAddress().getStreetDir().toUpperCase());
				trans.setStreetName(deliveryContact.getPersistentAddress().getStreetName().toUpperCase());
				if (deliveryContact.getPersistentAddress().getStreetType() != null) {
					trans.setStreetType(deliveryContact.getPersistentAddress().getStreetType().toUpperCase());
				}
				if (deliveryContact.getPersistentAddress().getStreetPostDir() != null) {
					trans.setStreetPostDir(deliveryContact.getPersistentAddress().getStreetPostDir().toUpperCase());
				}
				if (deliveryContact.getPersistentAddress().getSubUnitCode() != null) {
					trans.setSubUnitCode(deliveryContact.getPersistentAddress().getSubUnitCode());
				}
				if (deliveryContact.getPersistentAddress().getSubUnitNum() != null) {
					trans.setSubUnitNum(deliveryContact.getPersistentAddress().getSubUnitNum());
				}
				trans.setCity(deliveryContact.getPersistentAddress().getCity().toUpperCase());
				trans.setState(deliveryContact.getPersistentAddress().getState().toUpperCase());
				trans.setZip5(deliveryContact.getPersistentAddress().getZip());

				GUIZipCodeManager guiZipMngr = GUIZipCodeManager.getInstance();

				if (guiZipMngr.isGUIZipForPub(subscriptionOrder.getProduct().getProductCode(), deliveryContact
						.getPersistentAddress().getZip())) {
					trans.setStatus("G");
					isGannettUnit = true; // used below
				}
			} else {
				trans.setCity(deliveryContact.getUIAddress().getCity().toUpperCase());
				trans.setState(deliveryContact.getUIAddress().getState());
				trans.setZip5(deliveryContact.getUIAddress().getZip());
				// Do we need to set normal delivery fields when not verified????
				String tempStreet1 = "";
				if (deliveryContact.getUIAddress().getAddress1() != null) {
					tempStreet1 = deliveryContact.getUIAddress().getAddress1().trim().toUpperCase();
				}

				String tempStreet2 = "";
				if (deliveryContact.getUIAddress().getAptSuite() != null) {
					tempStreet2 = deliveryContact.getUIAddress().getAptSuite().trim().toUpperCase();
				}

				if ((tempStreet1.length() + tempStreet2.length()) > 54) {
					if (tempStreet1.length() > 27) {
						tempStreet1 = tempStreet1.substring(0, 27);
					}
					if (tempStreet2.length() > 27) {
						tempStreet2 = tempStreet2.substring(0, 27);
					}
				}
				String temp = tempStreet1 + "&&" + tempStreet2;
				if (temp.length() > 56) {
					temp = temp.substring(0, 56);
				}

				trans.setBadSubAddress(temp);
			}
			// hard code transaction type
			trans.setTransactionType("8");
			// hard code transaction code
			trans.setTransactionCode("NS");
			/*
			 * 24 BillFirstName, BillFirmName, BillAddr1, BillAddr2, BillUnitNum, BillHalfUnitNum, 30 BillStreetDir, BillStreetName,
			 * BillStreetType, BillStreetPostDir, BillSubUnitCode, 35 BillSubUnitNum, BillCity, BillState, BillZip5, BillHomePhone,
			 * BillBusPhone, SubsDur,
			 */
			if (billingContact != null) {
				trans.setBillingLastName(billingContact.getLastName().toUpperCase());
				trans.setBillingFirstName(billingContact.getFirstName().toUpperCase());
				trans.setBillingFirmName(billingContact.getFirmName().toUpperCase());
				trans.setBillingHomePhone(billingContact.getHomePhone());
				trans.setBillingBusinessPhone(billingContact.getBusinessPhone());

				if (billingContact.isAddressVerified()) {
					trans.setBillingAddress1(billingContact.getPersistentAddress().getAddress2().toUpperCase());
					// don't use addladdress 2
					trans.setBillingAddress2("");
					trans.setBillingUnitNum(billingContact.getPersistentAddress().getUnitNum());
					trans.setBillingHalfUnitNum(billingContact.getPersistentAddress().getHalfUnitNum());
					trans.setBillingStreetDir(billingContact.getPersistentAddress().getStreetDir().toUpperCase());
					trans.setBillingStreetName(billingContact.getPersistentAddress().getStreetName().toUpperCase());
					if (billingContact.getPersistentAddress().getStreetType() != null) {
						trans.setBillingStreetType(billingContact.getPersistentAddress().getStreetType().toUpperCase());
					}
					if (billingContact.getPersistentAddress().getStreetPostDir() != null) {
						trans.setBillingStreetPostDir(billingContact.getPersistentAddress().getStreetPostDir().toUpperCase());
					}
					if (billingContact.getPersistentAddress().getSubUnitCode() != null) {
						trans.setBillingSubUnitCode(billingContact.getPersistentAddress().getSubUnitCode());
					}
					if (billingContact.getPersistentAddress().getSubUnitNum() != null) {
						trans.setBillingSubUnitNum(billingContact.getPersistentAddress().getSubUnitNum());
					}
					trans.setBillingCity(billingContact.getPersistentAddress().getCity().toUpperCase());
					trans.setBillingState(billingContact.getPersistentAddress().getState().toUpperCase());
					trans.setBillingZip5(billingContact.getPersistentAddress().getZip());
				} else {
					trans.setBillingCity(billingContact.getUIAddress().getCity().toUpperCase());
					trans.setBillingState(billingContact.getUIAddress().getState().toUpperCase());
					trans.setBillingZip5(billingContact.getUIAddress().getZip());
					// Do we need to set normal delivery fields when not
					// verified????
					String tempStreet1 = "";
					if (billingContact.getUIAddress().getAddress1() != null) {
						tempStreet1 = billingContact.getUIAddress().getAddress1().trim().toUpperCase();
					}

					String tempStreet2 = "";
					if (billingContact.getUIAddress().getAptSuite() != null) {
						tempStreet2 = billingContact.getUIAddress().getAptSuite().trim().toUpperCase();
					}

					if ((tempStreet1.length() + tempStreet2.length()) > 54) {
						if (tempStreet1.length() > 27) {
							tempStreet1 = tempStreet1.substring(0, 27);
						}
						if (tempStreet2.length() > 27) {
							tempStreet2 = tempStreet2.substring(0, 27);
						}
					}
					String temp = tempStreet1 + "&&" + tempStreet2;
					if (temp.length() > 56) {
						temp = temp.substring(0, 56);
					}

					trans.setBadTrnGftAddress(temp);
				}
			}

			SubscriptionProductBO product = null;
			try {
				product = (SubscriptionProductBO) subscriptionOrder.getProduct();
			} catch (ClassCastException ce) {
				throw new UsatException("Invalid Product associated with Subscription order: " + ce.getMessage());
			}
			trans.setSubscriptionDuration(product.getAppliedTerm().getDuration());

			// defalault to not a one time bill may be cleared if not CC payment
			trans.setOneTimeBill("");
			if (subscriptionOrder.isGiftItem()) {
				if (subscriptionOrder.isOneTimeBill()) {
					trans.setOneTimeBill("Y");
				} else {
					trans.setOneTimeBill("N");
				}
			}
			if (isGannettUnit) {
				trans.setOneTimeBill("N");
			}

			/*
			 * 42 CCType, CCNum, CCExpireDt, EffDate1, PromoCode, Comment,
			 */
			// if it's a credit card payment type
			if (paymentInfo instanceof CreditCardPaymentMethodIntf) {

				CreditCardPaymentMethodIntf ccPayment = (CreditCardPaymentMethodIntf) paymentInfo;

				// set the payment type
				trans.setCreditCardType(paymentInfo.getPaymentType().getType());

				// clear bill charged indicator
				trans.setBillChargePd("");

				if (ccPayment.wasAuthorizedSuccessfully()) {
					trans.setCreditCardAuthCode(ccPayment.getAuthCode());
					trans.setCreditCardAuthDate(ccPayment.getAuthDate());
					// if a Gannett Unit always set the credit card to batch process
					// (just the way it's done ???)
					if (isGannettUnit) {
						trans.setCreditCardBatchProcess("Y");
					} else {
						trans.setCreditCardBatchProcess("N");
					}
				} else {
					trans.setCreditCardBatchProcess("Y");
				}

				trans.setCreditCardNumber(paymentInfo.getAccountNumberForPersistence());
				trans.setCreditCardExpirationDate(ccPayment.getExpirationDateForPersistence());

			} else { // Invoice /Bill me
				trans.setBillChargePd("B");
				trans.setOneTimeBill("");
				trans.setPerpCCFlag("");
				trans.setCreditCardType("");
				trans.setCreditCardBatchProcess("");
			}

			trans.setEffectiveDate(subscriptionOrder.getStartDate());

			KeyCodeParser parser = new KeyCodeParser(subscriptionOrder.getKeyCode());

			trans.setPromoCode(parser.getPromoCode());
			trans.setContestCode(parser.getContestCode());
			trans.setSrcOrdCode(parser.getSourceCode());

			// comments are blanked out
			trans.setComment("");

			/*
			 * 48 TtlMktCov, SubsAmount, RejComm, AutoRegComm, Code1ErrCode, PrintFlag, Filler, SrcOrdCode, ContestCode, DelPref, 58
			 * OneTimeBill, RateCode, PerpCCFlag, BillChargePd, NewAddlAddr1, NewAddlAddr2, CCAuthCode, 65 CCAuthDate, EmailType,
			 * EmailAddress, BadSubAddress, BadTrnGftAddress, CCBatchProc, 71 Status, NumPaper, PayAmount, Premium, ClubNum,
			 * Trandate
			 */

			// ttlmktcov Customer wants more infomation - we don't use
			trans.setTtlMktCov("");

			trans.setSubscriptionAmount(subscriptionOrder.getSubscriptionAmountForPersistence());

			// following two items not used
			trans.setRejComment("");
			trans.setAutoRegComm("");

			try {
				if (!deliveryContact.getUIAddress().isValidated()
						|| deliveryContact.getPersistentAddress().isMilitaryAddress()
						|| (billingContact != null && (billingContact.getPersistentAddress().isMilitaryAddress() || !billingContact
								.getUIAddress().isValidated()))) {
					trans.setCode1ErrorCode("B");
				}
			} catch (Exception e) {
				// if for some reason we can't determin if addresses were validated
				// then default to B
				trans.setCode1ErrorCode("B");
			}

			// not used
			trans.setPrintFlag("");

			// store order id in filler
			trans.setFiller(subscriptionOrder.getOrderID());

			// default orders to delivery method
			trans.setDeliveryPreference(subscriptionOrder.getDeliveryMethod());

			trans.setRateCode(subscriptionOrder.getSelectedTerm().getRelatedRateCode());
			// not used
			trans.setNewAddlAddr1("");
			trans.setNewAddlAddr2("");

			/*
			 * 70 CCBatchProc, 71 Status, NumPaper, PayAmount, Premium, ClubNum, Trandate
			 */
			if (subscriptionOrder.isGiftItem()) {
				trans.setEmailType("G");
			} else {
				trans.setEmailType("N");
			}

			trans.setEmailAddress(subscriptionOrder.getDeliveryEmailAddress());

			/*
			 * BadTrnGftAddress, CCBatchProc, 71 Status, NumPaper, PayAmount, Premium, ClubNum, Trandate
			 */

			if (subscriptionOrder.isChoosingEZPay() && !isGannettUnit) {
				trans.setPerpCCFlag("P");
			}

			int numPapers = subscriptionOrder.getQuantity();
			try {
				trans.setNumPaper(String.valueOf(numPapers));
			} catch (Exception e) {
				// goto one on exception
				trans.setNumPaper("1");
			}

			trans.setPayAmount(subscriptionOrder.getSubscriptionTotalAmountForPersistence());

			trans.setPremium(subscriptionOrder.getPremiumCode());

			trans.setClubNum(subscriptionOrder.getClubNumber());

			trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		} catch (UsatException ue) {
			// simply re-throw
			throw ue;
		} catch (Exception e) {
			// convert to to usatException
			UsatException ue = new UsatException(e);
			throw ue;
		}
		return trans;
	}

	/**
	 * 
	 * @param customer
	 * @param account
	 * @param paymentInfo
	 * @param renewalOrder
	 * @return
	 * @throws UsatException
	 */
	public static ExtranetSubscriberTransactionIntf createRenewalTransaction(CustomerIntf customer, SubscriberAccountIntf account,
			PaymentMethodIntf paymentInfo, RenewalOrderItemIntf renewalOrder) throws UsatException {

		SubscriberTransactionTO trans = new SubscriberTransactionTO();

		try {
			trans.setPubCode(account.getPubCode());
			trans.setTransactionRecType("02");

			ContactIntf contact = account.getDeliveryContact();
			trans.setLastName(contact.getLastName());
			trans.setFirstName(contact.getFirstName());
			trans.setFirmName(contact.getFirmName());

			if (contact.getPersistentAddress().getAddress2() != null) {
				trans.setAddAddr1(contact.getPersistentAddress().getAddress2().toUpperCase()); // double
			}

			trans.setAddAddr2("");
			trans.setUnitNum(contact.getPersistentAddress().getUnitNum());
			trans.setHalfUnitNum(contact.getPersistentAddress().getHalfUnitNum());
			trans.setStreetDir(contact.getPersistentAddress().getStreetDir().toUpperCase());
			trans.setStreetName(contact.getPersistentAddress().getStreetName().toUpperCase());
			if (contact.getPersistentAddress().getStreetType() != null) {
				trans.setStreetType(contact.getPersistentAddress().getStreetType().toUpperCase());
			}
			if (contact.getPersistentAddress().getStreetPostDir() != null) {
				trans.setStreetPostDir(contact.getPersistentAddress().getStreetPostDir().toUpperCase());
			}
			if (contact.getPersistentAddress().getSubUnitCode() != null) {
				trans.setSubUnitCode(contact.getPersistentAddress().getSubUnitCode());
			}
			if (contact.getPersistentAddress().getSubUnitNum() != null) {
				trans.setSubUnitNum(contact.getPersistentAddress().getSubUnitNum());
			}
			trans.setCity(contact.getPersistentAddress().getCity());
			trans.setState(contact.getPersistentAddress().getState());
			trans.setZip5(contact.getPersistentAddress().getZip());

			trans.setHomePhone(contact.getHomePhone());
			trans.setBusinessPhone(contact.getBusinessPhone());

			trans.setTransactionType("");
			trans.setTransactionCode("");

			// set up billing info if it exists
			// we don't send billing since we can't change billing on renewal transactions and we only want it if it changed.
			// contact = account.getBillingContact();
			// if (!account.isBillingSameAsDelivery()) {
			// trans.setBillingLastName(contact.getLastName());
			// trans.setBillingFirstName(contact.getFirstName());
			// trans.setBillingFirmName(contact.getFirmName());
			// trans.setBillingAddress1(contact.getPersistentAddress().getAddress2());
			// trans.setBillingUnitNum(contact.getPersistentAddress().getUnitNum());
			// trans.setBillingHalfUnitNum(contact.getPersistentAddress().getHalfUnitNum());
			// / trans.setBillingStreetDir(contact.getPersistentAddress().getStreetDir());
			// trans.setBillingStreetName(contact.getPersistentAddress().getStreetName());
			// trans.setBillingStreetType(contact.getPersistentAddress().getStreetType());
			// trans.setBillingStreetPostDir(contact.getPersistentAddress().getStreetPostDir());
			// trans.setBillingSubUnitCode(contact.getPersistentAddress().getSubUnitCode());
			// trans.setBillingSubUnitNum(contact.getPersistentAddress().getSubUnitNum());
			// trans.setBillingCity(contact.getPersistentAddress().getCity());
			// trans.setBillingState(contact.getPersistentAddress().getState());
			// trans.setBillingZip5(contact.getPersistentAddress().getZip());

			// trans.setBillingHomePhone(contact.getHomePhone());
			// trans.setBillingBusinessPhone(contact.getBusinessPhone());
			// }
			trans.setBillingLastName("");
			trans.setBillingFirstName("");
			trans.setBillingFirmName("");
			trans.setBillingAddress1("");
			trans.setBillingUnitNum("");
			trans.setBillingHalfUnitNum("");
			trans.setBillingStreetDir("");
			trans.setBillingStreetName("");
			trans.setBillingStreetType("");
			trans.setBillingStreetPostDir("");
			trans.setBillingSubUnitCode("");
			trans.setBillingSubUnitNum("");
			trans.setBillingCity("");
			trans.setBillingState("");
			trans.setBillingZip5("");
			trans.setBillingHomePhone("");
			trans.setBillingBusinessPhone("");

			SubscriptionProductBO product = null;
			try {
				product = (SubscriptionProductBO) renewalOrder.getProduct();
			} catch (ClassCastException ce) {
				throw new UsatException("Invalid Product associated with Subscription order: " + ce.getMessage());
			}

			// set term selected
			trans.setSubscriptionDuration(product.getAppliedTerm().getDuration());

			CreditCardPaymentMethodIntf ccPayment = (CreditCardPaymentMethodIntf) paymentInfo;

			// set the payment type
			trans.setCreditCardType(paymentInfo.getPaymentType().getType());

			// clear bill charged indicator
			trans.setBillChargePd("");

			if (ccPayment.wasAuthorizedSuccessfully()) {
				trans.setCreditCardAuthCode(ccPayment.getAuthCode());
				trans.setCreditCardAuthDate(ccPayment.getAuthDate());
				trans.setCreditCardBatchProcess("N");
			} else {
				trans.setCreditCardBatchProcess("Y");
			}

			trans.setCreditCardNumber(paymentInfo.getAccountNumberForPersistence());
			trans.setCreditCardExpirationDate(ccPayment.getExpirationDateForPersistence());

			// promotion code
			KeyCodeParser parser = new KeyCodeParser();
			parser.setKeyCode(renewalOrder.getKeyCode());
			trans.setPromoCode(parser.getPromoCode());
			trans.setSrcOrdCode(parser.getSourceCode());
			trans.setContestCode(parser.getContestCode());

			trans.setSubscriptionAmount(renewalOrder.getSubscriptionAmountForPersistence());

			trans.setAccountNum(account.getAccountNumber());

			trans.setFiller(renewalOrder.getOrderID());

			trans.setRateCode(renewalOrder.getSelectedTerm().getPiaRateCode());

			// if account was a one time bill originally, clear gift payer information.
			if (account.isOneTimeBill()) {
				trans.setDeleteGiftPayer("D");
			}

			trans.setOneTimeBill("N");

			if (renewalOrder.isChoosingEZPay()) {
				trans.setPerpCCFlag("P");
			}

			int numPapers = renewalOrder.getQuantity();
			try {
				trans.setNumPaper(String.valueOf(numPapers));
			} catch (Exception e) {
				// goto one on exception
				trans.setNumPaper("1");
			}

			// we don't support asking about total market coverage on web anymore
			trans.setTtlMktCov("E");

			trans.setPayAmount(renewalOrder.getSubscriptionTotalAmountForPersistence());

			trans.setPremium(renewalOrder.getPremiumCode());

			trans.setClubNum(renewalOrder.getClubNumber());

			trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

			// renewal tracking code goes in micrtrancode
			trans.setMicrTranCode(renewalOrder.getRenewalTrackingCode());

			trans.setExistingDeliveryMethod(renewalOrder.getExistingDeliveryMethod());

		} catch (UsatException ue) {
			throw ue;
		} catch (Exception e) {
			// convert to USAT Exception;
			UsatException ue = new UsatException(e);
			throw ue;
		}

		return trans;
	}

	// Write Vacation Hold Stop transaction
	public static ExtranetSubscriberTransactionIntf createVacationHoldStopSubscriptionTransaction(
			SubscriberAccountIntf subscriberAccountInfo, Date newStopDate, boolean donateWhileHeld) throws UsatException {
		SubscriberTransactionTO trans = new SubscriberTransactionTO();

		if (subscriberAccountInfo == null) {
			throw new UsatException(
					"SubscriptionTransactionFactory():createVacationHoldStopTransaction: Invalid arguments, cannot create customer information transaction.");
		}

		trans.setPubCode(subscriberAccountInfo.getPubCode());
		// Hard code transaction Record type
		trans.setTransactionRecType("06");
		trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		trans.setLastName(subscriberAccountInfo.getDeliveryContact().getLastName().toUpperCase());
		trans.setFirstName(subscriberAccountInfo.getDeliveryContact().getFirstName().toUpperCase());
		trans.setFirmName(subscriberAccountInfo.getDeliveryContact().getFirmName().toUpperCase());
		trans.setHomePhone(subscriberAccountInfo.getDeliveryContact().getHomePhone());
		trans.setBusinessPhone(subscriberAccountInfo.getDeliveryContact().getBusinessPhone());
		trans.setAddAddr1(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getAddress2()); // double
		// addr 2 not used
		trans.setAddAddr2("");
		trans.setUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getUnitNum());
		trans.setHalfUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getHalfUnitNum());
		trans.setStreetDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetDir().toUpperCase());
		trans.setStreetName(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetName().toUpperCase());
		trans.setStreetType(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetType().toUpperCase());
		trans.setStreetPostDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetPostDir().toUpperCase());
		trans.setSubUnitCode(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitCode());
		trans.setSubUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitNum());
		trans.setCity(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getCity().toUpperCase());
		trans.setState(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getState().toUpperCase());
		trans.setZip5(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getZip());

		// hard code transaction type
		trans.setTransactionType("9");

		if (donateWhileHeld) {
			trans.setTransactionCode("VE");
		} else {
			trans.setTransactionCode("VA");
		}

		trans.setAccountNum(subscriberAccountInfo.getAccountNumber());

		// default to not a one time bill may be cleared if not CC payment
		trans.setOneTimeBill("");

		// set effective date back to the stop date
		trans.setEffectiveDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());
		trans.setEffectiveDate(new SimpleDateFormat("yyyyMMdd").format(newStopDate));

		// ttlmktcov Customer wants more infomation - we don't use
		trans.setTtlMktCov("");

		// following two items not used
		trans.setRejComment("");
		trans.setAutoRegComm("");

		// not used
		trans.setPrintFlag("");

		// not used
		trans.setNewAddlAddr1("");
		trans.setNewAddlAddr2("");

		trans.setPerpCCFlag("");

		return trans;
	}

	// Write Vacation Hold Stop transaction
	public static ExtranetSubscriberTransactionIntf createVacationHoldStartSubscriptionTransaction(
			SubscriberAccountIntf subscriberAccountInfo, Date newStartDate, boolean donateWhileHeld) throws UsatException {
		SubscriberTransactionTO trans = new SubscriberTransactionTO();

		if (subscriberAccountInfo == null) {
			throw new UsatException(
					"SubscriptionTransactionFactory():createVacationHoldStopTransaction: Invalid arguments, cannot create customer information transaction.");
		}

		trans.setPubCode(subscriberAccountInfo.getPubCode());
		// Hard code transaction Record type
		trans.setTransactionRecType("06");
		trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		trans.setLastName(subscriberAccountInfo.getDeliveryContact().getLastName().toUpperCase());
		trans.setFirstName(subscriberAccountInfo.getDeliveryContact().getFirstName().toUpperCase());
		trans.setFirmName(subscriberAccountInfo.getDeliveryContact().getFirmName().toUpperCase());
		trans.setHomePhone(subscriberAccountInfo.getDeliveryContact().getHomePhone());
		trans.setBusinessPhone(subscriberAccountInfo.getDeliveryContact().getBusinessPhone());
		trans.setAddAddr1(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getAddress2()); // double
		// addr 2 not used
		trans.setAddAddr2("");
		trans.setUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getUnitNum());
		trans.setHalfUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getHalfUnitNum());
		trans.setStreetDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetDir().toUpperCase());
		trans.setStreetName(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetName().toUpperCase());
		trans.setStreetType(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetType().toUpperCase());
		trans.setStreetPostDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetPostDir().toUpperCase());
		trans.setSubUnitCode(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitCode());
		trans.setSubUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitNum());
		trans.setCity(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getCity().toUpperCase());
		trans.setState(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getState().toUpperCase());
		trans.setZip5(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getZip());

		// hard code transaction type
		trans.setTransactionType("8");
		// hard code transaction code
		if (donateWhileHeld) {
			trans.setTransactionCode("VE");
		} else {
			trans.setTransactionCode("VA");
		}

		trans.setAccountNum(subscriberAccountInfo.getAccountNumber());

		// default to not a one time bill may be cleared if not CC payment
		trans.setOneTimeBill("");

		// set effective date back to the stop date
		trans.setEffectiveDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());
		trans.setEffectiveDate(new SimpleDateFormat("yyyyMMdd").format(newStartDate));

		// ttlmktcov Customer wants more infomation - we don't use
		trans.setTtlMktCov("");

		// following two items not used
		trans.setRejComment("");
		trans.setAutoRegComm("");

		// not used
		trans.setPrintFlag("");

		// not used
		trans.setNewAddlAddr1("");
		trans.setNewAddlAddr2("");

		trans.setPerpCCFlag("");

		return trans;
	}

	/**
	 * 
	 * @param subscriptionOrder
	 *            - The subscription order item
	 * @param deliveryContact
	 *            - Where it should be delivered to
	 * @return - A XTRNTDWNLD transaction transfer object that is ready for persistence
	 * @throws UsatException
	 */
	public static ExtranetSubscriberTransactionIntf createChangeBillingAddressTransaction(
			SubscriberAccountIntf subscriberAccountInfo, ContactBO billingContact, PaymentMethodIntf paymentInfo,
			boolean futurePaymentNotices, boolean noBillingInfoChanges, boolean onEzpay) throws UsatException {
		SubscriberTransactionTO trans = new SubscriberTransactionTO();

		/*
		 * if (billingContact == null) { throw new UsatException(
		 * "SubscriptionTransactionFactory():createChangeBillingTransaction: Invalid arguments, cannot create subscription transaction."
		 * ); }
		 */
		if (billingContact != null && !billingContact.validateContact() && !UsaTodayConstants.ALLOW_FAILED_ADDRESSES) {
			throw new UsatException("The billing contact is not valid");
		}

		// Populate current customer information
		trans.setPubCode(subscriberAccountInfo.getPubCode());
		trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		trans.setLastName(subscriberAccountInfo.getDeliveryContact().getLastName().toUpperCase());
		trans.setFirstName(subscriberAccountInfo.getDeliveryContact().getFirstName().toUpperCase());
		trans.setFirmName(subscriberAccountInfo.getDeliveryContact().getFirmName().toUpperCase());
		trans.setHomePhone(subscriberAccountInfo.getDeliveryContact().getHomePhone());
		trans.setBusinessPhone(subscriberAccountInfo.getDeliveryContact().getBusinessPhone());
		trans.setAddAddr1(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getAddress2()); // double
		// addr 2 not used
		trans.setAddAddr2("");
		trans.setUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getUnitNum());
		trans.setHalfUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getHalfUnitNum());
		trans.setStreetDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetDir().toUpperCase());
		trans.setStreetName(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetName().toUpperCase());
		trans.setStreetType(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetType().toUpperCase());
		trans.setStreetPostDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetPostDir().toUpperCase());
		trans.setSubUnitCode(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitCode());
		trans.setSubUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitNum());
		trans.setCity(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getCity().toUpperCase());
		trans.setState(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getState().toUpperCase());
		trans.setZip5(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getZip());

		trans.setAccountNum(subscriberAccountInfo.getAccountNumber());

		if (billingContact != null) {
			trans.setBillingLastName(billingContact.getLastName().toUpperCase());
			trans.setBillingFirstName(billingContact.getFirstName().toUpperCase());
			trans.setBillingFirmName(billingContact.getFirmName().toUpperCase());
			trans.setBillingHomePhone(billingContact.getHomePhone());
			trans.setBillingBusinessPhone(billingContact.getBusinessPhone());
			if (billingContact.isAddressVerified()) {

				if (billingContact.getPersistentAddress().getAddress2() != null) {
					trans.setBillingAddress1(billingContact.getPersistentAddress().getAddress2().toUpperCase());
				}

				trans.setBillingUnitNum(billingContact.getPersistentAddress().getUnitNum());
				trans.setBillingHalfUnitNum(billingContact.getPersistentAddress().getHalfUnitNum());
				trans.setBillingStreetDir(billingContact.getPersistentAddress().getStreetDir().toUpperCase());
				trans.setBillingStreetName(billingContact.getPersistentAddress().getStreetName().toUpperCase());
				if (billingContact.getPersistentAddress().getStreetType() != null) {
					trans.setBillingStreetType(billingContact.getPersistentAddress().getStreetType().toUpperCase());
				}
				if (billingContact.getPersistentAddress().getStreetPostDir() != null) {
					trans.setBillingStreetPostDir(billingContact.getPersistentAddress().getStreetPostDir().toUpperCase());
				}
				if (billingContact.getPersistentAddress().getSubUnitCode() != null) {
					trans.setBillingSubUnitCode(billingContact.getPersistentAddress().getSubUnitCode());
				}
				if (billingContact.getPersistentAddress().getSubUnitNum() != null) {
					trans.setBillingSubUnitNum(billingContact.getPersistentAddress().getSubUnitNum());
				}
				trans.setBillingCity(billingContact.getPersistentAddress().getCity().toUpperCase());
				trans.setBillingState(billingContact.getPersistentAddress().getState().toUpperCase());
				trans.setBillingZip5(billingContact.getPersistentAddress().getZip());

			} else {
				trans.setBillingCity(billingContact.getUIAddress().getCity().toUpperCase());
				trans.setBillingState(billingContact.getUIAddress().getState());
				trans.setBillingZip5(billingContact.getUIAddress().getZip());
				// Do we need to set normal delivery fields when not verified????
				String tempStreet1 = "";
				if (billingContact.getUIAddress().getAddress1() != null) {
					tempStreet1 = billingContact.getUIAddress().getAddress1().trim().toUpperCase();
				}

				String tempStreet2 = "";
				if (billingContact.getUIAddress().getAptSuite() != null) {
					tempStreet2 = billingContact.getUIAddress().getAptSuite().trim().toUpperCase();
				}

				if ((tempStreet1.length() + tempStreet2.length()) > 54) {
					if (tempStreet1.length() > 27) {
						tempStreet1 = tempStreet1.substring(0, 27);
					}
					if (tempStreet2.length() > 27) {
						tempStreet2 = tempStreet2.substring(0, 27);
					}
				}
				String temp = tempStreet1 + "&&" + tempStreet2;
				if (temp.length() > 56) {
					temp = temp.substring(0, 56);
				}
				if (billingContact.getUiAddress().getAddress2() != null) {
					trans.setBillingAddress2(billingContact.getUIAddress().getAddress2());
				}

				trans.setBadSubAddress(temp);
			}
			try {
				if (!billingContact.getUIAddress().isValidated() || billingContact.getPersistentAddress().isMilitaryAddress()) {
					trans.setCode1ErrorCode("B");
				}
			} catch (Exception e) {
				// if for some reason we can't determine if addresses were validated
				// then default to B
				trans.setCode1ErrorCode("B");
			}
			// If billing address present or changed, type is 11, else type is 04
			trans.setTransactionRecType("11");
		} else {
			trans.setTransactionRecType("04");
		}

		// hard code transaction type
		trans.setTransactionType("");
		// hard code transaction code
		trans.setTransactionCode("");

		// default to not a one time bill may be cleared if not CC payment
		trans.setOneTimeBill("N");
		if (futurePaymentNotices) {
			trans.setOneTimeBill("Y");
		}
		trans.setPerpCCFlag("");
		if (onEzpay) {
			trans.setPerpCCFlag("P");
		}
		// Credit card information
		CreditCardPaymentMethodIntf ccPayment = (CreditCardPaymentMethodIntf) paymentInfo;

		if (ccPayment != null) {
			// set the payment type
			trans.setCreditCardType(paymentInfo.getPaymentType().getType());
			if (ccPayment.wasAuthorizedSuccessfully()) {
				trans.setCreditCardAuthCode(ccPayment.getAuthCode());
				trans.setCreditCardAuthDate(ccPayment.getAuthDate());
			} else {
				trans.setCreditCardBatchProcess("Y");
			}

			trans.setCreditCardNumber(ccPayment.getAccountNumberForPersistence());
			trans.setCreditCardExpirationDate(ccPayment.getExpirationDateForPersistence());
		}

		// clear bill charged indicator
		trans.setBillChargePd("");
		trans.setEffectiveDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());
		trans.setEmailAddress(subscriberAccountInfo.getDeliveryContact().getEmailAddress());
		trans.setTransactionDate(UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat());

		return trans;
	}

	public static ExtranetSubscriberTransactionIntf createComplaintTransaction(SubscriberAccountIntf subscriberAccountInfo,
			String comment, String creditDays1, String creditDays3, String transCode) throws UsatException {
		SubscriberTransactionTO trans = new SubscriberTransactionTO();

		trans.setPubCode(subscriberAccountInfo.getPubCode());
		trans.setTransactionRecType("07");
		trans.setTransactionType("E");

		String todayDate = UsatDateTimeFormatter.getCurrentDateInYYYYMMDDFormat();
		trans.setTransactionDate(todayDate);
		trans.setEffectiveDate(todayDate);
		trans.setAccountNum(subscriberAccountInfo.getAccountNumber());

		// // from the complaint specific type of problem

		trans.setTransactionCode(transCode);
		trans.setCreditDays1(creditDays1); // set for ut
		trans.setCreditDays3(creditDays3); // set for bw
		trans.setComment(comment); // from bo

		trans.setContactCustomer("N");

		// KeyCodeParser kp = new KeyCodeParser(subscriberAccountInfo.getKeyCode());
		// trans.setSrcOrdCode(kp.getSourceCode());
		// trans.setPromoCode(kp.getPromoCode());
		// trans.setContestCode(kp.getContestCode());

		trans.setLastName(subscriberAccountInfo.getDeliveryContact().getLastName().toUpperCase());
		trans.setFirstName(subscriberAccountInfo.getDeliveryContact().getFirstName().toUpperCase());
		trans.setFirmName(subscriberAccountInfo.getDeliveryContact().getFirmName().toUpperCase());
		trans.setUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getUnitNum());
		trans.setHalfUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getHalfUnitNum());
		trans.setStreetDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetDir().toUpperCase());
		trans.setStreetName(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetName().toUpperCase());
		trans.setStreetType(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetType().toUpperCase());
		trans.setStreetPostDir(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getStreetPostDir().toUpperCase());
		trans.setSubUnitCode(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitCode());
		trans.setSubUnitNum(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getSubUnitNum());
		trans.setCity(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getCity().toUpperCase());
		trans.setState(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getState().toUpperCase());
		trans.setZip5(subscriberAccountInfo.getDeliveryContact().getPersistentAddress().getZip());

		return trans;
	}
}
