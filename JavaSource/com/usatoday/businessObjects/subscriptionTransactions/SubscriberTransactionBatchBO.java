package com.usatoday.businessObjects.subscriptionTransactions;

import java.util.Collection;
import java.util.Random;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.subscriptionTransactions.ExtranetSubscriberTransactionBatchIntf;
import com.usatoday.business.interfaces.subscriptionTransactions.ExtranetSubscriberTransactionIntf;
import com.usatoday.integration.SubscriberTransactionDAO;
import com.usatoday.util.constants.UsaTodayConstants;

public class SubscriberTransactionBatchBO implements ExtranetSubscriberTransactionBatchIntf {

	public static final String dateFormatPart = "yyyyMMdd";

	@Override
	public int getCountOfNewTransactions() throws UsatException {

		SubscriberTransactionDAO dao = new SubscriberTransactionDAO();

		int tranCount = dao.getCountOfTransactionsInState(SubscriberTransactionDAO.NEW);

		return tranCount;
	}

	@Override
	public int getCountofTransactionsInProcessing() throws UsatException {

		SubscriberTransactionDAO dao = new SubscriberTransactionDAO();

		int tranCount = dao.getCountOfTransactionsInState(SubscriberTransactionDAO.IN_PROCESSING);

		return tranCount;
	}

	@Override
	public Collection<ExtranetSubscriberTransactionIntf> getNewTransactionsForProcessing() throws UsatException {

		SubscriberTransactionDAO dao = new SubscriberTransactionDAO();

		if (this.getCountofTransactionsInProcessing() > 0) {
			throw new UsatException(
					"Transactions are already in processsing. Another process is already processing records of a manual reset of records to New State (0) is required.");
		}

		// change state of records to isolate the batch
		dao.changeStateToInBatchProcessing();

		String batchID = this.determineNextBatchID();

		// assign a unique batch id to the batch
		dao.assignBatchIDToInProcessing(batchID);

		// retrieve the records for processing
		Collection<ExtranetSubscriberTransactionIntf> trans = dao.fetchTransactionsInProcessing();

		return trans;
	}

	@Override
	public int setTransactionStateFromProcessingToProcessed() throws UsatException {

		SubscriberTransactionDAO dao = new SubscriberTransactionDAO();

		int numberUpdated = dao.changeStateToProcessed();

		return numberUpdated;
	}

	@Override
	public Collection<ExtranetSubscriberTransactionIntf> getTransactionsInBatch(String batchID) throws UsatException {

		SubscriberTransactionDAO dao = new SubscriberTransactionDAO();

		Collection<ExtranetSubscriberTransactionIntf> trans = dao.fetchTransactionsInBatch(batchID);

		return trans;
	}

	@Override
	public int setTransactionStateOfBatchToReProcessed(String batchID) throws UsatException {

		SubscriberTransactionDAO dao = new SubscriberTransactionDAO();

		int numberUpdated = dao.changeStateToReProcessedForBatch(batchID);

		return numberUpdated;

	}

	@Override
	public int purgeTransactionsOlderThan(int numDays) throws UsatException {

		SubscriberTransactionDAO dao = new SubscriberTransactionDAO();

		if (numDays < 7) {
			throw new UsatException("Cannot purge records created less than 7 days ago");
		}
		int numDeleted = dao.deleteProcessedTransactionsOlderThanSpecifiedDays(numDays);

		return numDeleted;
	}

	/**
	 * 
	 * @return a unique batch id
	 */
	private String determineNextBatchID() {
		String newID = null;

		StringBuilder sb = new StringBuilder();

		DateTime now = new DateTime();

		SubscriberTransactionDAO dao = new SubscriberTransactionDAO();

		try {

			String lastBatchId = dao.getLastBatchIDFromToday();

			String incrementPart = "";
			if (lastBatchId != null) {
				int underscoreIndex = lastBatchId.indexOf('_');
				String increment = lastBatchId.substring(underscoreIndex + 1);
				increment = increment.trim();
				int incrementInt = Integer.valueOf(increment);
				incrementInt++;
				incrementPart = String.valueOf(incrementInt);
			} else {
				// new day
				incrementPart = "1";
			}
			sb.append(now.toString(SubscriberTransactionBatchBO.dateFormatPart));
			sb.append("_").append(incrementPart);

			newID = sb.toString();

		} catch (Exception e) {
			Random r = new Random();
			newID = now.toString("yyyyMMdd_" + r.nextInt());
		}

		if (newID.length() > 32) {
			newID = newID.substring(0, 31);
		}

		if (UsaTodayConstants.debug) {
			System.out.println("New Transaction Batch ID: " + newID);
		}

		return newID;
	}
}
