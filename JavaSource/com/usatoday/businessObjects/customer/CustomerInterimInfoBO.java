/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import java.sql.Timestamp;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.CustomerInterimInfoIntf;
import com.usatoday.integration.CustomerInterimInfoDAO;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class AddressBO
 * 
 *        based class implementation of a postal address.
 * 
 */
public class CustomerInterimInfoBO implements CustomerInterimInfoIntf {

	// static state fields
	private String publication = null;
	private String email = null;
	private String firstName = null;
	private String lastName = null;
	private String firmName = null;
	private String address1 = null;
	private String address2 = null;
	private String city = null;
	private String state = null;
	private String zip = null;
	private String homePhone = null;
	private String workPhone = null;
	private String billFirstName = null;
	private String billLastName = null;
	private String billFirmName = null;
	private String billAddress1 = null;
	private String billAddress2 = null;
	private String billCity = null;
	private String billState = null;
	private String billZip = null;
	private String billPhone = null;
	private String rateCode = null;
	private String promoCode = null;
	private String contestCode = null;
	private String srcOrdCode = null;
	private String subsAmount = null;
	private String subsDur = null;
	private String startDate = null;
	private long orderID = -1;
	private int processState = 0;
	private Timestamp insertedTimeStamp = null;
	private String delvMethod = null;

	/**
     * 
     */
	public CustomerInterimInfoBO() {
		super();
	}

	public CustomerInterimInfoBO(CustomerInterimInfoIntf source) {
		super();
		this.setAddress1(source.getAddress1());
		this.setAddress2(source.getAddress2());
		this.setBillAddress1(source.getBillAddress1());
		this.setBillAddress2(source.getBillAddress2());
		this.setBillCity(source.getBillCity());
		this.setBillFirmName(source.getBillFirmName());
		this.setBillFirstName(source.getBillFirstName());
		this.setBillLastName(source.getBillLastName());
		this.setBillPhone(source.getBillPhone());
		this.setBillState(source.getBillState());
		this.setBillZip(source.getBillZip());
		this.setCity(source.getCity());
		this.setContestCode(source.getContestCode());
		this.setEmail(source.getEmail());
		this.setFirmName(source.getFirmName());
		this.setFirstName(source.getFirstName());
		this.setHomePhone(source.getHomePhone());
		this.setInsertedTimeStamp(source.getInsertedTimeStamp());
		this.setLastName(source.getLastName());
		this.setOrderID(source.getOrderID());
		this.setProcessState(source.getProcessState());
		this.setPromoCode(source.getPromoCode());
		this.setPublication(source.getPublication());
		this.setRateCode(source.getRateCode());
		this.setSrcOrdCode(source.getSrcOrdCode());
		this.setStartDate(source.getStartDate());
		this.setState(source.getState());
		this.setSubsAmount(source.getSubsAmount());
		this.setSubsDur(source.getSubsDur());
		this.setWorkPhone(source.getWorkPhone());
		this.setZip(source.getZip());
		this.setDelvMethod(source.getDelvMethod());
		// this.setDelvMethod(source.)

	}

	public boolean save(CustomerInterimInfoIntf customerInterimInfoIntf) throws UsatException {

		boolean result = false;
		CustomerInterimInfoDAO customerInterimInfoDAO = new CustomerInterimInfoDAO();

		try {
			if (customerInterimInfoIntf.getOrderID() > 0) {
				customerInterimInfoDAO.update(customerInterimInfoIntf);
			} else {
				customerInterimInfoDAO.insert(customerInterimInfoIntf);
			}
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		}
		return result;
	}

	public boolean delete(CustomerInterimInfoIntf customerInterimInfoIntf) throws UsatException {

		boolean result = false;
		CustomerInterimInfoDAO customerInterimInfoDAO = new CustomerInterimInfoDAO();

		try {
			customerInterimInfoDAO.delete(customerInterimInfoIntf);
		} catch (Exception e) {
			result = false;
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		}
		return result;
	}

	public static int changeStateToInBatchProcessing() throws UsatException {
		CustomerInterimInfoDAO dao = new CustomerInterimInfoDAO();
		return dao.changeStateToInBatchProcessing();
	}

	public static int changeStateToReadyForDelete() throws UsatException {
		CustomerInterimInfoDAO dao = new CustomerInterimInfoDAO();
		return dao.changeStateToReadyForDelete();
	}

	public static int changeStateToBatchError() throws UsatException {
		CustomerInterimInfoDAO dao = new CustomerInterimInfoDAO();
		return dao.changeStateToBatchError();
	}

	public static int deleteAttributesOlderThanSpecifiedDays(int numDays) throws UsatException {
		if (numDays < 0) {
			throw new UsatException("CustomerInterimInfoBO::deleteAttributesOlderThanSpecifiedDays() - Invalid number of days: "
					+ numDays);
		}
		CustomerInterimInfoDAO dao = new CustomerInterimInfoDAO();
		return dao.deleteAttributesOlderThanSpecifiedDays(numDays);
	}

	private void appendPaddedString(StringBuffer buf, int fieldLength, String field) {
		if (buf == null) {
			return;
		}

		if (field == null) {
			for (int i = 0; i < fieldLength; i++) {
				buf.append(" ");
			}
			return;
		}

		int currentFieldLength = field.length();

		if (currentFieldLength <= fieldLength) {
			// no truncation
			buf.append(field);
			if (currentFieldLength != fieldLength) {
				// buffer remaining field with blanks
				for (int i = currentFieldLength; i < fieldLength; i++) {
					buf.append(" ");
				}
			}
		} else {
			// truncate
			buf.append(field.substring(0, fieldLength));
		}
	}

	public String getBatchFormattedString() throws UsatException {
		StringBuffer buf = new StringBuffer();
		try {

			this.appendPaddedString(buf, 2, this.getPublication());
			this.appendPaddedString(buf, 60, this.email);
			this.appendPaddedString(buf, 25, this.getFirstName());
			this.appendPaddedString(buf, 25, this.getLastName());
			this.appendPaddedString(buf, 28, this.getFirmName());
			this.appendPaddedString(buf, 28, this.getAddress1());
			this.appendPaddedString(buf, 28, this.getAddress2());
			this.appendPaddedString(buf, 28, this.getCity());
			this.appendPaddedString(buf, 2, this.getState());
			this.appendPaddedString(buf, 5, this.getZip());
			this.appendPaddedString(buf, 10, this.getHomePhone());
			this.appendPaddedString(buf, 10, this.getWorkPhone());
			this.appendPaddedString(buf, 10, this.getBillFirstName());
			this.appendPaddedString(buf, 15, this.getBillLastName());
			this.appendPaddedString(buf, 28, this.getBillFirmName());
			this.appendPaddedString(buf, 28, this.getBillAddress1());
			this.appendPaddedString(buf, 28, this.getBillAddress2());
			this.appendPaddedString(buf, 28, this.getBillCity());
			this.appendPaddedString(buf, 2, this.getBillState());
			this.appendPaddedString(buf, 5, this.getBillZip());
			this.appendPaddedString(buf, 10, this.getBillPhone());
			this.appendPaddedString(buf, 2, this.getRateCode());
			this.appendPaddedString(buf, 2, this.getPromoCode());
			this.appendPaddedString(buf, 2, this.getContestCode());
			this.appendPaddedString(buf, 1, this.getSrcOrdCode());
			this.appendPaddedString(buf, 7, this.getSubsAmount());
			this.appendPaddedString(buf, 3, this.getSubsDur());
			this.appendPaddedString(buf, 10, this.getStartDate());
		} catch (Exception e) {
			System.out
					.println("CustomerInterimInfoBO:getBatchFormattedString() - Failed to create batch record: " + e.getMessage());
			throw new UsatException(e);
		}

		return buf.toString();
	}

	/**
	 * @return Returns the address1.
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1
	 *            The address1 to set.
	 */
	public void setAddress1(String address1) {
		if (address1 != null) {
			if (address1.trim().length() <= 28) {
				this.address1 = address1.trim();
			} else {
				this.address1 = address1.trim().substring(0, 27);
				System.out.println("**** CustomerInterimInfoBO:setAddress1() - User value (length:" + address1.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the address2.
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2
	 *            The address2 to set.
	 */
	public void setAddress2(String address2) {
		if (address2 != null) {
			if (address2.trim().length() <= 28) {
				this.address2 = address2.trim();
			} else {
				this.address2 = address2.trim().substring(0, 27);
				System.out.println("**** CustomerInterimInfoBO:setAddress2() - User value (length:" + address2.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the billAddress1.
	 */
	public String getBillAddress1() {
		return billAddress1;
	}

	/**
	 * @param billAddress1
	 *            The billAddress1 to set.
	 */
	public void setBillAddress1(String billAddress1) {
		if (billAddress1 != null) {
			if (billAddress1.trim().length() <= 28) {
				this.billAddress1 = billAddress1.trim();
			} else {
				this.billAddress1 = billAddress1.trim().substring(0, 27);
				System.out.println("**** CustomerInterimInfoBO:setBillAddress1() - User value (length:"
						+ billAddress1.trim().length() + ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the billAddress2.
	 */
	public String getBillAddress2() {
		return billAddress2;
	}

	/**
	 * @param billAddress2
	 *            The billAddress2 to set.
	 */
	public void setBillAddress2(String billAddress2) {
		if (billAddress2 != null) {
			if (billAddress2.trim().length() <= 28) {
				this.billAddress2 = billAddress2.trim();
			} else {
				this.billAddress2 = billAddress2.trim().substring(0, 27);
				System.out.println("**** CustomerInterimInfoBO:setBillAddress2() - User value (length:"
						+ billAddress2.trim().length() + ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the billCity.
	 */
	public String getBillCity() {
		return billCity;
	}

	/**
	 * @param billCity
	 *            The billCity to set.
	 */
	public void setBillCity(String billCity) {
		if (billCity != null) {
			if (billCity.trim().length() <= 28) {
				this.billCity = billCity;
			} else {
				this.billCity = billCity.trim().substring(0, 27);
				System.out.println("**** CustomerInterimInfoBO:setBillCity() - User value (length:" + billCity.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the billFirmName.
	 */
	public String getBillFirmName() {
		return billFirmName;
	}

	/**
	 * @param billFirmName
	 *            The billFirmName to set.
	 */
	public void setBillFirmName(String billFirmName) {
		if (billFirmName != null) {
			if (billFirmName.trim().length() <= 28) {
				this.billFirmName = billFirmName.trim();
			} else {
				this.billFirmName = billFirmName.trim().substring(0, 27);
				System.out.println("**** CustomerInterimInfoBO:setBillFirmName() - User value (length:"
						+ billFirmName.trim().length() + ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the billFirstName.
	 */
	public String getBillFirstName() {
		return billFirstName;
	}

	/**
	 * @param billFirstName
	 *            The billFirstName to set.
	 */
	public void setBillFirstName(String billFirstName) {
		if (billFirstName != null) {
			if (billFirstName.trim().length() <= 10) {
				this.billFirstName = billFirstName.trim();
			} else {
				this.billFirstName = billFirstName.trim().substring(0, 9);
				System.out.println("**** CustomerInterimInfoBO:setBillFirstName() - User value (length:"
						+ billFirstName.trim().length() + ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the billLastName.
	 */
	public String getBillLastName() {
		return billLastName;
	}

	/**
	 * @param billLastName
	 *            The billLastName to set.
	 */
	public void setBillLastName(String billLastName) {
		if (billLastName != null) {
			if (billLastName.trim().length() <= 15) {
				this.billLastName = billLastName.trim();
			} else {
				this.billLastName = billLastName.trim().substring(0, 14);
				System.out.println("**** CustomerInterimInfoBO:setBillLastName() - User value (length:"
						+ billLastName.trim().length() + ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the billPhone.
	 */
	public String getBillPhone() {
		return billPhone;
	}

	/**
	 * @param billPhone
	 *            The billPhone to set.
	 */
	public void setBillPhone(String billPhone) {
		if (billPhone != null) {
			if (billPhone.trim().length() <= 10) {
				this.billPhone = billPhone.trim();
			} else {
				this.billPhone = billPhone.trim().substring(0, 9);
				System.out.println("**** CustomerInterimInfoBO:setBillPhone() - User value (length:" + billPhone.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the billState.
	 */
	public String getBillState() {
		return billState;
	}

	/**
	 * @param billState
	 *            The billState to set.
	 */
	public void setBillState(String billState) {
		if (billState != null) {
			if (billState.trim().length() <= 2) {
				this.billState = billState.trim();
			} else {
				this.billState = billState.trim().substring(0, 1);
				System.out.println("**** CustomerInterimInfoBO:setBillState() - User value (length:" + billState.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the billZip.
	 */
	public String getBillZip() {
		return billZip;
	}

	/**
	 * @param billZip
	 *            The billZip to set.
	 */
	public void setBillZip(String billZip) {
		if (billZip != null) {
			if (billZip.trim().length() <= 5) {
				this.billZip = billZip.trim();
			} else {
				this.billZip = billZip.trim().substring(0, 4);
				System.out.println("**** CustomerInterimInfoBO:setBillZip() - User value (length:" + billZip.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the city.
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            The city to set.
	 */
	public void setCity(String city) {
		if (city != null) {
			if (city.trim().length() <= 28) {
				this.city = city.trim();
			} else {
				this.city = city.trim().substring(0, 27);
				System.out.println("**** CustomerInterimInfoBO:setCity() - User value (length:" + city.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the contestCode.
	 */
	public String getContestCode() {
		return contestCode;
	}

	/**
	 * @param contestCode
	 *            The contestCode to set.
	 */
	public void setContestCode(String contestCode) {
		this.contestCode = contestCode;
	}

	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		if (email != null) {
			if (email.trim().length() <= 60) {
				this.email = email.trim();
			} else {
				this.email = email.trim().substring(0, 59);
				System.out.println("**** CustomerInterimInfoBO:setEmail() - User value (length:" + email.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the firmName.
	 */
	public String getFirmName() {
		return firmName;
	}

	/**
	 * @param firmName
	 *            The firmName to set.
	 */
	public void setFirmName(String firmName) {
		if (firmName != null) {
			if (firmName.trim().length() <= 28) {
				this.firmName = firmName.trim();
			} else {
				this.firmName = firmName.trim().substring(0, 27);
				System.out.println("**** CustomerInterimInfoBO:setFirmName() - User value (length:" + firmName.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		if (firstName != null) {
			if (firstName.trim().length() <= 25) {
				this.firstName = firstName.trim();
			} else {
				this.firstName = firstName.trim().substring(0, 24);
				System.out.println("**** CustomerInterimInfoBO:setFirstName() - User value (length:" + firstName.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the homePhone.
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * @param homePhone
	 *            The homePhone to set.
	 */
	public void setHomePhone(String homePhone) {
		if (homePhone != null) {
			if (homePhone.trim().length() <= 10) {
				this.homePhone = homePhone.trim();
			} else {
				this.homePhone = homePhone.trim().substring(0, 9);
				System.out.println("**** CustomerInterimInfoBO:setHomePhone() - User value (length:" + homePhone.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the insertedTimeStamp.
	 */
	public Timestamp getInsertedTimeStamp() {
		return insertedTimeStamp;
	}

	/**
	 * @param insertedTimeStamp
	 *            The insertedTimeStamp to set.
	 */
	public void setInsertedTimeStamp(Timestamp insertedTimeStamp) {
		this.insertedTimeStamp = insertedTimeStamp;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		if (lastName != null) {
			if (lastName.trim().length() <= 25) {
				this.lastName = lastName.trim();
			} else {
				this.lastName = lastName.trim().substring(0, 24);
				System.out.println("**** CustomerInterimInfoBO:setLastName() - User value (length:" + lastName.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the orderID.
	 */
	public long getOrderID() {
		return orderID;
	}

	/**
	 * @param orderID
	 *            The orderID to set.
	 */
	public void setOrderID(long orderID) {
		this.orderID = orderID;
	}

	/**
	 * @return Returns the processState.
	 */
	public int getProcessState() {
		return processState;
	}

	/**
	 * @param processState
	 *            The processState to set.
	 */
	public void setProcessState(int processState) {
		this.processState = processState;
	}

	/**
	 * @return Returns the promoCode.
	 */
	public String getPromoCode() {
		return promoCode;
	}

	/**
	 * @param promoCode
	 *            The promoCode to set.
	 */
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	/**
	 * @return Returns the publication.
	 */
	public String getPublication() {
		return publication;
	}

	/**
	 * @param publication
	 *            The publication to set.
	 */
	public void setPublication(String publication) {
		this.publication = publication;
	}

	/**
	 * @return Returns the rateCode.
	 */
	public String getRateCode() {
		return rateCode;
	}

	/**
	 * @param rateCode
	 *            The rateCode to set.
	 */
	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	/**
	 * @return Returns the srcOrdCode.
	 */
	public String getSrcOrdCode() {
		return srcOrdCode;
	}

	/**
	 * @param srcOrdCode
	 *            The srcOrdCode to set.
	 */
	public void setSrcOrdCode(String srcOrdCode) {
		this.srcOrdCode = srcOrdCode;
	}

	/**
	 * @return Returns the startDate.
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            The startDate to set.
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return Returns the state.
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            The state to set.
	 */
	public void setState(String state) {
		if (state != null) {
			if (state.trim().length() <= 2) {
				this.state = state.trim();
			} else {
				this.state = state.trim().substring(0, 1);
				System.out.println("**** CustomerInterimInfoBO:setState() - User value (length:" + state.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the subsAmount.
	 */
	public String getSubsAmount() {
		return subsAmount;
	}

	/**
	 * @param subsAmount
	 *            The subsAmount to set.
	 */
	public void setSubsAmount(String subsAmount) {
		if (subsAmount != null) {
			if (subsAmount.trim().length() <= 7) {
				this.subsAmount = subsAmount.trim();
			} else {
				this.subsAmount = subsAmount.trim().substring(0, 6);
				System.out.println("**** CustomerInterimInfoBO:setSubsAmount() - User value (length:" + subsAmount.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the subsDur.
	 */
	public String getSubsDur() {
		return subsDur;
	}

	/**
	 * @param subsDur
	 *            The subsDur to set.
	 */
	public void setSubsDur(String subsDur) {
		if (subsDur != null) {
			if (subsDur.trim().length() <= 3) {
				this.subsDur = subsDur.trim();
			} else {
				this.subsDur = subsDur.trim().substring(0, 2);
				System.out.println("**** CustomerInterimInfoBO:setSubsDur() - User value (length:" + city.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the workPhone.
	 */
	public String getWorkPhone() {
		return workPhone;
	}

	/**
	 * @return Returns the delvMethod.
	 */
	public String getDelvMethod() {
		return delvMethod;
	}

	/**
	 * @param delvMethod
	 *            .
	 */
	public void setDelvMethod(String delvMethod) {

		this.delvMethod = delvMethod;

	}

	/**
	 * @param workPhone
	 *            The workPhone to set.
	 */
	public void setWorkPhone(String workPhone) {
		if (workPhone != null) {
			if (workPhone.trim().length() <= 28) {
				this.workPhone = workPhone.trim();
			} else {
				this.workPhone = workPhone.trim().substring(0, 27);
				System.out.println("**** CustomerInterimInfoBO:setWorkPhone() - User value (length:" + workPhone.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the zip.
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 *            The zip to set.
	 */
	public void setZip(String zip) {
		if (zip != null) {
			if (zip.trim().length() <= 28) {
				this.zip = zip.trim();
			} else {
				this.zip = zip.trim().substring(0, 27);
				System.out.println("**** CustomerInterimInfoBO:setZip() - User value (length:" + zip.trim().length()
						+ ") exceeds table field length ****");
			}
		}
	}

}
