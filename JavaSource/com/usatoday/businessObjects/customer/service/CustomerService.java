package com.usatoday.businessObjects.customer.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.gannett.usat.iconapi.client.ChangeCard;
import com.gannett.usat.iconapi.client.DeliveryIssue;
import com.gannett.usat.iconapi.client.MakePayment;
import com.gannett.usat.iconapi.client.NameChange;
import com.gannett.usat.iconapi.client.NewSubscriptionStart;
import com.gannett.usat.iconapi.client.SubscriptionAddressChange;
import com.gannett.usat.iconapi.client.SuspendResume;
import com.gannett.usat.iconapi.client.UpdateEmail;
import com.gannett.usat.iconapi.domainbeans.addressChange.AddressChangeResponse;
import com.gannett.usat.iconapi.domainbeans.changeCard.ChangeCardEzPayResponse;
import com.gannett.usat.iconapi.domainbeans.changeCard.ChangeCardResponse;
import com.gannett.usat.iconapi.domainbeans.deliveryIssue.DeliveryIssueResponse;
import com.gannett.usat.iconapi.domainbeans.makePayment.MakeAPaymentResponse;
import com.gannett.usat.iconapi.domainbeans.nameChange.NameChangeResponse;
import com.gannett.usat.iconapi.domainbeans.newSubscriptionStart.StartSubscriptionResponse;
import com.gannett.usat.iconapi.domainbeans.suspendResume.SuspendResumeResponse;
import com.gannett.usat.iconapi.domainbeans.updateEmail.UpdateEmailResponse;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.shopping.RenewalOrderItemIntf;
import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentMethodIntf;
import com.usatoday.businessObjects.subscriptionTransactions.GenesysResponse;
import com.usatoday.businessObjects.util.KeyCodeParser;
import com.usatoday.util.constants.UsaTodayConstants;

public class CustomerService {

	/**
	 * 
	 * @param account
	 * @param holdStart
	 * @param holdEnd
	 * @param donate
	 * @return the persisted transactions
	 * @throws UsatException
	 */
	public static GenesysResponse availableOffers(SubscriberAccountIntf account, DateTime holdStart, DateTime holdEnd,
			boolean donate) throws UsatException {

		if (account.getProduct().isElectronicDelivery()) {
			throw new UsatException("Cannot place delivery holds on electronicly delivered products.");
		}

		if (holdStart == null) {
			throw new UsatException("You must specify the date to begin suspending delivery.");
		}

		// AvailableOffers availableOffersAPI = new AvailableOffers();

		SuspendResume suspendResumeAPI = new SuspendResume();

		//
		GenesysResponse gr = new GenesysResponse();

		try {

			String vacationRestart = "C";
			String holdStartStr = holdStart.toString("yyyyMMdd");

			String holdEndStr = "";
			if (holdEnd == null) {
				holdEndStr = "";
			} else {
				holdEndStr = holdEnd.toString("yyyyMMdd");
			}
			if (account.isDonatingToNIE()) {
				vacationRestart = "D";
			}
			// AvailableOffersResponse response = availableOffersAPI.
			SuspendResumeResponse response = suspendResumeAPI.createSuspendResume(account.getPubCode(), account.getAccountNumber(),
					holdStartStr, holdEndStr, vacationRestart);
			gr.addResponse(response);

		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}

		return gr;
	}

	/**
	 * 
	 * @param account
	 * @param billingContact
	 * @return
	 * @throws UsatException
	 */
	public static GenesysResponse processBillingAddressChange(SubscriberAccountIntf account, ContactIntf billingContact)
			throws UsatException {

		// enforce any business rules
		if (billingContact == null) {
			throw new UsatException(
					"CustomerService():processBillingAddressChange: Invalid arguments, no new delivery information.");
		}

		if (!billingContact.validateContact() && !UsaTodayConstants.ALLOW_FAILED_ADDRESSES) {
			throw new UsatException("The billing contact is not valid");
		}

		SubscriptionAddressChange api = new SubscriptionAddressChange();

		GenesysResponse gr = new GenesysResponse();

		try {
			UIAddressIntf address = billingContact.getUIAddress();
			String addr1 = address.getAddress1();
			if (address.getAptSuite() != null) {
				addr1 += " ";
				addr1 += address.getAptSuite();
			}
			if (addr1 != null) {
				addr1 = addr1.trim();
			}

			AddressChangeResponse response = api.createBillingAddressChange(account.getPubCode(), account.getAccountNumber(),
					billingContact.getFirmName(), addr1, address.getAddress2(), address.getCity(), address.getState(),
					address.getZip(), billingContact.getHomePhone(), billingContact.getBusinessPhone());

			gr.addResponse(response);
		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}

		return gr;
	}

	/**
	 * 
	 * @param account
	 * @param holdStart
	 * @param holdEnd
	 * @param donate
	 * @return the persisted transactions
	 * @throws UsatException
	 */
	public static GenesysResponse processChangeCard(String pubCode, String accountNumber, String cardNumber, String cardType,
			String cardExp, String ezPay) throws UsatException {

		ChangeCard changeCardAPI = new ChangeCard();
		GenesysResponse gr = new GenesysResponse();

		try {
			if (ezPay.equals("NEW")) {
				ChangeCardEzPayResponse response = changeCardAPI.createChangeCardEzPay(pubCode, accountNumber, cardNumber,
						cardType, cardExp, ezPay);
				gr.addResponse(response);
			} else {
				ChangeCardResponse response = changeCardAPI.createChangeCard(pubCode, accountNumber, cardNumber, cardType, cardExp,
						ezPay);
				gr.addResponse(response);
			}
		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}
		return gr;
	}

	/**
	 * 
	 * @param account
	 * @param complaint
	 * @return The persisted transactions
	 * @throws UsatException
	 */
	public static GenesysResponse processComplaint(SubscriberAccountIntf account, Complaint complaint) throws UsatException {

		// enforce any business rules

		if (account.getProduct().isElectronicDelivery()) {
			throw new UsatException("Cannot place delivery complaints on electronicly delivered products.");
		}

		Collection<DateTime> effectiveDates = complaint.getIssueDates();

		DeliveryIssue deliveryIssueAPI = new DeliveryIssue();

		//
		GenesysResponse gr = new GenesysResponse();

		try {
			for (DateTime eDate : effectiveDates) {
				String eDateStr = eDate.toString("yyyyMMdd");
				DeliveryIssueResponse response = deliveryIssueAPI.createDeliveryIssue(account.getPubCode(),
						account.getAccountNumber(), eDateStr, complaint.getTransactionCode());
				gr.addResponse(response);
			}
		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}

		return gr;
	}

	/**
	 * 
	 * @param account
	 * @param deliveryContact
	 * @param effectiveDate
	 * @return
	 * @throws UsatException
	 */
	public static GenesysResponse processDeliveryAddressChange(SubscriberAccountIntf account, ContactIntf deliveryContact,
			DateTime effectiveDate) throws UsatException {

		// enforce any business rules
		if (deliveryContact == null) {
			throw new UsatException(
					"CustomerService():processDeliveryAddressChange: Invalid arguments, no new delivery information.");
		}

		if (!deliveryContact.validateContact() && !UsaTodayConstants.ALLOW_FAILED_ADDRESSES) {
			throw new UsatException("The delivery contact is not valid");
		}

		SubscriptionAddressChange api = new SubscriptionAddressChange();

		//
		GenesysResponse gr = new GenesysResponse();

		try {
			UIAddressIntf address = deliveryContact.getUIAddress();
			String addr1 = address.getAddress1();
			if (address.getAptSuite() != null) {
				addr1 += " ";
				addr1 += address.getAptSuite();
			}
			if (addr1 != null) {
				addr1 = addr1.trim();
			}

			AddressChangeResponse response = api.createDeliveryAddressChange(account.getPubCode(), account.getAccountNumber(),
					effectiveDate, deliveryContact.getFirmName(), addr1, address.getAddress2(), address.getCity(),
					address.getState(), address.getZip(), deliveryContact.getHomePhone(), deliveryContact.getBusinessPhone());

			gr.addResponse(response);
		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}

		return gr;
	}

	/**
	 * 
	 * @param account
	 * @param paymentInfo
	 * @param amount
	 * @param rateCode
	 * @param termLength
	 * @param signUpForEZPAY
	 * @return
	 * @throws UsatException
	 */
	public static GenesysResponse processSubscriptionRenewal(RenewalOrderItemIntf renewalOrder,
			CreditCardPaymentMethodIntf paymentInfo) throws UsatException {
		//
		GenesysResponse gr = new GenesysResponse();

		MakePayment makePaymentAPI = new MakePayment();

		try {

			String ccExpDate = paymentInfo.getExpirationMonth() + "/" + paymentInfo.getExpirationYear().substring(2);
			String amount = String.valueOf(renewalOrder.getSubTotal());
			String rateCode = renewalOrder.getSelectedTerm().getPiaRateCode();
			String durationType = renewalOrder.getSelectedTerm().getDurationType();
			// String termLength = null; // product.getAppliedTerm().getDurationInWeeks(); // API doesn't want amount and term
			// length
			String termLength = renewalOrder.getSelectedTerm().getRenewalPeriodLength();

			boolean signUpForEZPAY = renewalOrder.isChoosingEZPay();
			if (renewalOrder.getAccount().isOnEZPay()) {
				// if already on ez-pay we don't send it
				signUpForEZPAY = false;
			}

			// we no longer update key code on a payment
			// KeyCodeParser parser = new KeyCodeParser();
			// parser.setKeyCode(renewalOrder.getKeyCode());
			// String promoCode = parser.getPromoCode();
			// String sourceCode =parser.getSourceCode();
			// String contestCode = parser.getContestCode();

			SubscriberAccountIntf account = renewalOrder.getAccount();

			String ezPayFlag = null;
			if (signUpForEZPAY) {
				ezPayFlag = "Y";
			}

			MakeAPaymentResponse response = makePaymentAPI.makePayment(account.getPubCode(), account.getAccountNumber(),
					paymentInfo.getCardNumber(), paymentInfo.getCreditCardTypeCode(), ccExpDate, amount, rateCode, ezPayFlag,
					termLength, durationType);

			gr.addResponse(response);
		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}

		return gr;
	}

	/**
	 * 
	 * @param subscriptionOrderItem
	 * @param deliveryContact
	 * @param billingContact
	 * @param paymentInfo
	 * @return GenesysResponse
	 * @throws UsatException
	 */
	public static GenesysResponse processSubscriptionStart(SubscriptionOrderItemIntf soi, ContactIntf deliveryContact,
			ContactIntf billingContact, CreditCardPaymentMethodIntf paymentInfo) throws UsatException {

		// enforce any business rules

		NewSubscriptionStart newStartAPI = new NewSubscriptionStart();

		//
		GenesysResponse gr = new GenesysResponse();

		try {
			String ccExpDate = paymentInfo.getExpirationMonth() + "/" + paymentInfo.getExpirationYear().substring(2);
			KeyCodeParser parser = new KeyCodeParser(soi.getKeyCode());

			// delivery address stuff
			String address1Temp = deliveryContact.getUIAddress().getAddress1() + " " + deliveryContact.getUIAddress().getAptSuite();
			address1Temp = address1Temp.trim();

			// billing setup
			String secondaryEmail = null;
			String bLastName = null;
			String bFirstName = null;
			String bFirmName = null;
			String bAddress1 = null;
			String bAddress2 = null;
			String bCity = null;
			String bState = null;
			String bZip = null;
			String billingPhone = null;
			if (billingContact != null) {
				secondaryEmail = billingContact.getEmailAddress();
				if (deliveryContact.getEmailAddress().equalsIgnoreCase(secondaryEmail)) {
					secondaryEmail = null;
				}
				bLastName = billingContact.getLastName();
				bFirstName = billingContact.getFirstName();
				bFirmName = billingContact.getFirmName();
				bAddress1 = billingContact.getUIAddress().getAddress1() + " " + billingContact.getUIAddress().getAptSuite();
				bAddress1 = bAddress1.trim();
				bAddress2 = billingContact.getUIAddress().getAddress2();
				bCity = billingContact.getUIAddress().getCity();
				bState = billingContact.getUIAddress().getState();
				bZip = billingContact.getUIAddress().getZip();
				billingPhone = billingContact.getHomePhone();
			}

			String delMethod = soi.getDeliveryMethod();
			if (delMethod == null) {
				delMethod = "C"; // default to carrier if not set
			}

			String fodCode = soi.getSelectedTerm().getFODCode();
			String durationType = soi.getSelectedTerm().getDurationType();

			StartSubscriptionResponse response = newStartAPI.createNewStart(soi.getProduct().getProductCode(), paymentInfo
					.getCardNumber(), paymentInfo.getCreditCardTypeCode(), ccExpDate, soi.getStartDateObj(), soi.getOrderID(),
					parser.getSourceCode(), parser.getPromoCode(), parser.getContestCode(), soi.getSelectedTerm().getPiaRateCode(),
					deliveryContact.getHomePhone(), deliveryContact.getBusinessPhone(), deliveryContact.getFirstName(),
					deliveryContact.getLastName(), deliveryContact.getEmailAddress(), secondaryEmail,
					deliveryContact.getFirmName(), address1Temp, deliveryContact.getUIAddress().getAddress2(), deliveryContact
							.getUIAddress().getCity(), deliveryContact.getUIAddress().getState(), deliveryContact.getUIAddress()
							.getZip(), bFirstName, bLastName, bFirmName, bAddress1, bAddress2, bCity, bState, bZip, billingPhone,
					soi.isGiftItem(), soi.isChoosingEZPay(), soi.isOneTimeBill(), null, null, fodCode, delMethod, soi
							.getSelectedTerm().getDuration(), durationType);

			gr.addResponse(response);

		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}

		return gr;
	}

	/**
	 * 
	 * @param account
	 * @param restartDate
	 * @return the perisisted transaction
	 * @throws UsatException
	 */

	public static GenesysResponse resumeSubscription(SubscriberAccountIntf account, DateTime restartDate, boolean nie)
			throws UsatException {

		if (account.getProduct().isElectronicDelivery()) {
			throw new UsatException("Cannot place delivery holds/restarts on electronicly delivered products.");
		}

		if (!account.isSubscriptionSuspended()) {
			throw new UsatException("Your account is not currently suspended.");
		}

		if (restartDate == null) {
			throw new UsatException("You must specify the date to resume delivery.");
		}

		SuspendResume suspendResumeAPI = new SuspendResume();

		//
		GenesysResponse gr = new GenesysResponse();
		String vacationRestart = "C";
		try {
			String restartDateStr = restartDate.toString("yyyyMMdd");
			if (nie) {
				vacationRestart = "D";
			}
			SuspendResumeResponse response = suspendResumeAPI.createSuspendResume(account.getPubCode(), account.getAccountNumber(),
					"", restartDateStr, vacationRestart);
			gr.addResponse(response);

		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}

		return gr;
	}

	/**
	 * 
	 * @param account
	 * @param holdStart
	 * @param holdEnd
	 * @param donate
	 * @return the persisted transactions
	 * @throws UsatException
	 */
	public static GenesysResponse suspendSubscription(SubscriberAccountIntf account, DateTime holdStart, DateTime holdEnd,
			boolean donate) throws UsatException {

		if (account.getProduct().isElectronicDelivery()) {
			throw new UsatException("Cannot place delivery holds on electronicly delivered products.");
		}

		if (holdStart == null) {
			throw new UsatException("You must specify the date to begin suspending delivery.");
		}

		SuspendResume suspendResumeAPI = new SuspendResume();

		//
		GenesysResponse gr = new GenesysResponse();

		try {
			String vacationRestart = "C";
			String holdStartStr = holdStart.toString("yyyyMMdd");

			String holdEndStr = "";
			if (holdEnd == null) {
				holdEndStr = "";
			} else {
				holdEndStr = holdEnd.toString("yyyyMMdd");
			}
			if (donate) {
				vacationRestart = "D";
			}
			SuspendResumeResponse response = suspendResumeAPI.createSuspendResume(account.getPubCode(), account.getAccountNumber(),
					holdStartStr, holdEndStr, vacationRestart);
			gr.addResponse(response);

		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}

		return gr;
	}

	/**
	 * 
	 * @param account
	 * @param holdStart
	 * @param holdEnd
	 * @param donate
	 * @return the persisted transactions
	 * @throws UsatException
	 */
	public static GenesysResponse updateEmail(String pubCode, String accountNumber, String emailAddress, String primaryEmail)
			throws UsatException {

		// hdcons-91

		UpdateEmail updateEmailAPI = new UpdateEmail();
		GenesysResponse gr = new GenesysResponse();

		try {

			UpdateEmailResponse response = updateEmailAPI.createUpdateEmail(pubCode, accountNumber, emailAddress, primaryEmail);
			gr.addResponse(response);

		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}

		return gr;
	}

	/**
	 * 
	 * @param account
	 * @param holdStart
	 * @param holdEnd
	 * @param donate
	 * @return the persisted transactions
	 * @throws UsatException
	 */
	public static GenesysResponse processNameChange(String pubCode, String accountNumber, String addressType, String firstName,
			String lastName) throws UsatException {

		NameChange nameChangeAPI = new NameChange();
		GenesysResponse gr = new GenesysResponse();

		try {
			NameChangeResponse response = nameChangeAPI.createNameChange(pubCode, accountNumber, addressType, firstName, lastName);
			gr.addResponse(response);
		} catch (Exception e) {
			throw new UsatException(e.getMessage());
		}
		return gr;
	}

	/**
	 * 
	 * @param account
	 * @param holdStart
	 * @param holdEnd
	 * @param donate
	 * @return the persisted transactions
	 * @throws UsatException
	 */
	public static GenesysResponse updateEmailAccounts(HashMap<String, SubscriberAccountIntf> accounts, String emailAddress,
			String primaryEmail) throws UsatException {

		String accountKey = "";
		String accountNumber = "";
		String pubCode = "";
		GenesysResponse gr = null;
		Iterator<String> aItr = accounts.keySet().iterator();
		while (aItr.hasNext()) {
			accountKey = aItr.next();
			accountNumber = accounts.get(accountKey).getAccountNumber();
			pubCode = accounts.get(accountNumber).getPubCode();
			UpdateEmail updateEmailAPI = new UpdateEmail();
			gr = new GenesysResponse();
			try {
				UpdateEmailResponse response = updateEmailAPI.createUpdateEmail(pubCode, accountNumber, emailAddress, primaryEmail);
				gr.addResponse(response);
			} catch (Exception e) {
				throw new UsatException(e.getMessage());
			}
		}
		return gr;
	}
}
