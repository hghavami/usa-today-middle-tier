package com.usatoday.businessObjects.customer.service;

import com.usatoday.UsatException;

/**
 * Insert the type's description here. Creation date: (4/1/2003 3:41:10 PM)
 * 
 * @author:
 */
public class UTComplaint extends Complaint {
	/**
	 * 
	 */
	public static final String NEWSPAPER_NOT_DELIVERED = "1";
	public static final String NEWSPAPER_WAS_LATE = "2";
	public static final String SUBSCRIPTION_DID_NOT_START = "3";
	public static final String SUBSCRIPTION_DID_NOT_RESTART = "4";
	public static final String NEWSPAPER_DELIVERED_DURING_VACA = "5";
	public static final String INCORRECT_NEWSPAPER_DELIVERED = "6";
	public static final String WET_NEWSPAPER = "7";
	public static final String TORN_NEWSPAPER = "8";
	public static final String MISSING_SECTION = "9";

	/**
	 * UTComplaint constructor comment.
	 */
	public UTComplaint() {
		super();
	}

	/**
	 * Insert the method's description here. Creation date: (5/1/2003 4:20:27 PM)
	 * 
	 * @param tranCodeStr
	 *            java.lang.String
	 */
	public void setTransactionCode(String tranCodeStr) throws UsatException {
		int tranCode = 0;

		try {
			tranCode = Integer.parseInt(tranCodeStr);

		} catch (NumberFormatException e) {
			throw new UsatException("UTComplaint::setTransactionCode() - Number Format Exception - Invalid Transaction Code: "
					+ tranCodeStr);
		}

		switch (tranCode) {
		case 1: {
			this.transactionCode = "MC"; // paper not delivered
			this.setIssueTypeText("Paper Not Delivered");
			break;
		}
		case 2: {
			this.transactionCode = "LN"; // Late Paper
			this.setIssueTypeText("Paper Was Late");
			break;
		}
		case 3: {
			this.transactionCode = "NC"; // Subscription did not start
			this.setIssueTypeText("Subscription Did Not Start");
			break;
		}
		case 4: {
			this.transactionCode = "VS"; // Subscription did not restart after vaca
			this.setIssueTypeText("Subscription Did Not Restart After Vacation");
			break;
		}
		case 5: {
			this.transactionCode = "VS"; // Paper delivered during vaca
			this.setIssueTypeText("Paper Delivered During Vacation");
			break;
		}
		case 6: {
			this.transactionCode = "PU"; // Incorrect paper delivered
			this.setIssueTypeText("Incorrect Publication Delivered");
			break;
		}
		case 7: // Wet Paper
			// this.transactionCode = "DC";
			this.transactionCode = "WC";
			this.setIssueTypeText("Wet Paper");
			break;
		case 8: // Torn Paper
			this.transactionCode = "DC";
			this.setIssueTypeText("Torn Paper");
			break;
		case 9: { // Missing Section
			this.transactionCode = "DC";
			this.setIssueTypeText("Missing Section");
			break;
		}
		default: {
			throw new UsatException("UTComplaint::setTransactionCode() - Invalid Complaint Specified." + tranCode);
		}
		}
	}

}
