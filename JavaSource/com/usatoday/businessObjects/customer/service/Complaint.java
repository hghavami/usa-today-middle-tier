package com.usatoday.businessObjects.customer.service;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.usatoday.UsatException;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * The <class>Complaint</class> serves as the base class for the delivery complaints supported on the extranet.
 * 
 * Creation date: (4/1/2003 2:35:24 PM)
 * 
 * @author: Andy East
 * 
 */
public class Complaint {
	/**
	 * 
	 */
	public final static int UT_COMPLAINT = 1;
	public final static int SW_COMPLAINT = 2;

	// genesys complaints
	public final static int ISSUE_NOT_DELIVERED = 1;
	public final static int ISSUE_WAS_DAMAGED = 2;
	public final static int ISSUE_WAS_WET = 3;

	protected String comment = null;
	private java.util.ArrayList<DateTime> issueDates = null;

	private DateTimeFormatter fmtMMddyyyy = DateTimeFormat.forPattern("MM/dd/yyyy");

	private java.lang.String isssueDateErrorMessage = null;
	private java.lang.String issueTypeText = null;
	protected String transactionCode = null;

	private boolean eRedelivery = false;

	/**
	 * Complaint constructor comment.
	 */
	public Complaint() {
		super();
	}

	/**
	 * Insert the method's description here. Creation date: (4/1/2003 3:33:28 PM)
	 * 
	 * @param newIssueDates
	 *            java.lang.String[]
	 */
	public void addIssueDate(java.lang.String issueDate) throws UsatException {

		DateTime dt = null;

		if (issueDate != null && issueDate.trim().length() > 0) {
			try {
				dt = this.fmtMMddyyyy.parseDateTime(issueDate);

			} catch (Exception e) {
				throw new UsatException("Invalid Date Entered. Please check: " + issueDate);
			}

			if (!this.getIssueDates().contains(dt)) {
				this.applyDateRules(dt);
				this.getIssueDates().add(dt);
			}
		}
	}

	public void addIssueDate(Date issueDate) throws UsatException {

		if (issueDate != null) {
			DateTime dt = new DateTime(issueDate.getTime());

			if (!this.getIssueDates().contains(dt)) {
				this.applyDateRules(dt);
				this.getIssueDates().add(dt);
			}
		}
	}

	/**
	 * Subclasses should override this method to add additional pub specific rules. but this method should be invoked as well.
	 * Creation date: (5/6/2003 9:55:13 AM)
	 * 
	 * @param date
	 *            java.util.Date
	 * @exception com.usatoday.esub.account.complaints.ComplaintException
	 *                The exception description.
	 */
	protected void applyDateRules(DateTime date) throws UsatException {
		// enforce issue date business rules

		DateTime now = new DateTime();

		// No future dates
		if (date.isAfter(now)) {
			throw new UsatException("Issue dates cannot be future dates");
		}

		// publish dates validated on back end
		/*
		 * SubscriptionPublishDateUtility du = SubscriptionPublishDateUtility.getInstance();
		 * 
		 * if(!du.isValidPublishingDate(this.getProductCode(), date)) { StringBuilder errorBuf = new StringBuilder(); ProductIntf p
		 * = SubscriptionProductBO.fetchProduct(this.getProductCode());
		 * errorBuf.append(p.getName()).append(" was not published on selected date: ");
		 * errorBuf.append(date.toString("MM/dd/yyyy")); throw new UsatException(errorBuf.toString()); }
		 */
	}

	/**
	 * Insert the method's description here. Creation date: (5/6/2003 11:03:28 AM)
	 */
	public void clearIssueDates() {
		this.isssueDateErrorMessage = null;
		this.getIssueDates().clear();
		// clear the 5 user entered dates.
	}

	/**
	 * Insert the method's description here. Creation date: (5/1/2003 12:59:48 PM)
	 * 
	 * @return com.usatoday.esub.account.complaints.Complaint
	 * @param type
	 *            int
	 * @exception com.usatoday.esub.account.complaints.InvalidComplaintException
	 *                The exception description.
	 */
	public static final Complaint complaintFactory(String pubCode) throws UsatException {

		Complaint newComplaint = null;

		int type = -1;
		if (UsaTodayConstants.UT_PUBCODE.equalsIgnoreCase(pubCode)) {
			type = Complaint.UT_COMPLAINT;
		} else if (UsaTodayConstants.SW_PUBCODE.equalsIgnoreCase(pubCode)) {
			type = Complaint.SW_COMPLAINT;
		}

		switch (type) {
		case Complaint.UT_COMPLAINT: {
			newComplaint = new UTComplaint();
			break;
		}
		case Complaint.SW_COMPLAINT: {
			newComplaint = new SWComplaint();
			break;
		}
		default: {
			newComplaint = new Complaint();
			// throw new UsatException("Invalide Complaint Type: " + type);
		}
		}

		return newComplaint;
	}

	/**
	 * Insert the method's description here. Creation date: (5/5/2003 3:52:24 PM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIsssueDateErrorMessage() {
		return isssueDateErrorMessage;
	}

	/**
	 * Insert the method's description here. Creation date: (4/1/2003 3:33:28 PM)
	 * 
	 * @return java.lang.String[]
	 */
	public java.util.ArrayList<DateTime> getIssueDates() {
		if (issueDates == null) {
			issueDates = new java.util.ArrayList<DateTime>();
		}
		return issueDates;
	}

	/**
	 * Insert the method's description here. Creation date: (5/7/2003 2:09:21 PM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getIssueTypeText() {
		return issueTypeText;
	}

	public String getIssueDatesAsHTMLString() {
		StringBuilder datesString = new StringBuilder();
		if (this.issueDates != null) {
			for (DateTime d : this.issueDates) {
				datesString.append(d.toString("MM/dd/yyyy")).append("<br>");
			}
		}
		return datesString.toString();
	}

	/**
	 * Insert the method's description here. Creation date: (5/13/2003 3:08:10 PM)
	 * 
	 * @return boolean
	 */
	public boolean isComplaintValid() {

		if (!this.issueDatesValid()) {
			return false;
		}

		return true;
	}

	/**
	 * This method checks that the issue dates entered meet the business rules for complaints. Subclasses may override if necessary.
	 * Creation date: (5/5/2003 3:47:56 PM)
	 * 
	 * @return boolean
	 */
	public boolean issueDatesValid() {
		if (this.getIssueDates().size() == 0) {
			this.setIsssueDateErrorMessage("At least one issue date is required.");
			return false;
		}

		return true;
	}

	/**
	 * Insert the method's description here. Creation date: (5/5/2003 3:52:24 PM)
	 * 
	 * @param newIsssueDateErrorMessage
	 *            java.lang.String
	 */
	protected void setIsssueDateErrorMessage(java.lang.String newIsssueDateErrorMessage) {
		isssueDateErrorMessage = newIsssueDateErrorMessage;
	}

	/**
	 * Insert the method's description here. Creation date: (5/7/2003 2:09:21 PM)
	 * 
	 * @param newIssueTypeText
	 *            java.lang.String
	 */
	protected void setIssueTypeText(java.lang.String newIssueTypeText) {
		issueTypeText = newIssueTypeText;
	}

	public boolean isERedelivery() {
		return this.eRedelivery;
	}

	public void setERedelivery(boolean redelivery) {
		this.eRedelivery = redelivery;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	/**
	 * This method should be ovrridden if product requires special codes
	 * 
	 * @param tranCodeStr
	 * @throws UsatException
	 */
	public void setTransactionCode(String tranCodeStr) throws UsatException {
		int tranCode = 0;

		try {
			tranCode = Integer.parseInt(tranCodeStr);

		} catch (NumberFormatException e) {
			throw new UsatException("Complaint::setTransactionCode() - Number Format Exception - Invalid Transaction Code: "
					+ tranCodeStr);
		}

		switch (tranCode) {
		case 1: {
			this.transactionCode = "MC"; //
			this.setIssueTypeText("Issue Not Delivered");
			break;
		}
		case 2: {
			this.transactionCode = "DC"; //
			this.setIssueTypeText("Issue Was Damaged");
			break;
		}
		case 3: {
			this.transactionCode = "WC"; //
			this.setIssueTypeText("Issue Was Wet");
			break;
		}
		default: {
			throw new UsatException("Complaint::setTransactionCode() - Invalid Complaint Specified." + tranCode);
		}
		}
	}

}
