package com.usatoday.businessObjects.customer.service;

import com.usatoday.UsatException;

/**
 * Insert the type's description here. Creation date: (4/1/2003 3:40:25 PM)
 * 
 * @author:
 */
public class SWComplaint extends Complaint {

	public static final String ISSUE_NOT_DELIVERED = "1";
	public static final String ISSUE_WAS_LATE = "2";
	public static final String SUBSCRIPTION_DID_NOT_START = "3";
	public static final String ISSUE_WAS_TORN = "4";

	/**
	 * SWComplaint constructor comment.
	 */
	public SWComplaint() {
		super();

	}

	/**
	 * Insert the method's description here. Creation date: (4/1/2003 3:40:25 PM)
	 * 
	 * @param newPubCode
	 *            java.lang.String
	 */

	/**
	 * Insert the method's description here. Creation date: (5/1/2003 4:19:17 PM)
	 * 
	 * @param tranCodeStr
	 *            java.lang.String
	 */
	public void setTransactionCode(String tranCodeStr) throws UsatException {
		int tranCode = 0;

		try {
			tranCode = Integer.parseInt(tranCodeStr);

		} catch (NumberFormatException e) {
			throw new UsatException("SWComplaint::setTransactionCode() - Number Format Exception - Invalid Transaction Code: "
					+ tranCodeStr);
		}

		switch (tranCode) {
		case 1: {
			this.transactionCode = "MC"; // issue not delivered
			this.setIssueTypeText("Issue Not Delivered");
			break;
		}
		case 2: {
			this.transactionCode = "LN"; // Late Issue
			this.setIssueTypeText("Issue Was Late");
			break;
		}
		case 3: {
			this.transactionCode = "NC"; // Subscription did not start
			this.setIssueTypeText("Subscriptoin Did Not Start");
			break;
		}
		case 4: {
			this.transactionCode = "DC"; // Torn Issue
			this.setIssueTypeText("Issue Was Torn");
			break;
		}
		default: {
			throw new UsatException("SWComplaint::setTransactionCode() - Invalid Complaint Specified." + tranCode);
		}
		}
	}

}
