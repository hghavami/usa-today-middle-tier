package com.usatoday.businessObjects.customer.service;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.shopping.OrderIntf;
import com.usatoday.business.interfaces.shopping.OrderItemIntf;
import com.usatoday.business.interfaces.shopping.RenewalOrderItemIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentMethodIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentMethodBO;
import com.usatoday.util.constants.UsaTodayConstants;

public class CustomerServiceEmails {

	public static String sendComplaintConfirmationEmail(CustomerIntf customer, SubscriberAccountIntf account, Complaint complaint)
			throws UsatException {
		String emailMessage = null;
		try {
			String pubcode = account.getPubCode();
			String phone = "1-800-872-0001.";

			SubscriptionProductIntf product = null;
			SubscriptionProductIntf brandingProduct = null;
			try {
				product = SubscriptionProductBO.getSubscriptionProduct(pubcode);
				if (product == null) {
					product = SubscriptionProductBO.getUsaTodaySubscriptionProduct();
				}
				brandingProduct = SubscriptionProductBO.getSubscriptionProduct(product.getBrandingPubCode());
				phone = product.getCustomerServicePhone();
			} catch (Exception e) {
				;
			}

			ContactIntf deliveryContact = account.getDeliveryContact();

			String emailSubject = CustomerServiceEmails.getGeneralSubjectForProduct(product);

			StringBuilder emailFormat = new StringBuilder();
			emailFormat.append("Thank you for contacting " + brandingProduct.getName()
					+ " regarding your delivery issue.\nWe apologize for the inconvenience.  " + "\n \n"
					+ "If your issue is not resolved within 1-2 business days, please call us at " + phone + "\n \n"
					+ "Your current delivery information is as follows:" + "\n \n" + deliveryContact.getFirstName() + " "
					+ deliveryContact.getLastName() + "\n");

			if ((deliveryContact.getFirmName() != null) && (!deliveryContact.getFirmName().equals(""))) {
				emailFormat.append(deliveryContact.getFirmName());
				emailFormat.append("\n");
			}

			emailFormat.append(deliveryContact.getUIAddress().getAddress1());

			if (deliveryContact.getUIAddress().getAptSuite() != null
					&& (!deliveryContact.getUIAddress().getAptSuite().equalsIgnoreCase(""))) {
				emailFormat.append("  ").append(deliveryContact.getUIAddress().getAptSuite());
			}
			emailFormat.append("\n");

			if ((deliveryContact.getUIAddress().getAddress2() != null)
					&& (!deliveryContact.getUIAddress().getAddress2().equals(""))) {
				emailFormat.append(deliveryContact.getUIAddress().getAddress2());
				emailFormat.append("\n");
			}

			emailFormat.append(deliveryContact.getUIAddress().getCity()).append(", ")
					.append(deliveryContact.getUIAddress().getState()).append("  ").append(deliveryContact.getUIAddress().getZip());
			emailFormat.append("\n");

			if ((deliveryContact.getHomePhone() != null)) {
				emailFormat.append("Home Telephone Number:  ");
				emailFormat.append(deliveryContact.getHomePhoneNumberFormatted());
				emailFormat.append("\n");
			}

			emailFormat.append("\n \nTo access Online Customer Service in the future, just go to");

			if (brandingProduct.getProductCode().equals(UsaTodayConstants.UT_PUBCODE)) {
				emailFormat.append(" www.usatodayservice.com.\n\n");
			} else {
				emailFormat.append(" www.mysportsweekly.com.\n\n");
			}

			emailFormat.append(CustomerServiceEmails.getUsatMailingAddressFooter(pubcode));

			emailMessage = emailFormat.toString();
			String fromAddress = UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS;

			CustomerServiceEmails.sendAnEmail(fromAddress, customer.getCurrentEmailRecord().getEmailAddress(), emailSubject,
					emailMessage);

		} catch (UsatException ue) {
			throw ue;
		} catch (Exception e) {
			throw new UsatException(e);
		}
		return emailMessage;
	}

	public static String sendSuspendResumeDeliveryEmail(CustomerIntf customer, DateTime holdStart, DateTime holdEnd,
			boolean donate, String promoText) throws Exception {

		SubscriberAccountIntf account = customer.getCurrentAccount();

		String pubcode = account.getPubCode();
		String phone = " 1-800-872-0001.";
		String webSiteURL = "www.usatodayservice.com";

		boolean isUT = true;

		SubscriptionProductIntf product = null;
		SubscriptionProductIntf brandingProduct = null;
		try {
			product = account.getProduct();
			if (product == null) {
				product = SubscriptionProductBO.getUsaTodaySubscriptionProduct();
			}
			brandingProduct = SubscriptionProductBO.getSubscriptionProduct(product.getBrandingPubCode());
			phone = product.getCustomerServicePhone();
		} catch (Exception e) {
			;
		}

		if (brandingProduct != null && brandingProduct.getProductCode().equals(UsaTodayConstants.BW_PUBCODE)) {
			webSiteURL = "www.mysportsweekly.com";
			isUT = false;
		}

		String homephone = account.getDeliveryContact().getHomePhoneNumberFormatted();

		String workphone = account.getDeliveryContact().getWorkPhoneNumberFormatted();

		String address1 = null;

		address1 = account.getDeliveryContact().getUIAddress().getAddress1();
		if (account.getDeliveryContact().getUIAddress().getAptSuite() != null) {
			address1 += " " + account.getDeliveryContact().getUIAddress().getAptSuite();
		}

		String address2 = account.getDeliveryContact().getUIAddress().getCity() + ", "
				+ account.getDeliveryContact().getUIAddress().getState() + " "
				+ account.getDeliveryContact().getUIAddress().getZip();

		String firmName = account.getDeliveryContact().getFirmName();
		String firstName = account.getDeliveryContact().getFirstName();
		String lastName = account.getDeliveryContact().getLastName();
		StringBuffer emailformat = new StringBuffer("");
		String emailMessage = null;
		String addlAddress1 = account.getDeliveryContact().getUIAddress().getAddress2();

		String companyName = "USA TODAY";
		if (brandingProduct != null) {
			companyName = brandingProduct.getName();
		}

		emailformat.append("Thank you for informing " + companyName + " of your subscription needs");

		if (account.isSubscriptionSuspended()) {
			emailformat.append(". You have asked us to restart your delivery on ");
			emailformat.append(holdEnd.toString("MM/dd/yyyy"));
		} else if (holdEnd == null) {

			emailformat.append(" for account number ");
			emailformat.append(account.getAccountNumber());
			emailformat.append(".\n");
			emailformat.append("You have asked us to stop your delivery on ");
			emailformat.append(holdStart.toString("MM/dd/yyyy"));
			emailformat.append(".\n");
			emailformat
					.append("You have not yet provided a restart date. When you are ready to resume delivery, please contact us at ");
			emailformat.append(webSiteURL);
			emailformat.append(" or");
			emailformat.append(phone);
		} else { // start and stop
			emailformat.append(". You have asked us to stop your delivery on ");
			emailformat.append(holdStart.toString("MM/dd/yyyy"));
			emailformat.append("\n");
			emailformat.append("and resume it on ");
			emailformat.append(holdEnd.toString("MM/dd/yyyy"));
			emailformat.append(" for account number ");
			emailformat.append(account.getAccountNumber());
			emailformat.append(".");
		}

		emailformat.append("\n \nDelivery information is as follows:\n \n");
		emailformat.append(firstName);
		emailformat.append(" ");
		emailformat.append(lastName);
		emailformat.append("\n");

		if ((firmName != null) && (!firmName.trim().equals(""))) {
			emailformat.append(firmName);
			emailformat.append("\n");
		}

		emailformat.append(address1);
		emailformat.append("\n");

		if ((addlAddress1 != null) && (!addlAddress1.trim().equals(""))) {
			emailformat.append(addlAddress1);
			emailformat.append("\n");
		}

		emailformat.append(address2);
		emailformat.append("\n");

		if ((homephone != null) && (!homephone.trim().equals(""))) {
			emailformat.append("Home Telephone Number:  ");
			emailformat.append(homephone);
			emailformat.append("\n");
		}

		if ((workphone != null) && (!workphone.trim().equals(""))) {
			emailformat.append("Work Telephone: ");
			emailformat.append(workphone);
			emailformat.append("\n");
		}

		// Check for Newspaper in Education program flag. Do not display if nie
		// is yes and doing a vacation restart

		if (donate) {
			emailformat.append("\nThank you for donating your newspaper issues to the USA TODAY Charitable Foundation.\n");
		} else {
			if (isUT) {
				emailformat.append("\nYour subscription will automatically be credited by the number of days your newspaper ");
			} else {
				emailformat.append("\nYour subscription will automatically be credited by the number of weeks/months your magazine ");
			}

			if (account.isSubscriptionSuspended()) {
				emailformat.append("was");
			} else {
				emailformat.append("is");
			}

			emailformat.append(" on hold.\n");
		}

		if (holdEnd != null) {
			emailformat.append("\nIf you have any questions about your subscription, please call us at");
			emailformat.append(phone);

			emailformat.append("\n \nTo access Online Customer Service in the future, just go to ");

			emailformat.append(webSiteURL);
			emailformat.append(".");
		}

		// PROMO TEXT
		if (promoText != null) {
			emailformat.append("\n\n").append(promoText);
		}

		String footerPub = pubcode;
		if (brandingProduct != null) {
			footerPub = brandingProduct.getProductCode();
		}
		emailformat.append("\n\n").append(CustomerServiceEmails.getUsatMailingAddressFooter(footerPub));

		emailMessage = emailformat.toString();

		String subject = CustomerServiceEmails.getGeneralSubjectForProduct(brandingProduct);
		String fromAddress = UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS;

		// send the message
		CustomerServiceEmails.sendAnEmail(fromAddress, customer.getCurrentEmailRecord().getEmailAddress(), subject, emailMessage);

		return emailMessage;

	}

	public static void sendRenewalConfirmationEmail(CustomerIntf customer, SubscriberAccountIntf account, OrderIntf order)
			throws UsatException {

		if (customer == null || order == null) {
			throw new UsatException(
					"Attempt to call renewal confirmation (CustomerServiceEmail:sendRenewalConfirmation() without a valid customer or order");
		}

		try {
			RenewalOrderItemIntf item = null;
			for (OrderItemIntf tempItem : order.getOrderedItems()) {
				if (tempItem instanceof RenewalOrderItemIntf) {
					item = (RenewalOrderItemIntf) tempItem;
					break;
				}
			}

			if (item == null) {
				throw new UsatException(
						"Attempt to call renewal confirmation (CustomerServiceEmail:sendRenewalConfirmation() without a valid renewal item in the order. Order ID: "
								+ order.getOrderID());
			}

			ProductIntf product = item.getProduct();

			String emailSubject = CustomerServiceEmails.getGeneralSubjectForProduct(product);

			StringBuilder email = new StringBuilder("\n \n");

			String phone = product.getCustomerServicePhone();

			NumberFormat formatter = NumberFormat.getCurrencyInstance();
			String ccnum = null;
			// If no initial bill me transaction then get the credit card
			// information
			if (order.getPaymentMethod() instanceof CreditCardPaymentMethodIntf) {
				ccnum = order.getPaymentMethod().getDisplayValue();
			}

			// TODO Houman or SAM - update to support monthly terms getDurationType == "M"
			String terms = item.getSelectedTerm().getDuration();

			terms = Integer.valueOf(terms).toString(); // remove leading 00's

			email.append("\nThank you for your subscription to ");

			String productName = product.getName();

			email.append(productName);
			email.append(".");

			email.append("  You have selected to continue ");

			email.append(productName);
			email.append(" for ");
			email.append(terms);
			String durationType = item.getSelectedTerm().getDurationType();
			if (durationType.equals("M")) {
				email.append(" months.\n"); 			
			} else {
				email.append(" weeks.\n");			
			}
			if (!item.isChoosingEZPay()) {
				email.append("The total amount billed to your credit card, including taxes of ");				
				email.append(formatter.format(order.getTaxAmount()));
				if (item.getQuantity() > 1) {
					email.append(" for " + item.getQuantity() + " copies");
				}
				email.append(" was ");
				email.append(formatter.format(order.getTotal()));
				email.append(".\n");
			}
			email.append("The credit card ended with the following 4 digits: ");
			email.append(ccnum).append(".");

			if (item.isChoosingEZPay()) {
				email.append("\n \n");
				email.append("You have enrolled in EZ-PAY.  ");
				email.append("\n");
				email.append("At the end of your term, your credit card will be charged automatically to continue");
				email.append(" the subscription at the current published rate unless you notify us otherwise.");
			}
			email.append("\n \nDelivery Information is as follows:\n \n");

			ContactIntf contact = account.getDeliveryContact();

			if ((contact.getLastName() != null) && contact.getLastName().length() > 0) {
				email.append(contact.getFirstName());
				email.append(" ");
				email.append(contact.getLastName());
				email.append("\n");
			}

			if ((contact.getFirmName() != null) && (contact.getFirmName().length() > 0)) {
				email.append(contact.getFirmName());
				email.append("\n");
			}

			UIAddressIntf address = null;

			if (account.getDeliveryContact().getPersistentAddress() != null) {
				address = account.getDeliveryContact().getPersistentAddress().convertToUIAddress();
			} else {
				address = account.getDeliveryContact().getUIAddress();
			}

			if (address == null) {
				throw new UsatException("CustomerServiceEmails:sendRenewalConfirmation() - Delivery Address object is null.");
			}

			String address1 = address.getAddress1();

			if (address.getAptSuite() != null && address.getAptSuite().length() > 0) {
				address1 = address1 + " " + address.getAptSuite();
			}
			email.append(address1.trim());
			email.append("\n");

			if (address.getAddress2() != null && address.getAddress2().length() > 0) {
				email.append(address.getAddress2()).append("\n");
			}

			String address2 = address.getCity() + ", " + address.getState() + " " + address.getZip();

			if ((address2 != null) && (address2.length() > 0)) {
				email.append(address2.trim());
				email.append("\n");
			}

			email.append("Home Telephone Number: ");
			email.append(contact.getHomePhoneNumberFormatted());
			email.append("\n");

			if ((contact.getBusinessPhone() != null && contact.getBusinessPhone().length() > 0)) {
				email.append("Work Telephone Number: ");
				email.append(contact.getWorkPhoneNumberFormatted());
				email.append("\n");
			}

			email.append("\n \n");
			email.append("*Final credit card verification is pending.");

			email.append("\n \n");
			email.append(" If you have any questions about your subscription, please call us at ");
			email.append(phone);
			email.append("\n \n");

			email.append(" To access Online Customer Service in the future, just go to ");

			if (product == null || product.getBrandingPubCode().equals(UsaTodayConstants.BW_PUBCODE)) {
				email.append("http://www.mysportsweekly.com");
			} else {
				email.append("http://service.usatoday.com");
			}

			// PROMO TEXT
			if (UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT != null && UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT.length() > 0) {
				email.append("\n\n").append(UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT);
			}

			email.append("\n\n").append(CustomerServiceEmails.getUsatMailingAddressFooter(product.getProductCode()));

			CustomerServiceEmails.sendAnEmail(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS, customer.getCurrentEmailRecord()
					.getEmailAddress(), emailSubject, email.toString());

		} catch (UsatException e) {
			throw e;
		} catch (Exception e) {
			throw new UsatException(e);
		}
	}

	protected static String getGeneralSubjectForProduct(ProductIntf product) {

		StringBuilder subject = new StringBuilder();

		subject.append(product.getName()).append(" Subscription Notification");

		return subject.toString();
	}

	protected static String getUsatMailingAddressFooter(String pubcode) {
		if (pubcode == null) {
			return "USA TODAY, 7950 Jones Branch Dr, McLean, Virginia, 22108";
		} else if (pubcode.equalsIgnoreCase(UsaTodayConstants.BW_PUBCODE)) {
			return "USA TODAY Sports Weekly, 7950 Jones Branch Dr, McLean, Virginia, 22108";
		} else {
			return "USA TODAY, 7950 Jones Branch Dr, McLean, Virginia, 22108";
		}
	}

	protected static void sendAnEmail(String fromEmail, String toEmail, String subject, String message) throws Exception {
		if ((toEmail == null) || (fromEmail == null) || (subject == null) || (message == null)) {
			if (UsaTodayConstants.debug) {
				System.out.println("Email send request aborted: toEmail='" + toEmail + "'  fromEmail='" + fromEmail + "' subject='"
						+ subject + "'  message='" + message + "'");
			}
			return;
		}

		com.usatoday.businessObjects.util.mail.SmtpMailSender mailSender = new com.usatoday.businessObjects.util.mail.SmtpMailSender();
		mailSender.setMailServerHost(UsaTodayConstants.EMAIL_SERVER);
		mailSender.setMessageSubject(subject);
		mailSender.setMessageText(message);
		mailSender.setSender(fromEmail);
		mailSender.addTORecipient(toEmail);
		mailSender.sendMessage();

		try {
			// check if sending a copy of email to usat
			if (UsaTodayConstants.EMAIL_CONFIRMATIONS_COPYTO_ADDRESS != null
					&& UsaTodayConstants.EMAIL_CONFIRMATIONS_COPYTO_ADDRESS.length() > 0) {

				mailSender = new com.usatoday.businessObjects.util.mail.SmtpMailSender();
				mailSender.setMailServerHost(UsaTodayConstants.EMAIL_SERVER);
				mailSender.setMessageSubject(subject + " [COPY]");
				mailSender.setMessageText(message);
				mailSender.setSender(fromEmail);
				mailSender.addTORecipient(UsaTodayConstants.EMAIL_CONFIRMATIONS_COPYTO_ADDRESS);
				mailSender.sendMessage();

			}
		} catch (Exception e) {
			System.out.println("Info Only: Model Code Failed to send copy of email to dev. " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * 
	  */
	public static void generateChangeDeliveryConfirmationEmailBody(CustomerIntf customer, ContactBO deliveryContact,
			String deliveryMethod, Date changeDeliveryAddressDate, boolean transType3) throws UsatException {

		if (customer == null) {
			throw new UsatException(
					"Attempt to call Change Billing confirmation (CustomerServiceEmail:sendChangeBillingConfirmation() without a valid customer");
		}

		try {
			String pubcode = customer.getCurrentAccount().getPubCode();
			String phone = " 1-800-872-0001.";
			SubscriptionProductIntf product = null;
			SubscriptionProductIntf brandingProduct = null;
			try {
				product = SubscriptionProductBO.getSubscriptionProduct(pubcode);
				if (product == null) {
					product = SubscriptionProductBO.getUsaTodaySubscriptionProduct();
				}
				brandingProduct = SubscriptionProductBO.getSubscriptionProduct(product.getBrandingPubCode());
				phone = product.getCustomerServicePhone();
			} catch (Exception e) {
				;
			}
			String emailSubject = CustomerServiceEmails.getGeneralSubjectForProduct(product);
			String days = "2 business days.";
			String deliveryMethodText = "";
			String guiCompleteText = "";

			if (brandingProduct != null && brandingProduct.getProductCode().equals(UsaTodayConstants.BW_PUBCODE)) {
				days = "seven to ten business days.";
			}

			String homephone = deliveryContact.getHomePhoneNumberFormatted();
			String workphone = deliveryContact.getWorkPhoneNumberFormatted();

			// Check for delivery method
			if (product != null && !product.isElectronicDelivery()) {
				if (deliveryMethod != null && (deliveryMethod.equals("M") || deliveryMethod.equals("L"))) {
					deliveryMethodText = "You will receive your subscription via mail delivery.";
				}
				if (deliveryMethod != null && (deliveryMethod.equals("C") || deliveryMethod.equals("A"))) {
					deliveryMethodText = "You will receive your subscription via morning delivery.";
				}
			}

			boolean guiStatus = false;
			// boolean guiStatus = deliveryContact.getPersistentAddress().isGUIAddress();
			if (guiStatus) {
				guiCompleteText = "In order to provide you with early morning delivery, the delivery "
						+ "of your newspaper will be handled by an independent agent. Due to this, you will not "
						+ "be able to interact with your account on our subscription website. If you have any questions "
						+ "about your subscription, please contact USA TODAY National Customer Service at 1-800-872-0001.";
			}
			String addlAddress1 = deliveryContact.getPersistentAddress().getAddress2();
			String addlAddress2 = "";
			String address1 = null;
			String address2 = "";
			StringBuffer formattedAddr = new StringBuffer("");

			if (deliveryContact.getPersistentAddress().getUnitNum() != null
					&& deliveryContact.getPersistentAddress().getUnitNum().trim().length() > 0) {
				formattedAddr.append(deliveryContact.getPersistentAddress().getUnitNum());
				formattedAddr.append(" ");
			}

			if (deliveryContact.getPersistentAddress().getStreetDir() != null
					&& deliveryContact.getPersistentAddress().getStreetDir().trim().length() > 0) {
				formattedAddr.append(deliveryContact.getPersistentAddress().getStreetDir());
				formattedAddr.append(" ");
			}

			if (deliveryContact.getPersistentAddress().getStreetName() != null
					&& deliveryContact.getPersistentAddress().getStreetName().trim().length() > 0) {
				formattedAddr.append(deliveryContact.getPersistentAddress().getStreetName());
				formattedAddr.append(" ");
			}

			if (deliveryContact.getPersistentAddress().getStreetType() != null
					&& deliveryContact.getPersistentAddress().getStreetType().trim().length() > 0) {
				formattedAddr.append(deliveryContact.getPersistentAddress().getStreetType());
				formattedAddr.append(" ");
			}

			if (deliveryContact.getPersistentAddress().getStreetPostDir() != null
					&& deliveryContact.getPersistentAddress().getStreetPostDir().trim().length() > 0) {
				formattedAddr.append(deliveryContact.getPersistentAddress().getStreetPostDir());
				formattedAddr.append(", ");
			}

			if (deliveryContact.getPersistentAddress().getSubUnitCode() != null
					&& deliveryContact.getPersistentAddress().getSubUnitCode().trim().length() > 0) {
				formattedAddr.append(deliveryContact.getPersistentAddress().getSubUnitCode());
				formattedAddr.append(" ");
			}

			if (deliveryContact.getPersistentAddress().getSubUnitNum() != null
					&& deliveryContact.getPersistentAddress().getSubUnitNum().trim().length() > 0) {
				formattedAddr.append(deliveryContact.getPersistentAddress().getSubUnitNum());
				formattedAddr.append(" ");
			}

			address1 = formattedAddr.toString();
			address2 = deliveryContact.getPersistentAddress().getCity() + ", " + deliveryContact.getPersistentAddress().getState()
					+ " " + deliveryContact.getPersistentAddress().getZip();

			String firmName = deliveryContact.getFirmName();
			String firstName = deliveryContact.getFirstName();
			String lastName = deliveryContact.getLastName();
			StringBuffer emailformat = new StringBuffer("");

			String companyName = "USA TODAY";
			if (brandingProduct != null) {
				companyName = brandingProduct.getName();
			}

			emailformat.append("Thank you for informing " + companyName
					+ " of your subscription needs.  You have asked us to change \n" + "the delivery ");
			if (changeDeliveryAddressDate != null) {
				emailformat.append("address");
			} else {
				emailformat.append("information");
			}

			emailformat.append(" for account number " + customer.getCurrentAccount().getAccountNumber() + ".");
			if (!transType3) {
				emailformat.append("  This change will occur within " + days);
			} else {
				emailformat.append("  This change will occur on "
						+ (new SimpleDateFormat("MM/dd/yyyy").format(changeDeliveryAddressDate)) + ".");
			}
			emailformat.append("\n\n");

			// Check for type 3 transaction (address actually was changed)
			if (transType3) {
				emailformat.append("When it does, the delivery address ");
			} else {
				emailformat.append("The new delivery information ");
			}
			emailformat.append("will reflect the following: \n" + "\n" + firstName.trim() + " " + lastName.trim() + "\n");

			if ((firmName != null) && (!firmName.trim().equals(""))) {
				emailformat.append(firmName);
				emailformat.append("\n");
			}

			emailformat.append(address1);
			emailformat.append("\n");

			if ((addlAddress1 != null) && (!addlAddress1.trim().equals(""))) {
				emailformat.append(addlAddress1);
				emailformat.append("\n");
			}

			if ((addlAddress2 != null) && (!addlAddress2.trim().equals(""))) {
				emailformat.append(addlAddress2);
				emailformat.append("\n");
			}

			emailformat.append(address2);
			emailformat.append("\n");

			if ((homephone != null) && (!homephone.trim().equals(""))) {
				emailformat.append("Home Telephone:  ");
				emailformat.append(homephone);
				emailformat.append("\n");
			}

			if ((workphone != null) && (!workphone.trim().equals(""))) {
				emailformat.append("Work Telephone:  ");
				emailformat.append(workphone);
				emailformat.append("\n");
			}
			if (!deliveryMethodText.trim().equals("")) {
				emailformat.append("\n");
				emailformat.append(deliveryMethodText);
				emailformat.append("\n \n");
			} else {
				emailformat.append("\n");
			}
			if (!guiCompleteText.trim().equals("")) {
				emailformat.append(guiCompleteText);
				emailformat.append("\n \n");
			}

			emailformat.append("If you have any questions about your subscription, please call us at");
			emailformat.append(phone);

			emailformat.append("\n \n");
			emailformat.append("To access Online Customer Service in the future, just go to");

			if (brandingProduct != null && brandingProduct.getProductCode().equalsIgnoreCase(UsaTodayConstants.BW_PUBCODE)) {
				emailformat.append(" www.mysportsweekly.com.");
			} else {
				emailformat.append(" www.usatodayservice.com.");
			}

			// PROMO TEXT
			if (UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT != null && UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT.length() > 0) {
				emailformat.append("\n\n").append(UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT);
			}

			emailformat.append("\n\n").append(CustomerServiceEmails.getUsatMailingAddressFooter(product.getProductCode()));

			/*
			 * String footerPub = pubcode; if (brandingProduct != null) { footerPub = brandingProduct.getProductCode(); }
			 * emailformat.append ("\n\n").append(getUsatMailingAddressFooter(footerPub));
			 */
			CustomerServiceEmails.sendAnEmail(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS, customer.getCurrentEmailRecord()
					.getEmailAddress(), emailSubject, emailformat.toString());
		} catch (UsatException e) {
			throw e;
		} catch (Exception e) {
			throw new UsatException(e);
		}
	}

	/**
	 * 
	  */
	public static void generateChangeBillingConfirmationEmailBody(CustomerIntf customer, ContactBO billingContact,
			CreditCardPaymentMethodBO ccPayment) throws UsatException {

		if (customer == null) {
			throw new UsatException(
					"Attempt to call Change Delivery confirmation (CustomerServiceEmail:sendChangeBillingConfirmation() without a valid customer");
		}

		try {

			String pubcode = customer.getCurrentAccount().getPubCode();
			String phone = " 1-800-872-0001.";

			SubscriptionProductIntf product = null;
			SubscriptionProductIntf brandingProduct = null;
			try {
				product = SubscriptionProductBO.getSubscriptionProduct(pubcode);
				if (product == null) {
					product = SubscriptionProductBO.getUsaTodaySubscriptionProduct();
				}
				brandingProduct = SubscriptionProductBO.getSubscriptionProduct(product.getBrandingPubCode());
				phone = product.getCustomerServicePhone();
			} catch (Exception e) {
				;
			}

			String emailSubject = CustomerServiceEmails.getGeneralSubjectForProduct(product);
			String days = "four to seven business days";
			String ccNum = "";
			String ccExp = "";
			String currCCNum = "************" + customer.getCurrentAccount().getCreditCardNum();
			String currCCExp = customer.getCurrentAccount().getCreditCardExpMonth()
					+ customer.getCurrentAccount().getCreditCardExpYear();
			String addlAddress1 = "";
			String addlAddress2 = "";
			String address1 = null;
			String address2 = "";
			String firmName = "";
			String firstName = "";
			String lastName = "";
			String homephone = "";
			StringBuffer formattedAddr = new StringBuffer("");

			if (brandingProduct != null && brandingProduct.getProductCode().equals(UsaTodayConstants.BW_PUBCODE)) {
				days = "seven to ten business days.";
			}
			if (billingContact != null) {
				homephone = billingContact.getHomePhoneNumberFormatted();

				addlAddress1 = billingContact.getPersistentAddress().getAddress2();
				addlAddress2 = "";
				address1 = null;
				address2 = "";

				if (billingContact.getPersistentAddress().getUnitNum() != null
						&& billingContact.getPersistentAddress().getUnitNum().trim().length() > 0) {
					formattedAddr.append(billingContact.getPersistentAddress().getUnitNum());
					formattedAddr.append(" ");
				}

				if (billingContact.getPersistentAddress().getStreetDir() != null
						&& billingContact.getPersistentAddress().getStreetDir().trim().length() > 0) {
					formattedAddr.append(billingContact.getPersistentAddress().getStreetDir());
					formattedAddr.append(" ");
				}

				if (billingContact.getPersistentAddress().getStreetName() != null
						&& billingContact.getPersistentAddress().getStreetName().trim().length() > 0) {
					formattedAddr.append(billingContact.getPersistentAddress().getStreetName());
					formattedAddr.append(" ");
				}

				if (billingContact.getPersistentAddress().getStreetType() != null
						&& billingContact.getPersistentAddress().getStreetType().trim().length() > 0) {
					formattedAddr.append(billingContact.getPersistentAddress().getStreetType());
					formattedAddr.append(" ");
				}

				if (billingContact.getPersistentAddress().getStreetPostDir() != null
						&& billingContact.getPersistentAddress().getStreetPostDir().trim().length() > 0) {
					formattedAddr.append(billingContact.getPersistentAddress().getStreetPostDir());
					formattedAddr.append(", ");
				}

				if (billingContact.getPersistentAddress().getSubUnitCode() != null
						&& billingContact.getPersistentAddress().getSubUnitCode().trim().length() > 0) {
					formattedAddr.append(billingContact.getPersistentAddress().getSubUnitCode());
					formattedAddr.append(" ");
				}

				if (billingContact.getPersistentAddress().getSubUnitNum() != null
						&& billingContact.getPersistentAddress().getSubUnitNum().trim().length() > 0) {
					formattedAddr.append(billingContact.getPersistentAddress().getSubUnitNum());
					formattedAddr.append(" ");
				}

				address1 = formattedAddr.toString();
				address2 = billingContact.getPersistentAddress().getCity() + ", "
						+ billingContact.getPersistentAddress().getState() + " " + billingContact.getPersistentAddress().getZip();

				firmName = billingContact.getFirmName();
				firstName = billingContact.getFirstName();
				lastName = billingContact.getLastName();
			}
			StringBuffer emailformat = new StringBuffer("");

			String companyName = "USA TODAY";
			if (brandingProduct != null) {
				companyName = brandingProduct.getName();
			}

			if (ccPayment != null) {
				ccNum = ccPayment.getCardNumber();
				ccExp = ccPayment.getExpirationMonth() + ccPayment.getExpirationYear();
			}

			emailformat.append("Thank you for informing " + companyName
					+ " of your subscription needs.  You have asked us to change \n" + "your billing information ");

			emailformat.append("for account number " + customer.getCurrentAccount().getAccountNumber() + ".");
			emailformat.append("  This change will occur within " + days);
			emailformat.append(".");
			emailformat.append("\n\n");

			emailformat.append("When it does, your billing information ");
			emailformat.append("will reflect the following: \n\n");

			if (billingContact == null || billingContact.getUiAddress().getZip() == null
					|| billingContact.getUiAddress().getZip().trim().equals("")) {
				emailformat.append("Billing address same as delivery.");
			} else {
				emailformat.append(firstName.trim() + " " + lastName.trim() + "\n");

				if ((firmName != null) && (!firmName.trim().equals(""))) {
					emailformat.append(firmName);
					emailformat.append("\n");
				}

				emailformat.append(address1);
				emailformat.append("\n");

				if ((addlAddress1 != null) && (!addlAddress1.trim().equals(""))) {
					emailformat.append(addlAddress1);
					emailformat.append("\n");
				}

				if ((addlAddress2 != null) && (!addlAddress2.trim().equals(""))) {
					emailformat.append(addlAddress2);
					emailformat.append("\n");
				}

				emailformat.append(address2);
				emailformat.append("\n");

				if ((homephone != null) && (!homephone.trim().equals(""))) {
					emailformat.append("Telephone Number:  ");
					emailformat.append(homephone);
					emailformat.append("\n");
				}
			}
			emailformat.append("\n \n");

			emailformat.append("Credit Card Information:  ");
			if (ccPayment == null || (ccNum.trim().equals("") && ccExp.trim().equals(""))
					|| (ccNum.equals(currCCNum) && ccExp.equals(currCCExp))) {
				emailformat.append("Same, no change");
			} else {
				emailformat.append("************" + ccNum.substring(ccNum.length() - 4));
				emailformat.append("\n");
				emailformat.append("CC Expiration Date:  ");
				emailformat.append(ccExp.substring(0, 2) + "/" + ccExp.substring(2));
			}
			emailformat.append("\n \n");
			emailformat.append("The updated information will be utilized for your next regularly scheduled payment. ");
			emailformat.append("If you have any questions about your subscription, please call us at");
			emailformat.append(phone);

			emailformat.append("\n \n");
			emailformat.append("To access Online Customer Service in the future, just go to ");

			if (brandingProduct == null || brandingProduct.getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
				emailformat.append("www.usatodayservice.com.");
			} else {
				emailformat.append("www.mysportsweekly.com.");
			}

			// PROMO TEXT
			if (UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT != null && UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT.length() > 0) {
				emailformat.append("\n\n").append(UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT);
			}

			emailformat.append("\n\n").append(CustomerServiceEmails.getUsatMailingAddressFooter(product.getProductCode()));

			/*
			 * String footerPub = pubcode; if (brandingProduct != null) { footerPub = brandingProduct.getProductCode(); }
			 * emailformat.append ("\n\n").append(getUsatMailingAddressFooter(footerPub));
			 */
			CustomerServiceEmails.sendAnEmail(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS, customer.getCurrentEmailRecord()
					.getEmailAddress(), emailSubject, emailformat.toString());
		} catch (UsatException e) {
			throw e;
		} catch (Exception e) {
			throw new UsatException(e);
		}
	}
}
