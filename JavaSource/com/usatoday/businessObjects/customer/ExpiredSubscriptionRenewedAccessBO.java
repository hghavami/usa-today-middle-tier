/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import java.sql.Timestamp;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.ExpiredSubscriptionRenewedAccessIntf;
import com.usatoday.integration.ExpiredSubscriptionRenewedAccessDAO;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class AddressBO
 * 
 *        based class implementation of a postal address.
 * 
 */
public class ExpiredSubscriptionRenewedAccessBO implements ExpiredSubscriptionRenewedAccessIntf {

	// static state fields
	private String publication = null;
	private String email = null;
	private String firstName = null;
	private String lastName = null;
	private String firmName = null;
	private String accountNum = null;
	private long orderID = -1;
	private Timestamp insertedTimeStamp = null;

	/**
     * 
     */
	public ExpiredSubscriptionRenewedAccessBO() {
		super();
	}

	public ExpiredSubscriptionRenewedAccessBO(ExpiredSubscriptionRenewedAccessIntf source) {
		super();
		this.setEmail(source.getEmail());
		this.setFirmName(source.getFirmName());
		this.setFirstName(source.getFirstName());
		this.setInsertedTimeStamp(source.getInsertedTimeStamp());
		this.setLastName(source.getLastName());
		this.setOrderID(source.getOrderID());
		this.setPublication(source.getPublication());
		this.setAccountNum(source.getAccountNum());
	}

	public boolean save(ExpiredSubscriptionRenewedAccessIntf ExpiredSubscriptionRenewedAccessIntf) throws UsatException {

		boolean result = false;
		ExpiredSubscriptionRenewedAccessDAO ExpiredSubscriptionRenewedAccessDAO = new ExpiredSubscriptionRenewedAccessDAO();

		try {
			ExpiredSubscriptionRenewedAccessDAO.insert(ExpiredSubscriptionRenewedAccessIntf);
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		}
		return result;
	}

	public boolean delete(ExpiredSubscriptionRenewedAccessIntf ExpiredSubscriptionRenewedAccessIntf) throws UsatException {

		boolean result = false;
		ExpiredSubscriptionRenewedAccessDAO ExpiredSubscriptionRenewedAccessDAO = new ExpiredSubscriptionRenewedAccessDAO();

		try {
			ExpiredSubscriptionRenewedAccessDAO.delete(ExpiredSubscriptionRenewedAccessIntf);
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		}
		return result;
	}

	private void appendPaddedString(StringBuffer buf, int fieldLength, String field) {
		if (buf == null) {
			return;
		}

		if (field == null) {
			for (int i = 0; i < fieldLength; i++) {
				buf.append(" ");
			}
			return;
		}

		int currentFieldLength = field.length();

		if (currentFieldLength <= fieldLength) {
			// no truncation
			buf.append(field);
			if (currentFieldLength != fieldLength) {
				// buffer remaining field with blanks
				for (int i = currentFieldLength; i < fieldLength; i++) {
					buf.append(" ");
				}
			}
		} else {
			// truncate
			buf.append(field.substring(0, fieldLength));
		}
	}

	public String getBatchFormattedString() throws UsatException {
		StringBuffer buf = new StringBuffer();
		try {

			this.appendPaddedString(buf, 2, this.getPublication());
			this.appendPaddedString(buf, 60, this.email);
			this.appendPaddedString(buf, 25, this.getFirstName());
			this.appendPaddedString(buf, 25, this.getLastName());
			this.appendPaddedString(buf, 28, this.getFirmName());
			this.appendPaddedString(buf, 10, this.getAccountNum());
		} catch (Exception e) {
			System.out.println("ExpiredSubscriptionRenewedAccessBO:getBatchFormattedString() - Failed to create batch record: "
					+ e.getMessage());
			throw new UsatException(e);
		}

		return buf.toString();
	}

	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		if (email != null) {
			if (email.trim().length() <= 60) {
				this.email = email.trim();
			} else {
				this.email = email.trim().substring(0, 59);
				System.out.println("**** ExpiredSubscriptionRenewedAccessBO:setEmail() - User value (length:"
						+ email.trim().length() + ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the firmName.
	 */
	public String getFirmName() {
		return firmName;
	}

	/**
	 * @param firmName
	 *            The firmName to set.
	 */
	public void setFirmName(String firmName) {
		if (firmName != null) {
			if (firmName.trim().length() <= 28) {
				this.firmName = firmName.trim();
			} else {
				this.firmName = firmName.trim().substring(0, 27);
				System.out.println("**** ExpiredSubscriptionRenewedAccessBO:setFirmName() - User value (length:"
						+ firmName.trim().length() + ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		if (firstName != null) {
			if (firstName.trim().length() <= 25) {
				this.firstName = firstName.trim();
			} else {
				this.firstName = firstName.trim().substring(0, 24);
				System.out.println("**** ExpiredSubscriptionRenewedAccessBO:setFirstName() - User value (length:"
						+ firstName.trim().length() + ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the insertedTimeStamp.
	 */
	public Timestamp getInsertedTimeStamp() {
		return insertedTimeStamp;
	}

	/**
	 * @param insertedTimeStamp
	 *            The insertedTimeStamp to set.
	 */
	public void setInsertedTimeStamp(Timestamp insertedTimeStamp) {
		this.insertedTimeStamp = insertedTimeStamp;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		if (lastName != null) {
			if (lastName.trim().length() <= 25) {
				this.lastName = lastName.trim();
			} else {
				this.lastName = lastName.trim().substring(0, 24);
				System.out.println("**** ExpiredSubscriptionRenewedAccessBO:setLastName() - User value (length:"
						+ lastName.trim().length() + ") exceeds table field length ****");
			}
		}
	}

	/**
	 * @return Returns the orderID.
	 */
	public long getOrderID() {
		return orderID;
	}

	/**
	 * @param orderID
	 *            The orderID to set.
	 */
	public void setOrderID(long orderID) {
		this.orderID = orderID;
	}

	/**
	 * @return Returns the publication.
	 */
	public String getPublication() {
		return publication;
	}

	/**
	 * @param publication
	 *            The publication to set.
	 */
	public void setPublication(String publication) {
		this.publication = publication;
	}

	/**
	 * @return Returns the startDate.
	 */
	public String getAccountNum() {
		return accountNum;
	}

	/**
	 * @param startDate
	 *            The startDate to set.
	 */
	public void setAccountNum(String startDate) {
		this.accountNum = startDate;
	}

	public boolean select(ExpiredSubscriptionRenewedAccessIntf ExpiredSubscriptionRenewedAccessIntf) throws UsatException {

		boolean result = false;
		ExpiredSubscriptionRenewedAccessDAO ExpiredSubscriptionRenewedAccessDAO = new ExpiredSubscriptionRenewedAccessDAO();

		try {
			result = ExpiredSubscriptionRenewedAccessDAO.select(ExpiredSubscriptionRenewedAccessIntf);
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		}
		return result;
	}
}
