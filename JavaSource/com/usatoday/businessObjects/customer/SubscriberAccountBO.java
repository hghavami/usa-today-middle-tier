package com.usatoday.businessObjects.customer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.business.interfaces.customer.PersistentSubscriberAccountIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.business.interfaces.products.OfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.integration.SubscriberAccountDAO;

public class SubscriberAccountBO implements SubscriberAccountIntf {

	private String pubCode = null;
	private String accountNumber = null;
	private String transRecType = null;
	private String deliveryMethod = null;
	private String frequencyOfDeliveryCode = null;
	private Double balance = null;
	private ContactIntf deliveryContact = null;
	private ContactIntf billingContact = null;
	private String keyCode = null;
	private int numberOfPapers = 1;
	private Date lastPaymentReceivedDate = null;
	private Date startDate = null;
	private Date expirationDate = null;
	private boolean active = true;
	private boolean onEZPay = false;
	private boolean subscriptionSuspended = false;
	private boolean oneTimeBill = false;
	// private boolean showDeliveryAlert = false;
	private String lastStopCode = "";
	private OfferIntf defaultRenewalOffer = null;
	private String creditCardExp = null;
	private String creditCardNum = null;
	private String creditCardType = null;

	private String renewalTypeAlpha = null;
	private String donationCode = null;
	private String rateCode = null;

	private boolean billingSameAsDelivery = true;
	private boolean expiredSubscriptionAccessRenewed = false;

	public SubscriberAccountBO(PersistentSubscriberAccountIntf accountData) {
		super();

		this.accountNumber = accountData.getAccountNumber();
		this.transRecType = accountData.getTransRecType();
		this.deliveryMethod = accountData.getDeliveryMethod();
		this.pubCode = accountData.getPubCode();
		this.balance = accountData.getBalance();
		this.keyCode = accountData.getKeyCode();
		this.numberOfPapers = accountData.getNumberOfPapers();
		this.lastPaymentReceivedDate = accountData.getLastPaymentReceivedDate();
		this.startDate = accountData.getStartDate();
		this.expirationDate = accountData.getExpirationDate();
		this.active = accountData.isActive();
		this.oneTimeBill = accountData.isOneTimeBill();
		// hd-cons109
		// this.showDeliveryAlert = this.getDeliveryAlertStatus();
		this.onEZPay = accountData.isOnEZPay();
		this.subscriptionSuspended = accountData.isSubscriptionSuspended();
		this.deliveryContact = this.createDeliveryContact(accountData);
		this.frequencyOfDeliveryCode = accountData.getFrequencyOfDeliveryCode();
		try {
			this.billingContact = this.createBillingContact(accountData);
		} catch (Exception e) {
			this.billingContact = null;
		}

		this.renewalTypeAlpha = accountData.getRenewalTypeAlpha();
		this.donationCode = accountData.getDonationCode();
		this.rateCode = accountData.getRateCode();
		this.lastStopCode = accountData.getLastStopCode();
		this.creditCardNum = accountData.getCreditCardNum();
		this.creditCardExp = accountData.getCreditCardExp();
		this.creditCardType = accountData.getCreditCardType();
	}

	private ContactIntf createDeliveryContact(PersistentSubscriberAccountIntf accountData) {
		ContactBO contact = new ContactBO();

		PersistentAddressBO address = new PersistentAddressBO(accountData.getStreetDir(), accountData.getHalfUnitNum(),
				accountData.getCity(), accountData.getState(), accountData.getStreetName(), accountData.getStreetPostDir(),
				accountData.getStreetType(), accountData.getSubUnitCode(), accountData.getSubUnitNum(), accountData.getUnitNum(),
				accountData.getZip(), accountData.getAddlAddr1(), false, false); // set military and gui flags to false since they
																					// aren't stored in table
		address.setValidated(true);
		contact.setPersistentAddress(address);
		contact.setBusinessPhone(accountData.getBusinssPhone());
		contact.setFirmName(accountData.getFirmName());
		contact.setFirstName(accountData.getFirstName());
		contact.setHomePhone(accountData.getHomePhone());
		contact.setLastName(accountData.getLastName());
		contact.setUIAddress(address.convertToUIAddress());
		return contact;
	}

	private ContactIntf createBillingContact(PersistentSubscriberAccountIntf accountData) {
		if (accountData.getBillingZip() == null || accountData.getBillingZip().length() == 0) {
			this.billingSameAsDelivery = true;
		} else {
			this.billingSameAsDelivery = false;
		}
		ContactBO contact = new ContactBO();
		PersistentAddressBO address = new PersistentAddressBO(accountData.getBillingStreetDir(),
				accountData.getBillingHalfUnitNum(), accountData.getBillingCity(), accountData.getBillingState(),
				accountData.getBillingStreetName(), accountData.getBillingStreetPostDir(), accountData.getBillingStreetType(),
				accountData.getBillingSubUnitCode(), accountData.getBillingSubUnitCode(), accountData.getBillingUnitNum(),
				accountData.getBillingZip(), accountData.getBillingAddress1(), false, false);
		address.setValidated(true);
		contact.setPersistentAddress(address);
		contact.setBusinessPhone(accountData.getBillingBusinessPhone());
		contact.setFirmName(accountData.getBillingFirmName());
		contact.setFirstName(accountData.getBillingFirstName());
		contact.setHomePhone(accountData.getBillingHomePhone());
		contact.setLastName(accountData.getBillingLastName());
		contact.setUIAddress(address.convertToUIAddress());
		return contact;
	}

	/**
	 * 
	 * @param accountnum
	 * @return the account if it can be found and is unique otherwise, null
	 * @throws UsatException
	 */
	public static final SubscriberAccountIntf getSubscriberAccount(String accountnum) throws UsatException {

		SubscriberAccountIntf subscriberAccount = null;

		PersistentSubscriberAccountIntf pSubscriberAccount = null;
		SubscriberAccountDAO dao = new SubscriberAccountDAO();

		// Collection<PersistentSubscriberAccountIntf> accounts = dao.getAccountByAccountNumberAndPub(accountnum, null);
		Collection<PersistentSubscriberAccountIntf> accounts = dao.getGenesysAccountByAccountNumber(accountnum, null);
		if (accounts.size() == 1) {
			pSubscriberAccount = (PersistentSubscriberAccountIntf) accounts.toArray()[0];
		}

		if (pSubscriberAccount != null) {
			subscriberAccount = new SubscriberAccountBO(pSubscriberAccount);
		}

		return subscriberAccount;
	}

	/**
	 * 
	 * @param accountNum
	 * @param pubCode
	 *            (optional, pass null to pull just by account number)
	 * @return All subscriber account records (SubscriberAccountBO objects) for the account and pub (if specified)
	 * @throws UsatException
	 */
	public static final Collection<SubscriberAccountIntf> getSubscriberAccountByAccountNumPub(String accountNum, String pubCode)
			throws UsatException {

		SubscriberAccountDAO dao = new SubscriberAccountDAO();

		// Collection<PersistentSubscriberAccountIntf> accounts = dao.getAccountByAccountNumberAndPub(accountNum, null);
		Collection<PersistentSubscriberAccountIntf> accounts = dao.getGenesysAccountByAccountNumber(accountNum, pubCode);

		Collection<SubscriberAccountIntf> boRecords = new ArrayList<SubscriberAccountIntf>();
		if (accounts.size() > 0) {
			// convert to BO's
			Iterator<PersistentSubscriberAccountIntf> itr = accounts.iterator();
			while (itr.hasNext()) {
				PersistentSubscriberAccountIntf accountTO = itr.next();
				SubscriberAccountBO bo = new SubscriberAccountBO(accountTO);
				boRecords.add(bo);
			}
		}
		return boRecords;
	}

	/**
	 * 
	 * @param phone
	 * @param zip
	 * @return
	 * @throws UsatException
	 */
	public static Collection<SubscriberAccountIntf> getSubscriberAccountsForPhoneAndZip(String phone, String zip)
			throws UsatException {
		Collection<PersistentSubscriberAccountIntf> records = null;

		SubscriberAccountDAO dao = new SubscriberAccountDAO();

		// records = dao.getAccountByDeliveryPhoneNumberAndZip(phone, zip);
		records = dao.getGenesysAccountByDeliveryPhoneNumberAndZip(phone, zip);
		if (records.size() == 0) {
			// records = dao.getAccountByBillingPhoneNumberAndZip(phone, zip);
			records = dao.getGenesysAccountByBillingPhoneNumberAndZip(phone, zip);
		}

		Collection<SubscriberAccountIntf> boRecords = new ArrayList<SubscriberAccountIntf>();
		if (records.size() > 0) {
			// convert to BO's
			Iterator<PersistentSubscriberAccountIntf> itr = records.iterator();
			while (itr.hasNext()) {
				PersistentSubscriberAccountIntf accountTO = itr.next();
				SubscriberAccountBO bo = new SubscriberAccountBO(accountTO);
				boRecords.add(bo);
			}
		}
		return boRecords;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public boolean isActive() {
		return this.active;
	}

	public Double getBalance() {
		return this.balance;
	}

	public ContactIntf getBillingContact() {
		return this.billingContact;
	}

	public OfferIntf getDefaultRenewalOffer() throws UsatException {
		SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(this.pubCode);

		if (this.defaultRenewalOffer == null) {
			SubscriptionOfferManager mngr = SubscriptionOfferManager.getInstance();
			try {
				/*
				 * if ("R".equalsIgnoreCase(this.getRenewalTypeAlpha())) { //use renewal rates this.defaultRenewalOffer =
				 * mngr.getRenewalOffer(this.rateCode, this.pubCode); } else { this.defaultRenewalOffer =
				 * mngr.getOffer(this.keyCode, this.pubCode); }
				 */
				// Use account or default keycode to get terms
				if (this.keyCode != null && !this.keyCode.trim().equals("nullnullnull")) {
					this.defaultRenewalOffer = mngr.getGenesysRenewalOffer(this.keyCode, this.pubCode, this.accountNumber, "", "Y");
				}
				if (this.keyCode == null || this.keyCode.trim().equals("nullnullnull")
						|| this.defaultRenewalOffer.getTerms().isEmpty()) {
					this.defaultRenewalOffer = mngr.getGenesysRenewalOffer(product.getDefaultRenewalKeycode(), this.pubCode,
							this.accountNumber, "", "Y");
				}
				// If not terms returned resubmit the renewal API without account number
				if (this.defaultRenewalOffer.getTerms().isEmpty()) {
					this.defaultRenewalOffer = mngr.getGenesysRenewalOffer(product.getDefaultRenewalKeycode(), this.pubCode,
							"", "", "Y");
				}
			}
			// catch (UsatException ue) {
			catch (Exception ue) {
				// no terms found for this subscription
				System.out.println();
				System.out.println("SubscriptionAccountBO::getDefaultRenewalOffer() - No Rates for account: " + this.accountNumber
						+ "  rateCode: " + this.rateCode + " renewalTypeAlpha: " + this.getRenewalTypeAlpha() + " keycode: "
						+ this.keyCode);
				this.defaultRenewalOffer = mngr.getDefaultRenewalOffer(this.pubCode);
			}
		}
		return this.defaultRenewalOffer;
	}

	public ContactIntf getDeliveryContact() {
		return this.deliveryContact;
	}

	public Date getExpirationDate() {
		return this.expirationDate;
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public Date getLastPaymentReceivedDate() {
		return this.lastPaymentReceivedDate;
	}

	public int getNumberOfPapers() {
		return this.numberOfPapers;
	}

	public boolean isOneTimeBill() {
		return this.oneTimeBill;
	}

	// hd-cons109
	/*
	 * public boolean isShowDeliveryAlert() { return this.showDeliveryAlert; }
	 * 
	 * public boolean getDeliveryAlertStatus() { com.usatoday.businessObjects.customer.EarlyAlertBO earlyAlertBO = new
	 * com.usatoday.businessObjects.customer.EarlyAlertBO();
	 * 
	 * try { // check to see if we have an active conn to the Iseries if
	 * (com.usatoday.businessObjects.util.AS400CurrentStatus.getJdbcActive()) { String accountnum7 = this.accountNumber.substring(2,
	 * 9); earlyAlertBO.determineEarlyAlertNew(this.pubCode,accountnum7); String earlyAlertResult =
	 * earlyAlertBO.getEarlyAlertDelay(); if (earlyAlertResult.equals("Y")) { this.showDeliveryAlert=true; } } } catch (Exception e)
	 * { // no object }
	 * 
	 * return this.showDeliveryAlert; }
	 */

	public boolean isOnEZPay() {
		return this.onEZPay;
	}

	public boolean isDonatingToNIE() {
		boolean donating = false;
		if ("VE".equalsIgnoreCase(this.lastStopCode)) {
			donating = true;
		}
		return donating;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public boolean isSubscriptionSuspended() {
		return this.subscriptionSuspended;
	}

	public boolean isBillingSameAsDelivery() {
		return this.billingSameAsDelivery;
	}

	/**
	 * @return Returns the renewalTypeAlpha.
	 */
	public String getRenewalTypeAlpha() {
		return renewalTypeAlpha;
	}

	public String getDonationCode() {
		return donationCode;
	}

	/**
	 * @return Returns the deliveryMethod.
	 */
	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	/**
	 * @param deliveryMethod
	 *            The deliveryMethod to set.
	 */
	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	/**
	 * @return Returns the creditCardExp.
	 */
	public String getCreditCardExp() {
		return creditCardExp;
	}

	/**
	 * @param creditCardExp
	 *            The creditCardExp to set.
	 */
	public void setCreditCardExp(String creditCardExp) {
		this.creditCardExp = creditCardExp;
	}

	/**
	 * @return Returns the creditCardNum.
	 */
	public String getCreditCardNum() {
		return creditCardNum;
	}

	/**
	 * @param creditCardNum
	 *            The creditCardNum to set.
	 */
	public void setCreditCardNum(String creditCardNum) {
		this.creditCardNum = creditCardNum;
	}

	/**
	 * @return Returns the creditCardType.
	 */
	public String getCreditCardType() {
		return creditCardType;
	}

	/**
	 * @param creditCardType
	 *            The creditCardType to set.
	 */
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	public SubscriptionProductIntf getProduct() {
		SubscriptionProductIntf product = null;
		try {
			product = SubscriptionProductBO.getSubscriptionProduct(this.getPubCode());
		} catch (Exception e) {
			; // ignore
		}
		return product;
	}

	// Check if an expired subscription exists in usat_expired_subscription_renewed_access file. If so allow eedition viewing
	public boolean isExpiredSubscriptionAccessRenewed() {

		ExpiredSubscriptionRenewedAccessBO esraBO = new ExpiredSubscriptionRenewedAccessBO();
		esraBO.setPublication(this.pubCode);
		esraBO.setAccountNum(this.accountNumber);
		esraBO.setEmail("");
		esraBO.setFirmName("");
		esraBO.setFirstName("");
		esraBO.setLastName("");
		esraBO.setOrderID(0);
		try {
			// Active subscription, if so check existing renewed access and delete it
			if (this.transRecType.substring(0, 1).equalsIgnoreCase("P")) {
				expiredSubscriptionAccessRenewed = true; // Just set it to true
				esraBO.delete(esraBO);
				// Pending subscription, check if access is already renewed, if not renew access
			} else {
				expiredSubscriptionAccessRenewed = esraBO.select(esraBO);
				if (!expiredSubscriptionAccessRenewed) {
					esraBO.setEmail(this.deliveryContact.getEmailAddress());
					esraBO.setFirmName(this.deliveryContact.getFirmName());
					esraBO.setFirstName(this.deliveryContact.getFirstName());
					esraBO.setLastName(this.deliveryContact.getLastName());
					esraBO.setOrderID(0);
					esraBO.save(esraBO);
					expiredSubscriptionAccessRenewed = true;
				}
			}
		} catch (Exception e) {
			expiredSubscriptionAccessRenewed = false;
		}
		return expiredSubscriptionAccessRenewed;
	}

	public void setexpiredSubscriptionAccessRenewed(boolean expiredSubscriptionAccessRenewed) {
		this.expiredSubscriptionAccessRenewed = expiredSubscriptionAccessRenewed;
	}

	public String getTransRecType() {
		return transRecType;
	}

	public void setTransRecType(String transRecType) {
		this.transRecType = transRecType;
	}

	@Override
	public String getCreditCardExpMonth() {
		String month = null;
		try {
			month = this.creditCardExp.substring(0, 2);
		} catch (Exception e) {
			month = null;
		}
		return month;
	}

	@Override
	public String getCreditCardExpYear() {
		String year = null;
		try {
			year = this.creditCardExp.substring(2);
			int yearInt = Integer.valueOf(year).intValue();
			yearInt += 2000; // since we arent' y2k compliant.jeez.
			year = String.valueOf(yearInt);
		} catch (Exception e) {
			year = null;
		}
		return year;
	}

	public void setFrequencyOfDeliveryCode(String frequencyOfDeliveryCode) {
		this.frequencyOfDeliveryCode = frequencyOfDeliveryCode;
	}

	@Override
	public String getFrequencyOfDeliveryCode() {
		return frequencyOfDeliveryCode;
	}

}
