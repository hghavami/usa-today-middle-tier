/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import com.usatoday.business.interfaces.PostalAddressIntf;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class AddressBO
 * 
 *        based class implementation of a postal address.
 * 
 */
public abstract class AddressBO implements PostalAddressIntf {

	protected String city = "";
	protected String state = "";
	protected String zip = "";
	protected String address2 = "";
	protected boolean validated = false;

	public String getZip() {
		if (zip == null) {
			return "";
		}
		return zip;
	}

	public String getCity() {
		if (city == null) {
			return "";
		}
		return city;
	}

	public String getState() {
		if (state == null) {
			return "";
		}
		return state;
	}

	public String getAddress2() {
		if (address2 == null) {
			return "";
		}
		return this.address2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.PostalAddressIntf#isValidated()
	 */
	public boolean isValidated() {
		return validated;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setValidated(boolean validated) {
		this.validated = validated;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
}
