package com.usatoday.businessObjects.customer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.AdditionalDeliveryAddressIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;
import com.usatoday.integration.CustomerDAO;
import com.usatoday.util.constants.UsaTodayConstants;

public class CustomerBO implements CustomerIntf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2834271218020974830L;
	private static final String passwordPool = "c2f3hjk48rst267xy9z";

	private EmailRecordIntf currentEmail = null;
	private SubscriberAccountIntf currentAccount = null;

	private HashMap<String, SubscriberAccountIntf> accounts = null;
	private HashMap<String, EmailRecordIntf> emailRecords = null;
	private HashMap<String, AdditionalDeliveryAddressIntf> additionalDeliveryAddress = null;

	/**
	 * 
	 * @param accounts
	 * @param emailRecords
	 */
	public CustomerBO(HashMap<String, SubscriberAccountIntf> accounts, HashMap<String, EmailRecordIntf> emailRecords) {
		super();
		this.accounts = accounts;
		this.emailRecords = emailRecords;
		if (this.accounts != null && this.accounts.size() > 0) {
			Iterator<String> aItr = this.accounts.keySet().iterator();
			while (aItr.hasNext()) {
				String accountKey = aItr.next();
				this.setCurrentAccount(accountKey);
				break;
			}
		} else {
			if (this.emailRecords != null && this.emailRecords.size() > 0) {
				this.setCurrentAccount(this.emailRecords.values().iterator().next().getAccountNumber());
			}
		}
	}

	/**
     * 
     */
	public SubscriberAccountIntf getCurrentAccount() {
		return currentAccount;
	}

	/**
     * 
     */
	public void setCurrentAccount(String accountNumber) {
		if (this.getAccounts() != null) {
			this.currentAccount = this.getAccounts().get(accountNumber);
			if (this.emailRecords != null) {
				this.currentEmail = this.emailRecords.get(accountNumber);
				if (this.currentEmail == null && this.emailRecords.size() > 0) {
					this.currentEmail = this.emailRecords.values().iterator().next();
				}
				if (this.currentAccount != null) {
					// this kludge is because of the way we store emails seperate from accounts and such.
					ContactBO c = (ContactBO) this.currentAccount.getDeliveryContact();
					c.setEmailAddress(this.currentEmail.getEmailAddress());
				}
			}
		} else {
			if (this.emailRecords != null) {
				this.currentEmail = this.emailRecords.get(accountNumber);
				if (this.currentEmail == null && this.emailRecords.size() > 0) {
					this.currentEmail = this.emailRecords.values().iterator().next();
				}
			}
		}
	}

	/**
     * 
     */
	public int getNumberOfAccounts() {
		if (this.getAccounts() != null) {
			return this.getAccounts().size();
		}
		return 0;
	}

	/**
     * 
     */
	public HashMap<String, SubscriberAccountIntf> getAccounts() {
		return this.accounts;
	}

	/**
     * 
     */
	public EmailRecordIntf getCurrentEmailRecord() {
		return this.currentEmail;
	}

	/**
     * 
     */
	public HashMap<String, EmailRecordIntf> getEmailRecords() {
		return this.emailRecords;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws LoginException
	 */
	public static final CustomerBO loginCustomer(String emailAddress, String password) throws LoginException {

		return CustomerBO.loginCustomer(emailAddress, password, null);
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param pubCode
	 * @return
	 * @throws LoginException
	 */
	public static final CustomerBO loginCustomer(String emailAddress, String password, String pubCode) throws LoginException {

		CustomerBO customer = null;

		try {
			CustomerDAO dao = new CustomerDAO();
			// customer = (CustomerBO)dao.getCustomerByEmailAndPasswordAndPub(emailAddress, password, pubCode);
			customer = (CustomerBO) dao.getCustomerByEmailAndPassword(emailAddress, password);
		} catch (UsatException ue) {
			if (ue instanceof LoginException) {
				LoginException le = (LoginException) ue;
				throw le;
			} else {
				LoginException le = new LoginException(LoginException.UNKNOWN_ERROR);
				throw le;
			}
		}

		return customer;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws LoginException
	 */
	public static final CustomerBO loginCustomerEmailAccountPub(String emailAddress, String account, String pubCode)
			throws LoginException {

		CustomerBO customer = null;

		try {
			CustomerDAO dao = new CustomerDAO();
			// customer = (CustomerBO) dao.getCustomerByEmailAndAccountAndPub(emailAddress, account, pubCode);
			customer = (CustomerBO) dao.getGenesysCustomerByEmailAndAccountAndPub(emailAddress, account, pubCode);
		} catch (UsatException ue) {
			if (ue instanceof LoginException) {
				LoginException le = (LoginException) ue;
				throw le;
			} else {
				LoginException le = new LoginException(LoginException.UNKNOWN_ERROR);
				throw le;
			}
		}

		return customer;
	}

	/**
	 * 
	 * @param identifier
	 * @return
	 * @throws LoginException
	 */
	public static final CustomerBO loginCustomerByUniqueIdentifier(String identifier) throws LoginException {

		CustomerBO customer = null;

		try {
			CustomerDAO dao = new CustomerDAO();
			// customer = (CustomerBO) dao.getCustomerByUniqueIdentifier(identifier);
			customer = (CustomerBO) dao.getGenesysCustomerByUniqueIdentifier(identifier);
		} catch (UsatException ue) {
			if (ue instanceof LoginException) {
				LoginException le = (LoginException) ue;
				throw le;
			} else {
				LoginException le = new LoginException(LoginException.UNKNOWN_ERROR);
				throw le;
			}
		}

		return customer;
	}

	/**
	 * 
	 * @param email
	 * @return
	 * @throws LoginException
	 */
	public static final CustomerBO loginCustomerByEmailAddress(String emailAddress) throws LoginException {

		CustomerBO customer = null;

		try {
			CustomerDAO dao = new CustomerDAO();
			// customer = dao.getCustomerByEmail(emailAddress);
			customer = dao.getGenesysCustomerByEmail(emailAddress);
		} catch (UsatException ue) {
			if (ue instanceof LoginException) {
				LoginException le = (LoginException) ue;
				throw le;
			} else {
				LoginException le = new LoginException(LoginException.UNKNOWN_ERROR);
				throw le;
			}
		}

		return customer;
	}

	public static final boolean loginCustomerByAutoLogin(String autoLogin) throws Exception {
		
		boolean result = false;
		try {
			CustomerDAO dao = new CustomerDAO();
			result = dao.getAutoLogin(autoLogin);
		} catch (Exception e) {
			System.out.println("Firefly Atypon Auto Login API had issues retrieving the user " + e.getMessage());
		}
		return result;
	}
	
	public static final String generateNewPassword() {

		// generate an 8 character password
		StringBuilder newPasswordBuilder = new StringBuilder();

		try {

			int numToFill = 8;
			if (numToFill > 0) {
				newPasswordBuilder.append(RandomStringUtils.random(numToFill, CustomerBO.passwordPool));
			}
		} catch (Exception exp) {
			newPasswordBuilder = new StringBuilder("bxz2ht5s");
		}
		return newPasswordBuilder.toString();
	}

	public boolean getIsElectronicEditionOnlyCustomer() {

		boolean eOnly = true;
		if (this.accounts != null && this.accounts.size() > 0) {
			for (SubscriberAccountIntf account : this.accounts.values()) {
				if (account.getPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)
						|| account.getPubCode().equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
					eOnly = false;
					break;
				}
			}
		}
		return eOnly;
	}

	public boolean hasAccountWithNoAccountNumber() {
		//
		boolean hasNewAccounts = false;
		if (this.emailRecords != null && this.accounts != null) {
			if (this.emailRecords.size() > this.accounts.size()) {
				hasNewAccounts = true;
			}
		}
		return hasNewAccounts;
	}

	public boolean getHasActiveAccounts() {
		boolean activeAccountsExist = false;

		if (this.emailRecords != null && this.emailRecords.size() > 0) {
			for (EmailRecordIntf anEmail : this.emailRecords.values()) {
				if (!anEmail.isStartDateInFuture()) {
					if (this.accounts.containsKey(anEmail.getAccountNumber())) {
						activeAccountsExist = true;
						break;
					} else {
						// check for grace period email
						if ((anEmail.getAccountNumber().length() == 0) || anEmail.getAccountNumber().equalsIgnoreCase("0000000")) {
							activeAccountsExist = true;
							break;
						}

						try {
							DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(anEmail.getPermStartDate());

							// only add eEditions without an account number for up to X days after purchase.
							if (dt.plusDays(UsaTodayConstants.EEDITION_GRACE_LOGIN_DAYS).isAfterNow()) {
								activeAccountsExist = true;
								break;
							}
						} catch (Exception e) {
							;
						}

					} // end else check for grace
				} // end if not a future start date email record
				else {
					// check for account override for case when someone has called to change start date
					if (this.accounts.containsKey(anEmail.getAccountNumber())) {
						activeAccountsExist = true;

						// update start date with start date from account
						try {
							EmailRecordBO eBO = (EmailRecordBO) anEmail;

							SubscriberAccountIntf sA = this.accounts.get(anEmail.getAccountNumber());

							DateTime dt = new DateTime(sA.getStartDate());

							// only update if the start date is not in future
							if (dt.isBeforeNow()) {
								// yyyymmdd
								eBO.setPermStartDate(dt.toString("yyyyMMdd"));
								eBO.save();
								break;
							} else {
								// account has future start date;
								activeAccountsExist = false;
							}
						} catch (Exception e) {
							activeAccountsExist = false;
						}

					}
				}
			} // end for emails
		} // end if has email records

		return activeAccountsExist;
	}

	public boolean getHasFutureStarts() {
		boolean hasFutureStarts = false;

		if (this.emailRecords != null && this.emailRecords.size() > 0) {
			for (EmailRecordIntf anEmail : this.emailRecords.values()) {
				if (anEmail.isStartDateInFuture()) {
					hasFutureStarts = true;
					break;
				}
			}
		}

		return hasFutureStarts;
	}

	public HashMap<String, AdditionalDeliveryAddressIntf> getAdditionalDeliveryAddress() {
		try {
			if (this.additionalDeliveryAddress != null) {
				this.additionalDeliveryAddress.clear();
			}
			HashMap<String, AdditionalDeliveryAddressIntf> additionalDeliveryAddresses = new HashMap<String, AdditionalDeliveryAddressIntf>();
			Iterator<AdditionalDeliveryAddressIntf> aAItr = AdditionalDeliveryAddressBO.select(this.currentEmail.getEmailAddress())
					.iterator();
			while (aAItr.hasNext()) {
				AdditionalDeliveryAddressIntf additionalDeliveryAddress = aAItr.next();
				additionalDeliveryAddresses.put(String.valueOf(additionalDeliveryAddress.getId()), additionalDeliveryAddress);
			}
			if (additionalDeliveryAddresses != null && additionalDeliveryAddresses.size() > 0) {
				this.setAdditionalDeliveryAddress(additionalDeliveryAddresses);
			}
		} catch (Exception e) {
			this.additionalDeliveryAddress = null;
		}
		return additionalDeliveryAddress;
	}

	public void setAdditionalDeliveryAddress(HashMap<String, AdditionalDeliveryAddressIntf> additionalDeliveryAddress) {
		this.additionalDeliveryAddress = additionalDeliveryAddress;
	}

}
