package com.usatoday.businessObjects.customer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.mail.internet.InternetAddress;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.business.interfaces.customer.TrialInstanceIntf;
import com.usatoday.integration.TrialCustomerDAO;
import com.usatoday.integration.TrialCustomerTO;

public class TrialCustomerBO implements TrialCustomerIntf {

	private HashMap<String, TrialInstanceIntf> trialInstances = new HashMap<String, TrialInstanceIntf>();

	private String emailAddress = null;
	private String password = null;

	private TrialInstanceIntf currentInstance = null;

	public static TrialCustomerIntf getTrialCustomerByEmail(String emailAddress) throws UsatException {
		TrialCustomerIntf customer = null;

		TrialCustomerDAO dao = new TrialCustomerDAO();

		Collection<TrialCustomerTO> tos = dao.getTrialRecordsForEmailAddress(emailAddress);

		if (tos == null || tos.size() == 0) {
			throw new UsatException("No Customer Found for Address: " + emailAddress);
		} else {

			Collection<TrialInstanceIntf> instances = new ArrayList<TrialInstanceIntf>();

			for (TrialCustomerTO to : tos) {
				TrialInstanceBO tiBO = new TrialInstanceBO(to);
				instances.add(tiBO);
			}

			// convert to to BO
			customer = new TrialCustomerBO(instances);

		}

		return customer;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @return
	 * @throws UsatException
	 */
	public static TrialCustomerIntf getTrialCustomerByEmailPassword(String emailAddress, String password) throws UsatException {
		TrialCustomerIntf customer = null;

		TrialCustomerDAO dao = new TrialCustomerDAO();

		try {
			Collection<TrialCustomerTO> tos = dao.getTrialRecordsForEmailAddressPassword(emailAddress, password);

			Collection<TrialInstanceIntf> instances = new ArrayList<TrialInstanceIntf>();

			for (TrialCustomerTO to : tos) {
				TrialInstanceBO tiBO = new TrialInstanceBO(to);
				instances.add(tiBO);
			}

			// convert to to BO
			customer = new TrialCustomerBO(instances);

		} catch (UsatException ue) {
			if (ue instanceof LoginException) {
				LoginException le = (LoginException) ue;
				throw le;
			} else {
				LoginException le = new LoginException(LoginException.UNKNOWN_ERROR);
				throw le;
			}
		}

		return customer;
	}

	/**
	 * 
	 * @param emailAddress
	 * @return
	 * @throws UsatException
	 */
	public static Collection<TrialCustomerIntf> getTrialCustomerByEmailLike(String emailAddress) throws UsatException {

		ArrayList<TrialCustomerIntf> customers = new ArrayList<TrialCustomerIntf>();
		HashMap<String, Collection<TrialInstanceIntf>> customerMap = new HashMap<String, Collection<TrialInstanceIntf>>();

		TrialCustomerDAO dao = new TrialCustomerDAO();

		if (emailAddress != null) {
			if (emailAddress.length() > 0) {
				emailAddress = "%" + emailAddress + "%";
			}
		}

		Collection<TrialCustomerTO> tos = dao.getTrialRecordsForEmailAddressLike(emailAddress);

		Collection<TrialInstanceIntf> instances = new ArrayList<TrialInstanceIntf>();

		// since the DAO will return potentially instances for multiple users..
		// need to figure out which ones go with which users...
		for (TrialCustomerTO to : tos) {
			TrialInstanceBO tiBO = new TrialInstanceBO(to);

			if (customerMap.containsKey(tiBO.getContactInformation().getEmailAddress())) {
				instances = customerMap.get(tiBO.getContactInformation().getEmailAddress());
			} else {
				instances = new ArrayList<TrialInstanceIntf>();
				customerMap.put(tiBO.getContactInformation().getEmailAddress(), instances);
			}
			instances.add(tiBO);
		}

		// now convert them to customers
		if (customerMap.size() > 0) {
			// convert to to BOs
			for (Collection<TrialInstanceIntf> trials : customerMap.values()) {
				TrialCustomerBO customer = new TrialCustomerBO(trials);
				customers.add(customer);
			}
		}

		return customers;
	}

	public static boolean validateNewTrialInstance(TrialInstanceIntf newCust) throws UsatException {
		boolean isValid = true;

		if (newCust == null) {
			throw new UsatException("invalid trial instance: null");
		}

		if (newCust.getContactInformation() == null) {
			throw new UsatException("invalid trial instance: null contact");
		}

		if (newCust.getContactInformation().getEmailAddress() == null
				|| newCust.getContactInformation().getEmailAddress().trim().length() == 0) {
			throw new UsatException("invalid trial: email address is required.");
		}

		try {
			String emailStr = newCust.getContactInformation().getEmailAddress();

			javax.mail.internet.InternetAddress emailAddress = new InternetAddress(emailStr);
			emailAddress.validate();

			int ampIndex = emailStr.indexOf('@');

			if (ampIndex == -1) {
				throw new UsatException("no apersand");
			} else {
				emailStr = emailStr.substring(ampIndex + 1);
				int dotIndex = emailStr.indexOf('.');

				if (dotIndex == -1) {
					throw new UsatException("no dot after ampersand");
				}

			}

		} catch (Exception e) {
			throw new UsatException("Invalid Trial: Bad Email address: " + newCust.getContactInformation().getEmailAddress());
		}

		TrialCustomerIntf existingCust = null;
		// check if email exists already
		try {
			existingCust = TrialCustomerBO.getTrialCustomerByEmail(newCust.getContactInformation().getEmailAddress());
		} catch (Exception exp) {
			// an exception is thrown if no records exist so this is a good thing.
			return isValid;
		}

		// if no email exists then good to go
		if (existingCust != null) {
			// dig deeper
			boolean instanceFound = existingCust.setCurrentInstance(newCust.getPartner().getPartnerID());
			if (instanceFound) {
				// throw exception
				isValid = false;
				throw new UsatException("A trial account already exists for this email address and partner program.");
			}

		}

		return isValid;
	}

	@SuppressWarnings("unused")
	private TrialCustomerBO() {
		super();
	}

	/**
	 * Constructor used when retrieved from database
	 * 
	 * @param source
	 */
	protected TrialCustomerBO(Collection<TrialInstanceIntf> instances) throws UsatException {

		if (instances == null || instances.size() == 0) {
			throw new UsatException("TrialCustomerBO() - No trials instances for customer.");
		}

		for (TrialInstanceIntf ti : instances) {
			if (this.trialInstances.size() == 0) {
				this.emailAddress = ti.getContactInformation().getEmailAddress();
				this.password = ti.getContactInformation().getPassword();
			}

			this.trialInstances.put(ti.getPartner().getPartnerID(), ti);

			if (this.currentInstance == null && !ti.getIsTrialPeriodExpired()) {
				this.currentInstance = ti;
			}
		}

		// if no unexpired trials
		if (this.currentInstance == null) {
			this.currentInstance = this.trialInstances.values().iterator().next();
		}

	}

	public void save() throws UsatException {

		if (this.trialInstances.size() > 0) {
			for (TrialInstanceIntf ti : this.trialInstances.values()) {
				ti.save();
			}
		}
	}

	public void addTrialInstance(TrialInstanceIntf newTrial) throws UsatException {
		if (this.trialInstances.containsKey(newTrial.getPartner().getPartnerID())) {
			throw new UsatException("Customer already has a trial with this partner.");
		}
		this.trialInstances.put(newTrial.getPartner().getPartnerID(), newTrial);
	}

	public Collection<TrialInstanceIntf> getAllTrialInstances() {
		return this.trialInstances.values();
	}

	public TrialInstanceIntf getCurrentInstance() {
		if (this.currentInstance == null) {
			// set to first
			if (this.trialInstances.size() > 0) {
				this.currentInstance = this.trialInstances.values().iterator().next();
			}
		}
		return this.currentInstance;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public String getPassword() {
		return this.password;
	}

	public boolean setCurrentInstance(String partnerID) {
		boolean found = false;

		if (this.trialInstances.containsKey(partnerID)) {
			this.currentInstance = this.trialInstances.get(partnerID);
			found = true;
		}
		return found;
	}

	public boolean setCurrentInstance(TrialInstanceIntf instance) {
		boolean found = false;

		if (instance == null) {
			return found;
		}

		String partnerID = instance.getPartner().getPartnerID();
		if (this.trialInstances.containsKey(partnerID)) {
			this.currentInstance = this.trialInstances.get(partnerID);
			found = true;
		}
		return found;
	}

	public void setEmailAddress(String address) {

		if (address != null && address.trim().length() > 0) {
			// apply email to all instances
			this.emailAddress = address;

			for (TrialInstanceIntf ti : this.trialInstances.values()) {
				ti.getContactInformation().setEmailAddress(address.trim());
			}
		}
	}

	public void setPassword(String word) {
		if (word != null && word.trim().length() > 0) {
			// apply email to all instances
			this.password = word.trim();

			for (TrialInstanceIntf ti : this.trialInstances.values()) {
				ti.getContactInformation().setPassword(word.trim());
			}
		}
	}

}
