package com.usatoday.businessObjects.customer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.TrialCustomerTOIntf;
import com.usatoday.business.interfaces.customer.TrialInstanceIntf;
import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.partners.TrialPartnerBO;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.util.mail.ExactTargetTriggeredSendNew;
import com.usatoday.integration.TrialCustomerDAO;
import com.usatoday.integration.TrialCustomerTO;
import com.usatoday.util.constants.UsaTodayConstants;

public class TrialInstanceBO implements TrialInstanceIntf {
	private long id = -1;
	private DateTime endDate = null;
	private DateTime startDate = null;
	private TrialPartnerIntf partner = null;
	private ContactBO contact = null;
	private boolean active = true;
	private boolean subscribed = false;
	private String clubNumber = null;
	private String pubCode = null;
	private String keycode = null;
	private String clientIP = null;
	private String ncsRepID = null;
	private String confirmationEmailSent = "Y";

	// ET Specific attribute
	private String trialSubType = "Sample"; // Exact Target

	public TrialInstanceBO() {
		super();
	}

	public TrialInstanceBO(TrialCustomerTOIntf source) {
		this.id = source.getID();
		this.active = source.getIsActive();
		this.subscribed = source.getIsSubscribed();
		this.pubCode = source.getPubCode();
		this.keycode = source.getKeyCode();
		this.clubNumber = source.getClubNumber();

		this.contact = new ContactBO();
		this.contact.setBusinessPhone("");
		this.contact.setHomePhone(source.getPhone());
		this.contact.setEmailAddress(source.getEmail());
		this.contact.setFirmName(source.getCompanyName());
		this.contact.setFirstName(source.getFirstName());
		this.contact.setLastName(source.getLastName());
		this.contact.setPassword(source.getPassword());
		this.startDate = source.getStartDate();
		this.endDate = source.getEndDate();
		this.clientIP = source.getClientIP();
		this.ncsRepID = source.getNcsRepID();
		this.confirmationEmailSent = source.getConfirmationEmailSent();
		// add trial sub type if added to db

		UIAddressBO address = new UIAddressBO();
		address.setAddress1(source.getAddress1());
		address.setAddress2(source.getAddress2());
		address.setAptSuite(source.getApartmentSuite());
		address.setCity(source.getCity());
		address.setState(source.getState());
		address.setZip(source.getZip());
		address.setValidated(true);

		this.contact.setUIAddress(address);

		// pull partner
		TrialPartnerIntf p = null;
		try {
			p = TrialPartnerBO.fetchPartner(source.getPartnerID(), true);
		} catch (Exception e) {
			System.out.println("Failed to fully initialize trial customer with id: " + source.getID() + "   Exception: "
					+ e.getMessage());
			e.printStackTrace();

		}
		this.setPartner(p);

	}

	public String getClubNumber() {
		return this.clubNumber;
	}

	public ContactBO getContactInformation() {
		return this.contact;
	}

	public DateTime getEndDate() {
		return this.endDate;
	}

	public long getID() {
		return this.id;
	}

	public boolean getIsActive() {
		return this.active;
	}

	public String getKeyCode() {
		return this.keycode;
	}

	public void setKeyCode(String code) {
		if (code != null) {
			if (code.length() > 10) {
				code = code.substring(0, 10);
			}
		}
		this.keycode = code;
	}

	public TrialPartnerIntf getPartner() {
		return this.partner;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public void setPubCode(String pubCode) {
		if (pubCode != null) {
			if (pubCode.length() > 10) {
				pubCode = pubCode.substring(0, 10);
			}
		}
		this.pubCode = pubCode;
	}

	public DateTime getStartDate() {
		return this.startDate;
	}

	public void save() throws UsatException {

		TrialCustomerDAO dao = new TrialCustomerDAO();
		TrialCustomerTO to = new TrialCustomerTO(this);

		if (this.id < 0) {
			// insert
			to = dao.insert(to);

			// update id
			this.id = to.getID();
		} else {
			// update
			to = dao.update(to);
		}
	}

	public void setClubNumber(String number) {
		if (number != null) {
			if (number.length() > 128) {
				number = number.substring(0, 128);
			}
		}
		this.clubNumber = number;
	}

	public void setContactInformation(ContactBO contact) {
		this.contact = contact;
	}

	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	public void setPartner(TrialPartnerIntf partner) {
		this.partner = partner;
	}

	public void setStartDate(DateTime start) {
		this.startDate = start;
	}

	public SubscriptionProductIntf getProduct() throws UsatException {
		SubscriptionProductIntf prod = SubscriptionProductBO.getSubscriptionProduct(this.getPartner().getPubCode());
		return prod;
	}

	public boolean getIsSubscribed() {
		return this.subscribed;
	}

	public void setIsSubscribed(boolean subscribed) {
		this.subscribed = subscribed;
	}

	public boolean getIsTrialPeriodExpired() {
		boolean trialExpired = true;
		if (this.getEndDate() == null || this.getEndDate().isAfterNow()) {
			trialExpired = false;
		} else {
			if (this.getEndDate() != null) {
				// we know it is officially expired. But lets check for same day of year. If same day, let them in.
				// We want them to have access all day on the final day of trial.
				DateTime now = new DateTime();
				if ((this.getEndDate().getDayOfYear() == now.getDayOfYear()) && (this.getEndDate().getYear() == now.getYear())) {
					trialExpired = false;
				}
			}
		}
		return trialExpired;
	}

	public boolean sendConfirmationEmail() {

		boolean emailSent = false;
		try {

			try {
				if (UsaTodayConstants.debug) {

					System.out.println("TrialInstanceBO: Email Service...   Starting");
				}

				String firstName = contact.getFirstName();
				String lastName = contact.getLastName();
				String emailAddr = contact.getEmailAddress();
				String company = contact.getFirmName();
				String phone = contact.getHomePhone();
				if (phone != null) {
					if (phone.length() > 10) {
						phone = phone.substring(0, 10);
					}
				}
				//
				String street = contact.getUiAddress().getAddress1() + " " + contact.getUiAddress().getAptSuite();
				String subType = this.getTrialSubType();
				String zeroDollar = "$0.0";
				String webpartner = partner.getPartnerName();

				DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
				java.util.Date date = new java.util.Date();
				String dateStr = dateFormat.format(date);

				String webtrialstart = this.getStartDate().toString("MM-dd-yyyy");
				String webtrialend = this.getEndDate().toString("MM-dd-yyyy");
				String trialLink = partner.getAuthLinkURL();
				String returnCode = ExactTargetTriggeredSendNew.sendExactTargetOrderConfirmation(emailAddr, trialLink, firstName,
						lastName, zeroDollar, "", "", "", street, partner.getConfirmationEmailTemplateID(),
						partner.getPartnerName(), this.pubCode, contact.getUiAddress().getCity(), company,
						Integer.toString(partner.getDurationInDays()), contact.getUiAddress().getZip(), contact.getPassword(),
						"Mail", zeroDollar, "", dateStr, "", dateStr, zeroDollar, subType, phone,
						contact.getUiAddress().getState(), "", "", webpartner, webtrialstart, webtrialend, "",
						partner.getPartnerID(), zeroDollar);

				if (returnCode.equalsIgnoreCase("Error")) {
					try {

						System.out.println("error!!!!  Send backup email" + this.getContactInformation().getEmailAddress());

					} catch (Exception e) {
						// e-mail not sent

					}
				}

				emailSent = true;

				if (UsaTodayConstants.debug) {
					System.out.println("TrialInstanceBO:  return code..  " + returnCode);

					System.out.println("TrialInstanceBO:  Exact Target Email Generation...Completed");
				}

			} catch (Exception e) {
				System.out.println("TrialInstanceBO : Failed to send confirmation email: ");

			}

		} catch (Exception e) {
			System.out.println("TrialInstanceBO : Failed to send confirmation email: ");

		}
		return emailSent;

	}

	public String getClientIP() {
		return this.clientIP;
	}

	public void setClientIP(String ip) {
		if (ip != null) {
			if (ip.length() > 32) {
				ip = ip.substring(0, 32);
			}
		}
		this.clientIP = ip;
	}

	public String getNcsRepID() {
		return ncsRepID;
	}

	public void setNcsRepID(String id) {
		if (id != null) {
			if (id.length() > 64) {
				id = id.substring(0, 64);
			}
		}
		this.ncsRepID = id;
	}

	public String getConfirmationEmailSent() {
		return this.confirmationEmailSent;
	}

	public void setConfirmationEmailSent(String flag) {

		if (flag == null || flag.trim().length() > 1) {
			this.confirmationEmailSent = "Y";
		} else {
			this.confirmationEmailSent = flag.trim();
		}
	}

	public String getTrialSubType() {
		return trialSubType;
	}

	public void setTrialSubType(String trialSubType) {
		this.trialSubType = trialSubType;
	}

}
