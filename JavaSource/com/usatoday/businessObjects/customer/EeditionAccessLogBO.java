/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import java.sql.Timestamp;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.EeditionAccessLogIntf;
import com.usatoday.integration.EeditionAccessLogDAO;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class AddressBO
 * 
 *        based class implementation of a postal address.
 * 
 */
public class EeditionAccessLogBO implements EeditionAccessLogIntf {

	// static state fields
	private String publication = null;
	private String accountNum = null;
	private String trialAccess = null;
	private Timestamp dateTime = null;
	private String clientIP = null;
	private String emailAddress = null;
	private long transID = 0;

	public long getTransID() {
		return transID;
	}

	public void setTransID(long transID) {
		this.transID = transID;
	}

	/**
     * 
     */
	public EeditionAccessLogBO() {
		super();
	}

	public EeditionAccessLogBO(EeditionAccessLogIntf source) {
		super();
		this.setTransID(source.getTransID());
		this.setPublication(source.getPublication());
		this.setAccountNum(source.getAccountNum());
		this.setClientIP(source.getClientIP());
		this.setDateTime(source.getDateTime());
		this.setEmailAddress(source.getEmailAddress().toUpperCase());
		this.setTransID(source.getTransID());
		this.setTrialAccess(source.getTrialAccess());
	}

	public boolean save(EeditionAccessLogIntf EeditionAccessLogIntf) throws UsatException {

		boolean result = false;
		EeditionAccessLogDAO EeditionAccessLogDAO = new EeditionAccessLogDAO();

		try {
			if (EeditionAccessLogIntf.getTransID() > 0) {
				EeditionAccessLogDAO.update(EeditionAccessLogIntf);
			} else {
				EeditionAccessLogDAO.insert(EeditionAccessLogIntf);
			}
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		}
		return result;
	}

	public boolean delete(EeditionAccessLogIntf EeditionAccessLogIntf) throws UsatException {

		boolean result = false;
		EeditionAccessLogDAO EeditionAccessLogDAO = new EeditionAccessLogDAO();

		try {
			EeditionAccessLogDAO.delete(EeditionAccessLogIntf);
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		}
		return result;
	}

	public static int deleteAttributesOlderThanSpecifiedDays(int numDays) throws UsatException {
		if (numDays < 0) {
			throw new UsatException("EeditionAccessLogBO::deleteAttributesOlderThanSpecifiedDays() - Invalid number of days: "
					+ numDays);
		}
		EeditionAccessLogDAO dao = new EeditionAccessLogDAO();
		return dao.deleteAttributesOlderThanSpecifiedDays(numDays);
	}

	private void appendPaddedString(StringBuffer buf, int fieldLength, String field) {
		if (buf == null) {
			return;
		}

		if (field == null) {
			for (int i = 0; i < fieldLength; i++) {
				buf.append(" ");
			}
			return;
		}

		int currentFieldLength = field.length();

		if (currentFieldLength <= fieldLength) {
			// no truncation
			buf.append(field);
			if (currentFieldLength != fieldLength) {
				// buffer remaining field with blanks
				for (int i = currentFieldLength; i < fieldLength; i++) {
					buf.append(" ");
				}
			}
		} else {
			// truncate
			buf.append(field.substring(0, fieldLength));
		}
	}

	public String getBatchFormattedString() throws UsatException {
		StringBuffer buf = new StringBuffer();
		try {
			this.appendPaddedString(buf, 10, String.valueOf(this.getTransID()));
			this.appendPaddedString(buf, 2, this.getPublication());
			this.appendPaddedString(buf, 7, this.getAccountNum());
			this.appendPaddedString(buf, 1, this.getTrialAccess());
			this.appendPaddedString(buf, 30, this.dateTime.toString());
			this.appendPaddedString(buf, 30, this.getClientIP());
			this.appendPaddedString(buf, 60, this.getEmailAddress().toUpperCase());
		} catch (Exception e) {
			System.out.println("EeditionAccessLogBO:getBatchFormattedString() - Failed to create batch record: " + e.getMessage());
			throw new UsatException(e);
		}

		return buf.toString();
	}

	/**
	 * @return Returns the publication.
	 */
	public String getPublication() {
		return publication;
	}

	/**
	 * @param publication
	 *            The publication to set.
	 */
	public void setPublication(String publication) {
		this.publication = publication;
	}

	public String getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public String getTrialAccess() {
		return trialAccess;
	}

	public void setTrialAccess(String trialAccess) {
		this.trialAccess = trialAccess;
	}

	public Timestamp getDateTime() {
		return dateTime;
	}

	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}
