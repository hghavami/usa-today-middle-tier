/*
 * Created on Apr 12, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import com.usatoday.business.interfaces.PersistentAddressIntf;

/**
 * @author aeast
 * @date Apr 12, 2006
 * @class PersistentAddressBO
 * 
 *        This class contains all fields required for a postal address as it is stored on the 400.
 * 
 */
public class PersistentAddressBO extends AddressBO implements PersistentAddressIntf {

	private String streetDir = "";
	private String halfUnitNum = "";
	private String streetName = "";
	private String streetPostDir = "";
	private String streetType = "";
	private String subUnitCode = "";
	private String subUnitNum = "";
	private String unitNum = "";
	private boolean militaryAddress = false;
	private boolean guiAddress = false;

	public PersistentAddressBO(String streetDir, String halfUnitNum, String city, String state, String streetName,
			String streetPostDir, String streetType, String subUnitCode, String subUnitNum, String unitNum, String zip,
			String address2, boolean militaryAddress, boolean guiAddress) {
		super();
		this.streetDir = streetDir;
		this.halfUnitNum = halfUnitNum;
		this.city = city;
		this.state = state;
		this.streetName = streetName;
		this.streetPostDir = streetPostDir;
		this.streetType = streetType;
		this.subUnitCode = subUnitCode;
		this.subUnitNum = subUnitNum;
		this.unitNum = unitNum;
		this.zip = zip;
		this.address2 = address2;
		this.militaryAddress = militaryAddress;
		this.guiAddress = guiAddress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.PersistentAddressIntf#getStreetDir()
	 */
	public String getStreetDir() {
		if (streetDir == null) {
			return "";
		}
		return streetDir;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.PersistentAddressIntf#getHalfUnitNum()
	 */
	public String getHalfUnitNum() {
		if (halfUnitNum == null) {
			return "";
		}
		return halfUnitNum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.PersistentAddressIntf#getStreetName()
	 */
	public String getStreetName() {
		if (streetName == null) {
			return "";
		}
		return streetName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.PersistentAddressIntf#getStreetType()
	 */
	public String getStreetType() {
		if (streetType == null) {
			return "";
		}
		return streetType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.PersistentAddressIntf#getStreetPostDir()
	 */
	public String getStreetPostDir() {
		if (streetPostDir == null) {
			return "";
		}
		return streetPostDir;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.PersistentAddressIntf#getSubUnitCode()
	 */
	public String getSubUnitCode() {
		if (subUnitCode == null) {
			return "";
		}
		return subUnitCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.PersistentAddressIntf#getSubUnitNum()
	 */
	public String getSubUnitNum() {
		if (subUnitNum == null) {
			return "";
		}
		return subUnitNum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.PersistentAddressIntf#getUnitNum()
	 */
	public String getUnitNum() {
		if (unitNum == null) {
			return "";
		}
		return unitNum;
	}

	public UIAddressBO convertToUIAddress() {
		UIAddressBO uiAddress = new UIAddressBO();

		// Address 1
		StringBuffer formattedAddr1 = new StringBuffer("");

		if (this.getUnitNum() != null && this.getUnitNum().trim().length() > 0) {
			formattedAddr1.append(this.getUnitNum().trim());
			formattedAddr1.append(" ");
		}

		if (this.getHalfUnitNum() != null && this.getHalfUnitNum().trim().length() > 0) {
			formattedAddr1.append(this.getHalfUnitNum().trim());
			formattedAddr1.append(" ");
		}

		if (this.getStreetDir() != null && this.getStreetDir().trim().length() > 0) {
			formattedAddr1.append(this.getStreetDir().trim());
			formattedAddr1.append(" ");
		}

		if (this.getStreetName() != null) {
			formattedAddr1.append(this.getStreetName().trim());
			formattedAddr1.append(" ");
		}

		if (this.getStreetType() != null && this.getStreetType().trim().length() > 0) {
			formattedAddr1.append(this.getStreetType().trim());
			formattedAddr1.append(" ");
		}

		if (this.getStreetPostDir() != null && this.getStreetPostDir().trim().length() > 0) {
			formattedAddr1.append(this.getStreetPostDir().trim());
			formattedAddr1.append(" ");
		}

		uiAddress.setAddress1(formattedAddr1.toString().trim());

		// Apartment/Suite
		StringBuffer formattedAddr2 = new StringBuffer("");

		if (this.getSubUnitCode() != null && this.getSubUnitCode().trim().length() > 0) {
			formattedAddr2.append(this.getSubUnitCode().trim());
			formattedAddr2.append(" ");
		}

		if (this.getSubUnitNum() != null && this.getSubUnitNum().trim().length() > 0) {
			formattedAddr2.append(this.getSubUnitNum().trim());
		}

		uiAddress.setAptSuite(formattedAddr2.toString().trim());

		uiAddress.setAddress2(this.getAddress2());
		uiAddress.setCity(this.getCity());
		uiAddress.setState(this.getState());
		uiAddress.setZip(this.getZip());

		if (this.validated) {
			uiAddress.setValidated(true);
		}

		return uiAddress;
	}

	public boolean isGUIAddress() {
		return this.guiAddress;
	}

	public boolean isMilitaryAddress() {
		return this.militaryAddress;
	}

	public String toString() {
		UIAddressBO addr = this.convertToUIAddress();

		return addr.toString();
	}
}
