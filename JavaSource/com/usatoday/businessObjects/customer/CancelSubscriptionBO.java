package com.usatoday.businessObjects.customer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.CancelSubscriptionIntf;
import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.businessObjects.util.mail.ExactTargetNoDatesNew;
import com.usatoday.businessObjects.util.mail.SmtpMailSender;
import com.usatoday.integration.CancelSubscriptionDAO;
import com.usatoday.util.constants.UsaTodayConstants;

public class CancelSubscriptionBO implements CancelSubscriptionIntf {

	/**
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cancelSubsReasonCode = null;
	private String cancelSubsReasonDesc = null;
	// internal use attributes
	private boolean confirmationEmailSent = false;
	private boolean isSWSubscription = false;
	// private boolean isUTSubscription = false;
	private boolean isEESubscription = false;
	private String comment = null;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public CancelSubscriptionBO(CancelSubscriptionIntf reasonsTO) {

		super();
		this.cancelSubsReasonCode = reasonsTO.getCancelSubsReasonCode();
		this.cancelSubsReasonDesc = reasonsTO.getCancelSubsReasonDesc();
	}

	public CancelSubscriptionBO() {

		super();
	}

	public static Collection<CancelSubscriptionIntf> getCancelSubscriptionReasons(String pubCode) throws UsatException {

		CancelSubscriptionDAO dao = new CancelSubscriptionDAO();

		Collection<CancelSubscriptionIntf> reasons = dao.getCancelSubscriptionReasons(pubCode);

		Collection<CancelSubscriptionIntf> boRecords = new ArrayList<CancelSubscriptionIntf>();

		if (reasons.size() > 0) {
			// convert to BO's
			Iterator<CancelSubscriptionIntf> itr = reasons.iterator();
			while (itr.hasNext()) {
				CancelSubscriptionIntf reasonsTO = itr.next();
				CancelSubscriptionBO bo = new CancelSubscriptionBO(reasonsTO);
				boRecords.add(bo);
			}
		}

		return boRecords;
	}

	public String getCancelSubsReasonCode() {

		return this.cancelSubsReasonCode;
	}

	public String getCancelSubsReasonDesc() {

		return this.cancelSubsReasonDesc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.shopping.OrderIntf#sendConfirmationEmail ()
	 */
	public void sendSubscriptionConfirmationEmail(CustomerIntf customer) throws UsatException {

		String eEditionDesc = null;
		String eEditionID_Cancels = UsaTodayConstants.EEDITION_CANCELS_ID;
		String eEditionDesc_Mail = UsaTodayConstants.EEDITION_NEWSTART_DESC;
		String eEditionDesc_EE = UsaTodayConstants.EEDITION_EE_NEWSTART_DESC;
		Boolean eEditionUTCompanion = UsaTodayConstants.UT_EE_COMPANION_ACTIVE;
		Boolean eEditionBWCompanion = UsaTodayConstants.BW_EE_COMPANION_ACTIVE;
		String eEditionUtLink = UsaTodayConstants.EEDITION_UT_LINK;
		String eEditionBwLink = UsaTodayConstants.EEDITION_BW_LINK;
		String eEditionLink = null;
		String ezpay = null;

		if (!this.confirmationEmailSent) {
			this.confirmationEmailSent = true;

			SmtpMailSender mail = new SmtpMailSender();

			mail.setMailServerHost(UsaTodayConstants.EMAIL_SERVER);
			mail.setSender(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS);

			String exactTargetEmailPub = null;
			String brandingPubCode = customer.getCurrentAccount().getProduct().getBrandingPubCode();
			if (brandingPubCode.equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
				this.isSWSubscription = true;
				exactTargetEmailPub = "BW";
				eEditionLink = eEditionBwLink;
				eEditionDesc = eEditionDesc_Mail;
			} else {
				if (brandingPubCode.equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE) && brandingPubCode.equalsIgnoreCase("UT")) {
					// this.isUTSubscription = true;
					exactTargetEmailPub = "UT";
					eEditionLink = eEditionUtLink;
				} else {
					this.isEESubscription = true;
					exactTargetEmailPub = "EE";
					eEditionLink = eEditionUtLink;
					eEditionDesc = eEditionDesc_EE;
				}
			}
			String messageSubject = null;
			if (!this.isSWSubscription) {
				messageSubject = "USA TODAY Subscription Notification";
			} else {
				messageSubject = "Sports Weekly Subscription Notification";
			}
			mail.setMessageSubject(messageSubject);
			String content = this.generateSubscriptionEmailBodyText(customer);
			mail.setMessageText(content);

			String recipientEmail = customer.getCurrentEmailRecord().getEmailAddress();

			// send confirmation email to billing contact if it exists.
			if (!customer.getCurrentAccount().isBillingSameAsDelivery() && customer.getCurrentAccount().getBillingContact() != null
					&& customer.getCurrentAccount().getBillingContact().getEmailAddress() != null
					&& !customer.getCurrentAccount().getBillingContact().getEmailAddress().equalsIgnoreCase(recipientEmail)) {
				recipientEmail = customer.getCurrentAccount().getBillingContact().getEmailAddress();
			}

			mail.addTORecipient(recipientEmail);
			// set ezpay
			if (customer.getCurrentAccount().isOnEZPay()) {
				ezpay = "Yes";
			} else {
				ezpay = "No";
			}

			// set gannett units
			String guiZipcode = "false";
			if (customer.getCurrentAccount().getDeliveryContact().getPersistentAddress().isGUIAddress()) {
				guiZipcode = "true";
			}

			DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
			java.util.Date date = new java.util.Date();
			String dateStr = dateFormat.format(date);

			String startDate = dateFormat.format(customer.getCurrentAccount().getStartDate());
			String confEmail = null;
			String firstName = null;
			String lastName = null;
			String company = null;
			String password = null;
			String phone = null;
			String gpFullName = null;
			String creditCard = null;
			String subscripType = "Cancel";
			SimpleDateFormat updatedate = new SimpleDateFormat("MM/dd/yyyy");
			String expirationDateSub = updatedate.format(customer.getCurrentAccount().getExpirationDate());
			UIAddressIntf address = null;
			confEmail = customer.getCurrentEmailRecord().getEmailAddress();
			firstName = customer.getCurrentAccount().getDeliveryContact().getFirstName();
			lastName = customer.getCurrentAccount().getDeliveryContact().getLastName();
			company = customer.getCurrentAccount().getDeliveryContact().getFirmName();
			password = customer.getCurrentAccount().getDeliveryContact().getPassword();
			phone = customer.getCurrentAccount().getDeliveryContact().getHomePhone();

			if (customer.getCurrentAccount().getDeliveryContact().getPersistentAddress() != null) {
				address = customer.getCurrentAccount().getDeliveryContact().getPersistentAddress().convertToUIAddress();
			} else {
				address = customer.getCurrentAccount().getDeliveryContact().getUIAddress();
			}
			if (address == null) {
				throw new UsatException("CancelSubscriptionBO:generateSubscriptionEmailBodyText() - Address object is null.");
			}

			String address1 = "";
			boolean isUTBranding = !this.isSWSubscription;
			eEditionDesc = customer.getCurrentAccount().getAccountNumber();
			try {
				if ((isUTBranding && (eEditionUTCompanion == true)) || (this.isSWSubscription && (eEditionBWCompanion == true))
						|| (this.isEESubscription)) {
					try {

						String returnCode = ExactTargetNoDatesNew.sendExactTargetNoDates(confEmail, eEditionLink, firstName,
								lastName, "0.0", creditCard, address1, address.getAddress2(), address.getAddress1(),
								eEditionID_Cancels, eEditionDesc, exactTargetEmailPub, address.getCity(), company, "0", address
										.getZip(), password, "Mail", "0", ezpay, dateStr, "1111", startDate, "0", subscripType,
								phone, address.getState(), gpFullName, guiZipcode, "", expirationDateSub, customer
										.getCurrentAccount().getAccountNumber(), "", "0");

						if (returnCode.equalsIgnoreCase("Error")) {
							try {
								mail.sendMessage();

							} catch (Exception e) {
								// e-mail not sent

							}
						}
					} catch (Exception e) {
						mail.sendMessage();

					}

				} else {
					mail.sendMessage();
				}

			} catch (Exception e) {
				System.out.println("CancelSubscriptionBO : Failed to send confirmation email: " + e.getMessage());
				System.out.println("To: " + customer.getCurrentAccount().getDeliveryContact().getEmailAddress());
				System.out.println("Contents: " + content);
				throw new UsatException(e);
			}

			// send developer copy in old format (not Exact Target generated)
			try {
				// mail.sendMessage();
				// send a copy to us.
				if (UsaTodayConstants.EMAIL_CONFIRMATIONS_COPYTO_ADDRESS != null
						&& UsaTodayConstants.EMAIL_CONFIRMATIONS_COPYTO_ADDRESS.trim().length() > 0) {
					mail.removeToRecipient(recipientEmail);
					mail.addTORecipient(UsaTodayConstants.EMAIL_CONFIRMATIONS_COPYTO_ADDRESS);
					mail.setMessageSubject(messageSubject + " Cancellation for Account: "
							+ customer.getCurrentAccount().getAccountNumber());

					StringBuffer newContent = new StringBuffer("This is a copy of an online subscription confirmation email.\n \n");
					newContent.append("Following was sent to customer.\n======= BEGIN Customer Email ==================\n");
					newContent.append(content);
					newContent
							.append("\n \n==============  End Customer Email =================================\n \n##################################\n##  Additional Order information not sent to customer:   ##\n############################\n");
					newContent.append("\tConfirmation Email Address: ").append(recipientEmail).append("\n");
					mail.setMessageText(newContent.toString());
					// send the copy
					mail.sendMessage();
				}
			} catch (Exception e) {
				System.out.println("CancelSubscriptionBO : Failed to send confirmation email: " + e.getMessage());
				System.out.println("To: " + customer.getCurrentAccount().getDeliveryContact().getEmailAddress());
				System.out.println("Contents: " + content);
				throw new UsatException(e);
			}
		}
	}

	/**
	 * This method is almost an exact copy of UTSubscription::getEmailText(). It should be completely rebuild such that it is not
	 * product dependent in order to achieve maximum flexibility.
	 * 
	 * @return
	 * @throws UsatException
	 */
	private String generateSubscriptionEmailBodyText(CustomerIntf customer) throws UsatException {

		StringBuffer email = new StringBuffer("\n \n");

		int pubnum = 0;
		SimpleDateFormat updatedate = new SimpleDateFormat("MM/dd/yyyy");
		String expirationDate = updatedate.format(customer.getCurrentAccount().getExpirationDate());

		if (this.isSWSubscription) {
			pubnum = 1;
		}

		UIAddressIntf address = null;

		if (customer.getCurrentAccount().getDeliveryContact().getPersistentAddress() != null) {
			address = customer.getCurrentAccount().getDeliveryContact().getPersistentAddress().convertToUIAddress();
		} else {
			address = customer.getCurrentAccount().getDeliveryContact().getUIAddress();
		}

		if (address == null) {
			throw new UsatException("CancelSubscriptionBO:generateSubscriptionEmailBodyText() - Address object is null.");
		}

		String address1 = address.getAddress1();
		if (address.getAptSuite() != null && address.getAptSuite().length() > 0) {
			address1 = address1 + " " + address.getAptSuite();
		}

		String address2 = address.getCity() + ", " + address.getState() + " " + address.getZip();

		String message1 = null;
		String message2 = null;
		String message3 = null;

		message1 = "Thank you for informing us of your subscription needs.  You have asked us to cancel your ";
		message2 = " subscription with account number ";
		message3 = ". Your account information is as follows:";

		email.append(message1);
		email.append(customer.getCurrentAccount().getProduct().getName());
		email.append(message2);
		email.append(customer.getCurrentAccount().getAccountNumber());
		email.append(message3);
		email.append("\n \n");
		email.append(customer.getCurrentAccount().getDeliveryContact().getFirstName());
		email.append(" ");
		email.append(customer.getCurrentAccount().getDeliveryContact().getLastName());
		email.append("\n");
		if (customer.getCurrentAccount().getDeliveryContact().getFirmName() != null
				&& !customer.getCurrentAccount().getDeliveryContact().getFirmName().trim().equals("")) {
			email.append(customer.getCurrentAccount().getDeliveryContact().getFirmName());
			email.append(".\n");
		}
		email.append(address1);
		email.append("\n");
		if ((address.getAddress2() != null) && (!address.getAddress2().trim().equals(""))) {
			email.append(address.getAddress2().trim());
			email.append("\n");
		}
		email.append(address2);
		email.append("\n");
		email.append(customer.getCurrentAccount().getDeliveryContact().getHomePhoneNumberFormatted());
		email.append("\n");

		if ((customer.getCurrentAccount().getDeliveryContact().getBusinessPhone() != null)
				&& (!customer.getCurrentAccount().getDeliveryContact().getBusinessPhone().trim().equals(""))) {
			email.append(customer.getCurrentAccount().getDeliveryContact().getWorkPhoneNumberFormatted());
			email.append("\n \n");
		}
		email.append("Reason for cancelling: ");
		email.append(this.getCancelSubsReasonDesc());
		email.append("\n");
		if (this.comment != null && !this.comment.trim().equals("")) {
			email.append("Comments: ");
			email.append(this.comment);
			email.append("\n");
		}
		email.append("\n");
		email.append("You currently are paid through ");
		email.append(expirationDate);
		email.append(". That is the last day you will have access to the e-Newspaper");
		email.append(".\n \n");

		if (UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT != null) {
			email.append("\n\n").append(UsaTodayConstants.EMAIL_GLOBAL_PROMO_TEXT);
		}

		email.append("\n \n");
		if (pubnum == 0) {
			email.append("USA TODAY, 7950 Jones Branch Dr, McLean, Virginia, 22108");
		} else {
			email.append("USA TODAY Sports Weekly, 7950 Jones Branch Dr, McLean, Virginia, 22108");
		}

		return email.toString();
	}

	public void setCancelSubsReasonCode(String cancelSubsReasonCode) {
		this.cancelSubsReasonCode = cancelSubsReasonCode;
	}

	public void setCancelSubsReasonDesc(String cancelSubsReasonDesc) {
		this.cancelSubsReasonDesc = cancelSubsReasonDesc;
	}

}
