/*
 * Created on Mar 4, 2011
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import java.sql.Timestamp;
import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.AdditionalDeliveryAddressIntf;
import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.integration.AdditionalDeliveryAddressDAO;

/**
 * @author hghavami
 * @date Mar 4, 2011
 * @class AdditionalAddressBO
 * 
 *        This class contains all fields required for the change delivery address screen.
 * 
 */
public class AdditionalDeliveryAddressBO implements AdditionalDeliveryAddressIntf {

	private int id = -1;
	private String currAcctNum = "";
	private String firstName = "";
	private String lastName = "";
	private String firmName = "";
	private String streetAddress = "";
	private String aptDeptSuite = "";
	private String addlAddr1 = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String homePhone = "";

	private String busPhone = "";

	private int emailID = -1;

	private Timestamp insertTimeStamp = null;

	public AdditionalDeliveryAddressBO() {
		super();
	}

	public AdditionalDeliveryAddressBO(AdditionalDeliveryAddressIntf source) {
		super();
		this.setCurrAcctNum(source.getCurrAcctNum());
		this.setFirstName(source.getFirstName());
		this.setLastName(source.getLastName());
		this.setFirmName(source.getFirmName());
		this.setStreetAddress(source.getStreetAddress());
		this.setAptDeptSuite(source.getAptDeptSuite());
		this.setAddlAddr1(source.getAddlAddr1());
		this.setCity(source.getCity());
		this.setState(source.getState());
		this.setZip(source.getZip());
		this.setHomePhone(source.getHomePhone());
		this.setBusPhone(source.getBusPhone());
		this.setEmailID(source.getEmailID());
		this.setInsertTimeStamp(source.getInsertTimeStamp());
	}

	public static boolean delete(AdditionalDeliveryAddressIntf additionalDeliveryAddressIntf) throws UsatException {

		boolean result = false;
		AdditionalDeliveryAddressDAO additionalDeliveryAddressDAO = new AdditionalDeliveryAddressDAO();

		try {
			additionalDeliveryAddressDAO.delete(additionalDeliveryAddressIntf.getId());
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		}
		return result;
	}

	public static boolean save(CustomerIntf customer) throws UsatException {

		boolean result = false;
		AdditionalDeliveryAddressDAO additionalDeliveryAddressDAO = new AdditionalDeliveryAddressDAO();

		try {
			additionalDeliveryAddressDAO.insert(customer);
		} catch (Exception e) {
			result = false;
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		}
		return result;
	}

	public static Collection<AdditionalDeliveryAddressIntf> select(String emailAddress) throws UsatException {

		Collection<AdditionalDeliveryAddressIntf> result = null;
		AdditionalDeliveryAddressDAO additionalDeliveryAddressDAO = new AdditionalDeliveryAddressDAO();

		try {
			result = additionalDeliveryAddressDAO.select(emailAddress);
		} catch (Exception e) {
			result = null;
			e.printStackTrace();
			System.out.println(e.getMessage());
			throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
		}
		return result;
	}

	public String getAddlAddr1() {
		return addlAddr1;
	}

	public String getAptDeptSuite() {
		return aptDeptSuite;
	}

	public String getBusPhone() {
		return busPhone;
	}

	public String getCity() {
		return city;
	}

	public String getCurrAcctNum() {
		return currAcctNum;
	}

	public int getEmailID() {
		return emailID;
	}

	public String getFirmName() {
		return firmName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public int getId() {
		return id;
	}

	public Timestamp getInsertTimeStamp() {
		return insertTimeStamp;
	}

	public String getLastName() {
		return lastName;
	}

	public String getState() {
		return state;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public String getZip() {
		return zip;
	}

	public void setAddlAddr1(String addlAddr1) {
		this.addlAddr1 = addlAddr1;
	}

	public void setAptDeptSuite(String aptDeptSuite) {
		this.aptDeptSuite = aptDeptSuite;
	}

	public void setBusPhone(String busPhone) {
		this.busPhone = busPhone;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCurrAcctNum(String currAcctnum) {
		this.currAcctNum = currAcctnum;
	}

	public void setEmailID(int emailID) {
		this.emailID = emailID;
	}

	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setInsertTimeStamp(Timestamp insertTimeStamp) {
		this.insertTimeStamp = insertTimeStamp;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
}
