package com.usatoday.businessObjects.customer;

import com.usatoday.UsatException;

public class LoginException extends UsatException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6125822574209192191L;
	private static final String[] errorText = {
			"Unknown Reason",
			"You have entered an invalid email or password.",
			"Invalid email or password. We have updated our login process to make it more secure. Please set up \"Online Account Access\" by selecting the link below.",
			"No corresponding account for user.",
			"Your USA TODAY account is currently inactive. To make a payment and re-activate your account call 1-800-872-0001. If you are a new subscriber, your e-Newspaper access will begin on the start date you requested for your subscription. If you have any questions, please select the Contact Us link below for assistance.",
			"Our records indicate the account you are trying to access may be stopped. To resume your delivery, please select the \"Subscribe\" button on the left side of the screen. For additional assistance, please contact National Customer Service at 800-872-0001.",
			"Our records indicate the password for the account you are trying to access needs to be reset.  Please use the \"Forgot Password\" link below to begin the process."};
	public static final int UNKNOWN_ERROR = 0;
	public static final int INVALID_LOGIN_ATTEMPT = 1;
	public static final int NO_ACCOUNT_SETUP = 2;
	public static final int NO_AS400_ACCOUNT_SETUP = 3;
	public static final int NO_AS400_ACCOUNT_SETUP_EDITION_ACCESS_EXPIRED = 4;
	public static final int EMAIL_RECORDS_BUT_NO_ACCOUNTS = 5;
	public static final int RESET_PASSWORD_REQUIRED = 6;

	private int errorCode = LoginException.UNKNOWN_ERROR;

	/**
	 * 
	 * @param errorCode
	 *            - the reason for the exception.
	 */
	public LoginException(int errorCode) {
		super();
		if (errorCode < LoginException.errorText.length && errorCode >= 0) {
			this.errorCode = errorCode;
		} else {
			this.errorCode = 0;
		}
	}

	/**
	 * 
	 * @return The reason code
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * 
	 * @return Informative message about the login issue
	 */
	public String getErrorMessage() {
		if (this.errorCode >= 0 && this.errorCode < LoginException.errorText.length) {
			return LoginException.errorText[this.errorCode];
		} else {
			return LoginException.errorText[0];
		}
	}
}
