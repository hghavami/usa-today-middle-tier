/*
 * Created on Feb 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.CreditCardInfoIntf;
import com.usatoday.integration.CreditCardInfoDAO;

/**
 * @author aeast
 * @date Feb 2, 2007
 * @class UserBO
 * 
 *        Concrete implementation of a User.
 * 
 */
public class CreditCardInfoBO implements CreditCardInfoIntf {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6996028023384178078L;
	private String creditCardNewGift = "";
	private String creditCardNum = "";
	private String creditCardExp = "";

	/**
     * 
     */
	public CreditCardInfoBO() {
		super();
	}

	/**
	 * 
	 * @param email
	 * @param street2
	 * @return
	 * @throws UsatException
	 */

	public CreditCardInfoBO determineCreditCardInfo(String pub, String accountNum, String newGift, String creditCardNum,
			String creditCardExp) throws UsatException {

		CreditCardInfoBO CreditCardInfo = null;

		CreditCardInfoDAO dao = new CreditCardInfoDAO();

		CreditCardInfo = dao.determineCreditCardInfo(pub, accountNum, newGift, creditCardNum, creditCardExp);
		return CreditCardInfo;
	}

	/**
	 * @return Returns the CreditCardInfo.
	 */
	public String getCreditCardNewGift() {
		return creditCardNewGift;
	}

	/**
	 * @param CreditCardInfo
	 *            The CreditCardInfo to set.
	 */
	public void setCreditCardNewGift(String creditCardNewGift) {
		this.creditCardNewGift = creditCardNewGift;
	}

	public String getCreditCardExp() {
		return creditCardExp;
	}

	public void setCreditCardExp(String creditCardExp) {
		this.creditCardExp = creditCardExp;
	}

	public String getCreditCardNum() {
		return creditCardNum;
	}

	public void setCreditCardNum(String creditCardNum) {
		this.creditCardNum = creditCardNum;
	}
}
