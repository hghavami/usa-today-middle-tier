/*
 * Created on Apr 12, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import com.usatoday.business.interfaces.UIAddressIntf;

/**
 * @author aeast
 * @date Apr 12, 2006
 * @class UIAddressBO
 * 
 *        This class represents a postal address as it is presented to users
 * 
 */
public class UIAddressBO extends AddressBO implements UIAddressIntf {

	private String address1 = null;
	private String aptSuite = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.UIAddressIntf#getAddress1()
	 */
	public String getAddress1() {
		return address1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.UIAddressIntf#getAptSuite()
	 */
	public String getAptSuite() {
		return aptSuite;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public void setAptSuite(String aptSuite) {
		this.aptSuite = aptSuite;
	}

	public String toString() {
		StringBuffer addressBuf = new StringBuffer();
		addressBuf.append("Address1: " + this.getAddress1());
		addressBuf.append(";Apt/Ste: " + this.getAptSuite());
		addressBuf.append(";Address2:  " + this.getAddress2());
		addressBuf.append(";City: " + this.getCity());
		addressBuf.append(";State: " + this.getState());
		addressBuf.append(";Zip: " + this.getZip());

		return addressBuf.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof UIAddressBO)) {
			return false;
		}

		UIAddressBO a = (UIAddressBO) obj;

		if (this.getZip() != null && this.getZip().equalsIgnoreCase(a.getZip())) {
			// zips equal check further
			if (this.getAddress1() != null && this.getAddress1().equalsIgnoreCase(a.getAddress1())) {
				// these addresses are going to be equal
				return true;
			} else {
				return false;
			}
		} else {
			return false; // zip aren't equal so can't be the same
		}
	}
}
