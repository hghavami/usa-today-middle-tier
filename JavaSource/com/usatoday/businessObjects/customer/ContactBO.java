package com.usatoday.businessObjects.customer;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.PersistentAddressIntf;
import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.ContactIntf;
import com.usatoday.businessObjects.util.Code1AddressValidator;
import com.usatoday.util.constants.UsaTodayConstants;

public class ContactBO implements ContactIntf {

	private String lastName = "";
	private String firstName = "";
	private String firmName = "";
	private UIAddressIntf uiAddress = null;
	private PersistentAddressIntf persistentAddress = null;
	private String homePhone = "";
	private String businessPhone = "";
	private String emailAddress = null;
	private String password = "";
	private boolean addressVerified = false;

	boolean goodAddress = true;
	boolean questionableAddress = false;

	public ContactBO() {
		super();
	}

	public String getLastName() {
		return lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getFirmName() {
		return firmName;
	}

	public UIAddressIntf getUIAddress() {
		return uiAddress;
	}

	public PersistentAddressIntf getPersistentAddress() {
		return persistentAddress;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public UIAddressIntf getUiAddress() {
		return this.uiAddress;
	}

	public void setBusinessPhone(String businessPhone) {
		if (businessPhone != null) {
			this.businessPhone = businessPhone.trim();
		} else {
			this.businessPhone = businessPhone;
		}
	}

	public void setEmailAddress(String emailAddress) {
		if (emailAddress != null) {
			this.emailAddress = emailAddress.trim();
		} else {
			this.emailAddress = emailAddress;
		}
	}

	public void setFirmName(String firmName) {
		if (firmName != null) {
			this.firmName = firmName.trim();
		} else {
			this.firmName = firmName;
		}
	}

	public void setFirstName(String firstName) {
		if (firstName != null) {
			this.firstName = firstName.trim();
		} else {
			this.firstName = firstName;
		}
	}

	public void setHomePhone(String homePhone) {
		if (homePhone != null) {
			this.homePhone = homePhone.trim();
		} else {
			this.homePhone = homePhone;
		}
	}

	public void setLastName(String lastName) {
		if (lastName != null) {
			this.lastName = lastName.trim();
		} else {
			this.lastName = lastName;
		}
	}

	public void setUIAddress(UIAddressIntf address) {
		this.uiAddress = address;
	}

	public void setPersistentAddress(PersistentAddressIntf address) {
		this.persistentAddress = address;
	}

	/**
	 * 
	 * @return true if the contact information is valid otherwise false Verifies mailing address only
	 * @throws UsatException
	 */
	public boolean validateContact() throws UsatException {
		boolean validContact = true;
		if (this.getUiAddress() == null && this.getPersistentAddress() == null) {
			return false;
		}

		if (this.getPersistentAddress() != null && this.getPersistentAddress().isValidated()) {
			if (this.uiAddress == null) {
				this.uiAddress = this.getPersistentAddress().convertToUIAddress();
			}
			validContact = true;
		} else {
			// no persistent address object so need to validate the UI address
			if (UsaTodayConstants.VALIDATE_POSTAL_ADDRESSES) {
				Code1AddressValidator validator = new Code1AddressValidator();
				validator.setSourceContact(this);

				try {
					// this.persistentAddress = validator.validateAddress();
					this.persistentAddress = validator.validateGenesysAddress();

					if (this.persistentAddress != null) {
						try {
							this.uiAddress = this.persistentAddress.convertToUIAddress();
						} catch (Exception e) {
							; // ignore
						}
					}
				} catch (UsatException ue) {
					// problems communicating with code 1
					if (UsaTodayConstants.ALLOW_FAILED_ADDRESSES) {
						this.addressVerified = false;
						this.goodAddress = true; // assume good address
						this.questionableAddress = false;
						validContact = true;
						return validContact;
					} else {
						throw ue;
					}
				}

				this.goodAddress = validator.isGoodAddress();
				this.questionableAddress = validator.isQuestionableAddress();
			} else {
				this.goodAddress = true;
				this.questionableAddress = false;
			}
		}

		if (this.isGoodAddress()) {
			validContact = true;
			if (UsaTodayConstants.VALIDATE_POSTAL_ADDRESSES) {
				this.addressVerified = true;
			} else {
				this.addressVerified = false;
			}
		} else {
			this.addressVerified = false;
			validContact = false;
		}

		return validContact;
	}

	public boolean validateContactEmailAddress() throws UsatException {
		return ContactBO.validateContactEmailAddress(this.emailAddress);
	}

	public static boolean validateContactEmailAddress(String address) throws UsatException {
		boolean valid = false;
		try {
			if (address == null || address.length() < 3) {
				return false;
			}

			@SuppressWarnings("unused")
			javax.mail.internet.InternetAddress iaddress = new javax.mail.internet.InternetAddress(address);
			valid = true;
		} catch (javax.mail.internet.AddressException e) {
			valid = false;
		}

		return valid;
	}

	public boolean isGoodAddress() {
		return this.goodAddress;
	}

	public boolean isQuestionableAddress() {
		return this.questionableAddress;
	}

	public boolean isAddressVerified() {
		return this.addressVerified;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.customer.ContactIntf#getPhoneNumberFormatted()
	 */
	public String getHomePhoneNumberFormatted() {
		StringBuffer phone = new StringBuffer("");
		if (this.homePhone != null && this.homePhone.length() == 10) {
			phone.append("(").append(this.homePhone.substring(0, 3));
			phone.append(") ").append(this.homePhone.substring(3, 6));
			phone.append("-").append(this.homePhone.substring(6));
		}
		return phone.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatoday.business.interfaces.customer.ContactIntf#getWorkPhoneNumberFormatted()
	 */
	public String getWorkPhoneNumberFormatted() {
		StringBuffer phone = new StringBuffer("");
		if (this.businessPhone != null && this.businessPhone.length() == 10) {
			phone.append("(").append(this.businessPhone.substring(0, 3));
			phone.append(") ").append(this.businessPhone.substring(3, 6));
			phone.append("-").append(this.businessPhone.substring(6));
		}
		return phone.toString();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if (password != null) {
			this.password = password.trim();
		} else {
			this.password = password;
		}
	}

}
