/*
 * Created on Apr 20, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import java.sql.Timestamp;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.EeditionTrialAccessIntf;

/**
 * @author aeast
 * @date Apr 20, 2006
 * @class AddressBO
 * 
 *        based class implementation of a postal address.
 * 
 */
public class EeditionTrialAccessBO implements EeditionTrialAccessIntf {

	private String active = null;
	private String additionalAddress = null;
	private String address1 = null;
	private String address2 = null;
	private String aptSuite = null;
	private String city = null;
	private String clientIP = null;
	private String companyName = null;
	private String country = null;
	private String emailAddress = null;
	private Timestamp endDate = null;
	private String firstName = null;
	private Timestamp insertDateTime = null;
	private String keyCode = null;
	private String lastName = null;
	private String partnerID = null;
	private String partnerNumber = null;
	private String password = null;
	private String phone = null;
	private String pubCode = null;
	private String repID = null;
	private Timestamp startDate = null;
	private String state = null;
	private String subscribed = null;
	private long transID = 0;
	private String zip = null;

	/**
     * 
     */
	public EeditionTrialAccessBO() {
		super();
	}

	public EeditionTrialAccessBO(EeditionTrialAccessIntf source) {
		super();
		this.setTransID(source.getTransID());
		this.setActive(source.getActive());
		this.setAdditionalAddress(source.getAdditionalAddress());
		this.setAddress1(source.getAddress1());
		this.setAddress2(source.getAddress2());
		this.setAptSuite(source.getAptSuite());
		this.setCity(source.getCity());
		this.setClientIP(source.getClientIP());
		this.setCompanyName(source.getCompanyName());
		this.setCountry(source.getCountry());
		this.setEmailAddress(source.getEmailAddress().toUpperCase());
		this.setEndDate(source.getEndDate());
		this.setFirstName(source.getFirstName());
		this.setInsertDateTime(source.getInsertDateTime());
		this.setKeyCode(source.getKeyCode());
		this.setLastName(source.getLastName());
		this.setPartnerID(source.getPartnerID());
		this.setPartnerNumber(source.getPartnerNumber());
		this.setPassword(source.getPassword());
		this.setPhone(source.getPhone());
		this.setPubCode(source.getPubCode());
		this.setRepID(source.getRepID());
		this.setStartDate(source.getStartDate());
		this.setState(source.getState());
		this.setSubscribed(source.getSubscribed());
		this.setTransID(source.getTransID());
		this.setZip(source.getZip());
	}

	private void appendPaddedString(StringBuffer buf, int fieldLength, String field) {
		if (buf == null) {
			return;
		}

		if (field == null) {
			for (int i = 0; i < fieldLength; i++) {
				buf.append(" ");
			}
			return;
		}

		int currentFieldLength = field.length();

		if (currentFieldLength <= fieldLength) {
			// no truncation
			buf.append(field);
			if (currentFieldLength != fieldLength) {
				// buffer remaining field with blanks
				for (int i = currentFieldLength; i < fieldLength; i++) {
					buf.append(" ");
				}
			}
		} else {
			// truncate
			buf.append(field.substring(0, fieldLength));
		}
	}

	public String getBatchFormattedString() throws UsatException {
		StringBuffer buf = new StringBuffer();
		try {

			this.appendPaddedString(buf, 10, String.valueOf(this.getTransID()));
			this.appendPaddedString(buf, 1, this.getSubscribed());
			this.appendPaddedString(buf, 10, this.getPubCode());
			this.appendPaddedString(buf, 60, this.getEmailAddress().toUpperCase());
			this.appendPaddedString(buf, 10, this.getPassword());
			this.appendPaddedString(buf, 30, this.getStartDate().toString());
			this.appendPaddedString(buf, 30, this.getEndDate().toString());
			this.appendPaddedString(buf, 30, this.getInsertDateTime().toString());
			this.appendPaddedString(buf, 10, this.getKeyCode());
			this.appendPaddedString(buf, 10, this.getFirstName());
			this.appendPaddedString(buf, 15, this.getLastName());
			this.appendPaddedString(buf, 128, this.getCompanyName());
			this.appendPaddedString(buf, 128, this.getAddress1());
			this.appendPaddedString(buf, 128, this.getAddress2());
			this.appendPaddedString(buf, 28, this.getAptSuite());
			this.appendPaddedString(buf, 128, this.getAdditionalAddress());
			this.appendPaddedString(buf, 128, this.getCity());
			this.appendPaddedString(buf, 30, this.getState());
			this.appendPaddedString(buf, 10, this.getZip());
			this.appendPaddedString(buf, 32, this.getCountry());
			this.appendPaddedString(buf, 15, this.getPhone());
			this.appendPaddedString(buf, 32, this.getClientIP());
			this.appendPaddedString(buf, 32, this.getPartnerID());
			this.appendPaddedString(buf, 128, this.getPartnerNumber());
			this.appendPaddedString(buf, 1, this.getActive());
			this.appendPaddedString(buf, 64, this.getRepID());
		} catch (Exception e) {
			System.out
					.println("EeditionTrialAccessBO:getBatchFormattedString() - Failed to create batch record: " + e.getMessage());
			throw new UsatException(e);
		}

		return buf.toString();
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getAdditionalAddress() {
		return this.additionalAddress;
	}

	public void setAdditionalAddress(String additionalAddress) {
		this.additionalAddress = additionalAddress;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAptSuite() {
		return this.aptSuite;
	}

	public void setAptSuite(String aptSuite) {
		this.aptSuite = aptSuite;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getClientIP() {
		return this.clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Timestamp getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Timestamp getInsertDateTime() {
		return this.insertDateTime;
	}

	public void setInsertDateTime(Timestamp insertDateTime) {
		this.insertDateTime = insertDateTime;
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPartnerID() {
		return this.partnerID;
	}

	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}

	public String getPartnerNumber() {
		return this.partnerNumber;
	}

	public void setPartnerNumber(String partnerNumber) {
		this.partnerNumber = partnerNumber;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public String getRepID() {
		return this.repID;
	}

	public void setRepID(String repID) {
		this.repID = repID;
	}

	public Timestamp getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSubscribed() {
		return this.subscribed;
	}

	public void setSubscribed(String subscribed) {
		this.subscribed = subscribed;
	}

	public long getTransID() {
		return this.transID;
	}

	public void setTransID(long transID) {
		this.transID = transID;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
}
