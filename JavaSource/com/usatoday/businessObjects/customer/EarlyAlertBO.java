/*
 * Created on Feb 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.EarlyAlertIntf;
import com.usatoday.integration.EarlyAlertDAO;
import com.usatoday.integration.EarlyAlertTO;

/**
  * 
 */
public class EarlyAlertBO implements EarlyAlertIntf {

	public static final String DEFAULT_DISPLAY = "Not Determined";
	/**
	 * 
	 */
	private static final long serialVersionUID = 7508649824013457630L;

	private String earlyAlert = "";
	private String earlyAlertDelay = null;

	/**
     * 
     */
	public EarlyAlertBO() {
		super();
	}

	public String determineEarlyAlertNew(String pub, String accountNum) throws UsatException {

		EarlyAlertDAO dao = new EarlyAlertDAO();

		EarlyAlertTO to = dao.determineEarlyAlertNew(pub, accountNum);
		this.earlyAlert = to.getEarlyAlert();
		this.earlyAlertDelay = to.getEarlyAlertDelay();

		return this.earlyAlertDelay;
	}

	/**
	 * @return Returns the earlyAlert.
	 */
	public String getEarlyAlert() {
		return earlyAlert;
	}

	/**
	 * @param earlyAlert
	 *            The earlyAlert to set.
	 */
	public void setEarlyAlert(String earlyAlert) {
		this.earlyAlert = earlyAlert;
	}

	public String getEarlyAlertDelay() {
		return this.earlyAlertDelay;
	}

}
