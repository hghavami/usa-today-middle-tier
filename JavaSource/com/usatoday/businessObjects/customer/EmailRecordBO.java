package com.usatoday.businessObjects.customer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;
import com.usatoday.integration.EmailRecordDAO;
import com.usatoday.util.constants.UsaTodayConstants;

public class EmailRecordBO implements EmailRecordIntf {

	
	private String accountNumber = null;
	private boolean active = true;
	private String emailAddress = null;
	private boolean gift = false;
	private Date lastUpdated = null;
	private String orderID = null;

	private String password = null;

	private String permStartDate = null;
	private String confirmationEmailSent = null;
	private String pubCode = null;

	private int serialNumber = -1;

	private String zip = null;
	private String userId = null;
	private String firstName = null;
	private String lastName = null;
	private String autoLogin = null;
	private String sessionKey = null;

	public EmailRecordBO(EmailRecordIntf rec) {
		super();
		this.emailAddress = rec.getEmailAddress();
		this.password = rec.getPassword();
		this.serialNumber = rec.getSerialNumber();
		this.orderID = rec.getOrderID();
		this.accountNumber = rec.getAccountNumber();
		this.lastUpdated = rec.getLastUpdated();
		this.pubCode = rec.getPubCode();
		this.active = rec.isActive();
		this.gift = rec.isGift();
		this.permStartDate = rec.getPermStartDate();
		this.zip = rec.getZip();
		this.confirmationEmailSent = rec.getConfirmationEmailSent();
		this.userId = rec.getFireflyUserId();
		this.firstName = rec.getFirstName();
		this.lastName = rec.getLastName();
		this.autoLogin = rec.getAutoLogin();
		this.sessionKey = rec.getSessionKey();
	}

	public EmailRecordBO(String emailAddress, String password, int serialNumber, String orderID, String accountNumber,
			Date lastUpdated, String pubCode, boolean active, boolean gift, String confirmationEmailSent, String permStartDate,
			String zip, String userId, String firstName, String lastName, String autoLogin, String sessionKey) {
		super();
		this.emailAddress = emailAddress;
		this.password = password;
		this.serialNumber = serialNumber;
		this.orderID = orderID;
		this.accountNumber = accountNumber;
		this.lastUpdated = lastUpdated;
		this.pubCode = pubCode;
		this.active = active;
		this.gift = gift;
		this.permStartDate = permStartDate;
		this.zip = zip;
		this.confirmationEmailSent = confirmationEmailSent;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.autoLogin = autoLogin;
		this.sessionKey = sessionKey;
	}
	
	/**
	 * 
	 * @param accountNumber
	 * @return
	 * @throws UsatException
	 */
	public static Collection<EmailRecordIntf> getEmailRecordsForAccount(String accountNumber) throws UsatException {
		Collection<EmailRecordIntf> emailBoRecords = new ArrayList<EmailRecordIntf>();

		Collection<EmailRecordIntf> records = null;

		EmailRecordDAO dao = new EmailRecordDAO();

		records = dao.getEmailRecordsForAccountNumber(accountNumber);

		if (records != null && records.size() > 0) {
			// convert to Business Objects
			Iterator<EmailRecordIntf> itr = records.iterator();
			while (itr.hasNext()) {
				EmailRecordIntf rec = itr.next();
				EmailRecordBO bo = new EmailRecordBO(rec);
				emailBoRecords.add(bo);
			}
		}
		return emailBoRecords;
	}

	/**
	 * 
	 * @param accountNumber
	 * @return
	 * @throws UsatException
	 */
	public static Collection<EmailRecordIntf> getEmailRecordsForAccountPub(String pubCode, String accountNumber)
			throws UsatException {
		Collection<EmailRecordIntf> emailBoRecords = new ArrayList<EmailRecordIntf>();

		Collection<EmailRecordIntf> records = null;

		EmailRecordDAO dao = new EmailRecordDAO();

		records = dao.getEmailRecordsForAccountNumberAndPub(accountNumber, pubCode);

		if (records != null && records.size() > 0) {
			// convert to Business Objects
			Iterator<EmailRecordIntf> itr = records.iterator();
			while (itr.hasNext()) {
				EmailRecordIntf rec = (EmailRecordIntf) itr.next();
				EmailRecordBO bo = new EmailRecordBO(rec);
				emailBoRecords.add(bo);
			}
		}
		return emailBoRecords;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param accountNumber
	 * @return
	 * @throws UsatException
	 */
	public static Collection<EmailRecordIntf> getEmailRecordsForEmailAccount(String emailAddress, String accountNumber)
			throws UsatException {
		Collection<EmailRecordIntf> emailBoRecords = new ArrayList<EmailRecordIntf>();

		Collection<EmailRecordIntf> records = null;

		EmailRecordDAO dao = new EmailRecordDAO();

		records = dao.getEmailRecordsForEmailAddressAndAccount(emailAddress, accountNumber);

		if (records != null && records.size() > 0) {
			// convert to Business Objects
			Iterator<EmailRecordIntf> itr = records.iterator();
			while (itr.hasNext()) {
				EmailRecordIntf rec = (EmailRecordIntf) itr.next();
				EmailRecordBO bo = new EmailRecordBO(rec);
				emailBoRecords.add(bo);
			}
		}
		return emailBoRecords;
	}

	/**
	 * 
	 * @param emailAddress
	 * @return
	 * @throws UsatException
	 */
	public static Collection<EmailRecordIntf> getEmailRecordsForEmailLike(String emailAddress) throws UsatException {
		Collection<EmailRecordIntf> emailBoRecords = new ArrayList<EmailRecordIntf>();

		Collection<EmailRecordIntf> records = null;

		EmailRecordDAO dao = new EmailRecordDAO();

		if (emailAddress != null) {
			if (emailAddress.length() > 0) {
				emailAddress = "%" + emailAddress + "%";
			}
		}

		records = dao.getEmailRecordsForEmailAddressLike(emailAddress);

		if (records != null && records.size() > 0) {
			// convert to Business Objects
			Iterator<EmailRecordIntf> itr = records.iterator();
			while (itr.hasNext()) {
				EmailRecordIntf rec = (EmailRecordIntf) itr.next();
				EmailRecordBO bo = new EmailRecordBO(rec);
				emailBoRecords.add(bo);
			}
		}
		return emailBoRecords;
	}

	/**
	 * 
	 * @param emailAddress
	 * @return
	 * @throws UsatException
	 */
	public static Collection<EmailRecordIntf> getEmailRecordsForEmailAddress(String emailAddress) throws UsatException {
		Collection<EmailRecordIntf> emailBoRecords = new ArrayList<EmailRecordIntf>();

		Collection<EmailRecordIntf> records = null;

		EmailRecordDAO dao = new EmailRecordDAO();

		records = dao.getEmailRecordsForEmailAddress(emailAddress);

		if (records != null && records.size() > 0) {
			// convert to Business Objects
			Iterator<EmailRecordIntf> itr = records.iterator();
			while (itr.hasNext()) {
				EmailRecordIntf rec = (EmailRecordIntf) itr.next();
				EmailRecordBO bo = new EmailRecordBO(rec);
				emailBoRecords.add(bo);
			}
		}
		return emailBoRecords;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param accountNumber
	 * @return
	 * @throws UsatException
	 */
	public static Collection<EmailRecordIntf> getEmailRecordsForEmailPwdAccount(String emailAddress, String password,
			String accountNumber) throws UsatException {
		Collection<EmailRecordIntf> emailBoRecords = new ArrayList<EmailRecordIntf>();

		Collection<EmailRecordIntf> records = null;

		EmailRecordDAO dao = new EmailRecordDAO();

		records = dao.getEmailRecordsForEmailAddressAndPasswordAndAccount(emailAddress, password, accountNumber);

		if (records != null && records.size() > 0) {
			// convert to Business Objects
			Iterator<EmailRecordIntf> itr = records.iterator();
			while (itr.hasNext()) {
				EmailRecordIntf rec = itr.next();
				EmailRecordBO bo = new EmailRecordBO(rec);
				emailBoRecords.add(bo);
			}
		}
		return emailBoRecords;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param accountNumber
	 * @return
	 * @throws UsatException
	 */
	public static Collection<EmailRecordIntf> getEmailRecordsForEmailPwd(String emailAddress, String password) throws UsatException {
		Collection<EmailRecordIntf> emailBoRecords = new ArrayList<EmailRecordIntf>();

		Collection<EmailRecordIntf> records = null;

		EmailRecordDAO dao = new EmailRecordDAO();

		records = dao.getEmailRecordsForEmailAddressAndPassword(emailAddress, password);

		if (records != null && records.size() > 0) {
			// convert to Business Objects
			Iterator<EmailRecordIntf> itr = records.iterator();
			while (itr.hasNext()) {
				EmailRecordIntf rec = itr.next();
				EmailRecordBO bo = new EmailRecordBO(rec);
				emailBoRecords.add(bo);
			}
		}
		return emailBoRecords;
	}

	/**
	 * 
	 * @param emailAddress
	 * @param password
	 * @param accountNumber
	 * @return
	 * @throws UsatException
	 */
	public static Collection<EmailRecordIntf> getEmailRecordsForPubEmailAccount(String pubCode, String emailAddress,
			String accountNumber) throws UsatException {
		Collection<EmailRecordIntf> emailBoRecords = new ArrayList<EmailRecordIntf>();

		Collection<EmailRecordIntf> records = null;

		EmailRecordDAO dao = new EmailRecordDAO();

		// records = dao.getEmailRecordsForPubAndEmailAddressAndAccount(pubCode, emailAddress, accountNumber);
		records = dao.getEmailRecordsForEmailAddressLike(emailAddress);

		if (records != null && records.size() > 0) {
			// convert to Business Objects
			Iterator<EmailRecordIntf> itr = records.iterator();
			while (itr.hasNext()) {
				EmailRecordIntf rec = (EmailRecordIntf) itr.next();
				EmailRecordBO bo = new EmailRecordBO(rec);
				emailBoRecords.add(bo);
			}
		}
		return emailBoRecords;
	}

	/**
	 * 
	 * @param key
	 * @return
	 * @throws UsatException
	 */
	public static EmailRecordBO getEmailRecordsForPrimaryKey(int key) throws UsatException {

		EmailRecordIntf record = null;
		EmailRecordBO bo = null;

		EmailRecordDAO dao = new EmailRecordDAO();

		record = dao.getEmailRecordForSerialNumber(key);

		if (record != null) {
			bo = new EmailRecordBO(record);
		}

		return bo;
	}


	public String getAccountNumber() {
		return accountNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public String getOrderID() {
		return orderID;
	}

	public String getPassword() {
		return password;
	}

	public String getPermStartDate() {
		return this.permStartDate;
	}

	public String getPubCode() {
		return pubCode;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public String getZip() {
		return this.zip;
	}

	public boolean isActive() {
		return active;
	}

	public boolean isGift() {
		return gift;
	}

	/**
	 * 
	 * @param emailRecord
	 */
	public void save() {
		try {
			EmailRecordDAO eDao = new EmailRecordDAO();
			if (this.serialNumber < 0) {
				int key = eDao.insert(this);
				this.serialNumber = key;
			} else {
				eDao.update(this);
			}
		} catch (Exception e) {
			System.out.println("Failed to Save Email Record for Subscription Order: " + e.getMessage());
			StringBuffer bodyText = new StringBuffer();
			System.out.println("Email Record: " + bodyText.toString());
			// do not throw exception as we were able to save the transaction record.

		} // end catch
	} // end perist email rec

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setActive(boolean activeFlag) {
		this.active = activeFlag;
	}

	public void setEmailAddress(String email) {
		this.emailAddress = email;
	}

	public void setGift(boolean giftFlag) {
		this.gift = giftFlag;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPermStartDate(String date) {
		this.permStartDate = date;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getConfirmationEmailSent() {
		return this.confirmationEmailSent;
	}

	public void setConfirmationEmailSent(String c) {
		try {
			if (c != null) {
				String newVal = c.trim();
				if ("Y".equalsIgnoreCase(newVal)) {
					this.confirmationEmailSent = "Y";
				} else if ("N".equalsIgnoreCase(newVal)) {
					this.confirmationEmailSent = "N";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.confirmationEmailSent = "";
		}
	}

	public boolean isStartDateInFuture() {
		boolean startDateInFuture = false;
		try {
			if (this.permStartDate != null && this.permStartDate.length() == 8 && !this.permStartDate.equalsIgnoreCase("00000000")) {
				DateTime dt = UsatDateTimeFormatter.convertYYYYMMDDToDateTime(this.getPermStartDate());
				if (this.pubCode.equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
					dt = dt.minusDays(6);
					if (dt.isAfterNow()) {
						startDateInFuture = true;
					}
				}
				// else if (this.pubCode.equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
				// dt = dt.minusDays(5);
				// if (dt.isAfterNow()) {
				// startDateInFuture = true;
				// }
				// }
				else {
					if (dt.isAfterNow()) {
						startDateInFuture = true;
					}
				}

			}
		} catch (Exception e) {
			startDateInFuture = false;
			e.printStackTrace();
		}
		return startDateInFuture;
	}

	public String getFireflyUserId() {
		return userId;
	}

	public void setFireflyUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAutoLogin() {
		return autoLogin;
	}

	public void setAutoLogin(String autoLogin) {
		this.autoLogin = autoLogin;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

}
