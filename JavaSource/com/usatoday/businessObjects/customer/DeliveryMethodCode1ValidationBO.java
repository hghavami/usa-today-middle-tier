/*
 * Created on Feb 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.businessObjects.customer;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.DeliveryMethodIntf;
import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.integration.DeliveryMethodDAO;
import com.usatoday.integration.DeliveryMethodTO;

/**
 * @author aeast
 * @date Feb 2, 2007
 * @class UserBO
 * 
 *        Concrete implementation of a User.
 * 
 */
public class DeliveryMethodCode1ValidationBO implements DeliveryMethodIntf {

	public static final String DEFAULT_DISPLAY = "Not Determined";
	/**
	 * 
	 */
	private static final long serialVersionUID = 7508649824013457630L;

	private String deliveryMethod = "";
	private String deliveryMethodDelay = null;

	/**
     * 
     */
	public DeliveryMethodCode1ValidationBO() {
		super();
	}

	/**
	 * 
	 * @param email
	 * @param street2
	 * @return
	 * @throws UsatException
	 */

	public String determineDeliveryMethod(String pub, String street1, String street2, String city, String state, String zip)
			throws UsatException {

		// validate street1/street2
		if (street1 == null || street1.trim().length() == 0) {
			throw new UsatException("Invalid street1 Address.");
		}

		DeliveryMethodDAO dao = new DeliveryMethodDAO();

		if (street2 == null) {
			// invalid street2 so blank it out so we get the failed attempt logged on back end
			street2 = "          ";
		}

		// DeliveryMethodTO to = dao.determineDeliveryMethod(pub, street1, street2, city, state, zip);
		DeliveryMethodTO to = dao.determineGenesysDeliveryMethod(pub, street1, street2, city, state, zip);
		this.deliveryMethod = to.getDeliveryMethod();
		this.deliveryMethodDelay = to.getDeliveryDayDelay();

		return this.deliveryMethod;
	}

	/**
	 * @return Returns the deliveryMethod.
	 */
	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	/**
	 * @param deliveryMethod
	 *            The deliveryMethod to set.
	 */
	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	/**
	 * 
	 * @param email
	 * @param street2
	 * @return
	 * @throws UsatException
	 */

	public String determineDeliveryMethod(String pub, UIAddressIntf address) throws UsatException {

		String deliveryMethod = null;
		// validate street1/street2
		if (address.getAddress1() == null || address.getAddress1().trim().length() == 0) {
			throw new UsatException("Invalid Address 1 Address.");
		}

		DeliveryMethodDAO dao = new DeliveryMethodDAO();

		String street2 = "";
		if (address.getAddress2() != null) {
			street2 = address.getAddress2();
		}

		// DeliveryMethodTO to = dao.determineDeliveryMethod(pub, address.getAddress1(), street2, address.getCity(),
		// address.getState(), address.getZip());
		DeliveryMethodTO to = dao.determineGenesysDeliveryMethod(pub, address.getAddress1(), street2, address.getCity(),
				address.getState(), address.getZip());
		deliveryMethod = to.getDeliveryMethod();
		deliveryMethodDelay = to.getDeliveryDayDelay();

		return deliveryMethod;
	}

	public String getDeliveryDayDelay() {
		return this.deliveryMethodDelay;
	}

	public String getDeliveryMethodDisplayErrorString() {
		String displayString = DeliveryMethodCode1ValidationBO.DEFAULT_DISPLAY;

		if (this.deliveryMethod != null && this.deliveryMethod.trim().length() > 0) {
			if (this.deliveryMethod.equalsIgnoreCase("C")) {
				displayString = "Morning delivery via carrier";
			} else if (this.deliveryMethod.equalsIgnoreCase("M")) {
				if (this.deliveryMethodDelay != null) {
					try {
						int numDays = Integer.parseInt(this.deliveryMethodDelay);
						if (numDays == 0) {
							displayString = "<br><br>US Mail: Newspaper arrives same day it is published."
									+ "<br><b>We may be able to convert your address to carrier delivery!</b>"
									+ "<br><br>To inquire, please contact our National Customer Service Center at 1-877-713-6241;<br> a representative is available to assist you Monday - Friday from 8:00AM - 7:00PM, Eastern Time."
									+ "<br><br>If you would prefer morning delivery via a carrier, please consider entering an alternate address;<br> we have many subscribers that receive morning delivery at their place of business.<br>";

						} else if (numDays > 0) {
							displayString = "<br><br>US Mail: Newspaper arrives "
									+ numDays
									+ " day(s) after publication.<br>"
									+ "<br><b>We may be able to convert your address to carrier delivery!</b>"
									+ "<br><br>To inquire, please contact our National Customer Service Center at 1-877-713-6241;<br> a representative is available to assist you Monday - Friday from 8:00AM - 7:00PM, Eastern Time."
									+ "<br><br>If you would prefer morning delivery via a carrier, please consider entering an alternate address;<br> we have many subscribers that receive morning delivery at their place of business.<br>"

							;
						}
					} catch (Exception e) {
						displayString = "<br><br>US Mail Delivery"
								+

								"<br><b>We may be able to convert your address to carrier delivery!</b>"
								+ "<br><br>To inquire, please contact our National Customer Service Center at 1-877-713-6241;<br> a representative is available to assist you Monday - Friday from 8:00AM - 7:00PM, Eastern Time."
								+ "<br><br>If you would prefer morning delivery via a carrier, please consider entering an alternate address;<br> we have many subscribers that receive morning delivery at their place of business.<br>"

						;
					}
				} else {
					displayString = "<br><br>US Mail Delivery"
							+

							"<br><b>We may be able to convert your address to carrier delivery!</b>"
							+ "<br><br>To inquire, please contact our National Customer Service Center at 1-877-713-6241;<br> a representative is available to assist you Monday - Friday from 8:00AM - 7:00PM, Eastern Time."
							+ "<br><br>If you would prefer morning delivery via a carrier, please consider entering an alternate address;<br> we have many subscribers that receive morning delivery at their place of business.<br>"

					;
				}
			}
		}

		return displayString;
	}

	public String getDeliveryMethodDisplayString() {
		String displayString = DeliveryMethodCode1ValidationBO.DEFAULT_DISPLAY;

		if (this.deliveryMethod != null && this.deliveryMethod.trim().length() > 0) {
			if (this.deliveryMethod.equalsIgnoreCase("C")) {
				displayString = "Morning delivery via carrier";
			} else if (this.deliveryMethod.equalsIgnoreCase("M")) {
				if (this.deliveryMethodDelay != null) {
					try {
						int numDays = Integer.parseInt(this.deliveryMethodDelay);
						if (numDays == 0) {
							displayString = "US Mail: Newspaper arrives same day it is published";
						} else if (numDays > 0) {
							displayString = "US Mail: Newspaper arrives " + numDays + " day(s) after publication";
						}
					} catch (Exception e) {
						displayString = "US Mail Delivery";
					}
				} else {
					displayString = "US Mail Delivery";
				}
			}
		}

		return displayString;
	}
}
