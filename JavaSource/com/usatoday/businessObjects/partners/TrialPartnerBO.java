package com.usatoday.businessObjects.partners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.business.interfaces.partners.TrialPartnerTOIntf;
import com.usatoday.integration.TrialPartnerDAO;
import com.usatoday.integration.TrialPartnerTO;

public class TrialPartnerBO implements TrialPartnerIntf {

	private static HashMap<String, TrialPartnerIntf> partnerCache = new HashMap<String, TrialPartnerIntf>();

	private int id = -1;
	private boolean active = false;
	private String confirmationEmailTemplateID = null;
	private String customLandingPageURL = null;
	private String completePageCustomHTML = null;
	private String launchPageCustomHTML = null;
	private String trialOverCustomHTML = null;
	private String disclaimerCustomHTML = null;
	private String description = null;
	private int durationInDays = 0;
	private DateTime startDate = null;
	private DateTime endDate = null;
	private String pubCode = null;
	private String keyCode = null;
	private String partnerName = null;
	private String partnerID = null;
	private String apiSecretHashKey = "";

	private boolean showClubNumber = false;
	private String clubNumberLabel = null;

	private boolean sendConfirmationOnCreation = true; // default to true

	private String logoImageURL = null;
	private String logoLinkURL = null;

	private String authLinkURL = null;

	/**
	 * 
	 * @return A collection of all partner records active or inactive
	 * @throws UsatException
	 */
	public static Collection<TrialPartnerIntf> fetchAllPartners() throws UsatException {
		TrialPartnerDAO dao = new TrialPartnerDAO();

		ArrayList<TrialPartnerIntf> partners = new ArrayList<TrialPartnerIntf>();

		Collection<TrialPartnerTO> tos = dao.fetchTrialPartners();
		for (TrialPartnerTO pTo : tos) {
			TrialPartnerBO pBO = new TrialPartnerBO(pTo);
			partners.add(pBO);
		}

		return partners;
	}

	/**
	 * 
	 * @return A collection of all active partner records
	 * @throws UsatException
	 */
	public static Collection<TrialPartnerIntf> fetchAllActivePartners() throws UsatException {
		TrialPartnerDAO dao = new TrialPartnerDAO();

		ArrayList<TrialPartnerIntf> partners = new ArrayList<TrialPartnerIntf>();

		Collection<TrialPartnerTO> tos = dao.fetchActiveTrialPartners();
		for (TrialPartnerTO pTo : tos) {
			TrialPartnerBO pBO = new TrialPartnerBO(pTo);
			partners.add(pBO);
		}

		return partners;
	}

	/**
	 * Clients should use the version of this method that uses the cache flag for high performance applications
	 * 
	 * @param partnerID
	 * @return The partner with the given partner id (not primary key) or NULL (pull from db)
	 * @throws UsatException
	 */
	public static TrialPartnerIntf fetchPartner(String partnerID) throws UsatException {
		return TrialPartnerBO.fetchPartner(partnerID, false);
	}

	/**
	 * 
	 * @param partnerID
	 * @param useCache
	 *            - if set to true, check memory for partner otherwise go straight to db
	 * @return The partner with the given partner id (not primary key) or NULL (pull from cache then db)
	 * @throws UsatException
	 */
	public static TrialPartnerIntf fetchPartner(String partnerID, boolean useCache) throws UsatException {
		TrialPartnerIntf partner = null;

		try {
			if (useCache) {
				partner = TrialPartnerBO.partnerCache.get(partnerID);
			}
		} catch (Exception e) {
			; // ignore
		}

		if (partner == null) {
			TrialPartnerDAO dao = new TrialPartnerDAO();

			TrialPartnerTO pTO = dao.fetchTrialPartnerByPartnerID(partnerID);

			if (pTO != null) {
				partner = new TrialPartnerBO(pTO);
			}

			if (partner != null) {
				// update cache if partner not in there
				if (!TrialPartnerBO.partnerCache.containsKey(partnerID)) {
					TrialPartnerBO.partnerCache.put(partner.getPartnerID(), partner);
				}
			}
		}

		return partner;
	}

	/**
	 * 
	 * @return The collection of cached partners
	 * @throws UsatException
	 */
	public static Collection<TrialPartnerIntf> getPartnerCache() throws UsatException {
		return TrialPartnerBO.partnerCache.values();
	}

	/**
	 * Reloads the cache of partners
	 */
	public static final void reloadPartnerCache() {
		TrialPartnerBO.partnerCache.clear();

		try {
			Collection<TrialPartnerIntf> tos = TrialPartnerBO.fetchAllActivePartners();

			for (TrialPartnerIntf pBO : tos) {
				TrialPartnerBO.partnerCache.put(pBO.getPartnerID(), pBO);
			}
		} catch (Exception e) {
			System.out.println("Failed to clear partner cache: TrialPartnerBO::reloadCachedPartners()" + e.getMessage());
			e.printStackTrace();
		}
	}

	private TrialPartnerBO(TrialPartnerTOIntf source) {
		if (source != null) {
			this.id = source.getID();
			this.active = source.getIsActive();
			this.confirmationEmailTemplateID = source.getConfirmationEmailTemplateID();
			this.customLandingPageURL = source.getCustomLandingPageURL();
			this.description = source.getDescription();
			this.durationInDays = source.getDurationInDays();
			this.startDate = source.getStartDate();
			this.endDate = source.getEndDate();
			this.pubCode = source.getPubCode();
			this.keyCode = source.getKeyCode();
			this.partnerName = source.getPartnerName();
			this.partnerID = source.getPartnerID();
			this.completePageCustomHTML = source.getCompletePageCustomHTML();
			this.disclaimerCustomHTML = source.getDisclaimerCustomHTML();
			this.launchPageCustomHTML = source.getLaunchPageCustomHTML();
			this.trialOverCustomHTML = source.getTrialOverCustomHTML();
			this.clubNumberLabel = source.getClubNumberLabel();
			this.showClubNumber = source.getIsShowClubNumber();
			this.logoImageURL = source.getLogoImageURL();
			this.logoLinkURL = source.getLogoLinkURL();
			this.authLinkURL = source.getAuthLinkURL();
			this.apiSecretHashKey = source.getAPISecretHashKey();
			this.sendConfirmationOnCreation = source.getIsSendConfirmationOnCreation();
		}
	}

	public boolean getActive() {
		return this.active;
	}

	public String getConfirmationEmailTemplateID() {
		return this.confirmationEmailTemplateID;
	}

	public String getCustomLandingPageURL() {
		return this.customLandingPageURL;
	}

	public String getDescription() {
		return this.description;
	}

	public int getDurationInDays() {
		return this.durationInDays;
	}

	public DateTime getEndDate() {
		return this.endDate;
	}

	public int getID() {
		return this.id;
	}

	public boolean getIsOfferCurrentlyRunning() {
		DateTime today = new DateTime();

		boolean programRunning = false;
		if (this.startDate != null) {
			if (today.isAfter(this.startDate)) {
				// if it's after the start date
				if (this.endDate == null) {
					// no end date
					programRunning = true;
				} else {
					if (today.isBefore(this.endDate)) {
						programRunning = true;
					}
				}
			}
		}
		return programRunning;
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public String getPartnerName() {
		return this.partnerName;
	}

	public String getPubCode() {

		return this.pubCode;
	}

	public DateTime getStartDate() {
		return this.startDate;
	}

	public String getPartnerID() {
		return this.partnerID;
	}

	public String getCompletePageCustomHTML() {
		return this.completePageCustomHTML;
	}

	public String getDisclaimerCustomHTML() {
		return this.disclaimerCustomHTML;
	}

	public String getLaunchPageCustomHTML() {
		return this.launchPageCustomHTML;
	}

	public String getTrialOverCustomHTML() {
		return this.trialOverCustomHTML;
	}

	public String getClubNumberLabel() {
		if (this.clubNumberLabel != null) {
			return this.clubNumberLabel;
		} else {
			return "";
		}
	}

	public boolean getIsShowClubNumber() {
		return this.showClubNumber;
	}

	public String getLogoImageURL() {
		return this.logoImageURL;
	}

	public String getLogoLinkURL() {
		return this.logoLinkURL;
	}

	public String getAuthLinkURL() {
		return this.authLinkURL;
	}

	public String getAPISecretHashKey() {
		return this.apiSecretHashKey;
	}

	public boolean getIsSendConfirmationOnCreation() {
		return this.sendConfirmationOnCreation;
	}

}
