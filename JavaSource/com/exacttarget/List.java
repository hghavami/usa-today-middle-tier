/**
 * List.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class List extends com.exacttarget.APIObject {
	private java.lang.String listName;
	private java.lang.Integer category;
	private com.exacttarget.ListTypeEnum type;
	private java.lang.String description;
	private com.exacttarget.Subscriber[] subscribers;

	public List() {
	}

	public java.lang.String getListName() {
		return listName;
	}

	public void setListName(java.lang.String listName) {
		this.listName = listName;
	}

	public java.lang.Integer getCategory() {
		return category;
	}

	public void setCategory(java.lang.Integer category) {
		this.category = category;
	}

	public com.exacttarget.ListTypeEnum getType() {
		return type;
	}

	public void setType(com.exacttarget.ListTypeEnum type) {
		this.type = type;
	}

	public java.lang.String getDescription() {
		return description;
	}

	public void setDescription(java.lang.String description) {
		this.description = description;
	}

	public com.exacttarget.Subscriber[] getSubscribers() {
		return subscribers;
	}

	public void setSubscribers(com.exacttarget.Subscriber[] subscribers) {
		this.subscribers = subscribers;
	}

	public com.exacttarget.Subscriber getSubscribers(int i) {
		return this.subscribers[i];
	}

	public void setSubscribers(int i, com.exacttarget.Subscriber value) {
		this.subscribers[i] = value;
	}

}
