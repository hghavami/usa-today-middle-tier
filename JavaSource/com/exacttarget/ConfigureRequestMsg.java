/**
 * ConfigureRequestMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ConfigureRequestMsg {
	private com.exacttarget.ConfigureOptions options;
	private java.lang.String action;
	private com.exacttarget.APIObject[] configurations;

	public ConfigureRequestMsg() {
	}

	public com.exacttarget.ConfigureOptions getOptions() {
		return options;
	}

	public void setOptions(com.exacttarget.ConfigureOptions options) {
		this.options = options;
	}

	public java.lang.String getAction() {
		return action;
	}

	public void setAction(java.lang.String action) {
		this.action = action;
	}

	public com.exacttarget.APIObject[] getConfigurations() {
		return configurations;
	}

	public void setConfigurations(com.exacttarget.APIObject[] configurations) {
		this.configurations = configurations;
	}

}
