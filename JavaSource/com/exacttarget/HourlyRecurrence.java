/**
 * HourlyRecurrence.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class HourlyRecurrence extends com.exacttarget.Recurrence {
	private com.exacttarget.HourlyRecurrencePatternTypeEnum hourlyRecurrencePatternType;
	private java.lang.Integer hourInterval;

	public HourlyRecurrence() {
	}

	public com.exacttarget.HourlyRecurrencePatternTypeEnum getHourlyRecurrencePatternType() {
		return hourlyRecurrencePatternType;
	}

	public void setHourlyRecurrencePatternType(com.exacttarget.HourlyRecurrencePatternTypeEnum hourlyRecurrencePatternType) {
		this.hourlyRecurrencePatternType = hourlyRecurrencePatternType;
	}

	public java.lang.Integer getHourInterval() {
		return hourInterval;
	}

	public void setHourInterval(java.lang.Integer hourInterval) {
		this.hourInterval = hourInterval;
	}

}
