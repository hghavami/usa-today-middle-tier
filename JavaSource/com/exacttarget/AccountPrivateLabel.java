/**
 * AccountPrivateLabel.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class AccountPrivateLabel extends com.exacttarget.APIObject {
	private java.lang.String name;
	private int ownerMemberID;
	private java.lang.String colorPaletteXML;

	public AccountPrivateLabel() {
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public int getOwnerMemberID() {
		return ownerMemberID;
	}

	public void setOwnerMemberID(int ownerMemberID) {
		this.ownerMemberID = ownerMemberID;
	}

	public java.lang.String getColorPaletteXML() {
		return colorPaletteXML;
	}

	public void setColorPaletteXML(java.lang.String colorPaletteXML) {
		this.colorPaletteXML = colorPaletteXML;
	}

}
