/**
 * DataExtensionError.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class DataExtensionError {
	private java.lang.String name;
	private java.math.BigInteger errorCode;
	private java.lang.String errorMessage;

	public DataExtensionError() {
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.math.BigInteger getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(java.math.BigInteger errorCode) {
		this.errorCode = errorCode;
	}

	public java.lang.String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(java.lang.String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
