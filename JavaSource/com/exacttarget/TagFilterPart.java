/**
 * TagFilterPart.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class TagFilterPart extends com.exacttarget.FilterPart {
	private java.lang.String[] tags;

	public TagFilterPart() {
	}

	public java.lang.String[] getTags() {
		return tags;
	}

	public void setTags(java.lang.String[] tags) {
		this.tags = tags;
	}

}
