/**
 * ImportDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ImportDefinition extends com.exacttarget.InteractionDefinition {
	private java.lang.Boolean allowErrors;
	private com.exacttarget.APIObject destinationObject;
	private com.exacttarget.ImportDefinitionFieldMappingType fieldMappingType;
	private javax.xml.soap.SOAPElement[] fieldMaps;
	private java.lang.String fileSpec;
	private com.exacttarget.FileType fileType;
	private com.exacttarget.AsyncResponse notification;
	private com.exacttarget.FileTransferLocation retrieveFileTransferLocation;
	private com.exacttarget.ImportDefinitionSubscriberImportType subscriberImportType;
	private com.exacttarget.ImportDefinitionUpdateType updateType;
	private java.lang.Integer maxFileAge;
	private java.lang.Integer maxFileAgeScheduleOffset;
	private java.lang.Integer maxImportFrequency;
	private java.lang.String delimiter;
	private java.lang.Integer headerLines;

	public ImportDefinition() {
	}

	public java.lang.Boolean getAllowErrors() {
		return allowErrors;
	}

	public void setAllowErrors(java.lang.Boolean allowErrors) {
		this.allowErrors = allowErrors;
	}

	public com.exacttarget.APIObject getDestinationObject() {
		return destinationObject;
	}

	public void setDestinationObject(com.exacttarget.APIObject destinationObject) {
		this.destinationObject = destinationObject;
	}

	public com.exacttarget.ImportDefinitionFieldMappingType getFieldMappingType() {
		return fieldMappingType;
	}

	public void setFieldMappingType(com.exacttarget.ImportDefinitionFieldMappingType fieldMappingType) {
		this.fieldMappingType = fieldMappingType;
	}

	public javax.xml.soap.SOAPElement[] getFieldMaps() {
		return fieldMaps;
	}

	public void setFieldMaps(javax.xml.soap.SOAPElement[] fieldMaps) {
		this.fieldMaps = fieldMaps;
	}

	public java.lang.String getFileSpec() {
		return fileSpec;
	}

	public void setFileSpec(java.lang.String fileSpec) {
		this.fileSpec = fileSpec;
	}

	public com.exacttarget.FileType getFileType() {
		return fileType;
	}

	public void setFileType(com.exacttarget.FileType fileType) {
		this.fileType = fileType;
	}

	public com.exacttarget.AsyncResponse getNotification() {
		return notification;
	}

	public void setNotification(com.exacttarget.AsyncResponse notification) {
		this.notification = notification;
	}

	public com.exacttarget.FileTransferLocation getRetrieveFileTransferLocation() {
		return retrieveFileTransferLocation;
	}

	public void setRetrieveFileTransferLocation(com.exacttarget.FileTransferLocation retrieveFileTransferLocation) {
		this.retrieveFileTransferLocation = retrieveFileTransferLocation;
	}

	public com.exacttarget.ImportDefinitionSubscriberImportType getSubscriberImportType() {
		return subscriberImportType;
	}

	public void setSubscriberImportType(com.exacttarget.ImportDefinitionSubscriberImportType subscriberImportType) {
		this.subscriberImportType = subscriberImportType;
	}

	public com.exacttarget.ImportDefinitionUpdateType getUpdateType() {
		return updateType;
	}

	public void setUpdateType(com.exacttarget.ImportDefinitionUpdateType updateType) {
		this.updateType = updateType;
	}

	public java.lang.Integer getMaxFileAge() {
		return maxFileAge;
	}

	public void setMaxFileAge(java.lang.Integer maxFileAge) {
		this.maxFileAge = maxFileAge;
	}

	public java.lang.Integer getMaxFileAgeScheduleOffset() {
		return maxFileAgeScheduleOffset;
	}

	public void setMaxFileAgeScheduleOffset(java.lang.Integer maxFileAgeScheduleOffset) {
		this.maxFileAgeScheduleOffset = maxFileAgeScheduleOffset;
	}

	public java.lang.Integer getMaxImportFrequency() {
		return maxImportFrequency;
	}

	public void setMaxImportFrequency(java.lang.Integer maxImportFrequency) {
		this.maxImportFrequency = maxImportFrequency;
	}

	public java.lang.String getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(java.lang.String delimiter) {
		this.delimiter = delimiter;
	}

	public java.lang.Integer getHeaderLines() {
		return headerLines;
	}

	public void setHeaderLines(java.lang.Integer headerLines) {
		this.headerLines = headerLines;
	}

}
