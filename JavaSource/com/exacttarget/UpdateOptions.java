/**
 * UpdateOptions.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class UpdateOptions extends com.exacttarget.Options {
	private com.exacttarget.ContainerID container;
	private java.lang.String action;

	public UpdateOptions() {
	}

	public com.exacttarget.ContainerID getContainer() {
		return container;
	}

	public void setContainer(com.exacttarget.ContainerID container) {
		this.container = container;
	}

	public java.lang.String getAction() {
		return action;
	}

	public void setAction(java.lang.String action) {
		this.action = action;
	}

}
