/**
 * ExtractDescription.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ExtractDescription extends com.exacttarget.ExtractTemplate {
	private com.exacttarget.ExtractParameterDescription[] parameters;

	public ExtractDescription() {
	}

	public com.exacttarget.ExtractParameterDescription[] getParameters() {
		return parameters;
	}

	public void setParameters(com.exacttarget.ExtractParameterDescription[] parameters) {
		this.parameters = parameters;
	}

}
