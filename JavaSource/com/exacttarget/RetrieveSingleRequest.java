/**
 * RetrieveSingleRequest.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class RetrieveSingleRequest extends com.exacttarget.Request {
	private com.exacttarget.APIObject requestedObject;
	private com.exacttarget.Options retrieveOption;

	public RetrieveSingleRequest() {
	}

	public com.exacttarget.APIObject getRequestedObject() {
		return requestedObject;
	}

	public void setRequestedObject(com.exacttarget.APIObject requestedObject) {
		this.requestedObject = requestedObject;
	}

	public com.exacttarget.Options getRetrieveOption() {
		return retrieveOption;
	}

	public void setRetrieveOption(com.exacttarget.Options retrieveOption) {
		this.retrieveOption = retrieveOption;
	}

}
