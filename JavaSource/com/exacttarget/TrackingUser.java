/**
 * TrackingUser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class TrackingUser extends com.exacttarget.APIObject {
	private java.lang.Boolean isActive;
	private java.lang.Integer employeeID;

	public TrackingUser() {
	}

	public java.lang.Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(java.lang.Boolean isActive) {
		this.isActive = isActive;
	}

	public java.lang.Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(java.lang.Integer employeeID) {
		this.employeeID = employeeID;
	}

}
