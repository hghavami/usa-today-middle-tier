/**
 * ListSubscriber.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ListSubscriber extends com.exacttarget.APIObject {
	private com.exacttarget.SubscriberStatus status;
	private java.lang.Integer listID;
	private java.lang.String subscriberKey;

	public ListSubscriber() {
	}

	public com.exacttarget.SubscriberStatus getStatus() {
		return status;
	}

	public void setStatus(com.exacttarget.SubscriberStatus status) {
		this.status = status;
	}

	public java.lang.Integer getListID() {
		return listID;
	}

	public void setListID(java.lang.Integer listID) {
		this.listID = listID;
	}

	public java.lang.String getSubscriberKey() {
		return subscriberKey;
	}

	public void setSubscriberKey(java.lang.String subscriberKey) {
		this.subscriberKey = subscriberKey;
	}

}
