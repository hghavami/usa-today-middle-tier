/**
 * DataExtensionFieldType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class DataExtensionFieldType {
	private java.lang.String _value_;
	private static java.util.HashMap _table_ = new java.util.HashMap();

	// Constructor
	protected DataExtensionFieldType(java.lang.String value) {
		_value_ = value;
		_table_.put(_value_, this);
	};

	public static final java.lang.String _Text = "Text";
	public static final java.lang.String _Number = "Number";
	public static final java.lang.String _Date = "Date";
	public static final java.lang.String _Boolean = "Boolean";
	public static final java.lang.String _EmailAddress = "EmailAddress";
	public static final java.lang.String _Phone = "Phone";
	public static final DataExtensionFieldType Text = new DataExtensionFieldType(_Text);
	public static final DataExtensionFieldType Number = new DataExtensionFieldType(_Number);
	public static final DataExtensionFieldType Date = new DataExtensionFieldType(_Date);
	public static final DataExtensionFieldType Boolean = new DataExtensionFieldType(_Boolean);
	public static final DataExtensionFieldType EmailAddress = new DataExtensionFieldType(_EmailAddress);
	public static final DataExtensionFieldType Phone = new DataExtensionFieldType(_Phone);

	public java.lang.String getValue() {
		return _value_;
	}

	public static DataExtensionFieldType fromValue(java.lang.String value) throws java.lang.IllegalArgumentException {
		DataExtensionFieldType enumeration = (DataExtensionFieldType) _table_.get(value);
		if (enumeration == null)
			throw new java.lang.IllegalArgumentException();
		return enumeration;
	}

	public static DataExtensionFieldType fromString(java.lang.String value) throws java.lang.IllegalArgumentException {
		return fromValue(value);
	}

	public boolean equals(java.lang.Object obj) {
		return (obj == this);
	}

	public int hashCode() {
		return toString().hashCode();
	}

	public java.lang.String toString() {
		return _value_;
	}

}
