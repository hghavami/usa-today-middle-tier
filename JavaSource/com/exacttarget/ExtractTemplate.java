/**
 * ExtractTemplate.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ExtractTemplate extends com.exacttarget.APIObject {
	private java.lang.String name;
	private java.lang.String configurationPage;

	public ExtractTemplate() {
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getConfigurationPage() {
		return configurationPage;
	}

	public void setConfigurationPage(java.lang.String configurationPage) {
		this.configurationPage = configurationPage;
	}

}
