/**
 * RetrieveSingleOptions.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class RetrieveSingleOptions extends com.exacttarget.Options {
	private com.exacttarget.APIProperty[] parameters;

	public RetrieveSingleOptions() {
	}

	public com.exacttarget.APIProperty[] getParameters() {
		return parameters;
	}

	public void setParameters(com.exacttarget.APIProperty[] parameters) {
		this.parameters = parameters;
	}

}
