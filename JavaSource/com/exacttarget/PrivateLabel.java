/**
 * PrivateLabel.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class PrivateLabel {
	private java.lang.Integer ID;
	private java.lang.String name;
	private java.lang.String colorPaletteXML;
	private java.lang.String logoFile;
	private int delete;
	private java.lang.Boolean setActive;

	public PrivateLabel() {
	}

	public java.lang.Integer getID() {
		return ID;
	}

	public void setID(java.lang.Integer ID) {
		this.ID = ID;
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getColorPaletteXML() {
		return colorPaletteXML;
	}

	public void setColorPaletteXML(java.lang.String colorPaletteXML) {
		this.colorPaletteXML = colorPaletteXML;
	}

	public java.lang.String getLogoFile() {
		return logoFile;
	}

	public void setLogoFile(java.lang.String logoFile) {
		this.logoFile = logoFile;
	}

	public int getDelete() {
		return delete;
	}

	public void setDelete(int delete) {
		this.delete = delete;
	}

	public java.lang.Boolean getSetActive() {
		return setActive;
	}

	public void setSetActive(java.lang.Boolean setActive) {
		this.setActive = setActive;
	}

}
