/**
 * ValidationAction.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ValidationAction {
	private java.lang.String validationType;
	private com.exacttarget.APIProperty[] validationOptions;

	public ValidationAction() {
	}

	public java.lang.String getValidationType() {
		return validationType;
	}

	public void setValidationType(java.lang.String validationType) {
		this.validationType = validationType;
	}

	public com.exacttarget.APIProperty[] getValidationOptions() {
		return validationOptions;
	}

	public void setValidationOptions(com.exacttarget.APIProperty[] validationOptions) {
		this.validationOptions = validationOptions;
	}

}
