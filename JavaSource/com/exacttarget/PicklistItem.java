/**
 * PicklistItem.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class PicklistItem {
	private java.lang.Boolean isDefaultValue;
	private java.lang.String label;
	private java.lang.String value;

	public PicklistItem() {
	}

	public java.lang.Boolean getIsDefaultValue() {
		return isDefaultValue;
	}

	public void setIsDefaultValue(java.lang.Boolean isDefaultValue) {
		this.isDefaultValue = isDefaultValue;
	}

	public java.lang.String getLabel() {
		return label;
	}

	public void setLabel(java.lang.String label) {
		this.label = label;
	}

	public java.lang.String getValue() {
		return value;
	}

	public void setValue(java.lang.String value) {
		this.value = value;
	}

}
