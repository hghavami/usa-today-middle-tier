/**
 * ForwardedEmailOptInEvent.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ForwardedEmailOptInEvent extends com.exacttarget.TrackingEvent {
	private java.lang.String optInSubscriberKey;

	public ForwardedEmailOptInEvent() {
	}

	public java.lang.String getOptInSubscriberKey() {
		return optInSubscriberKey;
	}

	public void setOptInSubscriberKey(java.lang.String optInSubscriberKey) {
		this.optInSubscriberKey = optInSubscriberKey;
	}

}
