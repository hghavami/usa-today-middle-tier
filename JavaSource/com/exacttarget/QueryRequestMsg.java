/**
 * QueryRequestMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class QueryRequestMsg {
	private com.exacttarget.QueryRequest queryRequest;

	public QueryRequestMsg() {
	}

	public com.exacttarget.QueryRequest getQueryRequest() {
		return queryRequest;
	}

	public void setQueryRequest(com.exacttarget.QueryRequest queryRequest) {
		this.queryRequest = queryRequest;
	}

}
