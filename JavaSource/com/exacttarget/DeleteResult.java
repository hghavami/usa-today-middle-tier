/**
 * DeleteResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class DeleteResult extends com.exacttarget.Result {
	private com.exacttarget.APIObject object;

	public DeleteResult() {
	}

	public com.exacttarget.APIObject getObject() {
		return object;
	}

	public void setObject(com.exacttarget.APIObject object) {
		this.object = object;
	}

}
