/**
 * ContentValidation.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ContentValidation extends com.exacttarget.APIObject {
	private com.exacttarget.ValidationAction validationAction;
	private com.exacttarget.Email email;
	private com.exacttarget.Subscriber[] subscribers;

	public ContentValidation() {
	}

	public com.exacttarget.ValidationAction getValidationAction() {
		return validationAction;
	}

	public void setValidationAction(com.exacttarget.ValidationAction validationAction) {
		this.validationAction = validationAction;
	}

	public com.exacttarget.Email getEmail() {
		return email;
	}

	public void setEmail(com.exacttarget.Email email) {
		this.email = email;
	}

	public com.exacttarget.Subscriber[] getSubscribers() {
		return subscribers;
	}

	public void setSubscribers(com.exacttarget.Subscriber[] subscribers) {
		this.subscribers = subscribers;
	}

}
