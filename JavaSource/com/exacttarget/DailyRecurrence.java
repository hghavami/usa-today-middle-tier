/**
 * DailyRecurrence.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class DailyRecurrence extends com.exacttarget.Recurrence {
	private com.exacttarget.DailyRecurrencePatternTypeEnum dailyRecurrencePatternType;
	private java.lang.Integer dayInterval;

	public DailyRecurrence() {
	}

	public com.exacttarget.DailyRecurrencePatternTypeEnum getDailyRecurrencePatternType() {
		return dailyRecurrencePatternType;
	}

	public void setDailyRecurrencePatternType(com.exacttarget.DailyRecurrencePatternTypeEnum dailyRecurrencePatternType) {
		this.dailyRecurrencePatternType = dailyRecurrencePatternType;
	}

	public java.lang.Integer getDayInterval() {
		return dayInterval;
	}

	public void setDayInterval(java.lang.Integer dayInterval) {
		this.dayInterval = dayInterval;
	}

}
