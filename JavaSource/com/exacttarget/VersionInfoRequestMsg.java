/**
 * VersionInfoRequestMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class VersionInfoRequestMsg {
	private boolean includeVersionHistory;

	public VersionInfoRequestMsg() {
	}

	public boolean isIncludeVersionHistory() {
		return includeVersionHistory;
	}

	public void setIncludeVersionHistory(boolean includeVersionHistory) {
		this.includeVersionHistory = includeVersionHistory;
	}

}
