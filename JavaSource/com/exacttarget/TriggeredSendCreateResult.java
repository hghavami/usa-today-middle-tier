/**
 * TriggeredSendCreateResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class TriggeredSendCreateResult extends com.exacttarget.CreateResult {
	private com.exacttarget.SubscriberResult[] subscriberFailures;

	public TriggeredSendCreateResult() {
	}

	public com.exacttarget.SubscriberResult[] getSubscriberFailures() {
		return subscriberFailures;
	}

	public void setSubscriberFailures(com.exacttarget.SubscriberResult[] subscriberFailures) {
		this.subscriberFailures = subscriberFailures;
	}

	public com.exacttarget.SubscriberResult getSubscriberFailures(int i) {
		return this.subscriberFailures[i];
	}

	public void setSubscriberFailures(int i, com.exacttarget.SubscriberResult value) {
		this.subscriberFailures[i] = value;
	}

}
