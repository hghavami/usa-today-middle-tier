/**
 * DefinitionRequestMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class DefinitionRequestMsg {
	private com.exacttarget.ObjectDefinitionRequest[] describeRequests;

	public DefinitionRequestMsg() {
	}

	public com.exacttarget.ObjectDefinitionRequest[] getDescribeRequests() {
		return describeRequests;
	}

	public void setDescribeRequests(com.exacttarget.ObjectDefinitionRequest[] describeRequests) {
		this.describeRequests = describeRequests;
	}

}
