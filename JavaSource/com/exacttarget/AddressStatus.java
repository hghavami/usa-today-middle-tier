/**
 * AddressStatus.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class AddressStatus {
	private com.exacttarget.SubscriberAddressStatus status;

	public AddressStatus() {
	}

	public com.exacttarget.SubscriberAddressStatus getStatus() {
		return status;
	}

	public void setStatus(com.exacttarget.SubscriberAddressStatus status) {
		this.status = status;
	}

}
