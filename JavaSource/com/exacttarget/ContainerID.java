/**
 * ContainerID.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ContainerID {
	private com.exacttarget.APIObject APIObject;

	public ContainerID() {
	}

	public com.exacttarget.APIObject getAPIObject() {
		return APIObject;
	}

	public void setAPIObject(com.exacttarget.APIObject APIObject) {
		this.APIObject = APIObject;
	}

}
