/**
 * Owner.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class Owner {
	private com.exacttarget.ClientID client;
	private java.lang.String fromName;
	private java.lang.String fromAddress;

	public Owner() {
	}

	public com.exacttarget.ClientID getClient() {
		return client;
	}

	public void setClient(com.exacttarget.ClientID client) {
		this.client = client;
	}

	public java.lang.String getFromName() {
		return fromName;
	}

	public void setFromName(java.lang.String fromName) {
		this.fromName = fromName;
	}

	public java.lang.String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(java.lang.String fromAddress) {
		this.fromAddress = fromAddress;
	}

}
