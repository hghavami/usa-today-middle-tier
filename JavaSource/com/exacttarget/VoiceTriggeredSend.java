/**
 * VoiceTriggeredSend.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class VoiceTriggeredSend extends com.exacttarget.APIObject {
	private com.exacttarget.VoiceTriggeredSendDefinition voiceTriggeredSendDefinition;
	private com.exacttarget.Subscriber subscriber;
	private java.lang.String message;
	private java.lang.String number;

	public VoiceTriggeredSend() {
	}

	public com.exacttarget.VoiceTriggeredSendDefinition getVoiceTriggeredSendDefinition() {
		return voiceTriggeredSendDefinition;
	}

	public void setVoiceTriggeredSendDefinition(com.exacttarget.VoiceTriggeredSendDefinition voiceTriggeredSendDefinition) {
		this.voiceTriggeredSendDefinition = voiceTriggeredSendDefinition;
	}

	public com.exacttarget.Subscriber getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(com.exacttarget.Subscriber subscriber) {
		this.subscriber = subscriber;
	}

	public java.lang.String getMessage() {
		return message;
	}

	public void setMessage(java.lang.String message) {
		this.message = message;
	}

	public java.lang.String getNumber() {
		return number;
	}

	public void setNumber(java.lang.String number) {
		this.number = number;
	}

}
