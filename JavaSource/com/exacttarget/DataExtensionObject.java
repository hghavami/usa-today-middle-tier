/**
 * DataExtensionObject.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class DataExtensionObject extends com.exacttarget.ObjectExtension {
	private java.lang.String name;
	private com.exacttarget.APIProperty[] keys;

	public DataExtensionObject() {
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public com.exacttarget.APIProperty[] getKeys() {
		return keys;
	}

	public void setKeys(com.exacttarget.APIProperty[] keys) {
		this.keys = keys;
	}

}
