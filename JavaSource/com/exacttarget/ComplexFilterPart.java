/**
 * ComplexFilterPart.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ComplexFilterPart extends com.exacttarget.FilterPart {
	private com.exacttarget.FilterPart leftOperand;
	private com.exacttarget.LogicalOperators logicalOperator;
	private com.exacttarget.FilterPart rightOperand;

	public ComplexFilterPart() {
	}

	public com.exacttarget.FilterPart getLeftOperand() {
		return leftOperand;
	}

	public void setLeftOperand(com.exacttarget.FilterPart leftOperand) {
		this.leftOperand = leftOperand;
	}

	public com.exacttarget.LogicalOperators getLogicalOperator() {
		return logicalOperator;
	}

	public void setLogicalOperator(com.exacttarget.LogicalOperators logicalOperator) {
		this.logicalOperator = logicalOperator;
	}

	public com.exacttarget.FilterPart getRightOperand() {
		return rightOperand;
	}

	public void setRightOperand(com.exacttarget.FilterPart rightOperand) {
		this.rightOperand = rightOperand;
	}

}
