/**
 * ExtractParameterDescription.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ExtractParameterDescription extends com.exacttarget.ParameterDescription {
	private java.lang.String name;
	private com.exacttarget.ExtractParameterDataType dataType;
	private java.lang.String defaultValue;
	private boolean isOptional;

	public ExtractParameterDescription() {
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public com.exacttarget.ExtractParameterDataType getDataType() {
		return dataType;
	}

	public void setDataType(com.exacttarget.ExtractParameterDataType dataType) {
		this.dataType = dataType;
	}

	public java.lang.String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(java.lang.String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public boolean isIsOptional() {
		return isOptional;
	}

	public void setIsOptional(boolean isOptional) {
		this.isOptional = isOptional;
	}

}
