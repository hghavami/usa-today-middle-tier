/**
 * IntegrationProfileDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class IntegrationProfileDefinition extends com.exacttarget.APIObject {
	private java.lang.String profileID;
	private java.lang.String name;
	private java.lang.String description;
	private int externalSystemType;

	public IntegrationProfileDefinition() {
	}

	public java.lang.String getProfileID() {
		return profileID;
	}

	public void setProfileID(java.lang.String profileID) {
		this.profileID = profileID;
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getDescription() {
		return description;
	}

	public void setDescription(java.lang.String description) {
		this.description = description;
	}

	public int getExternalSystemType() {
		return externalSystemType;
	}

	public void setExternalSystemType(int externalSystemType) {
		this.externalSystemType = externalSystemType;
	}

}
