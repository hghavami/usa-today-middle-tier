/**
 * ExtractResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ExtractResult extends com.exacttarget.Result {
	private javax.xml.soap.SOAPElement request;

	public ExtractResult() {
	}

	public javax.xml.soap.SOAPElement getRequest() {
		return request;
	}

	public void setRequest(javax.xml.soap.SOAPElement request) {
		this.request = request;
	}

}
