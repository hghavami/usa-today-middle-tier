/**
 * SMSTriggeredSend.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class SMSTriggeredSend extends com.exacttarget.APIObject {
	private com.exacttarget.SMSTriggeredSendDefinition SMSTriggeredSendDefinition;
	private com.exacttarget.Subscriber subscriber;
	private java.lang.String message;
	private java.lang.String number;

	public SMSTriggeredSend() {
	}

	public com.exacttarget.SMSTriggeredSendDefinition getSMSTriggeredSendDefinition() {
		return SMSTriggeredSendDefinition;
	}

	public void setSMSTriggeredSendDefinition(com.exacttarget.SMSTriggeredSendDefinition SMSTriggeredSendDefinition) {
		this.SMSTriggeredSendDefinition = SMSTriggeredSendDefinition;
	}

	public com.exacttarget.Subscriber getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(com.exacttarget.Subscriber subscriber) {
		this.subscriber = subscriber;
	}

	public java.lang.String getMessage() {
		return message;
	}

	public void setMessage(java.lang.String message) {
		this.message = message;
	}

	public java.lang.String getNumber() {
		return number;
	}

	public void setNumber(java.lang.String number) {
		this.number = number;
	}

}
