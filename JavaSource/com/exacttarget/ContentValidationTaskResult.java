/**
 * ContentValidationTaskResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ContentValidationTaskResult extends com.exacttarget.TaskResult {
	private com.exacttarget.ValidationResult[] validationResults;

	public ContentValidationTaskResult() {
	}

	public com.exacttarget.ValidationResult[] getValidationResults() {
		return validationResults;
	}

	public void setValidationResults(com.exacttarget.ValidationResult[] validationResults) {
		this.validationResults = validationResults;
	}

}
