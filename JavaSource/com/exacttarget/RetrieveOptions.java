/**
 * RetrieveOptions.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class RetrieveOptions extends com.exacttarget.Options {
	private java.lang.Integer batchSize;

	public RetrieveOptions() {
	}

	public java.lang.Integer getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(java.lang.Integer batchSize) {
		this.batchSize = batchSize;
	}

}
