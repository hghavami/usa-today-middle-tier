/**
 * Message.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class Message extends com.exacttarget.APIObject {
	private java.lang.String textBody;

	public Message() {
	}

	public java.lang.String getTextBody() {
		return textBody;
	}

	public void setTextBody(java.lang.String textBody) {
		this.textBody = textBody;
	}

}
