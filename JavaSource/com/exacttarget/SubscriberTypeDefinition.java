/**
 * SubscriberTypeDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class SubscriberTypeDefinition {
	private java.lang.String subscriberType;

	public SubscriberTypeDefinition() {
	}

	public java.lang.String getSubscriberType() {
		return subscriberType;
	}

	public void setSubscriberType(java.lang.String subscriberType) {
		this.subscriberType = subscriberType;
	}

}
