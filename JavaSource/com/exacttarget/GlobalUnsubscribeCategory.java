/**
 * GlobalUnsubscribeCategory.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class GlobalUnsubscribeCategory extends com.exacttarget.APIObject {
	private java.lang.String name;
	private boolean ignorableByPartners;
	private boolean ignore;

	public GlobalUnsubscribeCategory() {
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public boolean isIgnorableByPartners() {
		return ignorableByPartners;
	}

	public void setIgnorableByPartners(boolean ignorableByPartners) {
		this.ignorableByPartners = ignorableByPartners;
	}

	public boolean isIgnore() {
		return ignore;
	}

	public void setIgnore(boolean ignore) {
		this.ignore = ignore;
	}

}
