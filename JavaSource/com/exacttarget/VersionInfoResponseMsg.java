/**
 * VersionInfoResponseMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class VersionInfoResponseMsg {
	private com.exacttarget.VersionInfoResponse versionInfo;
	private java.lang.String requestID;

	public VersionInfoResponseMsg() {
	}

	public com.exacttarget.VersionInfoResponse getVersionInfo() {
		return versionInfo;
	}

	public void setVersionInfo(com.exacttarget.VersionInfoResponse versionInfo) {
		this.versionInfo = versionInfo;
	}

	public java.lang.String getRequestID() {
		return requestID;
	}

	public void setRequestID(java.lang.String requestID) {
		this.requestID = requestID;
	}

}
