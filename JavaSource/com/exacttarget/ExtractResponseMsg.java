/**
 * ExtractResponseMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf190834.07 v9308141138
 */

package com.exacttarget;

public class ExtractResponseMsg {
	private java.lang.String overallStatus;
	private java.lang.String requestID;
	private com.exacttarget.ExtractResult[] results;

	public ExtractResponseMsg() {
	}

	public java.lang.String getOverallStatus() {
		return overallStatus;
	}

	public void setOverallStatus(java.lang.String overallStatus) {
		this.overallStatus = overallStatus;
	}

	public java.lang.String getRequestID() {
		return requestID;
	}

	public void setRequestID(java.lang.String requestID) {
		this.requestID = requestID;
	}

	public com.exacttarget.ExtractResult[] getResults() {
		return results;
	}

	public void setResults(com.exacttarget.ExtractResult[] results) {
		this.results = results;
	}

	public com.exacttarget.ExtractResult getResults(int i) {
		return this.results[i];
	}

	public void setResults(int i, com.exacttarget.ExtractResult value) {
		this.results[i] = value;
	}

}
